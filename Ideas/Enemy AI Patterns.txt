Mark
----
-If far, move towards target
-Otherwise, move in a rectangle around the target:
  -Choose random point on rectangle to stop
  -Once reached, stop, then attack
    -1/2 chance for punch or kick (knocks down)
    -1/3 chance to go on the defensive and retreat
    -Go back to move in a rectangle around the target
-If hit, goes on the defensive and retreats for 2.5 seconds

Imposter
--------
With Offensive Mask:
  -Choose one of two movement options randomly:
    1. Keep distance for 3.5 seconds
    2. Move in a rectangle around the target, then approach
    -Go on the Offensive
  //-If interrupted, go on the Defensive
  -On the Offensive:
    -After keeping distance: 
      -If far from target, rechoose approach
      -If mid-distance from target, throw small dagger
      -If near target, choose randomly between a Downward Slice or Stab Combo
    -After moving in a rectangle:
      -Go up to target and choose randomly between a Downward Slice or Stab Combo

With Defensive Mask:
  -Keep distance from target for 4 seconds, then go on the Offensive
  -On the Offensive:
    -Approach target directly
      -If in range to attack, kick the target
    -Regardless of what happens, go back to Movement
  -If grabbed, go on the Defensive
  -On the Defensive:
    -If grabbed, use Grab Breaker: break out, grab the target in return, then pummel target 2 times and throw       backwards (Sidenote: Crystal's aerial grab counts as well, but Crystal can counter this by doing a          standing jump attack after falling)
 
   -If comboed for longer than a total duration of 1 second, use Combo Breaker (Defensive Special)

With Speed Mask:
  -Jump towards target, and randomly match either the target's X position, Y position, or Y position while 
   going right in front of the target. Afterwards, go on the Offensive:
    -If close, do an uppercut
    -If mid distance, do a Leap Punch
    -If further, do a Double Kick move in the air like Crystal's running jump attack
      -After landing a hit, go on the Defensive
      -If hit or grabbed while on the Offensive, randomly decides to either jump back (Defensive) or go back 
       on the Offensive
    -If too far, go back to Movement
  -If hit on Movement, go on the Offensive
  -On the Defensive:
    -Jump away from the target and go back to Movement
    -If hit, go back to Movement

Weapon Wielders (Gerald/Kate)
-----------------------------
With no Weapon:
  -Check if the target has a weapon that isn't wearable:
    -If so, approach target directly and switch to Preset1
    -Otherwise, look if there are any non-wearable weapons on the ground
      -If so, pick up the nearest one
      -Otherwise, resort to fighting with hands:
        -Circle around the player: 
          -When completed, go on the Offensive
  -On the Offensive:
    -If reached, stop, then attack:
      -1/2 chance for a punch or a kick (knocks down)
      -If completed, go back to Movement
      -If interrupted or not finished, go to Defensive
  -On Preset1:
    -Check if the target has weapon that isn't wearable:
      -If so, grab the target to paralyze it and switch to Preset2
      -If not, go back to Movement
  -On Preset2:
    -Check if the target is grabbed and has a weapon that isn't wearable:
      -If so, steal the target's weapon and switch to the Offensive (goes to a new behavior since the enemy now has a weapon)      
      -If not, go back to Movement

With Wearable Weapon
  
With Stabbable Weapon

With Swingable Weapon:
  The Roll move will be used similarly to the Ninjas' "jump poke" move in Streets of Rage 2; that is,
         they'll use it when closeby as a Defensive action
  
  Constantly try to move at the target, approaching and retreating 2 times at an uncomfortable range

  -Approach the target closely and switch to Preset3
  -On the Offensive:
    -1/2 chance of doing either a Jump Slash Combo or a Horizontal Slash
      -After the Jump Slash in the Jump Slash Combo, switch to Preset2
      -After the Horizontal Slash, switch back to Movement
  -On the Defensive:
    -If close to the target and target is looking at you, roll behind the target and switch to Preset1
    -Otherwise, face the target and do a horizontal slash
  -On Preset1:
    -Performed after a Roll; perform an Uppercut Swipe (knocks down) and switch back to Movement
  -On Preset2:
    -Performed after a Jump Slash; perform a Horizontal Slash (knocks down) and switch back to Movement
  -On Preset3:
    -Retreat from the target for 1 second
      -On completion, 1/2 chance to switch to either Preset4 or Preset5
  -On Preset4:
    -Approach the target closely and go on the Defensive
  -On Preset5:
    -Approach the target from mid-distance and go on the Offensive

Commander (Level 4 Boss)
---------
  -Check if have no minions left
    -If not, start the spawn timer

  -If the spawn timer is started:
    -If the spawn time passed, spawn minions
  -On the Offensive:
    -If have minions and they're defending, there's a 1/3 chance of making them attack
      -If not, throw a random minion at the player
    -Otherwise, if Commander is close or mid distance to the target, perform a (Retreating) Slide and switch to Preset1
  -On the Defensive:
    -If have minions and they're not defending, there's a 1/3 chance of making them defend
    -Otherwise, perform some defensive move and switch back to Movement
  -On Preset 1:
    -Performed after a Slide; perform a Punch
  -On Preset2:
    -Performed after the Punch part of the Punch-Uppercut Combo; perform an Uppercut and switch back to Movement