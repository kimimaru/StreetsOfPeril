﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The playable character, Graham
    public class Graham : Player
    {
        //Graham's aerial special attack - Graham Cyclone
        protected bool IsCyclone;

        public Graham()
        {
            Velocity = new Vector2(3, 2);
            OrigJumpVelocity = new Vector3(Velocity.X, 0f, 4f);
            Weight = 155;
            DashAttackSpeedChange = .17f;

            CharNum = (int)Characters.Graham;

            SpriteSheet = LoadGraphics.CharSheets[CharNum][0];
            //Hurtbox = new Hurtbox(((int)StandingAnim.GetAnimSizeFirst().X / 4), (int)Location.W / 3);

            //Animations
            StandAnim = new NewAnimation(true, true, MaxStandingLoops, SpriteSheet, new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
            RunningAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(444, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(484, 16, 64, 72), 100), new AnimFrame(new Rectangle(551, 15, 66, 73), 125, new Vector2(5, 0)), new AnimFrame(new Rectangle(619, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(659, 14, 67, 74), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(728, 17, 67, 71), 125, new Vector2(7, 0)));
            JumpingAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 106, 38, 86), 0, new Vector2(2, 0)), new AnimFrame(new Rectangle(104, 114, 46, 78), 0, new Vector2(6, 0)));
            DefSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(456, 754, 58, 54), 60, new Vector2(16, 0)), new AnimFrame(new Rectangle(528, 730, 87, 78), 60, new Vector2(-22, 0)), new AnimFrame(new Rectangle(8, 818, 73, 102), 60, new Vector2(-12, 0)), new AnimFrame(new Rectangle(88, 817, 69, 103), 60, new Vector2(6, 0)), new AnimFrame(new Rectangle(168, 848, 91, 72), 60, new Vector2(17, 0)), new AnimFrame(new Rectangle(272, 860, 96, 60), 60, new Vector2(-16, 0)), new AnimFrame(new Rectangle(376, 860, 96, 60), 60, new Vector2(-16, 0)), new AnimFrame(new Rectangle(480, 860, 84, 60), 60, new Vector2(-10, 0)));
            OffSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 1526, 63, 66), 40, new Vector2(20, 17)), new AnimFrame(new Rectangle(128, 1525, 46, 67), 40, new Vector2(-3, 16)), new AnimFrame(new Rectangle(184, 1526, 71, 66), 40, new Vector2(-9, 17)), new AnimFrame(new Rectangle(264, 1525, 48, 67), 40, new Vector2(3, 16)));
            GrabAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 113, 54, 79), 300));
            LandAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 131, 40, 61), 70, new Vector2(-3, 0)));
            DashAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 150, new Vector2(1, 0)), new AnimFrame(new Rectangle(88, 560, 73, 48), 600, new Vector2(-1, 0)));
            PickUpAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 250));
            DownJumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(360, 424, 43, 64), 300, new Vector2(2, 0)));
            JumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(416, 402, 36, 86), 40, new Vector2(5, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 30, new Vector2(-5, 0)), new AnimFrame(new Rectangle(520, 400, 53, 88), 100, new Vector2(-7, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 150, new Vector2(-5, 0)));
            RunJumpAttackAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 50, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 500, new Vector2(-13, 23)));
            Grabbedanim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 945, 39, 63), 1500, new Vector2(4, -1)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(152, 931, 50, 77), 700, new Vector2(-2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(216, 955, 67, 53), 0, new Vector2(-1, 0)), new AnimFrame(new Rectangle(384, 980, 87, 28), 400, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(384, 980, 87, 28), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(560, 957, 45, 51), 100));
            ForwardKnockedDownAnim = new NewAnimation(KnockedDownAnim, 1f);
            SwitchSidesAnim = new NewAnimation(false, true, 0, SpriteSheet, new AnimFrame(new Rectangle(320, 109, 56, 83), 100, new Vector2(-15, 11)), new AnimFrame(new Rectangle(384, 115, 61, 77), 100, new Vector2(-10, 63)), new AnimFrame(new Rectangle(456, 96, 28, 96), 100, new Vector2(-34, 63)));
            FThrowAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(248, 745, 53, 63), 100, new Vector2(9, 0)), new AnimFrame(new Rectangle(312, 753, 57, 55), 100, new Vector2(12, 0)), new AnimFrame(new Rectangle(377, 775, 68, 33), 350, new Vector2(8, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(128, 398, 41, 90), 150, new Vector2(8, 0)), new AnimFrame(new Rectangle(176, 409, 48, 79), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(232, 418, 53, 70), 150, new Vector2(-11, 0)));
            StabAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 411, 36, 77), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(56, 413, 58, 75), 100, new Vector2(-13, 0)));

            AttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(80, 203, 40, 77), 50, new Vector2(-4, 0)), new AnimFrame(new Rectangle(128, 205, 67, 75), 150, new Vector2(-14, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(208, 201, 39, 79), 50, new Vector2(1, 0)), new AnimFrame(new Rectangle(256, 204, 43, 76), 50, new Vector2(4, 0)), new AnimFrame(new Rectangle(312, 212, 81, 68), 200, new Vector2(-10, 0))) };

            GrabAttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2)), new AnimFrame(new Rectangle(600, 205, 46, 75), 200, new Vector2(-19, 1)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2))) };
            VictoriousAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(610, 406, 39, 80), 150), new AnimFrame(new Rectangle(653, 406, 42, 80), 150));

            SwimAnim = VictoriousAnim;

            CurKnockDownAnim = KnockedDownAnim;

            //Initialize state machine
            StateMachine = new PlayerStateMachine(StandAnim, UpdateStandingAnim);

            IsCyclone = false;

            Name = "Graham";

            Lives = 5;
            Damage = 7;
            Defense = 2;
            HitStun = 700;
        }

        //Constructor
        public Graham(int health, int healthbars, int playernum, int costume, bool avoidenemy = false) : this()
        {
            PlayerNum = playernum;

            ChangeCostume(costume);

            SetUpHealth(health);
            SetUpHurtbox();

            hud = new PlayerHUD(this);
            //hud = LoadGraphics.CreatePlayerHud(PlayerNum, LoadGraphics.GrahamIcon, avoidenemy);
        }

        //Resets actions - for when you get hit, knocked down, die, or complete a level
        protected override void ResetActions()
        {
            base.ResetActions();

            IsCyclone = false;
        }

        //Take damage after hitting something or multiple things with a special attack - doesn't apply when you're invincible
        public override void SpecialAttackHit(float activeTime, int index)
        {
            //If Graham used the special attack when he wasn't invincible, make it subtract health if it hits
            if (SpecialAtkInvincible == false && SpecialAtkHit == false && (IsSpecialAttacking == true || IsCyclone == true))
            {
                SpecialAtkHit = true;
                if (TempBar <= 0f)
                {
                    hud.SetDamageIndicator(activeTime, Health, HealthBars, 10);
                    Health -= 10;
                }
                else
                {
                    //if (ShouldSetDamageIndicator(10) == true) hud.SetDamageIndicator(activeTime, TempBar, HealthBars, 10);
                    TempBar -= 10;
                }

                //HealthUpdate(activeTime);
            }
        }

        protected override void PlayerAttack(float activeTime, SubLevel level)
        {
            if (Controlled == true)
            {
                if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.Attack]) == true && CheckAttacking() == true)
                {
                    //Grabbing moves
                    if (IsGrabbing == true)
                    {
                        //Make sure you can't attack with a grab attack if the enemy you're trying to attack isn't grabbed and you're still grabbing (Ex. other player hits enemy out of your grab)
                        if (Enem.gGrabbox.GSIsGrabbed == true)
                        {
                            //Throw enemy backwards - press the opposite direction you're facing followed by X
                            if (Keyboard.GetState().IsKeyDown(Backwards))
                            {
                                FThrowAnim.Reset();
                                IsBackwardThrowing = true;
                                FlipDirection();
                                Enem.ChangeDirection(!FacingRight);
                                Enem.GetGrabbed(this);
                                LoadSounds.Play(LoadSounds.PThrow);

                                //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                if (Enem != null)
                                {
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 5, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                    CreateThrowHitbox(true, 20, 30, 5, 10, FThrowAnim.GetFrameLength(0), 1, TowardsDir, 100, ThrowFighter, true);
                                }
                            }
                            //Throw enemy forwards - press the same direction you're facing followed by X
                            else if (Keyboard.GetState().IsKeyDown(Forwards))
                            {
                                FThrowAnim.Reset();
                                IsForwardThrowing = true;
                                Enem.GetGrabbed(this);
                                LoadSounds.Play(LoadSounds.PThrow);

                                //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                if (Enem != null)
                                {
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 5, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                    CreateThrowHitbox(true, 20, 30, 5, 10, FThrowAnim.GetFrameLength(0), 1, TowardsDir, 100, ThrowFighter, true);
                                }
                            }
                            //Grab Attack/Back grapple attack
                            else
                            {
                                //If you have the NoAttack status you can't do grab attacks
                                if (StatusCondition(status != (int)Status.Statuses.NoAttack) == true && FacingRight != Enem.FacingRight)
                                {
                                    CurDamagingAnim = CurGrabAttackAnim;
                                    CurGrabAttackAnim.Reset();
                                    GrabAttacking = true;

                                    int hitboxstrength = 2;
                                    float framelength = CurGrabAttackAnim.GetCurFrameLength();
                                    Hitbox.HitboxTypes knockdown = Hitbox.HitboxTypes.Standard;

                                    if (ComboCounter == (GrabAttackingAnim.Count - 1))
                                    {
                                        hitboxstrength = 5;
                                        framelength = CurGrabAttackAnim.GetFrameLength(CurGrabAttackAnim.MaxFrame);
                                        knockdown = Hitbox.HitboxTypes.KnockDown;
                                    }

                                    CreateHitboxFeet(true, 20, 20, hitboxstrength, 10, 0, framelength, knockdown, TowardsDir, ComboCounter == (GrabAttackingAnim.Count - 1) ? LoadSounds.Kick : LoadSounds.Punch1, true);
                                }
                                else if (FacingRight == Enem.FacingRight)
                                {
                                    //Back grapple attack
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 10, 20, 20, CurHeight + 10, 10, 8, 0, 1, true, FacingRight, null, 100, true));
                                }
                            }
                        }
                    }
                    //Jump attacks
                    else if (CanJump == false)
                    {
                        //If you have the NoJump status then you can't do jump attacks (you can fall off higher ground or objects to get into the air)
                        if (status != (int)Status.Statuses.NoJump)
                        {
                            LoadSounds.Play(LoadSounds.PThrow);

                            //Down jump attack
                            if (DownJumpAttacking == false && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]))
                            {
                                CurDamagingAnim = DownJumpAtkAnim;
                                DownJumpAtkAnim.Reset();
                                DownJumpAttacking = true;
                                CreateHitboxFeet(true, 20, 20, 2, 50, 0, DownJumpAtkAnim.FullDuration(), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                            }
                            //Standing jump attack
                            else if (AirVelocity.X == 0f)
                            {
                                CurDamagingAnim = JumpAtkAnim;
                                JumpAtkAnim.Reset();
                                JumpAttacking = true;

                                CreateHitboxFeet(true, 20, 20, 4, 50, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame - 1, JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame - 1), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                            }
                            //Running jump attack
                            else
                            {
                                CurDamagingAnim = RunJumpAttackAnim;
                                RunJumpAttackAnim.Reset();
                                JumpAttacking = true;

                                CreateHitboxFeet(true, 60, 20, 3, 60, RunJumpAttackAnim.GetFrameLength(0), RunJumpAttackAnim.GetFrameLength(RunJumpAttackAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                            }
                        }
                    }
                    //Standing attack and weapon attacks
                    else
                    {
                        //If you have the NoAttack status then you can't do standard or weapon attacks
                        if (status != (int)Status.Statuses.NoAttack)
                        {
                            //If you have a weapon and it's swingable or stabbable, use the weapon
                            if (weapon != null && weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                            {
                                //If your weapon is swingable, swing the weapon
                                if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                                {
                                    CurDamagingAnim = SwingAnim;
                                    SwingAnim.Reset();
                                    IsSwinging = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), SwingAnim.GetFrameLength(0), SwingAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                                //If your weapon stabs, stab with the weapon
                                else if (weapon != null && weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                                {
                                    CurDamagingAnim = StabAnim;
                                    StabAnim.Reset();
                                    IsStabbing = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 4, StabAnim.GetFrameLength(0), StabAnim.GetFrameLength(StabAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                            }
                            //Do a dash attack if you're running
                            else if (IsRunning == true)
                            {
                                CurDamagingAnim = DashAtkAnim;
                                DashAtkAnim.Reset();
                                DashAttackSpeed = KnockDownVelocity + 4f;
                                IsAttacking = true;

                                CreateHitboxFeet(true, 45, 30, 3, 10, DashAtkAnim, 1, 1, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                            }
                            //If you don't have a weapon or it's not swingable, do a normal attack
                            else
                            {
                                //Check if we should use the next combo animation
                                CheckComboTimeFrame();

                                CurDamagingAnim = CurAttackAnim;
                                CurAttackAnim.Reset();
                                LoadSounds.Play(LoadSounds.Attack);
                                IsAttacking = true;
                                IsWalking = false;
                                IsRunning = false;

                                //Allow Graham to turn around when starting an attack
                                if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Left])) FacingRight = false;
                                else if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Right])) FacingRight = true;

                                float activation = CurAttackAnim.MaxFrame > 0 ? CurAttackAnim.FullDurationExcluding(CurAttackAnim.MaxFrame) : 0f;
                                float duration = CurAttackAnim.GetFrameLength(CurAttackAnim.MaxFrame);

                                CreateHitboxFeet(true, ComboCounter == (AttackingAnim.Count - 1) ? 40 : 30, 20, (ComboCounter / 2) * 2, 50, activation, duration, ComboCounter == (AttackingAnim.Count - 1) ? Hitbox.HitboxTypes.KnockDown : Hitbox.HitboxTypes.Standard, TowardsDir, ComboCounter == (AttackingAnim.Count - 1) ? LoadSounds.Kick : LoadSounds.Punch1, true);
                            }
                        }
                    }
                }
            }
        }
        
        //The player's special attack - can be used in hitstun and while grabbing an enemy
        protected override void PlayerSpecialAttack()
        {
            if (IsSpecialAttacking == false && IsCyclone == false)
            {
                if (Controlled == true && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.SpecAttack]) == true)
                {
                    if (CheckCanUseSpecialAttack() == true)
                    {
                        IsWalking = false;
                        IsRunning = false;

                        //Do Graham's cyclone attack
                        if (CanJump == false)
                        {
                            LoadSounds.Play(LoadSounds.PThrow);
                            CurDamagingAnim = OffSpecAnim;
                            OffSpecAnim.Reset();
                            IsCyclone = true;
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            //The entire animation is very short, so each time Graham rotates will count as one hitbox (think of him spin-kicking the same enemy 5 times)
                            CreateHitboxSurrounding(2, 50, 0, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                            CreateHitboxSurrounding(2, 50, 100, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                            CreateHitboxSurrounding(2, 50, 200, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                            CreateHitboxSurrounding(3, 50, 300, 100, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                        }
                        //Do Graham's defensive special attack
                        else
                        {
                            LoadSounds.Play(LoadSounds.Explosion);
                            CurDamagingAnim = DefSpecAnim;
                            DefSpecAnim.Reset();
                            IsSpecialAttacking = true;
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            //Reset the grab attack counter if you used a special attack while grabbing
                            GrabAttacking = false;
                            ComboCounter = 0;
                            if (Enem != null)
                                Enem.GrabRecover();
                            IsHit = false;
                            CreateHitboxSurrounding(4, 50, 0, DefSpecAnim.FullDuration(), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                        }
                    }
                }
            }
        }

        //Checks what the player is holding
        protected override void CheckHolds(float activeTime, SubLevel level)
        {
            //Check if player is grabbing an enemy
            if (Enem != null)
            {
                if (IsDead == true || IsGrabbed == true)
                    Enem.GrabRecover();

                //Check if the player wants to release a grabbed enemy
                ReleaseEnemy(activeTime);

                if (Enem.gGrabbox.GSIsGrabbed == false)
                {
                    //Make sure the grab attack and throwing animations play out completely
                    if (GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false)
                    {
                        IsGrabbing = false;
                        //Enem = null;
                        GrabbedFighter = null;
                    }
                }
            }
        }

        //Animation updates
        protected void UpdateCycloneAnim()
        {
            if (IsCyclone == true)
                OffSpecAnim.Update(Main.GetActiveTime, GravityValue);
        }
        //End updates

        //Checks for animations
        protected override bool CheckAttacking()
        {
            return (base.CheckAttacking() == true && IsCyclone == false);
        }

        protected override bool CheckSpecialAttacking()
        {
            return (base.CheckSpecialAttacking() == true && JumpAttacking == false && DownJumpAttacking == false);
        }
        //End checks

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, float waterheight, Color drawcolor, float Depth)
        {
            Vector2 drawloc = GetDrawLoc(OffSet);
            if (IsCyclone == true)
                OffSpecAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else base.DrawAnimations(spriteBatch, OffSet, waterheight, drawcolor, Depth);
        }

        //Updates Animations
        public override void UpdateAnimations()
        {
            UpdateCycloneAnim();
            base.UpdateAnimations();
        }
    }
}
