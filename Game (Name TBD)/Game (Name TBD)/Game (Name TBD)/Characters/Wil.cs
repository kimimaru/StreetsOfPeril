﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The playable character, Wil
    public class Wil : Player
    {
        //The maximum number of free bullets Wil can store
        public const int MaxFreeBullets = 2;

        //The strength of Wil's Defensive and Offensive bullets, respectively
        public const int DefensiveBulletStrength = 5;
        public const int OffensiveBulletStrength = 9;

        //How long it takes Wil to shoot his gun with his Offensive Special
        private const float ShootTimer = 500f;
        private const float NewFreeBulletTimer = 60000f;

        //Wil's tripping animation
        private NewAnimation TripAnim;
        private float PrevShoot;

        //wil's exclusive actions
        private bool IsTripping;
        private bool IsShooting;

        //Wil's gun bullets - 2 free bullets max, and a new one every minute
        private int FreeBullets;
        private float PrevBullet;

        //Tracks the current number the bullets should be assigned to and how many of them have hit
        private int BulletCount;
        private List<int> BulletHit;

        //Constructor
        public Wil(int health, int healthbars, int playernum, int costume, bool avoidenemy = false)
        {
            Velocity = new Vector2(2, 2);
            OrigJumpVelocity = new Vector3(Velocity.X, 0f, 4f);
            Weight = 148;
            DashAttackSpeedChange = .15f;

            CharNum = (int)Characters.Wil;

            SpriteSheet = LoadGraphics.CharSheets[CharNum][0];
            ChangeCostume(costume);
            //Hurtbox = new Hurtbox(((int)StandingAnim.GetAnimSizeFirst().X / 4), (int)Location.W / 3);

            //Animations
            StandAnim = new NewAnimation(true, true, MaxStandingLoops, SpriteSheet, new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
            RunningAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(444, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(484, 16, 64, 72), 100), new AnimFrame(new Rectangle(551, 15, 66, 73), 125, new Vector2(5, 0)), new AnimFrame(new Rectangle(619, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(659, 14, 67, 74), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(728, 17, 67, 71), 125, new Vector2(7, 0)));
            JumpingAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 106, 38, 86), 0, new Vector2(2, 0)), new AnimFrame(new Rectangle(104, 114, 46, 78), 0, new Vector2(6, 0)));
            DefSpecAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(534, 517, 73, 88), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(618, 515, 73, 90), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(702, 517, 73, 88), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(786, 518, 73, 87), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(870, 517, 73, 88), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(954, 515, 73, 90), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(1038, 517, 73, 88), 150, new Vector2(-5, 0)), new AnimFrame(new Rectangle(1122, 518, 73, 87), 150, new Vector2(-5, 0)));
            OffSpecAnim = new NewAnimation(DefSpecAnim, 1f);//new NewAnimation(false, false, 0, CharSheet, new AnimFrame(new Rectangle(534, 517, 73, 88), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(618, 515, 73, 90), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(702, 517, 73, 88), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(786, 518, 73, 87), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(870, 517, 73, 88), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(954, 515, 73, 90), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(1038, 517, 73, 88), 500, new Vector2(-5, 0)), new AnimFrame(new Rectangle(1122, 518, 73, 87), 500, new Vector2(-5, 0)));
            GrabAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 113, 54, 79), 300));
            LandAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 131, 40, 61), 70, new Vector2(-3, 0)));
            DashAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 350, new Vector2(1, 0)), new AnimFrame(new Rectangle(88, 560, 73, 48), 600, new Vector2(-1, 0)));
            PickUpAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 250));
            DownJumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(360, 424, 43, 64), 300, new Vector2(2, 0)));
            JumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(416, 402, 36, 86), 40, new Vector2(5, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 30, new Vector2(-5, 0)), new AnimFrame(new Rectangle(520, 400, 53, 88), 100, new Vector2(-7, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 150, new Vector2(-5, 0)));
            RunJumpAttackAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 50, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 500, new Vector2(-13, 23)));
            Grabbedanim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 945, 39, 63), 1500, new Vector2(4, -1)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(152, 931, 50, 77), 725, new Vector2(-2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(216, 955, 67, 53), 0, new Vector2(-1, 0)), new AnimFrame(new Rectangle(384, 980, 87, 28), 400, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(384, 980, 87, 28), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(560, 957, 45, 51), 100));
            ForwardKnockedDownAnim = new NewAnimation(KnockedDownAnim, 1f);
            SwitchSidesAnim = new NewAnimation(false, true, 0, SpriteSheet, new AnimFrame(new Rectangle(320, 109, 56, 83), 100, new Vector2(-15, 11)), new AnimFrame(new Rectangle(384, 115, 61, 77), 100, new Vector2(-10, 63)), new AnimFrame(new Rectangle(456, 96, 28, 96), 100, new Vector2(-34, 63)));
            FThrowAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(248, 745, 53, 63), 150, new Vector2(9, 0)), new AnimFrame(new Rectangle(312, 753, 57, 55), 150, new Vector2(12, 0)), new AnimFrame(new Rectangle(377, 775, 68, 33), 150, new Vector2(8, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(128, 398, 41, 90), 150, new Vector2(8, 0)), new AnimFrame(new Rectangle(176, 409, 48, 79), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(232, 418, 53, 70), 150, new Vector2(-11, 0)));
            StabAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 411, 36, 77), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(56, 413, 58, 75), 100, new Vector2(-13, 0)));

            AttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(80, 203, 40, 77), 50, new Vector2(-4, 0)), new AnimFrame(new Rectangle(128, 205, 67, 75), 150, new Vector2(-14, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(208, 201, 39, 79), 50, new Vector2(1, 0)), new AnimFrame(new Rectangle(256, 204, 43, 76), 50, new Vector2(4, 0)), new AnimFrame(new Rectangle(312, 212, 81, 68), 200, new Vector2(-10, 0))) };
            GrabAttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2)), new AnimFrame(new Rectangle(600, 205, 46, 75), 200, new Vector2(-19, 1)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2))) };
            VictoriousAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(610, 406, 39, 80), 150), new AnimFrame(new Rectangle(653, 406, 42, 80), 150));
            TripAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 300));

            CurKnockDownAnim = KnockedDownAnim;

            //StateMachine = new PlayerStateMachine(StandAnim);

            PlayerNum = playernum;

            IsTripping = false;
            IsShooting = false;

            Name = "Wil";

            ObjectHeight = 78;

            SetUpHealth(health);
            SetUpHurtbox();

            FreeBullets = 2;
            PrevBullet = 0f;
            PrevShoot = 0f;

            BulletCount = 0;
            BulletHit = new List<int>();

            Lives = 5;
            Damage = 9;
            Defense = 3;
            HitStun = 725;
            hud = new WilHUD(this);
        }

        //Gets the number of free bullets Wil has
        public int GetFreeBullets
        {
            get { return FreeBullets; }
        }

        //Tells if Wil has free bullets
        public bool HasFreeBullets
        {
            get { return (FreeBullets > 0); }
        }

        //Spawns the player at the start of a sublevel
        public override void SpawnSub(float activeTime, TileEngine TileEngine, float X = 100, float Y = 200, float Z = 0f, bool firstsub = false, bool bonusstatuson = true)
        {
            FreeBullets = 2;
            SetBulletTimer();

            BulletCount = 0;
            BulletHit.Clear();

            base.SpawnSub(activeTime, TileEngine, X, Y, Z, firstsub, bonusstatuson);
        }

        //Respawns the player after losing a life - Continue parameter is for when a player uses a continue
        public override void Respawn(float activeTime, Vector3 Position, TileEngine TileEngine)
        {
            base.Respawn(activeTime, Position, TileEngine);
            FreeBullets = 2;
            SetBulletTimer();
        }

        //Resets actions - for when you get hit, knocked down, die, or complete a level
        protected override void ResetActions()
        {
            base.ResetActions();

            IsTripping = false;
            IsShooting = false;
        }

        protected override void BeginFall()
        {
            if (IsSpecialAttacking == true) BulletCount++;
            base.BeginFall();
        }

        protected override bool OffSpecInput()
        {
            return (PressedAttack(true) == true && PressedSpecAttack() == true);
        }

        //Make Wil lose health when one of his bullets damages something
        public override void SpecialAttackHit(float activeTime, int index)
        {
            //If Wil has infinite bullets, don't subtract free bullets or health
            if (status != (int)Status.Statuses.InfBullets)
            {
                //If the bullet was shot when Wil wasn't invincible, subtract free bullets or health
                /*if (Bullets[index].ShotState == false && BulletHit.Contains(Bullets[index].BulletNum))
                {
                    BulletHit.Remove(Bullets[index].BulletNum);

                    //Subtract a free bullet; if Wil is invincible or has infinite bullets (unique status for Break The Objects), don't subtract one or lose health
                    if (HasFreeBullets == true) FreeBullets--;
                    //If you don't have any free bullets, lose health for firing (if you're not Invincible)
                    else
                    {
                        //Lose 10 health if you have that much, otherwise lose whatever health you had -1 remaining to stop at 1 Health
                        float healthloss;

                        if (TempBar <= 0)
                        {
                            healthloss = (Health > 10) ? 10 : (Health - 1);
                            hud.SetDamageIndicator(activeTime, Health, HealthBars, (int)healthloss);
                            Health -= (int)healthloss;
                        }
                        else
                        {
                            //If Wil has over 10 health or 10 health in his temporary health bar, subtract 10
                            //Otherwise, subtract the (amount in his temporary health bar + his health) - 1 to end up at 1 health
                            if (TempBar > 10 || Health > 10) healthloss = 10;
                            else healthloss = ((TempBar + Health) - 1);

                            //if (ShouldSetDamageIndicator((int)healthloss) == true) hud.SetDamageIndicator(activeTime, TempBar, HealthBars, (int)healthloss);
                            TempBar -= (int)healthloss;
                        }

                        //HealthUpdate(activeTime);
                    }
                }*/
            }
        }

        private void SetBulletTimer()
        {
            PrevBullet = Main.GetActiveTime + NewFreeBulletTimer;
        }

        protected override void WilBulletChecks(float activeTime)
        {
            //Check to clear the bullethit list
            /*if (BulletHit.Count != 0 && Bullets.Count == 0)
            {
                BulletCount = 0;
                BulletHit.Clear();
            }*/

            //Check if Wil should receive a new free bullet
            if (FreeBullets < MaxFreeBullets && activeTime >= PrevBullet)
            {
                FreeBullets++;
                SetBulletTimer();
            }
        }

        //Attack method
        protected override void PlayerAttack(float activeTime, SubLevel level)
        {
            if (Controlled == true)
            {
                if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.Attack]) == true && CheckAttacking() == true)
                {
                    //Grabbing moves
                    if (IsGrabbing == true)
                    {
                        //Make sure you can't attack with a grab attack if the enemy you're trying to attack isn't grabbed and you're still grabbing (Ex. other player hits enemy out of your grab)
                        if (Enem.gGrabbox.GSIsGrabbed == true)
                        {
                            //Throw enemy backwards - press the opposite direction you're facing followed by X
                            if (Keyboard.GetState().IsKeyDown(Backwards))
                            {
                                FThrowAnim.Reset();
                                IsBackwardThrowing = true;
                                Enem.GetGrabbed(this);
                                LoadSounds.Play(LoadSounds.PThrow);

                                //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                if (Enem != null)
                                {
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                }
                            }
                            //Throw enemy forwards - press the same direction you're facing followed by X
                            else if (Keyboard.GetState().IsKeyDown(Forwards))
                            {
                                FThrowAnim.Reset();
                                IsForwardThrowing = true;
                                Enem.GetGrabbed(this);
                                LoadSounds.Play(LoadSounds.PThrow);

                                //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                if (Enem != null)
                                {
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 6, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                }
                            }
                            //Grab Attack/Back grapple attack
                            else
                            {
                                //If you have the NoAttack status you can't do grab attacks
                                if (StatusCondition(status != (int)Status.Statuses.NoAttack) && FacingRight != Enem.FacingRight)
                                {
                                    CurDamagingAnim = CurGrabAttackAnim;
                                    CurGrabAttackAnim.Reset();
                                    GrabAttacking = true;

                                    if (ComboCounter == (GrabAttackingAnim.Count - 1))
                                        CreateHitboxFeet(true, 20, 20, 2, 10, 0, CurGrabAttackAnim.GetCurFrameLength(), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                                    else CreateHitboxFeet(true, 20, 20, 2, 10, 0, CurGrabAttackAnim.GetCurFrameLength(), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                                }
                                else if (FacingRight == !Enem.FacingRight)
                                {
                                    //Trip attack
                                    IsTripping = true;
                                    TripAnim.Reset();
                                    Enem.GetGrabbed(this);
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 7, /*TripAnim.FullDurationExcluding(0)*/TripAnim.FullDuration(), 1, true, FacingRight, LoadSounds.Punch1, 100, true));
                                }
                            }
                        }
                    }
                    //Jump attacks
                    else if (CanJump == false)
                    {
                        //If you have the NoJump status then you can't do jump attacks (you can fall off higher ground or objects to get into the air)
                        if (status != (int)Status.Statuses.NoJump)
                        {
                            LoadSounds.Play(LoadSounds.PThrow);

                            //Down jump attack
                            if (DownJumpAttacking == false && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]))
                            {
                                CurDamagingAnim = DownJumpAtkAnim;
                                DownJumpAtkAnim.Reset();
                                DownJumpAttacking = true;                               
                                CreateHitboxFeet(true, 20, 30, 3, 10, 0, DownJumpAtkAnim.FullDuration(), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                            }
                            //Standing jump attack
                            else if (AirVelocity.X == 0f)
                            {
                                //Wil kicks forwards then back
                                CurDamagingAnim = JumpAtkAnim;
                                JumpAtkAnim.Reset();
                                CreateHitboxFeet(true, 20, 30, 4, 10, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame - 1, JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame - 1), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                                CreateHitboxFeet(false, 20, 30, 3, 10, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                                JumpAttacking = true;
                            }
                            //Running jump attack
                            else
                            {
                                CurDamagingAnim = RunJumpAttackAnim;
                                RunJumpAttackAnim.Reset();
                                CreateHitboxFeet(true, 60, 20, 4, 10, RunJumpAttackAnim.GetFrameLength(0), RunJumpAttackAnim.GetFrameLength(RunJumpAttackAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                                JumpAttacking = true;
                            }
                        }
                    }
                    //Standing attack and weapon attacks
                    else
                    {
                        //If you have the NoAttack status then you can't do standard or weapon attacks
                        if (status != (int)Status.Statuses.NoAttack)
                        {
                            //If you have a weapon and it's swingable or stabbable, use the weapon
                            if (weapon != null && weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                            {
                                //If your weapon is swingable, swing the weapon
                                if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                                {
                                    CurDamagingAnim = SwingAnim;
                                    SwingAnim.Reset();
                                    IsSwinging = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), SwingAnim.GetFrameLength(0), SwingAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                                //If your weapon stabs, stab with the weapon
                                else if (weapon != null && weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                                {
                                    CurDamagingAnim = StabAnim;
                                    StabAnim.Reset();
                                    IsStabbing = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 4, StabAnim.GetFrameLength(0), StabAnim.GetFrameLength(StabAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                            }
                            //Do a dash attack if you're running
                            else if (IsRunning == true)
                            {
                                CurDamagingAnim = DashAtkAnim;
                                DashAtkAnim.Reset();
                                IsAttacking = true;
                            
                                CreateHitboxFeet(true, 45, 20, 4, 60, 200, 150, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);
                                CreateHitboxFeet(true, 60, 20, 4, 60, 700, 150, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                            }
                            //If you don't have a weapon or it's not swingable, do a normal attack
                            else
                            {
                                CheckComboTimeFrame();

                                CurDamagingAnim = CurAttackAnim;
                                CurAttackAnim.Reset();
                                LoadSounds.Play(LoadSounds.Attack);
                                IsAttacking = true;
                                IsWalking = false;
                                IsRunning = false;

                                //Allow Wil to turn around when starting an attack
                                if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Left])) FacingRight = false;
                                else if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Right])) FacingRight = true;

                                CreateHitboxFeet(true, 35, 30, ComboCounter + 1, 10, 0, CurAttackAnim.GetCurFrameLength(), ComboCounter == (AttackingAnim.Count - 1) ? Hitbox.HitboxTypes.KnockDown : Hitbox.HitboxTypes.Standard, TowardsDir, ComboCounter == (AttackingAnim.Count - 1) ? LoadSounds.Kick : LoadSounds.Punch1, true);
                            }
                        }
                    }
                }
            }
        }

        //Makes Wil shoot a bullet during Wil's Defensive Special; the direction and orientation of the bullet depends on which frame in the animation finished
        private void ShootBullet(int framenum)
        {
            //Directions are reversed if the player is facing a different direction
            int direction = ForwardVal(1);
            
            //Change the direction and spawn location of the bullet depending on which direction Wil is facing
            int spawnlocleft = ForwardVal(CollisionBox.X, CollisionBox.X + CollisionBox.Width);
            int spawnlocright = ForwardVal(CollisionBox.X + CollisionBox.Width, CollisionBox.X);

            int spawnbottom = FeetLoc.Center.Y;

            float Z = Location.Z + 35f;

            //Add to the bullethit list on the first frame of the animation (played normally or reversed) so that the first bullet or subsequent bullets will subtract a free bullet or health if they hit
            if ((framenum == 0 && DefSpecAnim.IsReversing() == false) || (framenum == DefSpecAnim.MaxFrame && DefSpecAnim.IsReversing() == true))
            {
                //Add to the bullet count
                BulletHit.Add(BulletCount);
            }

            //The state of Wil when shooting the bullet (invincible or not)
            bool shotstate = status == (int)Status.Statuses.Invincible;

            //Bullet going up-right/up-left
            if (framenum == 0) ShootProjectile(new WBullet(this, new Vector2(5 * direction, -5), new Vector3(spawnlocright, spawnbottom, Z), BulletCount, true));
            //Bullet going up
            else if (framenum == 1) ShootProjectile(new WBullet(this, new Vector2(0, -5), new Vector3(CollisionBox.X + (CollisionBox.Width / 2), spawnbottom, Z), BulletCount, true));
            //Bullet going up-left/up-right
            else if (framenum == 2) ShootProjectile(new WBullet(this, new Vector2(-5 * direction, -5), new Vector3(spawnlocleft, spawnbottom, Z), BulletCount, true));
            //Bullet going left/right
            else if (framenum == 3) ShootProjectile(new WBullet(this, new Vector2(-5 * direction, 0), new Vector3(spawnlocleft, spawnbottom, Z), BulletCount, true));
            //Bullet going down-left/down-right
            else if (framenum == 4) ShootProjectile(new WBullet(this, new Vector2(-5 * direction, 5), new Vector3(spawnlocleft, spawnbottom, Z), BulletCount, true));
            //Bullet going down
            else if (framenum == 5) ShootProjectile(new WBullet(this, new Vector2(0, 5), new Vector3(CollisionBox.X + (CollisionBox.Width / 2), spawnbottom, Z), BulletCount, true));
            //Bullet going down-right/down-left
            else if (framenum == 6) ShootProjectile(new WBullet(this, new Vector2(5 * direction, 5), new Vector3(spawnlocright, spawnbottom, Z), BulletCount, true));
            //Bullet going right/left
            else ShootProjectile(new WBullet(this, new Vector2(5 * direction, 0), new Vector3(spawnlocright, spawnbottom, Z), BulletCount, true));

            LoadSounds.Play(LoadSounds.Gunshot);
        }

        //Sets the bullet direction and direction Wil is facing while shooting a bullet during Wil's Offensive Special
        private Vector2 SetBulletDirection(out int framenum)
        {
            Vector2 BulletSpeed = new Vector2(ForwardVal(5), 0f);
            framenum = DefSpecAnim.MaxFrame;

            //Make the bullet go up or down if the player is holding up or down
            if (Input.KeyHeld(GetActionKey((int)Action.Up)) == true)
            {
                BulletSpeed.Y = -5;
                framenum = 1;
            }
            else if (Input.KeyHeld(GetActionKey((int)Action.Down)) == true)
            {
                BulletSpeed.Y = 5;
                framenum = 5;
            }

            //If the player isn't holding forwards...
            if (Input.KeyHeld(Forwards) == false)
            {
                //If the player holds backwards and isn't holding forwards (prevents constant changing direction), then change the direction Wil is facing and make the bullet go in the opposite direction
                if (Input.KeyHeld(Backwards) == true)
                {
                    BulletSpeed.X = -BulletSpeed.X;
                    FlipDirection();
                }
                //Otherwise, check if the bullet is moving up or down and set the bullet's X velocity to 0; this means the player is holding only either up or down
                else if (BulletSpeed.Y != 0f)
                    BulletSpeed.X = 0f;
            }
            //If the player is holding forwards, check the Y direction the bullet is moving in and make Wil face diagonally if the bullet is moving diagonally
            else
            {
                if (BulletSpeed.Y < 0f) framenum--;
                else if (BulletSpeed.Y > 0f) framenum++;
            }

            return BulletSpeed;
        }

        //Wil's special attacks - shoot bullets in 8 directions (Defensive) and hold down button for directed gun shot (Offensive)
        protected override void PlayerSpecialAttack()
        {
            if (IsSpecialAttacking == false && IsShooting == false)
            {
                if (Controlled == true && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.SpecAttack]) == true)
                {
                    //Wil's specials - First check if Wil is able to perform one of his specials and has enough health or free bullets to do so
                    if (CheckCanUseSpecialAttack() == true)
                    {
                        //Wil's offensive special (hold Attack then press Special Attack) - fire a gun; can't use while in hitstun
                        if (IsHit == false && IsGrabbing == false && (status == (int)Status.Statuses.InfBullets || Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Attack])))
                        {
                            //Start the gun shot - the player has to hold down the Special Attack button for it to finish
                            PrevShoot = Main.GetActiveTime + ShootTimer;
                            IsShooting = true;
                            IsWalking = false;
                            IsRunning = false;
                        }
                        //Wil's defensive special - fire 8 bullets around him in a circle
                        else if (status != (int)Status.Statuses.InfBullets)
                        {
                            //Allow the player to change the circular direction Wil shoots his bullets in (hold Down for clockwise and nothing or anything else for counter-clockwise)
                            if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]) == true)
                                DefSpecAnim.ResetReverse();
                            else
                                DefSpecAnim.Reset();

                            IsSpecialAttacking = true;
                            IsHit = false;
                            IsWalking = false;
                            IsRunning = false;

                            //Reset the grab attack counter if you used a special attack while grabbing
                            ComboCounter = 0;
                            if (Enem != null)
                                Enem.GrabRecover();
                        }
                    }
                }
            }
        }

        //Checks what the player is holding
        protected override void CheckHolds(float activeTime, SubLevel level)
        {
            //Check if player is grabbing an enemy
            if (Enem != null)
            {
                if (IsDead == true || IsGrabbed == true)
                    Enem.GrabRecover();

                //Check if the player wants to release a grabbed enemy
                if (IsTripping == false)
                    ReleaseEnemy(activeTime);

                if (Enem.gGrabbox.GSIsGrabbed == false)
                {
                    //Make sure the grab attack and throwing animations play out completely
                    if (GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsTripping == false)
                    {
                        IsGrabbing = false;
                        GrabbedFighter = null;
                    }
                }
            }
        }

        //Animation updates
        private void UpdateTrippingAnim()
        {
            if (IsTripping == true)
            {
                TripAnim.Update(Main.GetActiveTime, GravityValue);
                if (TripAnim.IsAnimationEnd() == true)
                    IsTripping = false;
            }
        }

        protected override void UpdateOffSpecAnim()
        {
            //The player has to keep holding the Special Attack button to aim
            if (PressedSpecAttack(true) == true)
            {
                /*Allows the player to direct Wil's bullets; the direction defaults at Forward, and HOLDING a direction or a combination changes the following:
                 -The sprite used for Wil; the direction he's facing will reflect that of his Defensive Special (Ex. upwards if holding Up)
                 -The velocity of the bullet
                 
                 -Not holding anything resets back to the default direction (Forwards)
                 -Pressing backwards will literally change the direction Wil faces*/

                //By default, the bullet goes directly forwards
                int framenum;
                Vector2 BulletSpeed = SetBulletDirection(out framenum);

                //Change the way Wil is facing
                DefSpecAnim.SkipToFrame(framenum, false);

                //Once the animation is over, shoot the bullet in the direction specified at the end
                if (Main.GetActiveTime >= PrevShoot)
                {
                    //Create a new bullet
                    ShootProjectile(new WBullet(this, BulletSpeed, new Vector3(CollisionBox.X + CollisionBox.Width, FeetLoc.Center.Y, Location.Z + 35f), BulletCount, false, 80));

                    //Increment the bullet number
                    BulletHit.Add(BulletCount);
                    BulletCount++;

                    LoadSounds.Play(LoadSounds.Gunshot);

                    //Reset the free bullet timer when the gun is shot
                    SetBulletTimer();

                    StateMachine.ChangeStateIdle();
                }
            }
            //If the button is let go, go back to idle
            else
            {
                StateMachine.ChangeStateIdle();
            }
        }

        private void UpdateShootAnim()
        {
            //To fire the gun shot the special attack button must be held; to cancel firing, simply let go of the button
            if (IsShooting == true && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.SpecAttack]))
            {
                ResetStandingAnim();

                /*Allows the player to direct Wil's bullets; the direction defaults at Forward, and HOLDING a direction or a combination changes the following:
                 -The sprite used for Wil; the direction he's facing will reflect that of his Defensive Special (Ex. upwards if holding Up)
                 -The velocity of the bullet
                 
                 -Not holding anything resets back to the default direction (Forwards)
                 -Pressing backwards will literally change the direction Wil faces*/

                //By default, the bullet goes directly forwards
                int framenum;
                Vector2 BulletSpeed = SetBulletDirection(out framenum);

                //Change the way Wil is facing
                DefSpecAnim.SkipToFrame(framenum, false);

                //Once the animation is over, shoot the bullet in the direction specified at the end
                if (Main.GetActiveTime >= PrevShoot)
                {
                    IsShooting = false;

                    //Create a new bullet
                    ShootProjectile(new WBullet(this, BulletSpeed, new Vector3(CollisionBox.X + CollisionBox.Width, FeetLoc.Center.Y, Location.Z + 35f), BulletCount, false, 80));

                    //Increment the bullet number
                    BulletHit.Add(BulletCount);
                    BulletCount++;

                    LoadSounds.Play(LoadSounds.Gunshot);

                    //Reset the free bullet timer when the gun is shot
                    SetBulletTimer();
                }
            }
            else IsShooting = false;
        }

        protected override void UpdateDashAttackAnim()
        {
            if (IsRunning == true && IsAttacking == true)
            {
                DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();

                if (DashAtkAnim.IsAnimationEnd() == true)
                {
                    IsRunning = false;
                    IsAttacking = false;
                }
            }
        }

        protected override void UpdateDefSpecAnim()
        {
            //Get the frame of the defensive special animation that Wil is on
            int animnum = DefSpecAnim.CurrentFrame;
            DefSpecAnim.Update(Main.GetActiveTime, GravityValue);
            ResetStandingAnim();

            //If the animation moved onto a new frame, create a bullet that travels in a direction based on the frame that just passed (Ex. Wil facing northeast and the animation progresses, so the bullet moves northeast)
            if (animnum != DefSpecAnim.CurrentFrame)
                ShootBullet(animnum);

            if (DefSpecAnim.IsAnimationEnd() == true)
            {
                BulletCount++;
                StateMachine.ChangeStateIdle();
            }
        }

        protected override void UpdateSpecialAttackAnim()
        {
            if (IsSpecialAttacking == true)
            {
                //Get the frame of the defensive special animation that Wil is on
                int animnum = DefSpecAnim.CurrentFrame;
                DefSpecAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();

                //If the animation moved onto a new frame, create a bullet that travels in a direction based on the frame that just passed (Ex. Wil facing northeast and the animation progresses, so the bullet moves northeast)
                if (animnum != DefSpecAnim.CurrentFrame)
                    ShootBullet(animnum);

                if (DefSpecAnim.IsAnimationEnd() == true)
                {
                    IsSpecialAttacking = false;
                    BulletCount++;
                }
            }
        }
        //End updates

        //Checks if the character can use a special attack or not
        protected override bool CheckCanUseSpecialAttack()
        {
            float healthvalue = Health;

            //Check if the player has a temp health bar and use that value plus the player's health instead of just the health
            if (TempBar > 0f)
            {
                healthvalue = TempBar + Health;
            }

            return (status != (int)Status.Statuses.NoSpecial && CheckSpecialAttacking() == true && (status == (int)Status.Statuses.Invincible || HasFreeBullets == true || (healthvalue >= 2 || HealthBars > 0)));
        }

        //Checks for animations
        protected override bool CheckAttacking()
        {
            return (base.CheckAttacking() == true && IsShooting == false && IsTripping == false);
        }

        protected override bool CheckSpecialAttacking()
        {
            return (base.CheckSpecialAttacking() == true && CanJump == true && IsTripping == false);
        }

        protected override bool CheckWalking()
        {
            return (base.CheckWalking() == true && IsShooting == false);
        }

        protected override bool CheckJump()
        {
            return (base.CheckJump() == true && IsTripping == false && IsShooting == false);
        }

        //Checks if the player can pick up or drop an item
        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && IsShooting == false);
        }

        protected override bool CheckSwitchSide(SubLevel level)
        {
            return (base.CheckSwitchSide(level) && IsTripping == false);
        }

        //Checks if the player can throw a weapon
        protected override bool CheckCanThrow()
        {
            return (base.CheckCanThrow() == true && IsShooting == false);
        }
        //End checks

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, float waterheight, Color drawcolor, float Depth)
        {
            Vector2 drawloc = GetDrawLoc(OffSet);

            if (IsShooting == true)
                DefSpecAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsTripping == true)
                TripAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else base.DrawAnimations(spriteBatch, OffSet, waterheight, drawcolor, Depth);
        }

        //Updates Animations
        public override void UpdateAnimations()
        {
            UpdateTrippingAnim();
            UpdateShootAnim();

            base.UpdateAnimations();
        }
    }
}