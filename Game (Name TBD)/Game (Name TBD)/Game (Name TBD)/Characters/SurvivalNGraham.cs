﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A special Graham designed for the Survival - Normal challenge that always loses Oxygen Tank health when hit
    public sealed class SurvivalNGraham : Graham
    {
        public SurvivalNGraham(int health, int healthbars, int playernum, int costume, bool avoidenemy = false) : base(health, healthbars, playernum, costume, avoidenemy)
        {
            
        }

        /*public override void TakeDamage(float activeTime, bool FacingLeft, int damage, Status enemystat, Hitbox enembox, TileEngine TileEngine, List<BeatEmUpObj> Solids)
        {
            base.TakeDamage(activeTime, FacingLeft, damage, enemystat, enembox, TileEngine, Solids);

            //Make the oxygen tank take damage whenever Graham does
            if (OxygenTank != null) OxygenTank.Break(activeTime, 1, enembox, TileEngine);
        }*/
    }
}
