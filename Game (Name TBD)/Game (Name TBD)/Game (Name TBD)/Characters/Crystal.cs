﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The playable character, Crystal
    public class Crystal : Player
    {
        //Crystal's exclusive animations
        private NewAnimation GrabKickAnim;
        private NewAnimation SpinKickAnim;
        private NewAnimation PunchBarrageAnim;

        //Crystal's crouching animations used if she slides and stops under a solid object and her standing height is too high for her to stand up
        private NewAnimation CrouchAnim;
        //private NewAnimation CrouchWalkingAnim;
        //private NewAnimation CrouchHitAnim;

        private bool IsCrouching;

        private bool IsGrabKicking;
        private bool IsSpinKicking;
        private bool IsBarrage;
        private bool IsHBarrage;

        //Make it so Crystal can grab enemies in the air only once each time she jumps (so she can't grab again while jumping down or something)
        private bool AerialGrabbed;

        //Constructor
        public Crystal(int health, int healthbars, int playernum, int costume, bool avoidenemy = false)
        {
            Velocity = new Vector2(4, 2);
            OrigJumpVelocity = new Vector3(Velocity.X, 0f, 4.5f);
            Weight = 145;
            DashAttackSpeedChange = .1f;

            CharNum = (int)Characters.Crystal;

            SpriteSheet = LoadGraphics.CharSheets[(int)Characters.Graham/*CharNum*/][0];

            ChangeCostume(costume);
            //Hurtbox = new Hurtbox(((int)StandingAnim.GetAnimSizeFirst().X / 4), (int)Location.W / 3);

            //Animations
            StandAnim = new NewAnimation(true, true, MaxStandingLoops, SpriteSheet, new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
            RunningAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(444, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(484, 16, 64, 72), 100), new AnimFrame(new Rectangle(551, 15, 66, 73), 125, new Vector2(5, 0)), new AnimFrame(new Rectangle(619, 16, 38, 72), 100, new Vector2(-2, 0)), new AnimFrame(new Rectangle(659, 14, 67, 74), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(728, 17, 67, 71), 125, new Vector2(7, 0)));
            JumpingAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 106, 38, 86), 0, new Vector2(2, 0)), new AnimFrame(new Rectangle(104, 114, 46, 78), 0, new Vector2(6, 0)));
            DefSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(168, 539, 73, 69), 700, new Vector2(-5, 0)));
            OffSpecAnim = new NewAnimation(true, false, 4, SpriteSheet, new AnimFrame(new Rectangle(56, 1526, 63, 66), 40, new Vector2(20, 17)), new AnimFrame(new Rectangle(128, 1525, 46, 67), 40, new Vector2(-3, 16)), new AnimFrame(new Rectangle(184, 1526, 71, 66), 40, new Vector2(-9, 17)), new AnimFrame(new Rectangle(264, 1525, 48, 67), 40, new Vector2(3, 16)));
            GrabAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 113, 54, 79), 300));
            LandAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 131, 40, 61), 70, new Vector2(-3, 0)));
            DashAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 100, new Vector2(1, 0)), new AnimFrame(new Rectangle(88, 560, 73, 48), 500, new Vector2(-1, 0)));
            PickUpAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 250));
            DownJumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(360, 424, 43, 64), 300, new Vector2(2, 0)));
            JumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(416, 402, 36, 86), 40, new Vector2(5, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 30, new Vector2(-5, 0)), new AnimFrame(new Rectangle(520, 400, 53, 88), 100, new Vector2(-7, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 150, new Vector2(-5, 0)));
            RunJumpAttackAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 50, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 500, new Vector2(-13, 23)));
            Grabbedanim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 945, 39, 63), 1500, new Vector2(4, -1)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(152, 931, 50, 77), 675, new Vector2(-2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(216, 955, 67, 53), 0, new Vector2(-1, 0)), new AnimFrame(new Rectangle(384, 980, 87, 28), 400, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(384, 980, 87, 28), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(560, 957, 45, 51), 100));
            ForwardKnockedDownAnim = new NewAnimation(KnockedDownAnim, 1f);
            SwitchSidesAnim = new NewAnimation(false, true, 0, SpriteSheet, new AnimFrame(new Rectangle(320, 109, 56, 83), 100, new Vector2(-15, 11)), new AnimFrame(new Rectangle(384, 115, 61, 77), 100, new Vector2(-10, 63)), new AnimFrame(new Rectangle(456, 96, 28, 96), 100, new Vector2(-34, 63)));
            FThrowAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(248, 745, 53, 63), 150, new Vector2(9, 0)), new AnimFrame(new Rectangle(312, 753, 57, 55), 150, new Vector2(12, 0)), new AnimFrame(new Rectangle(377, 775, 68, 33), 150, new Vector2(8, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(128, 398, 41, 90), 200, new Vector2(8, 0)), new AnimFrame(new Rectangle(176, 409, 48, 79), 200, new Vector2(-1, 0)), new AnimFrame(new Rectangle(232, 418, 53, 70), 200, new Vector2(-11, 0)));
            StabAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 411, 36, 77), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(56, 413, 58, 75), 100, new Vector2(-13, 0)));

            AttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(80, 203, 40, 77), 50, new Vector2(-4, 0)), new AnimFrame(new Rectangle(128, 205, 67, 75), 150, new Vector2(-14, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(208, 201, 39, 79), 50, new Vector2(1, 0)), new AnimFrame(new Rectangle(256, 204, 43, 76), 50, new Vector2(4, 0)), new AnimFrame(new Rectangle(312, 212, 81, 68), 200, new Vector2(-10, 0))) };

            GrabAttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))), 
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))), 
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2)), new AnimFrame(new Rectangle(600, 205, 46, 75), 200, new Vector2(-19, 1)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2))) };
            VictoriousAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(610, 406, 39, 80), 150), new AnimFrame(new Rectangle(653, 406, 42, 80), 150));
            CrouchAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 131, 40, 61), 70, new Vector2(-3, 0)));
            PunchBarrageAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 106, 38, 86), 0, new Vector2(2, 0)));
            GrabKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 300, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 300, new Vector2(-13, 23)));
            SpinKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 300, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 300, new Vector2(-13, 23)));

            CurKnockDownAnim = KnockedDownAnim;

            PlayerNum = playernum;

            IsCrouching = false;

            IsGrabKicking = false;
            IsSpinKicking = false;
            AerialGrabbed = false;
            IsBarrage = false;
            IsHBarrage = false;

            Name = "Crystal";

            ObjectHeight = 74;

            SetUpHealth(health);
            SetUpHurtbox();

            Lives = 5;
            Damage = 6;
            Defense = 2;
            HitStun = 675;
            hud = new PlayerHUD(this);
            //hud = LoadGraphics.CreatePlayerHud(PlayerNum, LoadGraphics.CrystalIcon, avoidenemy);
        }

        //Resets actions - for when you get hit, knocked down, die, or complete a level
        protected override void ResetActions()
        {
            base.ResetActions();

            IsGrabKicking = false;
            IsSpinKicking = false;
            IsBarrage = false;
            IsHBarrage = false;
            AerialGrabbed = false;
            //if (IsCrouching == false) Location.W = 70;
        }

        protected override bool OffSpecInput()
        {
            return (PressedAttack(true) == true && PressedSpecAttack() == true);
        }

        //Take damage after hitting something or multiple things with a special attack - doesn't apply when you're invincible
        public override void SpecialAttackHit(float activeTime, int index)
        {
            //If Crystal used the special attack when she wasn't invincible, make it subtract health if it hits
            if (SpecialAtkInvincible == false && SpecialAtkHit == false && (IsSpecialAttacking == true || IsHBarrage == true))
            {
                SpecialAtkHit = true;
                if (TempBar <= 0f)
                {
                    hud.SetDamageIndicator(activeTime, Health, HealthBars, 10);
                    Health -= 10;
                }
                else
                {
                    //if (ShouldSetDamageIndicator(10) == true) hud.SetDamageIndicator(activeTime, TempBar, HealthBars, 10);
                    TempBar -= 10;
                }

                //HealthUpdate(activeTime);
            }
        }

        protected override void PlayerAttack(float activeTime, SubLevel level)
        {
            if (Controlled == true && IsAttacking == false && JumpAttacking == false && DownJumpAttacking == false && GrabAttacking == false && IsGrabKicking == false && IsSpinKicking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsSwinging == false && IsStabbing == false)
            {
                if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.Attack]) == true && CheckAttacking() == true)
                {
                    //Grabbing moves
                    if (IsGrabbing == true)
                    {
                        //Make sure you can't attack with a grab attack if the enemy you're trying to attack isn't grabbed and you're still grabbing (Ex. other player hits enemy out of your grab)
                        if (Enem.gGrabbox.GSIsGrabbed == true)
                        {
                            //Throw enemy backwards - press the opposite direction you're facing followed by X
                            if (Keyboard.GetState().IsKeyDown(Backwards))
                            {
                                if (Location.Z == ObjectTile.Z)
                                {
                                    FThrowAnim.Reset();
                                    IsBackwardThrowing = true;
                                    Enem.GetGrabbed(this);
                                    LoadSounds.Play(LoadSounds.CThrow);

                                    //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                    if (Enem != null)
                                    {
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                    }
                                }
                            }
                            //Throw enemy forwards - press the same direction you're facing followed by X
                            //Or, do a spin kick if Crystal is grabbing an enemy with her aerial grab
                            else if (Keyboard.GetState().IsKeyDown(Forwards))
                            {
                                if (IsOnObject == true || Location.Z == ObjectTile.Z)
                                {
                                    FThrowAnim.Reset();
                                    IsForwardThrowing = true;
                                    Enem.GetGrabbed(this);
                                    LoadSounds.Play(LoadSounds.CThrow);

                                    //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                    if (Enem != null)
                                    {
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                    }
                                }
                                //Spin Kick
                                else if (status != (int)Status.Statuses.NoAttack)
                                {
                                    CurDamagingAnim = SpinKickAnim;
                                    SpinKickAnim.Reset();
                                    IsSpinKicking = true;
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 5, SpinKickAnim.GetFrameLength(0) / 2, SpinKickAnim.GetFrameLength(0) / 2, false, FacingRight, LoadSounds.Punch1, 40));
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 5, SpinKickAnim.GetFrameLength(0) * 1.5f, SpinKickAnim.GetFrameLength(0) / 3, true, FacingRight, LoadSounds.Kick, 40));
                                }
                            }
                            //Grab Attack/Back grapple attack
                            else
                            {
                                if (IsOnObject == true || Location.Z == ObjectTile.Z)
                                {
                                    //If you have the NoAttack status you can't do grab attacks
                                    if (status != (int)Status.Statuses.NoAttack && FacingRight != Enem.FacingRight)
                                    {
                                        CurDamagingAnim = CurGrabAttackAnim;
                                        CurGrabAttackAnim.Reset();
                                        GrabAttacking = true;

                                        //if (GrabAttackNum == (GrabAttackingAnim.Count - 1))
                                        //    Hitboxes.Add(new Hitbox(GetHitboxXValue(30), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, 0, CurGrabAttackAnim.GetCurFrameLength(), true, FacingRight, LoadSounds.Kick, 30));
                                        //else Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 2, 0, CurGrabAttackAnim.GetCurFrameLength(), false, FacingRight, LoadSounds.Punch1, 30));
                                    }
                                    else if (FacingRight == !Enem.FacingRight)
                                    {
                                        //Back grapple attack - punch barrage with 7 total hitboxes, the last being a kick that knocks down
                                        CurDamagingAnim = PunchBarrageAnim;
                                        IsBarrage = true;
                                        PunchBarrageAnim.Reset();

                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 200, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 300, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 400, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 500, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 600, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 1, 700, 1, false, FacingRight, LoadSounds.Punch1, 10));
                                        //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, 800, 4, true, FacingRight, LoadSounds.Kick, 70));
                                    }
                                }
                                //Crystal's kick across face, kick across chest attack
                                else if (status != (int)Status.Statuses.NoAttack)
                                {
                                    CurDamagingAnim = GrabKickAnim;
                                    GrabKickAnim.Reset();
                                    IsGrabKicking = true;
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 6, GrabKickAnim.GetFrameLength(0) / 3, GrabKickAnim.GetFrameLength(0) / 3, false, FacingRight, LoadSounds.Punch1, 60));
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 7, (int)(GrabKickAnim.GetFrameLength(0) * (5f/3f)), GrabKickAnim.GetFrameLength(0) / 3, true, FacingRight, LoadSounds.Kick, 60));
                                }
                            }
                        }
                    }
                    //Jump attacks
                    else if (CanJump == false)
                    {
                        //If you have the NoJump status then you can't do jump attacks (you can fall off higher ground or objects to get into the air)
                        if (status != (int)Status.Statuses.NoJump)
                        {
                            LoadSounds.Play(LoadSounds.CThrow);

                            //Down jump attack (aerial grab)
                            if (DownJumpAttacking == false && AerialGrabbed == false && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]))
                            {
                                CurDamagingAnim = DownJumpAtkAnim;
                                DownJumpAtkAnim.Reset();
                                DownJumpAttacking = true;

                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 4, 0, DownJumpAtkAnim.FullDuration(), false, FacingRight, LoadSounds.Punch1, 40));
                                CreateThrowHitbox(true, 20, 30, 4, 10, 0f, DownJumpAtkAnim.FullDuration(), FacingRight, 0, AerialGrab, true);
                            }
                            //Standing jump attack
                            else if (AirVelocity.X == 0f)
                            {
                                CurDamagingAnim = JumpAtkAnim;
                                JumpAtkAnim.Reset();
                                JumpAttacking = true;
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 6, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame - 1, JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame - 1), true, FacingRight, LoadSounds.Kick, 40));
                            }
                            //Running jump attack
                            else
                            {
                                //Crystal does a double kick forwards
                                CurDamagingAnim = RunJumpAttackAnim;
                                RunJumpAttackAnim.Reset();
                                JumpAttacking = true;
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(60), FeetLoc.Y - 10, 60, 20, CurHeight + 10, 10, 3, RunJumpAttackAnim.GetFrameLength(0), RunJumpAttackAnim.GetFrameLength(0), false, FacingRight, LoadSounds.Punch1, 40));
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(60), FeetLoc.Y - 10, 60, 20, CurHeight + 10, 10, 5, RunJumpAttackAnim.GetFrameLength(0) * 3, RunJumpAttackAnim.GetFrameLength(RunJumpAttackAnim.MaxFrame - 1), true, FacingRight, LoadSounds.Kick, 40));
                            }
                        }
                    }
                    //Standing attack and weapon attacks
                    else
                    {
                        //If you have the NoAttack status then you can't do standard or weapon attacks
                        if (status != (int)Status.Statuses.NoAttack)
                        {
                            //If you have a weapon and it's swingable or stabbable, use the weapon
                            if (weapon != null && weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                            {
                                //If your weapon is swingable, swing the weapon
                                if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                                {
                                    CurDamagingAnim = SwingAnim;
                                    SwingAnim.Reset();
                                    IsSwinging = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), SwingAnim.GetFrameLength(0), SwingAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                                //If your weapon stabs, stab with the weapon
                                else if (weapon != null && weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                                {
                                    CurDamagingAnim = StabAnim;
                                    StabAnim.Reset();
                                    IsStabbing = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 4, StabAnim.GetFrameLength(0), StabAnim.GetFrameLength(StabAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                            }
                            //Do a crouch attack if you're crouching
                            else if (IsCrouching == true)
                            {
                                //CurDamagingAnim = CrouchAttackAnim;
                                //CrouchAttackAnim.Reset(activeTime);
                                LoadSounds.Play(LoadSounds.Attack);
                                IsAttacking = true;
                                IsWalking = false;
                                IsRunning = false;

                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(45), FeetLoc.Y - 15, 45, 30, CurHeight + 10, 10, 3, (int)(CrouchAnim.GetFrameLength(0) * 1.5f), (int)(CrouchAnim.GetFrameLength(0) * 1.5f), true, FacingRight, LoadSounds.Kick, 60));
                            }
                            //Do a dash attack if you're running
                            else if (IsRunning == true)
                            {
                                CurDamagingAnim = DashAtkAnim;
                                LoadSounds.Play(LoadSounds.Sweep);
                                DashAtkAnim.Reset();
                                IsAttacking = true;
                                DashAttackSpeed = KnockDownVelocity + 1f;
                                //Location.W = Location.W / 2;

                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(40), FeetLoc.Y - 10, 40, 20, Location.Z + 10f, 10, 3, DashAtkAnim.GetFrameLength(0), (int)(DashAtkAnim.FullDurationExcluding(0) * .7f), true, FacingRight, LoadSounds.Kick, 60));
                            }
                            //If you don't have a weapon or it's not swingable, do a normal attack
                            else
                            {
                                CheckComboTimeFrame();

                                CurDamagingAnim = CurAttackAnim;
                                CurAttackAnim.Reset();
                                LoadSounds.Play(LoadSounds.Attack);
                                IsAttacking = true;
                                IsWalking = false;
                                IsRunning = false;

                                //Allow Crystal to turn around when starting an attack
                                if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Left])) FacingRight = false;
                                else if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Right])) FacingRight = true;

                                //Hitboxes.Add(new Hitbox(AttackNum == (AttackingAnim.Count - 1) ? GetHitboxXValue(40) : GetHitboxXValue(30), FeetLoc.Y - 10, AttackNum == (AttackingAnim.Count - 1) ? 40 : 30, 20, CurHeight + 50, 50, ((AttackNum / 3) * 2), 0, CurAttackAnim.GetCurFrameLength(), AttackNum == (AttackingAnim.Count - 1), FacingRight, AttackNum == (AttackingAnim.Count - 1) ? LoadSounds.Kick : LoadSounds.Punch1, (AttackNum + 1) * 10));
                            }
                        }
                    }
                }
            }
        }

        //The player's special attack - can be used in hitstun and while grabbing an enemy
        protected override void PlayerSpecialAttack()
        {
            if (IsSpecialAttacking == false && IsHBarrage == false)
            {
                if (Controlled == true && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.SpecAttack]) == true)
                {
                    if (CheckCanUseSpecialAttack() == true)
                    {
                        //Crystal's Offensive special (hold Attack then press Special Attack) - Handstand Barrage
                        if (IsHit == false && IsGrabbing == false && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Attack]) == true)
                        {
                            CurDamagingAnim = OffSpecAnim;
                            LoadSounds.Play(LoadSounds.CSpcAtk);
                            OffSpecAnim.Reset();
                            IsHBarrage = true;
                            IsWalking = false;
                            IsRunning = false;
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            float activation = OffSpecAnim.FullDuration();
                            float duration = OffSpecAnim.GetFrameLength(0);

                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 30, 2, 150, 100, false, FacingRight, LoadSounds.Punch1, 40));
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 30, 2, 250, 100, false, FacingRight, LoadSounds.Punch1, 40));
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 30, 2, 350, 100, false, FacingRight, LoadSounds.Punch1, 40));
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 30, 2, 450, 100, false, FacingRight, LoadSounds.Punch1, 40));
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 30, 4, 550, 100, true, FacingRight, LoadSounds.Punch1, 50));
                        }
                        //Crystal's Defensive special
                        else
                        {
                            CurDamagingAnim = DefSpecAnim;
                            LoadSounds.Play(LoadSounds.CSpcAtk);
                            DefSpecAnim.Reset();
                            IsSpecialAttacking = true;
                            IsWalking = false;
                            IsRunning = false;
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            //Reset the grab attack counter if you used a special attack while grabbing
                            GrabAttacking = false;
                            ComboCounter = 0;
                            if (Enem != null)
                                Enem.GrabRecover();
                            IsHit = false;
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, CurHeight + 50, 50, 4, 0, DefSpecAnim.FullDuration(), true, FacingRight, LoadSounds.Kick, 60));
                        }
                    }
                }
            }
        }

        protected override void PlayerJump(float activeTime, SubLevel level)
        {
            if (CanJump == true)
            {
                if (Controlled == true && status != (int)Status.Statuses.NoJump && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.Jump]) == true)
                {
                    if (CheckJump() == true)
                    {
                        //Jump from an aerial grab and hurt the enemy
                        if (AerialGrabbed == true)//IsGrabbing == true && IsOnObject == false && Location.Z > PlayerTile.Z)
                        {
                            CanJump = false;
                            AirVelocity.X = 0f;
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z, 0, 4, 0, 1, true, FacingRight, LoadSounds.Kick, 30));
                            JumpingAnim.Reset();
                            Location.Z = (int)Location.Z;
                            return;
                        }

                        LandAnim.Reset();

                        IsJumpLand = true;
                        IsWalking = false;
                        CanJump = false;

                        //If you're grabbing an enemy but not doing anything with the enemy, you can jump out of the grab
                        if (Enem != null)
                            Enem.GrabRecover();
                    }
                }
            }
            else if (IsJumpLand == false)
            {
                //Allow the player to control the height of Crystal's jump depending on how long the jump key is held
                if (Controlled == true && AirVelocity.Z > 1.4f && AirVelocity.Z < 2.8f && Keyboard.GetState().IsKeyUp(Controls[PlayerNum][(int)Action.Jump]))
                    AirVelocity.Z = 1.4f;

                ObjectMove(new Vector2(AirVelocity.X, 0));
            }
        }

        //Make the player grab an enemy
        public override void GrabEnemy(Enemy enem, SubLevel level)
        {
            base.GrabEnemy(enem, level);

            //Aerial grab - Move Crystal to the enemy's location; The height she can grab is dependent on the height of the hitbox, so don't make it too large
            if (Location.Z > Enem.GetLocationHeight.Z)
            {
                AirVelocity = new Vector3(0f, 0f, .6f);
                AerialGrabbed = true;

                Vector2 movevel = new Vector2(FacingRight == true ? Enem.FeetLoc.X - FeetLoc.Right : Enem.FeetLoc.Right - FeetLoc.X, Enem.FeetLoc.Y - FeetLoc.Y);

                ObjectMove(movevel);

                Location.Z = Enem.CurHeight + 38f;
                //float NewLoc = FacingRight == true ? Enem.gLocation.X - 5 : Enem.gLocation.X + 5;
                //
                //Location.Y = enem.gLocation.Y;
                //Location.Z = enem.gLocation.Z + 38f;
                //
                ////Check if Crystal is stuck in a wall or item container
                //if (Movement.CanMoveTo(Location, new Vector4(NewLoc, Location.Y, Location.Z, Location.W), GCollisionBox, TileEngine, Solids) == false)
                //    NewLoc = enem.gLocation.X;
                //
                //if (TileEngine.TileXExists(new Rectangle((int)NewLoc, FeetLoc().Y, FeetLoc().Width, FeetLoc().Height), Vector2.Zero) == false)
                //    NewLoc = Location.X;
                //
                //Location.X = NewLoc;
            }
        }

        //Checks what the player is holding
        protected override void CheckHolds(float activeTime, SubLevel level)
        {
            //Check if player is grabbing an enemy
            if (Enem != null)
            {
                if (IsDead == true || IsGrabbed == true)
                    Enem.GrabRecover();

                //Check if the player wants to release a grabbed enemy
                if (IsBarrage == false) ReleaseEnemy(activeTime);

                if (Enem.gGrabbox.GSIsGrabbed == false)
                {
                    //Make sure the grab attack and throwing animations play out completely
                    if (GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsGrabKicking == false && IsSpinKicking == false)
                    {
                        if (IsKnockedDown == false && AerialGrabbed == true && (Location.Z > ObjectTile.Z && IsOnObject == false))
                        {
                            Location.Z = ObjectTile.Z + 38f;
                            AirVelocity.Z = .6f;
                            CanJump = false;
                            AirVelocity.X = 0f;
                            JumpingAnim.Reset();
                        }

                        IsGrabbing = false;

                        //Don't allow Crystal's punch barrage attack hitboxes to linger if she killed the enemy early
                        if (IsBarrage == true)
                        {
                            IsBarrage = false;
                            Hitboxes.Clear();
                        }

                        //Enem = null;
                        GrabbedFighter = null;
                    }
                }
            }

            //Check if player is holding weapon
            /*if (weapon != null)
            {
                if (IsDead() == true)
                    weapon.WeaponDrop(true);

                if (weapon.GSIsPickedUp == false)
                {
                    weapon = null;
                }
            }*/
        }

        //Animation updates
        protected override void UpdateSpecialAttackAnim()
        {
            if (IsHBarrage == true)
            {
                OffSpecAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();
                if (OffSpecAnim.IsAnimationEnd() == true)
                {
                    IsHBarrage = false;
                    SpecialAtkHit = false;
                }
            }
            else base.UpdateSpecialAttackAnim();
        }

        private void UpdateGrabKickAnim()
        {
            if (IsGrabKicking == true)
            {
                GrabKickAnim.Update(Main.GetActiveTime, GravityValue);

                if (GrabKickAnim.IsAnimationEnd() == true)
                {
                    IsGrabKicking = false;
                    CanJump = false;
                }
                ResetStandingAnim();
            }
        }

        private void UpdateSpinKickAnim()
        {
            if (IsSpinKicking == true)
            {
                SpinKickAnim.Update(Main.GetActiveTime, GravityValue);

                if (SpinKickAnim.IsAnimationEnd() == true)
                {
                    IsSpinKicking = false;
                    CanJump = false;
                }
                ResetStandingAnim();
            }
        }

        protected override void UpdateDashAttackAnim()
        {
            if (IsRunning == true && IsAttacking == true)
            {
                DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();

                if (DashAtkAnim.IsAnimationEnd() == true)
                {
                    IsRunning = false;
                    IsAttacking = false;

                    //Make sure Crystal is on a whole number X value
                    Location.X = (int)Location.X;
                }
                //Make Crystal's slide attack move smoothly
                else if (DashAtkAnim.CurrentFrame > 0)
                    DashAttack();
                else Location.X = (int)Location.X;
            }
        }

        private void UpdatePunchBarrageAnim()
        {
            if (IsBarrage == true)
                PunchBarrageAnim.Update(Main.GetActiveTime, GravityValue);
        }
        //End updates

        //Checks for animations
        protected override bool CheckAttacking()
        {
            return (base.CheckAttacking() == true && IsGrabKicking == false && IsSpinKicking == false && IsHBarrage == false && IsBarrage == false);
        }

        protected override bool CheckSpecialAttacking()
        {
            return (base.CheckSpecialAttacking() == true && AerialGrabbed == false && IsCrouching == false && CanJump == true && IsGrabKicking == false && IsSpinKicking == false && IsBarrage == false);
        }

        //Check if the player should run or not; Crystal cannot run if she is crouching
        protected override bool CheckCanRun()
        {
            return (base.CheckCanRun() == true && IsCrouching == false);
        }

        protected override bool CheckWalking()
        {
            return (base.CheckWalking() == true && IsHBarrage == false);
        }

        protected override bool CheckJump()
        {
            return (base.CheckJump() == true && IsHBarrage == false && IsCrouching == false && IsGrabKicking == false && IsSpinKicking == false && IsBarrage == false);
        }

        //Checks if the player can pick up or drop an item
        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && IsHBarrage == false);
        }

        protected override bool CheckSwitchSide(SubLevel level)
        {
            return (base.CheckSwitchSide(level) && (IsOnObject == true || Location.Z == ObjectTile.Z) && IsBarrage == false);
        }

        //Checks if the player can throw a weapon
        protected override bool CheckCanThrow()
        {
            return (base.CheckCanThrow() == true && IsCrouching == false && IsHBarrage == false);
        }

        protected override bool ObjectShouldFall()
        {
            return (base.ObjectShouldFall() == true && AerialGrabbed == false);
        }

        //Crystal's Aerial Grab; if the correct conditions are not met for Crystal to grab, we'll just hurt the victim instead
        private void AerialGrab(BeatEmUpObj victim, int damage, Hitbox hitbox)
        {
            if (Fighter.IsObjectFighter(victim) == true && victim.InScreenBounds(SubLvl.GetCamera) == true)
            {
                GrabFighter((Fighter)victim);

                AirVelocity = new Vector3(0f, 0f, .6f);
                AerialGrabbed = true;
            }
            else
            {
                victim.TakeDamage(this, TotalDamageDealt(hitbox), hitbox);

                //Make this object enter hitlag
                EnterHitLag();
            }
        }
        //End checks

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, float waterheight, Color drawcolor, float Depth)
        {
            Vector2 drawloc = GetDrawLoc(OffSet);

            if (IsCrouching == true && IsKnockedDown == false)
                CrouchAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsGrabKicking == true)
                GrabKickAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsSpinKicking == true)
                SpinKickAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsBarrage == true)
                PunchBarrageAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsHBarrage == true)
                OffSpecAnim.Draw(spriteBatch, drawloc, FacingRight, drawcolor, 0f, Depth);
            else base.DrawAnimations(spriteBatch, OffSet, waterheight, drawcolor, Depth);
        }

        //Updates Animations
        public override void UpdateAnimations()
        {
            UpdatePunchBarrageAnim();
            UpdateGrabKickAnim();
            UpdateSpinKickAnim();

            base.UpdateAnimations();
        }
    }
}
