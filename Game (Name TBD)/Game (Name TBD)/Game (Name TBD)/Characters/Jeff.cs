﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The playable character, Jeff
    public class Jeff : Player
    {
        //Jeff's unique actions and animations
        //private Animation SpecialGrabbingAnim;

        private NewAnimation PickUpEnemyAnim;
        private NewAnimation CarryAnim;
        private NewAnimation CarryWalkingAnim;
        private NewAnimation CarryRunningAnim;
        private NewAnimation StompAnim;

        //Jeff's carrying actions
        private bool IsCarrying;
        private bool IsStomping;
        private bool IsSpecialGrabbing;

        //Constructor
        public Jeff(int health, int healthbars, int playernum, int costume, bool avoidenemy = false)
        {
            Velocity = new Vector2(3, 2);
            OrigJumpVelocity = new Vector3(Velocity.X, 0f, 4f);
            Weight = 167;

            CharNum = (int)Characters.Jeff;

            SpriteSheet = LoadGraphics.CharSheets[(int)Characters.Graham/*CharNum*/][0];
            ChangeCostume(costume);
            //Hurtbox = new Hurtbox(((int)StandingAnim.GetAnimSizeFirst().X / 4), (int)Location.W / 3);

            //Animations
            StandAnim = new NewAnimation(true, true, MaxStandingLoops, SpriteSheet, new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
            RunningAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(200, 8, 29, 80), 75, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 75, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 75, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 75, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 75, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 75, new Vector2(1, 0)));
            JumpingAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(56, 106, 38, 86), 0, new Vector2(2, 0)), new AnimFrame(new Rectangle(104, 114, 46, 78), 0, new Vector2(6, 0)));
            DefSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(168, 539, 73, 69), 700, new Vector2(-5, 0)));
            OffSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 945, 39, 63), 1000/*700*/, new Vector2(4, -1)));
            GrabAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 113, 54, 79), 300));
            LandAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 131, 40, 61), 70, new Vector2(-3, 0)));
            DashAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 150, new Vector2(1, 0)), new AnimFrame(new Rectangle(88, 560, 73, 48), 600, new Vector2(-1, 0)));
            PickUpAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(560, 957, 45, 51), 250));
            DownJumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(360, 424, 43, 64), 300, new Vector2(2, 0)));
            JumpAtkAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(416, 402, 36, 86), 40, new Vector2(5, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 30, new Vector2(-5, 0)), new AnimFrame(new Rectangle(520, 400, 53, 88), 100, new Vector2(-7, 0)), new AnimFrame(new Rectangle(464, 407, 47, 81), 150, new Vector2(-5, 0)));
            RunJumpAttackAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(160, 113, 56, 79), 50, new Vector2(8, 0)), new AnimFrame(new Rectangle(224, 139, 84, 53), 500, new Vector2(-13, 23)));
            Grabbedanim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 945, 39, 63), 1500, new Vector2(4, -1)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(152, 931, 50, 77), 825, new Vector2(-2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(216, 955, 67, 53), 0, new Vector2(-1, 0)), new AnimFrame(new Rectangle(384, 980, 87, 28), 400, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(384, 980, 87, 28), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(480, 968, 70, 40), 150), new AnimFrame(new Rectangle(560, 957, 45, 51), 100));
            ForwardKnockedDownAnim = new NewAnimation(KnockedDownAnim, 1f);
            SwitchSidesAnim = new NewAnimation(false, true, 0, SpriteSheet, new AnimFrame(new Rectangle(320, 109, 56, 83), 100, new Vector2(-15, 11)), new AnimFrame(new Rectangle(384, 115, 61, 77), 100, new Vector2(-10, 63)), new AnimFrame(new Rectangle(456, 96, 28, 96), 100, new Vector2(-34, 63)));
            FThrowAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(248, 745, 53, 63), 150, new Vector2(9, 0)), new AnimFrame(new Rectangle(312, 753, 57, 55), 150, new Vector2(12, 0)), new AnimFrame(new Rectangle(377, 775, 68, 33), 150, new Vector2(8, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(128, 398, 41, 90), 200, new Vector2(8, 0)), new AnimFrame(new Rectangle(176, 409, 48, 79), 200, new Vector2(-1, 0)), new AnimFrame(new Rectangle(232, 418, 53, 70), 200, new Vector2(-11, 0)));
            StabAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 411, 36, 77), 100, new Vector2(-1, 0)), new AnimFrame(new Rectangle(56, 413, 58, 75), 100, new Vector2(-13, 0)));

            AttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 204, 61, 76), 100, new Vector2(-9, 0))), new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(80, 203, 40, 77), 50, new Vector2(-4, 0)), new AnimFrame(new Rectangle(128, 205, 67, 75), 150, new Vector2(-14, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(208, 201, 39, 79), 50, new Vector2(1, 0)), new AnimFrame(new Rectangle(256, 204, 43, 76), 50, new Vector2(4, 0)), new AnimFrame(new Rectangle(312, 212, 81, 68), 200, new Vector2(-10, 0))) };

            GrabAttackingAnim = new List<NewAnimation>() { new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0))),
                                        new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(496, 201, 40, 79), 200, new Vector2(-19, 0)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2)), new AnimFrame(new Rectangle(600, 205, 46, 75), 200, new Vector2(-19, 1)), new AnimFrame(new Rectangle(536, 1115, 36, 77), 50, new Vector2(-16, 2))) };
            VictoriousAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(610, 406, 39, 80), 150), new AnimFrame(new Rectangle(653, 406, 42, 80), 150));
            StompAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(360, 424, 43, 64), 700, new Vector2(2, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            PlayerNum = playernum;

            IsCarrying = false;
            IsStomping = false;
            IsSpecialGrabbing = false;

            Name = "Jeff";

            ObjectHeight = 80;

            SetUpHealth(health);
            SetUpHurtbox();

            Lives = 5;
            Damage = 7;
            Defense = 2;
            HitStun = 825;
            hud = new PlayerHUD(this);
            //hud = LoadGraphics.CreatePlayerHud(PlayerNum, LoadGraphics.JeffIcon, avoidenemy);
        }

        //Resets actions - for when you get hit, knocked down, die, or complete a level
        protected override void ResetActions()
        {
            base.ResetActions();
   
            if (IsCarrying == true) DropEnemy();
            IsStomping = false;
        }

        //Take damage after hitting something or multiple things with a special attack - doesn't apply when you're invincible
        public override void SpecialAttackHit(float activeTime, int index)
        {
            //If Jeff used the special attack when he wasn't invincible, make it subtract health if it hits
            if (SpecialAtkInvincible == false && SpecialAtkHit == false && (IsSpecialAttacking == true || IsSpecialGrabbing == true))
            {
                SpecialAtkHit = true;
                if (TempBar <= 0f)
                {
                    hud.SetDamageIndicator(activeTime, Health, HealthBars, 10);
                    Health -= 10;
                }
                else
                {
                    //if (ShouldSetDamageIndicator(10) == true) hud.SetDamageIndicator(activeTime, TempBar, HealthBars, 10);
                    TempBar -= 10;
                }

                //HealthUpdate(activeTime);
            }
        }

        protected override void PlayerAttack(float activeTime, SubLevel level)
        {
            if (Controlled == true)
            {
                if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.Attack]) == true && CheckAttacking() == true)
                {
                    //Moves Jeff can do when carrying an enemy
                    if (IsCarrying == true)
                    {
                        //Jeff's Stomp attack - pierces defense but takes status into account
                        if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]))
                        {
                            CurDamagingAnim = StompAnim;
                            StompAnim.Reset();
                            IsStomping = true;
                            IsWalking = false;
                            IsRunning = false;

                            //Enem.CarryFollow(activeTime, new Vector2(Location.X, Location.Y), FacingRight, TileEngine);
                            //Enem.Carry(Location.Z, FacingRight);
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 9, /*StompAnim.FullDurationExcluding(StompAnim.MaxFrame)*/150, 1, true, FacingRight, LoadSounds.Kick, 150, true));
                        }
                        //Throw the enemy
                        else
                        {
                            FThrowAnim.Reset();
                            IsForwardThrowing = true;
                            IsWalking = false;
                            IsRunning = false;
                            LoadSounds.Play(LoadSounds.PThrow);

                            //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                            if (Enem != null)
                            {
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 50, 10, 7, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                            }
                        }
                    }
                    //Grabbing moves
                    else if (IsGrabbing == true)
                    {
                        //Make sure you can't attack with a grab attack if the enemy you're trying to attack isn't grabbed and you're still grabbing (Ex. other player hits enemy out of your grab)
                        if (Enem.gGrabbox.GSIsGrabbed == true)
                        {
                            //Throw enemy backwards - press the opposite direction you're facing followed by X
                            if (Keyboard.GetState().IsKeyDown(Backwards))
                            {
                                FThrowAnim.Reset();
                                IsBackwardThrowing = true;
                                Enem.GetGrabbed(this);
                                LoadSounds.Play(LoadSounds.PThrow);

                                //Be sure to make it so enemies can hit each other with certain moves (being thrown, etc.)
                                if (Enem != null)
                                {
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, CurHeight + 10, 10, 8, FThrowAnim.GetFrameLength(0), 1, true, FacingRight, null, 100, true));
                                }
                            }
                            //Carry the enemy above your head if not carrying, or throw otherwise - press the same direction you're facing followed by X
                            else if (Keyboard.GetState().IsKeyDown(Forwards))
                            {
                                if (status != (int)Status.Statuses.NoGrab)
                                {
                                    //PickupAnim.Reset(activeTime);
                                    CarryEnemy(activeTime);
                                }
                            }
                            //Grab Attack/Back grapple attack
                            else
                            {
                                //If you have the NoAttack status you can't do grab attacks
                                if (status != (int)Status.Statuses.NoAttack && FacingRight != Enem.FacingRight)
                                {
                                    CurDamagingAnim = CurGrabAttackAnim;
                                    CurGrabAttackAnim.Reset();
                                    GrabAttacking = true;

                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 10, 10, 5 + (GrabAttackNum * 3), 0, CurGrabAttackAnim.GetCurFrameLength(), GrabAttackNum == (GrabAttackingAnim.Count - 1), FacingRight, GrabAttackNum != (GrabAttackingAnim.Count - 1) ? LoadSounds.Punch1 : LoadSounds.Kick, 40 + (GrabAttackNum * 20)));
                                }
                                else if (FacingRight == !Enem.FacingRight)
                                {
                                    //Back grapple attack
                                    //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 10, 10, 8, 0, 1, true, FacingRight, null, 100, true));
                                }
                            }
                        }
                    }
                    //Jump attacks
                    else if (CanJump == false)
                    {
                        //If you have the NoJump status then you can't do jump attacks (you can fall off higher ground or objects to get into the air)
                        if (status != (int)Status.Statuses.NoJump)
                        {
                            LoadSounds.Play(LoadSounds.PThrow);

                            //Down jump attack
                            if (DownJumpAttacking == false && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Down]))
                            {
                                CurDamagingAnim = DownJumpAtkAnim;
                                DownJumpAtkAnim.Reset();
                                DownJumpAttacking = true;
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 10, 10, 3, 0, DownJumpAtkAnim.FullDuration(), false, FacingRight, LoadSounds.Punch1, 40));
                            }
                            //Standing jump attack
                            else if (AirVelocity.X == 0f)
                            {
                                //One hitbox on each side of Jeff
                                CurDamagingAnim = JumpAtkAnim;
                                JumpAtkAnim.Reset();
                                JumpAttacking = true;
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 10, 10, 3, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame - 1, JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame - 1), true, FacingRight, LoadSounds.Kick, 40));
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 10, 10, 3, JumpAtkAnim.FullDurationExcluding(JumpAtkAnim.MaxFrame - 1, JumpAtkAnim.MaxFrame), JumpAtkAnim.GetFrameLength(JumpAtkAnim.MaxFrame - 1), true, !FacingRight, LoadSounds.Kick, 40));
                            }
                            //Running jump attack
                            else
                            {
                                CurDamagingAnim = RunJumpAttackAnim;
                                RunJumpAttackAnim.Reset();
                                
                                JumpAttacking = true;
                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(60), FeetLoc.Y - 10, 60, 20, Location.Z + 10, 10, 4, RunJumpAttackAnim.FullDurationExcluding(RunJumpAttackAnim.MaxFrame), RunJumpAttackAnim.GetFrameLength(RunJumpAttackAnim.MaxFrame), true, FacingRight, LoadSounds.Kick, 40));
                            }
                        }
                    }
                    //Standing attack and weapon attacks
                    else
                    {
                        //If you have the NoAttack status then you can't do standard or weapon attacks
                        if (status != (int)Status.Statuses.NoAttack)
                        {
                            //If you have a weapon and it's swingable or stabbable, use the weapon
                            if (weapon != null && weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                            {
                                //If your weapon is swingable, swing the weapon
                                if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                                {
                                    CurDamagingAnim = SwingAnim;
                                    SwingAnim.Reset();
                                    IsSwinging = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), SwingAnim.GetFrameLength(0), SwingAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                                //If your weapon stabs, stab with the weapon
                                else if (weapon != null && weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                                {
                                    CurDamagingAnim = StabAnim;
                                    StabAnim.Reset();
                                    IsStabbing = true;
                                    IsWalking = false;
                                    IsRunning = false;

                                    weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 4, StabAnim.GetFrameLength(0), StabAnim.GetFrameLength(StabAnim.MaxFrame), Hitbox.HitboxTypes.KnockDown, FacingRight, weapon.GWeaponHitSound, true);
                                    LoadSounds.Play(weapon.GWeaponUseSound);
                                }
                            }
                            //Do a dash attack if you're running
                            else if (IsRunning == true)
                            {
                                CurDamagingAnim = DashAtkAnim;
                                DashAtkAnim.Reset();
                                IsAttacking = true;

                                //Hitboxes.Add(new Hitbox(GetHitboxXValue(40), FeetLoc.Y - 15, 40, 15, CurHeight + 30, 30, 3, DashAtkAnim.GetFrameLength(0), DashAtkAnim.FullDurationExcluding(0), true, FacingRight, LoadSounds.Kick, 60));
                            }
                            //If you don't have a weapon or it's not swingable, do a normal attack
                            else
                            {
                                CheckComboTimeFrame();

                                CurDamagingAnim = CurAttackAnim;
                                CurAttackAnim.Reset();
                                LoadSounds.Play(LoadSounds.Attack);
                                IsAttacking = true;
                                IsWalking = false;
                                IsRunning = false;

                                //Allow Jeff to turn around when starting an attack
                                if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Left])) FacingRight = false;
                                else if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Right])) FacingRight = true;

                                //Hitboxes.Add(new Hitbox(AttackNum == (AttackingAnim.Count - 1) ? GetHitboxXValue(40) : GetHitboxXValue(30), FeetLoc.Y - 10, AttackNum == (AttackingAnim.Count - 1) ? 40 : 30, 20, CurHeight + 50, 50, ((AttackNum / 2) * 2), 0, CurAttackAnim.GetCurFrameLength(), AttackNum == (AttackingAnim.Count - 1), FacingRight, AttackNum == (AttackingAnim.Count - 1) ? LoadSounds.Kick : LoadSounds.Punch1, (AttackNum + 1) * 10));
                                //if (AttackNum == AttackAnim.MaxFrame)
                                //    Hitbox.Add(new Hitbox(GetHitboxXValue(40), FeetLoc().Y - 15, 20, 30, Location.Z + 10, 10, 0, 0, 200, true, FacingRight, LoadSounds.Kick, 40));
                                //else if (AttackNum == 0) Hitbox.Add(new Hitbox(GetHitboxXValue(30), FeetLoc().Y - 15, 20, 30, Location.Z + 10, 10, 0, 0, 50, false, FacingRight, LoadSounds.Punch1, 20));
                                //else Hitbox.Add(new Hitbox(GetHitboxXValue(30), FeetLoc().Y - 15, 20, 30, Location.Z + 10, 10, ((AttackNum / 2) * 2), 0, 200, false, FacingRight, LoadSounds.Punch1, 20));
                            }
                        }
                    }
                }
            }
        }

        //The player's special attack - can be used in hitstun and while grabbing an enemy
        protected override void PlayerSpecialAttack()
        {
            if (IsSpecialAttacking == false && IsSpecialGrabbing == false)
            {
                if (Controlled == true && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.SpecAttack]) == true)
                {
                    //Jeff's specials - the offensive one is performed while grabbing an enemy and the defensive one can be performed at any other time
                    if (CheckCanUseSpecialAttack() == true)
                    {
                        IsWalking = false;
                        IsRunning = false;

                        if (IsGrabbing == true)
                        {
                            CurDamagingAnim = OffSpecAnim;
                            OffSpecAnim.Reset();
                            IsSpecialGrabbing = true;
                            Enem.GetGrabbed(this);
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            //Reset the grab attack counter
                            GrabAttacking = false;
                            ComboCounter = 0;

                            //Choke the enemy 3 times then throw it
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 20f, 20, 2, 100, 2, false, FacingRight, LoadSounds.Punch1, 70));
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 20f, 20, 2, 200, 2, false, FacingRight, LoadSounds.Punch1, 80));
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 20f, 20, 2, 300, 2, false, FacingRight, LoadSounds.Punch1, 90));
                            //Hitboxes.Add(new Hitbox(GetHitboxXValue(20), FeetLoc.Y - 15, 20, 30, Location.Z + 20f, 20, 8, 600, 5, true, FacingRight, null, 100, true));
                        }
                        else
                        {
                            CurDamagingAnim = DefSpecAnim;
                            LoadSounds.Play(LoadSounds.PSpcAtk);
                            DefSpecAnim.Reset();
                            IsSpecialAttacking = true;
                            SpecialAtkInvincible = status == (int)Status.Statuses.Invincible;

                            IsHit = false;
                            //Hitboxes.Add(new Hitbox((int)Location.X - CollisionBox.Width, CollisionBox.Y, CollisionBox.Width * 2, CollisionBox.Height, Location.Z + 50f, 50, 4, DefSpecAnim.FullDuration() / 2, DefSpecAnim.FullDuration() / 2, true, FacingRight, LoadSounds.Kick, 60));
                        }
                    }
                }
            }
        }

        //Check if the player can pick up an item or weapon
        protected override void CheckPickUp(float activeTime, SubLevel level)
        {
            if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.ItemKey]) == true)
            {
                //If you're grabbing an enemy, switch to the other side of the enemy when you press the Item key
                if (CheckSwitchSide(level) == true && IsCarrying == false && IsStomping == false && IsSpecialAttacking == false && IsSpecialGrabbing == false)
                {
                    SwitchSide();
                }
                //Return to grabbing the enemy if you're carrying it when you press the Item key, and play the picking up animation in reverse
                else if (IsCarrying == true && IsStomping == false && IsForwardThrowing == false)
                {
                    //NOTE: You can currently keep picking up and regrabbing the enemy since GetGrabbed and DropEnemy() resets the enemy's actions! Fix this!
                    //CarryAnim.ResetReverse(activeTime);
                    Enem.GetGrabbed(this);
                    DropEnemy();
                    IsGrabbing = true;
                    IsWalking = false;
                    IsRunning = false;
                    Enem.gGrabbox.Switched();
                }

                //Throw or drop a weapon if the player has one
                if (weapon != null)
                {
                    //If the weapon is held rather than worn, check if you can throw it and check if the player is already holding the attack button; throw it if so
                    if (weapon.GetWeaponType != (int)Weapon.WeaponTypes.None && CheckCanThrow() == true && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Attack]))
                    {
                        //Throw the weapon and make sure you can't get hurt by the same weapon you just threw
                        weapon.Throw();

                        StabAnim.Reset();
                        IsStabbing = true;

                        weapon = null;
                    }
                    //Otherwise, drop the weapon if you can
                    else if (CheckCanPickUp() == true)
                        weapon.Drop();
                }
            }
        }

        //Make Jeff carry the enemy
        private void CarryEnemy(float activeTime)
        {
            //IsGrabbing = false;
            //IsCarrying = true;

            //Make Jeff move slower when carrying an enemy
            //MoveVelocity = new Vector2(MoveVelocity.X - 1f, MoveVelocity.Y - 1f);
            //Enem.Carry(Location.Z + 50f, FacingRight);
        }

        //Make Jeff uncarry the enemy (I don't wan't to use the word drop because that will confuse it with the enemy's Drop method)
        private void DropEnemy()
        {
            //IsCarrying = false;
            //MoveVelocity = new Vector2(MoveVelocity.X + 1f, MoveVelocity.Y + 1f);

            //if (Enem != null && Enem.gsCarried == true)
                //Enem.Drop();
        }

        //Checks what the player is holding
        protected override void CheckHolds(float activeTime, SubLevel level)
        {
            //Check if player is grabbing an enemy
            if (Enem != null)
            {
                if (IsDead == true || IsGrabbed == true)
                {
                    Enem.GrabRecover();
                    Enem.gsCarried = false;
                }

                //Check if the player wants to release a grabbed enemy
                if (IsCarrying == false)
                    ReleaseEnemy(activeTime);
                //else if (IsCarrying == true && IsStomping == false && Enem.gsCarried == true) Enem.CarryFollow(activeTime, new Vector2(Location.X, Location.Y), FacingRight, level.GetTileEngine);

                if (Enem.gGrabbox.GSIsGrabbed == false && Enem.gsCarried == false)
                {
                    //Make sure the grab attack and throwing animations play out completely
                    if (GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsStomping == false)
                    {
                        IsGrabbing = false;
                        
                        if (IsCarrying == true)
                            DropEnemy();

                        //This is here in case Jeff isn't finished choking the enemy; we don't want his defensive special attack animation to play nor his hitboxes to linger around
                        if (IsSpecialGrabbing == true)
                        {
                            IsSpecialGrabbing = false;
                            SpecialAtkHit = false;
                            Hitboxes.Clear();
                        }

                        GrabbedFighter = null;
                    }
                }
            }

            //Check if player is holding weapon
            /*if (weapon != null)
            {
                if (IsDead() == true)
                    weapon.WeaponDrop(true);

                if (weapon.GSIsPickedUp == false)
                {
                    weapon = null;
                }
            }*/
        }

        //Animation updates
        private void UpdateSpecialGrabbingAnim()
        {
            if (IsSpecialGrabbing == true)
            {
                OffSpecAnim.Update(Main.GetActiveTime, GravityValue);
                if (OffSpecAnim.IsAnimationEnd() == true)
                {
                    IsSpecialGrabbing = false;
                    SpecialAtkHit = false;
                }
            }
        }

        private void UpdateStompAnim()
        {
            if (IsStomping == true)
            {
                StompAnim.Update(Main.GetActiveTime, GravityValue);

                if (StompAnim.IsAnimationEnd() == true)
                    IsStomping = false;

                ResetStandingAnim();
            }
        }

        private new void UpdateThrowAnim()
        {
            if (IsBackwardThrowing == true || IsForwardThrowing == true)
            {
                FThrowAnim.Update(Main.GetActiveTime, GravityValue);
                if (FThrowAnim.IsAnimationEnd() == true)
                {
                    //Since Jeff's back throw makes him face the other direction, we need to transition into it smoothly, so we change it after the animation is done
                    if (IsBackwardThrowing == true)
                        FacingRight = !FacingRight;

                    IsBackwardThrowing = false;
                    IsForwardThrowing = false;
                }

                ResetStandingAnim();
            }
        }

        protected override void UpdateDashAtkAnim()
        {
            if (DashAtkAnim.CurrentFrame == 0)
                DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
            else if (DashAtkAnim.CurrentFrame > 0)
            {
                //Make Jeff do a small jump in the air after starting the attack
                if (AirVelocity.Z == 0f)
                    Jump(new Vector3(ForwardVal(TrueVelocity.X), 0f, 2.5f));
                else ObjectMove(GetAirVelocityVec2);
            }
            else Location.X = (int)Location.X;
        }

        protected override void UpdateDashAttackAnim()
        {
            if (IsRunning == true && IsAttacking == true)
            {
                int animnum = DashAtkAnim.CurrentFrame;
                if (DashAtkAnim.CurrentFrame == 0)
                {
                    DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
                }

                ResetStandingAnim();

                //Make Jeff rise in the air a little after starting the attack
                if (AirVelocity.X == 0f && DashAtkAnim.CurrentFrame != 0)
                {
                    AirVelocity.X = FacingRight == true ? TrueVelocity.X : -TrueVelocity.X;
                    AirVelocity.Z = 2.5f;
                }
                //Make Jeff's dash attack move smoothly
                else if (DashAtkAnim.CurrentFrame > 0)
                {
                    ObjectMove(new Vector2(AirVelocity.X, 0));
                }
                else Location.X = (int)Location.X;
            }
        }
        //End updates

        //Checks for animations
        protected override bool CheckAttacking()
        {
            return (base.CheckAttacking() == true && IsStomping == false && IsSpecialGrabbing == false);
        }

        protected override bool CheckSpecialAttacking()
        {
            return (base.CheckSpecialAttacking() == true && CanJump == true && IsCarrying == false && IsStomping == false);
        }

        protected override bool CheckWalking()
        {
            return (base.CheckWalking() == true && IsStomping == false && IsSpecialGrabbing == false);
        }

        //NOTE: Maybe Jeff should be able to jump when carrying an enemy (he just doesn't jump as high) since he can already run when doing so
        protected override bool CheckJump()
        {
            return (base.CheckJump() == true && IsCarrying == false && IsStomping == false && IsSpecialGrabbing == false);
        }

        //Checks if the player can pick up or drop an item
        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && IsForwardThrowing == false && IsBackwardThrowing == false && GrabAttacking == false && IsCarrying == false && IsStomping == false && IsSpecialGrabbing == false);
        }

        //Checks if the player can throw a weapon
        protected override bool CheckCanThrow()
        {
            return (base.CheckCanThrow() == true && IsCarrying == false && IsStomping == false && IsSpecialGrabbing == false);
        }
        //End checks

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, float waterheight, Color drawcolor, float Depth)
        {
            if (IsStomping == true)
                StompAnim.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), FacingRight, drawcolor, 0f, Depth);
            else if (IsSpecialGrabbing == true)
                OffSpecAnim.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), FacingRight, drawcolor, 0f, Depth);
            //else if (IsCarrying == true && IsWalking == true)
                //CarryWalkingAnim.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), FacingRight, drawcolor, Depth);
            //else if (IsCarrying == true)
                //CarryAnim.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), FacingRight, drawcolor, 0f, waterheight, Depth);
            else base.DrawAnimations(spriteBatch, OffSet, waterheight, drawcolor, Depth);
        }

        //Updates Animations
        public override void UpdateAnimations()
        {
            UpdateSpecialGrabbingAnim();
            UpdateStompAnim();

            base.UpdateAnimations();
        }
    }
}
