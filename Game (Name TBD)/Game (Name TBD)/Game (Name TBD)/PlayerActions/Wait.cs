﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Makes the player wait a certain amount of time
    public sealed class Wait : PAction
    {
        //The amount of time to wait
        private float TimeWait;
        private float PrevWait;

        public Wait(Player play, float timewait) : base(play)
        {
            ActionType = PActions.Idle;

            TimeWait = timewait;
            PrevWait = Main.GetActiveTime + TimeWait;
        }

        public override void Update(SubLevel sublevel)
        {
            //Simply do nothing for the specified amount of time
            if (Main.GetActiveTime >= PrevWait) Complete = true;
        }
    }
}
