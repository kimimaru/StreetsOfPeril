﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Makes the player walk to a location
    public sealed class WalkTo : PAction
    {
        private Vector2 Direction;

        private Vector2 Destination;

        public WalkTo(Player play, Vector2 direction, Vector2 destination) : base(play)
        {
            Direction = direction;
            Destination = destination;

            ActionType = PActions.Moving;
        }

        public override void Update(SubLevel sublevel)
        {
            player.PureMove(Direction.X * player.TrueVelocity.X, Direction.Y * player.TrueVelocity.Y, true);

            if (Direction.X != 0)
            {
                float loc = (player.GetLocationHeight.X + sublevel.GCameraOffSet.X) * -Direction.X;
                float dest = Destination.X * -Direction.X;
                if (loc <= dest) Direction.X = 0;
            }

            if (Direction.Y != 0)
            {
                float loc = (player.GetLocationHeight.Y + sublevel.GCameraOffSet.Y) * -Direction.Y;
                float dest = Destination.Y * -Direction.Y;
                if (loc <= dest) Direction.Y = 0;
            }

            if (Direction == Vector2.Zero)
            {
                Complete = true;
            }
        }
    }
}
