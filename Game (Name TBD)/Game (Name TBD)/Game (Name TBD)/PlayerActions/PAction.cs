﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An action for a Player; it is used to make the player do something on its own in cutscenes and Level Intros/Outros
    public abstract class PAction
    {
        public enum PActions
        {
            None, Idle, Moving, Jumping
        };

        //Player reference
        protected Player player;

        //Tells if the action is complete or not
        protected bool Complete;

        //The action type
        protected PActions ActionType;

        private PAction()
        {
            Complete = false;

            ActionType = PActions.None;
        }

        protected PAction(Player play) : this()
        {
            player = play;
        }

        public bool Completed
        {
            get { return Complete; }
        }

        public PActions GetActionType
        {
            get { return ActionType; }
        }

        //Make the player do something
        public abstract void Update(SubLevel sublevel);
    }
}
