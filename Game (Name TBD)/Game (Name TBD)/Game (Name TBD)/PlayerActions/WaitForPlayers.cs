﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Makes the player wait for the other players to finish whatever actions they're doing, which will eventually lead into this one
    public sealed class WaitForPlayers : PAction
    {
        public WaitForPlayers(Player play) : base(play)
        {
            ActionType = PActions.Idle;
        }

        public override void Update(SubLevel sublevel)
        {
            bool Done = true;

            //Check if every player is waiting for the other to finish their actions
            for (int i = 0; i < sublevel.GetPlayers.Count; i++)
            {
                if (sublevel.GetPlayers[i].CurAction.GetActionType != PActions.Idle)
                {
                    Done = false;
                    break;
                }
            }

            if (Done == true) Complete = true;
        }
    }
}
