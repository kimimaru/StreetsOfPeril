﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A timed bomb that is thrown by enemies (Diver, etc.); upon stopping completely, the bomb will explode after a certain amount of time
    //(SCRAPPED) Certain types of timed bombs may explode on contact, in which case the hitbox must be the same as the resulting explosion's
    public sealed class TimedBomb : Projectile
    {
        //A timer for the timed bomb blowing up
        private float ExplosionTime;
        private float PrevExplode;

        //Checks if the time bomb blew up or not
        private bool Exploded;

        public TimedBomb(Vector3 velocity, Vector3 location, Status stat, float explosiontime, int bombstrength, TileEngine TileEngine, bool contact, int score = 0)
        {
            //ProjectileSprite = new Animation(LoadGraphics.VersusSwitchIcon, false, 1, 0);
            SetLocation(location);
            SetAirVelocity(velocity);
            //Hurtbox = new Hurtbox((int)ProjectileSprite.GetFrameSize().X, (int)ProjectileSprite.GetFrameSize().Y);
            //Hitbox = new Hitbox(0, 0, 0, 0, -1000f, 0, bombstrength, 0f, 0f, true, true, null);

            status = stat;
            Contact = contact;

            FacingRight = true;

            ExplosionTime = explosiontime;
            PrevExplode = 0f;

            Exploded = false;
        }

        public override bool IsDead
        {
            get { return Exploded; }
        }

        //Tells the projectile to do something if it hits something else or gets hit
        //public override void Intersect()
        //{
        //    if (Contact == true && Exploded == false) Explode();
        //}

        //Checks if the timed bomb should explode
        private bool ShouldExplode()
        {
            return (IsInAir == false && AirVelocity.Y == 0f && (Main.GetActiveTime - PrevExplode) >= ExplosionTime);
        }

        //Called when the timed bomb blows up
        private void Explode(List<Hazard> HazardList)
        {
            //HazardList.Add(new Explosion(new Animation(LoadGraphics.Explosion, false, 1, 1000f), new Vector3(Location.X, Location.Y, Location.Z), new Hitbox((int)Location.X, (int)Location.Y, Hurtbox.Width, Hurtbox.Height, MaxHeight, ObjectHeight, Hitbox.Strength, 0, 400, true, true, null), new Status(), ObjectTile));
            Exploded = true;
        }

        //The timed bomb will bounce on the floor after landing
        //protected override void ProjectileLand()
        //{
        //    ShootVelocity.X = (int)(ShootVelocity.X / 2);
        //    ShootVelocity.Y = -(int)(ShootVelocity.Y / 2);
        //
        //    //Start the exploding timer when the bomb is about to explode
        //    if (ShootVelocity.Y == 0f)
        //    {
        //        ShootVelocity.X = 0f;
        //        PrevExplode = Main.GetActiveTime;
        //    }
        //}

        //public override void Update(float activeTime, SubLevel level, bool wallcheck = true)
        //{
        //    //Move(activeTime, ShootVelocity, true, level.GTileEngine, level.GetSolids, wallcheck);
        //
        //    status.ObjectStatUpdate();
        //
        //    //If the bomb is on the floor, isn't bouncing anymore, and enough time passed for it to explode, make the bomb explode
        //    if (ShouldExplode() == true)
        //    {
        //        Explode(level.GetHazards);
        //    }
        //}

        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    //Make the bomb blink red (or maybe white instead?) when it's about to explode
        //
        //    base.Draw(spriteBatch, OffSet, TileEngine);
        //}
    }
}
