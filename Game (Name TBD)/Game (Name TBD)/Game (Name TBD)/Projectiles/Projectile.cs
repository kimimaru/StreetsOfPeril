﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Base projectile class - inheritance may be used to make projectiles hittable, do special things, move in different ways, and more
    //Projectiles are shot by BeatEmUpObjs
    public class Projectile : Hazard
    {
        //The object that shot this projectile
        protected BeatEmUpObj ObjShot;

        //The Projectile's sprite
        protected AnimFrame ProjectileSprite;

        //Determines if the projectile disappears after hitting something
        protected bool Contact;

        //Determines if the projectile is Dead or not; this is set to true when it hits something, if Contact is true
        protected bool Dead;

        protected Projectile()
        {
            GravityValue = TileEngine.Gravity;

            //Standard projectiles just fly in a certain direction forever
            UseGravity = false;

            ObjType = ObjectType.Projectile;
            StatForever = true;

            ObjShot = null;

            //Projectiles do not have any health (at least the ones we have as of now)
            SetUpHealth(0);

            Icon = LoadGraphics.WilIcon;

            Dead = false;

            hud = new HUD(this, new Vector2(0, 70));
        }

        protected Projectile(BeatEmUpObj objshot, Texture2D Sprites, Vector3 velocity, Vector3 location) : this()
        {
            ObjShot = objshot;
            SetAirVelocity(velocity);
            SetLocation(location);

            SpriteSheet = Sprites;
            ProjectileSprite = new AnimFrame(new Rectangle(0, 0, SpriteSheet.Width, SpriteSheet.Height), 0f);

            ObjectLength = Sprites.Width;
            ObjectHeight = Sprites.Height;

            SetUpHurtbox();

            //If the Projectile is moving left, flip the direction it's facing
            if (AirVelocity.X < 0) FlipDirection();

            //Get the rotation the initial rotation of the object
            GetInitRotation();

            //Projectiles take the HUD of their owners since we'll want to know the object that shot it
            //We may or may not want to override this behavior in the future, so keep it for now
            hud = ObjShot.hud;
        }

        public Projectile(BeatEmUpObj objshot, Texture2D Sprites, Vector3 velocity, Vector3 location, Status stat, int hitboxstrength, bool knockdown, int score = 0) : this(objshot, Sprites, velocity, location)
        {
            CreateHitboxSurrounding(hitboxstrength, Hurtbox.Height, 0f, Hitbox.InfiniteHitbox, knockdown == true ? Hitbox.HitboxTypes.KnockDown : Hitbox.HitboxTypes.Standard, !FacingRight, LoadSounds.Kick, true);

            status = stat;
            Contact = true;

            //If the projectile has a set rotation, use that; otherwise, find the rotation based on the direction it's traveling
            //if (rotation != null) Rotation = (float)rotation;
            GetInitRotation();//ProjectileRotation(Velocity);
        }

        //When calling Die(), we will set Dead to true
        //Die() is called if the Projectile goes off screen by a certain amount, damages an object, or touches something if Contact is true
        public override bool IsDead
        {
            get { return Dead; }
        }

        //Gets the Projectile's initial rotation based on its speed upon being launched
        //NOTE: This is currently incorrect because of sprite flipping based on the direction the Projectile is facing
        //This method covers mostly all the rotations, but some are messed up because the sprite is flipped
        protected void GetInitRotation()
        {
            //If the Projectile is moving in the Y, rotate as if it was moving upwards
            if (AirVelocity.Y != 0f)
            {
                //Rotate the Projectile 90 degrees
                Rotate(90);
        
                //Depending on whether the Projectile is moving right or left, rotate the Projectile 45 degrees so it faces the direction it's moving
                if (AirVelocity.X > 0f) Rotate(45);
                else if (AirVelocity.X < 0f) Rotate(-45);
        
                //If the Projectile is moving upwards, rotate it 180 degrees
                if (AirVelocity.Y < 0f) Rotate(180);
            }
        }

        //Returns the rotation of a projectile based on the direction it's moving in
        public static float ProjectileRotation(Vector2 ProjectileSpeed)
        {
            float rotation = 0f;

            //If the projectile is moving downwards, its rotation is at least 90 degrees
            if (ProjectileSpeed.Y > 0f)
            {
                rotation = MathHelper.PiOver2;

                //If the projectile is moving to the right, it should be rotated down-right
                if (ProjectileSpeed.X > 0f) rotation = (MathHelper.PiOver4);
                //If the projectile is moving to the left, it should be rotated down-left
                else if (ProjectileSpeed.X < 0f) rotation = -(MathHelper.PiOver4);
            }
            //If the projectile is moving upwards, its rotation is at least -90 degrees
            else if (ProjectileSpeed.Y < 0f)
            {
                rotation = -MathHelper.PiOver2;

                //If the projectile is moving to the right, it should be rotated up-right
                if (ProjectileSpeed.X > 0f) rotation = -(MathHelper.PiOver4);
                //If the projectile is moving to the left, it should be rotated up-left
                else if (ProjectileSpeed.X < 0f) rotation = (MathHelper.PiOver4);
            }

            return rotation;
        }

        //Tells the projectile to do something if it hits something else or gets hit
        public virtual void Intersect()
        {
            if (Contact == true)
            {
                Dead = true;
                ClearHitboxes();
            }
        }

        //Assume all Projectiles are invincible; we can override this for a particular projectile if needed
        protected override bool IsInvincible
        {
            get { return true; }
        }

        public override void Die()
        {
            base.Die();
            
            //Mention that the Projectile is dead and stop it from drawing
            Dead = true;

            DrawEnabled = false;
        }

        public override bool CanInflictStatus(BeatEmUpObj victim)
        {
            return (base.CanInflictStatus(victim) == true && ObjShot.ObjType != ObjectType.Player);
        }

        public override bool CanBeInflictedStatus(BeatEmUpObj attacker)
        {
            return false;
        }

        public override bool CanDamageObject(BeatEmUpObj victim)
        {
            return (base.CanDamageObject(victim) == true && HasHitboxes == true && victim.ObjType != ObjShot.ObjType && victim.ObjType != ObjectType.Projectile);
        }

        public override void DamageObject(BeatEmUpObj victim, Hitbox hitbox)
        {
            base.DamageObject(victim, hitbox);
            
            //Once the projectile hits something, destroy it
            //NOTE: Same story with Weapons; it'll still hit even when it's dead since we store a reference to the Hitbox when checking for collisions
            Die();
        }
        protected override void LandActions()
        {
            //If the projectile lands on the floor or a solid, destroy it
            Intersect();
        }

        protected override void HitScreenX()
        {
            Intersect();
        }

        protected override void HitScreenY()
        {
            Intersect();
        }

        protected override void HitWallX()
        {
            Intersect();
        }

        protected override void HitWallY()
        {
            Intersect();
        }

        protected override void HitSolidX()
        {
            Intersect();
        }

        protected override void HitSolidY()
        {
            Intersect();
        }

        protected override void ObjectUpdate()
        {
            if (IsDead == false)
            {
                //Make the projectile move
                ObjectMove(TrueVelocityAir);
                
                //If the projectile is offscreen, destroy it and stop it from drawing
                if (IsOffScreenRange(SubLvl.GCameraOffSet, 100, 100) == true)
                {
                    Die();
                    DrawEnabled = false;
                }
            }
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            if (Dead == false)
                ProjectileSprite.Draw(spriteBatch, SpriteSheet, GetDrawLoc(cameraloc), FacingRight, status.GStatusColor, GetRotation, GetDrawDepth(cameraloc));
        }

        protected override bool ShouldDrawShadow()
        {
            return (base.ShouldDrawShadow() == true && Dead == false);
        }
    }
}
