﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //Wil's gun bullets
    public sealed class WBullet : Projectile
    {
        //Wil reference
        private Wil wil;

        //Determines if Wil was invincible or not when shooting the bullet
        public bool ShotState;

        //The number of the bullet; determines if the bullet was in the same batch as others
        public int BulletNum;

        //Constructor; "defensive" means the bullet comes from Wil's defensive special
        public WBullet(Wil wilref, Vector2 velocity, Vector3 location, int bulletnum, bool defensive, int score = 50) : base(wilref, LoadGraphics.projectile, new Vector3(velocity, 0f), location)
        {
            wil = wilref;

            CreateHitboxSurrounding(defensive == true ? Wil.DefensiveBulletStrength : Wil.OffensiveBulletStrength, Hurtbox.Height, 0f, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, !FacingRight, LoadSounds.Kick, true/*score*/);

            Contact = true;

            ShotState = wil.StatusCondition(wil.status == (int)Status.Statuses.Invincible);
            BulletNum = bulletnum;
        }

        public override void DamageObject(BeatEmUpObj victim, Hitbox hitbox)
        {
            base.DamageObject(victim, hitbox);

            //Set Wil's InteractionHUD to the victim's HUD if possible
            if (victim.CanSetHUD() == true)
                wil.SetInteractionHUD(victim.hud);
        }
    }
}
