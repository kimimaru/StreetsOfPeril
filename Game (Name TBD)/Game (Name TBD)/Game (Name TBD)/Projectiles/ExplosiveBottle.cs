﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A bottle that Minecart Riders (maybe other characters too) throw at the player minecart that explodes once it hits the ground
    public sealed class ExplosiveBottle : Projectile
    {
        //The bottle breaking animation that appears when the bottle touches the ground
        //private Animation BreakAnim;

        public ExplosiveBottle(Vector2 velocity, Vector2 location, Status stat, TileEngine TileEngine, bool facingright)
        {
            //ProjectileSprite = new Animation(LoadGraphics.ExplosiveBottle, false, 1, 400);
            //BreakAnim = new Animation(LoadGraphics.charstandinganim, 3, 400);
            Location = new Vector3(location, 50f);
            SetAirVelocity(new Vector3(velocity.X, 0f, velocity.Y));
            //Hurtbox = new Hurtbox((int)ProjectileSprite.GetFrameSize().X, (int)ProjectileSprite.GetFrameSize().Y);
            //Hitbox = Hitbox.Empty;//new Hitbox(0, 0, 0, 0, 0, 0, 1, 0, 0, false, facingright, null);
            ObjectTile = TileEngine.CurTile(CollisionBox);

            status = stat;
            Contact = false;

            Rotation = 0f;
            FacingRight = facingright;
            if (FacingRight == false) AirVelocity.X = -Velocity.X;
        }

        public override bool IsDead
        {
            get { return (AirVelocity.X == 0f); }
        }

        //public override void Update(float activeTime, SubLevel level, bool wallcheck = true)
        //{
        //    if (ShootVelocity.X == 0f)
        //    {
        //        //if (BreakAnim.IsAnimationEnd() == false)
        //            //BreakAnim.Update(activeTime);
        //    }
        //    else
        //    {
        //        //Move(activeTime, ShootVelocity, true, level.GTileEngine, level.GetSolids, false);
        //
        //        Movement.RotateCounterClockWise(FacingRight, ref Rotation, 12);
        //        //SpriteAnim.Update(activeTime);
        //
        //        //Create a hitbox once the bottle reaches the ground
        //        if (Location.Z == ObjectTile.Z)
        //        {
        //            //Play a sound for the bottle breaking
        //            LoadSounds.Play(LoadSounds.Break);
        //
        //            //BreakAnim.Reset(activeTime);
        //            Hitbox.Reset();
        //
        //            Hitbox explosionbox = new Hitbox(CollisionBox.X - 10, CollisionBox.Y - 10, 40, 40, Location.Z + 20f, 20, 8, 0, 700, true, false, LoadSounds.Kick);
        //            level.GetHazards.Add(new Explosion(new Animation(LoadGraphics.Explosion, false, 1, 700), new Vector3(CollisionBox.X, FeetLoc.Y, Location.Z), explosionbox, new Status(), level.GTileEngine));
        //        }
        //    }
        //}
        //
        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    /*if (Position.Z == ProjectileTile.Z && BreakAnim.IsAnimationEnd() == false)
        //        BreakAnim.Draw(spriteBatch, new Vector2(Position.X + OffSet.X, Position.Y - Position.Z + OffSet.Y), FacingRight, status.GStatusColor, new Vector2(Hurtbox.Width / 2, Hurtbox.Height / 2), 0f, GetDrawDepth(OffSet));
        //    else*/ if (Location.Z > ObjectTile.Z) base.Draw(spriteBatch, OffSet, TileEngine);
        //}
    }
}
