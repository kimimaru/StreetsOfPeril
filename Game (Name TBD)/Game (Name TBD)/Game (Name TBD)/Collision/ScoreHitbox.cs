﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game__Name_TBD_
{
    //A type of Hitbox that grants a specific Score value to the object it belongs to when it connects; this is used exclusively by Players
    public class ScoreHitbox : Hitbox
    {
        //The Hitbox's Score value
        private int Score;

        protected ScoreHitbox()
        {
            Score = 0;
        }

        public ScoreHitbox(Hitbox hitbox, int score) : base(hitbox)
        {
            Score = score;
        }

        //Gets the Score value of the Hitbox; this is used by ScoreHitboxes
        public override int GetScoreValue
        {
            get { return Score; }
        }
    }
}
