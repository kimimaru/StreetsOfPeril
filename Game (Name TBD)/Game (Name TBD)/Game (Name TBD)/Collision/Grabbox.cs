﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public sealed class Grabbox
    {
        private Vector2 WidthHeight;
        private bool IsGrabbed;

        //Whether the grabbox is halted or not (for when players switch sides and/or cutscenes) and how much time is left for the enemy to break from the grab
        private bool Halted;
        private float BreakRemaining;

        //Controls how long it takes for you to get out of a grab
        private float GrabBreakTimer;
        private float PrevGrabBreakTimer;

        //Controls how long after a grab before you can be grabbed again
        private float GrabRecoveryTimer;
        private float PrevGrabRecoveryTimer;

        //Tells how many times the thing grabbing you repositioned you or itself (switched sides or regrabbed from carrying)
        private byte SwitchTimes;

        //Constructor
        public Grabbox(int width, int height, float grabtime, float grabrecovery)
        {
            WidthHeight = new Vector2(width, height);
            IsGrabbed = false;

            Halted = false;
            BreakRemaining = 0f;

            GrabBreakTimer = grabtime;
            PrevGrabBreakTimer = 0f;

            GrabRecoveryTimer = grabrecovery;
            PrevGrabRecoveryTimer = 0f;

            SwitchTimes = 0;
        }

        //Gets the length of the grabbox based on the enemy's length
        public static int GetLength(int enemylength)
        {
            return (int)Math.Round(enemylength * .15f, MidpointRounding.AwayFromZero);
        }

        //Accessors
        public int Width
        {
            get { return (int)WidthHeight.X; }
        }

        public int Height
        {
            get { return (int)WidthHeight.Y; }
        }

        public bool GSIsGrabbed
        {
            get { return IsGrabbed; }
            set { IsGrabbed = value; }
        }

        public bool CanGetGrabbed(float activeTime)
        {
            return (IsGrabbed == false && (activeTime - PrevGrabRecoveryTimer) >= GrabRecoveryTimer);
        }

        public void GetGrabbed(float activeTime)
        {
            IsGrabbed = true;

            //Update the previous time until you can break out
            PrevGrabBreakTimer = activeTime;
        }

        //Halts the grabbox from the recovery time - used when Players start switching sides (Vaulting) to the other side of the enemy
        public void Halt()
        {
            Halted = true;

            //Store the time remaining in the grab break timer by finding the difference
            BreakRemaining = (Main.GetActiveTime - PrevGrabBreakTimer);
        }

        //Continues the grabbox - used when Players finished switching sides (Vaulting) to the other side of the enemy
        public void UnHalt()
        {
            Halted = false;

            //Restore the time remaining in the grab break timer before the halt by going back to the original difference
            PrevGrabBreakTimer = (Main.GetActiveTime - BreakRemaining);
        }

        //Increases the counter for the number of times the object grabbing you switched sides or regrabbed you from carrying
        public void Switched()
        {
            SwitchTimes++;
        }

        //Checks if the enemy should break out of the grab based on the number of times it has been switched (used in the Player class when switching sides)
        public bool ShouldBreakGrab()
        {
            return SwitchTimes >= 2;
        }

        public void ResetSwitched()
        {
            SwitchTimes = 0;
        }

        //Resets the recovery time from a grab so the enemy can be grabbed again - currently only used when Tank blocks a move so he can be grabbed afterwards
        public void ResetRecovery()
        {
            PrevGrabRecoveryTimer = Main.GetActiveTime - GrabRecoveryTimer;
        }

        //Sets the recovery time from a grab - for Players breaking grabs
        public void Recover(float activeTime)
        {
            IsGrabbed = false;
            Halted = false;
            PrevGrabRecoveryTimer = activeTime;
            SwitchTimes = 0;
        }

        private void BreakGrab(float activeTime)
        {
            if (SwitchTimes >= 2 || (Halted == false && (activeTime - PrevGrabBreakTimer) >= GrabBreakTimer))
            {
                Recover(activeTime);
            }
        }

        public void Update(float activeTime)
        {
            if (IsGrabbed == true)
                BreakGrab(activeTime);
        }

        //Only for debugging
        public void Draw(SpriteBatch spriteBatch, Vector2 Location, Vector2 OffSet)
        {
            Debug.DrawRectangle(spriteBatch, new Rectangle((int)Location.X, (int)Location.Y, Width, Height), OffSet, Color.Green, .9991f);
        }
    }
}