﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game__Name_TBD_
{
    //A class that handles hitbox-hurtbox collision among BeatEmUpObjs in a SubLevel
    public sealed class NewCollision
    {
        //The amount of hitlag objects experience when hitting something
        public const float HitLag = 100f;

        //SubLevel reference
        private SubLevel Sublevel;

        //DamageCollision references
        private List<DamageCollision> Collisions;

        public NewCollision(SubLevel level)
        {
            Sublevel = level;

            Collisions = new List<DamageCollision>();
        }

        //Performs hitbox-hurtbox collision checks
        public void PerformCollisions()
        {
            //Check collisions between types of objects
            CheckCollisions<Player, Player>(Sublevel.GetPlayers, Sublevel.GetPlayers, false, true);
            CheckCollisions<Player, Enemy>(Sublevel.GetPlayers, Sublevel.GetEnemies, true, false);
            CheckCollisions<Player, ItemContainer>(Sublevel.GetPlayers, Sublevel.GetContainers, false, false);
            CheckCollisions<Player, Hazard>(Sublevel.GetPlayers, Sublevel.GetHazards, true, false);
            CheckCollisions<Enemy, Hazard>(Sublevel.GetEnemies, Sublevel.GetHazards, true, false);
            CheckCollisions<Weapon, Player>(Sublevel.GetWeapons, Sublevel.GetPlayers, false, false);
            CheckCollisions<Weapon, Enemy>(Sublevel.GetWeapons, Sublevel.GetEnemies, false, false);
            CheckCollisions<Weapon, ItemContainer>(Sublevel.GetWeapons, Sublevel.GetContainers, false, false);

            //Carry out all the collisions
            CarryOutCollisions();
        }

        //Take in two BeatEmUpObj lists and check for collisions
        //secondlistcheck determines if the victims also check for hurting the attackers, and sametype indicates that both lists are of the same type
        private void CheckCollisions<T, H>(List<T> attackers, List<H> victims, bool secondlistcheck, bool sametype) where T : BeatEmUpObj where H : BeatEmUpObj
        {
            //Loop through the attackers
            for (int i = 0; i < attackers.Count; i++)
            {
                T Attacker = attackers[i];

                //Loop through the victims
                for (int j = 0; j < victims.Count; j++)
                {
                    H Victim = victims[j];

                    //If both lists are the same type, check if the Victim is the same as the Attacker and continue if so since we don't want the object to hurt itself
                    if (sametype == true && Victim == Attacker) continue;

                    //Loop through the attacker's hitboxes and check if any hit the victim
                    for (int k = 0; k < Attacker.Hitboxes.Count; k++)
                    {
                        //Construct a DamageCollision
                        DamageCollision potentialcollision = new DamageCollision(Attacker, Victim, Attacker.Hitboxes[k]);

                        //Check for a successful collision
                        if (potentialcollision.SuccessfulCollision() == true)
                        {
                            Collisions.Add(potentialcollision);

                            //Exit upon reaching the first successful collision
                            break;
                        }
                    }

                    //If the victims should check for collisions, do so
                    if (secondlistcheck == true)
                    {
                        //Loop through the victim's hitboxes and check if any hit the attacker
                        for (int k = 0; k < Victim.Hitboxes.Count; k++)
                        {
                            //Construct a DamageCollision
                            DamageCollision potentialcollision = new DamageCollision(Victim, Attacker, Victim.Hitboxes[k]);

                            //Check for a successful collision
                            if (potentialcollision.SuccessfulCollision() == true)
                            {
                                Collisions.Add(potentialcollision);

                                //Exit upon reaching the first successful collision
                                break;
                            }
                        }
                    }
                }
            }
        }

        //Finalizes all collisions and carries them out
        private void CarryOutCollisions()
        {
            //Loop through all collisions and carry them out
            for (int i = 0; i < Collisions.Count; i++)
            {
                Collisions[i].CarryOutCollision();
                Collisions.RemoveAt(i);
                i--;
            }
        }
    }

    //A struct for holding the things involved in a damage collision: the attacker, the victim, and the first hitbox the attacker connected with
    public struct DamageCollision
    {
        //The attacker
        public BeatEmUpObj Attacker;

        //The victim
        public BeatEmUpObj Victim;

        //The attacker's hitbox
        public Hitbox AttackerHitbox;

        public DamageCollision(BeatEmUpObj attacker, BeatEmUpObj victim, Hitbox attackerhitbox)
        {
            Attacker = attacker;
            Victim = victim;
            AttackerHitbox = attackerhitbox;
        }

        //Checks if the Attacker can harm the Victim
        public bool SuccessfulCollision()
        {
            return (Victim.CanGetHit(Attacker, AttackerHitbox) == true);
        }

        //Carries out the collision
        public void CarryOutCollision()
        {
            //Double check to make sure the object can still get hurt (Ex. multiple objects hit that same one)!
            if (SuccessfulCollision() == true)
            {
                Attacker.DamageObject(Victim, AttackerHitbox);
            }
        }
    }
}
