﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game__Name_TBD_
{
    //A special type of Hitbox used in Throw moves; it contains a delegate that gets called on the victim the Hitbox hits
    //It derives from ScoreHitbox so we can assign Throw moves used by Players a Score value; this value should be 0 for all other objects
    public sealed class ThrowHitbox : ScoreHitbox
    {
        //The ThrowHitbox's delegate definition
        public delegate void ThrowDelegate(BeatEmUpObj victim, int damage, Hitbox hitbox);

        //The method the ThrowHitbox has assigned
        private ThrowDelegate DamageMethod;

        public ThrowHitbox(Hitbox hitbox, int score, ThrowDelegate damagemethod) : base(hitbox, score)
        {
            HitboxType = HitboxTypes.Throw;

            DamageMethod = damagemethod;
        }

        //Gets the Hitbox's damaging method; this is used by ThrowHitboxes
        public override ThrowDelegate GetThrowDelegate
        {
            get { return DamageMethod; }
        }
    }
}
