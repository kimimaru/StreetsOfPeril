﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //A Hurtbox; this exists on all objects and is the region Hitboxes check for collisions with
    //If a foreign Hitbox hits an object's Hurtbox, the object will be dealt damage
    public sealed class Hurtbox
    {
        //Constants for the amounts to scale the lengths and heights of BeatEmUpObjs to set the Hurtbox's dimensions
        public const float HurtboxXScale = .75f;
        public const float HurtboxYScale = (1f/3f);

        private Vector2 WidthHeight;

        //Tracks when the Hurtbox was hit last
        public float PrevHit;

        //Constructor
        public Hurtbox(int width, int height)
        {
            WidthHeight = new Vector2(width, height);
            
            PrevHit = 0f;
        }

        //Vector2 constructor
        public Hurtbox(Vector2 widthheight) : this((int)widthheight.X, (int)widthheight.Y)
        {

        }

        //Accessors
        public int Width
        {
            get { return (int)WidthHeight.X; }
        }

        public int Height
        {
            get { return (int)WidthHeight.Y; }
        }

        //Only for debugging
        public void Draw(SpriteBatch spriteBatch, Vector2 Location, Vector2 OffSet)
        {
            Debug.DrawRectangle(spriteBatch, new Rectangle((int)Location.X, (int)Location.Y, (int)WidthHeight.X, (int)WidthHeight.Y), OffSet, Color.Blue);
        }
    }
}
