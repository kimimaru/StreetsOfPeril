﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //A Hitbox; this is produced by objects when they use moves
    //If a Hitbox touches an object's Hurtbox, it deals damage to the object it touched
    public class Hitbox
    {
        //Hitbox types
        public enum HitboxTypes
        {
            Standard, KnockDown, Throw
        }

        //The value to make a hitbox stay out forever
        public const float InfiniteHitbox = -1;

        protected Rectangle Box;

        //The list of Hurtboxes this Hitbox hit; this is to prevent the Hitbox from hitting the same Hurtbox more than once
        protected List<Hurtbox> Hurtboxes;

        //The type of Hitbox this is
        protected HitboxTypes HitboxType;

        //The current height the Hitbox is at
        protected float Height;

        //Time before the hitbox activates
        protected float ActivationTime;
        protected float PrevActivationTime;

        //How long the hitbox lasts
        protected float Duration;
        protected float PrevDuration;

        //The amount of time the activation time and duration were extended by due to hitlag
        protected float HitDelay;

        //The height range the Hitbox covers; it is added to the Height to obtain the MaxHeight the Hitbox covers
        protected int HeightRange;

        //Damage property used for differing strengths of attacks
        public int Strength;

        //The direction the object hit by the Hitbox faces after taking damage; if null, the object does not change the direction it's facing
        public bool? HitDirection;

        //The sound that plays when the hitbox successfully hits something
        public SoundEffect HitSound;

        protected Hitbox()
        {
            //Initialize the Hurtbox list
            Hurtboxes = new List<Hurtbox>();
        }

        //Constructor; this is a more direct and readable one made during refactoring
        public Hitbox(Rectangle hitrect, int strength, float minhitboxheight, int heightrange, float delay, float duration, HitboxTypes hitboxtype, bool? hitboxdirection, SoundEffect hitsound) : this()
        {
            Box = hitrect;
            Strength = strength;

            //The max height a Hitbox reaches is Height + HeightRange
            Height = minhitboxheight;
            HeightRange = heightrange;

            ActivationTime = delay;
            Duration = duration;
            HitboxType = hitboxtype;
            HitDirection = hitboxdirection;
            HitSound = hitsound;

            //Set the activation and duration times
            PrevActivationTime = Main.GetActiveTime;
            PrevDuration = Main.GetActiveTime + ActivationTime;
        }

        //Copy constructor
        public Hitbox(Hitbox otherbox) : this()
        {
            this.Box = otherbox.Box;
            this.Strength = otherbox.Strength;
            
            this.Height = otherbox.Height;
            this.HeightRange = otherbox.HeightRange;
        
            this.ActivationTime = otherbox.ActivationTime;
            this.Duration = otherbox.Duration;
            this.HitboxType = otherbox.HitboxType;
            this.HitDirection = otherbox.HitDirection;
            this.HitSound = otherbox.HitSound;
        
            this.PrevActivationTime = Main.GetActiveTime;
            this.PrevDuration = Main.GetActiveTime + this.ActivationTime;
        }

        public static Hitbox Empty
        {
            get { return new Hitbox(Rectangle.Empty, 0, 0, 0, 0, 0, HitboxTypes.Standard, null, null); }
        }

        public Rectangle GBox
        {
            get { return Box; }
        }

        public HitboxTypes GetHitboxType
        {
            get { return HitboxType; }
        }

        protected float MaxHeight
        {
            get { return (Height + HeightRange); }
        }

        public float MaxHitboxHeight
        {
            get { return Height; }
        }

        public int HitboxHeightRange
        {
            get { return HeightRange; }
        }

        //Tells if the Hitbox ever hit anything
        public bool Connected
        {
            get { return (Hurtboxes.Count > 0); }
        }

        //Gets the Score value of the Hitbox; this is used by ScoreHitboxes
        public virtual int GetScoreValue
        {
            get { return 0; }
        }

        //Checks if the Hitbox has a damaging method
        public bool HasThrowDelegate
        {
            get { return (GetThrowDelegate != null); }
        }

        //Gets the Hitbox's damaging method; this is used by ThrowHitboxes
        public virtual ThrowHitbox.ThrowDelegate GetThrowDelegate
        {
            get { return null; }
        }

        //Adds to the list of Hurtboxes that the Hitbox hit
        public void AddHurtbox(Hurtbox hurtbox)
        {
            Hurtboxes.Add(hurtbox);
        }

        //Gets the object's true activation time, taking hitlag and water into account
        protected float TrueActivationTime(bool isinwater)
        {
            float rate = 1f;
            if (isinwater == true) rate = NewAnimation.WaterSlow;

            return (int)(ActivationTime * rate);
        }

        //Gets the object's true duration, taking hitlag and water into account
        protected float TrueDuration(bool isinwater)
        {
            float rate = 1f;
            if (isinwater == true) rate = NewAnimation.WaterSlow;

            return (int)(Duration * rate);
        }

        //Tells if the Hitbox is active; this is when it can hit
        public bool IsActive(bool isinwater)
        {
            return (HasStarted(isinwater) == true && IsFinished(isinwater) == false);//(Duration == InfiniteHitbox || ((Main.GetActiveTime - PrevDuration) < TrueDuration(isinwater))));
        }

        //Tells if the hitbox has started
        public bool HasStarted(bool isinwater)
        {
            return ((Main.GetActiveTime - PrevActivationTime) >= TrueActivationTime(isinwater));
        }

        //Tells when the hitbox is finished
        public bool IsFinished(bool isinwater)
        {
            return (Duration != InfiniteHitbox && (Main.GetActiveTime - PrevDuration) >= TrueDuration(isinwater));
        }

        //Tells if the Hitbox can hit an object
        public bool CanHit(bool isinwater, BeatEmUpObj beatemupobj)
        {
            return (IsActive(isinwater) == true && (Height <= beatemupobj.MaxHeight && MaxHeight >= beatemupobj.CurHeight) &&
                Box.Intersects(beatemupobj.CollisionBox) == true && Hurtboxes.Contains(beatemupobj.Hurtbox) == false);
        }

        //Sets the Hitbox's hitbox type (used for modifying hitboxes for special conditions, such as when Tank blocks an attack)
        public void SetHitboxType(HitboxTypes hitboxtype)
        {
            HitboxType = hitboxtype;
        }

        //Make the hitbox move with the object it belongs to and change its height if the object is in the air
        public void Follow(float x, float y, float JumpVelocityV)
        {
            Box.X += (int)x;
            Box.Y += (int)y;

            Height += JumpVelocityV;
        }

        //Delays the hitbox activation/duration due to hitlag
        public void DelayHitbox(bool isinwater, float amount)
        {
            //If the Hitbox has already started, don't delay the activation
            if (HasStarted(isinwater) == false) PrevActivationTime += amount;
            PrevDuration += amount;
        }

        //Ends a Hitbox early; this is used when certain Hitboxes connect, namely aerial grabs
        public void EndHitbox()
        {
            Duration = 0f;
        }

        //Only for debugging
        public void Draw(SpriteBatch spriteBatch, bool isinwater, Vector2 OffSet)
        {
            if (IsActive(isinwater) == true)
            {
                //Draw hitboxes Red; if they're Throw hitboxes, draw them DeepPink
                Color drawcolor = Color.Red;
                if (HitboxType == HitboxTypes.Throw) drawcolor = Color.DeepPink;

                Debug.DrawRectangle(spriteBatch, new Rectangle((int)Box.X, (int)Box.Y, Box.Width, Box.Height), OffSet, drawcolor, .998f);
            }
        }
    }
}
