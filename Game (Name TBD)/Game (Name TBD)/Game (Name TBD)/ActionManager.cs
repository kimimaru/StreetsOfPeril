﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Runtime.Serialization;

namespace Game__Name_TBD_
{
    //Helps manages the actions an enemy does
    public class ActionManager
    {
        //The various action states enemies can be in, which decides the types of actions they do
        //Preset behaviors are behaviors that they're set to do (Ex. Galsia running with a knife) or a follow-up to a specific action (Gerald/Kate's Uppercut Swipe ONLY after a roll has been performed)
        //The Command behavior is reserved to the Commander boss' minions, who can be Basic or Intermediate enemies
        public enum ActionStates
        {
            Movement, Offense, Defense, Preset1, Preset2, Preset3, Preset4, Preset5, Command
        };

        //NEW AI: The actual action the enemy is performing and the current action state/behavior the enemy is in
        public Action CurrentAction;
        public int CurState;

        //Checks if an action is chosen
        public bool ActionChose;

        //Checks if the current action is done
        public bool ActionDone;

        //Checks if the current action cannot be completed
        public bool ActionNotDone;

        //Checks if a new target was already set
        public bool TargetSet;

        //The time since the last action was started
        private float PrevAction;

        //The time since the last action was finished
        private float PrevActionDone;

        //The maximum time an action can be going on for - 5 seconds
        //public const float MaxActionTime = 5000f;

        //Default constructor
        public ActionManager(Enemy enem, int behavior = (int)ActionStates.Movement)
        {
            ActionChose = false;
            ActionDone = false;
            ActionNotDone = false;
            TargetSet = false;

            CurState = behavior;
            CurrentAction = new Idle(enem);

            PrevAction = 0f;
            PrevActionDone = 0f;
        }

        //Constructor for initializing starting action
        public ActionManager(Enemy enem, Action startaction, int behavior = (int)ActionStates.Movement) : this(enem, behavior)
        {
            CurrentAction = startaction;
            ActionChose = true;
        }

        //Switches to a new behavior
        //Consider setting CurrentAction to null and check if it's null or not instead of using ActionChose; this may be problematic since the behavior passed in is from the action, and if the enemy gets hit it won't have an action to go back to
        public void SwitchBehavior(int? behavior)
        {
            //Don't switch if the behavior is null or the enemy hasn't chosen an action yet, otherwise the enemy would have finished the action already and chosen a state upon finishing
            if (ActionChose == true && behavior != null) CurState = (int)behavior;

            CurrentAction.Reset();

            //Perform post-action effects on the enemy (Setting a wait timer, etc.)
            ActionChose = false;
            ActionDone = false;
            ActionNotDone = false;
        }

        //Gets the time since the last action was chosen
        public float TimeLastActionChosen
        {
            get { return (Main.GetActiveTime - PrevAction); }
        }

        //Gets the time since the last action was ended
        public float TimeLastActionEnded
        {
            get { return (Main.GetActiveTime - PrevActionDone); }
        }

        //Confirms that an action was chosen
        public void ChoseAction(float activeTime)
        {
            ActionChose = true;
            PrevAction = activeTime;
        }

        //Ends an action, whether it was finished or can't be finished
        public void EndAction(float activeTime, bool unfinished = false)
        {
            if (unfinished == false) ActionDone = true;
            else ActionNotDone = true;
            PrevActionDone = activeTime;

            CurrentAction.Complete();
        }

        public bool IsActionDone()
        {
            return (ActionDone == true || ActionNotDone == true);// || (activeTime - PrevAction) > MaxActionTime);
        }

        //Determines if an enemy should switch its target to another player - Enemy dependent
        public bool ShouldSwitchTarget()
        {
            Random randtarget = new Random();
            int newtarget = randtarget.Next(0, 3);

            return (newtarget == 0);
        }

        //If an enemy is switching targets, determine which target the enemy chooses
        //If there is only one player to begin with then don't even call methods relating to switching targets
        public Player SwitchTarget(List<Player> Players)
        {
            TargetSet = true;

            return (Players[new Random().Next(0, Players.Count)]);
        }
    }
}