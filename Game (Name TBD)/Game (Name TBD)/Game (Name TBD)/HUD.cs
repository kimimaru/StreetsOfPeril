﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Use inheritance to make separate HUDs for players, enemies, and objects (maybe separate into Weapons, Items, etc.) if needed
    public class HUD
    {
        //The dimensions of each main HUD icon (object portrait)
        protected const int IconSize = 16;

        //The min and max possible transparency values for the HUD
        protected const int MinTransparency = 0;
        protected const int MaxTransparency = 255;

        //The max size of a Status icon
        protected static readonly Vector2 StatusIconSize;

        //How long it takes for the health indicator to drain after an object takes damage
        protected const float HealthIndDrain = 300f;

        //The total number of frames the death icon lasts; half of this value is how many frames it's drawn
        protected const int DeadIconFrames = 4;
        protected const int DeadIconDraw = (DeadIconFrames / 2);

        //The minimum size a health bar can be scaled
        protected const float MinHealthBarScale = .1f;

        //Constants for the health bar, current health, and end of the health bar indices, respectively
        protected const int HealthBarIndex = 0;
        protected const int CurHealthIndex = 1;
        protected const int HealthEndIndex = 2;

        //The opacity of the health bar that shows how much damage has been taken and how much an item heals (for players), respectivelly
        protected const float DamageOpacity = (180f / 255f);
        protected const float HealOpacity = (70f / 255f);

        //Icons for objects that the HUD draws; if null, don't draw (although everything will most likely have an icon anyway - all players and enemies, at least)
        protected Texture2D Icon;

        //The object associated with the HUD
        protected BeatEmUpObj BeatEmUpObject;

        //The main position of the HUD on screen; all other positions are relative to this one
        protected Vector2 MainPosition;

        //For drawing the HUD with some transparency; can be used for fading the HUD at times (HUD disappearing from view) or anything else
        protected int Transparency;

        //Stores health before getting hit and time since getting hit to show how much health was lost or gained
        protected float OrigHealth;
        protected float PrevHealth;
        protected float DrainSpeed;
        
        //Determines whether to show the dead icon or not
        protected int DrawDeadIcon;

        //For properly drawing the status timer even after completing a level
        protected float StatTimer;

        //For player HUD and level timer at start of level
        protected float[] EndDepth;

        //Duration a temporary HUD stays on screen if no damage is dealt or received
        protected const float HUDDur = 2500f;

        //Timer for how long the HUD has been on screen
        protected float PrevHUDDur;

        protected HUD()
        {
            Transparency = MaxTransparency;

            StatTimer = 1f;

            OrigHealth = 0f;
            DrawDeadIcon = 0;
            DrainSpeed = 0f;

            PrevHUDDur = 0f;
            //TEMPORARY
            EndDepth = new float[] { .995f, .996f };
        }

        //Main constructor
        public HUD(BeatEmUpObj beatemupobj, Vector2 mainposition) : this()
        {
            BeatEmUpObject = beatemupobj;

            Transparency = MaxTransparency;

            MainPosition = mainposition;
        }

        static HUD()
        {
            StatusIconSize = new Vector2(13, 11);
        }

        public static float GetHUDDuration
        {
            get { return HUDDur; }
        }

        //Gets the onscreen location of the HUD
        public Vector2 GetPosition
        {
            get { return MainPosition; }
        }

        //The positions for each main piece of information; derived HUD types may have their own for object-specific properties (Ex. Score for Players)
        protected Vector2 StatusPos
        {
            get { return new Vector2(MainPosition.X, MainPosition.Y - 1); }
        }

        //NOTE: We'll see if this happens in the future, but the bar underneath the Status icon may not be necessary
        //Instead, we can draw the status icon two times: one with some transparency, and another fully opaque on top that decreases as the Status runs out
        //This may require larger icons since it's currently hard to see
        protected Vector2 StatusDurPos
        {
            get { return new Vector2(StatusPos.X + 3, StatusPos.Y + StatusIconSize.Y + 1); }
        }

        protected Vector2 IconPos
        {
            get { return new Vector2(MainPosition.X + 11, MainPosition.Y); }
        }

        protected virtual Vector2 NamePos
        {
            get { return new Vector2(IconPos.X + IconSize + 2, MainPosition.Y + 3); }
        }

        protected virtual Vector2 HealthPos
        {
            get { return new Vector2(IconPos.X + 1, IconPos.Y + (IconSize - 1) + 3); }
        }

        //Checks if the HUD is inactive
        public bool IsInactive
        {
            get { return (Main.GetActiveTime >= PrevHUDDur); }
        }

        //Takes in a color and returns the same color with the HUD's transparency value 
        protected Color GetDrawColor(Color color)
        {
            return color * (float)(Transparency / (float)MaxTransparency);
        }

        //Sets the HUD's transparency
        public void SetTransparency(int newtransparency)
        {
            Transparency = newtransparency;

            //Ensure the transparency value doesn't go below the minimum possible value or above the maximum possible value
            if (Transparency < MinTransparency) Transparency = MinTransparency;
            else if (Transparency > MaxTransparency) Transparency = MaxTransparency;
        }

        //Sets the main position of the HUD
        public void SetPosition(Vector2 newposition)
        {
            MainPosition = newposition;
        }

        //Returns how long the HUD has been active
        public float GetHUDActiveTime()
        {
            return (Main.GetActiveTime - (PrevHUDDur - HUDDur));
        }

        //Restarts HUD
        public void Restart()
        {
            PrevHUDDur = Main.GetActiveTime + HUDDur;
        }

        //Restarts the HUD, setting its duration to a particular amount of time - this is only to be used by Players for their InteractionHUDs
        public void RestartWithDur(float duration)
        {
            PrevHUDDur = Main.GetActiveTime + duration;
        }

        //Ends the HUD's duration of drawing onscreen
        public void EndDuration()
        {
            PrevHUDDur = 0f;
        }

        //Sets the depth array back to its original values (Starting a level)
        public void ResetDepth()
        {
            EndDepth[0] = .995f;
            EndDepth[1] = .996f;
        }

        //Sets the depth array to override the screen transition (Ending a level)
        public void SetDepth()
        {
            EndDepth[0] = .999f;
            EndDepth[1] = 1f;
        }

        //Returns how much to scale the health bar based on the object's current max health
        protected float GetHealthBarScale()
        {
            //Find out how much to scale the health bar by dividing the object's current max health by the max possible health allowed in a health bar
            float healthscale = BeatEmUpObject.CurMaxHealth / (float)BeatEmUpObj.MaxHealthInBar;
            if (healthscale < .1f) healthscale = .1f;

            return healthscale;
        }

        //Returns the health bar, current health, and health bar end graphics in an array (0 = health bar, 1 = cur health, 2 = health bar end)
        protected virtual Texture2D[] GetHealthTextures()
        {
            //Get the graphics for the health bar; each health bar has a different color
            //If it exceeds the last one, use the last one
            int healthindex = BeatEmUpObject.HealthBars;
            if (healthindex >= LoadGraphics.HealthTex.Length) healthindex = LoadGraphics.HealthTex.Length - 1;

            //Initialize the array with the proper health graphics
            Texture2D[] healthtextures = new Texture2D[3]
            {
                LoadGraphics.HealthTex[healthindex],
                LoadGraphics.CurHealthTex[healthindex],
                LoadGraphics.HealthTexEnd[healthindex]
            };

            return healthtextures;
        }

        public void Update()
        {
            //If the object has a Status that doesn't last forever under any condition, update a value that states how long it lasts relative to its duration
            if (BeatEmUpObject.status != (int)Status.Statuses.None && BeatEmUpObject.StatForever == false && BeatEmUpObject.status.IsInfinite == false)
                StatTimer = BeatEmUpObject.status.RelativeStatDur;

            //If the object is dead, update a little red X that appears over its icon for 2 frames every 2 frames
            if (BeatEmUpObject.IsDead == true)
                DrawDeadIcon = (DrawDeadIcon + 1) % DeadIconFrames;
            else if (DrawDeadIcon != 0) DrawDeadIcon = 0;

            //Update the effect that shows the object's health draining
            if (OrigHealth != 0f && (Main.GetActiveTime - PrevHealth) >= HealthIndDrain)//Main.GetActiveTime >= PrevHealth)
            {
                //If there's no more health left to show draining, set the health and drain speed to 0
                if (OrigHealth <= 0f)
                {
                    OrigHealth = 0f;
                    DrainSpeed = 0f;
                }
                //Otherwise subtract the health from the amount that should be drained per frame
                else OrigHealth -= DrainSpeed;
            }

            //Let any derived HUDs update any specific information they may have
            HUDUpdate();
        }

        protected virtual void HUDUpdate()
        {

        }

        //Sets the original health and time before the damage amount moves down to your current health
        public void SetDamageIndicator(float activeTime, float health, int healthbars, int totaldamage)
        {
            //If the current health is less than the total damage...
            if (health <= totaldamage)
            {
                //If you have more healthbars left, take the totaldamage and subtract it from the health so the graphic doesn't go past the health bar
                if (healthbars > 0) OrigHealth = totaldamage - health;
                //Otherwise, use the health as the difference so the graphic isn't bigger than the amount of damage that should be done
                else OrigHealth = health;
            }
            //...otherwise simply use the total damage dealt as the difference
            else OrigHealth = totaldamage;

            DrainSpeed = (float)Math.Ceiling(OrigHealth / 20f);

            PrevHealth = activeTime;
            //PrevHealth = Main.GetActiveTime + HealthIndDrain;
        }

        //Resets the damage indicator
        public void ResetDamageIndicator()
        {
            OrigHealth = 0f;
            PrevHealth = 0f;
            DrainSpeed = 0f;
        }

        //Draws the object's icon
        protected void DrawIcon(SpriteBatch spriteBatch)
        {
            //Don't draw if the object doesn't have an icon
            if (BeatEmUpObject.Icon != null)
            {
                //Don't draw the Icon if the player is currently not drawn due to the Invincibile Status' color
                if (BeatEmUpObject.status.GStatusColor.A != 0)
                    spriteBatch.Draw(BeatEmUpObject.Icon, IconPos, null, GetDrawColor(BeatEmUpObject.status.GStatusColor), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[1]);

                //If the object is dead, draw the dead icon if it's on a frame it'll draw (the red X)
                if (BeatEmUpObject.ShouldDrawDeadIcon() == true && DrawDeadIcon < DeadIconDraw)
                    spriteBatch.Draw(LoadGraphics.DeadIcon, IconPos + new Vector2(1, 1), null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[1] + .001f);
            }
        }

        //Draws the object's name
        protected void DrawName(SpriteBatch spriteBatch)
        {
            //If the object doesn't have a name for some reason, don't draw it
            if (BeatEmUpObject.Name != null)
                spriteBatch.DrawString(LoadGraphics.ImgFont, BeatEmUpObject.Name, NamePos, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
        }

        //Draws the object's health
        protected void DrawHealth(SpriteBatch spriteBatch)
        {
            //Some objects, such as Lasers and Spikes, will have a Max Health of 0 to avoid drawing their health bars
            if (BeatEmUpObject.CurMaxHealth > 0)
            {
                //Find how much to scale the size of the health bar
                float healthscale = GetHealthBarScale();

                //Find out how much to scale the size of the current health bar
                float curhealthscale = (BeatEmUpObject.GetHealth / (float)BeatEmUpObj.MaxHealthInBar);

                //Get the graphics for the health bars
                Texture2D[] healthtextures = GetHealthTextures();

                //This is the end location to draw the end of the health bar
                Vector2 endloc = new Vector2(HealthPos.X + (healthtextures[HealthBarIndex].Width * healthscale), HealthPos.Y + 1);

                //Draw one end of the health bar at the front; this allows us to easily scale the mid-section
                //Next draw the midsection with the current health on top
                //Finally draw the end of the health bar
                spriteBatch.Draw(healthtextures[HealthEndIndex], HealthPos + new Vector2(-1, 1), null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
                spriteBatch.Draw(healthtextures[HealthBarIndex], HealthPos, null, GetDrawColor(Color.White), 0f, Vector2.Zero, new Vector2(healthscale, 1), SpriteEffects.None, EndDepth[0]);
                spriteBatch.Draw(healthtextures[CurHealthIndex], HealthPos + new Vector2(0, 1), null, GetDrawColor(Color.White), 0f, Vector2.Zero, new Vector2(curhealthscale, 1), SpriteEffects.None, EndDepth[1]);
                spriteBatch.Draw(healthtextures[HealthEndIndex], endloc, null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);

                //If the object took damage recently, show how much damage was dealt on the health bar as well
                DrawDamageDifference(spriteBatch, healthtextures[CurHealthIndex]);
            }
        }

        //Draws the object's Status icon and Status bar
        protected void DrawStatus(SpriteBatch spriteBatch)
        {
            //If the object has a Status of None, don't draw anything
            if (BeatEmUpObject.status != (int)Status.Statuses.None)
            {
                //Draw Status icon
                spriteBatch.Draw(LoadGraphics.StatusSprites[BeatEmUpObject.status.GCond - 1], StatusPos, null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);

                //If the object has a Status that doesn't last forever under any condition, draw its remaining duration
                if (BeatEmUpObject.StatForever == false && BeatEmUpObject.status.IsInfinite == false)
                {
                    //Find out how much to move the bar showing how much time is remaining for the status downwards
                    //Since we start drawing at the top and scaling reduces the height, we need to move the bar down so it looks right
                    float stattimescale = LoadGraphics.NewCurStatusTime.Height - (LoadGraphics.NewCurStatusTime.Height * StatTimer);
                    Vector2 curstatusstartloc = new Vector2(1, 1 + stattimescale);
                    
                    spriteBatch.Draw(LoadGraphics.NewStatusTime, StatusDurPos, null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
                    spriteBatch.Draw(LoadGraphics.NewCurStatusTime, StatusDurPos + curstatusstartloc, null, GetDrawColor(Color.White), 0f, Vector2.Zero, new Vector2(1, StatTimer), SpriteEffects.None, EndDepth[1]);
                }
            }
        }

        //Draws information about the object on screen
        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw Icon
            DrawIcon(spriteBatch);

            //Draw name
            DrawName(spriteBatch);

            //Draw health
            DrawHealth(spriteBatch);

            //Draw Status icon and Status bar
            DrawStatus(spriteBatch);

            //Let any derived HUDs draw any specific information they may have
            HUDDraw(spriteBatch);
        }

        protected virtual void HUDDraw(SpriteBatch spriteBatch)
        {

        }

        //Overload for Draw to be used by Players when drawing their InteractionHUDs; draws the object's HUD in a particular location
        public void DrawInteractionHUD(SpriteBatch spriteBatch, Vector2 position)
        {
            //Move the HUD's position for this Draw, then set it back
            Vector2 prevmainposition = MainPosition;
            MainPosition = position;

            //Draw the HUD normally
            Draw(spriteBatch);

            //Set the object HUD's position back to its original location
            MainPosition = prevmainposition;
        }

        //Show the amount of damage the most recent attack did by displaying a different colored health bar where the object's current health is and end it
        //where the object's original health was
        protected void DrawDamageDifference(SpriteBatch spriteBatch, Texture2D curhealthtex)
        {
            //If the object recently took damage, let's show how much damage it took relative to the whole health bar
            if (OrigHealth > 0f)
            {
                //Find out the scale of the width of the health bar in comparison to the max health allowed in a health bar
                float barscale = (curhealthtex.Width / (float)BeatEmUpObj.MaxHealthInBar);

                //Next find out how the health the object has scales with the barscale
                float curhealthscale = BeatEmUpObject.GetHealth * barscale;

                //Finally figure out how much the damage portion scales with the barscale
                float orighealthscale = OrigHealth * barscale;

                //Start drawing at the normal position for health plus the scale for the current amount of health we have
                Vector2 startDrawLoc = HealthPos + new Vector2(curhealthscale, 1);

                //Draw the damage difference with an opacity, scaling by orighealthscale
                spriteBatch.Draw(curhealthtex, startDrawLoc, new Rectangle(0, 0, 1, curhealthtex.Height), GetDrawColor(Color.White * DamageOpacity), 0f, Vector2.Zero, new Vector2(orighealthscale, 1), SpriteEffects.None, EndDepth[1]);
            }
        }
    }
}