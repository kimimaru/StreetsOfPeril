﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //This class handles object movement and checks for movement collisions
    public static class Movement
    {
        //Movement methods passed in by the object that moved; the first is used when the object moves, the second is used when the object bumps into a wall, and the third is used when the object bumps into a solid object
        public delegate void Move(float velocity);
        public delegate void HitWall();
        public delegate void HitSolid();

        //Find the minimum initial air velocity needed to reach the higher ground
        //Determine how high an object needs to jump, and set the object's vertical air velocity to the proper value
        //The formula used is for a sum decreasing at a constant rate with the last number as 0, with numtimes being the number of elements in the sum
        //Furthermore, the offset from 0 is subtracted from the main sum to get the exact value (Ex. 1.9 initial airvelocity.Y for heightdiff = 10)
        //gravityvalue is the current gravity value the object is subjected to (land or in water), curtileheight is the height of the tile the object is on, and nexttileheight is the height of the tile the object wants to jump to
        //CREDIT TO: Anand Oza for the calculation!
        public static float MinAirVelocityZToHigherGround(float gravityvalue, float curtileheight, float nexttileheight)
        {
            //This calculation can also be done as an algorithm, in which you simulate the jump from the height difference (keep adding the gravityvalue until you get the value you want)
            //It works ONLY for one gravity value, so if the objects initially starts in water then goes onto land it won't reach the platform; a fix can be implemented that checks the height of water and such, but it doesn't seem necessary at the moment

            //The difference in height from the tile the object wants to go to and the tile the object is on
            float heightdiff = nexttileheight - curtileheight;

            //Temp value stored for a part of the calculation for readability
            float change = (gravityvalue / 2f);

            //Find the number of times the object will be affected by gravity (the number of times gravityvalue decreases your height)
            float numtimes = (float)Math.Floor((change + Math.Sqrt(Math.Pow(change, 2) + (2 * gravityvalue * heightdiff))) / gravityvalue);

            //Using the number of times gravity affects the object, find the offset and calculate the sum to find the minimum vertical air velocity the object needs to jump to exactly reach the other tile
            float minairvelocityz = ((gravityvalue * numtimes) + (heightdiff - ((float)Math.Pow(numtimes, 2) * change - (numtimes * change))) / numtimes);

            return minairvelocityz;
        }

        //The total time an object spends in the air (Ex. 4 + 3.8 + 3.6...-3.6 - 3.8 - 4 = OrigTileHeight); divide by 2 to get how long it takes for an object to rise or fall
        public static float TotalAirTime(float AirVelocity, float GravityValue)
        {
            //Divide the AirVelocity by the gravity constant the object is experiencing (normal or in water); this gives us how many frames it takes for an object to rise or fall
            //Then, multiply by 2 to get the total air time
            float AirTime = (AirVelocity / GravityValue) * 2;

            return AirTime;
        }
    }
}