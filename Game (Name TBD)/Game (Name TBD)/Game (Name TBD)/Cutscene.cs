﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Out of gameplay cutscenes played before/after levels or in the intro (SOR/FF styled cutscenes)
    //NOTE: The story may be explained in-game through various events, so the intro and ending cutscenes may be the only cutscenes in the game
    public class Cutscene
    {
        //Cutscene graphic(s) - All even numbers for TransNum correlates to fade in
        private List<Texture2D> CutsceneGraphics;
        private List<Vector2> CutLocations;
        private List<float> CutTimes;
        private int CutsceneNum;
        private float PrevCutTime;
        private int CutTransition;
        private int CutTransNum;
        private int CutTransChange;
        private bool CutsceneGraphicOn;

        //The text to display (in graphic form for now)
        private List<Texture2D> Text;
        private List<Vector2> TextLocations;
        private List<float> TextTimes;
        private int TextNum;
        private float PrevTextTime;
        private int TextTransition;
        private int TextTransNum;
        private int TextTransChange;
        private bool TextOn;

        //Tells if the cutscene is finished or not
        private bool Finished;

        //Check if enter was pressed
        private KeyboardState KeyboardState;

        //Constructor
        public Cutscene(List<Texture2D> cutscenegraphics, List<Texture2D> text, List<Vector2> cutlocations, List<Vector2> textlocations, List<float> cuttimes, List<float> texttimes)
        {
            CutsceneGraphics = cutscenegraphics;
            CutLocations = cutlocations;
            CutTimes = cuttimes;
            CutsceneNum = 0;
            PrevCutTime = 0;
            CutTransition = 0;
            CutTransNum = 0;
            CutTransChange = 5;
            CutsceneGraphicOn = true;

            Text = text;
            TextLocations = textlocations;
            TextTimes = texttimes;
            TextNum = 0;
            PrevTextTime = 0f;
            TextTransition = 0;
            TextTransNum = 0;
            TextTransChange = 5;
            TextOn = true;

            Finished = false;

            KeyboardState = new KeyboardState(Keys.Enter);
        }

        public bool IsFinished()
        {
            return Finished;
        }

        private void ReverseFade(float activeTime, ref float PrevTime, ref int Change, ref bool On)
        {
            Change = -Change;
            PrevTime = activeTime;
            On = false;
        }

        //Fades text or a graphic in/out
        private void Transition(float activeTime, float Time, ref int SceneNum, ref float PrevTime, ref int Transition, ref int TransNum, ref int Change, ref bool On)
        {
            //If we're supposed to transition, then do the transition
            if (On == true)
            {
                Transition += Change;
                
                //Fade in
                if (Transition >= 255)
                {
                    Transition = 255;
                    ReverseFade(activeTime, ref PrevTime, ref Change, ref On);
                }
                //Fade out
                else if (Transition <= 0)
                {
                    Transition = 0;
                    SceneNum++;
                    ReverseFade(activeTime, ref PrevTime, ref Change, ref On);
                }
            }
            else if ((activeTime - PrevTime) >= Time)
            {
                On = true;
                TransNum++;
            }
        }

        //Fade the text or graphics in or out
        public void Update(float activeTime)
        {
            if (Finished == false)
            {
                if (CutsceneGraphics != null && CutsceneNum < CutsceneGraphics.Count)
                    Transition(activeTime, CutTimes[CutTransNum], ref CutsceneNum, ref PrevCutTime, ref CutTransition, ref CutTransNum, ref CutTransChange, ref CutsceneGraphicOn);
                if (Text != null && TextNum < Text.Count)
                    Transition(activeTime, TextTimes[TextTransNum], ref TextNum, ref PrevTextTime, ref TextTransition, ref TextTransNum, ref TextTransChange, ref TextOn);

                //End condition, also if the player presses enter during the cutscene then skip it
                if ((CutsceneNum == CutsceneGraphics.Count && TextNum == Text.Count && CutsceneGraphicOn == false && TextOn == false) || Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
                {
                    Finished = true;
                }
            }

            KeyboardState = Keyboard.GetState();
        }

        //Draw the black screen with the graphics and text over it
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadGraphics.BlackFade, new Vector2(0, 0), null, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);
            if (CutsceneGraphics != null && CutsceneNum < CutsceneGraphics.Count)
                spriteBatch.Draw(CutsceneGraphics[CutsceneNum], CutLocations[CutsceneNum], null, new Color(CutTransition, CutTransition, CutTransition), 0f, Vector2.Zero, 1f, SpriteEffects.None, .997f);
            if (Text != null && TextNum < Text.Count)
                spriteBatch.Draw(Text[TextNum], TextLocations[TextNum], null, new Color(TextTransition, TextTransition, TextTransition), 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
        }
    }
}
