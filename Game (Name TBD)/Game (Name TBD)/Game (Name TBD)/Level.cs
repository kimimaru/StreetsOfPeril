﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The levels in the game
    //NOTE: Be creative with these level intros and outros! They should probably have the same theme as the level (in the "Level X Start" or "Level X End" text, or separate; be sure to draw out how you want it digitally or on paper first)!
    //Maybe even show a world map between each level (like Super Castlevania IV)!
    //NOTE: An idea for the future is to have each SubLevel have a link to the next one, sort of like a LinkedList! The first and last of each Level would be denoted
    //by a boolean that can be set for each sublevel, and if you get a Bonus Stage, it inserts it in between the current level and the next level
    public class Level
    {
        //Set a delegate based on the entrance or exit chosen and call that method at the start or end of the level; when the entrance or exit is over, set it to null
        //The types of level entrances (how sublevel begins)
        public enum LevelEntrances
        {
            None, WalkRight, WalkLeft, WalkUp, WalkDown, JumpIn
        };

        //The types of level exits (how sublevel ends)
        public enum LevelExits
        {
            None, ExitRight, ExitLeft, ExitUp, ExitDown, JumpOut, WalkTo
        };

        //Level transition delegate; it can be used for Sublevels, Challenge levels, and more
        public delegate void LevelTrans(float activeTime, ref LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc);

        //The sublevels in the level
        protected List<SubLevel> SubLevels;

        //Level Start and End animations
        protected Intro LevelIntro;
        protected Outro LevelOutro;

        //Tells how many times the player autosaved or not
        public int AutoSaved;

        //Current sublevel
        protected int CurSubNum;

        //Timer used for Game Over
        protected float GameOverTime;

        //Timer used for the level end
        protected float EndLevelTime;

        //Tells when the level is over
        protected bool LevelFinished;

        //Determines if the player(s) will be taken to a bonus stage or not at the end of the level
        public static bool StartBonus = false;

        //Player instances - players are inside the level
        protected List<Player> Players;

        //Variables for making the text that tells a player to press start to join in blink
        private float PrevBlink;
        private CharInfo[] ContDisplays;

        //The character costumes that are taken; true indicates taken, false indicates available
        private bool[][] CostumesTaken;

        protected int LevelNum;

        //The location on screen to draw the level
        protected readonly Vector2 LevelDrawLoc;

        //For using a continue
        private KeyboardState KeyboardState;

        public Level()
        {
            CurSubNum = 0;

            AutoSaved = 3;

            GameOverTime = 0;
            EndLevelTime = 0f;

            LevelDrawLoc = new Vector2(170, -2);

            KeyboardState = new KeyboardState(Keys.Enter);
        }

        //Constructor
        public Level(List<Player> players, Texture2D background, Texture2D foreground, int lvlnum) : this()
        {
            LevelNum = lvlnum;

            Players = players;
            PrevBlink = 0f;

            //Sets up drop-in joining states
            SetUpDropInJoin();

            //Create sublevels
            SubLevelCreate();

            LevelIntro = new Intro(new Texture2D[] { LoadGraphics.LevelIntro, LoadGraphics.LevelNumGraphic[LevelNum], LoadGraphics.LevelStart });

            //This gets set right when the level ends
            LevelOutro = null;
        }

        //Accessors and Manipulation Procedures
        public virtual HUD GetTimer
        {
            get { return null; }
        }

        public int GetLevelNum
        {
            get { return LevelNum; }
        }

        protected String LevelName
        {
            get { return ("Level " + Convert.ToString(LevelNum + 1) + "-" + Convert.ToString(CurSubNum + 1)); }
        }

        public int GetTimeBonus
        {
            get
            {
                return -1;
            }
        }

        public SubLevel CurSubLevel
        {
            get { return SubLevels[CurSubNum]; }
        }

        //Level entrance and exit methods
        //Default entrance/exit method
        public static void DefaultEnterExit(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            levelentranceexit = null;
        }

        public static void EnterRight(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            bool Done = true;

            //Move the players to the right
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 StopPosition = new Vector2(destinationloc[Players[i].GPlayerNum].X, destinationloc[Players[i].GPlayerNum].Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(Players[i].GetLocationHeight.X < StopPosition.X ? 1 : 0, 0));
                if (Players[i].GetLocationHeight.X < StopPosition.X) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void EnterLeft(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            bool Done = true;

            //Move the players to the left
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 StopPosition = new Vector2(destinationloc[Players[i].GPlayerNum].X, destinationloc[Players[i].GPlayerNum].Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(Players[i].GetLocationHeight.X > StopPosition.X ? -1 : 0, 0));
                if (Players[i].GetLocationHeight.X > StopPosition.X) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void EnterUp(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            bool Done = true;

            //Move the players to the left
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 StopPosition = new Vector2(destinationloc[Players[i].GPlayerNum].X, destinationloc[Players[i].GPlayerNum].Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(0, Players[i].GetLocationHeight.Y > StopPosition.Y ? -1 : 0));
                if (Players[i].GetLocationHeight.Y > StopPosition.Y) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void EnterDown(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            bool Done = true;

            //Move the players to the left
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 StopPosition = new Vector2(destinationloc[Players[i].GPlayerNum].X, destinationloc[Players[i].GPlayerNum].Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(0, Players[i].GetLocationHeight.Y < StopPosition.Y ? 1 : 0));
                if (Players[i].GetLocationHeight.Y < StopPosition.Y) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void ExitRight(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            Vector2 StopPosition = new Vector2(Main.ScreenSize.X + 30, 0);

            bool Done = true;

            //Move the players to the right
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 playerlocation = new Vector2(Players[i].GetLocationHeight.X + Offset.X, 0);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(playerlocation.X < StopPosition.X ? 1 : 0, 0));
                if (playerlocation.X < StopPosition.X) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void ExitLeft(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            Vector2 StopPosition = new Vector2(-50, 0);

            bool Done = true;

            //Move the players to the right
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 playerlocation = new Vector2(Players[i].GetLocationHeight.X + Offset.X, 0);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(playerlocation.X > StopPosition.X ? -1 : 0, 0));
                if (playerlocation.X > StopPosition.X) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void ExitUp(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            Vector2 StopPosition = new Vector2(0, -50);

            bool Done = true;

            //Move the players to the right
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 playerlocation = new Vector2(0, Players[i].GetLocationHeight.Y + Offset.Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(0, playerlocation.Y > StopPosition.Y ? -1 : 0));
                if (playerlocation.Y > StopPosition.Y) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        public static void ExitDown(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            Vector2 StopPosition = new Vector2(0, Main.ScreenSize.Y + 20);

            bool Done = true;

            //Move the players to the right
            for (int i = 0; i < Players.Count; i++)
            {
                Vector2 playerlocation = new Vector2(0, Players[i].GetLocationHeight.Y + Offset.Y);

                //Players[i].DoAction(Main.GetActiveTime, Offset, TileEngine, Solids, 0, new Vector2(0, playerlocation.Y < StopPosition.Y ? 1 : 0));
                if (playerlocation.Y < StopPosition.Y) Done = false;
            }

            //If all players are at the right spot, end it
            if (Done == true)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].ChangeControl(true);

                levelentranceexit = null;
            }
        }

        protected bool IsCleared()
        {
            //If the last sublevel in the level is cleared, then the level is cleared
            if (SubLevels[SubLevels.Count - 1].GFade().IsFading() == true && SubLevels[SubLevels.Count - 1].GFade().IsFadingIn() == false)//SubLevelComplete() == true) 
                return true;

            return false;
        }

        //Checks if there is a Game Over
        protected virtual bool IsGameOver()
        {
            return (Player.Continues == 0);
        }

        protected virtual void GameOver(float activeTime, Main main)
        {
            //Play the Game Over music
            if (GameOverTime == 0f)
            {
                LoadSounds.PlayGameOverMusic();
                GameOverTime = activeTime;
            }

            //Wait until the Game Over song is over after getting a Game Over with all players before going back to the Title Screen
            if ((activeTime - GameOverTime) >= GetGameOverTime)
            {
                main.ResetLevels();
            }
        }

        //Gets the time it takes to end the game after getting a game over
        private float GetGameOverTime
        {
            get { return (LoadSounds.Songs[(int)LoadSounds.Music.GOver] != null ? (float)LoadSounds.Songs[(int)LoadSounds.Music.GOver].Duration.TotalMilliseconds + 150f : 7000f); }
        }

        //Gets the time it takes to end the level after score and everything have been calculated (the music is over)
        protected float GetLevelEndTime
        {
            get { return (LoadSounds.Songs[(int)LoadSounds.Music.LevelC] != null ? ((float)LoadSounds.Songs[(int)LoadSounds.Music.LevelC].Duration.TotalMilliseconds) : 5000f); }
        }

        protected bool LevelEnded(float activeTime)
        {
            return (activeTime - EndLevelTime) >= GetLevelEndTime;
        }

        public bool CanPause()
        {
            return (CurSubLevel.CanPause() == true && Main.CurrentFPS == Main.FPS);
        }

        public bool LevelComplete()
        {
            return LevelFinished;
        }

        //Sets a player's drop-in join state to dead (PressStart or GameOver depending on the number of remaining continues)
        public void SetDeadState(int playernum)
        {
            if (ContDisplays != null) ContDisplays[playernum].ChangeState(Player.Continues > 0 ? (int)CharInfo.LevelState.PressStart : (int)CharInfo.LevelState.GameOver);
        }

        //Increases the counter that keeps track of the number of lives a player lost; if a player is completely dead, its drop-in join status is changed
        //The counter is for Rank Mode only
        public virtual void LostLife(int index, bool completelydead)
        {
            if (completelydead == true)
            {
                SetDeadState(Players[index].GPlayerNum);
                FreeCostume(Players[index]);
            }
        }

        //Takes a costume
        private void TakeCostume(CharInfo player)
        {
            CostumesTaken[player.GetCharacterNum][player.GetCostumeNum] = true;
        }

        //Takes a costume - player
        private void TakeCostume(Player player)
        {
            CostumesTaken[player.GetCharNum][player.Alternate] = true;
        }

        //Frees up a costume
        private void FreeCostume(CharInfo player)
        {
            CostumesTaken[player.GetCharacterNum][player.GetCostumeNum] = false;
        }

        //Frees up a costume - player
        public void FreeCostume(Player player)
        {
            CostumesTaken[player.GetCharNum][player.Alternate] = false;
        }

        //Checks if a player wants to join the game and if so makes the player join
        private void DropInJoin(float activeTime)
        {
            //Don't bother checking for this information if no continues remain
            if (Player.Continues > 0)
            {
                for (int i = 0; i <= (int)PlayerIndex.Four; i++)
                {
                    //Make sure there is no player here
                    if (ContDisplays[i].GetState != (int)CharInfo.LevelState.Playing)
                    {
                        //Allow the player to join in
                        if (ContDisplays[i].GetState < (int)CharInfo.LevelState.ChooseCharacter && ContDisplays[i].GetState > (int)CharInfo.LevelState.GameOver)
                        {
                            //If the player is on "Press Start," it will go to "Continues: #," then to choosing a character to play as
                            if (ContDisplays[i].PressedPause(KeyboardState) == true)
                            {
                                ContDisplays[i].ChangeState(ContDisplays[i].GetState + 1);
                                if (ContDisplays[i].GetState == (int)CharInfo.LevelState.ChooseCharacter)
                                {
                                    ContDisplays[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, ContDisplays[i], true));
                                    TakeCostume(ContDisplays[i]);
                                }
                            }
                        }
                        //Let the player choose his or her character
                        else if (ContDisplays[i].GetState == (int)CharInfo.LevelState.ChooseCharacter)
                        {
                            //Drop out of playing by pressing the Attack key
                            if (ContDisplays[i].PressedAttack(KeyboardState) == true)
                            {
                                FreeCostume(ContDisplays[i]);
                                ContDisplays[i].ChangeState((int)CharInfo.LevelState.PressStart);
                            }
                            //Cycle through the characters with the left and right buttons
                            else if (ContDisplays[i].PressedLeft(KeyboardState) == true)
                            {
                                FreeCostume(ContDisplays[i]);
                                ContDisplays[i].DecrementCharNum();
                                ContDisplays[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, ContDisplays[i], true));
                                TakeCostume(ContDisplays[i]);
                                LoadSounds.Play(LoadSounds.Select);
                            }
                            else if (ContDisplays[i].PressedRight(KeyboardState) == true)
                            {
                                FreeCostume(ContDisplays[i]);
                                ContDisplays[i].IncrementCharNum();
                                ContDisplays[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, ContDisplays[i], true));
                                TakeCostume(ContDisplays[i]);
                                LoadSounds.Play(LoadSounds.Select);
                            }
                            //Cycle through the available costumes
                            else if (ContDisplays[i].PressedSpecAttack(KeyboardState) == true)
                            {
                                //Don't play the sound if the player wasn't able to switch costumes
                                int curcostume = ContDisplays[i].GetCostumeNum;

                                FreeCostume(ContDisplays[i]);
                                ContDisplays[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, ContDisplays[i]));
                                TakeCostume(ContDisplays[i]);
                                if (curcostume != ContDisplays[i].GetCostumeNum) LoadSounds.Play(LoadSounds.Choose);
                            }
                            //The player chose a character, so create the character!
                            else if (ContDisplays[i].PressedPause(KeyboardState) == true)
                            {
                                Player player;

                                int character = ContDisplays[i].GetCharacterNum;
                                int costume = ContDisplays[i].GetCostumeNum;

                                //Based on the value displayed, add a new character and have the player join in on the fun!
                                if (character == (int)Player.Characters.Graham) player = CreateObjects.Graham(i, costume);
                                else if (character == (int)Player.Characters.Wil) player = CreateObjects.Wil(i, costume);
                                else if (character == (int)Player.Characters.Crystal) player = CreateObjects.Crystal(i, costume);
                                else player = CreateObjects.Jeff(i, costume);

                                player.Spawn(CurSubLevel);
                                player.SpawnSub(activeTime, CurSubLevel.GetTileEngine, CurSubLevel.RespawnLoc().X, CurSubLevel.RespawnLoc().Y, 260, CurSubNum == 0);
                                player.SetStats(null, null, null, null, null, Player.RespawnInvincibility);

                                Players.Add(player);

                                //Subtract a continue
                                Player.Continues--;

                                ContDisplays[i].ChangeState((int)CharInfo.LevelState.Playing);

                                //If there are no continues left, display Game Over for everyone and set all of the join statuses to Game Over
                                if (Player.Continues <= 0)
                                {
                                    for (int k = 0; k < ContDisplays.Length; k++)
                                    {
                                        if (ContDisplays[k].GetState != (int)CharInfo.LevelState.Playing)
                                            ContDisplays[k].ChangeState((int)CharInfo.LevelState.GameOver);
                                    }
                                }
                                //Otherwise, simply change this player's join status back to Press Start
                                //else ContDisplays[i].ChangeState((int)CharInfo.LevelState.PressStart);
                            }
                        }
                    }
                }
            }

            //Update the text that blinks on and off, telling a player to join in
            if ((activeTime - PrevBlink) >= 2000f)
                PrevBlink = activeTime;
        }

        //Spawns all players at the start of the level
        public void SpawnPlayers(float activeTime)
        {
            SubLevels[CurSubNum].SpawnPlayers(activeTime, true);
        }

        //Check if every player in the game is dead
        protected bool CheckAllPlayersDead()
        {
            return (Players.Count == 0);
        }

        //Stops the level timer (Rank Mode)
        public virtual void StopTime(float activeTime)
        {
            
        }

        //Resumes the level timer (Rank Mode)
        public virtual void ResumeTime(float activeTime)
        {
            
        }

        protected void SetUpDropInJoin()
        {
            ContDisplays = new CharInfo[Player.MaxPlayers];
            CostumesTaken = new bool[Player.MaxCharacters][];

            int continues = Player.Continues > 0 ? (int)CharInfo.LevelState.PressStart : (int)CharInfo.LevelState.GameOver;
            for (int i = 0; i < ContDisplays.Length; i++)
            {
                ContDisplays[i] = new CharInfo(i, 0, 0, continues);
                CostumesTaken[i] = new bool[Player.MaxCostumes];
            }

            for (int i = 0; i < Players.Count; i++)
            {
                int playernum = Players[i].GPlayerNum;
                ContDisplays[playernum].ChangeState((int)CharInfo.LevelState.Playing);
                TakeCostume(Players[i]);
            }
        }

        //Creates the sublevels based on the level number
        protected void SubLevelCreate()
        {
            SubLevels = new List<SubLevel>();

            if (LevelNum == 0)
            {
                SubLevels.Add(new SubLevel(LevelEditor.LoadLevel("EnableTest"), this, Players));
                //SubLevels.Add(new SubLevel(LevelEditor.LoadLevelOld("EnemyTest"), Players, LoadGraphics.BG, LoadGraphics.FG, LevelNum, null, (int)Level.LevelEntrances.WalkRight, (int)Level.LevelExits.ExitLeft));
                //SubLevels.Add(new SubLevel(LevelEditor.LoadLevel("TestLevel2"), Players, LoadGraphics.BG, LoadGraphics.FG, LevelNum, null, (int)Level.LevelEntrances.WalkRight));
            }
            else if (LevelNum == 1)
            {
                SubLevels.Add(new SubLevel(LevelEditor.LoadLevel("NewMoveTest"), this, Players));
                //SubLevels.Add(new SubLevel(LevelEditor.LoadLevelOld("2-1"), this, Players, LoadGraphics.BG, LoadGraphics.FG, LevelNum, (int)Level.LevelEntrances.WalkRight));
                //SubLevels.Add(new SubLevel(LevelEditor.LoadLevelOld("2-2"), this, Players, LoadGraphics.BG, LoadGraphics.FG, LevelNum));
                //SubLevels.Add(new SubLevel(LevelEditor.LoadLevelOld("2-3"), this, Players, LoadGraphics.BG, LoadGraphics.FG, LevelNum, (int)Level.LevelEntrances.WalkRight));
            }
            else if (LevelNum == 2)
            {
                SubLevels.Add(new Sub3_4(this, Players));
            }
            else if (LevelNum == 5)
            {
                SubLevels.Add(new Sub6_3(this, Players, LoadGraphics.BG, LoadGraphics.FG));
            }
            else if (LevelNum == 6)
            {
                SubLevels.Add(new Sub7_1(this, Players, LoadGraphics.BG, LoadGraphics.FG));
            }
        }

        //Do level actions like add new players if they want to join or update the timer if in Rank Mode
        protected virtual void LevelAction(float activeTime)
        {
            //If there are fewer than 4 players playing, see if a player wants to join in or use a continue
            if (Players.Count < ((int)PlayerIndex.Four + 1))
            {
                DropInJoin(activeTime);
            }
        }

        //Moves onto the next sublevel
        protected virtual void NextSublevel(float activeTime)
        {
            //Change to the next sublevel and spawn the players in the new level
            CurSubNum++;
            CurSubLevel.SpawnPlayers(activeTime);
        }

        //Performs actions when the LevelEnd animation ends
        protected virtual void EndLevel(float activeTime, Main main)
        {
            EndLevelTime = activeTime;
        }

        //Completes the level
        protected void CompleteLevel(float activeTime, Main main)
        {
            if (LevelEnded(activeTime) == true)
                LevelFinished = true;
        }

        public void Update(float activeTime, Main main)
        {
            //Don't update anything else until the level intro ("Level X Start") is done
            if (LevelIntro == null || LevelIntro.Done() == true)
            {
                //Check if the level is cleared
                //If not, continue updating the sublevel
                if (IsCleared() == false)
                {
                    //If the game can be paused, check for pausing it, update the timer, and check if players want to join in the middle
                    if (CanPause() == true)
                    {
                        if (Main.IsPaused == true) return;

                        //Do level actions like add new players if they want to join or update the timer if in Rank Mode
                        LevelAction(activeTime);
                    }

                    //If there is still a player alive, update the sublevel
                    if (CheckAllPlayersDead() == false)
                    {
                        //Update the sublevels
                        CurSubLevel.Update(activeTime, main);

                        //If the sublevel is complete...
                        if (CurSubLevel.SubLevelComplete() == true || (CurSubNum == (SubLevels.Count - 1) && IsCleared() == true))
                        {
                            //If there are sublevels left, respawn the players for the next one, otherwise the level should end
                            if ((CurSubNum + 1) < SubLevels.Count)
                            {
                                //Move onto the next sublevel
                                NextSublevel(activeTime);
                            }
                            else
                            {
                                //Reset the player HUD depth
                                for (int i = 0; i < Players.Count; i++) Players[i].hud.SetDepth();

                                LevelOutro = new Outro(new Texture2D[] { LoadGraphics.LevelIntro, LoadGraphics.LevelNumGraphic[LevelNum], LoadGraphics.LevelClear }, (LevelNum + 1) * 1000, (Main.Difficulty + 1) * 1000, GetTimeBonus);
                                //LevelEnd.Reset(activeTime);
                                LoadSounds.PlayLevelCompleteMusic();
                            }
                        }
                    }
                    //All players are dead and no continues are left; game over
                    else if (IsGameOver() == true)
                    {
                        GameOver(activeTime, main);
                    }
                }
                //Add the remaining time to the players' score
                else
                {
                    //Update the level end animation, then give the players score based on their time if they're in Rank Mode
                    if (LevelOutro == null || LevelOutro.Done() == false)
                    {
                        if (LevelOutro != null)
                        {
                            LevelOutro.Update(activeTime, Players, KeyboardState);
                            if (SubLevels[CurSubNum].SubLevelComplete() == false) SubLevels[CurSubNum].Update(activeTime, main);
                        }
                        if (LevelOutro == null || LevelOutro.Done() == true)
                        {
                            EndLevel(activeTime, main); 
                        }
                    }
                    else
                    {
                        //Complete the level
                        CompleteLevel(activeTime, main);
                    }
                }
            }
            else if (LevelIntro != null)
            {
                LevelIntro.Update(activeTime, Players, KeyboardState);
            }

            //Make sure you don't automatically use a continue by holding Enter beforehand
            KeyboardState = Keyboard.GetState();
        }

        protected void DrawScreenStates(SpriteBatch spriteBatch)
        {
            float X = (Main.ScreenHalf.X / 2) + 50;
            float Y = Main.ScreenHalf.Y - 40;

            if (LevelIntro != null && LevelIntro.Done() == false)
                LevelIntro.Draw(spriteBatch);
            else if (IsCleared() == true && LevelOutro != null)
                LevelOutro.Draw(spriteBatch);
            else if (CheckAllPlayersDead() == true && Player.Continues == 0)
            {
                String gameover = "Game Over";
                Vector2 gameoversize = LoadGraphics.ImgFont.MeasureString(gameover);
                spriteBatch.DrawString(LoadGraphics.ImgFont, gameover, Main.ScreenHalf, Color.Yellow, 0f, (gameoversize / 2), 4f, SpriteEffects.None, .998f);
            }
        }

        //Draws additional information about a level, including time for RankLevels
        protected virtual void DrawInfo(float activeTime, SpriteBatch spriteBatch)
        {
            //Check all the missing players and show the option for joining in
            if (Players.Count < ((int)PlayerIndex.Four + 1))
            {
                Color[] Colors = new Color[] { Color.Red, Color.Blue, Color.Green, Color.Yellow };

                for (int i = 0; i <= (int)PlayerIndex.Four; i++)
                {
                    //If a player number isn't in the list, draw the join status for that player
                    if (ContDisplays[i].GetState != (int)CharInfo.LevelState.Playing)
                    {
                        if (ContDisplays[i].GetState < (int)CharInfo.LevelState.ChooseCharacter)
                        {
                            if ((activeTime - PrevBlink) <= 1500f)
                            {
                                String text;
                                if (ContDisplays[i].GetState == (int)CharInfo.LevelState.PressStart) text = " Player " + (i + 1) + "\nPress Start";
                                else if (ContDisplays[i].GetState == (int)CharInfo.LevelState.NumContinues) text = "Continues: " + Player.Continues + "\n Press Start";
                                else text = "Game Over";

                                spriteBatch.DrawString(LoadGraphics.ImgFont, text, new Vector2(PlayerHUD.GetPlayerHUDLoc(i).X - 20, PlayerHUD.GetPlayerHUDLoc(i).Y - 40), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                            }
                        }
                        //Draw the character the player is selecting
                        else if (ContDisplays[i].GetState >= (int)CharInfo.LevelState.ChooseCharacter)
                        {
                            Texture2D CharacterIcon = LoadGraphics.CharacterIcons[ContDisplays[i].GetCharacterNum];
                            String Character = Enum.GetName(typeof(Player.Characters), ContDisplays[i].GetCharacterNum);

                            Vector2 drawloc = new Vector2(PlayerHUD.GetPlayerHUDLoc(i).X + 5, PlayerHUD.GetPlayerHUDLoc(i).Y - 40);

                            spriteBatch.Draw(CharacterIcon, new Vector2(drawloc.X - CharacterIcon.Width, drawloc.Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);
                            spriteBatch.DrawString(LoadGraphics.ImgFont, Character, drawloc, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);

                            //Display the outfit number the character is wearing
                            spriteBatch.DrawString(LoadGraphics.ImgFont, "Outfit " + (ContDisplays[i].GetCostumeNum + 1), new Vector2(drawloc.X, drawloc.Y + 10), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);
                        }
                    }
                }
            }
        }

        public void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            SubLevels[CurSubNum].Draw(activeTime, spriteBatch);
            DrawScreenStates(spriteBatch);
            spriteBatch.DrawString(LoadGraphics.HUDFont, LevelName, LevelDrawLoc, Color.Blue, 0f, Vector2.Zero, .75f, SpriteEffects.None, .997f);

            DrawInfo(activeTime, spriteBatch);
        }

        //A level intro
        //For the intro, the separate words "Level X Start!" come together onto the screen (Level comes from left, X from top, and Start from right) along with other graphics that fit with the theme of the level and disperse
        //For the outro, the same thing happens, except it says "Level X Complete!" (or "Clear")
        protected class Intro
        {
            //The level text graphics
            protected Texture2D[] LevelText;

            //The current position and velocity of the level text
            protected Vector2[] CurTextLoc;
            protected Vector2[] TextVelocity;

            //The destination for the level text
            protected Vector2[] LevelTextDest;

            //The time to wait where it says "Level X Start" or
            protected float WaitTime;
            protected float PrevWait;

            //Tells if the intro or outro is completely finished or not
            protected bool Completed;

            public Intro()
            {
                WaitTime = 1000f;
                PrevWait = 0f;
                Completed = false;
            }

            //There should be 3 textures
            public Intro(Texture2D[] leveltext) : this()
            {
                LevelText = leveltext;

                Initialize();
            }

            //Sets up properties for the intro
            protected void Initialize()
            {
                //Assign the starting locations, velocities, and destinations of the graphics
                CurTextLoc = new Vector2[] { new Vector2(0 - GetGraphicSize(0).X, Main.ScreenHalf.Y - (GetGraphicSize(0).Y / 2)), new Vector2(Main.ScreenHalf.X - (GetGraphicSize(1).X / 2), -GetGraphicSize(1).Y), new Vector2(Main.ScreenSize.X, Main.ScreenHalf.Y - (GetGraphicSize(2).Y / 2)) };
                TextVelocity = new Vector2[] { new Vector2(5, 0), new Vector2(0, 5), new Vector2(-5, 0) };

                LevelTextDest = new Vector2[] { new Vector2(Main.ScreenHalf.X - GetGraphicSize(0).X - 20, Main.ScreenHalf.Y - (GetGraphicSize(0).Y / 2)), new Vector2(Main.ScreenHalf.X - (GetGraphicSize(1).X / 2), Main.ScreenHalf.Y - (GetGraphicSize(1).Y / 2)), new Vector2(Main.ScreenHalf.X + 20, Main.ScreenHalf.Y - (GetGraphicSize(2).Y / 2)) };
            }

            //Gets the size of the graphic and resorts to a default value if the graphic is null
            private Vector2 GetGraphicSize(int index)
            {
                if (LevelText != null)
                {
                    if (LevelText[index] != null)
                        return new Vector2(LevelText[index].Width, LevelText[index].Height);
                }

                return Vector2.Zero;
            }

            public virtual bool Done()
            {
                return Completed;
            }

            //Get the velocity of the graphic; if it's about to overshoot its destination, make it land exactly on it
            protected Vector2 NextVelocity(Vector2 currentloc, Vector2 destination, Vector2 velocity)
            {
                Vector2 nextvelocity = velocity;

                //Make sure it doesn't overshoot in the X
                if (Math.Abs(currentloc.X - destination.X) < Math.Abs(velocity.X))
                    nextvelocity.X = destination.X - currentloc.X;

                //Make sure it doesn't overshoot in the Y
                if (Math.Abs(currentloc.Y - destination.Y) < Math.Abs(velocity.Y))
                    nextvelocity.Y = destination.Y - currentloc.Y;

                return nextvelocity;
            }

            protected virtual void EndIntroOutro(float activeTime)
            {
                //...if we didn't wait yet after the text stopped moving, wait for a specific amount of time
                if (PrevWait == 0f)
                {
                    PrevWait = activeTime;

                    TextVelocity = new Vector2[] { -TextVelocity[0], -TextVelocity[1], -TextVelocity[2] };
                    LevelTextDest = new Vector2[] { new Vector2(0 - GetGraphicSize(0).X, Main.ScreenHalf.Y - (GetGraphicSize(0).Y / 2)), new Vector2(Main.ScreenHalf.X - (GetGraphicSize(1).X / 2), -GetGraphicSize(1).Y), new Vector2(Main.ScreenSize.X, Main.ScreenHalf.Y - (GetGraphicSize(2).Y / 2)) };
                }
                //...we waited already, so the intro or outro is done
                else Completed = true;
            }

            public virtual void Update(float activeTime, List<Player> Players, KeyboardState keyboardstate)
            {
                //Wait for the timer
                if (Completed == false && (activeTime - PrevWait) >= WaitTime)
                {
                    bool finished = true;

                    //Check to see if each graphic is in the right spot; if not, move it closer to the destination
                    for (int i = 0; i < CurTextLoc.Length; i++)
                    {
                        if (CurTextLoc[i] != LevelTextDest[i])
                        {
                            CurTextLoc[i] += NextVelocity(CurTextLoc[i], LevelTextDest[i], TextVelocity[i]);
                            finished = false;
                        }
                    }

                    //If each graphic is in the right spot...
                    if (finished == true)
                    {
                        //...check to end the intro or outro
                        EndIntroOutro(activeTime);
                    }
                }
            }

            public virtual void Draw(SpriteBatch spriteBatch)
            {
                //Don't draw if the graphics or location arrays are null
                if (LevelText != null && CurTextLoc != null)
                {
                    //Draw all the graphics in their spots
                    for (int i = 0; i < CurTextLoc.Length; i++)
                    {
                        if (LevelText[i] != null && CurTextLoc[i] != null)
                            spriteBatch.Draw(LevelText[i], CurTextLoc[i], null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                    }
                }
            }
        }

        //A level outro
        protected class Outro : Intro
        {
            //The point bonuses that players get at the end of a level
            private int ClearBonus;
            private int DifficultyBonus;
            private int TimeBonus;

            //The end location of the Bonus points' display location; this is subtracted from half of the screen size
            private const int BonusEndLoc = 100;

            //The X velocity of the Bonus points' display; how fast they move to their destination
            private const int BonusVelocity = 8;

            private Vector2 ClearLoc;
            private Vector2 DifficultyLoc;
            private Vector2 TimeLoc;

            //The timer for adding the score slowly
            private float PrevScoreAdd;

            private bool TextMove;
            private bool BonusMove;

            //Constructor; the point bonuses passed in are the ones that will be added to the players' scores at the end of the level
            //All of these should be their respective values * 1000 except for Time, which will be * 100; the Clear and Difficulty Bonus should be 1 + the level and difficulty, respectively
            public Outro(Texture2D[] leveltext, int clearbonus, int difficultybonus, int timebonus)
            {
                LevelText = leveltext;
                WaitTime = 0f;
                PrevScoreAdd = 0f;

                ClearLoc = new Vector2(BonusDestination - DestinationDistance, Main.ScreenHalf.Y - 50f);
                DifficultyLoc = new Vector2(Main.ScreenSize.X, Main.ScreenHalf.Y - 20f);
                TimeLoc = new Vector2(BonusDestination - DestinationDistance, Main.ScreenHalf.Y + 10f);

                ClearBonus = clearbonus;
                DifficultyBonus = difficultybonus;
                TimeBonus = timebonus;

                TextMove = false;
                BonusMove = false;

                Initialize();
            }

            //The distance the left (Clear and Time Bonus) and right (Difficulty Bonus) bonus text should be from the bonus destination
            private int DestinationDistance
            {
                get { return (int)(Main.ScreenSize.X - BonusDestination); }
            }

            //The destination on-screen of the bonus points
            private float BonusDestination
            {
                get { return (Main.ScreenHalf.X - BonusEndLoc); }
            }

            //Helper method to GivePlayerBonus that saves code
            private void GivePlayersPoints(List<Player> Players, int points)
            {
                for (int i = 0; i < Players.Count; i++) Players[i].AddScore(points);
            }

            //Gives the players the bonus points for completing the level
            private void GivePlayerBonus(float activeTime, ref int score, List<Player> Players, KeyboardState keyboardstate)
            {
                //Don't give points if the amount of points to give is <= 0
                if (score > 0)
                {
                    //Add the score immediately if a player presses the pause button
                    if (Input.CheckPlayerPress(keyboardstate, Players, (int)Player.Action.Pause) == true)
                    {
                        GivePlayersPoints(Players, score);

                        score -= score;
                    }
                    //Otherwise, wait for the timer and add the score
                    else if ((activeTime - PrevScoreAdd) >= 45f)
                    {
                        int points = 100;

                        GivePlayersPoints(Players, points);

                        LoadSounds.Play(LoadSounds.ScoreAdd);
                        score -= points;
                        if (score > 0) PrevScoreAdd = activeTime;
                    }
                }
            }

            protected override void EndIntroOutro(float activeTime)
            {
                //If we didn't wait yet after the text stopped moving, wait for a specific amount of time
                if (PrevWait == 0f)
                {
                    PrevWait = activeTime;

                    //Make the text move up to put the Clear Bonus, Level Bonus, and Time Bonus underneath
                    for (int i = 0; i < LevelTextDest.Length; i++)
                    {
                        TextVelocity[i] = new Vector2(0, -3);
                        LevelTextDest[i] = new Vector2(LevelTextDest[i].X, LevelTextDest[i].Y - 90f);
                    }
                }
                //...we waited already, so the first part of the outro is done
                else BonusMove = true;
            }

            public override void Update(float activeTime, List<Player> Players, KeyboardState keyboardstate)
            {
                //Wait for the timer
                if (BonusMove == false)
                {
                    bool finished = true;

                    //Check to see if each graphic is in the right spot; if not, move it closer to the destination
                    for (int i = 0; i < CurTextLoc.Length; i++)
                    {
                        if (CurTextLoc[i] != LevelTextDest[i])
                        {
                            CurTextLoc[i] += NextVelocity(CurTextLoc[i], LevelTextDest[i], TextVelocity[i]);
                            finished = false;
                        }
                    }

                    //If each graphic is in the right spot...
                    if (finished == true)
                    {
                        //...check to end the intro or outro
                        EndIntroOutro(activeTime);
                    }
                }
                //Move the bonus text
                else if (TextMove == false)
                {
                    Vector2 cleardest = new Vector2(BonusDestination, ClearLoc.Y);
                    Vector2 diffdest = new Vector2(BonusDestination, DifficultyLoc.Y);
                    Vector2 timedest = new Vector2(BonusDestination, TimeLoc.Y);

                    ClearLoc += NextVelocity(ClearLoc, cleardest, new Vector2(BonusVelocity, 0));
                    DifficultyLoc += NextVelocity(DifficultyLoc, diffdest, new Vector2(-BonusVelocity, 0));
                    TimeLoc += NextVelocity(TimeLoc, timedest, new Vector2(BonusVelocity, 0));

                    if (ClearLoc == cleardest && DifficultyLoc == diffdest && TimeLoc == timedest)
                        TextMove = true;
                }
                //Give players their bonus points
                else if (BonusMove == true && TextMove == true && Completed == false)
                {
                    //Start with the Clear Bonus, then move onto the Difficulty Bonus, then end with the Time Bonus
                    if (ClearBonus > 0) GivePlayerBonus(activeTime, ref ClearBonus, Players, keyboardstate);
                    else if (DifficultyBonus > 0) GivePlayerBonus(activeTime, ref DifficultyBonus, Players, keyboardstate);
                    else
                    {
                        GivePlayerBonus(activeTime, ref TimeBonus, Players, keyboardstate);
                        if (TimeBonus <= 0) Completed = true;
                    }
                }
            }

            public override void Draw(SpriteBatch spriteBatch)
            {
                base.Draw(spriteBatch);

                String clearstring = "Clear Bonus: " + String.Format("{000000:0}", ClearBonus);
                String difficultystring = "Difficulty Bonus: " + String.Format("{000000:0}", DifficultyBonus);
                String timestring = "Time Bonus: " + String.Format("{000000:0}", TimeBonus);

                spriteBatch.DrawString(LoadGraphics.HUDFont, clearstring, ClearLoc, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                spriteBatch.DrawString(LoadGraphics.HUDFont, difficultystring, DifficultyLoc, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                if (TimeBonus >= 0) spriteBatch.DrawString(LoadGraphics.HUDFont, timestring, TimeLoc, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            }
        }
    }
}