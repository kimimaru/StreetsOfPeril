﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //(Put on hold until you get player feedback): Vary Weapon attacks! Make jump attacks with some weapons and maybe even change Special Attacks based on the weapon you have!
    //NOTE: Weapons do too much in their Update() methods when held right now...just draw them at a specified location when held and change their heights when the weapon is dropped or thrown
    public class Weapon : PickupObj
    {
        //NOTE: Remove the "None" WeaponType during or after refactoring, since Wearable Weapons have been scrapped
        public enum WeaponTypes
        {
            None, Stab, Swing
        };

        //The Weapon's sprite
        protected AnimFrame WeaponSprite;

        //Weapon properties
        protected int TimesLanded;
        protected int Range;

        //The max number of times a weapon can land; this can vary between weapons and weapon types
        protected int MaxTimesLand;

        //Swingable property of the weapon - 0 = none, 1 = stab (knife), 2 = swing; None, or Wearable, weapons can also pass their status effects onto enemies
        protected WeaponTypes WeaponType;

        protected bool Thrown;
        protected bool Disappear;

        //The sounds played when the weapon is used (swung or stabbed with) and when the weapon hits something
        protected SoundEffect WeaponUseSound;
        protected SoundEffect WeaponHitSound;

        //How long the weapon stays out before it disappears on the floor after it gets knocked out of that character's hand
        protected float WeaponDur;
        protected float PrevWeaponDur;

        //The previous fighter that held the weapon (reset upon landing)
        protected Fighter PrevFighter;

        //Constructor
        protected Weapon()
        {
            ObjType = ObjectType.Weapon;
            StatForever = true;

            //Default icon and spritesheets; default to Knife
            Icon = LoadGraphics.WeaponIcons[(int)WeaponTypes.Stab];
            SpriteSheet = LoadGraphics.WeaponSprite[(int)WeaponTypes.Stab];
            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            WeaponType = WeaponTypes.None;

            Thrown = false;
            Disappear = false;
            FacingRight = false;

            TimesLanded = 0;
            MaxTimesLand = 5;
            PrevWeaponDur = 0f;

            ClearRotation();

            hud = new WeaponHUD(this);
        }

        public Weapon(String name, float damageadder, int range, Vector2 location, Vector2 velocity, float weapondur, Status stat, int swingable, SoundEffect weaponusesound, SoundEffect weaponhitsound, bool facingright = true) : this()
        {
            Location = new Vector3(location, 0f);
            Velocity = velocity;

            Name = name;
            SpriteSheet = LoadGraphics.WeaponSprite[swingable];
            Icon = LoadGraphics.WeaponIcons[swingable];
            WeaponSprite = new AnimFrame(new Rectangle(0, 0, SpriteSheet.Width, SpriteSheet.Height), 0f);

            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            SetUpHurtbox();
            SetUpHealth(0);

            WeaponType = (WeaponTypes)swingable;
            FacingRight = facingright;
            Damage = (int)damageadder;
            Range = range;

            WeaponUseSound = weaponusesound;
            WeaponHitSound = weaponhitsound;

            WeaponDur = weapondur;

            status = stat;
            
            //hud = new HUD(LoadGraphics.WeaponIcons[Swingable]);
        }

        //Copy constructor for Onslaught
        public Weapon(Weapon oldweapon) : this()
        {
            this.Location = oldweapon.Location;
            this.Velocity = oldweapon.Velocity;

            this.Name = oldweapon.Name;
            this.SpriteSheet = oldweapon.SpriteSheet;
            this.Icon = oldweapon.Icon;
            this.WeaponSprite = oldweapon.WeaponSprite;
            this.ObjectLength = oldweapon.ObjectLength;
            this.ObjectHeight = oldweapon.ObjectHeight;

            this.SetUpHurtbox();
            this.SetUpHealth(0);

            this.WeaponType = oldweapon.WeaponType;
            this.Damage = oldweapon.Damage;
            this.Range = oldweapon.Range;

            this.WeaponUseSound = oldweapon.WeaponUseSound;
            this.WeaponHitSound = oldweapon.WeaponHitSound;

            this.WeaponDur = oldweapon.WeaponDur;

            this.status = new Status(oldweapon.status.GCond, oldweapon.status.GStatusDur);

            this.hud = new WeaponHUD(this);
        }

        //The possible statuses a Knife can have
        public static Status[] KnifeStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DefenseDown, 10000f, 35), new Status((int)Status.Statuses.Poison, 2000f, 25) }; }
        }

        //The possible statuses a Katana can have
        public static Status[] KatanaStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DefenseDown, 9000f, 50), new Status((int)Status.Statuses.NoJump, 10000f, 25) }; }
        }

        //The possible statuses the WeaponWielders' signature weapon, the Swift Blade, can have
        public static Status[] SwiftBladeStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.SpeedDown, 12000f, 60), new Status((int)Status.Statuses.NoGrab, 10000f, 40) }; }
        }

        //Weapons don't have health, so they're dead when they're set to disappear
        public sealed override bool IsDead
        {
            get { return Disappear; }
        }

        //Gets how many more times a Weapon can land
        public int GetLandsRemaining
        {
            get { return (MaxTimesLand - TimesLanded); }
        }

        public float FallVelocityX
        {
            get
            {
                return (ForwardVal(TrueVelocity.X));
            }
        }

        public SoundEffect GWeaponUseSound
        {
            get { return WeaponUseSound; }
        }

        public SoundEffect GWeaponHitSound
        {
            get { return WeaponHitSound; }
        }

        public bool GDisappear
        {
            get { return Disappear; }
        }

        public bool IsThrown
        {
            get { return Thrown; }
        }

        public int GetWeaponType
        {
            get { return (int)WeaponType; }
        }

        public int GetRange
        {
            get { return Range; }
        }

        public override Rectangle FeetLoc
        {
            get
            {
                /*if (IsPickedUp == true) return Fighter.FeetLoc;
                 else return base.FeetLoc;*/
                if (IsPickedUp == true)
                {
                    return fighter.FeetLoc;
                }
                else return base.FeetLoc;
            }
        }

        //Determines if the object holding the weapon should drop the weapon based on how long the weapon was held
        public bool ShouldDropWeapon(float activeTime)
        {
            return (Main.GetActiveTime >= PrevWeaponDur);
        }

        //Checks if a weapon can be stolen
        //public bool CanBeStolen(Weapon enemyweapon)
        //{
        //    //The Weapon Wielders cannot steal a wearable weapon like Brass Knuckles
        //    return (Swingable != (int)Weapon.WeaponTypes.None && enemyweapon == null);
        //}

        //When the player's weapon gets stolen by the Weapon Wielders (Gerald/Kate) - the move is performed after the Weapon Wielder grabs the player
        public static void EnemyStealWeapon(Player player, Enemy enemystealer, SubLevel level)
        {
            //For this to work, the player must have a weapon and the enemy must not
            if (player.weapon != null && enemystealer.weapon == null)
            {
                //The Weapon Wielder took the player's weapon, so the player no longer has a weapon
                enemystealer.SetWeapon(player.weapon);
                player.weapon.Drop();
                enemystealer.weapon.GetPickedUp(enemystealer);
            }
        }

        //Tells the weapon to bounce off an enemy or object it hits when it's thrown
        public void Bounce()
        {
            Thrown = false;

            FlipDirection();
            SetAirVelocity(new Vector3((int)-(AirVelocity.X / 2f), 0f, AirVelocity.Z));
            UseGravity = true;
            ClearHitboxes();
        }

        protected override void MovingActions()
        {
            RotateWeapon();
        }
         
        //Rotates the weapon when it is in the air
        protected void RotateWeapon()
        {
            float rotationdegrees = 22;
            if (IsInWater == true) rotationdegrees = 11;
         
            //Rotate the weapon counter-clockwise when facing left and clockwise when facing right 
            Rotate(false, rotationdegrees);
        }

        protected override void LandActions()
        {
            Thrown = false;
            ClearRotation();
            ClearHitboxes();

            //If the weapon had an owner...
            if (PrevFighter != null)
            {
                //If the weapon didn't land enough times yet, increment the number of times it landed and check to see if it should disappear
                if (TimesLanded < MaxTimesLand)
                {
                    //Increment the number of times the Weapon landed
                    TimesLanded++;

                    //If the weapon was dropped, switched, or hit out the max number of times total (usually 5), make it disappear
                    if (TimesLanded >= MaxTimesLand)
                        Die();
                }

                //Clear the previous Fighter that was holding this weapon
                PrevFighter = null;
            }
        }

        protected override void BeginFall()
        {
            base.BeginFall();

            //If the Weapon started falling while it was dead, restart its hud again
            if (IsDead == true) hud.Restart();
        }

        //Have the Fighter pick up the object
        public override void GetPickedUp(Fighter fight)
        {
            base.GetPickedUp(fight);

            PrevFighter = fight;

            PrevWeaponDur = Main.GetActiveTime + WeaponDur;
            Location.Z = fighter.MaxHeight;

            UseGravity = false;

            Location.Z = fighter.MaxHeight;
        }

        protected override void HitScreenX()
        {
            if (Thrown == true) Bounce();
        }

        protected override void HitScreenY()
        {
            if (Thrown == true) Bounce();
        }

        protected override void HitWallX()
        {
            if (Thrown == true) Bounce();
            //Bounce();
        }

        protected override void HitWallY()
        {
            if (Thrown == true) Bounce();
        }

        protected override void HitSolidX()
        {
            if (Thrown == true) Bounce();
        }

        protected override void HitSolidY()
        {
            if (Thrown == true) Bounce();
        }

        protected override void FallingActions()
        {
            //Rotate the Weapon if it's in the air and isn't moving
            if (GetAirVelocityVec2 == Vector2.Zero)
                RotateWeapon();
        }

        //Weapons cannot hurt their owners
        public override bool CanDamageObject(BeatEmUpObj victim)
        {
            return (base.CanDamageObject(victim) == true && HasHitboxes == true && victim != PrevFighter);
        }

        public override void DamageObject(BeatEmUpObj victim, Hitbox hitbox)
        {
            base.DamageObject(victim, hitbox);
            
            //If the Weapon was thrown and hits something, make it bounce off whatever it hit
            //NOTE: Even though we clear the hitboxes here, we still have a reference to it in our DamageCollision object
            //Find a way to prevent this; the quick fix right now is checking if Hitboxes.Count > 0 in CanDamageObject
            //This'll work fine, but there may be a more elegant solution
            if (IsPickedUp == false) Bounce();
            
            //If the Weapon was used by a Player, check if the Player can set its InteractionHUD to the object's HUD and do so
            if (PrevFighter != null && PrevFighter.ObjType == ObjectType.Player && victim.CanSetHUD() == true)
                PrevFighter.SetInteractionHUD(victim.hud);
        }
         
        //Called when the Weapon is hit out of a Fighter's hands
        public void HitOut()
        {
            //Make the Weapon fly in the opposite direction the Fighter is facing
            SetAirVelocity(new Vector3(ForwardVal(-TrueVelocity.X), 0f, 0f));
            ClearHitboxes();
         
            //Drop the weapon normally
            Drop();
        }
         
        //Called when the Weapon is dropped by a Fighter, whether manually or hit out
        public void Drop()
        {
            ClearFighter();
            UseGravity = true;

            PrevFighter.ClearWeapon();
            Location.Z = PrevFighter.MaxHeight - 50;
        }

        public override bool CanBePickedUp(Fighter fight)
        {
            return (PrevFighter == null && base.CanBePickedUp(fight) == true);
        }

        //Send the weapon flying forward (maybe at an arc or other angle for different characters) and make it bounce off whatever it hits
        //Unlike other games, the weapon won't disappear immediately, but the counter for the # of times it landed will increase
        public void Throw()
        {
            //If the Weapon is not in water, create a hitbox and make it ignore gravity so it can fly forwards without being affected by it
            if (IsInWater == false)
            {
                Rectangle feetloc = FeetLoc;

                //For now just create a hitbox surrounding the Weapon; later we'll elaborate on how the hitbox should be
                CreateHitboxSurrounding(Damage, ObjectHeight, 0f, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, FacingRight, WeaponHitSound, true);

                Thrown = true;
            }
            else
            {
                UseGravity = true;
            }

            //Drop the weapon, detaching it from its Fighter
            ClearFighter();

            PrevFighter.ClearWeapon();

            //Set the weapon's velocity
            SetAirVelocity(new Vector3(FallVelocityX, 0f, 0f));

            Location.Z = PrevFighter.MaxHeight - 50f;
        }

        //Switch when picking up another weapon
        public void Switch(Weapon newweapon)
        {
            ClearFighter();

            SetLocation(newweapon.GetLocation);

            //Set the tile and current object to the Weapon's
            ObjectTile = newweapon.ObjectTile;
            ObjectLand(newweapon.ObjectOn);
        }

        //Creates a hitbox for the weapon - for use by objects that use weapons (Ex. Enemy or Player swinging the weapon)
        //horizontal as true means the hitbox is longer in the X direction (imagine a sword pointed to the left towards an X of 0), and horizontal as false means the hitbox is in the Y direction (imagine a sword pointed up towards a Y of 0)
        //When not null, heightdiff acts as an upwards or downwards hitbox for higher hitboxes, like when an enemy would hold a sword up with both hands in front of him or an upward thrust when the sword moves above his head (the hitbox is small, but it is very high)
        public void SetHitbox(Vector2 hitlocation, float startingheight, float activationtime, float duration, bool knockdown, SoundEffect weaponswing, bool horizontal, float? heightdiff = null)
        {
            //The hitbox length and width differ depending on the orientation of the weapon (Ex. horizontal or vertical swing)
            int hitboxlength;
            int hitboxwidth;
            float heightdifference;

            //High hitbox, so use the bottom of the weapon for the width and height (Ex. flat part at the bottom of the hilt of a sword)
            if (heightdiff != null)
            {
                hitboxlength = 20;
                hitboxwidth = 20;
                heightdifference = (float)heightdiff;
            }
            //Otherwise, its a horizontal swing, so the length of the hitbox should be the range of the weapon
            else if (horizontal == true)
            {
                hitboxlength = Range;
                hitboxwidth = 20;
                heightdifference = hitboxwidth;
            }
            //Otherwise, it's a vertical swing, so the width of the hitbox should be the range of the weapon
            else
            {
                hitboxlength = 20;
                hitboxwidth = Range;
                heightdifference = hitboxlength;
            }
        
            //Hitbox = new Hitbox((int)hitlocation.X, (int)hitlocation.Y, hitboxlength, hitboxwidth, startingheight + heightdifference, (int)heightdifference, (int)Damage, activationtime, duration, knockdown, !FacingRight, WeaponHitSound, 70);
            //AddHitbox(new Hitbox((int)hitlocation.X, (int)hitlocation.Y, hitboxlength, hitboxwidth, startingheight + heightdifference, (int)heightdifference, (int)Damage, activationtime, duration, knockdown, !FacingRight, WeaponHitSound, 70));
            //Play a custom sound for that swing; if it's null, play the weapon's swing sound instead
            if (weaponswing == null && IsPickedUp == true) LoadSounds.Play(WeaponUseSound);
        }

        public void SetHitbox(float activeTime, Vector2 HitLocation, float activationtime, float duration, SoundEffect weaponswing)
        {
            //Hitbox = new Hitbox((int)HitLocation.X, (int)HitLocation.Y, ObjectLength, ObjectHeight, MaxHeight, ObjectHeight, (int)Damage, activationtime, duration, true, !FacingRight, WeaponHitSound, 70);
            //AddHitbox(new Hitbox((int)HitLocation.X, (int)HitLocation.Y, ObjectLength, ObjectHeight, MaxHeight, ObjectHeight, (int)Damage, activationtime, duration, true, !FacingRight, WeaponHitSound, 70));
            if (weaponswing != null) LoadSounds.Play(WeaponUseSound);
        }

        public void SetHitbox(float activeTime, Vector2 HitLocation, Vector2 HitSize, float maxheight, float activationtime, float duration, SoundEffect weaponswing)
        {
            //Hitbox = new Hitbox((int)HitLocation.X, (int)HitLocation.Y, (int)HitSize.X, (int)HitSize.Y, maxheight, ObjectHeight, (int)Damage, activationtime, duration, true, !FacingRight, WeaponHitSound, 70);
            //AddHitbox(new Hitbox((int)HitLocation.X, (int)HitLocation.Y, (int)HitSize.X, (int)HitSize.Y, maxheight, ObjectHeight, (int)Damage, activationtime, duration, true, !FacingRight, WeaponHitSound, 70));
            if (weaponswing != null) LoadSounds.Play(WeaponUseSound);
        }

        public void SetHitbox(Hitbox hitbox, SoundEffect weaponswing)
        {
            AddHitbox(hitbox);
            if (weaponswing != null) LoadSounds.Play(weaponswing);
        }

        protected override void HitLagActions(float diff)
        {
            base.HitLagActions(diff);

            if (IsPickedUp == true)
            {
                GetOwner.EnterHitLag();

                //Delay hitboxes
                for (int i = 0; i < Hitboxes.Count; i++) 
                    Hitboxes[i].DelayHitbox(IsInWater, diff);
            }
        }

        public sealed override void Die()
        {
            base.Die();

            TimesLanded = MaxTimesLand;
            Disappear = true;

            //Give the weapon the Invincible status so it blinks
            InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));
        }

        protected sealed override void ObjectUpdate()
        {
            //If the weapon isn't picked up, check if it should be moving
            if (IsPickedUp == false)
            {
                //If the weapon is in the air and has an aerial velocity, make it move
                if (IsInAir == true && AirVelocity.X != 0f)
                    ObjectMove(new Vector2(AirVelocity.X, 0));

                //If the weapon isn't dead, check if it's offscreen
                if (IsDead == false && IsOffScreenRange(SubLvl.GCameraOffSet, (int)Main.ScreenSize.X * 2, (int)Main.ScreenSize.Y * 2) == true)
                {
                    Die();

                    //Don't draw the Weapon if it died from being too far offscreen
                    DrawEnabled = false;
                }
            }
            else
            {
                //Make the weapon follow the fighter
                Location.X = fighter.GetLocation.X;
                Location.Y = fighter.FeetLoc.Center.Y;
                Location.Z = fighter.MaxHeight - 50f;

                ChangeDirection(fighter.FacingRight);

                ObjectTile = SubLvl.GetTileEngine.CurTile(FeetLoc);
            }
        }

        protected void DrawAnimations(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            //Don't draw the weapon if it's invisible when invincible
            if (status.GStatusColor.A != 0)
            {
                Vector3 temploc = Vector3.Zero;
                Vector2 DrawLocation;

                if (IsPickedUp == true)
                {
                    //temploc = GetPlayer() != null ? fighter.WeaponLoc(!fighter.GetFacingDir, ObjectLength) : GetEnemy().WeaponLoc(FacingRight, ObjectLength);
                    
                    //Set the draw location so that the character holds the bottom of the weapon, regardless of how long the weapon is
                    DrawLocation = new Vector2(Location.X + temploc.X + (float)Math.Ceiling((double)ObjectLength / 2), fighter.CurHeight + (temploc.Y + (float)Math.Ceiling((double)ObjectHeight / 2)));
                    Rotation = temploc.Z;
                }
                else
                {
                    DrawLocation = new Vector2(Location.X, Location.Z);
                }

                float depth = GetDrawDepth(cameraloc);

                //Change the depth the weapon is drawn at to be the depth of the object holding if it's picked up
                if (IsPickedUp == true)
                {
                    depth = fighter.GetDrawDepth(cameraloc) + .001f;
                }
                else if (IsOnGround == true) depth = .00001f;

                WeaponSprite.Draw(spriteBatch, SpriteSheet, GetDrawLoc(cameraloc), !FacingRight, status.GStatusColor, GetRotation, depth);
            }
        }

        protected sealed override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            DrawAnimations(spriteBatch, cameraloc, TileEngine);
        }

        protected sealed override bool ShouldDrawShadow()
        {
            return (base.ShouldDrawShadow() == true && IsPickedUp == false);
        }

        public override string ToString()
        {
            return (Name + "," + status.ToString());
        }
    }
}