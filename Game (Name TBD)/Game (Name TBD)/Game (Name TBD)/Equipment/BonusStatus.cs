﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Player's Bonus Status Meter; it tracks the possible statuses of enemies the player killed and gives the player a positive status once the meter reaches 100
    public class BonusStatus
    {
        //The maximum value of the bonus status meter
        public const int MaxStatusMeter = 100;

        //The threshold for the Bonus Status icon to start blinking
        private const int BlinkThreshold = 80;

        //The amount to add to the bonus status meter when kiling an enemy
        public const int AmountAdded = 10;

        //The amount to subtract from the bonus status meter when taking damage
        public const int AmountLost = -2;

        //The Player reference
        private Player player;

        //The Bonus Status Meter's numerical value corresponding to how much of it is filled
        private int StatusMeter;

        //Whether the Bonus Status Meter is updated or not
        private bool MeterOn;

        //Tells if the player currently has an Infinite status or not; in this case - the Bonus Status Meter must be turned off
        private bool InfStatus;

        //The list of possible statuses obtained from the enemies
        private int[] StatusesObtained;

        //For drawing the Bonus Status icon on the player HUD - at 80, it starts showing up, blinking slowly; at 90, it blinks faster; at 100, it no longer blinks
        //The icon shows up for only half of these values, since the other half is used to make it not show up and thus blink at even intervals
        private const float SlowBlink = 400f;
        private const float FastBlink = 100f;
        private float PrevBlink;

        public BonusStatus(Player play)
        {
            player = play;
            InfStatus = player.status.IsInfinite;

            StatusMeter = 0;

            MeterOn = !InfStatus;

            //This will be the length of all the Statuses, but only some will be used since the negative statuses won't be used
            StatusesObtained = new int[Enum.GetValues(typeof(Status.Statuses)).Length];
        }

        //Gets how fast the icon is blinking (for drawing it in the HUD)
        private float CurBlinkRate
        {
            get
            {
                if (StatusMeter == MaxStatusMeter) return 0f;
                else if (StatusMeter >= (BlinkThreshold + 10)) return FastBlink;

                return SlowBlink;
            }
        }

        //Tells if the Bonus Status should be drawn on the HUD or not
        public bool ShouldDraw
        {
            get { return (StatusMeter == MaxStatusMeter || (StatusMeter >= BlinkThreshold && (Main.GetActiveTime - PrevBlink) < (CurBlinkRate / 2f))); }
        }

        //Tells if the Bonus Status Meter is activated or not
        public bool Activated
        {
            get { return MeterOn; }
        }

        //Reactivates the Bonus Status
        public void Reactivate()
        {
            MeterOn = true;
        }

        //Deactivates the Bonus Status
        public void Deactivate()
        {
            MeterOn = false;
        }

        //Resets the status meter and the statuses obtained (upon death)
        public void ResetStatusMeter()
        {
            StatusMeter = 0;

            //Reset the statuses obtained
            for (int i = 0; i < StatusesObtained.Length; i++)
                StatusesObtained[i] = 0;
        }

        //Increases or decreases the bonus status meter
        public void ChangeBonusMeter(int amount)
        {
            //Change the Status Meter only if it's on
            if (MeterOn == true)
            {
                StatusMeter += amount;

                if (StatusMeter < 0) StatusMeter = 0;
                else if (StatusMeter > MaxStatusMeter) StatusMeter = MaxStatusMeter;
            }
        }

        //Increases the chance of obtaining a particular status based on the possible statuses the enemy killed has
        public void IncreaseChanceStatus(Status[] enemystatuses, int enemystartingstatus, int enemykilledstatus)
        {
            /*CHOICE ORDER:
               1. If the enemy had a status right before dying, increase the positive of that one; if there is no positive equivalent, go to 2.
               2. If the enemy spawned with a status, increase the positive of that one; if there is no positive equivalent to the status, go to 3
               3. If the enemy didn't spawn with a status:
                  3.1. If the enemy has the potential to spawn with a status, go to 3.2, else go to 4
                  3.2. If the enemy has a positive or positive equivalent to at least one of its potential statuses, increase a random one among the ones that are positive or have positive equivalents, else go to 4
               4. If the enemy doesn't have a possible status to spawn with, choose a random one among the following statuses: DamageBoost, DefenseBoost, SpeedBoost
             */

            //Check if we can increase the chance status (the Bonus Status Meter is enabled or not and the Status Meter is less than its max value)
            if (CanIncreaseChanceStatus() == true)
            {
                //Start with case 1; check if the enemy had a status right before dying
                switch (enemykilledstatus != (int)Status.Statuses.None)
                {
                    //The enemy had a status when it died
                    case true:
                        Status statuscheck = new Status(enemykilledstatus, 0, 0);

                        //If the status is positive
                        if (statuscheck.IsPositiveStatus() == true)
                        {
                            StatusesObtained[statuscheck.GCond]++;
                        }
                        //The status isn't positive, so check its inverse
                        else
                        {
                            statuscheck = Status.Invert(statuscheck);

                            //If its inverse is positive
                            if (statuscheck.IsPositiveStatus() == true)
                                StatusesObtained[statuscheck.GCond]++;
                            //Otherwise there's no positive or positive equivalent, so go to case 2
                            else goto case false;
                        }
                        
                        break;
                    //The enemy didn't have a status right before it died; case 2
                    case false:
                        //Next is case 2; check if the enemy spawned with a status
                        switch (enemystartingstatus != (int)Status.Statuses.None)
                        {
                            //The enemy spawned with a status
                            case true:
                                Status posstatuscheck = new Status(enemystartingstatus, 0, 0);

                                //If the status is positive
                                if (posstatuscheck.IsPositiveStatus() == true)
                                {
                                    StatusesObtained[posstatuscheck.GCond]++;
                                }
                                //The status isn't positive, so check its inverse
                                else
                                {
                                    posstatuscheck = Status.Invert(posstatuscheck);

                                    //If it's inverse is positive
                                    if (posstatuscheck.IsPositiveStatus() == true)
                                        StatusesObtained[posstatuscheck.GCond]++;
                                    //Otherwise there's no positive or positive equivalent, so go to case 3
                                    else goto case false;
                                }

                                break;
                            //The enemy didn't spawn with a status; case 3
                            case false:
                                //Case 3.1; see if the enemy has the potential to spawn with a status
                                switch (enemystatuses != null && enemystatuses.Length > 0)
                                {
                                    //The enemy does have the potential to spawn with a status; case 3.2
                                    case true:
                                        //Create a temporary list to store the positive or positive-equivalent statuses the enemy can spawn with
                                        List<int> potentialstatuses = new List<int>();

                                        //Check for any potential statuses that are positive or have positive equivalents
                                        for (int i = 0; i < enemystatuses.Length; i++)
                                        {
                                            Status newposstatuscheck = new Status(enemystatuses[i].GCond, 0, 0);

                                            //Check for positive; if so, add to the list of positive statuses to choose from
                                            if (newposstatuscheck.IsPositiveStatus() == true)
                                            {
                                                potentialstatuses.Add(newposstatuscheck.GCond);
                                            }
                                            //The status isn't positive, so check if its inverse is positive
                                            else
                                            {
                                                newposstatuscheck = Status.Invert(newposstatuscheck);

                                                //If it's inverse is positive, add to the list of positive statuses to choose from
                                                if (newposstatuscheck.IsPositiveStatus() == true)
                                                    potentialstatuses.Add(newposstatuscheck.GCond);
                                            }
                                        }

                                        //Choose a random status out of the possible ones
                                        if (potentialstatuses.Count > 0)
                                        {
                                            int statuschosen = potentialstatuses[new Random().Next(0, potentialstatuses.Count)];

                                            StatusesObtained[statuschosen]++;
                                        }
                                        //If none of the possible statuses are positive or have positive equivalents, go to case 4
                                        else goto case false;

                                        break;
                                    //Case 4; the enemy did not spawn with a status and does not have the potential to spawn with any statuses
                                    case false:
                                        //Create a temporary array of the choices at this point and choose a random one to increase
                                        int[] randompositivestatus = new int[] { (int)Status.Statuses.DamageBoost, (int)Status.Statuses.DefenseBoost, (int)Status.Statuses.SpeedBoost };

                                        int newstatuschosen = randompositivestatus[new Random().Next(0, randompositivestatus.Length)];

                                        StatusesObtained[newstatuschosen]++;
                                        break;
                                }

                                break;
                        }
                        break;
                }
            }
        }

        public bool CanIncreaseChanceStatus()
        {
            return (MeterOn == true && StatusMeter < MaxStatusMeter);
        }

        public bool CanGiveBonusStatus()
        {
            return (MeterOn == true && StatusMeter == MaxStatusMeter && player.status == (int)Status.Statuses.None);
        }

        //Gives the Player a Bonus Status once the StatusMeter is on, full, and the Player doesn't currently have a status
        public void GiveBonusStatus()
        {
            //If the player can receive the status, give it to the player
            if (CanGiveBonusStatus() == true)
            {
                /*SELECTION PROCESS:
                   1. Calculate the total of all statuses obtained
                   2. Find the total percentage of each status:
                      2.1. Divide the number of statuses obtained from that status by the total of all statuses; this will add up to 100%
                   3. Choose a status based on the CUMULATIVE percentages - higher percentages have a better chance of being chosen (some code and statistics knowledge taken from here: http://www.vcskicks.com/random-element.php)
                   (Ex. killing 2 Christophers and 1 Greg results in a 67% chance of getting DamageBoost and a 33% chance of getting SpeedBoost)
                 */

                //The total statuses accumulated
                int totalstatuses = StatusesObtained.Sum();

                //The individual percentages (they go from 0 to 1 and all add up to 1)
                double[] statuspercentages = new double[StatusesObtained.Length];

                //Calculate the percentages
                for (int i = 0; i < StatusesObtained.Length; i++)
                {
                    //Keep the values as doubles for accuracy
                    statuspercentages[i] = (StatusesObtained[i] / (double)totalstatuses);
                }

                //Choose the status
                int receivedstatus = Helper.ChooseValuePercentage(statuspercentages);

                //Precautionary measures - in the event that a status isn't properly chosen for some reason, just choose a random status out of the possible ones in the game
                if (receivedstatus < 0) receivedstatus = new Random().Next(0, StatusesObtained.Length);

                //Give the player the status effect and reset the Status Meter
                player.SetStats(null, null, null, null, null, new Status(receivedstatus, 10000f, 0));
                ResetStatusMeter();
                Reactivate();

                LoadSounds.Play(LoadSounds.GetBonusStatus);
            }
        }

        public void Update(float activeTime)
        {
            //Check for giving the Bonus Status
            if (CanGiveBonusStatus() == true)
                GiveBonusStatus();

            //Check if the player isn't dead and just got an infinite status or just stopped having one and deactivate or reactivate the Bonus Status Meter, respectively
            if (player.IsDead == false && InfStatus == false && player.status.IsInfinite == true)
            {
                ResetStatusMeter();
                Deactivate();
            }
            else if (InfStatus == true && player.status.IsInfinite == false)
                Reactivate();

            //Update the infinite status check
            InfStatus = player.status.IsInfinite;

            //Update the timers for drawing the Bonus Status icon
            if (StatusMeter >= BlinkThreshold)
            {
                if ((activeTime - PrevBlink) >= CurBlinkRate)
                    PrevBlink = activeTime;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (StatusMeter == MaxStatusMeter || (StatusMeter >= BlinkThreshold && (Main.GetActiveTime - PrevBlink) < (CurBlinkRate / 2f)))
            {
                //player.GetHUD.DrawBonusStatus(spriteBatch, true);
            }
        }
    }
}
