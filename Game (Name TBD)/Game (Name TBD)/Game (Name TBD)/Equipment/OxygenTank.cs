﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An oxygen tank that characters and enemies wear underwater - the tank can be broken, resulting in the character losing a life or the enemy dying; there are ones with different colored straps (for characters only right now; maybe enemies later)!
    //NOTE: To make things easier, we may need to have separate animations for characters and enemies with the oxygen tanks and goggles on
    public sealed class OxygenTank : BeatEmUpObj
    {
        //The maximum number of hits an Oxygen Tank can take
        public const int MaxHits = 5;

        //The time before the oxygen tank can get hit again since the last hit
        private float PrevHit;

        //The Oxygen Tank's sprite
        private AnimFrame TankSprite;

        //The fighter equipped with the tank
        private Fighter Fighter;

        public OxygenTank(Texture2D tanksprite, Texture2D tankicon, Fighter fight)
        {
            Fighter = fight;

            //FINISH LATER
            SpriteSheet = tanksprite;
            Icon = tankicon;

            TankSprite = new AnimFrame(new Rectangle(0, 0, SpriteSheet.Width, SpriteSheet.Height), 0f);

            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            SetUpHealth(MaxHits);
            SetUpHurtbox();

            Location = new Vector3(Fighter.FacingRight == true ? Fighter.GetLocation.X - SpriteSheet.Width : Fighter.GetLocation.X + 39, Fighter.GetLocation.Y, Fighter.MaxHeight - SpriteSheet.Height);
            status = Fighter.status;

            BlocksMove = false;
            UseGravity = false;

            StatusAffected = false;

            TileBlocked = false;
            SolidBlocked = false;

            PrevHit = 0f;

            hud = new HUD(this, Vector2.Zero);
        }

        //Gets the oxygen tank color based on the player number
        public static Texture2D GetOxygenTankColor(int playernum)
        {
            switch (playernum)
            {
                case 0: return LoadGraphics.OxygenTankFillRed;
                case 1: return LoadGraphics.OxygenTankFillBlue;
                default: return LoadGraphics.OxygenTankFillGreen;
                //case 2: return LoadGraphics.OxygenTankFillGreen;
                //default: return LoadGraphics.OxygenTankFillYellow;
            }
        }

        //Gets the oxygen tank color based on the team the player is on
        public static Texture2D GetOxygenTankColorTeam(int teamcolor)
        {
            switch (teamcolor)
            {
                case 0: return LoadGraphics.OxygenTankFillRed;
                case 1: return LoadGraphics.OxygenTankFillGreen;
                default: return LoadGraphics.OxygenTankFillBlue;
            }
        }

        //Oxygen tanks receive only 1 damage from everything
        public override int TotalDamageReceived(int damage)
        {
            return 1;
        }

        public override bool CanGetHit(BeatEmUpObj attacker, Hitbox hitbox)
        {
            return (base.CanGetHit(attacker, hitbox) == true && Main.GetActiveTime >= PrevHit);
        }

        //Oxygen tanks can't be inflicted with a Status
        public override bool CanBeInflictedStatus(BeatEmUpObj attacker)
        {
            return false;
        }

        protected override void TakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.TakeDamageActions(objecthitby, damage, hitbox);

            PrevHit = Main.GetActiveTime + 1500f;
        }

        public override void Die()
        {
            //If the Oxygen Tank gets broken, the Fighter dies
            base.Die();
            Fighter.Die();

            status = new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus);

            LoadSounds.Play(LoadSounds.Break);
        }

        //Replenishes the oxygen tank's health - only called when a player picks up a Replacement Tank 
        public void HealTank(bool sound = true)
        {
            FullHeal();
            if (sound == true) LoadSounds.Play(LoadSounds.Restore);
        }

        protected override void ObjectUpdate()
        {
            base.ObjectUpdate();

            if (IsDead == false)
            {
                Location = new Vector3(Fighter.FacingRight == true ? Fighter.GetLocation.X - SpriteSheet.Width : Fighter.GetLocation.X + 39, Fighter.FeetLoc.Y, Fighter.MaxHeight - 15 - SpriteSheet.Height);

                status = Fighter.status;
            }
            //Make the oxygen tank rise in water if it's broken
            else Location.Z += 2f;
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            base.ObjectDraw(spriteBatch, cameraloc, TileEngine);

            float depth = Fighter.GetDrawDepth(cameraloc);
            if (IsDead == true) depth = GetDrawDepth(cameraloc);

            //Don't draw the oxygen tank if it's invisible when invincible
            if (TankSprite != null)
                TankSprite.Draw(spriteBatch, SpriteSheet, GetDrawLoc(cameraloc), Fighter.FacingRight, status.GStatusColor, GetRotation, depth);
        }

        public override bool CanSetHUD()
        {
            return false;
        }

        protected override bool ShouldDrawShadow()
        {
            return false;
        }

        protected override bool CanDrawNoStatus()
        {
            return false;
        }   
    }
}
