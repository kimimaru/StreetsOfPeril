﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //The base class for Fighters (Players and Enemies)
    public abstract class Fighter : BeatEmUpObj
    {
        //The color for MoveInvincibility
        protected static readonly Color MoveInvColor;

        //The original jump velocity a fighter has and a modifier that changes how fast the Fighter moves when knocked down
        protected Vector3 OrigJumpVelocity;
        protected Vector2 MoveModifier;

        //The current slide speeds of the fighter
        protected float SlideSpeed;

        //NOTE: Will be changed to CurAnim so we don't need to perform a lot of ifs when updating or drawing
        //An animation that points to the most recent animation that is associated with hitboxes (Ex. AttackAnim); it's used to extend the duration of the animation when you hit something
        protected NewAnimation CurDamagingAnim;

        protected NewAnimation StandAnim;
        protected NewAnimation GrabbedAnim;
        protected NewAnimation HurtAnim;
        protected NewAnimation KnockedDownAnim;
        protected NewAnimation ForwardKnockedDownAnim;
        protected NewAnimation SwimAnim;

        protected NewAnimation CurKnockDownAnim;

        //The length of Hitstun a Fighter experiences
        protected float HitStun;
        protected float PrevStun;

        protected bool IsHit;
        protected bool IsKnockedDown;

        //The timer for having the Fighter be invincible for a little while after getting back up and a bool that determines if the Fighter gains this invincibility
        public const float KnockDownInvincibility = 400f;
        protected float PrevKnockInv;
        protected bool GainKnockInv;

        //The temporary health bar from the Health Juice item
        public int TempBar { get; protected set; }

        //The MoveInvincibility color timer; this tracks when to change the Fighter's DrawColor if it has MoveInvincibility
        //The color to draw the Fighter; this defaults to the Status color but changes if the Fighter has MoveInvincibility
        protected const float MoveInvTimer = 140f;
        protected const float MoveInvTimerHalf = (MoveInvTimer / 2f);
        protected float PrevMoveInv;
        protected Color DrawColor;

        //The sound the Fighter makes when dying
        protected SoundEffect DeathSound;

        //The oxygen tank the fighter has if it's in an underwater level
        public OxygenTank OxygenTank { get; protected set; }

        //The current HoT (healing over time) item affecting the Fighter
        //This reference is used to easily stop this Item from healing if the Fighter picks up another HoT Item while this one is active
        public Item HoTItem { get; protected set; }

        //The weapon the fighter is holding
        public Weapon weapon { get; protected set; }

        //The fighter this fighter is grabbing; this abstraction enables players to grab other players and enemies to grab other enemies
        public Fighter GrabbedFighter { get; protected set; }

        protected Fighter()
        {
            OrigJumpVelocity = new Vector3(4f, 4f, 4f);

            SlideSpeed = 0f;
            PrevHitLag = 0f;
            PrevStun = 0f;

            TempBar = 0;

            GainKnockInv = true;

            DrawColor = status.GStatusColor;
            PrevMoveInv = 0f;

            OxygenTank = null;
            HoTItem = null;
            weapon = null;
            GrabbedFighter = null;
        }

        static Fighter()
        {
            MoveInvColor = new Color(225, 225, 120);
        }

        public static bool IsObjectFighter(BeatEmUpObj beatemupobj)
        {
            return (beatemupobj.ObjType == ObjectType.Player || beatemupobj.ObjType == ObjectType.Enemy);
        }

        //The Fighter is invincible if it is invincible normally or from a move
        protected override bool IsInvincible
        {
            get { return (base.IsInvincible == true || KnockedDown == true || HasMoveInvincibility == true); }
        }

        //Checks if the Fighter has invincibility from a move
        protected virtual bool HasMoveInvincibility
        {
            get { return (Main.GetActiveTime < PrevKnockInv); }
        }

        //The grabbox of the Fighter; enemies will return their grabbox rectangles instead
        public virtual Rectangle GrabBox
        {
            get { return CollisionBox; }
        }

        //The speed of the Fighter when getting knocked down; this isn't affected by the Fighter's Status
        public float KnockDownVelocity
        {
            get
            {
                float knockdownvel = OrigJumpVelocity.X + MoveModifier.X;
                if (IsInWater == true)
                {
                    knockdownvel -= WaterSpeedSlow;
                    if (knockdownvel < 0f) knockdownvel = 0f;
                }

                return knockdownvel;
            }
        }

        public Vector3 GetOrigJumpVelocity
        {
            get { return OrigJumpVelocity; }
        }

        public bool HasTempBar
        {
            get { return (TempBar > 0); }
        }

        public bool HasOxygenTank
        {
            get { return (OxygenTank != null); }
        }

        public bool HasHoTItem
        {
            get { return (HoTItem != null); }
        }

        public bool HasWeapon
        {
            get { return (weapon != null); }
        }

        public bool IsGrabbing
        {
            get { return (GrabbedFighter != null); }
        }

        //The current health used by the Fighter; if the Fighter has a TempBar from the Health Juice, that will be used instead of the Fighter's normal Health
        public override int GetHealth
        {
            get { return (TempBar <= 0 ? Health : TempBar); }
        }

        //The direction facing towards the Fighter - in general, most hitboxes by Fighters will cause the victim to face towards the attacker; use this as shorthand
        public bool TowardsDir
        {
            get { return !FacingRight; }
        }

        //The invincibility given to fighters who have died
        protected Status DeathInvincibility
        {
            get { return new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0); }
        }

        //Checks if the Fighter is in hitstun; we need to always call TotalHitStun() because the status can end while the Fighter is in hitstun
        public bool IsInHitstun
        {
            get { return ((Main.GetActiveTime - PrevStun) < TotalHitStun()); }
        }

        //Tells if the Fighter is knocked down or not
        public virtual bool KnockedDown
        {
            get { return IsKnockedDown; }
        }

        public Vector2 FighterCenter
        {
            get
            {
                Rectangle collisionbox = CollisionBox;

                return new Vector2(collisionbox.Center.X, collisionbox.Center.Y);
            }
        }

        //Brushes right up against an object's feetloc horizontally
        //If facingright is true, then this object's feetloc.Right will be 1 pixel from the left of that object's feetloc.Left and vice versa
        protected float BrushUpHorizontal(BeatEmUpObj otherobject, bool facingright)
        {
            Rectangle otherfeetloc = otherobject.FeetLoc;
            Rectangle feetloc = FeetLoc;

            float xval = facingright == true ? otherfeetloc.Right - feetloc.Left : otherfeetloc.Left - feetloc.Right;

            return xval;
        }

        //The same as above, except vertical; if top is true, it means this object is brushing up against the bottom of the other's feetloc.Bottom
        protected float BrushUpVertical(BeatEmUpObj otherobject, bool top)
        {
            Rectangle otherfeetloc = otherobject.FeetLoc;
            Rectangle feetloc = FeetLoc;

            float yval = top == true ? feetloc.Y - otherfeetloc.Bottom : feetloc.Bottom - otherfeetloc.Y;

            return yval;
        }

        //Match the top of another object's feetloc
        protected float MatchFeetTop(BeatEmUpObj otherobject)
        {
            return (otherobject.FeetLoc.Y - FeetLoc.Y);
        }

        //Modifies the Fighter's air velocity
        public void ChangeAirVelocity(Vector3 airvel)
        {
            SetAirVelocity(airvel);
        }

        //Resets the fighter's actions - used most often for when it gets hit, knocked down, dies, or lands on the floor
        protected virtual void ResetActions()
        {
            //If we have a weapon and it's picked up, clear its hitboxes
            if (weapon != null && weapon.IsPickedUp == true) weapon.Hitboxes.Clear();
            ClearHitboxes();
        }

        //Calculates the total hitstun the Fighter experiences when hit by something
        private float TotalHitStun()
        {
            if (StatusCondition(status == (int)Status.Statuses.StunDown) == true) return (int)(HitStun / 2f);
            else return HitStun;
        }

        //Make the Fighter enter hitstun
        protected virtual void EnterHitstun()
        {
            ResetActions();
            //IsHit = true;
            AirVelocity = Vector3.Zero;
            //HurtAnim.Reset();
            PrevStun = Main.GetActiveTime;

            //If you're grabbed, refresh the grab duration

        }

        //Recovers the Fighter from hitstun
        protected void HitstunRecover()
        {
            PrevStun = 0f;
        }

        //Knock down the Fighter
        protected void KnockDown(NewAnimation knockdownanim)
        {
            ResetActions();
            Jump(new Vector3(ForwardVal(-KnockDownVelocity), 0f, OrigJumpVelocity.Z));
            CurKnockDownAnim = knockdownanim;
            CurKnockDownAnim.Reset();

            SlideSpeed = KnockDownVelocity;

            GrabRecover();

            IsKnockedDown = true;

            //Perform any fighter-specific actions when getting knocked down
            KnockDownActions();
        }

        //Recovers the Fighter from a knock down
        public virtual void KnockDownRecover()
        {
            //IsKnockedDown = false;

            //If the Fighter is supposed to be invincible for a little after getting up, reset the timer for that invincibility
            if (GainKnockInv == true) PrevKnockInv = Main.GetActiveTime + KnockDownInvincibility;
        }

        //Fighter-specific actions that occur when the fighter gets knocked down
        protected virtual void KnockDownActions()
        {

        }

        public sealed override void LoseHealth(int finaldamage)
        {
            SetDamageIndicator(finaldamage);

            //If we have a temporary health bar, subtract health from that, otherwise subtract from normal health
            if (TempBar > 0) TempBar -= finaldamage;
            else Health -= finaldamage;

            HealthUpdate();
        }

        protected sealed override void HealthUpdate()
        {
            //If you run out of the temporary health bar, apply any leftover damage to your normal health and show the damage difference
            if (TempBar < 0)
            {
                SetDamageIndicator(-TempBar);
                Health += TempBar;
                TempBar = 0;
            }

            base.HealthUpdate();
        }

        //Gives the fighter a temporary health bar (obtained by picking up Health Juice)
        public void GrantTempHealthBar()
        {
            TempBar = MaxHealth[HealthBars];
        }

        //Gets the total amount of health the Fighter currently has, which includes the TempBar
        public sealed override int TotalCurHealth()
        {
            return (base.TotalCurHealth() + TempBar);
        }

        public override void TakeDamage(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            //Change the direction the Fighter is facing to the direction the hitbox is in
            if (hitbox.HitDirection != null)
                FacingRight = hitbox.HitDirection.Value;

            //Lose health
            base.TakeDamage(objecthitby, damage, hitbox);
        }

        protected override void TakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.TakeDamageActions(objecthitby, damage, hitbox);

            //If the hitbox should knock down or the Fighter is in the air, knock down the Fighter
            if (hitbox.GetHitboxType == Hitbox.HitboxTypes.KnockDown || IsInAir == true)
            {
                KnockDown(KnockedDownAnim);
            }
            //Otherwise make the Fighter enter hitstun
            else
            {
                EnterHitstun();
            }

            //Release a Fighter if one is being grabbed
            ClearGrabbedFighter();

            //Check if the Fighter should drop its weapon after getting hit
            if (weapon != null)
            {
                if (weapon.ShouldDropWeapon(Main.GetActiveTime) == true)
                {
                    weapon.HitOut();
                }
            }
        }

        protected override void HitLagActions(float diff)
        {
            base.HitLagActions(diff);

            //if (CurDamagingAnim != null) CurDamagingAnim.DelayAnim(diff);
        }

        //Checks if the HUD should show how much damage has been done depending if the temporary health bar minus the damage would equal 0 or not
        //Without this, if it equals 0, it will show that much damage be done to the Fighter's health, even though health is untouched
        private bool ShouldSetDamageIndicator(int damage)
        {
            return ((TempBar - damage) != 0);
        }

        //Easy method for setting the damage indicator
        protected override void SetDamageIndicator(int finaldamage)
        {
            if (ShouldSetDamageIndicator(finaldamage) == true) hud.SetDamageIndicator(Main.GetActiveTime, GetHealth, HealthBars, finaldamage);
        }

        //For Players - set the Player's interaction HUD and move it underneath the Player's own HUD
        public virtual void SetInteractionHUD(HUD objecthud)
        {

        }

        protected override void EnterWater()
        {
            base.EnterWater();

            OxygenTank = new OxygenTank(LoadGraphics.FallingRock, LoadGraphics.OxygenTankFillBlue, this);
        }

        protected override void ExitWater()
        {
            base.ExitWater();

            OxygenTank = null;
        }

        protected override void LandActions()
        {
            ResetActions();

            //If the fighter is knocked down, update the knocked down animation and play a sound (if not in water)
            if (KnockedDown == true)
            {
                LandKnockedDown();
                CurKnockDownAnim.Update(Main.GetActiveTime);
            }
            else
            {
                LandNormal();
                if (ObjectTile.Z >= 0f) LoadSounds.Play(LoadSounds.JumpLand);
            }
        }

        protected override void BeginFall()
        {
            base.BeginFall();

            //Clear grabbed fighter
            ClearGrabbedFighter();

            //Reset the Fighter's actions when it starts falling
            ResetActions();
        }

        public override void Die()
        {
            base.Die();

            //Set temp bar to 0
            TempBar = 0;

            //Knock down the Fighter if it isn't knocked down
            if (KnockedDown == false)
                KnockDown(CurKnockDownAnim);

            //Release grabbed fighter if the Fighter is grabbing one
            ClearGrabbedFighter();

            //Drop the weapon the Fighter is holding if it's holding one
            if (HasWeapon == true)
            {
                weapon.HitOut();
            }

            //Remove the HoT item
            ClearHoTItem();
        }

        //Players can't inflict Statuses on other players and enemies can't inflict Statuses on other enemies
        //Versus mode may be an exception, so we'll take that into account when the time comes
        public override bool CanInflictStatus(BeatEmUpObj victim)
        {
            return (base.CanInflictStatus(victim) == true && ObjType != victim.ObjType && IsObjectFighter(victim) == true);
        }

        protected override void FallIntoPit()
        {
            base.FallIntoPit();
            IsKnockedDown = false;
        }

        //Gets called when the Fighter lands knocked down
        protected virtual void LandKnockedDown()
        {
            
        }

        //Gets called when the Fighter lands normally (not knocked down)
        protected virtual void LandNormal()
        {
            
        }

        //Sets the Fighter's HoT Item
        public void SetHoTItem(Item item)
        {
            //If the Fighter is already affected by an HoT item, disable that one
            if (HasHoTItem == true) HoTItem.DisableHoT();
            HoTItem = item;
        }

        //Clears the Fighter's HoT Item
        public void ClearHoTItem()
        {
            //Kill off the Item so it no longer heals the Fighter
            if (HasHoTItem == true)
            {
                HoTItem.DisableHoT();
                HoTItem = null;
            }
        }

        //Sets the Fighter's Weapon
        public void SetWeapon(Weapon newweapon)
        {
            weapon = newweapon;
        }

        //Clears the Fighter's Weapon
        public void ClearWeapon()
        {
            weapon = null;
        }

        //For picking up items and weapons
        public virtual void Pickup(PickupObj pickupobj)
        {
            //If the object picked up is a weapon, check if the fighter already has a weapon and switch if so
            if (pickupobj.ObjType == ObjectType.Weapon)
            {
                if (weapon != null) weapon.Switch((Weapon)pickupobj);
                weapon = (Weapon)pickupobj;
            }

            //Tell the PickupObj that it has been picked up by this Fighter
            pickupobj.GetPickedUp(this);

            //If the Fighter is a Player, set its InteractionHUD to the PickupObj's HUD
            SetInteractionHUD(pickupobj.hud);
        }

        //Makes the Fighter recover from a grab
        public virtual void GrabRecover()
        {
            
        }

        //Makes this Fighter release its grabbed Fighter
        protected void ClearGrabbedFighter()
        {
            if (GrabbedFighter != null)
            {
                GrabbedFighter.GrabRecover();
                GrabbedFighter = null;
            }
        }

        //Makes the Fighter get grabbed; opponent is a BeatEmUpObj incase we want to have other objects grab Fighters
        public virtual void GetGrabbed(BeatEmUpObj opponent)
        {
            ResetActions();

            //If the Fighter was grabbed on the ground, alter its position (aerial grabs move the grabber instead)
            if (CurHeight == opponent.CurHeight)
            {
                Vector2 movevel = new Vector2(BrushUpHorizontal(opponent, opponent.FacingRight), MatchFeetTop(opponent));

                //Make the Fighter go in front of the opponent when it gets grabbed
                ObjectMove(movevel);
            }

            //Get grabbed
            //IsGrabbed = true;
        }

        //Grab another Fighter
        public virtual void GrabFighter(Fighter fighter)
        {
            GrabbedFighter = fighter;
            GrabbedFighter.GetGrabbed(this);

            //The Fighter grabbed the other Fighter in the air
            if (CurHeight > GrabbedFighter.CurHeight)
            {
                //If the Fighter is facing right, brush up against the left side of the grabbed Fighter; otherwise, brush up against the right side
                float side = BrushUpHorizontal(GrabbedFighter, !FacingRight);
                float top = MatchFeetTop(GrabbedFighter);

                Vector2 movevel = new Vector2(side, top);

                //Make the Fighter go in front of the opponent when it grabs the opponent in the air
                ObjectMove(movevel);

                //Make the Fighter be above the opponent since it grabbed in the air
                Location.Z = GrabbedFighter.CurHeight + 20f;
            }
        }

        //Throws a Fighter
        public void ThrowFighter(BeatEmUpObj victim, int damage, Hitbox hitbox)
        {
            //Take damage like normal
            victim.TakeDamage(this, damage, hitbox);
            
            //Put a hitbox around the victim
            victim.CreateHitboxSurrounding(4, ObjectHeight / 2, 0f, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, FacingRight, LoadSounds.Kick, true);
        }

        //RPS version of throwing the opponent
        //NOTE: We shouldn't need this if everything works out fine
        //protected virtual void RPSThrowOpponent(int damage, Hitbox hitbox, SubLevel level)
        //{
        //
        //}

        //Make the fighter perform a throw move on its opponent
        //protected virtual void ThrowOpponent(int damage, Hitbox hitbox, SubLevel level)
        //{
        //
        //}

        //Creates a throw hitbox; we currently don't play any sounds for any throw hitboxes, so we'll pass in null for the sound effect
        public ThrowHitbox CreateThrowHitbox(bool front, int length, int width, int hitboxstrength, int hitboxheight, float hitboxdelay, float hitboxdur, bool? hitboxdirection, int score, ThrowHitbox.ThrowDelegate throwdelegate, bool addhitbox)
        {
            Hitbox hitbox = CreateHitboxFeet(front, length, width, hitboxstrength, hitboxheight, hitboxdelay, hitboxdur, Hitbox.HitboxTypes.Throw, hitboxdirection, null, false);

            ThrowHitbox newhitbox = new ThrowHitbox(hitbox, score, throwdelegate);
            
            if (addhitbox == true) AddHitbox(newhitbox);
            return newhitbox;
        }

        protected void UpdateKnockDownAnim()
        {
            //Check if the Fighter is knocked down
            if (KnockedDown == true)
            {
                //If the Fighter is not on the first frame of the knock down animation...
                if (CurKnockDownAnim.CurrentFrame != 0)
                {
                    //If the Fighter is on the ground, update the knock down animation and slide on the floor
                    if (IsInAir == false)
                    {
                        CurKnockDownAnim.Update(Main.GetActiveTime, StatusCondition(status == (int)Status.Statuses.StunDown) == true ? .4f : .2f);
                        //Slide while on the floor

                    }
                    //Otherwise the Fighter is in the air, so reset the knock down animation and carry the slide's momentum into the velocity while falling
                    else
                    {
                        CurKnockDownAnim.Reset();
                        AirVelocity.X = ForwardVal(-SlideSpeed);
                    }

                    //Check if the fighter is dead, on the floor, and not invincible yet
                    if (IsDead == true && status != (int)Status.Statuses.Invincible)
                    {
                        InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));

                        //Restart the object's HUD so it can get removed properly
                        hud.Restart();

                        //Play death sound
                        if (DeathSound != null) LoadSounds.Play(DeathSound);
                    }

                    //If the fighter's jump velocity is 0, the player is on the floor; once that's true, check if the animation is done or if the player is dead, which ends the animation earlier
                    if (AirVelocity.Y == 0f && (CurKnockDownAnim.IsAnimationEnd() == true || IsDead == true && CurKnockDownAnim.CurrentFrame == 2))
                    {
                        //Recover from being knocked down
                        KnockDownRecover();
                    }
                }
                //The Fighter is in the air, so move until it hits the ground
                else
                {
                    ObjectMove(new Vector2(AirVelocity.X, 0));
                }
            }
        }

        public virtual void UpdateAnimations()
        {

        }

        protected void UpdateMoveInvincibility()
        {
            //Set the Fighter's draw color to its Status' color by default
            DrawColor = status.GStatusColor;

            //Check if the Fighter is invincible from a move
            if (HasMoveInvincibility == true)
            {
                //If the timer is up, reset it; if it's more than halfway done, the draw color is set to the Fighter's Status' color
                if (Main.GetActiveTime >= PrevMoveInv)
                    PrevMoveInv = Main.GetActiveTime + MoveInvTimer;

                //If the timer is less than or equal to halfway done, set the draw color to the MoveInvincibility color
                if (Main.GetActiveTime <= (PrevMoveInv - MoveInvTimerHalf))
                    DrawColor = MoveInvColor;

            }
            //Reset PrevMoveInv to 0 if the Fighter does not have MoveInvincibility; this maintains consistency since MoveInvincibility can be renewed immediately after it expires
            else if (PrevMoveInv != 0f) PrevMoveInv = 0f;
        }

        protected sealed override void ObjectUpdate()
        {
            //Check for picking up an item or weapon in the level
            //HandleObjPickup(level);

            //Fighter-specific update
            FighterUpdate();

            //UpdateAnimations();

            //Update the color tint the Fighter is drawn with if it has move invincibility
            UpdateMoveInvincibility();
        }

        //Handles picking up objects in the level
        protected virtual void HandleObjPickup()
        {

        }

        //Fighter-specific update logic
        protected virtual void FighterUpdate()
        {

        }

        public virtual bool CanGetGrabbed()
        {
            return (IsInvincible == false);
        }

        public virtual bool CanAttack()
        {
            return (StatusCondition(status != (int)Status.Statuses.NoAttack) == true);
        }

        public virtual bool CanJump()
        {
            return (StatusCondition(status != (int)Status.Statuses.NoJump) == true);
        }

        public virtual bool CanGrab()
        {
            return (StatusCondition(status != (int)Status.Statuses.NoGrab) == true && IsDead == false);
        }

        public virtual bool CanSpecial()
        {
            return (StatusCondition(status != (int)Status.Statuses.NoSpecial) == true);
        }

        //Fighters can't pick up any PickupObjs if they have the NoGrab Status
        public virtual bool CanPickUp()
        {
            return (CanGrab() == true && IsInAir == false && KnockedDown == false && IsInHitstun == false);
        }

        protected virtual void FighterDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {

        }

        protected sealed override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            FighterDraw(spriteBatch, cameraloc, TileEngine);

            //Draw the Fighter's Oxygen Tank
            if (OxygenTank != null)
                OxygenTank.Draw(spriteBatch, cameraloc, TileEngine);
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Vector2 OffSet)
        {
            base.DebugDraw(spriteBatch, OffSet);

            if (Debug.GrabboxDraw == true)
                Debug.DrawRectangle(spriteBatch, GrabBox, OffSet, Color.Green, .9992f);
        }
    }
}
