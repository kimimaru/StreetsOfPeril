﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Status effects
    //NOTE: Maybe make the tint of the Invincible status fade instead of blink on and off?
    public sealed class Status
    {
       //The value to make a status last forever
       public const float InfiniteStatus = 1f;

       //How long it takes for Poison to harm an object afflicted with it
       private const float PoisonInterval = 160f;

       //The maximum length the Poison status should ever last; Poison needs this because it slowly drains the health of whatever is inflicted with it
       public const float MaxPoison = 8000f;

       //The speed modifier SpeedBoost, SpeedDown, and Rainbow have
       public const float SpeedModifier = 2f;

       //The default Min and Max color values of each Status
       private const int MinColorValue = 0;
       private const int MaxColorValue = 125;

       private int Cond;
       private int Percentage;
       private float PrevDur;
       private float StatusDur;
       private float PrevPoison;

       //Status tint properties
       private Color StatusColor;
       private Fade StatusTint;

       //Just for keeping track of all the statuses in the game
       public enum Statuses
       {
           None, DamageBoost, DamageDown, DefenseBoost, DefenseDown, SpeedBoost, SpeedDown, StunDown, Poison, NoJump, NoAttack, NoGrab, NoSpecial, Invincible, Rainbow, InfBullets
       };

       //Constructors
       public Status()
       {
           Cond = (int)Statuses.None;
           Percentage = 0;
           PrevDur = 0f;
           PrevPoison = 0f;
           StatusColor = Color.White;
           StatusTint = Fade.EmptyFade;
       }

       //Set StatusDur to InfiniteStatus to make the status last forever
       public Status(int newcond, float statdur) : this()
       {
           Cond = newcond;
           Percentage = 100;
           StatusDur = statdur;

           StatusUpdate();
       }

       //Constructor for set percentages
       public Status(int newcond, float statdur, int percentage) : this(newcond, statdur)
       {
           Percentage = percentage;

           //StatusUpdate();
       }

       //Copy constructor
       public Status(Status status) : this(status.GCond, status.GStatusDur, status.GPercentage)
       {

       }
       
       //Operator overloading - ease of use
       public static bool operator ==(Status cond, int newcond)
       {
           if (cond != null && cond.GCond == newcond) return true;
           return false;
       }

       public static bool operator !=(Status cond, int newcond)
       {
           if (cond != null && cond.GCond != newcond) return true;
           return false;
       }
       
       //Inverts the current status
       public static Status Invert(Status status)
       {
           int cond = status.GCond;

           //Invert applies only to the Damage, Defense, and Speed Boost/Down statuses
           if (cond >= (int)Statuses.DamageBoost && cond <= (int)Statuses.SpeedDown)
           {
               //If the status is an even number, it's a Down, so just subtract 1 to get the Boost counterpart; the opposite applies to an odd numbered status
               if (cond % 2 == 0) cond -= 1;
               else cond += 1;

               return new Status(cond, status.GStatusDur, status.GPercentage);
           }

           //If Invert doesn't work because the status isn't Damage/Defense/Speed Boost/Down, just return the status without refreshing its duration
           return status;
       }
       
       //Gets the name of a status
       public static String StatName(Status status)
       {   
           return Enum.GetName(typeof(Statuses), status.GCond);
       }

       public static String StatName(int status)
       {
           return Enum.GetName(typeof(Statuses), status);
       }

       //Gets the number of a status from a given name
       public static int StatNum(String status)
       {
           Statuses stat;
           if (Enum.TryParse<Statuses>(status, out stat) == true) return (int)stat;
           else return (int)Status.Statuses.None;
       }

       //Returns a status given a string representation of the status
       public static Status StatFull(String status)
       {
           String[] stat = status.Split(',');
           
           int condition = 0;
           float duration = 0f;
           int percentage = 0;
           
           //Find the status effect
           if (stat.Length > 0)
           {
               condition = Status.StatNum(stat[0]);
           
               //Find the duration
               if (stat.Length > 1)
               {
                   float.TryParse(stat[1], out duration);
           
                   //Find the percentage
                   if (stat.Length > 2)
                   {
                       int.TryParse(stat[2], out percentage);
                   }
               }
           }
           
           return new Status(condition, duration, percentage);
       }

       //Tells if the status is a positive status; static version
       public static bool IsPositiveStatus(int Cond)
       {
           return (Cond == (int)Statuses.DamageBoost || Cond == (int)Statuses.DefenseBoost || Cond == (int)Statuses.SpeedBoost || Cond == (int)Statuses.StunDown || Cond == (int)Statuses.Invincible || Cond == (int)Statuses.Rainbow || Cond == (int)Statuses.InfBullets);
       }

       //Tells if the status is a negative status; static version
       public static bool IsNegativeStatus(int Cond)
       {
           return (IsPositiveStatus(Cond) == false && Cond != (int)Statuses.None);
       }

       //Accessors and Manipulation Procedures
       public int GCond
       {
           get { return Cond; }
       }

       public float GStatusDur
       {
           get { return StatusDur; }
       }

       public int GPercentage
       {
           get { return Percentage; }
       }

       public Color GStatusColor
       {
           get { return StatusColor; }
       }

       //Tells if the status lasts forever
       public bool IsInfinite
       {
           get { return (StatusDur == InfiniteStatus); }
       }
       
       //Tells if the current status should end or not
       public bool ShouldEnd
       {
           get { return (IsInfinite == false && (Main.GetActiveTime - PrevDur) >= StatusDur); }
       }
        
       //Tells if the status is a positive status
       public bool IsPositiveStatus()
       {
           return IsPositiveStatus(Cond);
       }

       //Tells if the status is a negative status
       public bool IsNegativeStatus()
       {
           return IsNegativeStatus(Cond);
       }

       //Tells if the status increases damage
       public bool IncreasesDamage()
       {
           return (Cond == (int)Statuses.DamageBoost || Cond == (int)Statuses.Rainbow);
       }

       //Tells if the status increases defense
       public bool IncreasesDefense()
       {
           return (Cond == (int)Statuses.DefenseBoost || Cond == (int)Statuses.Rainbow);
       }

       //Tells if the status increases speed
       public bool IncreasesSpeed()
       {
           return (Cond == (int)Statuses.SpeedBoost || Cond == (int)Statuses.Rainbow);
       }

       //Tells if the status is a NoStatus (NoJump to NoSpecial)
       public bool IsNoStatus()
       {
           return (Cond >= (int)Statuses.NoJump && Cond <= (int)Statuses.NoSpecial);
       }

       //Make the status blink at a faster rate depending on how short it lasts
       public float ColorChangeRate
       {
           get { return ((int)(Math.Min((int)Math.Max((int)(20000f / StatusDur), 1), 7))); }
       }

       //Changes the status' alpha color to be opaque - called at the end of levels
       public void SetColorOpaque()
       {
           StatusColor.A = 255;
       }

       //Resets the poison timer after the poison damaged an object
       public void ResetPoisonTimer()
       {
           PrevPoison = Main.GetActiveTime;
       }

       //Gets the duration of the status relative to its max duration; for use in the HUD class when drawing the status timer bar
       public float RelativeStatDur
       {
           get { return (float)((StatusDur - (Main.GetActiveTime - PrevDur)) / StatusDur); }
       }

       //Determines when the poison should damage the object
       public bool PoisonDamageObject()
       {
           if (Cond == (int)Status.Statuses.Poison && (Main.GetActiveTime - PrevPoison) >= PoisonInterval)
           {
               return true;
           }

           return false;
       }

       //Determines if something gets afflicted with the status effect, based on the percentage
       public bool ShouldAfflict()
       {
           Random random = new Random();
           int statusafflict = random.Next(1, 101);

           return (Cond != (int)Statuses.None && statusafflict <= Percentage);
       }

       //Changes value of the color tint
       public void UpdateTint()
       {
           if (Cond == (int)Statuses.None || IsNoStatus() == true) return;

           //Update the tint of the Status
           StatusTint.UpdateContinuous(Main.GetActiveTime);

           //Update the color of the tint depending on the Status Effect the object has
           if (Cond == (int)Statuses.DamageBoost || Cond == (int)Statuses.DamageDown)
           {
               StatusColor = new Color(StatusColor.R, StatusTint.CurFadeVal, StatusTint.CurFadeVal);
           }
           else if (Cond == (int)Statuses.DefenseBoost || Cond == (int)Statuses.DefenseDown)
           {
               StatusColor = new Color(StatusTint.CurFadeVal, StatusTint.CurFadeVal, StatusColor.B);
           }
           else if (Cond == (int)Statuses.SpeedBoost || Cond == (int)Statuses.SpeedDown)
           {
               StatusColor = new Color(StatusTint.CurFadeVal, StatusColor.G, StatusTint.CurFadeVal);
           }
           else if (Cond == (int)Statuses.Poison)
           {
               StatusColor = new Color(StatusTint.CurFadeVal, StatusTint.CurFadeVal, StatusTint.CurFadeVal);
           }
           else if (Cond == (int)Statuses.StunDown)
           {
               StatusColor = new Color(StatusTint.CurFadeVal, StatusTint.CurFadeVal + 70, StatusColor.B);
           }
           else if (Cond == (int)Statuses.Invincible)
           {
               if (StatusColor.A == 255)
                   StatusColor.A = 0;
               else StatusColor.A = 255;
           }
           else if (Cond == (int)Statuses.Rainbow)
           {
               //Flash multiple colors
               if (StatusColor.R == 255)
               {
                   StatusColor = new Color(StatusColor.R, StatusTint.CurFadeVal, StatusTint.CurFadeVal);
                   if (StatusColor.G <= 0)
                   {
                       StatusColor.R = 0;
                       StatusColor.G = 255;
                   }
               }
               else if (StatusColor.G == 255)
               {
                   StatusColor = new Color(StatusTint.CurFadeVal, StatusColor.G, StatusTint.CurFadeVal);
                   if (StatusColor.B <= 0)
                   {
                       StatusColor.G = 0;
                       StatusColor.B = 255;
                   }
               }
               else if (StatusColor.B == 255)
               {
                   StatusColor = new Color(StatusTint.CurFadeVal, StatusTint.CurFadeVal, StatusColor.B);
                   if (StatusColor.R == 0)
                   {
                       StatusColor.B = 0;
                       StatusColor.R = 255;
                   }
               }
           }
       }
       
       //Resets the current status to None
       public void Reset()
       {
           Cond = (int)Statuses.None;
           StatusDur = 0;
           PrevDur = 0f;
           StatusColor = Color.White;
           StatusTint = Fade.EmptyFade;
       }
       
       //Restarts the current status
       //public void Restart()
       //{
       //    PrevDur = Main.GetActiveTime/* + StatusDur*/;
       //    //Initialize();
       //}

       //Reset status
       public void StatusReset(bool reset = false)
       {
           //If the status is done, reset it; otherwise, make the character glow (indicates the status)
           if (ShouldEnd == true || reset == true)
           {
               Cond = (int)Statuses.None;
               StatusDur = 0;
               PrevDur = 0f;
               StatusColor = Color.White;
               StatusTint = Fade.EmptyFade;
           }
           else
           {
               UpdateTint();
           }
       }

       //Status methods - Max Poison length is 8 seconds (half a health bar)!
       private void /*Initialize*/StatusUpdate()
       {
           int mincolorvalue = MinColorValue;
           int maxcolorvalue = MaxColorValue;

           if (Cond == (int)Statuses.None) return;
           else if (Cond == (int)Statuses.DamageDown)
           {
               StatusColor.R = (byte)150;
           }
           else if (Cond == (int)Statuses.DefenseDown)
           {
               StatusColor.B = (byte)150;
           }
           else if (Cond == (int)Statuses.SpeedDown)
           {
               StatusColor.G = (byte)150;
           }
           else if (Cond == (int)Statuses.Poison)
           {
               //Poison gets too dark at 0, so it needs a higher minimum value
               mincolorvalue = 50;

               //Make sure Poison doesn't last longer than the maximum length it should (the exception is a Poison that lasts forever, which is used in Survival - Easy)
               if (StatusDur > MaxPoison) StatusDur = MaxPoison;
               PrevPoison = Main.GetActiveTime;
           }
           /*else if (Cond == (int)Statuses.NoJump)
           {
               StatusColor = new Color(220, 220, 220);
               MinColorValue = 140;
               MaxColorValue = 240;
               ColorValue = 240;
           }
           else if (Cond == (int)Statuses.NoAttack)
           {
               StatusColor = new Color(200, 160, 160);
               StatusColor.R = 220;
               MinColorValue = 140;
               MaxColorValue = 240;
               ColorValue = 240;
           }
           else if (Cond == (int)Statuses.NoGrab)
           {
               StatusColor = new Color(240, 240, 160);
               StatusColor.G = 220;
               MinColorValue = 140;
               MaxColorValue = 240;
               ColorValue = 240;
           }
           else if (Cond == (int)Statuses.NoSpecial)
           {
               StatusColor = new Color(240, 180, 240);
               StatusColor.B = 200;
               MinColorValue = 140;
               MaxColorValue = 240;
               ColorValue = 240;
           }*/
           else if (Cond == (int)Statuses.StunDown)
           {
               mincolorvalue = 115;
               maxcolorvalue = 185;
               StatusColor = new Color(115, 185, 250);
           }
           else if (Cond == (int)Statuses.Rainbow)
           {
               StatusColor = new Color(255, 0, 0, 255);
           }

           StatusTint = new Fade((int)-ColorChangeRate, mincolorvalue, maxcolorvalue);

           UpdateTint();
           PrevDur = Main.GetActiveTime;
       }
       
       //Update's an object's status; StatForever is for objects that keep their Status but ignore its duration (Ex. Enemies spawning with a status)
       public void Update(bool StatForever)
       {
           //If the current Status Effect is None, there's no need to do anything
           if (Cond != (int)Statuses.None)
           {
               //Update the color of the Status Effect
               UpdateTint();

               //If the object has a Status Effect that lasts forever under certain circumstances, don't reset the Status
               //Otherwise, check if the Status should end and reset it if so
               if (StatForever == false && ShouldEnd == true)
               {
                   Reset();
               }
           }
       }

       //Updates the player's status, modifying stats and reverting the changes when the status ends
       public void PlayerStatUpdate()
       {
           if (Cond != (int)Statuses.None) 
               StatusReset();
       }

       //Updates the enemy's status, modifying stats and reverting the changes when the status ends if the enemy did not spawn with the status
       public void EnemyStatUpdate(bool StatForever)
       {
           if (StatForever == true)
               UpdateTint();
           else if (Cond != (int)Statuses.None) StatusReset();
       }

       //Update an object's status by changing its color according to the status it has and changing the tint value
       public void ObjectStatUpdate()
       {
           if (Cond == (int)Statuses.Invincible)
               StatusReset();
           else if (Cond != (int)Statuses.None)
               UpdateTint();
       }

       //Draws a symbol for the No-Statuses (NoJump, NoAttack, NoGrab, and NoSpecial) next to the object if the object has one of those status effects; these statuses do not tint the color of the object
       public void DrawNoStatusSymbol(SpriteBatch spriteBatch, Vector2 Location, float depth)
       {
           if (IsNoStatus() == true)
               spriteBatch.Draw(LoadGraphics.NoStatusSprites[Cond - (int)Statuses.NoJump], Location, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, depth);
       }

       public override String ToString()
       {
           return (StatName(Cond) + "," + (int)StatusDur + "," + Percentage);
       }
    }
}