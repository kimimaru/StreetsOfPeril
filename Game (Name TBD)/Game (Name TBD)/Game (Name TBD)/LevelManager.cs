﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class manages the creation of objects at certain points in a sublevel; only make one per camera
    //NOTE: It seems that this class will be obsolete since the LevelEditor will handle everything regarding levels; also, the Camera class can just store all of these lists
    public class LevelManager
    {
        //These are for the stopping points, enemy creation points, and container creation points in sublevels
        private List<ObjectSpawnPoint<Enemy>> EnemyCreatePoints;
        private List<ObjectSpawnPoint<ItemContainer>> ContainerCreatePoints;
        private List<ObjectSpawnPoint<Hazard>> HazardCreatePoints;
        private List<Vector2> StopPoints;

        //Constructor - difficulty just makes extra enemies with different stats and maybe makes more containers - check the value of Main.Difficulty when creating stuff
        public LevelManager(int subnum, int cnum, int levelnum = 0)
        {
            EnemyCreatePoints = new List<ObjectSpawnPoint<Enemy>>();
            ContainerCreatePoints = new List<ObjectSpawnPoint<ItemContainer>>();
            HazardCreatePoints = new List<ObjectSpawnPoint<Hazard>>();
            StopPoints = new List<Vector2>();
        }

        //Reading in from a saved level
        public LevelManager(LevelData level)
        {
            EnemyCreatePoints = level.EnemPoints;
            ContainerCreatePoints = level.ContPoints;
            HazardCreatePoints = level.HazardPoints;
            StopPoints = level.StopPoints;
        }

        public ObjectSpawnPoint<Enemy> CurrentEnem()
        {
            return EnemyCreatePoints[0];
        }

        public ObjectSpawnPoint<ItemContainer> CurrentContainer()
        {
            return ContainerCreatePoints[0];
        }

        public ObjectSpawnPoint<Hazard> CurrentHazard()
        {
            return HazardCreatePoints[0];
        }

        public Vector2 CurrentStop()
        {
            return new Vector2((Main.ScreenSize.X / 2) + StopPoints[0].X, (Main.ScreenSize.Y / 2) + StopPoints[0].Y);
        }

        public void ContinueEnem()
        {
            EnemyCreatePoints.RemoveAt(0);
        }

        public void ContinueCont()
        {
            ContainerCreatePoints.RemoveAt(0);
        }

        public void ContinueHazard()
        {
            HazardCreatePoints.RemoveAt(0);
        }

        public void ContinueStop()
        {
            StopPoints.RemoveAt(0);
        }

        public int EnemCount()
        {
            return EnemyCreatePoints.Count;
        }

        public bool EnemEmpty()
        {
            return (EnemyCreatePoints.Count <= 0);
        }

        public bool ContEmpty()
        {
            return (ContainerCreatePoints.Count <= 0);
        }

        public bool HazardEmpty()
        {
            return (HazardCreatePoints.Count <= 0);
        }

        public bool StopDone()
        {
            return (StopPoints.Count <= 0);
        }

        public bool Done()
        {
            return (EnemEmpty() == true && ContEmpty() == true && HazardEmpty() == true && StopDone() == true);
        }
    }

    //This struct is used for determining an object to create in the level and where to create it (camera position)
    //It's generic so it will work with any type of object (although enemies, item containers, and hazards are the only things that'll be spawned)
    public struct ObjectSpawnPoint<T>
    {
        //The stop point to spawn on (for levels with backtracking, spawning one batch of enemies after another on the "same" stop, or spawning enemies right after the camera unlocks)
        public int? StopNumber;
        public Vector2 AppearLocation;
        public T NewObject;
        public int Difficulty;
        public int MinPlayers;
        public bool Designated;

        public ObjectSpawnPoint(Vector2 appearlocation, T newobject, int? stopnumber = null, int difficulty = 0, int minplayers = 1, bool designated = true)
        {
            StopNumber = stopnumber;
            AppearLocation = new Vector2((Main.ScreenSize.X / 2) + appearlocation.X, (Main.ScreenSize.Y / 2) + appearlocation.Y);
            NewObject = newobject;
            Difficulty = difficulty;
            MinPlayers = minplayers;
            Designated = designated;
        }
    }
}
