﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //This class is for a barrel, crate, or other kind of item container
    //The item or weapon gets created after the barrel is destroyed, then it gets added to the level's item or weapon list
    public sealed class ItemContainer : BeatEmUpObj
    {
        //The numerical value of each type of item container
        public enum Containers
        {
            Barrel
        };

        //The ItemContainer's sprites
        private NewAnimation ContainerSprite;

        //The Z value is the height the container is above the ground - this can be used for hanging boxes or something!
        private Item HeldItem;
        private Weapon HeldWeapon;
        private PickupObj HeldObj;

        //Records amount of health the container needs to pass to change to a different broken sprite
        private readonly int SpriteChangeHealth;
        private int BreakHealth;
        private int BrokenCounter;

        public ItemContainer()
        {
            Alternate = 0;

            BrokenCounter = 0;

            BlocksMove = true;
            ObjType = ObjectType.Container;
            StatForever = true;

            //Default icon and spritesheets; default to Barrel
            Icon = LoadGraphics.BarrelIcon;
            SpriteSheet = LoadGraphics.ContainerSprite;

            //Item containers cannot hit anything, so they have no need for hitboxes
            Hitboxes = null;

            hud = new HUD(this, new Vector2(0, 0));
        }

        //Pass in a new Item for helditem and a new Weapon for heldweapon; ALWAYS make one of them null so two things don't spawn from the container
        public ItemContainer(Texture2D Sprites, String name, Vector3 location, int health, /*PickupObj heldobj*/Item helditem, Weapon heldweapon, int height, int alternate, bool hanging, params Vector2[] widthheights) : this()
        {
            Location = location;
            ObjectLength = (int)widthheights[0].X;
            ObjectHeight = (int)widthheights[0].Y;
            SpriteSheet = Sprites;
            ContainerSprite = new NewAnimation(SpriteSheet, new AnimFrame(new Rectangle(1, 3, 30, 49), 0f), new AnimFrame(new Rectangle(32, 0, 32, 52), 0f));
            SetUpHurtbox();

            SetUpHealth(health);
            BreakHealth = Health;
            SpriteChangeHealth = (int)Math.Ceiling(Health / (float)(ContainerSprite.MaxFrame + 1));

            HeldItem = helditem;
            HeldWeapon = heldweapon;
            //SetHeldObj(heldobj);
            Name = name;

            Alternate = alternate;

            //Hanging containers stay in the air and aren't affected by gravity
            UseGravity = !hanging;
        }

        public Item GetHeldItem
        {
            get { return HeldItem; }
        }

        public Weapon GetHeldWeapon
        {
            get { return HeldWeapon; }
        }

        public override int TotalDamageReceived(int damage)
        {
            //Get the total damage like normal
            int newdamage = base.TotalDamageReceived(damage);
        
            //Any damage dealt to an ItemContainer must deal at least 1 damage; this is so it can actually break
            if (newdamage <= 0) newdamage = 1;
            
            return newdamage;
        }

        //Spawns the item or weapon that was in the item container after it has been broken
        private void SpawnItem(List<Item> ItemList, List<Weapon> WeaponList, TileEngine TileEngine)
        {
            if (HeldItem != null && ItemList != null)
            {
                HeldItem.SetLocationTile(new Vector3(CollisionBox.Center.X, FeetLoc.Bottom - (HeldItem.CollisionBox.Height / 2), Location.Z), TileEngine);
                ItemList.Add(HeldItem);
            }
            if (HeldWeapon != null && WeaponList != null)
            {
                HeldWeapon.SetLocationTile(new Vector3(CollisionBox.Center.X, FeetLoc.Bottom - (HeldWeapon.CollisionBox.Height / 2), Location.Z), TileEngine);
                WeaponList.Add(HeldWeapon);
            }
        }

        //Checks whether to change sprites or not depending on the container's health
        //If Health gets below a value relative to its BreakHealth, switch to the next sprite or multiple sprites if enough damage was done
        //SpriteChangeHealth is determined by the max health and maximum number of sprites; for example, if a container had 15 health and 3 sprites, it would change sprites at 10 and 5 Health; a difference of 5
        //If the container instead had 2 sprites, it would change sprites at 7 Health, a difference of 7
        private void SpriteCheck()
        {
            //If the current sprite being displayed isn't the last one...
            if (BrokenCounter < ContainerSprite.MaxFrame)
            {
                //If the container was just broken, switch it to the last broken sprite it has
                if (IsDead == true) BrokenCounter = ContainerSprite.MaxFrame;
                else
                {
                    //Check if enough damage was done to change the sprite; the difference between BreakHealth and Health must match or exceed SpriteChangeHealth
                    int breakdifference = (BreakHealth - Health) / SpriteChangeHealth;

                    //If enough damage was done, subtract the SpriteChangeHealth by the number of sprite changes to get the next BreakHealth
                    //Also increase the BrokenCounter by the amount of times the sprite changed
                    if (breakdifference >= 1)
                    {
                        BreakHealth -= breakdifference * SpriteChangeHealth;
                        BrokenCounter += breakdifference;

                        //If the BrokenCounter is going to exceed the maximum number of sprites, set it to the maximum number of sprites
                        if (BrokenCounter > ContainerSprite.MaxFrame) BrokenCounter = ContainerSprite.MaxFrame;
                    }
                }

                //Whether the sprite changed or not, change the sprite to the current sprite and the max height to the current sprite's height
                ContainerSprite.SkipToFrame(BrokenCounter, false);
            }
        }

        protected override void AltTakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.AltTakeDamageActions(objecthitby, damage, hitbox);

            //Check for changing the ItemContainer's current sprite
            SpriteCheck();
        }

        //Sets the Container's HeldObj
        public void SetHeldObj(PickupObj heldobj)
        {
            HeldObj = heldobj;
        }

        private void SpawnHeldObj()
        {
            //If the container holds an item, spawn it
            if (HeldObj != null)
            {
                HeldObj.SetLocation(new Vector3(CollisionBox.Center.X, FeetLoc.Bottom - (HeldObj.CollisionBox.Height / 2), CurHeight));
            
                //Add the object to the item list if it's an item
                if (HeldObj.ObjType == BeatEmUpObj.ObjectType.Item)
                {
                    SubLvl.AddObject<Item>((Item)HeldObj, SubLvl.GetItems);
                }
                //Add the object to the weapon list if it's a weapon
                else if (HeldObj.ObjType == BeatEmUpObj.ObjectType.Weapon)
                {
                    SubLvl.AddObject<Weapon>((Weapon)HeldObj, SubLvl.GetWeapons);
                }
            }  
        }
        
        //Spawn the held object upon death
        public override void Die()
        {
            base.Die();

            //If the container holds an item, spawn it
            SpawnHeldObj();

            //Immediately removes the container's solidity so objects can walk through it while it's dead
            LoseSolidity();

            //Give the container the Invincible status so it blinks
            InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));
            LoadSounds.Play(LoadSounds.Break);
        }

        public void Break(float activeTime, int damage, Hitbox playerhitbox, List<Item> ItemList, List<Weapon> WeaponList, TileEngine TileEngine, List<BeatEmUpObj> Solids)
        {
            //Make every attack always do at least 1 damage to the item container
            Health -= damage <= 0 ? 1 : damage;
            SpriteCheck();

            if (IsDead == true)
            {
                Solids.Remove(this);
                SpawnItem(ItemList, WeaponList, TileEngine);
                status = new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0);
                LoadSounds.Play(LoadSounds.Break);
            }
        }

        //Moves the item container (used only in the Guess The Barrel bonus stage)
        public void PureMove(float X, float Y, float Z)
        {
            Location.X += X;
            Location.Y += Y;
            Location.Z += Z;
        }

        protected override void BeginFall()
        {
            base.BeginFall();

            //If the Container started falling while it was dead, restart its hud again
            if (IsDead == true) hud.Restart();
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            //Draw the container's current sprite
            ContainerSprite.Draw(spriteBatch, GetDrawLoc(cameraloc), false, status.GStatusColor, 0f, GetDrawDepth(cameraloc));
        }
    }
}