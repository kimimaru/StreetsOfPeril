﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The first sublevel for Level 7 - The entire level is underwater
    public class Sub7_1 : SubLevel
    {
        //Constructor
        public Sub7_1(Level curlevel, List<Player> players, Texture2D background, Texture2D foreground)
        {
            Players = players;
            BG = background;
            FG = foreground;

            CurLevel = curlevel;

            LevelData level = LevelEditor.LoadLevelOld("7-1");

            TileEngine = level.TileEngine;

            //LevelCamera = new Camera(BG, FG, new LevelManager(level));
        }

        //Resets all players at the start of the sublevel
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].IsCompletelyDead == true) continue;

                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, (i * 45) + 30, 250, 260, true);
                Players[i].UpdateAnimations();
            }
        }
    }
}