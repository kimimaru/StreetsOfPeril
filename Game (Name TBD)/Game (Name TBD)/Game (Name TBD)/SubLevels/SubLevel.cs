﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //This class acts as a section in the level
    //May have to make numerous inherited sublevels because they will all mostly be very different
    public class SubLevel
    {
        //Delegates for level entrances and exits
        protected Level.LevelTrans LevelEntrance;
        protected Level.LevelTrans LevelExit;

        protected Texture2D BG;
        protected Texture2D FG;
        protected TileEngine TileEngine;

        //Handles the intro and outro of sublevels
        protected Fade Fade;

        //The starting location and offset of the players
        protected Vector3[] StartLocations;
        protected Vector2 StartingOffset;

        //Tells when the sublevel is finished, excluding exit transitions
        protected bool SubLevelFinished;

        //Player instances - players are inside the level
        protected List<Player> Players;

        //Camera for the level
        protected Camera LevelCamera;

        //Enemy-Item-Weapon lists - used to easily keep track of all enemies, items, and weapons
        protected List<Enemy> EnemList;
        protected List<ItemContainer> ContainerList;
        protected List<Item> ItemList;
        protected List<Weapon> WeaponList;

        //Solid objects that things will be blocked by and can be jumped on
        protected List<BeatEmUpObj> Solids;

        protected List<Hazard> Hazards;

        //Handles the updating and drawing of all effects in the SubLevel, such as Fighters hitting each other, explosions, or anything else
        protected EffectsManager EffectManage;

        //Handles object collisions
        protected NewCollision Collisions;

        //Current level reference
        protected Level CurLevel;

        protected SubLevel()
        {
            Fade = new Fade(-5, 0, 255);

            LevelEntrance = Level.DefaultEnterExit;
            LevelExit = Level.DefaultEnterExit;

            SubLevelFinished = false;

            EnemList = new List<Enemy>();
            ContainerList = new List<ItemContainer>();
            ItemList = new List<Item>();
            WeaponList = new List<Weapon>();

            Solids = new List<BeatEmUpObj>();

            Hazards = new List<Hazard>();

            EffectManage = new EffectsManager();
            Collisions = new NewCollision(this);
        }

        //Constructor
        public SubLevel(SublevelProp level, Level curlevel, List<Player> players) : this()
        {
            Players = players;
            BG = LoadGraphics.BG;

            CurLevel = curlevel;

            LoadData(level);
        }

        protected void LoadData(SublevelProp level)
        {
            StartLocations = new Vector3[Player.MaxPlayers];

            for (int i = 0; i < level.StartingLocations.Length; i++)
                StartLocations[i] = level.StartingLocations[i].Location;

            StartingOffset = level.StartingOffset;

            Level.LevelTrans[] entrances = new Level.LevelTrans[] { Level.DefaultEnterExit, Level.EnterRight, Level.EnterLeft, Level.EnterUp, Level.EnterDown };
            Level.LevelTrans[] exits = new Level.LevelTrans[] { Level.DefaultEnterExit, Level.ExitRight, Level.ExitLeft, Level.ExitUp, Level.ExitDown };

            LevelEntrance = entrances[level.LevelEntrance];
            LevelExit = exits[level.LevelExit];

            TileEngine = level.TileEngine;

            //Make the level underwater if it's supposed to be
            if (level.Underwater == true) TileEngine.EditTileValuesAll(999f, (int)TileEngine.TileTypes.Water);

            if (level.CameraType == false) LevelCamera = new Camera(BG, FG, level);
            else LevelCamera = new LCamera(BG, level.CameraRange);
        }

        //Accessors and Manipulation Procedures
        protected virtual bool IsCleared()
        {
            //Check if there are no more enemies left in the level and if the camera is done scrolling
            if (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false)
            {
                return true;
            }

            return false;
        }

        public List<Player> GetPlayers
        {
            get { return Players; }
        }

        public List<Enemy> GetEnemies
        {
            get { return EnemList; }
        }

        public List<ItemContainer> GetContainers
        {
            get { return ContainerList; }
        }

        public List<Item> GetItems
        {
            get { return ItemList; }
        }

        public List<Weapon> GetWeapons
        {
            get { return WeaponList; }
        }

        public List<Hazard> GetHazards
        {
            get { return Hazards; }
        }

        public Camera GetCamera
        {
            get { return LevelCamera; }
        }

        public List<BeatEmUpObj> GetSolids
        {
            get { return Solids; }
        }

        public bool HasCamera
        {
            get { return (LevelCamera != null); }
        }

        public Vector2 GCameraOffSet
        {
            get
            {
                if (HasCamera == false) return Vector2.Zero;
                else return LevelCamera.CameraDrawLoc;
            }
        }

        public TileEngine GetTileEngine
        {
            get { return TileEngine; }
        }

        //The location for players to respawn in the sublevel when they lose a life or continue
        public virtual Vector3 RespawnLoc()
        {
            return new Vector3(120 - GCameraOffSet.X, 180 - GCameraOffSet.Y, -1);
        }

        public virtual bool SubLevelComplete()
        {
            return (SubLevelFinished == true && Fade.Finished() == true);
        }

        public virtual bool CanPause()
        {
            return (SubLevelFinished == false && Fade.IsFading() == false);
        }

        public Fade GFade()
        {
            return Fade;
        }

        //Starts the sublevel after the LevelEntrance is complete
        protected virtual void StartSub(float activeTime)
        {
            if (CurLevel != null) CurLevel.ResumeTime(activeTime);
        }

        //Ends the sublevel before the LevelExit starts
        protected virtual void EndSub(float activeTime, Main main)
        {
            if (CurLevel != null) CurLevel.StopTime(activeTime);
        }

        protected virtual void SetSpawnStats(Player player)
        {

        }

        //Adds an object to the Sublevel in the specified list and spawns it
        public void AddObject<T>(T beatemupobj, List<T> objlist) where T : BeatEmUpObj
        {
            //Make sure we don't have a null object or list
            if (beatemupobj != null && objlist != null)
            {
                beatemupobj.Spawn(this);
                LevelSpawn(beatemupobj);
                objlist.Add(beatemupobj);
            }
            else Debug.OutputValue("beatemupobj or objlist is null!");
        }

        //Adds a collection of objects to the Sublevel in the specified list and spawns all of them
        public void AddObjectList<T>(List<T> beatemupobjlist, List<T> designatedlist) where T : BeatEmUpObj
        {
            //Go through all objects and add them to the designated list
            for (int i = 0; i < beatemupobjlist.Count; i++)
                AddObject(beatemupobjlist[i], designatedlist);
        }

        //Level-specific spawning on objects
        public virtual void LevelSpawn(BeatEmUpObj beatemupobj)
        {

        }

        //Updates a list of BeatEmUpObjs
        public void UpdateList<T>(List<T> beatemupobjlist) where T : BeatEmUpObj
        {
            for (int i = 0; i < beatemupobjlist.Count; i++)
            {
                beatemupobjlist[i].Update();
                if (beatemupobjlist[i].ShouldRemove == true)
                {
                    //We should remove the object, so despawn it and remove it
                    beatemupobjlist[i].Despawn();
                    beatemupobjlist.RemoveAt(i);
                    i--;
                }
            }
        }

        //Check if an object touched a solid in the X direction
        public BeatEmUpObj TouchedSolidX(BeatEmUpObj beatemupobj, int objvelocityx)
        {
            for (int i = 0; i < Solids.Count; i++)
            {
                BeatEmUpObj solid = Solids[i];

                //Make sure the object checking isn't the solid
                if (beatemupobj != solid)
                {
                    //Check if the object touched the solid in the X direction
                    if (solid.TouchedX(beatemupobj, objvelocityx) == true) return solid;
                }
            }

            return null;
        }

        //Check if an object touched a solid in the Y direction
        public BeatEmUpObj TouchedSolidY(BeatEmUpObj beatemupobj, int objvelocityy)
        {
            for (int i = 0; i < Solids.Count; i++)
            {
                BeatEmUpObj solid = Solids[i];

                //Make sure the object checking isn't the solid
                if (beatemupobj != solid)
                {
                    //Check if the object touched the solid in the Y direction
                    if (solid.TouchedY(beatemupobj, objvelocityy) == true) return solid;
                }
            }

            return null;
        }

        //Check if an object jumped underneath a solid
        public BeatEmUpObj JumpedUnderSolid(BeatEmUpObj beatemupobj)
        {
            for (int i = 0; i < Solids.Count; i++)
            {
                BeatEmUpObj solid = Solids[i];

                //Make sure the object checking isn't the solid
                if (beatemupobj != solid)
                {
                    //Check if the object jumped and hit its head underneath the solid
                    if (solid.JumpedUnder(beatemupobj) == true) return solid;
                }
            }

            return null;
        }

        //Check if an object is on a solid
        public BeatEmUpObj IsOnSolid(BeatEmUpObj beatemupobj)
        {
            for (int i = 0; i < Solids.Count; i++)
            {
                BeatEmUpObj solid = Solids[i];

                //Make sure the object checking isn't the solid
                if (beatemupobj != solid)
                {
                    //Check if the object is on the solid
                    if (solid.IsOn(beatemupobj) == true) return solid;
                }
            }

            return null;
        }

        //Spawns all players at the start of the sublevel
        public virtual void SpawnPlayers(float activeTime, bool first = false)
        {
            if (StartLocations == null || StartLocations.Length < Player.MaxPlayers) 
            {
                StartLocations = new Vector3[Player.MaxPlayers];
                for (int i = 0; i < StartLocations.Length; i++) 
                    StartLocations[i] = new Vector3(-50, 100 + (i * 40), 0f);
            }

            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, StartLocations[Players[i].GPlayerNum].X + StartingOffset.X, StartLocations[Players[i].GPlayerNum].Y + StartingOffset.Y, StartLocations[Players[i].GPlayerNum].Z, first);
                SetSpawnStats(Players[i]);

                //Add the player's weapon to the weapon list if the player brought one from another sublevel
                if (Players[i].weapon != null)
                {
                    WeaponList.Add(Players[i].weapon);

                    //Reset the weapon's HUD so the weapon information isn't on screen when fading back in
                    Players[i].weapon.hud.EndDuration();
                }
            }

            LevelCamera.Update(this);
        }

        protected virtual void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            //if (Fade.Finished() == true) SubLevelFinished = true;
            if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
            //else if (curlevel.GetTimer != null && Fade.IsFading() == false && Fade.IsFadingIn() == false) curlevel.GetTimer.Restart(activeTime, curlevel.GetTimer.Time);
        }

        //Checks if any player is dead at the end of the level to prevent the zombie glitch
        protected bool CheckPlayerDead()
        {
            //If a player is dead, wait for him/her to respawn before ending the level to prevent the "Zombie Glitch"
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].IsDead == true) return true;
            }

            return false;
        }

        //Adds to the counter tracking the number of lives lost; separated because it makes checking for rank mode easier
        protected void LostLife(int index, bool completelydead)
        {
            if (CurLevel != null) CurLevel.LostLife(index, completelydead);
        }

        //Ends the sublevel, fading out the screen and doing whatever else may be needed
        protected virtual void EndSubLevel()
        {
            Fade.ContinueFade();

            //If player icons are completely transparent at the end of the level (Ex. Invincible status), make them opaque
            for (int i = 0; i < Players.Count; i++) Players[i].status.SetColorOpaque();
        }

        protected virtual void OnEnemyDeath(ref int index)
        {
            Enemy deadenemy = EnemList[index];

            //Update bonus status
            if (deadenemy.gLastAttacked != null)
            {
                if (deadenemy.gLastAttacked.GetBonusStatus != null)
                {
                    deadenemy.gLastAttacked.GetBonusStatus.IncreaseChanceStatus(deadenemy.PossibleStatuses, deadenemy.StartingStatus, deadenemy.DeathStatus);
                    deadenemy.gLastAttacked.GetBonusStatus.ChangeBonusMeter(BonusStatus.AmountAdded);
                }
            }

            //Check if the enemy is in Commander's list, and remove it if so
            Commander.RemoveMinionLevel(EnemList, deadenemy);

            if (LevelCamera != null) LevelCamera.CheckRemoveDesignated(deadenemy.Designated);
            EnemList.RemoveAt(index);
            index--;
        }

        //Update all enemies
        protected virtual void UpdateEnemyList(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                EnemList[i].Update();

                //Check if the enemy is dead
                if (EnemList[i].ShouldRemove == true)
                {
                    OnEnemyDeath(ref i);
                }
            }
        }

        //Updates all item containers
        protected virtual void UpdateContainerList(float activeTime)
        {
            for (int i = 0; i < ContainerList.Count; i++)
            {
                ContainerList[i].Update();

                if (ContainerList[i].ShouldRemove == true)
                {
                    ContainerList[i].Despawn();
                    ContainerList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Update all items
        protected virtual void UpdateItemList(float activeTime)
        {
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].Update();

                if (ItemList[i].ShouldRemove == true)
                {
                    ItemList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Update all weapons
        protected virtual void UpdateWeaponList(float activeTime)
        {
            for (int i = 0; i < WeaponList.Count; i++)
            {
                WeaponList[i].Update();

                //If it's not picked up and it should disappear and it's on the ground, remove it
                //Also remove the weapon if it's thrown and passed a certain point
                if (WeaponList[i].ShouldRemove == true)
                {
                    WeaponList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Updates all hazards
        protected virtual void UpdateHazards(float activeTime)
        {
            for (int i = 0; i < Hazards.Count; i++)
            {
                Hazards[i].Update();

                if (Hazards[i].ShouldRemove == true)
                {
                    Hazards[i].Despawn();
                    Hazards.RemoveAt(i);
                    i--;
                }
            }
        }

        protected virtual void OnPlayerDeath(ref int index)
        {
            Player deadplayer = Players[index];

            deadplayer.Respawn(Main.GetActiveTime, RespawnLoc(), TileEngine);
            LostLife(index, deadplayer.IsCompletelyDead);
            if (deadplayer.IsCompletelyDead == true)
            {
                Players.RemoveAt(index);
                index--;
            }
        }

        //Update all players
        protected virtual void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Update();

                //Check if a player is dead; if so, restart the timer and respawn the player in the correct location
                if (Players[i].IsDead == true && Players[i].KnockedDown == false)
                {
                    OnPlayerDeath(ref i);
                }
            }
        }

        //Update the camera
        protected virtual void UpdateCamera(float activeTime)
        {
            if (LevelCamera != null) LevelCamera.Update(this);
        }

        //Perform collisions
        protected virtual void UpdateCollisions(float activeTime)
        {
            Collisions.PerformCollisions();
        }

        //Updates anything else that the sublevel may need
        protected virtual void UpdateOther(float activeTime)
        {

        }

        //Updates anything else that the sublevel may need when the sublevel isn't completed yet
        protected virtual void UpdateOtherNotComplete(float activeTime)
        {

        }

        //Updates anything else that the sublevel may need when the sublevel is already completed
        protected virtual void UpdateOtherComplete(float activeTime)
        {

        }

        //This is needed to prevent animations from messing up at the start of a level
        protected virtual void UpdatePlayerAnimations(float activeTime)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].UpdateAnimations();

                //This updates players' weapons during a transition so they continue following them
                if (Players[i].weapon != null) Players[i].weapon.Update();
            }
        }

        public virtual void Update(float activeTime, Main main)
        {
            //Don't update if you have to draw the sublevel intro/outro
            if (Fade.IsFading() == false)
            {
                //Check whether to pause or not
                //main.PauseGame(Players);
                //if (Main.IsPaused == true) return;

                //Check if the sublevel is cleared
                if (SubLevelFinished == false)
                {
                    //Check if the sublevel is cleared and no players are dead
                    if (IsCleared() == true && CheckPlayerDead() == false)
                    {
                        //Mark that the sublevel is complete
                        SubLevelFinished = true;

                        //Do whatever needs to be done when the sublevel is complete (stop timer if in Rank Mode, etc.)
                        EndSub(activeTime, main);
                        return;
                    }
                }
                else
                {
                    //Move players at the end of the sublevel
                    if (LevelExit != null)
                    {
                        LevelExit(activeTime, ref LevelExit, Players, GCameraOffSet, TileEngine, Solids, StartLocations);

                        //If the level exit is done... 
                        if (LevelExit == null)
                        {
                            //Start fading out and perform other actions (give back control to players, etc.)
                            EndSubLevel();
                            return;
                        }
                    }
                }

                //Move players at the start of the first sublevel
                if (LevelEntrance != null)
                {
                    LevelEntrance(activeTime, ref LevelEntrance, Players, GCameraOffSet, TileEngine, Solids, StartLocations);

                    //If the level entrance is done, perform actions
                    if (LevelEntrance == null)
                        StartSub(activeTime);
                }

                //Update level camera
                UpdateCamera(activeTime);

                //Update lists
                UpdateEnemyList(activeTime);
                UpdateContainerList(activeTime);
                UpdateItemList(activeTime);
                UpdateWeaponList(activeTime);
                UpdateHazards(activeTime);

                //Update players after those to let certain things occur first
                UpdatePlayers(activeTime);

                //Perform collision checks
                UpdateCollisions(activeTime);

                //Does anything that needs to be done when the sublevel isn't complete
                UpdateOtherNotComplete(activeTime);

                //Check whether to pause or not
                main.PauseGame(Players);
                //if (Main.IsPaused == true) return;
            }
            else
            {
                Transition(activeTime);

                //Does anything that needs to be done when the sublevel is complete
                UpdateOtherComplete(activeTime);
            }

            //Does anything that needs to be done regardless of whether the sublevel is complete or not
            UpdateOther(activeTime);
        }

        protected virtual void DrawEnemyList(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                EnemList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected virtual void DrawContainerList(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < ContainerList.Count; i++)
            {
                ContainerList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected virtual void DrawItemList(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected virtual void DrawWeaponList(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < WeaponList.Count; i++)
            {
                WeaponList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected virtual void DrawHazards(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Hazards.Count; i++)
                Hazards[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
        }

        protected virtual void DrawPlayers(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected virtual void DrawCamera(SpriteBatch spriteBatch)
        {
            if (LevelCamera != null)
                LevelCamera.Draw(spriteBatch, GCameraOffSet);
        }

        protected virtual void DrawTileEngine(SpriteBatch spriteBatch)
        {
            TileEngine.Draw(spriteBatch, GCameraOffSet);
        }

        //Draws other things that the sublevel may need
        protected virtual void DrawOther(SpriteBatch spriteBatch)
        {

        }

        public void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            DrawCamera(spriteBatch);
            DrawEnemyList(activeTime, spriteBatch);
            DrawContainerList(spriteBatch);
            DrawItemList(spriteBatch);
            DrawWeaponList(spriteBatch);
            DrawHazards(spriteBatch);
            DrawTileEngine(spriteBatch);
            DrawPlayers(activeTime, spriteBatch);

            DrawOther(spriteBatch);

            Fade.Draw(spriteBatch);

            #if DEBUG
                DebugDraw(spriteBatch);
            #endif
        }

        protected void DebugDraw(SpriteBatch spriteBatch)
        {
            if (Debug.TileEngineDraw == true)
            {
                Vector2 position = Vector2.Zero;
                if (HasCamera == true) position = -GCameraOffSet;
                TileEngine.DrawHeight(spriteBatch, position, -position);
            }
        }
    }
}