﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The fourth sublevel for Level 3 - on top of the train
    public class Sub3_4 : SubLevel
    {
        //Scrolling background
        private ParaScroll ParallaxScroll;

        //The location of the train
        private Vector2 TrainLocation;

        private const float ExclaimTimer = 1000f;
        private float PrevExclaim;

        private bool Jumped;
        private float PrevJump;

        public Sub3_4(Level curlevel, List<Player> players)
        {
            Players = players;

            LevelData level = LevelEditor.LoadLevel("3-4").ConvertToLevelData();//LevelEditor.LoadLevelOld("3-4");
            TileEngine = level.TileEngine;

            LevelExit = ExitTrain;

            CurLevel = curlevel;

            TrainLocation = new Vector2(0, Main.ScreenHalf.Y);

            PrevExclaim = 0f;

            Jumped = false;
            PrevJump = 0f;

            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));
            ParallaxScroll = new ParaScroll(LoadGraphics.BG, new Vector2(1, 0), true, null);
        }

        //Accessors and Manipulation Procedures
        protected override bool IsCleared()
        {
            //Check if the players reached the end of the level
            if (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false && CheckPlayerDead() == false)
            {
                return true;
            }

            return false;
        }

        private bool PlayersOffTrain()
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].CurHeight > -70) return false;
            }

            return true;
        }

        //Custom level exit for this level
        private void ExitTrain(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            //Check if all of the characters jumped off the train and are below a certain height
            if (Jumped == true && PlayersOffTrain() == true)
            {
                //Set all of the players inactive so they don't die
                for (int i = 0; i < Players.Count; i++)
                    Players[i].SetActive(false);

                //Scroll the camera and move the train to show it going into the tunnel (not fully implemented yet)
                //GCameraOffSet.X -= 2;
                TrainLocation.X += 3;
                if (TrainLocation.X > (Main.ScreenSize.X + TileEngine.TileSize)) LevelExit = null;
            }
            //Move the players to the right spots to jump
            else if (Jumped == false && Main.GetActiveTime > PrevExclaim)
            {
                bool Done = true;

                //Move the players downwards
                for (int i = 0; i < Players.Count; i++)
                {
                    Rectangle feetloc = Players[i].FeetLoc;

                    //Players[i].DoAction(activeTime, Offset, TileEngine, Solids, 0, new Vector2(0, feetloc.Y <= (Main.ScreenSize.Y - feetloc.Height) ? 1 : 0));

                    //Since the player moved, reassign the variable
                    feetloc = Players[i].FeetLoc;

                    if (feetloc.Y < (Main.ScreenSize.Y - feetloc.Height - Players[i].TrueVelocity.Y)) Done = false;
                    //else Players[i].DoAction(activeTime, Offset, TileEngine, Solids, 0, Vector2.Zero);
                }

                //If everyone is in the proper places, they can jump
                if (Done == true)
                {
                    Jumped = true;
                    PrevJump = activeTime;
                }
            }
            //Make the players wait a second then jump
            else if (Jumped == true && (activeTime - PrevJump) >= 1000f)
            {
                //Make the characters jump
                for (int i = 0; i < Players.Count; i++)
                {
                    //Players[i].DoAction(activeTime, Offset, TileEngine, Solids, 1, Vector2.Zero);

                    //Make sure the characters don't go onto a tile that doesn't exist
                    if (TileEngine.TileYExists(Players[i].FeetLoc, new Vector2(0, Players[i].TrueVelocity.Y)) == true && Players[i].CurHeight != 0)
                    {
                        //Stop the characters so they get to the right spots
                        if (Players[i].FeetLoc.Y < (Main.ScreenSize.Y + TileEngine.TileSize))
                        {
                            //Make the characters move fast enough to go over one tile by the time they land; the tile right under them has to be used as a wall to prevent enemies from dying if they walk underneath players
                            Players[i].PureMove(0, 1.5f);
                        }
                        //else if (Players[i].GetObjectTile.Z >= 0f) Players[i].GetObjectTile = TileEngine.CurTile(Players[i].FeetLoc());
                    }
                }
            }
        }

        //The location for players to respawn in the minecart level when they lose a life or continue
        public override Vector3 RespawnLoc()
        {
            return new Vector3(200 - GCameraOffSet.X, 220 - GCameraOffSet.Y, -1);
        }

        //Set the exclamation timer, remove control from players, and have them look to the right
        protected override void EndSub(float activeTime, Main main)
        {
            base.EndSub(activeTime, main);
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].ChangeDirection(true);
                Players[i].ChangeControl(false);
            }

            PrevExclaim = Main.GetActiveTime + ExclaimTimer;
        }

        //Resets all players at the start of the sublevel
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].IsCompletelyDead == true) continue;

                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 200, 220, 0f, true);
            }
        }

        protected override void UpdateCamera(float activeTime)
        {
            //Update the scrolling background
            ParallaxScroll.Update();

            base.UpdateCamera(activeTime);
        }

        protected override void DrawCamera(SpriteBatch spriteBatch)
        {
            ParallaxScroll.Draw(spriteBatch);
            spriteBatch.Draw(LoadGraphics.FG, new Rectangle((int)LevelCamera.CameraDrawLoc.X, 200, LoadGraphics.FG.Width, LoadGraphics.FG.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.0001f);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadGraphics.enemsprite, TrainLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            if (SubLevelFinished == true && Main.GetActiveTime < PrevExclaim)
            {
                for (int i = 0; i < Players.Count; i++)
                    Players[i].DrawExpression(spriteBatch, GCameraOffSet, 0);
            }
        }
    }
}
