﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The third sublevel for Level 6 - The minecart rush
    //Enemies fall from above and come from the sides in minecarts, while Minecart Riders come from the sides and throw an explosive projectile onto the player's minecart!
    //There will be a time where are a lot of Minecart Riders are throwing these projectiles, so the players better be good at dodging!
    //Item containers will come in the form of hanging barrels that (functionally move to the left) players can break, and players and enemies can jump off or be thrown off the minecart, which will result in death/enormous damage (May be changed with playtesting)
    //You can change the location the player minecart is at (simulate it speeding up or slowing down) by changing the OffSet
    public sealed class Sub6_3 : SubLevel
    {
        //The scrolling background
        private ParaScroll ParallaxScroll;

        //The minecart
        private Animation MineCart;
        private Vector2 MineCartPos;

        //The maximum number of enemies that can be spawned at once - increases over time
        private int MaxEnemies;

        //The time before new enemies fall onto the minecart
        private float EnemySpawnTime;

        //Constructor
        public Sub6_3(Level curlevel, List<Player> players, Texture2D background, Texture2D foreground)
        {
            Players = players;
            BG = background;
            FG = foreground;
            //OffSet = new Vector2(-100, 0);

            CurLevel = curlevel;

            MineCart = new Animation(LoadGraphics.BagOfJewels, false, 4, 300);
            MineCartPos = new Vector2(130, 166);

            LevelExit = MinecartLeave;

            LevelData level = LevelEditor.LoadLevelOld("6-3");

            //Manage = new LevelManager(level);
            TileEngine = LevelEditor.LoadLevelOld("6-3").TileEngine;

            MaxEnemies = 3;
            EnemySpawnTime = 0f;
            ParallaxScroll = new ParaScroll(BG, new Vector2(2, 0), true, null);
        }

        //Accessors and Manipulation Procedures
        //NOTE: Make it so the level also isn't complete until players aren't about to jump or in the air; killing themselves during the outro looks weird
        protected override bool IsCleared()
        {
            //Check if there are no more enemies left in the level
            if (/*Manage.EnemEmpty() == true && */EnemList.Count == 0 && PlayersNotInAir() == true)
            {
                return true;
            }

            return false;
        }

        //Checks if all players aren't in the air or about to jump
        private bool PlayersNotInAir()
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].IsInAir == true || Players[i].FeetLoc.Intersects(MineCartRect) == false) return false;
            }

            return true;
        }

        //Custom level exit for the minecart level
        private void MinecartLeave(float activeTime, ref Level.LevelTrans levelentranceexit, List<Player> Players, Vector2 Offset, TileEngine TileEngine, List<BeatEmUpObj> Solids, Vector3[] destinationloc)
        {
            //OffSet.X += 2;

            //The minecart reached the end
            if (GCameraOffSet.X >= 175)
            {
                levelentranceexit = null;
            }
        }

        //The rectangle that players and enemies stay alive in; unless they're on minecarts, if they go outside this rectangle they take damage that hurts them severely (or kills them)
        private Rectangle MineCartRect
        {
            get { return new Rectangle(128, 128, 160, 128); }
        }

        //The location for players to respawn in the minecart level when they lose a life or continue
        public override Vector3 RespawnLoc()
        {
            return new Vector3(MineCartPos.X - GCameraOffSet.X, MineCartPos.Y - GCameraOffSet.Y, -1);
        }

        //Starts the sublevel after the LevelEntrance is complete
        protected override void StartSub(float activeTime)
        {
            base.StartSub(activeTime);
        }

        //Ends the sublevel before the LevelExit starts
        protected override void EndSub(float activeTime, Main main)
        {
            base.EndSub(activeTime, main);

            //Take away control from the player
            for (int i = 0; i < Players.Count; i++) Players[i].ChangeControl(false);
        }

        //Resets all players at the start of the sublevel
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            Vector2[] StartLocations = new Vector2[] { new Vector2(120, 140), new Vector2(156, 140), new Vector2(120, 200), new Vector2(156, 200) };

            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].IsCompletelyDead == true) continue;

                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, StartLocations[i].X, StartLocations[i].Y);
            }
        }

        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.IsFading() == false && Fade.IsFadingIn() == false)
            {
                EnemySpawnTime = activeTime;
                //if (curlevel.GetTimer != null) curlevel.GetTimer.Restart(activeTime, curlevel.GetTimer.Time);
            }
            else UpdatePlayerAnimations(activeTime);//UpdatePlayers(activeTime, curlevel);
        }

        //Ends the sublevel, fading out the screen and doing whatever else may be needed
        protected override void EndSubLevel()
        {
            base.EndSubLevel();

            //Give control back to the players since the level is complete
            for (int i = 0; i < Players.Count; i++)
                Players[i].ChangeControl(true);
        }

        //Update all enemies - Make them have a smaller JumpVelocityChange and slower movement speed
        protected override void UpdateEnemyList(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                //Make an enemy die when landing outside a minecart - that is, every enemy except those who jump from minecarts and minecart riders 
                if (EnemList[i].FeetLoc.Intersects(MineCartRect) == false && (/*EnemList[i].gsAction.//Minecart behavior here! &&*/ !(EnemList[i] is MinecartRider)))
                {
                    if (EnemList[i].CurHeight == EnemList[i].ObjectTile.Z)
                    {
                        if (EnemList[i].IsDead == false)
                        {
                            LoadSounds.Play(LoadSounds.Kick);
                            EnemList[i].Die();
                            EnemList[i].ChangeDirection(true);
                            EnemList[i].Jump(-7f, 0f, 6f, false);
                            EnemList[i].ResetKnockDownAnim();
                        }
                        else EnemList[i].ObjectMove(new Vector2(-7, 0));
                    }
                }

                EnemList[i].Update();

                if (EnemList[i].IsDead == true && EnemList[i].KnockedDown == false && EnemList[i].hud.IsInactive == true)
                {
                    //Give the player points for killing the enemy; if a Minecart Rider goes off the side, don't give the player points
                    //if (EnemList[i].gHealth <= 0 && EnemList[i].gPlayerIndex > Enemy.NoPlayerIndex)
                    //    Players[EnemList[i].gPlayerIndex].PointsUpdate(EnemList[i].gScore);
                    EnemList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Update all players
        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                //Make a player die when landing outside the minecart
                //If the player hasn't died from the minecart tracks, kill the player and launch him or her to the left very fast and very high
                //If the player is already dead and didn't touch the minecart tracks before and touches them, do the same
                //If the player is on the floor after getting launched, make the player move left
                if (Players[i].FeetLoc.Intersects(MineCartRect) == false)
                {
                    if (Players[i].CurHeight == Players[i].ObjectTile.Z)
                    {
                        if (Players[i].IsDead == false)
                        {
                            LoadSounds.Play(LoadSounds.Kick);
                            //Players[i].Die();
                            Players[i].ChangeDirection(true);
                            //Players[i].SetJumpVelocity(-7, 6);
                            Players[i].ResetKnockDownAnim();
                        }
                        else Players[i].ObjectMove(new Vector2(-7, 0));
                    }
                }

                Players[i].Update();

                //Check if a player is dead; if so, restart the timer and respawn the player in the correct location
                if (Players[i].IsDead == true && Players[i].KnockedDown == false)
                {
                    Players[i].Respawn(activeTime, RespawnLoc(), TileEngine);
                    LostLife(i, Players[i].IsCompletelyDead);
                    if (Players[i].IsCompletelyDead == true)
                    {
                        Players.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        protected override void UpdateOther(float activeTime)
        {
            //Update the scrolling background
            ParallaxScroll.Update();

            MineCart.Update(activeTime);
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            //Make new enemies spawn and fall onto the minecart
            if (EnemList.Count < MaxEnemies && (activeTime - EnemySpawnTime) >= 1500f)
            {
                //if (false)//Manage.EnemEmpty() == false)
                //{
                    EnemySpawnTime = activeTime;
                    EnemList.Add(null);//Manage.CurrentEnem().NewObject);
                    int curenemy = EnemList.Count - 1;

                    //If the enemy is a minecart rider, make it spawn from the side
                    //NOTE: When integrating the new AI system into the other enemies, check their action and location in their constructors to see which direction they should face
                    //For adding the hazard, simply set their CurrentAction to MoveTo in their constructors if they're supposed to spawn with a "jump from minecart" behavior
                    if ((EnemList[curenemy].gsAction.CurrentAction as MoveTo) != null)
                    {
                        EnemList[curenemy].ChangeDirection(EnemList[curenemy].GetLocationHeight.X < (TileEngine.TileSize * 4) ? false : true);
                        Hazards.Add(new Minecart(EnemList[curenemy], 2f));
                    }
                    //Manage.ContinueEnem();

                    //if (Manage.EnemCount() == 2)
                    //    MaxEnemies += 2;
                //}
            }
        }

        protected override void UpdateOtherComplete(float activeTime)
        {
            //Move the minecart and players to the center of the screen in the beginning and the right side at the end
            //OffSet.X += 2;
        }

        protected override void DrawCamera(SpriteBatch spriteBatch)
        {
            ParallaxScroll.Draw(spriteBatch);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            MineCart.Draw(spriteBatch, MineCartPos + GCameraOffSet, .2f);
        }
    }
}
