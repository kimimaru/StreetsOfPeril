﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An object that can be picked up by Fighters, including Items and Weapons
    public abstract class PickupObj : BeatEmUpObj
    {
        //The Fighter that picked up this object
        protected Fighter fighter;

        public PickupObj()
        {
            fighter = null;
        }

        public bool IsPickedUp
        {
            get { return (fighter != null); }
        }

        //Gets the Fighter that is in possession of this object
        public Fighter GetOwner
        {
            get { return fighter; }
        }

        //Called when the PickupObj is dropped
        protected void ClearFighter()
        {
            fighter = null;
        }

        //Checks if this object can be picked up by a Fighter
        public virtual bool CanBePickedUp(Fighter fight)
        {
            return (IsDead == false && IsPickedUp == false && CurHeight == fight.CurHeight && CollisionBox.Intersects(fight.CollisionBox) == true);
        }

        //Have the Fighter pickup the object
        public virtual void GetPickedUp(Fighter fight)
        {
            //Set the fighter reference to the one passed in
            fighter = fight;
        }
    }
}
