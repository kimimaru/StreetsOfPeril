﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A class for animating a series of sprites using a spritesheet
    //All animations play forward by default; if you want the opposite to occur, simply call the ReverseAnimation() method or create a new animation with reversed frames
    public sealed class NewAnimation
    {
        //The percent speed modifier that animations play at when in water and when an object has the StunDown status
        public const float WaterSlow = 1.15f;
        public const float StunDownSpeed = .5f;

        //The spritesheet; this is optional as long as you pass in a spritesheet to draw in the Draw overload
        private Texture2D SpriteSheet;

        //The animation frames
        private List<AnimFrame> Frames;

        //The current frame
        private int CurFrame;

        //How many times the animation loops
        private int MaxLoops;
        private int CurLoop;

        //Tells if the animation is affected by water or not
        private bool WaterAffected;

        //The full duration of the animation, excluding water
        private float FullAnimDuration;

        //Tells if the animation reverses or not after finishing
        private bool ShouldReverse;
        private bool CurReverse;

        //Tells if the animation is finished or not
        private bool AnimationEnd;

        private NewAnimation()
        {
            Frames = new List<AnimFrame>();
            FullAnimDuration = 0;

            CurFrame = 0;
            CurLoop = 0;
            WaterAffected = true;

            AnimationEnd = false;

            ShouldReverse = false;
            CurReverse = false;
        }

        public NewAnimation(Texture2D spritesheet, params AnimFrame[] frames) : this()
        {
            SpriteSheet = spritesheet;
            Frames.AddRange(frames);

            //Get the full duration here to cut down on time later (Ex. creating hitboxes that last an entire animation)
            //Also set all the frames animations to this one
            for (int i = 0; i < Frames.Count; i++)
            {
                Frames[i].SetAnimation(this);
                FullAnimDuration += Frames[i].GetDuration;
            }
        }

        public NewAnimation(bool wateraffected, Texture2D spritesheet, params AnimFrame[] frames) : this(spritesheet, frames)
        {
            WaterAffected = wateraffected;
        }

        public NewAnimation(int maxloops, Texture2D spritesheet, params AnimFrame[] frames) : this(spritesheet, frames)
        {
            MaxLoops = maxloops;
        }

        public NewAnimation(bool wateraffected, int maxloops, Texture2D spritesheet, params AnimFrame[] frames) : this(spritesheet, frames)
        {
            WaterAffected = wateraffected;
            MaxLoops = maxloops;
        }

        public NewAnimation(bool wateraffected, bool shouldreverse, int maxloops, Texture2D spritesheet, params AnimFrame[] frames) : this(wateraffected, maxloops, spritesheet, frames)
        {
            ShouldReverse = shouldreverse;
        }

        //Copy constructor; "speedmodifier" tells how much to speed up or slow down this animation in relation to the old one's frames
        public NewAnimation(NewAnimation oldanim, float speedmodifier = 1f) : this()
        {
            this.SpriteSheet = oldanim.SpriteSheet;

            for (int i = 0; i < oldanim.Frames.Count; i++)
            {
                this.Frames.Add(new AnimFrame(oldanim.Frames[i], speedmodifier));
                this.FullAnimDuration += this.Frames[i].GetDuration;
                this.Frames[i].SetAnimation(this);
            }
            
            this.WaterAffected = oldanim.WaterAffected;
            this.MaxLoops = oldanim.MaxLoops;

            //this.FullAnimDuration = oldanim.FullAnimDuration;
            this.ShouldReverse = oldanim.ShouldReverse;
        }

        //Constructs an animation like normal except reverses all the frames
        public static NewAnimation ReversedAnimation(bool wateraffected, bool shouldreverse, int maxloops, Texture2D spritesheet, params AnimFrame[] frames)
        {
            NewAnimation newanim = new NewAnimation(wateraffected, shouldreverse, maxloops, spritesheet, frames);
            newanim.Frames.Clear();
            for (int i = frames.Length - 1; i >= 0; i--)
                newanim.Frames.Add(frames[i]);

            return newanim;
        }

        //The default origin for drawing a sprite, given a Vector2 value
        public static Vector2 DefaultOrigin(Vector2 TextureDimensions, bool facingright)
        {
            //For X dimensions that don't divide by 2 evenly, we need to get the ceiling when facingright is false and use the floor when facingright is true
            float X = TextureDimensions.X / 2f;
            if (X != (int)X)
            {
                if (facingright == false) X = (int)Math.Ceiling(X);
                else X = (int)Math.Floor(X);
            }

            return (new Vector2(X, (int)TextureDimensions.Y));
        }

        //Indexer for accessing frames
        public AnimFrame this[int frame]
        {
            get
            {
                if (frame >= 0 && frame <= MaxFrame) return Frames[frame];
                else return null;
            }
        }

        public Texture2D GetSpriteSheet
        {
            get { return SpriteSheet; }
        }

        public int CurrentFrame
        {
            get { return CurFrame; }
        }

        public int MaxFrame
        {
            get { return (Frames.Count - 1); }
        }

        public AnimFrame CurrentAnimFrame
        {
            get { return Frames[CurFrame]; }
        }

        //Tells if the animation is active (Only applies if the animation is a one-time animation that is reset before being played, like an attack animation)
        public bool IsActive
        {
            get { return (AnimationEnd == false); }
        }

        //Tells if the animation is currently being played in reverse
        public bool IsReversing()
        {
            return CurReverse;
        }

        //Tells if the animation is affected by water
        public bool AffectedByWater
        {
            get { return WaterAffected; }
        }

        //Gets the size of a frame in an animation
        public Vector2 FrameSize(int frame)
        {
            if (frame >= 0 && frame <= MaxFrame)
                return Frames[CurFrame].FrameSize;
            else return Vector2.Zero;
        }

        //Move onto the next frame
        private void NextFrame()
        {
            CurFrame++;
            if (CurFrame > MaxFrame)
            {
                if (ShouldReverse == false)
                {
                    CurFrame = 0;
                    CurLoop++;
                    if (CurLoop >= MaxLoops) AnimationEnd = true;
                }
                else
                {
                    CurFrame = MaxFrame - 1;
                    if (CurFrame < 0) CurFrame = 0;
                    CurReverse = true;
                }
            }

            CurrentAnimFrame.ResetFrame();
        }

        //Move onto the next frame while the animation is currently in reverse
        private void NextFrameReverse()
        {
            CurFrame--;
            if (CurFrame < 0)
            {
                //We shouldn't reverse, meaning this animation was reset starting in reverse and thus should play only once
                if (ShouldReverse == false)
                {
                    CurFrame = 0;
                    AnimationEnd = true;
                    CurReverse = false;
                }
                //Otherwise, reverse it so it looks like a continuous loop, playing each frame only once (Ex. since it just played the first frame, move to the second)
                else
                {
                    CurFrame = 1;
                    if (CurFrame > MaxFrame) CurFrame = MaxFrame;
                    CurLoop++;
                    if (CurLoop >= MaxLoops) AnimationEnd = true;
                    CurReverse = false;
                }
            }

            CurrentAnimFrame.ResetFrame();
        }

        //Gets the length of the current frame in an animation, not taking hit delay or water into account
        public float GetCurFrameLength()
        {
            return (CurrentAnimFrame.GetDuration);
        }

        //Gets the length of a frame in an animation, not taking hit delay or water into account
        public float GetFrameLength(int frame)
        {
            if (frame >= 0 && frame <= MaxFrame)
                return Frames[frame].GetDuration;
            else return 0f;
        }

        //Gets the combined length of several frames in an animation, not taking hit delay or water into account
        //Essentially the opposite of FullDurationExcluding() for convenience purposes
        public float GetFrameLengths(params int[] frames)
        {
            //Start with no duration
            float duration = 0f;

            //Make sure the frames passed in exist
            if (frames != null)
            {
                //Add up the length of each specified frame
                for (int i = 0; i < frames.Length; i++)
                    duration += GetFrameLength(frames[i]);
            }

            return duration;
        }

        //Gets the combined length of the frames including and inbetween startframe and endframe, not taking hit delay or water into account
        public float GetFrameLengthsBetween(int startframe, int endframe)
        {
            //Start with no duration
            float duration = 0f;

            //Ensure we have proper input
            if (startframe >= 0 && endframe <= MaxFrame)
            {
                for (int i = startframe; i <= endframe; i++)
                    duration += Frames[i].GetDuration;
            }

            return duration;
        }

        //Gets the length of the entire animation, not taking hit delay or water into account
        public float FullDuration()
        {
            return FullAnimDuration;
        }

        //Gets the length of the entire animation, not taking hit delay or water into account, excluding the duration of specific frames
        //We don't check for duplicate frames, but there is no reason to pass in more than one of a particular frame number to begin with
        public float FullDurationExcluding(params int[] frames)
        {
            //Start with the full duration
            float duration = FullAnimDuration;

            //Make sure the frames passed in exist
            if (frames != null)
            {
                //Go through each frame
                for (int i = 0; i < frames.Length; i++)
                    duration -= GetFrameLength(frames[i]);
            }

            return duration;
        }

        //Skips to a certain frame of the animation
        public void SkipToFrame(int frame, bool resetframe)
        {
            if (frame >= 0 && frame <= MaxFrame)
            {
                CurFrame = frame;
                if (resetframe == true) CurrentAnimFrame.ResetFrame();
            }
        }

        //Checks if the animation is done
        public bool IsAnimationEnd()
        {
            return AnimationEnd;
        }

        //Resets animation
        public void Reset()
        {
            CurFrame = 0;
            CurLoop = 0;
            CurrentAnimFrame.ResetFrame();
            AnimationEnd = false;
        }

        //Resets the animation, causing it to start playing in reverse next time it plays
        public void ResetReverse()
        {
            CurFrame = MaxFrame;
            CurLoop = 0;
            CurReverse = true;
            CurrentAnimFrame.ResetFrame();
            AnimationEnd = false;
        }

        //Delays the current animation frame for hitlag
        public void DelayAnim(float amount)
        {
            CurrentAnimFrame.DelayFrame(amount);
        }

        public void Update(float activeTime, float slowrate = TileEngine.Gravity)
        {
            if (CurrentAnimFrame.FrameComplete(slowrate) == true)
            {
                if (CurReverse == false) NextFrame();
                else NextFrameReverse();
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            if (SpriteSheet != null)
                CurrentAnimFrame.Draw(spriteBatch, Position, facingright, statuscolor, rotation, Layer);
        }

        //Overload with no spritesheet defined in the animation
        public void Draw(SpriteBatch spriteBatch, Texture2D spritesheet, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            if (spritesheet != null)
                CurrentAnimFrame.Draw(spriteBatch, spritesheet, Position, facingright, statuscolor, rotation, Layer);
        }
    }

    //A frame of an animation
    public sealed class AnimFrame
    {
        //The animation this AnimFrame belongs to
        private NewAnimation Anim;

        //The part of the spritesheet to draw
        private Rectangle DrawSection;

        //The amount to offset the default drawing origin
        private Vector2 OffsetOrigin;

        //How long this frame lasts
        private float FrameDuration;
        private float PrevDuration;

        //The amount of time the duration of this animation frame is extended by due to hitlag
        private float HitDelay;

        public AnimFrame()
        {
            PrevDuration = 0f;
            HitDelay = 0f;

            DrawSection = Rectangle.Empty;
            OffsetOrigin = Vector2.Zero;
        }

        public AnimFrame(Rectangle framebox, float frameduration) : this()
        {
            DrawSection = framebox;

            FrameDuration = frameduration;
        }

        public AnimFrame(Rectangle framebox, float frameduration, Vector2 offsetorigin) : this(framebox, frameduration)
        {
            //Negate the offset origin so we can pass in positive values and still have it draw correctly
            OffsetOrigin = -offsetorigin;
        }

        //Copy constructor; "speedmodifier" tells how much to speed up or slow down this frame in relation to the old frame
        public AnimFrame(AnimFrame oldframe, float speedmodifier = 1f) : this()
        {
            this.Anim = oldframe.Anim;
            this.DrawSection = oldframe.DrawSection;
            this.OffsetOrigin = oldframe.OffsetOrigin;
            this.FrameDuration = (int)(oldframe.FrameDuration * speedmodifier);
        }

        public float GetDuration
        {
            get { return FrameDuration; }
        }

        private Texture2D GetSpriteSheet
        {
            get { return Anim.GetSpriteSheet; }
        }

        //The size of the animation frame
        public Vector2 FrameSize
        {
            get { return new Vector2(DrawSection.Width, DrawSection.Height); }
        }

        //Gets whether the frame should be flipped or not based on the direction it's facing
        private SpriteEffects GetFlip(bool facingright)
        {
            return facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
        }

        //Gets the location to draw the frame, with all other factors taken into account
        private Vector2 TrueDrawPos(Vector2 Position, bool facingright, float rotation)
        {
            Vector2 trueoffset = OffsetOrigin;
            if (facingright == false) trueoffset.X = -trueoffset.X;

            return (Position - DrawYRotation(rotation)) + trueoffset;
        }

        //The duration of the animation frame after taking hit delay and water into account
        public float TrueFrameDuration(float slowrate)
        {
            float rate = 1f;
            if (Anim.AffectedByWater == true && slowrate < TileEngine.Gravity) rate = NewAnimation.WaterSlow;
            //Speed up the animation since the object has StunDown; we know this because this is the only case where a higher value is set
            else if (slowrate > TileEngine.Gravity) rate = NewAnimation.StunDownSpeed;

            return (int)((FrameDuration * rate) + HitDelay);
        }

        //Sets this frame's animation (ONLY to be called in the animation's constructor)
        public void SetAnimation(NewAnimation anim)
        {
            if (anim != null) Anim = anim;
        }

        public bool FrameComplete(float slowrate)
        {
            return (/*Math.Round(Main.GetActiveTime - PrevDuration, 1)*/(Main.GetActiveTime - PrevDuration) >= TrueFrameDuration(slowrate));
        }

        private Vector2 DrawYRotation(float rotation)
        {
            float defaulty = 0f;

            if (rotation != 0f) defaulty = (float)Math.Ceiling(DrawSection.Height / 2f);

            return new Vector2(0, defaulty);
        }

        private Vector2 DrawOriginRotation(float rotation, bool facingright)
        {
            Vector2 defaultorigin = NewAnimation.DefaultOrigin(new Vector2(DrawSection.Width, DrawSection.Height), facingright);

            if (rotation != 0f) defaultorigin.Y -= (float)Math.Floor(DrawSection.Height / 2f);

            return defaultorigin;
        }

        //Delays the animation frame for hitlag
        public void DelayFrame(float amount)
        {
            PrevDuration += amount;
        }

        //Resets the frame's duration
        public void ResetFrame()
        {
            PrevDuration = Main.GetActiveTime;
            HitDelay = 0f;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            if (statuscolor.A != 0)
                spriteBatch.Draw(GetSpriteSheet, TrueDrawPos(Position, facingright, rotation), DrawSection, statuscolor, rotation, DrawOriginRotation(rotation, facingright), 1f, GetFlip(facingright), Layer);
        }

        //Overload for drawing with a spritesheet if you don't have one defined; this is used if an object needs to change spritesheets on the fly (Ex. Theodore after enraging)
        public void Draw(SpriteBatch spriteBatch, Texture2D SpriteSheet, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            if (statuscolor.A != 0)
                spriteBatch.Draw(SpriteSheet, TrueDrawPos(Position, facingright, rotation), DrawSection, statuscolor, rotation, DrawOriginRotation(rotation, facingright), 1f, GetFlip(facingright), Layer);
        }
    }
}
