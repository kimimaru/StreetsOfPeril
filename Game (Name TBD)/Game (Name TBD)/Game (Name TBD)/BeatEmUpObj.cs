﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //The base Beat 'Em Up Object; all interactable objects found in levels will inherit from this
    public abstract class BeatEmUpObj
    {
        //Enum of object types
        public enum ObjectType
        {
            Default, Player, Enemy, Container, Item, Weapon, Hazard, Projectile
        };

        //A delegate for an object touching the environment and having its movement blocked, whether it's a Tile, Solid, or the Screen Bounds
        private delegate void TouchCollision();

        //The default max fall speed an object can have
        public const float DefaultMaxFall = -30f;

        //The amount being in water affects movement speed
        public const float WaterSpeedSlow = 1f;

        //The maximum speed an object can move
        public const float MaxObjectSpeed = 16f;

        //The minimum speed an object can move normally
        public const float MinObjectSpeed = 1f;

        //The maximum health allowed in a health bar
        public const int MaxHealthInBar = 100;

        //The default rotation value for an object (if Rotation != null)
        public const float DefaultRotation = 0f;

        //The divisor for the layer depth and the minimum layer depth allowed
        protected const float DepthDivisor = 1000f;
        protected const float MinDepth = .0001f;

        //Whether the object is active or not, should update or not, or should draw or not
        protected bool Active;
        protected bool UpdateEnabled;
        protected bool DrawEnabled;

        //SubLevel reference
        protected SubLevel SubLvl;

        //Tells if the object was spawned
        public bool Spawned { get; private set; }

        //Tells if the object was despawned; this is useful when there are references to objects but the object itself was removed from the level (Ex. projectiles)
        public bool Despawned { get; private set; }

        //The icon and spritesheet the object uses
        public Texture2D Icon { get; protected set; }
        public Texture2D SpriteSheet { get; protected set; }

        //The rotation of the object; a value of null indicates that the object is not rotated at all
        //XNA rotates objects counter-clockwise when they're flipped horizontally and clockwise when they're not flipped
        protected float? Rotation;

        //Tells if the object is affected by Status Effects or not; even when this is true, the Status will update and draw normally
        protected bool StatusAffected;

        //Whether the object blocks movement or not
        public bool BlocksMove { get; protected set; }

        //Whether the object is affected by gravity or not
        protected bool UseGravity;

        //Tells if the object is blocked by the camera bounds (screen bounds); so far this is only set to true for Players
        protected bool CameraBlocked;

        //Tells if the object is blocked by tiles
        protected bool TileBlocked;

        //Tells if the object is blocked by Solids; so far, this is only used in the Guess The Barrel Bonus Stage
        protected bool SolidBlocked;

        //Object stats, including health, damage, and defense
        protected int MaxHealthBars;
        public int HealthBars { get; protected set; }
        protected int[] MaxHealth;
        protected int Health;
        public int Damage { get; protected set; }
        public int Defense { get; protected set; }

        //The location, movement speed, air speed, tile of the object, solid the object is on, and prior air state (whether the object just started falling)
        protected Vector3 Location;
        protected Vector2 Velocity;
        protected Vector3 AirVelocity;
        public TileEngine.Tile ObjectTile { get; protected set; }
        protected BeatEmUpObj ObjectOn { get; private set; }
        private bool PriorAirState;

        //The height and length of the object, in pixels; these are used for height collisions and hurtbox lengths, respectively
        public int ObjectHeight { get; protected set; }
        public int ObjectLength { get; protected set; }

        //Whether an object is facing right or not; if an object faces a neutral direction (Ex. Items, Item Containers, etc.) then this defaults to true
        public bool FacingRight { get; protected set; }

        //The type of BeatEmUpObj this object is
        public ObjectType ObjType { get; protected set; }

        //The name of the object
        public String Name { get; protected set; }

        //The Score value the BeatEmUpObj holds; this is used in different ways for each object
        //For Enemies, this is the amount of points given to a Player when killed while for Players this is simply the amount of points they have
        public int Score { get; protected set; }

        //The status of the object and a bool determining if the status lasts forever
        public Status status { get; protected set; }
        public bool StatForever { get; protected set; }

        //The hitboxes an object has
        public List<Hitbox> Hitboxes { get; protected set; }

        //Object hurtbox
        public Hurtbox Hurtbox { get; protected set; }

        //States whether the object needs to be defeated to move the camera
        public bool Designated { get; protected set; }

        //An alternate costume for the object
        public int Alternate { get; protected set; }

        //The current gravity value; it defaults to normal gravity
        protected float GravityValue;
        protected float MaxFallSpeed;

        //Hitlag value; all objects experience the same amount hitlag when damaging other objects
        protected float PrevHitLag;

        //The object's HUD
        public HUD hud { get; protected set; }

        //Default constructor; set up all default values to prevent crashes
        protected BeatEmUpObj()
        {
            //Objects are Active by default
            Active = true;
            UpdateEnabled = true;
            DrawEnabled = true;

            GravityValue = TileEngine.Gravity;

            FacingRight = true;
            MaxFallSpeed = DefaultMaxFall;
            
            BlocksMove = false;
            UseGravity = true;

            StatusAffected = true;

            CameraBlocked = false;
            TileBlocked = true;
            SolidBlocked = true;

            Rotation = null;

            ObjType = ObjectType.Default;
            Name = "BeatEmUpObj";

            SpriteSheet = LoadGraphics.ScalableBox;
            Icon = LoadGraphics.ScalableBox;

            ObjectLength = 0;
            ObjectHeight = 0;

            Alternate = 0;

            ClearHitLag();
            ClearAirVelocity();
            PriorAirState = false;

            Hitboxes = new List<Hitbox>();

            Designated = false;

            StatForever = false;
            status = new Status();
            hud = new HUD(this, Vector2.Zero);

            SetUpHealth(0);
            SetUpHurtbox();
        }

        //Sets up an object's Hurtbox; we don't want the Hurtbox being the exact size of the object, so we reduce its size to be in line with how hitboxes are created
        protected void SetUpHurtbox()
        {
            Hurtbox = new Hurtbox((int)(ObjectLength * Hurtbox.HurtboxXScale), (int)(ObjectHeight * Hurtbox.HurtboxYScale));
        }

        //Gets the location of the object
        public Vector3 GetLocation
        {
            get { return Location; }
        }

        //Gets the Vector2 representation of the location of the object
        public Vector2 GetLocationVec2
        {
            get { return new Vector2(Location.X, Location.Y); }
        }

        //Gets the location of the object with the object's height
        public Vector4 GetLocationHeight
        {
            get { return new Vector4(Location, ObjectHeight); }
        }

        //Gets the object's current health
        public virtual int GetHealth
        {
            get { return Health; }
        }

        //Gets the max health for the current health bar the object is on
        public int CurMaxHealth
        {
            get { return MaxHealth[HealthBars]; }
        }

        //Tells if the object has hitboxes
        public bool HasHitboxes
        {
            get { return (Hitboxes != null && Hitboxes.Count > 0); }
        }

        //Returns a rectangle at the object's feet; this is used for movement collisions (Ex. screen bounds, solid collisions, can move onto a tile, etc.)
        //In other words, it's the environmental collision box
        public virtual Rectangle FeetLoc
        {
            get
            {
                Rectangle feetloc = new Rectangle((int)Location.X - (int)Math.Ceiling(Hurtbox.Width / 2f), (int)Location.Y, Hurtbox.Width, Hurtbox.Height / 8);
                //The height of the feetloc must be at least 1
                if (feetloc.Height == 0) feetloc.Height = 1;
                feetloc.Y -= feetloc.Height;

                return feetloc;
            }
        }

        //The object's collision box, used primarily for hitbox-hurtbox collisions
        public virtual Rectangle CollisionBox
        {
            get { return new Rectangle((int)Location.X - (int)(Math.Ceiling(Hurtbox.Width / 2f)), (int)Location.Y - (int)(Hurtbox.Height * Hurtbox.HurtboxXScale), Hurtbox.Width, Hurtbox.Height); }
        }

        //The location to draw the object
        public Vector2 GetDrawLoc(Vector2 cameraloc)
        {
            return new Vector2(Location.X + cameraloc.X, Location.Y - (int)Location.Z + cameraloc.Y);
        }

        //Gets the depth to draw an object; objects with a higher Y value are drawn on top
        //This static version enables us to standardize the depth calculation for all kinds of objects, including ones that don't derive from BeatEmUpObj
        public static float GetObjectDrawDepth(float objecty, Vector2 cameraloc)
        {
            float depth = ((objecty + cameraloc.Y) / DepthDivisor);
            if (depth <= 0) depth = MinDepth;

            return depth;
        }

        //Gets the layer depth to draw the object; objects with a higher Y value are drawn on top
        public float GetDrawDepth(Vector2 cameraloc)
        {
            return GetObjectDrawDepth(Location.Y, cameraloc);
        }

        //Tells if the object is currently active or not
        public bool IsActive
        {
            get { return Active; }
        }

        //Tells if the object is currently updating or not
        public bool IsUpdating
        {
            get { return UpdateEnabled; }
        }

        //Tells if the object is currently drawing or not
        public bool IsDrawing
        {
            get { return DrawEnabled; }
        }

        //Tells if the object is on another object that blocks movement
        public bool IsOnObject
        {
            get { return (ObjectOn != null); }
        }

        //Tells if the object is in the air or not
        public bool IsInAir
        {
            get 
            {
                //The object is in the air if it's not at the same height as its tile or is at the tile's height and still retains air velocity, and isn't on a solid object
                return ((Location.Z != ObjectTile.Z || (Location.Z == ObjectTile.Z && GetAirVelocity != Vector3.Zero)) && IsOnObject == false);
            }
        }

        //Tells if the object is on the ground or not; convenience method
        public bool IsOnGround
        {
            get { return (IsInAir == false); }
        }

        //Tells if the object just ended being grounded
        protected bool EndedGrounded
        {
            get { return (PriorAirState == false && IsInAir == true); }
        }

        //Tells if the object just started falling
        protected bool StartedFalling
        {
            get { return (EndedGrounded == true && AirVelocity.Z == 0f); }
        }

        //Tells if an object is in hitlag
        public bool IsInHitLag
        {
            get { return (Main.GetActiveTime < PrevHitLag); }
        }

        //Tells if an object is rotating
        public bool IsRotating
        {
            get { return (Rotation != null); }
        }

        //Gets an object's rotation
        public float GetRotation
        {
            get { return (IsRotating == false ? DefaultRotation : (float)Rotation); }
        }

        public Vector3 GetAirVelocity
        {
            get { return AirVelocity; }
        }

        //Gets the Vector2 representation of the air velocity of the object
        public Vector2 GetAirVelocityVec2
        {
            get { return new Vector2(AirVelocity.X, AirVelocity.Y); }
        }

        //Tells if the object is in water or not
        public bool IsInWater
        {
            get { return (GravityValue == TileEngine.WaterGravity); }
        }

        //Tells if the object is dead or not
        public virtual bool IsDead
        {
            get { return (Health <= 0 && HealthBars <= 0); }
        }

        //Current height of the object
        public float CurHeight
        {
            get { return Location.Z; }
        }

        //Max height of the object
        public float MaxHeight
        {
            get { return (CurHeight + ObjectHeight); }
        }

        //Gets how high the object is in the air from the tile it's on
        public float HeightFromTile
        {
            get { return (CurHeight - ObjectTile.Z); }
        }

        //The true speed the object moves, taking water and Status Effects into account
        public Vector2 TrueVelocity
        {
            get { return GetTrueVelocity(Velocity); }
        }

        //The true speed the object moves in the air, taking water and Status Effects into account
        public Vector2 TrueVelocityAir
        {
            get { return GetTrueVelocity(GetAirVelocityVec2); }
        }

        //Tells if the object is immune to damage or not
        protected virtual bool IsInvincible
        {
            get { return (StatusCondition(status == (int)Status.Statuses.Invincible) == true || IsDead == true); }
        }

        //Tells if the object should be removed from the sublevel or not
        //Most objects in the game will be removed when they're dead and their HUD is inactive
        public virtual bool ShouldRemove
        {
            get { return (IsDead == true && hud.IsInactive == true); }
        }

        //If the object has anything that influences its velocity, return that here; by default, return 0
        protected virtual Vector2 ObjVelocityFactor()
        {
            return Vector2.Zero;
        }

        //Ensures that the object's velocity is at least the minimum value when it should (Ex. Player walking shouldn't ever have a velocity of 0)
        //Also make sure the object's velocity isn't greater than the maximum value
        private Vector2 CheckMinMaxObjSpeed(Vector2 truevelocity, Vector2 origvelocity)
        {
            Vector2 newtruevelocity = truevelocity;

            //Check for exceeding the max X speed
            if (truevelocity.X > 0 && truevelocity.X > MaxObjectSpeed) newtruevelocity.X = MaxObjectSpeed;
            else if (truevelocity.X < 0 && truevelocity.X < -MaxObjectSpeed) newtruevelocity.X = -MaxObjectSpeed;

            //If the OrigVelocity is not 0, the TrueVelocity can't go lower than 1 if positive or higher than -1 if negative
            if (origvelocity.X >= MinObjectSpeed && truevelocity.X < MinObjectSpeed) newtruevelocity.X = MinObjectSpeed;
            else if (origvelocity.X <= -MinObjectSpeed && truevelocity.X > -MinObjectSpeed) newtruevelocity.X = -MinObjectSpeed;

            //Check for exceeding the max Y speed
            if (truevelocity.Y > 0 && truevelocity.Y > MaxObjectSpeed) newtruevelocity.Y = MaxObjectSpeed;
            else if (truevelocity.Y < 0 && truevelocity.Y < -MaxObjectSpeed) newtruevelocity.Y = -MaxObjectSpeed;

            if (origvelocity.Y >= MinObjectSpeed && truevelocity.Y < MinObjectSpeed) newtruevelocity.Y = MinObjectSpeed;
            else if (origvelocity.Y <= -MinObjectSpeed && truevelocity.Y > -MinObjectSpeed) newtruevelocity.Y = -MinObjectSpeed;

            return newtruevelocity;
        }

        //Gets the true speed the object moves, taking water and Status Effects into account
        //This method takes in a particular velocity and factors in everything else; this allows us to get the true velocity of air speeds and such
        public Vector2 GetTrueVelocity(Vector2 velocity)
        {
            //Store the original velocity
            Vector2 OrigVelocity = velocity;
            Vector2 TrueVelocity = OrigVelocity;

            //If the object has SpeedBoost or SpeedDown, add or subtract to the object's velocity, respectively
            if (StatusCondition(status.IncreasesSpeed()) == true) TrueVelocity = new Vector2(TrueVelocity.X + Status.SpeedModifier, TrueVelocity.Y + Status.SpeedModifier);
            else if (StatusCondition(status == (int)Status.Statuses.SpeedDown) == true) TrueVelocity = new Vector2(TrueVelocity.X - Status.SpeedModifier, TrueVelocity.Y - Status.SpeedModifier);

            //If the object has any specifics that influence its speed (Ex. Players running), factor them in here
            TrueVelocity += ObjVelocityFactor();

            //Lower movement speed by 1 if the object is in water
            if (IsInWater == true)
            {
                TrueVelocity.X -= WaterSpeedSlow;
                TrueVelocity.Y -= WaterSpeedSlow;
            }

            //Make sure the object moves, even if its X or Y speed is reduced below the minimum speed if positive or raised above the minimum speed if negative
            //This doesn't apply if the object's original speed was below the minimum speed if positive or above the minimum speed if negative
            TrueVelocity = CheckMinMaxObjSpeed(TrueVelocity, OrigVelocity);

            return TrueVelocity;
        }

        //Sets the object's Active state
        public void SetActive(bool active)
        {
            Active = active;
        }

        //Sets the object's Update state
        public void SetUpdate(bool update)
        {
            UpdateEnabled = update;
        }

        //Sets the object's Drawing state
        public void SetDrawing(bool drawing)
        {
            DrawEnabled = drawing;
        }

        //Check if an object touched this one from the X direction
        public virtual bool TouchedX(BeatEmUpObj otherobj, int objvelocityx)
        {
            Rectangle objfeetloc = otherobj.FeetLoc;
            objfeetloc.X += objvelocityx;

            return (BlocksMove == true && IsDead == false && objfeetloc.Intersects(CollisionBox) && (otherobj.CurHeight < MaxHeight && otherobj.MaxHeight > CurHeight));
        }

        //Check if an object touched this one from the Y direction
        public virtual bool TouchedY(BeatEmUpObj otherobj, int objvelocityy)
        {
            Rectangle objfeetloc = otherobj.FeetLoc;
            objfeetloc.Y += objvelocityy;

            return (BlocksMove == true && IsDead == false && objfeetloc.Intersects(CollisionBox) && (otherobj.CurHeight < MaxHeight && otherobj.MaxHeight > CurHeight));
        }

        //Check if an object jumped and touched the underside of this one
        public virtual bool JumpedUnder(BeatEmUpObj otherobj)
        {
            float nextmaxheight = (otherobj.MaxHeight + otherobj.GetAirVelocity.Z);
            
            return (BlocksMove == true && IsDead == false && otherobj.FeetLoc.Intersects(CollisionBox) && nextmaxheight >= CurHeight && otherobj.CurHeight < MaxHeight);
        }

        //Check if another object is on top of this one
        public virtual bool IsOn(BeatEmUpObj otherobj)
        {
            return (BlocksMove == true && IsDead == false && otherobj.FeetLoc.Intersects(CollisionBox) && otherobj.CurHeight >= CurHeight && otherobj.CurHeight <= MaxHeight);
        }

        //Returns the object's location onscreen (has the camera factored in)
        public Vector3 GetLocationCamera(Vector2 cameraloc)
        {
            Vector3 location = Location + new Vector3(cameraloc.X, cameraloc.Y, 0f);

            return location;
        }

        //Returns the object's FeetLoc with the camera location factored in
        protected Rectangle FeetLocCamera(Vector2 cameraloc)
        {
            //Get the object's FeetLoc and add the camera's position to it
            Rectangle feetloc = FeetLoc;
            feetloc.X += (int)cameraloc.X;
            feetloc.Y += (int)cameraloc.Y;

            return feetloc;
        }

        //Returns a value dependent on the direction the object is facing
        public int ForwardVal(int value)
        {
            if (FacingRight == true) return value;
            else return -value;
        }

        public float ForwardVal(float value)
        {
            if (FacingRight == true) return value;
            else return -value;
        }

        public int ForwardVal(int value1, int value2)
        {
            if (FacingRight == true) return value1;
            else return value2;
        }

        public float ForwardVal(float value1, float value2)
        {
            if (FacingRight == true) return value1;
            else return value2;
        }

        //Tells if an object is in the screen bounds
        public bool InScreenBounds(Camera levelcamera)
        {
            return (levelcamera.ObjectInScreenBounds(FeetLoc) == true);
        }

        //Tells if an object will be in the screen bounds in a specified new location
        public bool WillBeInScreenBounds(Camera levelcamera, Vector2 newloc)
        {
            Rectangle feetloc = FeetLoc;

            //Get the difference between the new location and the object's current one, then add the X and Y of the result
            Vector2 locdiff = newloc - GetLocationVec2;
            feetloc.X += (int)locdiff.X;
            feetloc.Y += (int)locdiff.Y;

            return (levelcamera.ObjectInScreenBounds(feetloc) == true);
        }

        //Tells if the object is offscreen
        public bool IsOffScreen(Vector2 cameraloc)
        {
            Rectangle feetloc = FeetLocCamera(cameraloc);

            //Check all four sides of the screen
            return (feetloc.X >= Main.ScreenSize.X || feetloc.Right <= 0 || feetloc.Y >= Main.ScreenSize.Y || feetloc.Bottom <= 0);
        }

        //Tells if the object is offscreen by a certain amount; "offrange" must be greater than or equal to 0 for this to work properly
        public bool IsOffScreenRange(Vector2 cameraloc, int offrangex, int offrangey)
        {
            Rectangle feetloc = FeetLocCamera(cameraloc);

            int rightedge = (int)Main.ScreenSize.X + offrangex;
            int leftedge = 0 - offrangex;
            int bottomedge = (int)Main.ScreenSize.Y + offrangey;
            int topedge = 0 - offrangey;

            //Check all four sides of the screen
            return (feetloc.X >= rightedge || feetloc.Right <= leftedge || feetloc.Y >= bottomedge || feetloc.Bottom <= topedge);
        }

        //Changes the direction the object faces to be opposite the current direction the object is facing
        public void FlipDirection()
        {
            FacingRight = !FacingRight;
        }

        //Changes the direction the object faces
        public void ChangeDirection(bool faceright)
        {
            FacingRight = faceright;
        }

        //The total damage dealt when hitting something
        public virtual int TotalDamageDealt(Hitbox objecthitbox)
        {
            //Add the object's Damage with the move's Strength before everything else; this results in the highest value
            int newdamage = Damage + objecthitbox.Strength;

            //If the object has DamageBoost or DamageDown, double or halve the total damage the object deals, respectively
            if (StatusCondition(status.IncreasesDamage()) == true) newdamage *= 2;
            else if (StatusCondition(status == (int)Status.Statuses.DamageDown) == true) newdamage /= 2;

            //Don't let the total damage dealt get lower than 0
            if (newdamage < 0) newdamage = 0;

            return newdamage;
        }

        //The total damage received when being hit by something
        public virtual int TotalDamageReceived(int damage)
        {
            int newdamage = damage;

            //If the object has DefenseBoost or DefenseDown, halve or double the total damage dealt to the object, respectively
            if (StatusCondition(status.IncreasesDefense() == true) == true) newdamage /= 2;
            else if (StatusCondition(status == (int)Status.Statuses.DefenseDown) == true) newdamage *= 2;

            //Subtract the object's Defense after everything else; this results in the lowest value
            newdamage -= Defense;

            //Don't let the total damage received get lower than 0
            if (newdamage < 0) newdamage = 0;

            return newdamage;
        }

        //Fully heals the object
        public void FullHeal()
        {
            HealthBars = MaxHealthBars;
            Health = MaxHealth[HealthBars];

            //Reset the damage indicator, as healing max health with a damage indicator causes the damage indicator to extend off the health bar
            //hud.ResetDamageIndicator();
        }

        //Heals the object by a certain amount
        public void Heal(int healamount)
        {
            Health += healamount;
            HealthUpdate();

            //Reset the damage indicator, as healing around max health with a damage indicator can cause the damage indicator to extend off the health bar
            hud.ResetDamageIndicator();
        }

        //Makes the object lose health; overrideable for objects like Players who can obtain Temporary Health Bars from the Health Juice
        public virtual void LoseHealth(int finaldamage)
        {
            SetDamageIndicator(finaldamage);

            Health -= finaldamage;
            HealthUpdate();
        }

        //Updates the object's health
        protected virtual void HealthUpdate()
        {
            //If we ran out of health for this health bar...
            if (Health <= 0)
            {
                //If we have more health bars left, move onto the next and carry on any remaining damage to the next health bar
                if (HealthBars > 0)
                {
                    HealthBars--;
                    Health = MaxHealth[HealthBars] + Health;
                }
                //Otherwise we're dead, so keep health at 0
                else Health = 0;
            }
            //If we healed and have more health than the max health for our current health bar...
            else if (Health > MaxHealth[HealthBars])
            {
                //If we are not at the max health bar, move onto the previous health bar and subtract the remaining health
                if (HealthBars < MaxHealthBars)
                {
                    HealthBars++;
                    Health -= MaxHealthInBar;
                }
                //If the number of health bars equals or (somehow) exceeds the max number of health bars and our health is still too high, set the health
                //to the max health for that health bar
                if (HealthBars >= MaxHealthBars && Health > MaxHealth[MaxHealthBars])
                {
                    Health = MaxHealth[MaxHealthBars];
                }
            }
        }

        //Sets up an object's health and health bars to the proper values upon being created
        protected void SetUpHealth(int totalhealth)
        {
            //Set the number of max health bars to the number passed in - 1 (Ex. MaxHealthBars = 1 if healthbars = 2)
            //MaxHealthBars = healthbars - 1;

            //Loop through the total health passed in, adding an additional health bar each time and subtracting from the total health each time the max
            //health allowed in a health bar is surpassed
            //For example, if an object has 2 health bars, the first with the max health and the last with 20 health, pass in (MaxHealthInBar + 20)
            //If you want exactly 4 health bars with max health in them, pass in (MaxHealthInBar * 4)
            for (; totalhealth > MaxHealthInBar; totalhealth -= MaxHealthInBar)
                MaxHealthBars++;

            //Set up the max health array; there is always one max health bar no matter what
            MaxHealth = new int[MaxHealthBars + 1];

            //Loop through the health bars right before the final health bar and fill them with max health
            for (int i = 0; i < MaxHealth.Length - 1; i++) MaxHealth[i] = MaxHealthInBar;

            //The last health bar has any health leftover that was passed in (Ex. (MaxHealthInBar + 20) health fills the last bar with 20 health)
            MaxHealth[MaxHealthBars] = totalhealth;

            //Fully heal the object upon creation
            FullHeal();
        }

        //Gets the total amount of max health the object has
        public int TotalMaxHealth()
        {
            return (MaxHealth.Sum());
        }

        //Gets the total amount of health the object currently has
        public virtual int TotalCurHealth()
        {
            int totalhealth = 0;

            //Find out how much health is in the health bars after the object's current health bar
            for (int i = 0; i < (HealthBars - 1); i++)
                totalhealth += MaxHealth[i];

            //Add the object's current health; we do this because it can be less than the max health for that health bar
            totalhealth += Health;

            return totalhealth;
        }

        //Gets the total amount of health the object has missing
        public int TotalHealthMissing()
        {
            return (TotalMaxHealth() - TotalCurHealth());
        }

        //Occurs when the object takes damage
        public virtual void TakeDamage(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            //Objects lose hitlag when taking damage
            ClearHitLag();

            //Make the object lose health
            LoseHealth(TotalDamageReceived(damage));

            //Play the sound the hitbox makes
            LoadSounds.Play(hitbox.HitSound);

            //If the object is dead, make it perform any actions upon dying
            if (IsDead == true)
                Die();
            //Otherwise perform other actions when not dead
            else
            {
                //If the object that hit this object should inflict a status on this object, inflict it
                if (objecthitby.CanInflictStatus(this) == true && CanBeInflictedStatus(objecthitby) == true)
                    InflictStatus(objecthitby.status);

                //Perform any other necessary actions
                TakeDamageActions(objecthitby, damage, hitbox);
            }

            //Perform any further actions that occur regardless if the object dies or not (Ex. changing Enemy AI state, etc.)
            AltTakeDamageActions(objecthitby, damage, hitbox);
        }

        //Actions that occur when an object takes non-fatal damage
        protected virtual void TakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {

        }

        //Actions that occur when an object takes damage, whether it's fatal or not
        protected virtual void AltTakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {

        }

        //Makes the object enter hitlag
        public void EnterHitLag()
        {
            //Get our previous hitlag
            float previousprevhitlag = PrevHitLag;

            //Update the current hitlag
            SetHitLag();

            //Get the difference between our current hitlag and the previous hitlag
            float diff = PrevHitLag - previousprevhitlag;

            //If this difference is greater than 0...
            if (diff > 0)
            {
                //Find out how much to delay our hitboxes/animations; if the hitlag difference is greater than the hitlag constant, delay by the constant
                float hitlag = diff;
                if (hitlag > NewCollision.HitLag) hitlag = NewCollision.HitLag;

                //Delay the hitboxes by the specified amount
                if (HasHitboxes == true)
                {
                    for (int i = 0; i < Hitboxes.Count; i++)
                        Hitboxes[i].DelayHitbox(IsInWater, hitlag);
                }

                //Allow object-specific actions to occur when entering hitlag (Ex. delaying a Fighter's current animation)
                HitLagActions(hitlag);
            }
        }

        protected virtual void HitLagActions(float diff)
        {

        }

        //Sets an object's hitlag 
        public void SetHitLag()
        {
            PrevHitLag = Main.GetActiveTime + NewCollision.HitLag;
        }

        //Clears an object's hitlag
        public void ClearHitLag()
        {
            PrevHitLag = 0f;
        }

        //Creates a Hitbox from the object's feet extending in front of it or behind it (standard)
        public Hitbox CreateHitboxFeet(bool front, int length, int width, int hitboxstrength, int hitboxheight, float hitboxdelay, float hitboxdur, Hitbox.HitboxTypes type, bool? hitboxdirection, SoundEffect hitsound, bool addhitbox)
        {
            //Start with the collision box since it's around the object
            Rectangle collisionbox = CollisionBox;
            collisionbox.X = collisionbox.Center.X;

            //If the object isn't facing the direction the hitbox will be in, then move the starting location of the hitbox
            if (FacingRight != front) collisionbox.X -= length;

            //Set the Y position to be at the center of the object's FeetLoc, then set the width and height accordingly
            collisionbox.Y = FeetLoc.Center.Y - (width / 2);
            collisionbox.Width = length;
            collisionbox.Height = width;

            Hitbox newhitbox = new Hitbox(collisionbox, hitboxstrength, CurHeight, hitboxheight, hitboxdelay, hitboxdur, type, hitboxdirection, hitsound);

            if (addhitbox == true) AddHitbox(newhitbox);
            return newhitbox;
        }

        //Creates a Hitbox from the object's feet extending in front of it or behind it (standard)
        //This overload takes in an animation and the starting and ending active frames of the hitbox in the animation
        public Hitbox CreateHitboxFeet(bool front, int length, int width, int hitboxstrength, int hitboxheight, NewAnimation anim, int startframe, int endframe, Hitbox.HitboxTypes type, bool? hitboxdirection, SoundEffect hitsound, bool addhitbox)
        {
            return CreateHitboxFeet(front, length, width, hitboxstrength, hitboxheight, anim.GetFrameLengthsBetween(0, startframe - 1), anim.GetFrameLengthsBetween(startframe, endframe), type, hitboxdirection, hitsound, addhitbox);
        }

        //Returns a Hitbox that surrounds the object (used mostly for Projectiles)
        public Hitbox CreateHitboxSurrounding(int hitboxstrength, int hitboxheight, float hitboxdelay, float hitboxdur, Hitbox.HitboxTypes type, bool? hitboxdirection, SoundEffect hitsound, bool addhitbox)
        {
            Hitbox newhitbox = new Hitbox(CollisionBox, hitboxstrength, CurHeight, hitboxheight, hitboxdelay, hitboxdur, type, hitboxdirection, hitsound);

            if (addhitbox == true) AddHitbox(newhitbox);
            return newhitbox;
        }

        //Adds a Hitbox to the object's list of Hitboxes
        public void AddHitbox(Hitbox hitbox)
        {
            if (hitbox != null) Hitboxes.Add(hitbox);
        }

        //Removes all of an object's hitboxes
        public void ClearHitboxes()
        {
            if (HasHitboxes == true) Hitboxes.Clear();
        }

        //Makes the object shoot a projectile
        public void ShootProjectile(Projectile projectile)
        {
            SubLvl.AddObject(projectile, SubLvl.GetHazards);
        }

        protected virtual void SetDamageIndicator(int finaldamage)
        {
            hud.SetDamageIndicator(Main.GetActiveTime, Health, HealthBars, finaldamage);
        }

        //Tells if the object should take poison damage
        protected virtual bool ShouldTakePoisonDamage()
        {
            return (IsDead == false && StatForever == false && StatusCondition(status.PoisonDamageObject()) == true);
        }

        //Makes the object lose health if poisoned
        protected virtual void TakePoisonDamage()
        {
            //Reset the poison timer
            status.ResetPoisonTimer();

            //Make the object lose health
            LoseHealth(1);

            //If the object is dead due to the poison damage, call its Die method
            if (IsDead == true)
                Die();
        }

        //Tells if this object can get hit by another object (the attacker) or not
        public virtual bool CanGetHit(BeatEmUpObj attacker, Hitbox hitbox)
        {
            return (IsInvincible == false && attacker.CanDamageObject(this) == true && hitbox.CanHit(attacker.IsInWater, this) == true);
        }

        //Occurs when the object enters water
        protected virtual void EnterWater()
        {
            GravityValue = TileEngine.WaterGravity;
        }

        //Occurs when the object exits water
        protected virtual void ExitWater()
        {
            GravityValue = TileEngine.Gravity;
        }

        //Checks which tile in the level the object is on
        protected void UpdateTile(TileEngine tileEngine)
        {
            //Set the tile the object is on
            ObjectTile = tileEngine.CurTile(FeetLoc);
        }

        //Sets the location of the object - often used when spawning
        public void SetLocation(Vector3 location)
        {
            Location = location;
        }

        //A more convenient overload for setting individual parts of the object's location
        public void SetLocation(float? x, float? y, float? z)
        {
            if (x != null) Location.X = (float)x;
            if (y != null) Location.Y = (float)y;
            if (z != null) Location.Z = (float)z;
        }

        //Sets the location of the object and updates its tile
        public void SetLocationTile(Vector3 location, TileEngine tileEngine)
        {
            SetLocation(location);

            UpdateTile(tileEngine);
        }

        //Moves the object's current location by a designated amount
        public void MoveLocation(Vector3 location)
        {
            Location += location;
        }

        //Sets the object's Camera designation that determines if it needs to be defeated to progress or not
        public void SetDesignated(bool designated)
        {
            Designated = designated;
        }

        //Gives the object solidity; objects will be blocked by this one when moving
        protected void GainSolidity()
        {
            SubLvl.GetSolids.Add(this);
            BlocksMove = true;
        }

        //Makes the object lose its solidity; other objects will no longer be blocked by this one when moving
        protected void LoseSolidity()
        {
            SubLvl.GetSolids.Remove(this);
            BlocksMove = false;
        }

        //Performs initialization on the object when it spawns
        public virtual void Spawn(SubLevel level)
        {
            //Initialize reference
            SubLvl = level;

            //Check if the object is designated and mention that it must be defeated to move the camera if one is present
            if (Designated == true && SubLvl.HasCamera == true)
                SubLvl.GetCamera.IncrementDesignated();

            //If this object should block movement, give it solidity
            if (BlocksMove == true)
                GainSolidity();

            //Set the tile the object is on
            UpdateTile(SubLvl.GetTileEngine);

            //Check if the object should be underwater or not
            if (IsInWater == false)
            {
                //If we're not in water and now are, enter the water
                if (ObjectTile.TileType == (int)TileEngine.TileTypes.Water) EnterWater();
            }
            else
            {
                //If we were in water and are no longer in it, exit the water
                if (ObjectTile.TileType != (int)TileEngine.TileTypes.Water) ExitWater();
            }

            //If the object spawns in the air, make it start falling
            if (IsInAir == true) BeginFall();

            //State the object as spawned properly
            Spawned = true;
        }

        //Performs actions on the object when it is removed from the level
        //This has a variety of uses, including but not limited to: spawning another object in this one's place after death, giving players points, and more
        public virtual void Despawn()
        {
            //Check if the object is designated and mention that it has been defeated if the camera is present
            if (Designated == true && SubLvl.HasCamera == true)
                SubLvl.GetCamera.DecrementDesignated();

            //If this object currently blocks movement, remove its solidity
            if (BlocksMove == true)
                LoseSolidity();

            //State the object as despawned properly
            Despawned = true;
        }

        //Kills the object
        public virtual void Die()
        {
            //Set current health and health bars to 0
            Health = 0;
            HealthBars = 0;

            //Clear the object's Status upon death
            ResetStatus();

            //Clears the object's hitboxes
            ClearHitboxes();

            //Restart the object's HUD so it can get removed properly, if it depends on the HUD being inactive
            hud.Restart();
        }

        //Makes the object's hitboxes follow where they're going and if they're ascending or descending
        public void HitboxFollow(float x, float y, float airvelocityv)
        {
            if (HasHitboxes == true)
            {
                for (int i = 0; i < Hitboxes.Count; i++)
                    Hitboxes[i].Follow(x, y, airvelocityv);
            }
        }

        //Return the Status or a variation of the Staus that this object has; it will be passed onto an object that should be afflicted
        //This method adds flexibility to passing on Statuses (Ex. Weapon with Poison has a 25% chance to pass on but wants the afflicted object to have a 0% chance of passing it on)
        //Not written yet because we want to maintain consistency with passing on Statuses; if it'll be needed for sure later, we'll write it
        //public Status StatusPassed()

        //Determines if this object can pass its status onto another object (the victim)
        public virtual bool CanInflictStatus(BeatEmUpObj victim)
        {
            return (victim.status == (int)Status.Statuses.None && status.ShouldAfflict() == true);
        }

        //Determines if this object can be inflicted with another object's status
        public virtual bool CanBeInflictedStatus(BeatEmUpObj attacker)
        {
            return true;
        }

        //Inflicts a Status Effect onto this object
        public void InflictStatus(Status newstatus)
        {
            status = new Status(newstatus);

            //We don't want an object staying invincible forever unless we intend it to be (Ex. VersusEnemy)
            if (StatForever == true && status == (int)Status.Statuses.Invincible) StatForever = false;
        }

        //Immediately resets the object's status
        public void ResetStatus()
        {
            status.Reset();
        }

        //A method that eases checking for a condition relating to Statuses; it helps StatusAffected work
        public bool StatusCondition(bool statuscheck)
        {
            return (StatusAffected == true && statuscheck == true);
        }

        //Determines if this object can damage another object (the victim)
        //This is primarily used for special cases, such as Players being able to hurt other Players only if the PlayerDamage option is on
        public virtual bool CanDamageObject(BeatEmUpObj victim)
        {
            return true;
        }

        //Damages the object according to the type of Hitbox hit with it
        //The default and standard behavior is to make the object take damage normally
        public virtual void DamageObject(BeatEmUpObj victim, Hitbox hitbox)
        {
            //If the Hitbox isn't a Throw, make the victim take damage like normal
            if (hitbox.GetHitboxType != Hitbox.HitboxTypes.Throw)
            {
                victim.TakeDamage(this, TotalDamageDealt(hitbox), hitbox);

                //Make this object enter hitlag
                EnterHitLag();
            }
            //Throw moves don't cause hitlag
            else if (hitbox.HasThrowDelegate == true) hitbox.GetThrowDelegate(victim, TotalDamageDealt(hitbox), hitbox);

            //Add the victim's hurtbox to the hitbox's Hurtbox list so it cannot damage the victim again
            hitbox.AddHurtbox(victim.Hurtbox);
        }

        //Checks if the object is on a solid or not
        private void CheckIsOnObject()
        {
            //Check if the object is on a solid; if so, check if it's still on that solid (BIG PERFORMANCE BOOST!)
            if (IsOnObject == true && ObjectOn.IsOn(this) == false)
            {
                ObjectOn = null;

                //If the object has no air velocity, check if it's on another solid now; this allows objects to move across solids seamlessly
                if (AirVelocity.Z == 0f)
                {
                    ObjectOn = SubLvl.IsOnSolid(this);
                }
            }
        }

        protected virtual void MoveX(float velocity)
        {
            Location.X += velocity;
            HitboxFollow(velocity, 0, 0);
        }

        protected virtual void MoveY(float velocity)
        {
            Location.Y += velocity;
            HitboxFollow(0, velocity, 0);
        }

        //For camera bounds
        protected virtual void HitScreenX()
        {

        }

        protected virtual void HitScreenY()
        {

        }

        //For tiles
        protected virtual void HitWallX()
        {
            
        }

        protected virtual void HitWallY()
        {

        }

        //For solids
        protected virtual void HitSolidX()
        {

        }

        protected virtual void HitSolidY()
        {

        }

        //Moves the object in the X direction
        /*Idea for improvement (NOT necessarily going to do this unless it becomes a problem):
         *Have the TouchedX and TouchedY methods check if an object will collide based on the direction it's traveling and its velocity
         *We already do this for IsOn regarding height, and it works perfectly; think about it*/
        private void MoveObjectX(float velocity)
        {
            //Don't move if there is no velocity
            if (velocity != 0f)
            {
                //Check for any collisions in the new location, then compare with our current location to determine how far to move
                int collisionvelocity = (int)velocity;

                //If our velocity isn't an integer value, round up or down depending if it's negative or positive
                if (collisionvelocity != velocity)
                {
                    collisionvelocity = velocity < 0f ? (int)Math.Floor(velocity) : (int)Math.Ceiling(velocity);
                }

                //The amount the object will move and the method called if the object collides with something
                float mindist = velocity;
                TouchCollision blockmethod = null;

                Rectangle feetloc = FeetLoc;
                Rectangle newfeetloc = feetloc;
                newfeetloc.X += collisionvelocity;

                //Check if the object is in screen bounds
                Vector4? screendistance = null;
                if (CameraBlocked == true) screendistance = SubLvl.GetCamera.ObjectDistFromScreen(newfeetloc);

                //Check if the object is blocked by a tile
                TileEngine.Tile? tileblocked = null;
                if (TileBlocked == true) 
                {
                    if (SubLvl.GetTileEngine.TileXExists(feetloc, new Vector2(collisionvelocity, 0)) == true) tileblocked = SubLvl.GetTileEngine.CurTile(newfeetloc);
                    else
                    {
                        //Get the right of the last tile
                        if (velocity > 0)
                        {
                            float value = (SubLvl.GetTileEngine[SubLvl.GetTileEngine.NumTilesX - 1, (feetloc.Y - (int)SubLvl.GetTileEngine.GetTileOffSet().Y) / TileEngine.TileSize].Right - 1) - feetloc.Right;
                            if (value < mindist) mindist = value;
                        }
                        //Get the left of the first tile
                        else
                        {
                            float value = SubLvl.GetTileEngine[0, (feetloc.Y - (int)SubLvl.GetTileEngine.GetTileOffSet().Y) / TileEngine.TileSize].Left - feetloc.Left;
                            if (value > mindist) mindist = value;
                        }

                        blockmethod = HitWallX;
                    }
                }

                //Check if the object is blocked by a solid
                BeatEmUpObj solidblocked = null;
                if (SolidBlocked == true) solidblocked = SubLvl.TouchedSolidX(this, collisionvelocity);

                if (velocity > 0f)
                {
                    //Get distance from solid
                    if (solidblocked != null && (solidblocked.CollisionBox.Left - feetloc.Right) < mindist)
                    {
                        mindist = solidblocked.CollisionBox.Left - feetloc.Right;
                        blockmethod = HitSolidX;
                    }

                    //Get distance from tile
                    if (tileblocked != null && CurHeight < tileblocked.Value.Z && (tileblocked.Value.Left - feetloc.Right) < mindist)
                    {
                        mindist = tileblocked.Value.Left - feetloc.Right;
                        blockmethod = HitWallX;
                    }

                    //Get distance from screen
                    if (screendistance != null && (screendistance.Value.Z + collisionvelocity) < mindist)
                    {
                        mindist = screendistance.Value.Z + collisionvelocity;
                        blockmethod = HitScreenX;
                    }
                }
                else
                {
                    //Get distance from solid
                    if (solidblocked != null && -(feetloc.Left - solidblocked.CollisionBox.Right) > mindist)
                    {
                        mindist = -(feetloc.Left - solidblocked.CollisionBox.Right);
                        blockmethod = HitSolidX;
                    }

                    //Get distance from tile
                    if (tileblocked != null && CurHeight < tileblocked.Value.Z && -(feetloc.Left - tileblocked.Value.Right) > mindist)
                    {
                        mindist = -(feetloc.Left - tileblocked.Value.Right);
                        blockmethod = HitWallX;
                    }

                    //Get distance from screen
                    if (screendistance != null && (screendistance.Value.X + collisionvelocity) > mindist)
                    {
                        mindist = (screendistance.Value.X + collisionvelocity);
                        blockmethod = HitScreenX;
                    }
                }

                //Move the object by the specified amount, then call the collision method if the object hit anything
                MoveX(mindist);
                if (blockmethod != null) blockmethod();
            }
        }

        //Moves the object in the Y direction
        private void MoveObjectY(float velocity)
        {
            //Don't move if there is no velocity
            if (velocity != 0f)
            {
                //Check for any collisions in the new location, then compare with our current location to determine how far to move
                int collisionvelocity = (int)velocity;

                //If our velocity isn't an integer value, round up or down depending if it's negative or positive
                if (collisionvelocity != velocity)
                {
                    collisionvelocity = velocity < 0f ? (int)Math.Floor(velocity) : (int)Math.Ceiling(velocity);
                }

                //The amount the object will move and the method called if the object collides with something
                float mindist = velocity;
                TouchCollision blockmethod = null;

                Rectangle feetloc = FeetLoc;
                Rectangle newfeetloc = feetloc;
                newfeetloc.Y += collisionvelocity;

                //Check if the object is in screen bounds
                Vector4? screendistance = null;
                if (CameraBlocked == true) screendistance = SubLvl.GetCamera.ObjectDistFromScreen(newfeetloc);

                //Check if the object is blocked by a tile
                TileEngine.Tile? tileblocked = null;
                if (TileBlocked == true)
                {
                    if (SubLvl.GetTileEngine.TileYExists(feetloc, new Vector2(0, collisionvelocity)) == true) tileblocked = SubLvl.GetTileEngine.CurTile(newfeetloc);
                    else
                    {
                        //Get the bottom of the last tile
                        if (velocity > 0)
                        {
                            float value = (SubLvl.GetTileEngine[(feetloc.X - (int)SubLvl.GetTileEngine.GetTileOffSet().X) / TileEngine.TileSize, SubLvl.GetTileEngine.NumTilesY - 1].Bottom - 1) - feetloc.Bottom;
                            if (value < mindist) mindist = value;
                        }
                        //Get the top of the first tile
                        else
                        {
                            float value = SubLvl.GetTileEngine[(feetloc.X - (int)SubLvl.GetTileEngine.GetTileOffSet().X) / TileEngine.TileSize, 0].Top - feetloc.Top;
                            if (value > mindist) mindist = value;
                        }

                        blockmethod = HitWallY;
                    }
                }

                //Check if the object is blocked by a solid
                BeatEmUpObj solidblocked = null;
                if (SolidBlocked == true) solidblocked = SubLvl.TouchedSolidY(this, collisionvelocity);

                if (velocity > 0f)
                {
                    //Get distance from solid
                    if (solidblocked != null && (solidblocked.CollisionBox.Top - feetloc.Bottom) < mindist)
                    {
                        mindist = solidblocked.CollisionBox.Top - feetloc.Bottom;
                        blockmethod = HitSolidY;
                    }

                    //Get distance from tile
                    if (tileblocked != null && CurHeight < tileblocked.Value.Z && (tileblocked.Value.Top - feetloc.Bottom) < mindist)
                    {
                        mindist = tileblocked.Value.Top - feetloc.Bottom;
                        blockmethod = HitWallY;
                    }

                    //Get distance from screen
                    if (screendistance != null && (screendistance.Value.W + collisionvelocity) < mindist)
                    {
                        mindist = screendistance.Value.W + collisionvelocity;
                        blockmethod = HitScreenY;
                    }
                }
                else
                {
                    //Get distance from solid
                    if (solidblocked != null && -(feetloc.Top - solidblocked.CollisionBox.Bottom) > mindist)
                    {
                        mindist = -(feetloc.Top - solidblocked.CollisionBox.Bottom);
                        blockmethod = HitSolidY;
                    }

                    //Get distance from tile
                    if (tileblocked != null && CurHeight < tileblocked.Value.Z && -(feetloc.Top - tileblocked.Value.Bottom) > mindist)
                    {
                        mindist = -(feetloc.Top - tileblocked.Value.Bottom);
                        blockmethod = HitWallY;
                    }

                    //Get distance from screen
                    if (screendistance != null && (screendistance.Value.Y + collisionvelocity) > mindist)
                    {
                        mindist = (screendistance.Value.Y + collisionvelocity);
                        blockmethod = HitScreenY;
                    }
                }

                //Move the object by the specified amount, then call the collision method if the object hit anything
                MoveY(mindist);
                if (blockmethod != null) blockmethod();
            }
        }

        //Makes the object move vertically or horizontally, updating its current tile
        public void ObjectMove(Vector2 velocity)
        {
            //Walk in the X direction
            MoveObjectX(velocity.X);

            //Walk in the Y direction
            MoveObjectY(velocity.Y);

            //Get the tile the object is on - if it's not moving in the X or Y direction, there's no reason it'd be on a different tile
            if (velocity.X != 0f || velocity.Y != 0f) UpdateTile(SubLvl.GetTileEngine);

            //Do anything else the object wants to do while it's moving
            MovingActions();
        }

        //Do something while the object is moving
        protected virtual void MovingActions()
        {

        }

        //Called when the object falls into a pit
        protected virtual void FallIntoPit()
        {
            //Kill the object
            Die();
        }

        //Update the object's height and air velocity while it's falling
        private void UpdateFall()
        {
            Location.X = (int)Location.X;
            Location.Z += AirVelocity.Z;
            Location.Z = (float)Math.Round(Location.Z, 2);
            HitboxFollow(0, 0, AirVelocity.Z);

            //Ensure the object doesn't fall faster than its designated max fall speed (this will be useful for underwater levels)
            if (AirVelocity.Z != MaxFallSpeed)
            {
                AirVelocity.Z -= GravityValue;
                AirVelocity.Z = (float)Math.Round(AirVelocity.Z, 2);
                if (AirVelocity.Z < MaxFallSpeed) AirVelocity.Z = MaxFallSpeed;
            }

            //Perform whatever else the object may need to do while falling
            FallingActions();
        }

        //Do something while the object is falling
        protected virtual void FallingActions()
        {

        }

        //Affect the object by gravity and detect when it lands on the ground
        protected void ObjectFall()
        {
            //If the object hit its head against the bottom of a solid object, make the object start falling instantly
            if (AirVelocity.Z > 0f && SubLvl.JumpedUnderSolid(this) != null)
                AirVelocity.Z = 0f;

            //Update the object's height and air velocity
            UpdateFall();

            BeatEmUpObj objectlanded = null;
            if (AirVelocity.Z <= 0f)
            {
                objectlanded = SubLvl.IsOnSolid(this);
            }

            //If the object should land on a solid object and the object is above the tile, make the object land on it
            if (objectlanded != null && objectlanded.MaxHeight >= ObjectTile.Z)
            {
                ObjectLand(objectlanded);
            }
            //Check if the object landed on the ground by comparing the object's height with the tile's
            else if (Location.Z <= ObjectTile.Z)
            {
                ObjectLand(null);

                //If the object falls into a pit (a tile with a height less than 0) and isn't dead, kill it
                if (Location.Z < 0f && IsDead == false)
                {
                    FallIntoPit();
                }
            }
        }

        //Does something when the object lands
        protected virtual void LandActions()
        {
            
        }

        //Called when an object begins a fall
        protected virtual void BeginFall()
        {
            Location.X = (int)Location.X;
        }

        //Set the object's Z value to the tile it's in and set the solid it's on if it landed on a solid
        protected void ObjectLand(BeatEmUpObj objecton)
        {
            //If the object landed on a solid object, set the object's height to the solid's height and set the solid the object is on to the solid
            if (objecton != null)
            {
                ObjectOn = objecton;
                Location.Z = ObjectOn.MaxHeight;
            }
            //Otherwise set the object's height to the tile height it landed on
            else Location.Z = ObjectTile.Z;

            //Reset air velocity
            ClearAirVelocity();

            //Do whatever the object needs to do when it lands
            LandActions();
        }

        //Set our rotation to the default value of 0 if we're not rotating
        private void StartRotation()
        {
            if (IsRotating == false) Rotation = 0f;
        }

        //Clears the rotation of the object
        public void ClearRotation()
        {
            Rotation = null;
        }

        //Rotates an object a certain amount, in degrees
        public void Rotate(float degrees)
        {
            //If we aren't currently rotating, start
            StartRotation();

            //Convert degrees to radians
            float radians = MathHelper.ToRadians(degrees);

            //Add the rotation amount
            Rotation += radians;

            //Ensure we wrap back to 0 degrees if we go over 360 degrees (2π) or under -360 degrees (-2π)
            Rotation %= MathHelper.TwoPi;
        }

        //Rotates an object a certain amount, in degrees
        //If clockwise is true, rotate clockwise; otherwise rotate counterclockwise
        public void Rotate(bool clockwise, float degrees)
        {
            //If we aren't currently rotating, start
            StartRotation();

            //Convert degrees to radians
            float radians = MathHelper.ToRadians(degrees);

            //Perform our calculations, as rotating in different directions yields a different amount
            float rotationadd = (float)Rotation + radians;
            float rotationsub = (float)Rotation - radians;

            //The way we rotate the object is dependent on the direction we want to rotate and the direction it's facing
            if (clockwise == true)
                Rotation = FacingRight == true ? rotationsub : rotationadd;
            else
                Rotation = FacingRight == true ? rotationadd : rotationsub;

            //Ensure we wrap back to 0 degrees if we go over 360 degrees (2π) or under -360 degrees (-2π)
            Rotation %= MathHelper.TwoPi;
        }
        
        //Updates the object's hitboxes
        protected void UpdateHitboxes()
        {
            //Update hitboxes
            if (HasHitboxes == true)
            {
                for (int i = 0; i < Hitboxes.Count; i++)
                {
                    //Hitboxes[i].Update(Main.GetActiveTime, GravityValue);
                    if (Hitboxes[i].IsFinished(IsInWater) == true)
                    {
                        Hitboxes.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        //Update method: since all of these objects are found in levels (save the Level Editor), we'll pass the level in
        public void Update()
        {
            //Check if the object can update
            if (CanUpdate() == true)
            {
                //Update all hitboxes
                UpdateHitboxes();

                //Update status
                status.Update(StatForever);

                //Make an object take damage from poison if it should
                if (ShouldTakePoisonDamage() == true)
                    TakePoisonDamage();

                //Update HUD
                hud.Update();

                //Check if the object was in the air before updating
                PriorAirState = IsInAir;

                //Make the object perform any necessary actions for updating itself
                ObjectUpdate();

                //Check if the object is on a solid or not
                CheckIsOnObject();

                //Make the object fall if it's in the air or has some air velocity
                if (ShouldFall() == true)
                {
                    //If the object wasn't in the air before, make it start falling
                    //We don't use StartedFalling here because we just checked if the object was in the air; doing so helps keep BeatEmUpObj optimized
                    if (PriorAirState == false && AirVelocity.Z == 0f) BeginFall();

                    ObjectFall();
                }
            }
        }

        //Object-specific update method that can be overridden
        protected virtual void ObjectUpdate()
        {

        }

        //Tells if the object can update at all
        private bool CanUpdate()
        {
            return (Active == true && UpdateEnabled == true && IsInHitLag == false);
        }

        //Tells if the object can draw at all
        private bool CanDraw()
        {
            return (Active == true && DrawEnabled == true);
        }

        //Tells if the object should fall
        private bool ShouldFall()
        {
            return (UseGravity == true && IsInAir == true && ObjectShouldFall() == true);
        }

        //Object-specific implementations for determining if they should fall (Ex. aerial grabs)
        protected virtual bool ObjectShouldFall()
        {
            return true;
        }

        //Clears the object's air velocity by setting all components to 0
        protected void ClearAirVelocity()
        {
            AirVelocity = Vector3.Zero;
        }

        //Sets the object's air velocity
        protected void SetAirVelocity(Vector3 airvelocity)
        {
            AirVelocity = airvelocity;
        }

        //Makes the object jump
        protected virtual void Jump(Vector3 airvelocity)
        {
            SetAirVelocity(airvelocity);

            //Launch the object into the air
            ObjectFall();
        }

        //Update method in the Level Editor
        public virtual void LevelEditorUpdate(SublevelProp levelprop)
        {
            status.Update(true);
        }

        //Draw a shadow underneath a falling object
        protected void DrawShadow(SpriteBatch spriteBatch, Vector2 cameraloc)
        {
            //Draw the object's shadow if it can
            if (ShouldDrawShadow() == true)
            {
                //Reduce the size of the shadow the higher the object is in the air
                float ShadowScale = Helper.Clamp((float)(1f - (HeightFromTile / 100f)), .4f, 1f);

                //Center the shadow underneath the object
                Vector2 shadoworigin = new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2);
                Vector2 shadowloc = new Vector2(CollisionBox.Center.X + cameraloc.X, Location.Y + cameraloc.Y - ObjectTile.Z);
                SpriteEffects flip = SpriteEffects.FlipHorizontally;
                if (FacingRight == false) flip = SpriteEffects.None;
                
                spriteBatch.Draw(LoadGraphics.PlayerShadow, shadowloc, null, Color.White, 0f, shadoworigin, ShadowScale, flip, 0.0019f);
            }
        }

        //Draw method: for drawing in-game
        public void Draw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            //Don't draw the object if it's not active
            if (CanDraw() == true)
            {
                //If the object is in the air and should draw a shadow underneath it, do so
                DrawShadow(spriteBatch, cameraloc);

                //Make the object draw anything it needs to
                ObjectDraw(spriteBatch, cameraloc, TileEngine);

                //Draw NoStatus 
                if (CanDrawNoStatus() == true)
                    status.DrawNoStatusSymbol(spriteBatch, GetDrawLoc(cameraloc) - new Vector2(0, ObjectHeight), GetDrawDepth(cameraloc));

                //Draw HUD
                //Players will be drawing all HUDs, so this doesn't need to be here; keep it for now
                //if (hud.IsInactive == false)
                //    hud.Draw(spriteBatch);

                #if DEBUG
                    DebugDraw(spriteBatch, cameraloc);
                #endif
            }
        }

        //Object-specific draw method that can be overridden
        protected virtual void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {

        }

        //For drawing Level Editor-specific information
        public virtual void LevelEditorDraw(SpriteBatch spriteBatch, SublevelProp levelprop)
        {
            
        }

        //Tells if the object should draw a shadow underneath it
        protected virtual bool ShouldDrawShadow()
        {
            return (Location.Z > ObjectTile.Z && IsOnObject == false);
        }

        //Tells if the object can draw the NoStatus symbol above it; this would be overridden for Weapons, which do not draw it when they're picked up
        protected virtual bool CanDrawNoStatus()
        {
            return (status.IsNoStatus() == true);
        }

        //Tells if the object's HUD can be set to a Player's InteractionHUD when interacting with it (hurting the player, getting hurt, etc.)
        //By default, this always returns true
        public virtual bool CanSetHUD()
        {
            return true;
        }

        //Determines if the object should draw the Dead icon (flashing red X) on the HUD; by default it returns if the object is dead or not
        public virtual bool ShouldDrawDeadIcon()
        {
            return (IsDead == true);
        }

        //Tells if the object can draw the HUD; for Players, this is always true
        //Likely won't be used since Players will handle the drawing of all object HUDs
        //protected virtual bool CanDrawHUD()
        //{
        //    return (hud.IsInActive(Main.GetActiveTime) == false);
        //}

        //For drawing debug information
        public virtual void DebugDraw(SpriteBatch spriteBatch, Vector2 cameraloc)
        {
            //Draw hitboxes
            if (Debug.HitboxDraw == true)
            {
                if (HasHitboxes == true)
                {
                    for (int i = 0; i < Hitboxes.Count; i++)
                        Hitboxes[i].Draw(spriteBatch, IsInWater, cameraloc);
                }
            }

            if (Debug.HurtboxDraw == true)
                Hurtbox.Draw(spriteBatch, new Vector2(CollisionBox.X, CollisionBox.Y), cameraloc);

            //Draw feet location
            if (Debug.FeetLocDraw == true)
                Debug.DrawRectangle(spriteBatch, FeetLoc, cameraloc, Color.White, .9991f);
        }
    }
}
