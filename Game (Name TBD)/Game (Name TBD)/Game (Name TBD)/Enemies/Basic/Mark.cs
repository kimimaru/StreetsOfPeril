﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Mark : Enemy
    {
        private NewAnimation KickAnim;

        //Constructor
        public Mark(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;

            EnemNum = (int)Enemies.Mark;
            Alternate = alternate;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            KickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 400, new Vector2(-3, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 200;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.SpeedDown, 10000f, 40) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        protected override Vector2 ObjVelocityFactor()
        {
            Vector2 distvelocity = base.ObjVelocityFactor();

            //Make the enemy move faster when further from the target and slower when closer to the target
            if (Target != null)
            {
                //Check the distances from the X and Y separately
                if (TargetProximityX() > 150) distvelocity.X += 1f;
                else if (TargetProximityX() < 75) distvelocity.X -= 1f;

                if (TargetProximityY() > 150) distvelocity.Y += 1f;
                else if (TargetProximityY() < 75) distvelocity.Y -= 1f;
            }

            return distvelocity;
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //Check for jumping up a ledge
            //if ((activeTime - (Action.Actions[0][(int)Actions.Jump] as Jump).PrevJump) > 1500f && CheckJumpHigherSurface(activeTime, TileEngine) == true)
            //{
            //    Action.CurAction = (int)Actions.Jump;
            //    return;
            //}
            
            /*NOTE: Mark's AI will match that of Streets Of Rage 2's Galsia enemy for now, then I'll decide what I want to do with him*/
            //Make Mark travel around the target in a circle
            //Later you may want to use the orbit code found online here (http://xboxforums.create.msdn.com/forums/p/112011/670251.aspx#670251):
            /*Vector2 moonPosition = GetPositionOfSatellite(_sunPosition, DistanceBetweenSunAndMoon, _moonDirection); 

              public Vector2 GetPositionOfSatellite(Vector2 center, float distance, float directionInRadians) 
              { 
                  float yDifference = (float)Math.Sin(directionInRadians); 
                  float xDifference = (float)Math.Cos(directionInRadians); 
                  Vector2 direction = new Vector2(xDifference, yDifference); 
                  Vector2 precisePositionOfSatellite = center + direction * distance; 
                  return precisePositionOfSatellite; 
              } 
              
              With _moonDirection being a value increasing in radians, like .001*/

            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Mark moves around the player
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the player if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Mark approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the player
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Mark is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y && TargetProximityX() <= 25f)
                    {
                        //There is a 1/3 chance that Mark will go on the defensive after attacking
                        int randbehavior = (new Random().Next(0, 3) == 0) ? (int)ActionManager.ActionStates.Defense : (int)ActionManager.ActionStates.Movement;

                        //Mark will do a basic attack; there is a 1/4 chance he'll do a kick
                        if (new Random().Next(0, 4) == 0)
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, KickAnim, hitbox, randbehavior, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        else
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, hitbox, randbehavior, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Mark isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(25f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }
    }
}
