﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Paul : Enemy
    {
        //Paul's Double Kick animation
        public NewAnimation DoubleKickAnim;

        public NewAnimation SlideAnim;

        //Constructor
        public Paul(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(2, 2);
            Weight = 130;
            
            //Check if Paul should do his minecart behavior
            if (behavior > 0) ActionManage.CurState = (int)ActionManager.ActionStates.Preset1;

            EnemNum = (int)Enemies.Paul;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));

            DoubleKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 450, new Vector2(-3, 0)));
            SlideAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 1000, new Vector2(-3, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 250;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2); }
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Mark moves around the player
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the player if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Mark approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the player
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Paul is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y && TargetProximityX() <= 25f)
                    {
                        int randattack = new Random().Next(0, 3);

                        //Paul will do a basic attack; there is a 1/3 chance he'll do a slide
                        if (randattack == 0)
                            ActionManage.CurrentAction = new Slide(this, SlideAnim, true, KnockDownVelocity, null, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        //There's a 1/3 chance Paul will do a double kick
                        else if (randattack == 1)
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 30, 20, 0, 20, 0, 200, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, DoubleKickAnim, hitbox, (int)ActionManager.ActionStates.Preset3, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        }
                        else
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, hitbox, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Mark isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(25f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                }
                //Paul's minecart behavior; move towards the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new MoveTo(this, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2);
                }
                //The second part of Paul's minecart behavior; jump onto the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                {
                    ActionManage.CurrentAction = new JumpAttack(this, null, new Vector3(ForwardVal(OrigJumpVelocity.X), 0f, OrigJumpVelocity.Z), null, LoadSounds.JumpKick, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //The second kick for Paul's double kick
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
                {
                    Hitbox hitbox = CreateHitboxFeet(true, 35, 20, 8, 25, 0, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false);
                    ActionManage.CurrentAction = new SimpleAttack(this, DoubleKickAnim, hitbox, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
            }
            else if (!(ActionManage.CurrentAction is Idle))
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        protected override bool CanDoAction()
        {
            return (base.CanDoAction() == true && (ActionManage.CurState != (int)ActionManager.ActionStates.Preset2 || ActionManage.TimeLastActionEnded >= 400f));
        }
    }
}
