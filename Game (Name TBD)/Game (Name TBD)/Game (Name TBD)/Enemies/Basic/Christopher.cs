﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Christopher : Enemy
    {
        private NewAnimation UppercutAnim;

        //Constructor
        public Christopher(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;

            EnemNum = (int)Enemies.Christopher;

            //Animations
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 15, 47, 70), 500, new Vector2(1, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 500, new Vector2(-3, 0)));
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(527, 112, 39, 70), 150, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 123, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(216, 22, 38, 62), 200, new Vector2(1, 0)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(288, 21, 44, 63), 700, new Vector2(7, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(599, 107, 41, 62), 200, new Vector2(3, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(348, 21, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(420, 63, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(516, 53, 71, 31), 150), new AnimFrame(new Rectangle(420, 63, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(516, 53, 71, 31), 150), new AnimFrame(new Rectangle(600, 41, 39, 43), 100, new Vector2(2, 0)));
            UppercutAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(120, 118, 64, 74), 100, new Vector2(-4, 0)), new AnimFrame(new Rectangle(204, 104, 37, 88), 500, new Vector2(6, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(600, 41, 39, 43), 350, new Vector2(2, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 250;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 7000f, 30), new Status((int)Status.Statuses.SpeedDown, 10000f, 35) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Christopher moves around the target
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Christopher approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the target
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Christopher is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y && TargetProximityX() <= 25f)
                    {
                        int action = new Random().Next(0, 2);

                        //Christopher will do a basic attack; there is a 1/2 chance he'll do an uppercut, but if the target is jumping he'll always do it
                        if ((Target.CurHeight > Target.ObjectTile.Z && Target.IsOnObject == false) || action == 1)
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 39, 20, 0, ObjectHeight, 0, UppercutAnim.FullDuration(), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, UppercutAnim, hitbox, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        else
                        {
                            Hitbox hitbox = CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false);
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, hitbox, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Christopher isn't in range to land an attack, match the target's Y position and go up to the target
                    else
                    {
                        ActionManage.CurrentAction = new Approach(this, new Vector2(25f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                    {
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    else ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && ActionManage.CurState == (int)ActionManager.ActionStates.Movement);
        }

        public override bool CheckShouldPickUp(Item item, Weapon Weapon)
        {
            if (item != null)
            {
                //Christopher mainly goes for status items, so make Christopher pick up an item if he doesn't have a status
                return (status == (int)Status.Statuses.None);
            }

            return false;
        }
    }
}
