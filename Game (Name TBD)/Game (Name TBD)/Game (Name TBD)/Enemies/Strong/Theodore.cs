﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //An enemy that becomes enraged and thus stronger when alone
    public class Theodore : Enemy
    {
        private NewAnimation EnrageAnim;
        private NewAnimation DashAnim;
        public NewAnimation DashKickAnim;

        //Enumeration of actions
        private enum Actions
        {
            Idle, Jump, Enrage /*Enraged only actions here*/, LDash, WalkTD, WalkTH, WalkTV, MWalkTH, MWalkTV, MWalkAH, MWalkAV, MatchY, BasicAttack, DPunch, SWalkTH, SWalkTV, SWalkAH, SWalkAV
        };

        //Tells if Theodore is enraged or not
        private bool Enraged;

        public Theodore(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(3f, 3f);
            Weight = 125;

            EnemNum = (int)Enemies.Theodore;

            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            //Animations
            EnrageAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 2000, new Vector2(1, 0)));
            DashAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 300, new Vector2(-3, 0)));
            DashKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 300, new Vector2(-10, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Enraged = false;

            GainKnockInv = true;

            Name = name;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 950;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;

            //Start Theodore out enraged by changing his starting behavior (probably won't use this at all since it goes against his design, but it's here as an option anyway just in case I want to be creative)
            if (behavior > 0) ActionManage.CurState = (int)ActionManager.ActionStates.Preset1;//Enrage(0f);
        }

        protected override void HitWallX()
        {
            //Stop the Dash part of the Lightning Dash if the enemy hits a wall or solid
            if (ActionManage.CurrentAction.Animation == DashAnim)
            {
                //Face the target
                FaceTarget();
                ActionManage.EndAction(Main.GetActiveTime);
            }
        }

        protected override void HitSolidX()
        {
            HitWallX();
        }

        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1); }
        }

        //Enrages Theodore, boosting his Damage and Defense by 1.5x, his speed by 1, and giving him new moves to use as well as reducing the time he can be grabbed and slightly increasing his grab recovery time
        public void Enrage(float activeTime)
        {
            //Boost Theodore's stats by 1.5x, rounding up
            Damage = (int)Math.Ceiling(Damage * 1.5f);
            Defense = (int)Math.Ceiling(Defense * 1.5f);
            Velocity = new Vector2(Velocity.X + 1f, Velocity.Y + 1f);

            //Change Theodore's spritesheet to its enraged counterpart
            ChangeCostume(4);

            Grabbox = new Grabbox(Grabbox.GetLength(ObjectLength), 6, 1700, 1200);
            Enraged = true;
        }

        //Make the enemy act according to the its position relative to the target
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Theodore moves around the target
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Theodore approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the target
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Theodore is really close to the target in the Y direction and if Theodore is close to the target in the X direction and not enraged, do a double punch
                    if (TargetProximityY() <= TrueVelocity.Y && TargetProximityX() <= 25)
                    {
                        //If Theodore is enraged and is facing the target, perform the Lightning Dash, which follows into a kick
                        if (Enraged == true && IsFacingTarget() == true) ActionManage.CurrentAction = new LDash(this, DashAnim, (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        //Theodore will do the first punch of the double punch
                        else ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 35, 20, 4, 16, 0, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                    }
                    //If Theodore isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(25f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                }
                //Make Theodore get enraged since there are no more enemies present (only here as a precaution in case Enrage gets interrupted)
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new Enrage(this, EnrageAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset1);
                }
                //Make Theodore do the second punch in the Double Punch move
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 35, 20, 5, 16, 0, 100, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //Make Theodore do his Lightning Dash move
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
                {
                    ActionManage.CurrentAction = new LDash(this, DashAnim, (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Make Theodore do the Lightning Dash Kick move, which is performed right after the Lightning Dash
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset4)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, DashKickAnim, CreateHitboxFeet(true, 45, 20, 7, 20, 100, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        //Handle switching into Theodore's enraged state here since it should interrupt any action he is currently performing
        protected override void SetDoAction()
        {
            //Check if Theodore is alone; if so, Theodore should enrage and become stronger
            if (Enraged == false && ActionManage.CurState != (int)ActionManager.ActionStates.Preset1 && SubLvl.GetEnemies.Count == 1)
            {
                ActionManage.CurrentAction = new Enrage(this, EnrageAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset1);
                ActionManage.CurState = (int)ActionManager.ActionStates.Preset1;
            }

            base.SetDoAction();
        }
    }
}
