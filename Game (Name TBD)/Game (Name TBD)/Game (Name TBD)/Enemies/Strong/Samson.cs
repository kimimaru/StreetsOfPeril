﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Samson : Enemy
    {
        private NewAnimation SwingAnim;
        private NewAnimation ThrustKickAnim;
        private NewAnimation ThrowAnim;

        //Constructor
        public Samson(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(2, 2);
            Weight = 165;

            if (behavior > 0) ActionManage.CurState = (int)ActionManager.ActionStates.Preset1;

            EnemNum = (int)Enemies.Samson;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            ThrustKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 700, new Vector2(-3, 0)));
            ThrowAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 1000, new Vector2(-3, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 600, new Vector2(-3, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            GainKnockInv = true;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 1000;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 7500f, 75), new Status((int)Status.Statuses.DefenseDown, 7500f, 75), new Status((int)Status.Statuses.SpeedDown, 6000f, 70) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2); }
        }

        //Make the enemy act according to the its position relative to the target
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Samson moves around the target
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Samson approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the target
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Samson is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y)
                    {
                        //If Samson has a weapon that's not wearable, attack with it
                        if (weapon != null && weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                        {
                            ActionManage.CurrentAction = new WeaponAttack(this, SwingAnim, weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, 30, 200, 400, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.SharpHit, false), LoadSounds.SharpSwing, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        }
                        //If Samson is close, perform a close-ranged attacks
                        else if (TargetProximityX() <= 25f)
                        {
                            int randmove = new Random().Next(0, 2);

                            //Samson will do either a 3-hit combo or a grab
                            if (randmove == 0) ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 1, 15, 0, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset3, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                            else ActionManage.CurrentAction = new Grab(this, AttackingAnim, (int)ActionManager.ActionStates.Preset5, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        }
                        //If Samson is mid-distance, perform a Thrust Kick
                        else ActionManage.CurrentAction = new SimpleAttack(this, ThrustKickAnim, CreateHitboxFeet(true, 45, 20, 4, 15, 200, 500, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                    }
                    //If Samson isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(new Random().Next(0, 2) == 0 ? 25f : 40f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                }
                //Samson's minecart behavior; move towards the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new MoveTo(this, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2);
                }
                //The second part of Samson's minecart behavior; jump onto the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                {
                    ActionManage.CurrentAction = new JumpAttack(this, null, new Vector3(ForwardVal(OrigJumpVelocity.X), 0f, OrigJumpVelocity.Z), null, LoadSounds.JumpKick, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //The second (elbow) hit of Samson's 3-hit combo
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 1, 15, 0, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //The third (kick) hit of Samson's 3-hit combo
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset4)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 2, 15, 100, 100, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //Performed after a successful grab; Samson throws the target face down
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset5)
                {
                    ActionManage.CurrentAction = new Throw(this, ThrowAnim, CreateHitboxFeet(true, 20, 20, 3, 30, 300, 1, Hitbox.HitboxTypes.KnockDown, TowardsDir, null, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        protected override bool CanDoAction()
        {
            return (base.CanDoAction() == true && (ActionManage.CurState != (int)ActionManager.ActionStates.Preset2 || ActionManage.TimeLastActionEnded >= 400f));
        }

        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && ActionManage.CurState == (int)ActionManager.ActionStates.Movement);
        }

        public override bool CheckShouldPickUp(Item item, Weapon Weapon)
        {
            //Samson will pick up only swingable or stabbable weapons if he does not already have a weapon
            if (Weapon != null && Weapon.GetWeaponType != (int)Weapon.WeaponTypes.None && weapon == null)
            {
                return true;
            }

            return false;
        }
    }
}
