﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An enemy used in underwater levels that swims across the screen with a harpoon or swims down from above, afterwards swimming towards the target; another action involves throwing underwater bombs as he swims across the screen (like Rolento from FF2)
    //NOTE: I'm strongly considering having Diver be an enemy you may actually have to kill (maybe the boss of Level 7 can be a swimmer who shares many traits with Diver, like "swimming")
    //As a result he'll need to be less vulnerable from the front and the sides; right now his only vulnerable spot is from the back, and he moves too fast to make it matter
    //THEN AGAIN: He does fill the purpose that the Minecart Rider does in that he's just an annoyance that goes away...think about it more before deciding

    //Also have an attack where Diver swims across and throws underwater bombs as he passes (sort of like Rolento in FF2)
    public class Diver : Enemy
    {
        //The constant Z position that the Diver stays at (simulates swimming)
        public float ConstantZ;

        //How long it takes for Diver to fall after matching the target's X and Y
        private float DiveRate;

        //How long Diver must wait before performing an action
        private float NextActionWait;

        //Determines if Diver throws bombs or not
        private bool ThrowBomb;

        //Constructor - the Diver is never on the ground unless dead, so always set the Z location to something greater than the ground he'd be on
        public Diver(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, Texture2D sprite, float diverate, int behavior, int alternate = 0)
        {
            SetLocation(location);
            ConstantZ = Location.Z;
            Velocity = new Vector2(6, 4);

            EnemNum = (int)Enemies.Diver;

            //The standing and walking animations are the same swimming motion; the standing animation just needs to be instantiated so it doesn't crash when he happens to stand
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            //Check if Diver should start out with his Drill behavior or not
            ActionManage = new ActionManager(this, diverate >= 0f ? (int)ActionManager.ActionStates.Preset1 : (int)ActionManager.ActionStates.Preset2);

            CurKnockDownAnim = KnockedDownAnim;

            DiveRate = diverate;
            NextActionWait = 0f;

            Name = name;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 300;
            HitStun = 450;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;

            if (behavior > 0) ThrowBomb = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DefenseDown, 6000f, 100), new Status((int)Status.Statuses.Poison, 4500f, 50) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Gets the Diver's dive rate
        public float GetDiveRate
        {
            get { return DiveRate; }
        }

        //Sets the wait timer after finishing an action
        public void SetActionWaitTimer()
        {
            NextActionWait = Main.GetActiveTime;
        }

        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            /*The Diver follows this sequence: 
            (with drill behavior)
            1. Match the player's X and Y position and swim in from above
            2. Swim towards the player from your X position (left by default) until you hit a side of the screen
            (with sweep behavior)
            1. Match the player's Y position and swim at the player from a side of the screen
            2. Match the player's Y position and swim at the player again, this time from the side of the screen you stopped at last time*/

            //If the Diver has the drill behavior, use that; otherwise, use the sweep behavior - a DiveRate of -1 means that Diver shouldn't perform his drill behavior
            if (DiveRate > 0f && ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
            {
                ActionManage.CurrentAction = new SwimInAbove(this, DiveRate, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2);
            }
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                //Check the direction Diver was moving before being interrupted and pass that in; if Diver was just starting the action, a direction of 0 will be passed in
                float swimdirection = 0f;
                SwimIn swimin = ActionManage.CurrentAction as SwimIn;
                if (swimin != null) swimdirection = swimin.Direction;

                //Make Diver throw an underwater bomb instead of using his harpoon if he has projectiles, which means he's set to throw bombs
                ActionManage.CurrentAction = new SwimIn(this, swimdirection, ThrowBomb, DiveRate > 0f ? (int?)ActionManager.ActionStates.Preset1 : null);
            }
        }

        protected override bool CanPerformAction()
        {
            //Make Diver wait a little after swimming to the other side of the screen
            return (base.CanPerformAction() == true && (Main.GetActiveTime - NextActionWait) >= 300f);
        }

        public override bool CanGetGrabbed()
        {
            return false;
        }

        //Check if Diver should stop being knocked down
        public override void DiverHeightCheck()
        {
            //If Diver is alive, check his height and stop him from being knocked down after reaching his constant height
            if (IsDead == false && Location.Z <= ConstantZ)
            {
                Location.Z = ConstantZ;

                KnockDownRecover();
            }
        }
    }
}
