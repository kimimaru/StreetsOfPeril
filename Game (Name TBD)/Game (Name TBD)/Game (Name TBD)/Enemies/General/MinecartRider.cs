﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An enemy used in Level 6-3 that moves towards the player's minecart from a side of the screen, throws a projectile when in range, and retreats offscreen
    public class MinecartRider : Enemy
    {
        //How high and far the explosive bottle gets thrown
        private Vector2 BottleVelocity;

        //Constructor
        public MinecartRider(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, Texture2D sprite, Vector2 bottlevelocity, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(2, 2);

            //Initialize actions
            ActionManage = new ActionManager(this, new MoveTo(this, (int)ActionManager.ActionStates.Preset2), (int)ActionManager.ActionStates.Preset1);

            EnemNum = (int)Enemies.MinecartRider;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 700, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            BottleVelocity = bottlevelocity;

            Name = name;
            SetUpHealth(health);
            SetUpHurtbox();
            SetUpSprites();

            FacingRight = Location.X > 0;

            Damage = damage;
            Defense = defense;
            Score = 350;
            HitStun = 450;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 8000f, 100), new Status((int)Status.Statuses.DefenseDown, 8000f, 100), new Status((int)Status.Statuses.SpeedDown, 8000f, 100) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //public override bool IsDead()
        //{
        //    if ((Health <= 0 && HealthBars <= 0) || (Location.X < -(TileEngine.TileSize * 3)) || (Location.X > Main.ScreenSize.X + (TileEngine.TileSize * 3))) return true;
        //    return false;
        //}

        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3); }
        }

        public Vector2 GetBottleVelocity
        {
            get { return BottleVelocity; }
        }

        //Minecart Rider cannot be knocked down until he is defeated
        /*public override void GetHurt(float activeTime, bool FacingRight, Player player, Player lastattacked, int damage, Status weaponstat, Hitbox playerbox, TileEngine TileEngine)
        {
            //The original direction MinecartRider was facing; we do not want to change this at all regardless of how he gets hit (can cause him to move through the player minecart
            bool FacingDirection = FacingRight;

            //Original hitbox properties; to save code, we'll make these properties values that would avoid Minecart Rider from getting knocked down and set them back after Minecart Rider takes damage
            bool OrigHitKnockdown = playerbox.KnockDown;
            playerbox.KnockDown = false;

            //Make the Minecart Rider take damage like normal, then 
            base.GetHurt(activeTime, FacingRight, player, lastattacked, damage, weaponstat, playerbox, TileEngine);

            //Set the properties back to their original values
            playerbox.KnockDown = OrigHitKnockdown;
            FacingRight = FacingDirection;
        }*/

        //The Minecart Rider's action sequence
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            /*The Minecart Rider follows this sequence: 
            1. Move towards the player minecart
            2. Throw the projectile
            3. Move away the player minecart, offscreen
            
            If the Minecart Rider is hit, he still continues with his actions; if he dies he flies off his minecart and his minecart retreats*/

            //Make the Minecart Rider move towards the player minecart
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
            {
                ActionManage.CurrentAction = new MoveTo(this, (int)ActionManager.ActionStates.Preset2);
            }
            //Make the Minecart Rider throw his explosive bottle once he's near the player minecart
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                ActionManage.CurrentAction = new ThrowBottle(this, BottleVelocity, (int)ActionManager.ActionStates.Preset3);
            }
            //Make the Minecart Rider retreat from the minecart with invincibility
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
            {
                ActionManage.CurrentAction = new MoveAway(this);
            }
        }

        protected override bool CanPerformAction()
        {
            //Make the Minecart Rider wait a little in between actions
            return (base.CanPerformAction() == true && ActionManage.TimeLastActionEnded >= 300f);
        }

        public override bool CanGetGrabbed()
        {
            return false;
        }
    }
}
