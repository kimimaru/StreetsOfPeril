﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The enemy in the Avoid Enemy versus mode
    //NOTE: Change to hold player references instead of indices
    public class VersusEnemy : Enemy
    {
        //The index of the player who commanded the enemy
        private int CommandedPlayer;

        //The index of the player the enemy is going for
        private int TargetNum;

        //How long the Versus Enemy stays actively going for a target
        private const float ActiveDuration = 4500f;
        private float PrevActive;

        //Make players able to set the enemy's name (and maybe how long it takes before he stops going for someone)
        public VersusEnemy(String name = "Dummy")
        {
            SetLocation(new Vector3(200, 50, 0));
            Velocity = new Vector2(2, 2);

            PrevActive = 0f;

            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));

            CommandedPlayer = 0;
            TargetNum = -1;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(BeatEmUpObj.MaxHealthInBar);
            SetUpSprites();

            Damage = 500;
            Defense = 500;
            Score = 0;
            HitStun = 0f;
            status = new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0);
            StatForever = true;
        }

        //For setting the versusenemy's target
        public void SetTarget(float activeTime, Player target, int commander, int targetnum)
        {
            Target = target;
            CommandedPlayer = commander;
            TargetNum = targetnum;
            PrevActive = activeTime;
        }

        //Gets the index of the player the versusenemy is currently controlled by
        public int GetCommander()
        {
            return CommandedPlayer;
        }

        //Gets the index of the player the versusenemy is currently going for
        public int GetTargetNum()
        {
            return TargetNum;
        }

        //Make the VersusEnemy go for whoever he's told to go for
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //The VersusEnemy approaches whoever he's set to go to
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
            {
                ActionManage.CurrentAction = new Approach(this, new Vector2(20, 0), false, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Once the VersusEnemy reached his target, he'll attack it
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
            {
                ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        protected override bool CanDoAction()
        {
            return (base.CanDoAction() == true && Target != null && TargetNum > -1);
        }

        protected override void ActionFinished(float activeTime)
        {
            //Stop the enemy after a certain amount of time or if the target has been killed
            if ((activeTime - PrevActive) >= ActiveDuration || (ActionManage.ActionDone == true && Target.IsDead == true))
            {
                Stop();
            }
            else base.ActionFinished(activeTime);
        }

        private void Stop()
        {
            Target = null;
            TargetNum = -1;
            ResetActions();
            ActionManage.SwitchBehavior((int)ActionManager.ActionStates.Movement);
            ActionManage.CurrentAction = new Idle(this);
        }
    }
}
