﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The boss of Level 4; Commander represents his name well by commanding enemies
    //The enemies (aka Commander's minions) will have their behaviors switched to a special one (Command) when they're surrounding Commander, in which they move with him
    //When Commander commands his minions to attack, they switch to their Movement behavior and act like normal; this also occurs when Commander gets knocked down or grabbed if they're defending him
    //As long as Commander has minions, he is invincible in the sense that any damage dealt to him won't subtract from his Health, but he can still get hit, grabbed, and knocked down
    //However, Commander can take damage and die from the Poison status if he has been inflicted with it, even if he still has minions
    public sealed class Commander : Enemy
    {
        //The enemies Commander commands; Commander has a max of 5
        private List<Enemy> Minions;

        //The timer before Commander spawns more minions
        private const float MinionSpawnTime = 4000f;//20000f;
        private float PrevMinionSpawn;

        private NewAnimation CommandAnim;
        private NewAnimation UppercutAnim;
        private NewAnimation SlideBackAnim;
        private NewAnimation SDownKickAnim;

        public Commander(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(3, 3);
            Grabbox = new Grabbox(Grabbox.GetLength(ObjectLength), 6, 1500, 1000);

            EnemNum = (int)Enemies.Commander;

            Minions = new List<Enemy>(5);

            PrevMinionSpawn = 0f;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(80, 123, 61, 85), 300, new Vector2(0, 0)));
            WalkingAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 124, 60, 84), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(80, 123, 61, 85), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(152, 122, 61, 86), 200, new Vector2(0, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(224, 121, 55, 87), 50, new Vector2(0, 0)), new AnimFrame(new Rectangle(288, 119, 51, 89), 50, new Vector2(0, 0)), new AnimFrame(new Rectangle(352, 124, 60, 84), 75, new Vector2()), new AnimFrame(new Rectangle(424, 125, 59, 83), 50, new Vector2()), new AnimFrame(new Rectangle(496, 132, 91, 76), 350, new Vector2()));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(352, 352, 53, 80), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(528, 340, 62, 92), 500, new Vector2(0, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(472, 356, 47, 76), 200, new Vector2(0, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 445, 76, 75), 0, new Vector2(0, 0)), new AnimFrame(new Rectangle(96, 492, 106, 28), 400, new Vector2(0, 0)), new AnimFrame(new Rectangle(216, 468, 100, 52), 150, new Vector2(0, 0)), new AnimFrame(new Rectangle(96, 492, 106, 28), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(216, 468, 100, 52), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(328, 454, 70, 66), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(328, 454, 70, 66), 350, new Vector2(0, 0)));

            CommandAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(8, 344, 56, 88), 200), new AnimFrame(new Rectangle(72, 345, 51, 87), 50), new AnimFrame(new Rectangle(136, 344, 55, 88), 50), new AnimFrame(new Rectangle(200, 363, 80, 69), 200));
            SDownKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(120, 756, 47, 92), 50), new AnimFrame(new Rectangle(8, 755, 55, 93), 50), new AnimFrame(new Rectangle(72, 740, 41, 108), 250));
            UppercutAnim = new NewAnimation(SDownKickAnim, 1f);
            SlideBackAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(472, 356, 47, 76), 500, new Vector2()));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;
            ObjectLength = 59;
            ObjectHeight = 86;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 200;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public List<Enemy> GetMinions
        {
            get { return Minions; }
        }

        //The max number of minions Commander has
        public int MaxMinions
        {
            get { return Minions.Capacity; }
        }

        public override bool IsBoss()
        {
            return true;
        }

        //Tells if Commander has minions
        public bool HasMinions()
        {
            return (Minions.Count > 0);
        }

        private bool MinionsDefending()
        {
            return (Minions[0].gsAction.CurState == (int)ActionManager.ActionStates.Command);
        }

        //Checks if Commander should spawn minions; if he no longer has minions and he isn't waiting for the minions to spawn, start the spawn timer
        private bool ShouldSpawnMinions()
        {
            return (HasMinions() == false && PrevMinionSpawn == 0f);
        }

        //Checks if Commander can spawn minions; if the spawn timer started and the spawn time passed, spawn them
        private bool CanSpawnMinions()
        {
            return (PrevMinionSpawn != 0f && (Main.GetActiveTime - PrevMinionSpawn) >= MinionSpawnTime);
        }

        //The formation of the minions around Commander based on the number of minions there are
        private int[] MinionFormation()
        {
            if (Minions.Count == MaxMinions) return new int[] { 0, 1, 2, 3, 4 };
            else if (Minions.Count == 4) return new int[] { 0, 1, 3, 4 };
            else if (Minions.Count == 3) return new int[] { 0, 2, 4 };
            else if (Minions.Count == 2) return new int[] { 0, 4 };
            else return new int[] { 2 };
        }

        protected override void ResetActions()
        {
            base.ResetActions();

            //if (Action.CurState == (int)ActionManager.ActionStates.Offense)
            //{
            //    ThrowMinion throwminion = Action as ThrowMinion;
            //
            //    if (throwminion != null) throwminion.ThrownMinion.Drop();
            //}
        }

        public override int TotalDamageReceived(int damage)
        {
            //Commander doesn't take any damage if he has minions remaining
            if (HasMinions() == true) return 0;
            else return base.TotalDamageReceived(damage);
        }

        protected override void KnockDownActions()
        {
            base.KnockDownActions();

            //If Commander has minions and they're in the Command behavior, switch them to their normal behavior
            if (HasMinions() == true && MinionsDefending() == true)
                SetMinionCommand((int)ActionManager.ActionStates.Movement);
        }

        //If Commander gets grabbed, has minions, and the minions are in the Command behavior, switch them to their normal behavior
        public override void GetGrabbed(BeatEmUpObj opponent)
        {
            base.GetGrabbed(opponent);

            if (HasMinions() == true && MinionsDefending() == true)
            {
                SetMinionCommand((int)ActionManager.ActionStates.Movement);
            }
        }

        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //Check how many minions are remaining; if none are remaining, spawn more if necessary
            if (ShouldSpawnMinions() == true)
            {
                //Reset the spawn timer
                PrevMinionSpawn = Main.GetActiveTime;
            }

            //Spawn more minions
            if (CanSpawnMinions() == true)
            {
                //Spawn minions
                //NOTE: Check if the enemies are dead in the sublevel and if Commanders' minion list still has the dead enemy!
                ActionManage.CurrentAction = new SpawnMinions(this, CommandAnim, OffSet, EnemList, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);

                PrevMinionSpawn = 0f;
            }
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
            {
                //NOTE: The Finished, Interrupt, and Unfinished states are temporary for this right now
                ActionManage.CurrentAction = new Approach(this, new Vector2(120, 0), false, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
            }
            //Tell the minions to attack players
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
            {
                //If Commander has minions and they're defending him, tell them to attack
                if (HasMinions() == true && MinionsDefending() == true)
                {
                    //Tell the minions to attack
                    //Action.CurrentAction = new CommandMinions(this, CommandAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);

                    //Otherwise, throw a minion at the target
                    ActionManage.CurrentAction = new ThrowMinion(this, /*ThrowAnim*/CommandAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Otherwise...
                else //if()
                {
                    //NOTE: Random just for testing
                    int randattack = new Random().Next(0, 2);

                    //Make Commander use his Retreating Punch; this is the first part where Commander slides backwards slightly
                    if (randattack == 0) ActionManage.CurrentAction = new Slide(this, SlideBackAnim, false, KnockDownVelocity - 1, Hitbox.Empty, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                    //Make Commander start his Punch-Uppercut Combo with the punch
                    else ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 50, 20, 6, ObjectHeight / 2, AttackingAnim.FullDurationExcluding(0), AttackingAnim.GetFrameLength(0), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
            }
            //Tell the minions to assemble and defend Commander
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
            {
                //If Commander has minions and they're not defending him, tell them to defend
                if (HasMinions() == true && MinionsDefending() == false)
                {
                    ActionManage.CurrentAction = new CommandMinions(this, CommandAnim, (int)ActionManager.ActionStates.Command, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
            }
            //Make Commander perform the second part of the Retreating Punch; the punch itself
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
            {
                ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 50, 20, 8, ObjectHeight / 2, AttackingAnim.FullDurationExcluding(0), AttackingAnim.GetFrameLength(0), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);//new SDownKick(this, SDownKickAnim, CreateHitbox(30, 20, (int)Location.W, 8, SDownKickAnim.FullDurationExcluding(SDownKickAnim.MaxFrame), SDownKickAnim.FullDuration(), true, LoadSounds.Kick), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement); //new SimpleAttack(this, AttackAnim, CreateHitbox(61, 20, 30, 6, AttackAnim.FullDurationExcluding(AttackAnim.MaxFrame), AttackAnim.GetFrameLength(AttackAnim.MaxFrame), true, LoadSounds.Kick), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Make Commander perform the Uppercut portion of his Punch-Uppercut combo
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                ActionManage.CurrentAction = new SimpleAttack(this, UppercutAnim, CreateHitboxFeet(true, 20, 20, 7, ObjectHeight, UppercutAnim.GetFrameLength(0), UppercutAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        //Commands a minion to do something
        public void SetMinionCommand(int MinionBehavior)
        {
            int[] minionformation = null;
            if (MinionBehavior == (int)ActionManager.ActionStates.Command) minionformation = MinionFormation();

            //Switch the minions' behavior to the designated one
            for (int i = 0; i < Minions.Count; i++)
            {
                Minions[i].gsAction.ActionChose = true;
                Minions[i].gsAction.SwitchBehavior(MinionBehavior);
                Minions[i].gsAction.ActionChose = true;

                //If the enemy is set to the Command behavior, set the enemy's current action to defend the commander and help
                //This is needed because it stores the commander the enemies have, which they'll need when they have to rechoose an action to perform after getting hit
                if (MinionBehavior == (int)ActionManager.ActionStates.Command)
                {
                    //Change the indexes around so the formation looks nice and even (Ex. 2 minions left, one on each side) based on an array detailing each formation based on the number of minions present
                    //Probably have an array of indices for each case
                    Minions[i].gsAction.CurrentAction = new DefendCommander(Minions[i], Minions[i].GetWalkAnim, this, minionformation[i]);
                }
                else Minions[i].gsAction.EndAction(Main.GetActiveTime);
            }
        }

        //The Command behavior is the same for all enemies that support it
        public static void CommandBehavior(Enemy minion)
        {
            //If a null reference is passed in for the enemy for whatever reason, don't do anything
            if (minion != null)
            {
                DefendCommander defend = minion.gsAction.CurrentAction as DefendCommander;

                //Get the current commander; if the enemy is in this behavior in the first place, then it must stay committed to it
                if (defend != null) minion.gsAction.CurrentAction = new DefendCommander(minion, minion.GetWalkAnim, defend.GetCommander, defend.GetMinionIndex);
                else minion.gsAction.CurState = (int)ActionManager.ActionStates.Movement;
            }
        }

        //Checks for the Commander in the level enemy list and removes the dead enemy from Commander's minion list (that will be removed from the level enemy list right after)
        public static void RemoveMinionLevel(List<Enemy> LevelEnemList, Enemy deadenemy)
        {
            Commander commander = null;
            for (int i = 0; i < LevelEnemList.Count; i++)
            {
                commander = LevelEnemList[i] as Commander;
                if (commander != null)
                {
                    commander.GetMinions.Remove(deadenemy);
                    break;
                }
            }
        }

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine, float waterheight, Color drawcolor, float depth)
        {
            base.DrawAnimations(spriteBatch, OffSet, TileEngine, waterheight, drawcolor, depth);
        }
    }
}
