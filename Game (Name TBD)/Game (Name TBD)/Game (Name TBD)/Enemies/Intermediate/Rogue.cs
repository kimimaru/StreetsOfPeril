﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //An enemy that comes from the side of the screen and hits the player with a projectile - one knocks down and the other doesn't
    public class Rogue : Enemy
    {
        //Rogue's kick animation
        private NewAnimation KickAnim;
        private float PrevKick;

        //The time Rogue waits before sneaking in/out
        private float PrevSneak;

        //Constructor
        public Rogue(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(3, 3);
            Weight = 130;

            //Initialize actions
            ActionManage = new ActionManager(this, (int)ActionManager.ActionStates.Preset1);

            EnemNum = (int)Enemies.Rogue;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            KickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 400, new Vector2(-3, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            PrevKick = 0f;

            PrevSneak = 0f;

            GainKnockInv = true;

            Name = name;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 550;
            HitStun = 450;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 8000f, 100), new Status((int)Status.Statuses.Poison, 3000f, 80) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Reset the enemy's actions upon getting hurt or falling
        protected override void ResetActions()
        {
            base.ResetActions();

            ActionManage.TargetSet = false;

            PrevKick = 0f;
            PrevSneak = 0f;
        }

        //Sets the sneak timer after sneaking in or out of the screen
        public void SetSneakTimer()
        {
            PrevSneak = Main.GetActiveTime;
        }

        //Rogue's action sequence
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            /*Rogue follows this sequence: 
            1. Match Player's Y position
            2. Come in from a side (the side he's closest to)
            3. Shoot
            4. Go out through a side
            
            At any point Rogue is taken away from the side (thrown, hit and knocked down), he immediately moves to the nearest side*/

            //Check for jumping up a ledge
            //if ((activeTime - (Action.Actions[1][(int)AltActions.Jump] as Jump).PrevJump) > 1500f && CheckJumpHigherSurface(activeTime, TileEngine) == true)
            //{
            //    Action.CurBehavior = 1;
            //    Action.CurAction = (int)AltActions.Jump;
            //    return;
            //}

            //Check to see if Rogue is off a side and make him run to that side if so
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)// || (Location.X + OffSet.X > 15 && Location.X + OffSet.X < 387))
            {
                ActionManage.CurrentAction = new RunTo(this, (int)ActionManager.ActionStates.Preset2);
            }
            //Match the player's Y while offscreen
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                ActionManage.CurrentAction = new Approach(this, new Vector2(999, 0), false, (int)ActionManager.ActionStates.Preset3, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1);
            }
            //Sneak in from the side of the screen
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
            {
                ActionManage.CurrentAction = new SneakIn(this, (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1);
            }
            //Shoot a bullet
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset4)
            {
                ActionManage.CurrentAction = new ShootBullet(this, (int)ActionManager.ActionStates.Preset5, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1);
            }
            //Sneak back offscreen
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset5)
            {
                ActionManage.CurrentAction = new SneakOut(this, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1);
            }
            //Rogue kicks the target if he encounters it nearby him while running
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
            {
                ActionManage.CurrentAction = new SimpleAttack(this, KickAnim, CreateHitboxFeet(true, 39, 20, 3, 30, 100, 300, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Preset1);
            }
        }

        protected override bool CanPerformAction()
        {
            //Make Rogue wait before sneaking in or out
            return (base.CanPerformAction() == true && (Main.GetActiveTime - PrevSneak) >= 400f);
        }

        protected override void SetDoAction()
        {
            //Make Rogue kick a player he comes across while running to the side of the screen
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1 && status != (int)Status.Statuses.NoAttack && (Main.GetActiveTime - PrevKick) >= 2000f)
            {
                for (int i = 0; i < SubLvl.GetPlayers.Count; i++)
                {
                    //Check if the player is close enough
                    if (Math.Abs(GetLocationVec2.X - SubLvl.GetPlayers[i].GetLocationVec2.X) < 30 && Math.Abs(GetLocationVec2.Y - SubLvl.GetPlayers[i].GetLocationVec2.Y) < 15)
                    {
                        ActionManage.CurState = (int)ActionManager.ActionStates.Offense;
                        ActionManage.ActionChose = false;
                        PrevKick = Main.GetActiveTime;
                    }
                }
            }

            base.SetDoAction();
        }
    }
}
