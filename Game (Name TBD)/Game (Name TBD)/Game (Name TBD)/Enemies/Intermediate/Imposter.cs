﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Imposter - An enemy that disguises herself as an item container, then reveals herself when a player gets near and starts fighting
    //Imposter has 3 masks that she changes from, each providing her with a different moveset focusing on a different stat - Attack, Defense, or Speed
    public class Imposter : Enemy
    {
        private NewAnimation SwitchMaskAnim;
        private NewAnimation RevealAnim;
        private NewAnimation StabComboAnim;
        private NewAnimation SliceAnim;
        private NewAnimation DefSpecAnim;
        private NewAnimation KickAnim;
        private NewAnimation ThrowAnim;
        private NewAnimation DoubleKickJumpAnim;
        private NewAnimation LeapPunchAnim;
        private NewAnimation UppercutPunchAnim;

        //The three different Masks that Imposter can wear
        private enum Masks
        {
            Offensive, Defensive, Speed
        }

        //The current mask Imposter is on
        private int CurMask;

        //Tells how long Imposter will stay on a mask; it is currently 12 seconds
        private const float MaskTime = 12000f;

        //Tracks how long Imposter is on a mask
        private float PrevMask;

        //Checks how long Imposter was comboed so she can retaliate if she has the Defensive Mask on
        private const float BreakComboTime = 1000f;
        private float TimeComboed;

        //Tells if Imposter is disguised or not
        private bool? Disguised;

        //Constructor - startingmask represents each mask's behavior and moveset; the "disguise" as the item container is triggered when Disguised is true, which it starts out as
        public Imposter(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, int startingmask = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;
            //OrigJumpVelocity.X = Velocity.X;
            //OrigJumpVelocity.Z = Velocity.Y;
            Weight = 150;

            //Set the mask Imposter spawns with
            CurMask = (startingmask % Enum.GetValues(typeof(Masks)).Length);

            EnemNum = (int)Enemies.Imposter;

            //Animations
            StandAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 17, 36, 79), 300, new Vector2(1, 0)));
            WalkingAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(65, 16, 41, 80), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(120, 17, 36, 79), 150, new Vector2(1, 0)), new AnimFrame(new Rectangle(168, 16, 36, 80), 150, new Vector2(1, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(337, 256, 45, 80), 500, new Vector2(12, 0)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(400, 21, 30, 75), 700, new Vector2(12, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(243, 23, 42, 73), 300, new Vector2(-3, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(307, 26, 47, 70), 200, new Vector2(-3, 3)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(447, 28, 54, 68), 0, new Vector2(0, 1)), new AnimFrame(new Rectangle(521, 75, 88, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(624, 62, 83, 34), 150, new Vector2(2, 0)), new AnimFrame(new Rectangle(521, 75, 88, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(624, 62, 83, 34), 150, new Vector2(2, 0)), new AnimFrame(new Rectangle(723, 55, 56, 41), 100, new Vector2(3, 0)));
            
            DefSpecAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(243, 23, 42, 73), 900, new Vector2(-3, 0)));
            DoubleKickJumpAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(84, 271, 80, 51), 1500, new Vector2(-8, 18)));
            SliceAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(337, 256, 45, 80), 125, new Vector2(12, 0)), new AnimFrame(new Rectangle(264, 231, 60, 105), 125, new Vector2(19, 0)), new AnimFrame(new Rectangle(204, 240, 49, 96), 125, new Vector2(10, 0)));
            StabComboAnim = new NewAnimation(SliceAnim, 1f);
            KickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(84, 271, 80, 51), 1000, new Vector2(-8, 18)));
            LeapPunchAnim = new NewAnimation(KickAnim, 1f);
            UppercutPunchAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 17, 36, 79), 300, new Vector2(1, 0)));
            ThrowAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(84, 271, 80, 51), 750, new Vector2(-8, 18)));
            RevealAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(193, 124, 32, 92), 100, new Vector2(3, 0)), new AnimFrame(new Rectangle(241, 129, 48, 87), 100, new Vector2(11, 0)), new AnimFrame(new Rectangle(301, 111, 48, 105), 100, new Vector2(11, 0)), new AnimFrame(new Rectangle(361, 121, 48, 95), 100, new Vector2(11, 0)), new AnimFrame(new Rectangle(421, 127, 48, 89), 100, new Vector2(11, 0)), new AnimFrame(new Rectangle(481, 129, 48, 87), 100, new Vector2(11, 0)), new AnimFrame(new Rectangle(540, 128, 51, 88), 100, new Vector2(10, 0)), new AnimFrame(new Rectangle(600, 137, 36, 79), 100, new Vector2(1, 0)), new AnimFrame(new Rectangle(648, 137, 36, 79), 100, new Vector2(1, 0)));
            SwitchMaskAnim = new NewAnimation(RevealAnim, 1f);

            CurKnockDownAnim = KnockedDownAnim;

            PrevMask = 0f;
            TimeComboed = 0f;
            Disguised = true;

            GainKnockInv = true;

            Name = name;
            ObjectLength = 36;
            ObjectHeight = 80;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 600;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 7500f, 60), new Status((int)Status.Statuses.DefenseDown, 8500f, 60) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        protected override void EnterHitstun()
        {
            base.EnterHitstun();

            //If Imposter is wearing the Defensive Mask and gets hit, start tracking how long she's in hitstun
            if (CurMask == (int)Masks.Defensive && TimeComboed == 0f) TimeComboed = Main.GetActiveTime;
        }

        protected override void KnockDownActions()
        {
            base.KnockDownActions();

            TimeComboed = 0f;
        }

        protected override void AltTakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.AltTakeDamageActions(objecthitby, damage, hitbox);

            //If Imposter got hurt while disguised, get rid of her disguise
            if (Disguised == true)
            {
                Disguised = null;
                PrevMask = Main.GetActiveTime;
            }
        }

        protected override void ResetActions()
        {
            base.ResetActions();

            Disguised = null;
        }

        private void StompAttack(BeatEmUpObj victim, int damage, Hitbox hitbox)
        {
            Hitbox.HitboxTypes origtype = hitbox.GetHitboxType;
            hitbox.SetHitboxType(Hitbox.HitboxTypes.KnockDown);
            hitbox.HitSound = LoadSounds.Punch1;

            bool inair = victim.IsInAir;

            //Make the victim take damage
            victim.TakeDamage(this, damage, hitbox);

            //If the victim is a Fighter...
            if (victim.ObjType == ObjectType.Enemy || victim.ObjType == ObjectType.Player)
            {
                Fighter fight = (Fighter)victim;

                //Check if the fighter was grounded; if so, change its air velocity so it'll land on the ground immediately since it was knocked up from taking damage
                if (inair == false)
                {
                    fight.ChangeAirVelocity(new Vector3(0f, 0f, -fight.GetOrigJumpVelocity.Z));
                }
            }

            hitbox.SetHitboxType(origtype);
        }

        //If Imposter is grabbed with the Defensive Mask on, she'll grab the player, pummel him or her, then throw
        public override void GetGrabbed(BeatEmUpObj opponent)
        {
            base.GetGrabbed(opponent);

            //If Imposter is able to grab and the player can get grabbed, grab the player to start Grab Breaker
            if (CurMask == (int)Masks.Defensive && status != (int)Status.Statuses.NoGrab && Target.CanGetGrabbed() == true)
            {
                GrabRecover();
                ActionManage.CurrentAction = new Grab(this, null, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                ActionManage.ActionChose = true;
            }
        }

        //Gets the current mask Imposter is wearing
        public int GetCurMask()
        {
            return CurMask;
        }

        //Checks if Imposter can switch masks
        private bool CanSwitchMask()
        {
            //If Imposter is attacking, or if she's defending herself with the Defensive mask on, don't switch; otherwise, check the timer and see if she should change masks
            return (((CurMask == (int)Masks.Defensive && ActionManage.CurState == (int)ActionManager.ActionStates.Movement) || (CurMask != (int)Masks.Defensive && ActionManage.CurState != (int)ActionManager.ActionStates.Offense)) && (Main.GetActiveTime - PrevMask) >= MaskTime);
        }

        //Switches the Mask Imposter is wearing - this is called after the animation is finished
        public void ChangeMask()
        {
            //Switch to the next mask, starting back at the Offensive Mask if the current mask is the Speed Mask
            //Offensive: Imposter gains a Dagger which puts out stronger and further ranged hitboxes at the cost of speed; she also can throw smaller daggers as projectiles (can't be picked up)
            //Defensive: Imposter is normal speed and has a lot of defensive options, like a defensive special if she's in hitstun for too long
            //Speed: Imposter gains speed, the ability to do aerial attacks, and jumps around frequently as a form of movement
            CurMask = (CurMask + 1) % (int)Enum.GetValues(typeof(Masks)).Length;
            ResetActions();
            PrevMask = Main.GetActiveTime;
        }

        protected override Vector2 ObjVelocityFactor()
        {
            Vector2 maskvelocity = base.ObjVelocityFactor();

            //Adjust Imposter's movespeed based on the mask she has
            if (CurMask == (int)Masks.Offensive) maskvelocity += new Vector2(-1, -1);
            else if (CurMask == (int)Masks.Speed) maskvelocity += new Vector2(1, 1);

            return maskvelocity;
        }

        private void ChooseNewTarget(List<Player> Players)
        {
            //There is a 1/5 chance that Imposter will switch targets in her Movement state if there are nearby players
            if (Players.Count > 1 && new Random().Next(0, 5) == 0)
            {
                float ClosestPlayer = 9999f;

                //Check for the closest player and switch to that one; this can be the same target she had previously
                for (int i = 0; i < Players.Count; i++)
                {
                    float playerdistance = Vector2.Distance(Players[i].GetLocationVec2, GetLocationVec2);

                    if (Players[i].KnockedDown == false && playerdistance < ClosestPlayer)
                    {
                        ClosestPlayer = playerdistance;
                        Target = Players[i];
                    }
                }
            }
        }

        //Chooses either Imposter's Downward Slice move or her Stab Combo move when she's wearing the Offensive Mask
        private void RandomOMaskAttack()
        {
            //There's a 1/2 chance of Imposter attacking with either a stab combo or a downward slice
            int randattack = new Random().Next(0, 2);

            //Attack the player with either a stab combo or a downward slice
            if (randattack == 0)
                ActionManage.CurrentAction = new SimpleAttack(this, SliceAnim, CreateHitboxFeet(true, 30, 20, 5, 10, SliceAnim.FullDurationExcluding(2), SliceAnim.GetFrameLength(2), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.SharpHit, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            else ActionManage.CurrentAction = new SimpleAttack(this, StabComboAnim, CreateHitboxFeet(true, 30, 20, 5, 20, 0, 200, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.SharpHit, false), (int)ActionManager.ActionStates.Preset3, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
        }

        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            /*Imposter's AI:
             * 1. With the Offensive mask on, is very aggressive and will approach the target often (like Souther from SOR1)
             * 2. With the Defensive mask on, act sort of like the SOR 1 sister duo in Streets of Rage Remake, with only 1 of them left
             *      -Move in and out like SOR 3 Electra, attacking when in range
             *      -The basic idea is that she'll give you time to hit her when she approaches, but you can't hit her too much otherwise she'll retaliate
             * 3. With the Speed mask on, jump around, weaving in and out then attacking the target when matching its Y value; sort of like Zamza from SOR2, including the timer between jumps,
             *    except with fewer up-close moves
             */

            //If Imposter's is disguised, she's disguising herself as an item container, so don't make her do anything until a player comes near
            if (Disguised == true)
            {
                //Look through all the players
                for (int i = 0; i < Players.Count; i++)
                {
                    //If a player is within a certain distance from Imposter, make her reveal herself and set her target to that player
                    if (Vector2.Distance(Players[i].GetLocationVec2, GetLocationVec2) <= 50)
                    {
                        Disguised = false;
                        RevealAnim.Reset();
                        Target = Players[i];
                        break;
                    }
                }
            }
            //If Imposter isn't disguised or revealing herself, start doing normal actions
            else if (Disguised == null)
            {
                //Every 12 seconds, Imposter will switch masks
                if (CanSwitchMask() == true)
                {
                    ActionManage.CurState = (int)ActionManager.ActionStates.Preset5;
                    ActionManage.CurrentAction = new SwitchMask(this, SwitchMaskAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset5, (int)ActionManager.ActionStates.Preset5);
                }
                //If the target is knocked down, don't do anything
                else if (Target.IsDead/*GIsKnockedDown*/ == false)
                {
                    //Movement
                    if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                    {
                        ChooseNewTarget(Players);

                        MovementAI();
                    }
                    //Offensive
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                    {
                        OffenseAI();
                    }
                    //Defensive
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                    {
                        DefenseAI();
                    }
                    //Preset1
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                    {
                        Preset1AI();
                    }
                    //Preset2
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                    {
                        Preset2AI();
                    }
                    //Do the second hit of Imposter's Stab Combo
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
                    {
                        ActionManage.CurrentAction = new SimpleAttack(this, StabComboAnim, CreateHitboxFeet(true, 30, 20, 5, 20, 0, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.SharpHit, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    //Perform Imposter's Defensive Special, Combo Breaker, if it's somehow interrupted
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset4)
                    {
                        ActionManage.CurrentAction = new SimpleAttack(this, DefSpecAnim, CreateHitboxFeet(true, 40, 30, 6, 30, 150, 650, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Preset4);
                    }
                    //Switch masks if it was somehow interrupted previously
                    else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset5)
                    {
                        ActionManage.CurrentAction = new SwitchMask(this, SwitchMaskAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset5, (int)ActionManager.ActionStates.Preset5);
                    }
                }
                //Switch to Idle if the target is knocked down and Imposter isn't already idling
                else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
                {
                    //Imposter's Offensive behavior with the Offensive Mask on is reliant on which action she previously performed, so reset back to movement after Idle is completed
                    ActionManage.CurrentAction = new Idle(this, (CurMask == (int)Masks.Offensive && ActionManage.CurState == (int)ActionManager.ActionStates.Offense) ? (int?)ActionManager.ActionStates.Movement : null);
                }
            }
        }

        //The AI for Imposter when she's in the Movement behavior
        private void MovementAI()
        {
            //Offensive Mask
            if (CurMask == (int)Masks.Offensive)
            {
                //Randomly choose to retreat for 3.5 seconds or move around the target in a rectangle
                int movementoption = new Random().Next(0, 2);

                //Retreat for 3.5 seconds
                if (movementoption == 0)
                    ActionManage.CurrentAction = new Retreat(this, 3500f, 65, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Offense);
                //Move in a rectangle around the target
                else
                    ActionManage.CurrentAction = new CircleAround(this, new Vector2(65), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Offense);

                //Move towards the player to go in for either a downward slice or a stab combo
                //Action.CurrentAction = new Approach(this, new Vector2(30, 0f), (int)ActionManager.ActionStates.Offense, null, null);
            }
            //Defensive Mask
            else if (CurMask == (int)Masks.Defensive)
            {
                //Retreat from the player
                ActionManage.CurrentAction = new Retreat(this, 4000f, 60, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Offense);
            }
            //Speed Mask
            else if (CurMask == (int)Masks.Speed)
            {
                //Check how far Imposter is from the target and determine where to jump
                //If Imposter is offscreen, then jump back onscreen; the target is always onscreen, so use its position relative to Imposter's to find out which direction to jump
                /*if (IsOffScreen(OffSet) == true)
                {
                    Vector2 jumptowards = new Vector2(XYLoc.X, XYLoc.Y);

                    //If offscreen in the X direction, jump towards the center of the screen
                    if (IsOffScreenX(OffSet) == true)
                    {
                        if (TargetInFront() == true) jumptowards.X += Main.ScreenHalf.X;
                        else jumptowards.X -= Main.ScreenHalf.X;
                    }

                    //If offscreen in the Y direction, jump towards the center of the screen
                    if (IsOffScreenY(OffSet) == true)
                    {
                        if (TargetBelow() == true) jumptowards.Y += Main.ScreenHalf.Y;
                        else jumptowards.Y -= Main.ScreenHalf.Y;
                    }

                    Action.CurrentAction = new JumpTo(this, jumptowards, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement);
                }
                else
                {*/
                //Randomly choose to either match the target's X position, Y position, or Y position while going directly in front of the player
                int randaction = new Random().Next(0, 3);
                Vector2 jumploc = new Vector2();

                //Make sure Imposter doesn't jump to the target's X if she's already really close to it
                if (randaction == 0 && TargetProximityX() > TrueVelocity.X)
                    jumploc = new Vector2(Target.GetLocationVec2.X, GetLocationVec2.Y);
                //Make sure Imposter doesn't jump to the target's Y if she's already really close to it
                else if (randaction == 1 && TargetProximityY() > TrueVelocity.Y)
                    jumploc = new Vector2(GetLocationVec2.X, Target.GetLocationVec2.Y);
                //Approach the target directly if the other options aren't possible
                else
                    jumploc = new Vector2(Target.GetLocationVec2.X + (Target.FacingRight == true ? 40 : -20), Target.GetLocationVec2.Y);

                ActionManage.CurrentAction = new JumpTo(this, jumploc, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement);
                //}
            }
        }

        //The AI for Imposter when she's in the Offensive behavior
        private void OffenseAI()
        {
            //Offensive Mask
            if (CurMask == (int)Masks.Offensive)
            {
                //After retreating...
                if (ActionManage.CurrentAction is Retreat)
                {
                    //If Imposter is too far from the target, switch back to movement
                    if (TargetProximityX() > 120 && TargetProximityY() > (TrueVelocity.Y * 2))
                    {
                        ActionManage.ActionNotDone = true;
                        ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                    }
                    //Otherwise, if Imposter is a decent distance away from the target, throw a small dagger
                    else if (TargetProximityX() >= 60)
                    {
                        //Throw a small dagger
                        ActionManage.CurrentAction = new ShootBullet(this, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    //Otherwise, if Imposter is close to the target, choose a random move
                    else
                    {
                        //Choose a random move
                        RandomOMaskAttack();
                    }
                }
                //After moving around the target...
                else if (ActionManage.CurrentAction is CircleAround || ActionManage.CurrentAction is Approach)
                {
                    //Make sure Imposter is close enough to the target
                    if (TargetProximityY() <= (TrueVelocity.Y * 2) && TargetProximityX() <= 40f)
                    {
                        //If so, choose a random move
                        RandomOMaskAttack();
                    }
                    //If Imposter isn't in range to land the attack, match the target's Y position and go up to the target
                    else
                    {
                        ActionManage.CurrentAction = new Approach(this, new Vector2(30f, 0f), true, null, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                }
            }
            //Defensive Mask
            else if (CurMask == (int)Masks.Defensive)
            {
                //Slowly approach the player and go in for a kick
                if (TargetProximityY() <= TrueVelocity.Y && TargetProximityX() <= 35f)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, KickAnim, CreateHitboxFeet(true, 39, 35, 3, 30, 100, 300, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //If Imposter isn't in range to land the kick, match the target's Y position and go up to the target
                else
                {
                    ActionManage.CurrentAction = new Approach(this, new Vector2(35f, 0f), true, null, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
            }
            //Speed Mask
            else if (CurMask == (int)Masks.Speed)
            {
                //If Imposter is around the target's Y position then do an attack
                if (TargetProximityX() <= 120 && TargetProximityY() <= (TrueVelocity.Y * 2))
                {
                    //Imposter randomly chooses to switch back to the Offense or Defensive behavior if gets hit while trying to attack
                    int? offensebehavior = (new Random().Next(0, 2) == 0) ? (int)ActionManager.ActionStates.Offense : (int)ActionManager.ActionStates.Defense;

                    //If Imposter is close to the target from the X and Y, do an uppercut
                    if (TargetProximityX() <= 20f)
                    {
                        ActionManage.CurrentAction = new SimpleAttack(this, UppercutPunchAnim, CreateHitboxFeet(true, 39, 20, 2, 30, 0, 700, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Defense, offensebehavior);//Movement);
                    }
                    //If Imposter is mid-distance away, do the Leap Punch move
                    else if (TargetProximityX() <= 70f)
                    {
                        ActionManage.CurrentAction = new JumpAttack(this, LeapPunchAnim, new Vector3(ForwardVal(TrueVelocity.X + 1), 0f, OrigJumpVelocity.Z - 1f), CreateHitboxFeet(true, 39, 20, 5, 25, 200, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), null, (int)ActionManager.ActionStates.Defense, offensebehavior);//Movement);
                    }
                    //Otherwise, do the Double Kick move
                    else
                    {
                        ActionManage.CurrentAction = new DoubleKickJ(this, DoubleKickJumpAnim, (int)ActionManager.ActionStates.Defense, offensebehavior);//Movement);
                    }
                }
                //Otherwise switch back to movement
                else
                {
                    ActionManage.ActionNotDone = true;
                }
            }
        }

        //The AI for Imposter when she's in the Defensive behavior
        private void DefenseAI()
        {
            //Offensive Mask
            if (CurMask == (int)Masks.Offensive)
            {
                //No behavior yet, so just switch back to Idle
                ActionManage.CurrentAction = new Idle(this, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Defensive Mask
            else if (CurMask == (int)Masks.Defensive)
            {
                //Check if Imposter is grabbing; if she is, then it's because she's following up with grab breaker
                //if (IsGrabbing == true)
                //{
                    //IsGrabbing = false;
                ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 20, 20, 5, 15, 100, 150, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                //}
            }
            //Speed Mask
            else if (CurMask == (int)Masks.Speed)
            {
                Vector2 JumpAway = new Vector2(GetLocationVec2.X, GetLocationVec2.Y);

                if (TargetInFront() == true) JumpAway.X -= 60;
                else JumpAway.X += 60;

                if (TargetBelow() == true) JumpAway.Y -= 60;
                else JumpAway.Y += 60;

                //When in the Defense state, usually after an attack, Imposter jumps away from the target and goes back into the Movement state
                ActionManage.CurrentAction = new JumpTo(this, JumpAway, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        //The AI for Imposter when she's in the Preset1 behavior
        private void Preset1AI()
        {
            //Do another attack as part of the Grab Breaker move
            if (CurMask == (int)Masks.Defensive)
            {
                ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 20, 20, 5, 15, 100, 150, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        //The AI for Imposter when she's in the Preset2 behavior
        private void Preset2AI()
        {
            //Do a throw as part of the Grab Breaker Move
            if (CurMask == (int)Masks.Defensive)
            {
                ActionManage.CurrentAction = new Throw(this, ThrowAnim, CreateHitboxFeet(true, 20, 20, 5, 15, 250, 1, Hitbox.HitboxTypes.KnockDown, TowardsDir, null, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        //Imposter can perform an action when grabbed if she has the Defensive Mask on
        protected override bool CanDoAction()
        {
            //NOTE: Needs revision; this currently allows Imposter to interrupt most actions while wearing the Speed mask
            return (base.CanDoAction() == true && (CurMask != (int)Masks.Speed || ActionManage.TimeLastActionEnded >= 400f));//(IsHit == false && IsKnockedDown == false && ((Grabbox.GSIsGrabbed == false) || (Grabbox.GSIsGrabbed == true && Action.CurBehavior == (int)Masks.Defensive)) && Carried == false);
        }

        private void UpdateRevealAnim(float activeTime)
        {
            //If Imposter isn't disguised, she's revealing herself - a value of null indicates that Imposter is done revealing herself
            if (Disguised == false)
            {
                RevealAnim.Update(activeTime, GravityValue);
                if (RevealAnim.IsAnimationEnd() == true)
                {
                    Disguised = null;
                    PrevMask = activeTime;
                }
            }
        }

        protected override void UpdateHitAnim(float activeTime)
        {
            if (IsHit == true)
            {
                HurtAnim.Update(activeTime);

                if (IsInHitstun == false)
                {
                    IsHit = false;
                    TimeComboed = 0f;
                }
                //If Imposter has the Defensive Mask on and is comboed for over 1 second, get her out of hitstun and make her use her defensive special, which is a kick in which she raises her leg above her head and slams it down (similar to Shiva's defensive special in SOR2 except Imposter lifts her leg straight up instead)
                else if (CurMask == (int)Masks.Defensive && (Main.GetActiveTime - TimeComboed) >= BreakComboTime)
                {
                    ActionManage.CurState = (int)ActionManager.ActionStates.Preset4;
                    ActionManage.CurrentAction = new SimpleAttack(this, DefSpecAnim, CreateThrowHitbox(true, 40, 30, 6, 30, 150, 650, TowardsDir, 0, StompAttack, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Preset4, (int)ActionManager.ActionStates.Preset4);
                    ActionManage.ActionChose = true;
                    IsHit = false;
                    TimeComboed = 0f;
                }
            }
        }

        protected override void UpdateAnimations(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        {
            if (Disguised == false) UpdateRevealAnim(activeTime);
            else base.UpdateAnimations(activeTime, OffSet, TileEngine, Solids);
        }

        public override bool CanGetGrabbed()
        {
            return (base.CanGetGrabbed() == true && Disguised == null);
        }

        protected override void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine, float waterheight, Color drawcolor, float depth)
        {
            //Draw an item container sprite if Imposter is disguised; otherwise, draw her normal animations - don't draw the container tinted by her status, otherwise it'll give away her disguise
            if (Disguised == true) new Animation(LoadGraphics.ContainerSprite, false, 2, 0).Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), false, drawcolor, 0f, depth);
            else if (Disguised == false) RevealAnim.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), !FacingRight, drawcolor, 0f, depth);
            else base.DrawAnimations(spriteBatch, OffSet, TileEngine, waterheight, drawcolor, depth);
        }
    }
}
