﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //An enemy that uses nunchucks as weapons
    public class Greg : Enemy
    {
        private NewAnimation ThrowAnim;
        private NewAnimation ThrustAnim;
        private NewAnimation SpinAnim;
        private NewAnimation NSwingAnim;

        //Constructor
        public Greg(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;
            Weight = 145;

            EnemNum = (int)Enemies.Greg;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            ThrowAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 1000));
            ThrustAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 1500, new Vector2(2, 0)));
            SpinAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 200, new Vector2(1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 200, new Vector2(1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 200, new Vector2(1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 200, new Vector2(1, 0)));
            NSwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 400, new Vector2(2, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 500;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageDown, 8000f, 40), new Status((int)Status.Statuses.DefenseDown, 9000f, 35), new Status((int)Status.Statuses.SpeedDown, 8000f, 45) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Checks if Greg should use the Nunchuck Spin move or not
        private bool CheckNunchuckSpin(List<Player> Players)
        {
            //Check how many players are surrounding Greg; if he is surrounded by 2 or more players, make him use Nunchuck Spin
            if (Players.Count > 1)
            {
                int playersaround = 0;
                for (int i = 0; i < Players.Count; i++)
                {
                    //Get the distance each player is from Greg
                    if (Vector2.Distance(GetLocationVec2, Players[i].GetLocationVec2) <= 50)
                        playersaround++;
                }

                return (playersaround >= 2);
            }

            return false;
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Greg moves around the target
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Greg approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the target
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Greg is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y)
                    {
                        //From mid-distance, Greg will do a Nunchuck Thrust or a Nunchuck Swing
                        if (TargetProximityX() > 25f)
                        {
                            int randmove = new Random().Next(0, 2);

                            if (randmove == 0)
                                ActionManage.CurrentAction = new SimpleAttack(this, ThrustAnim, CreateHitboxFeet(true, 30, 20, 4, 20, 1000, 100, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                            else ActionManage.CurrentAction = new SimpleAttack(this, NSwingAnim, CreateHitboxFeet(true, 30, 30, 3, 30, 100, 200, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        else
                        {
                            int randmove = new Random().Next(0, 4);

                            //Greg will do a basic attack; there is a 1/4 chance he'll do a grab
                            if (randmove != 0)
                                ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                            else ActionManage.CurrentAction = new Grab(this, null, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Greg isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(new Random().Next(0, 2) == 0 ? 25f : 30f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Greg has a 1/4 chance of performing the Nunchuck Spin move normally in his Defensive behavior; if more than one player is near, he'll always perform it
                    int spin = new Random().Next(0, 4);

                    if (spin == 0 || CheckNunchuckSpin(Players) == true)
                        ActionManage.CurrentAction = new NSpin(this, SpinAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                    //Back away from the target until you're far enough
                    else if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.CurrentAction = new Idle(this, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
                //Performed after a successful grab; throw the target
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new Throw(this, ThrowAnim, CreateHitboxFeet(true, 39, 20, 0, 30, 0, 1, Hitbox.HitboxTypes.KnockDown, TowardsDir, null, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
                //Performed after the first swing in the Nunchuck Swing move; this is the second one that knocks down
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, NSwingAnim, CreateHitboxFeet(true, 30, 30, 4, 30, 100, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }
    }
}
