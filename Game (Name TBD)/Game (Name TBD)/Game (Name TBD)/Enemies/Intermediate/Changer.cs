﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //An enemy that can punch, charge at players, and generate or invert his own status after a certain amount of time
    public class Changer : Enemy
    {
        //The animation for generating a new status
        private NewAnimation GenerateAnim;

        //The animation for charging
        private NewAnimation ChargeAnim;
        private NewAnimation SlideAnim;

        //Alternate actions
        private enum AltActions
        {
            Idle, Generate
        };

        //Enumeration of actions
        private enum Actions
        {
            Idle, Jump, Charge, WalkTD, WalkTH, WalkTV, MWalkTH, MWalkTV, MWalkAH, MWalkAV, MatchY, Slide, BasicAttack, SWalkTH, SWalkTV, SWalkAH, SWalkAV
        };

        //The counter telling Changer when to generate a new status; happens every 10 seconds
        private const float GenerateTime = 10000f;
        private float PrevGenerate;

        //Constructor - Changer can have varying durations and percentages for his Status effects, so he has the lowest values for each in his potential statuses
        public Changer(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;
            Grabbox = new Grabbox(Grabbox.GetLength(ObjectLength), 6, 1300, 1000);

            EnemNum = (int)Enemies.Changer;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            GenerateAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 1000, new Vector2(1, 0)));
            ChargeAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 1000, new Vector2(-3, 0)));
            SlideAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 1000, new Vector2(1, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            PrevGenerate = Main.GetActiveTime;

            Name = name;

            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 500;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            StatForever = false;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageBoost, 4000f, 20), new Status((int)Status.Statuses.DamageDown, 4000f, 20), new Status((int)Status.Statuses.DefenseBoost, 4000f, 20), new Status((int)Status.Statuses.DefenseDown, 4000f, 20), new Status((int)Status.Statuses.SpeedBoost, 4000f, 20), new Status((int)Status.Statuses.SpeedDown, 4000f, 20), new Status((int)Status.Statuses.StunDown, 4000f, 20) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Sets the time between generating new status effects
        public void SetGenerateTimer()
        {
            PrevGenerate = Main.GetActiveTime;
        }

        //Checks if Changer can generate a new status
        private bool CanGenerate()
        {
            //If Changer isn't defending himself and enough time passed since the last generate
            return (ActionManage.CurState != (int)ActionManager.ActionStates.Defense && (Main.GetActiveTime - PrevGenerate) >= GenerateTime);
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //After enough time has passed since the last time a status has been generated, generate a new status effect!
            if (CanGenerate() == true)
            {
                ActionManage.CurrentAction = new Generate(this, GenerateAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //If the target is knocked down, don't do anything
            else if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Mark moves around the target
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        ActionManage.CurrentAction = new Approach(this, new Vector2(70), false, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Offense);
                    }
                    //If you're around the same Y position as the target and are far in the X, prepare to charge at the target
                    else if (TargetProximityY() <= (TrueVelocity.Y * 2) && TargetProximityX() >= 70)
                    {
                        ActionManage.CurrentAction = new Idle(this, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    //Otherwise start moving around the target
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //Check Changer's Y position in relation to the target's Y...
                    if (TargetProximityY() <= (TrueVelocity.Y * 2))
                    {
                        //If Changer is really close to the target in the X direction, attack
                        if (TargetProximityX() <= 25f)
                        {
                            //There is a 1/3 chance that Mark will go on the defensive after attacking
                            int randbehavior = (new Random().Next(0, 3) == 0) ? (int)ActionManager.ActionStates.Defense : (int)ActionManager.ActionStates.Movement;

                            //Changer will do a basic attack; there is a 1/4 chance he'll do a kick
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), randbehavior, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        //If Changer is mid-distance from the target, do a slide
                        else if (TargetProximityX() <= 70)
                            ActionManage.CurrentAction = new Slide(this, SlideAnim, true, KnockDownVelocity, null, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        //If Changer is far from the target, do a slide
                        else
                            ActionManage.CurrentAction = new Charge(this, ChargeAnim, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    //If Changer isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(25f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.ActionNotDone = true;
                }
            }
            else if (!(ActionManage.CurrentAction is Idle))
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }
    }
}
