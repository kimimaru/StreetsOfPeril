﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Grunt : Enemy
    {
        private NewAnimation JumpKickAnim;
        private NewAnimation LungeAnim;
        private NewAnimation HeadbuttAnim;

        //Constructor
        public Grunt(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;
            Weight = 120;

            //Start with the minecart behavior if set to do so
            if (behavior > 0) ActionManage.CurState = (int)ActionManager.ActionStates.Preset1;

            EnemNum = (int)Enemies.Grunt;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 700, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            JumpKickAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)));
            LungeAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 300, new Vector2(-10, 0)));
            HeadbuttAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 300, new Vector2(-3, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            Name = name;

            SetUpHurtbox();
            SetUpHealth(health);

            Damage = damage;
            Defense = defense;
            Score = 450;
            HitStun = 400;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
            hud = display;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageBoost, 11000f, 30), new Status((int)Status.Statuses.DamageDown, 10000, 60), new Status((int)Status.Statuses.DefenseDown, 10000, 55), new Status((int)Status.Statuses.SpeedDown, 9000f, 70), new Status((int)Status.Statuses.Poison, 5000f, 40) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2); }
        }

        private void GrabHeadbutt(BeatEmUpObj victim, int damage, Hitbox hitbox, SubLevel level)
        {
            //Grab victim in the air
            if (true)
            {

            }
            //If the target can't be grabbed, simply damage it
            else
            {

            }
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Mark moves around the player
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the player if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Mark approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the player
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Grunt is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y)
                    {
                        //Grunt's punch knocks down
                        if (TargetProximityX() <= 25f)
                        {
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 39, 20, 0, 30, 0, 700, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        else
                        {
                            //1/2 chance of doing a Lunge Grab or a Jump Kick
                            int randmove = new Random().Next(0, 2);

                            if (randmove == 0)
                                ActionManage.CurrentAction = new LungeGrab(this, HeadbuttAnim, (int)ActionManager.ActionStates.Preset3, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                            else ActionManage.CurrentAction = new JumpAttack(this, null, new Vector3(ForwardVal(TrueVelocity.X), 0f, OrigJumpVelocity.Z), CreateHitboxFeet(true, 39, 20, 0, 25, 0, 700, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), LoadSounds.JumpKick, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Mark isn't in range to land an attack, match the target's Y position and go up to the target
                    else ActionManage.CurrentAction = new Approach(this, new Vector2(new Random().Next(0, 2) == 0 ? 25f : 50f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    else ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                }
                //Grunt's minecart behavior; move towards the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new MoveTo(this, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Preset2);
                }
                //The second part of Grunt's minecart behavior; jump onto the player minecart
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
                {
                    ActionManage.CurrentAction = new JumpAttack(this, null, new Vector3(ForwardVal(OrigJumpVelocity.X), 0f, OrigJumpVelocity.Z), null, LoadSounds.JumpKick, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Called after a Lunge Grab; Grunt headbutts the target
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
                {
                    //Headbutt the target
                    ActionManage.CurrentAction = new Headbutt(this, HeadbuttAnim, CreateHitboxFeet(true, 20, 20, 0, 20, 300, 100, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
            }
            else if (!(ActionManage.CurrentAction is Idle))
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        protected override bool CanDoAction()
        {
            return (base.CanDoAction() == true && (ActionManage.CurState != (int)ActionManager.ActionStates.Preset2 || ActionManage.TimeLastActionEnded >= 400f));
        }

        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && ActionManage.CurState == (int)ActionManager.ActionStates.Movement);
        }

        //Grunt picks up: Items (health, HoT, status)
        public override bool CheckShouldPickUp(Item item, Weapon Weapon)
        {
            if (item != null)
            {
                Random random = new Random();

                //Grunt focuses primarily on picking up health items, so the percentage of health he has remaining will greatly influence him picking up the item
                //Grunt does NOT pick up replacement tanks underwater, however
                if ((TotalCurHealth() / TotalMaxHealth()) * 100 <= 45)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
