﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Gerald and Kate, Male and Female versions of the Weapon Wielders - they act differently and have a different moveset depending on the weapon they're holding
    //They can use any weapon including wearable ones like Brass Knuckles (but can't steal them), and if they have no weapons they'll scramble and find the nearest one; if one doesn't exist, they'll take the player's weapon if the player has one and it isn't wearable
    //They even have their own unique weapon, the Swift Blade, that they can spawn with which is unobtainable elsewhere
    //NOTE: Find a way to efficiently add the weapon they spawn with to the weapon list
    public class WeaponWielder : Enemy
    {
        private NewAnimation StabAnim;
        private NewAnimation SwingAnim;
        private NewAnimation RollAnim;
        private NewAnimation JumpslashAnim;
        private NewAnimation HSlashAnim;
        private NewAnimation StealAnim;

        //The spawnweapon parameter signifies the weapon Gerald or Kate spawns with
        //Kate has slightly different stats than Gerald
        public WeaponWielder(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, Vector2 velocity, Weapon spawnweapon, bool Male, int alternate = 0)
        {
            SetLocation(location);
            Velocity = velocity;

            EnemNum = (int)Enemies.WeaponWielder;

            //Animations
            StandAnim = new NewAnimation(true, true, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 13, 39, 71), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(380, 129, 39, 70), 150, new Vector2(-3, 0)), new AnimFrame(new Rectangle(450, 130, 39, 69), 200, new Vector2(-3, 0)));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(11, 14, 48, 70), 200, new Vector2(0, 0)), new AnimFrame(new Rectangle(132, 12, 39, 72), 200, new Vector2(-3, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(192, 182, 69, 70), 500, new Vector2(-10, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 300));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 105, 46, 63), 700, new Vector2(8, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 105, 55, 63), 0, new Vector2(1, 1)), new AnimFrame(new Rectangle(144, 147, 79, 21), 400, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(144, 147, 79, 21), 150, new Vector2(4, 0)), new AnimFrame(new Rectangle(240, 137, 70, 31), 150, new Vector2(-1, 0)), new AnimFrame(new Rectangle(324, 125, 40, 43), 100, new Vector2(1, 0)));
            PickupAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 350, new Vector2(1, 0)));

            StabAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(264, 22, 43, 62), 200, new Vector2(2, 0)));
            SwingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(204, 22, 40, 62), 600));
            RollAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 150));
            HSlashAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 500));
            JumpslashAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(132, 12, 39, 72), 2000));
            StealAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(324, 125, 40, 43), 250, new Vector2(1, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            //If the Weapon Wielder spawned with a weapon, give it to him/her
            //NOTE: This is broken since we changed Weapon to properly inherit from PickUpObj, but it still won't work without a SubLevel reference
            //So this is what we'll do: set the WeaponWielder's spawnweapon here, then call GetPickedUp on the weapon in Spawn()
            if (spawnweapon != null)
            {
                spawnweapon.GetPickedUp(this);
                weapon = spawnweapon;
            }

            Name = name;

            Damage = damage;
            Defense = defense;
            Score = 650;
            HitStun = 350;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;

            //Gerald's stats
            if (Male == true)
            {
                Weight = 155;
            }
            //Kate's stats; Kate has less Health, Damage, and Defense than Gerald but has less hitstun and more speed than him
            else
            {
                if ((health - 20) >= 1) health -= 20;
                Velocity.X += 1;
                if ((Damage - 1) >= 0) Damage -= 1;
                if ((Defense - 1) >= 0) Defense -= 1;
                HitStun = 325;
            }

            SetUpHealth(health);
            SetUpHurtbox();
            SetUpSprites();
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.SpeedBoost, 7000f, 45) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Checks if the WeaponWielder is male or female
        public bool IsMale
        {
            get { return (Weight > 140); }
        }

        protected override void HitWallX()
        {
            //Stop the Roll action if the enemy hits a wall or solid while rolling
            if (ActionManage.CurrentAction.Animation == RollAnim)
            {
                //Face the player after the roll
                FaceTarget();

                ActionManage.EndAction(Main.GetActiveTime);
            }
        }

        protected override void HitSolidX()
        {
            HitWallX();
        }

        //Gerald/Kate are invincible while rolling
        protected override bool EnemMoveInvincibility
        {
            get { return (ActionManage.CurrentAction.Animation == RollAnim); }//IsRolling == false); }
        }

        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //The Weapon Wielder has a weapon, so make him/her act based on the type of weapon equipped
            if (weapon != null)
            {
                if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.None)
                {
                    WearableWeaponAI();
                }
                else if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                {
                    StabbableWeaponAI();
                }
                else if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                {
                    SwingableWeaponAI();
                }
            }
            //The Weapon Wielder scrambles and look for nearby weapons, steals a weapon from the target, or fights the target with his or her bare hands
            else
            {
                NoWeaponAI();
            }
        }

        private void NoWeaponAI()
        {
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
            {
                //If the target has a weapon, approach the target with the intention of stealing it from the target
                if (Target.weapon != null && Target.weapon.GetWeaponType != (int)Weapon.WeaponTypes.None)
                {
                    ActionManage.CurrentAction = new Approach(this, new Vector2(25, 0), false, (int)ActionManager.ActionStates.Preset1, null, null);
                }
                //There are no weapons available on the ground, fight with your bare hands!
                else
                {
                    //Move towards the target if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of the Weapon Wielder approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(60), (int)ActionManager.ActionStates.Offense, null, null);
                }
            }
            //Fight the target with your bare hands
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
            {
                //1/2 chance for a punch or kick
                if (new Random().Next(0, 2) == 0)
                    ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 0, 30, 0, AttackingAnim.FullDuration(), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                else ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 30, 20, 0, 30, 0, AttackingAnim.FullDuration(), Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
            }
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
            {

            }
            //Grab the target
            //NOTE: Make sure to always check if the target has a weapon that can be stolen because the target can pick up or throw a weapon away in between these actions
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
            {
                //If Gerald/Kate is close enough to grab the target, then do so
                if (TargetProximityX() <= 30f && TargetProximityY() <= (TrueVelocity.Y * 2))
                    ActionManage.CurrentAction = new Grab(this, null, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                //Otherwise, switch back to movement
                else ActionManage.ActionNotDone = true;
            }
            //Steal the target's weapon
            //The Weapon Wielders attack the target with his/her own weapon after stealing it
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                //Make sure Gerald/Kate is close enough to steal the weapon from the should-be grabbed target, then steal the weapon and switch to Preset2 to attack the target with it
                if (/*Target.GIsGrabbed == true && */TargetProximityX() <= 30f && TargetProximityY() <= (TrueVelocity.Y * 2))
                    ActionManage.CurrentAction = new StealWeapon(this, StealAnim, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                //Otherwise, switch back to movement
                else ActionManage.ActionNotDone = true;
            }
        }

        private void WearableWeaponAI()
        {

        }

        private void StabbableWeaponAI()
        {
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
            {
                ActionManage.CurrentAction = new CircleAround(this, new Vector2(60), (int)ActionManager.ActionStates.Offense, null, null);
            }
        }

        //Gerald/Kate use the weapon's range to their advantage and attack from mid-range
        private void SwingableWeaponAI()
        {
            //Switch between approaching and retreating for 2 times at an uncomfortably close range from the target, then attack
            if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
            {
                ActionManage.CurrentAction = new Approach(this, new Vector2(30, 0), false, (int)ActionManager.ActionStates.Preset3, null, null);
            }
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
            {
                //Do either a Jump Slash Combo or a Horizontal Slash
                int rand = new Random().Next(0, 2);

                //The Jump Slash Combo leads into a Horizontal Slash
                if (rand == 0)
                    ActionManage.CurrentAction = new Jumpslash(this, JumpslashAnim, (int)ActionManager.ActionStates.Preset2, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                else ActionManage.CurrentAction = new WeaponAttack(this, HSlashAnim, weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 3, 100, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, false), weapon.GWeaponUseSound, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
            {
                //Roll behind the target if the target and the enemy are facing each other
                if (TargetInFront(false) == true && Target.FacingRight == FacingRight)
                    ActionManage.CurrentAction = new Roll(this, RollAnim, (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                //Otherwise, face the target (if you're not already) and do a horizontal slash
                else ActionManage.CurrentAction = new WeaponAttack(this, HSlashAnim, weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 3, 100, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, false), weapon.GWeaponUseSound, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Perform an Uppercut Swipe, which is done after a roll
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
            {
                ActionManage.CurrentAction = new UppercutSwipe(this, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Perform a Horizontal Slash, which is done after the Jump Slash part of the Jump Slash combo
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset2)
            {
                ActionManage.CurrentAction = new WeaponAttack(this, HSlashAnim, weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, ObjectHeight / 3, 100, 200, Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, false), weapon.GWeaponUseSound, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Perform the initial retreat
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset3)
            {
                int randdistance = new Random().Next(0, 2);

                ActionManage.CurrentAction = new Retreat(this, 1000, 90, randdistance == 0 ? (int)ActionManager.ActionStates.Preset4 : (int)ActionManager.ActionStates.Preset5, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Approach closeby and go on the Defensive
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset4)
            {
                int rand = new Random().Next(0, 2);

                ActionManage.CurrentAction = new Approach(this, new Vector2(50, 0), false, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
            //Approach from far and go on the offensive
            else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset5)
            {
                ActionManage.CurrentAction = new Approach(this, new Vector2(80, 0), false, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
            }
        }

        //The Weapon Wielders should switch back to Movement (for now; maybe a Preset to check for weapons on the floor later) after losing their weapons
        protected override void LoseWeapon()
        {
            ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
        }

        public override bool CheckCanPickUp()
        {
            return (base.CheckCanPickUp() == true && ActionManage.CurState == (int)ActionManager.ActionStates.Movement);
        }

        public override bool CheckShouldPickUp(Item item, Weapon Weapon)
        {
            //Gerald/Kate pick up weapons that they find on the floor if they don't have weapons
            if (weapon == null && Weapon != null)
            {
                return true;
            }

            return false;
        }
    }
}
