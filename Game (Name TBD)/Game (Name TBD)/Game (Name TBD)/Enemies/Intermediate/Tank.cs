﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public class Tank : Enemy
    {
        private NewAnimation BlockAnim;

        //Tank can no longer walk while blocking, so this isn't needed any longer
        //private Animation BlockWalkAnim;
        private NewAnimation ParryAnim;
        private NewAnimation BumpAnim;
        private NewAnimation BodySlamAnim;

        //The maximum damage Tank can receive that doesn't break his block (like Yoshi's knockback threshold for his double jump in the Super Smash Bros. series)
        private const int BlockDamageThreshold = 20;

        //Constructor
        public Tank(String name, int health, int healthbars, int damage, int defense, Status stat, Vector3 location, HUD display, int behavior = 0, int alternate = 0)
        {
            SetLocation(location);
            Velocity = new Vector2(1, 1);
            Weight = 180;

            EnemNum = (int)Enemies.Tank;

            //Animations
            StandAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(72, 19, 48, 77), 300));
            WalkingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 24, 48, 72), 150), new AnimFrame(new Rectangle(132, 25, 73, 71), 150, new Vector2(-6, 0)));
            AttackingAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 198, 48, 78), 75, new Vector2(0, 0)), new AnimFrame(new Rectangle(72, 199, 90, 77), 200, new Vector2(-11, 0)), new AnimFrame(new Rectangle(180, 204, 69, 72), 75, new Vector2(0, 0)));
            HurtAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(156, 119, 59, 61), 700, new Vector2(14, 0)));
            GrabbedAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 112, 46, 68), 300, new Vector2(-4, 0)));
            GrabHitAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(71, 115, 47, 64), 200, new Vector2(-4, 4)));
            KnockedDownAnim = new NewAnimation(false, false, 0, SpriteSheet, new AnimFrame(new Rectangle(228, 117, 58, 62), 0, new Vector2(-3, 1)), new AnimFrame(new Rectangle(300, 143, 66, 37), 400, new Vector2(3, -1)), new AnimFrame(new Rectangle(384, 130, 65, 50), 150, new Vector2(3, 0)), new AnimFrame(new Rectangle(300, 143, 66, 37), 150, new Vector2(3, -1)), new AnimFrame(new Rectangle(384, 130, 65, 50), 150, new Vector2(3, 0)), new AnimFrame(new Rectangle(468, 121, 47, 59), 100, new Vector2(1, 0)));
            BlockAnim = new NewAnimation(GrabbedAnim, 1f);
            ParryAnim = new NewAnimation(GrabHitAnim, 1f);
            BodySlamAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(288, 219, 83, 39), 1, new Vector2(-2, 35)));
            BumpAnim = new NewAnimation(true, false, 0, SpriteSheet, new AnimFrame(new Rectangle(12, 112, 46, 68), 500, new Vector2(-4, 0)));

            CurKnockDownAnim = KnockedDownAnim;

            GainKnockInv = true;

            ObjectLength = 48;

            Name = name;
            SetUpHurtbox();
            SetUpHealth(health);
            SetUpSprites();

            Damage = damage;
            Defense = defense;
            Score = 400;
            HitStun = 500;
            status = stat;
            SpawnStatus = status.GCond;
            if (status != (int)Status.Statuses.None) StatForever = true;
        }

        public static Status[] GetPossibleStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DefenseBoost, 10000f, 20), new Status((int)Status.Statuses.DefenseDown, 11000f, 50) }; }
        }

        public override Status[] PossibleStatuses
        {
            get { return GetPossibleStatuses; }
        }

        //Checks if Tank can block; he must not be grabbed or carried, must be in the Movement or Defensive behavior, must not be in hitstun, must not have been hit on the back half of his hurtbox - or if he was then the hitbox must be facing him, and the total damage he receives cannot exceed his block damage threshold
        private bool CanBlock(Hitbox playerbox, int damage)
        {
            bool hitback = playerbox.GBox.Intersects(BackSide);

            return (Carried == false && Grabbox.GSIsGrabbed == false && IsHit == false && (ActionManage.CurState == (int)ActionManager.ActionStates.Movement || ActionManage.CurState == (int)ActionManager.ActionStates.Defense || ActionManage.CurState == (int)ActionManager.ActionStates.Command) && (hitback == false || (hitback == true && playerbox.HitDirection == FacingRight)) && TotalDamageReceived(damage) <= BlockDamageThreshold);
        }

        //Checks if Tank can parry; he must have low health
        //NOTE: Tank already has the most Defense in the game and may likely have a lot of health, so try to make this a ratio instead (curhealth / totalhealth)
        private bool CanParry()
        {
            return (HealthBars == 0 && Health <= (MaxHealth[HealthBars] / 2));
        }

        //The backside of Tank; if he gets hit only here (meaning even though this overlaps his normal hurtbox for initial collisions, the attack can't hit his normal hurtbox at all during this check) while blocking, it'll break his block
        //NOTE: Right now, if you hit even a bit of Tank's back even if you attacked from the front, it'll break his block; find a way to fix this
        //NOTE: The current fix is to stop the hitbox from breaking Tank's block if its direction isn't the direction Tank is facing...so far so good!
        private Rectangle BackSide
        {
            get { return new Rectangle(ForwardVal(CollisionBox.X, CollisionBox.X + (int)Math.Round(CollisionBox.Width / 2f, MidpointRounding.AwayFromZero)), CollisionBox.Y, CollisionBox.Width / 2, CollisionBox.Height); }
        }

        public override void TakeDamage(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            //Tells if Tank blocked or not; if Tank blocks the attack, we set this to true
            bool Blocked = false;

            //Original hitbox; we'll construct a new hitbox using the current one passed in and tweak its values to avoid Tank from getting knocked down when Tank blocks
            Hitbox newhitbox = hitbox;
            int newdamage = damage;

            //Track Tank's current status so we can negate it if he blocked
            Status prevstatus = status;

            //Tank takes 1/2 damage when blocking an attack - he can even block attacks that should knock him down (changed from before because it'd be too easy to get through his block, which is his defining trait, so it should be strong)
            //However, if an attack is too strong and deals more damage than his block threshold, he will not be able to block it
            //When blocking, Tank is also immune to status effects
            //He also moves back slightly when hit when blocking (like Atlus from Final Fight 2)
            if (CanBlock(newhitbox, newdamage) == true)
            {
                newhitbox = new Hitbox(hitbox);

                //Halve the potential damage that can be done to Tank before taking his Defense or Status into account; this results in the lowest possible value
                newdamage /= 2;

                //Play the "clank" animation when he gets hit while blocking, showing that your attack isn't effective


                Blocked = true;
                newhitbox.SetHitboxType(Hitbox.HitboxTypes.Standard);
                newhitbox.HitSound = LoadSounds.Punch1;
            }

            //Take damage like normal, then see whether Tank blocked the attack or not and take him out of hitstun if so
            base.TakeDamage(objecthitby, newdamage, newhitbox);

            //If Tank blocked the attack and isn't dead, take him out of hitstun and make him block or parry depending on how much health he has remaining
            if (Blocked == true && IsDead == false)
            {
                HitstunRecover();

                //Tank was inflicted with a Status, so reset it
                if (prevstatus != status) ResetStatus();

                //Make Tank able to get grabbed directly after blocking an attack; this allows Players to counter it
                Grabbox.ResetRecovery();

                //Parry (simply counter-attack) the target's attack if Tank can parry
                if (CanParry() == true)
                {
                    Hitbox parryhitbox = CreateHitboxFeet(true, 25, 40, 6, 40, 100, 350, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, true);
                    ActionManage.CurrentAction = new SimpleAttack(this, ParryAnim, parryhitbox);
                }
                //Otherwise just block normally
                else
                {
                    ActionManage.CurrentAction = new Block(this, BlockAnim);

                    //Add dust for Tank sliding
                }

                ActionManage.ChoseAction(Main.GetActiveTime);
            }
        }

        //Make the enemy act according to the its position relative to the player
        protected override void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            //If the target is knocked down, don't do anything
            if (Target.IsDead/*GIsKnockedDown*/ == false)
            {
                //Tank moves around the player
                if (ActionManage.CurState == (int)ActionManager.ActionStates.Movement)
                {
                    //Move towards the player if you're far; move in either the X or Y direction
                    if (TargetProximity() > 150)
                    {
                        //There is a 1/2 chance of Tank approaching vertically or horizontally
                        ActionManage.CurrentAction = (new Random().Next(0, 2) == 0) ? new Approach(this, new Vector2(70, 999), false) : new Approach(this, new Vector2(999, 70), false);
                    }
                    //Otherwise start moving around the player
                    else ActionManage.CurrentAction = new CircleAround(this, new Vector2(70), (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Defense);
                }
                //When on the offensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Offense)
                {
                    //If Tank is really close to the target in the Y direction, attack
                    if (TargetProximityY() <= TrueVelocity.Y)
                    {
                        //If Tank is very close, he'll do a Punch-Bump combo
                        if (TargetProximityX() <= 25f)
                        {
                            //Tank will do a punch first, then lead into a Bump
                            ActionManage.CurrentAction = new SimpleAttack(this, AttackingAnim, CreateHitboxFeet(true, 39, 20, 0, 30, 0, 500, Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, false), (int)ActionManager.ActionStates.Preset1, (int)ActionManager.ActionStates.Defense, (int)ActionManager.ActionStates.Movement);
                        }
                        //If Tank is mid-distance, do a body slam
                        else
                        {
                            //Tank will do a body slam
                            ActionManage.CurrentAction = new JumpAttack(this, BodySlamAnim, new Vector3(ForwardVal(TrueVelocity.X), 0f, OrigJumpVelocity.Z), CreateHitboxFeet(true, CollisionBox.Width, 30, 9, ObjectHeight, 0, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), null, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                        }
                    }
                    //If Tank isn't in range to land an attack, match the target's Y position and go up to the target
                    else
                    {
                        ActionManage.CurrentAction = new Approach(this, new Vector2(new Random().Next(0, 2) == 0 ? 25f : 50f, 0), true, (int)ActionManager.ActionStates.Offense, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                }
                //When on the defensive...
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Defense)
                {
                    //Back away from the target until you're far enough
                    if (TargetProximityX() <= 40)
                    {
                        ActionManage.CurrentAction = new Retreat(this, 2500f, 80, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                    else
                    {
                        ActionManage.CurrentAction = new Idle(this, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                    }
                }
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Preset1)
                {
                    ActionManage.CurrentAction = new SimpleAttack(this, BumpAnim, CreateHitboxFeet(true, 30, 30, 4, 25, 100, 300, Hitbox.HitboxTypes.KnockDown, TowardsDir, LoadSounds.Kick, false), (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement, (int)ActionManager.ActionStates.Movement);
                }
                //Defend Commander
                else if (ActionManage.CurState == (int)ActionManager.ActionStates.Command)
                {
                    Commander.CommandBehavior(this);
                }
            }
            else if (ActionManage.CurrentAction.Animation != GetIdleAnim)
            {
                ActionManage.CurState = (int)ActionManager.ActionStates.Movement;
                ActionManage.CurrentAction = new Idle(this);
            }
        }

        public override void OverrideActions()
        {
            if (IsHit == false && IsKnockedDown == false)
            {
                ClearHitboxes();

                ActionManage.CurrentAction = new Block(this, BlockAnim);
                ActionManage.ChoseAction(Main.GetActiveTime);

                //Add dust for Tank sliding
            }
        }
    }
}
