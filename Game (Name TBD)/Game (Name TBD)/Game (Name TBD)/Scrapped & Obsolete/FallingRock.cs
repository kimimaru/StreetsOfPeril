﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A rock that falls from the ceiling and hits the player - used in Level 5
    //Players can hit the rock to launch it at enemies, unless it is too big
    //Falling rocks with status effects can be used in a Challenge level (obstacle course probably), but I think the ones in the main game won't have any
    public class FallingRock : Hazard
    {
        private Hitbox Hitbox;

        //For effects like getting hit and more
        private EffectsManager Effects;

        //Makes the rock shake a little bit when it's about to fall (an indicator for it falling)
        private int[] Shakes;
        //private int CurShake;

        //private bool GotHit;

        //Tells the rock to start falling when a player approaches
        private bool StartFalling;

        //Constructor - Dust and sprites are passed in as a parameter because there can be different colored dust and different sized/colored rocks depending on the part of the level you're in
        public FallingRock(Texture2D Sprite, Texture2D dust, Vector3 location, Status stat, int hitboxstrength)
        {
            Graphic = Sprite;

            //The little dust that falls from the ceiling, indicating that the rock is going to fall, and also appears once the rock hits the floor
            InteractionGraphic = new Animation(dust, false, 1, 1000);

            Location = location;

            ObjectLength = Graphic.Width;
            ObjectHeight = Graphic.Height;

            SetUpHealth(0);
            SetUpHurtbox();

            Hurtbox = new Hurtbox(Graphic.Width, Graphic.Height);
            //Hitbox = new Hitbox(CollisionBox.X, CollisionBox.Y, Graphic.Width, Graphic.Height, Location.Z + Graphic.Height, Graphic.Height, hitboxstrength, 1, Hitbox.InfiniteHitbox, true, false, LoadSounds.Kick);

            Effects = new EffectsManager();

            StartFalling = false;
            Shakes = new int[] { -1, 0, 1 };
            //CurShake = 1;

            status = stat;

            //GotHit = false;
        }

        //Copy constructor for the AvoidRock bonus stage
        public FallingRock(FallingRock otherrock, float X, float Z, int hitboxstrength, Status stat)
        {
            this.Graphic = otherrock.Graphic;
            this.InteractionGraphic = otherrock.InteractionGraphic;

            this.Location = new Vector3(X, otherrock.Location.Y, Z);
            this.GravityValue = otherrock.GravityValue;

            this.ObjectLength = Graphic.Width;
            this.ObjectHeight = Graphic.Height;

            this.SetUpHealth(0);
            this.SetUpHurtbox();
            
            //this.Hitbox = new Hitbox((int)Location.X, (int)Location.Y, Graphic.Width, Graphic.Height, Location.Z + Graphic.Height, Graphic.Height, hitboxstrength, 0, Hitbox.InfiniteHitbox, true, false, LoadSounds.Kick);

            this.Effects = new EffectsManager();

            this.StartFalling = false;
            this.Shakes = otherrock.Shakes;
            //this.CurShake = 1;

            this.status = new Status(stat.GCond, stat.GStatusDur, stat.GPercentage);

            //this.GotHit = false;
        }

        //public override Hitbox GHitbox
        //{
        //    get { return Hitbox; }
        //}

        //Only for the Avoid Rock bonus stage
        public void ImmediateFall()
        {
            //Maybe make the rocks not knock down? Right now the bonus stage is ridiculously hard
            if (StartFalling == true)
            {
                InteractionGraphic.End();
                Hitbox.SetHitboxType(Hitbox.HitboxTypes.Standard);
            }
        }

        //The falling rock should disappear if it hits the floor and its status is not invincible, or if it's too far off screen
        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    return ((Location.Z == ObjectTile.Z && status == (int)Status.Statuses.None) || (Location.X + OffSet.X < -100 || Location.X + OffSet.X > Main.ScreenSize.X + 100));
        //}

        /*private void TouchWater(float activeTime, Vector2 slowamount, bool into)
        {
            if (into == true)
            {
                if (GravityValue != TileEngine.WaterGravity)
                {
                    GravityValue = TileEngine.WaterGravity;

                    //Soften the impact of the rock's fall upon hitting the water by slowing it down slightly; if the Y velocity will be positive after touching the water, set it to 0
                    if (Velocity.Y <= slowamount.Y) Velocity.Y -= slowamount.Y;
                    else Velocity.Y = 0f;

                    if (Velocity.Y != 0f) Effects.AddAnim(activeTime, new Vector3(Location.X + (Graphic.Width / 2), Location.Y, Location.Z), new Animation(LoadGraphics.Splash, false, 1, 300));
                }
            }
            else
            {
                if (GravityValue != TileEngine.Gravity)
                {
                    GravityValue = TileEngine.Gravity;

                    if (Velocity.Y != 0f) Effects.AddAnim(activeTime, new Vector3(Location.X + (Graphic.Width / 2), Location.Y, Location.Z), new Animation(LoadGraphics.Splash, false, 1, 300));
                }
            }
        }

        //If the rock hits someone on the head (minheight is + or - 20 from their maxheight), create a hitbox that knocks down
        public override void InteractPlayer(float activeTime, Player player, TileEngine TileEngine, List<Collideable> Solids, List<Hazard> HazardList)
        {
            //If a player gets near the rock, make it fall 
            if (StartFalling == false && Math.Abs(player.GLocation.X - Location.X) <= 50 && Math.Abs(player.FeetLoc().Y - (Location.Y + Graphic.Height)) <= 30)
            {
                StartFalling = true;
                InteractionGraphic.Reset(activeTime);
            }

            //Check if the rock's hitbox hit the player; if the rock was hit by a player then another player can still get hit by it
            if (player.CanGetHit(player.FeetLoc(), Hitbox) == true)
            {
                //The rock hit the player on the head, so create a new hitbox that knocks down
                if (Location.Z >= player.MaxHeight - 20 && Location.Z <= player.MaxHeight + 20)
                {
                    Velocity = new Vector2(player.GFacingRight == true ? 2 : -2, 2);
                }
                //The rock hit the player elsewhere, so create a weaker hitbox that doesn't knock down
                else if (Location.Z > ObjectTile.Z)
                {
                    float velocityY = 0f;

                    if (GotHit == false)
                    {
                        Hitbox.Strength -= 2;
                        Hitbox.KnockDown = false;
                        Hitbox.HitSound = LoadSounds.Punch1;
                        velocityY = Velocity.Y + 1;
                    }

                    Velocity = new Vector2(player.GFacingRight == true ? 2 : -2, velocityY);
                }

                //Make the player get hit and ensure that the rock can't get hit by or hit anything else
                player.TakeDamage(activeTime, GotHit == false ? player.GFacingRight : !Hitbox.Direction, Hitbox.Strength, status, Hitbox, TileEngine, Solids);
                SetHitLag(activeTime);
                GotHit = false;
                Hitbox.Reset();
            }
                
            //Check if the player hits the rock if it didn't get hit by anything yet
            if (InteractionGraphic.IsAnimationEnd() == true && Velocity.X == 0f && GotHit == false && status != (int)Status.Statuses.Invincible)
            {
                for (int i = 0; i < player.GHitbox.Count; i++)
                {
                    if (player.GHitbox[i].Throw == false && player.GHitbox[i].Intersect(CollisionBox, Location.Z, Location.Z + Location.W) == true)
                    {
                        player.SetHitLag(activeTime);
                        if (player.GHitbox[i].HitSound != null) LoadSounds.Play(player.GHitbox[i].HitSound);
                        Velocity = new Vector2(player.GFacingRight == true ? 6 : -6, 0f);
                        Hitbox.Direction = Velocity.X > 0f ? true : false;
                        GotHit = true;
                        Effects.AddHitAnim(activeTime, Rectangle.Intersect(player.GHitbox[i].GBox, CollisionBox), Location.Z);
                    }
                }
            }
        }

        public override void InteractEnemy(float activeTime, Enemy enemy, TileEngine TileEngine)
        {
            //Check if the rock's hitbox hit the enemy; if the rock was hit by a player then the enemy can still get hit by it
            if (enemy.CanGetHurt(enemy.FeetLoc(), Hitbox) == true)
            {
                //The rock hit the enemy on the head, so create a new hitbox that knocks down
                if ((Location.Z >= enemy.MaxHeight - 20 && Location.Z <= enemy.MaxHeight + 20))
                {
                    Velocity = new Vector2(enemy.gFacingLeft == false ? 2 : -2, 1);
                }
                //The rock hit the enemy elsewhere, so create a weaker hitbox that doesn't knock down
                else if (Location.Z > ObjectTile.Z)
                {
                    float velocityY = 0f;

                    if (GotHit == false)
                    {
                        Hitbox.Strength -= 2;
                        Hitbox.KnockDown = false;
                        Hitbox.HitSound = LoadSounds.Punch1;
                        velocityY = Velocity.Y + 1;
                    }

                    Velocity = new Vector2(enemy.gFacingLeft == false ? 2 : -2, velocityY);
                }

                //Make the enemy get hit and ensure that the rock can't get hit by or hit anything else
                enemy.GetHurt(activeTime, GotHit == false ? enemy.gFacingLeft : Hitbox.Direction, null, enemy.gLastAttacked, Hitbox.Strength, status, Hitbox, TileEngine);
                SetHitLag(activeTime);
                GotHit = false;
                Hitbox.Reset();
            }
        }

        //Check if a weapon hits a rock
        public override void InteractWeapon(float activeTime, Weapon weapon, TileEngine TileEngine)
        {
            //Check if the player hits the rock if it didn't get hit by anything yet
            if (InteractionGraphic.IsAnimationEnd() == true && Velocity.X == 0f && GotHit == false && status == (int)Status.Statuses.None)
            {
                if (weapon.GHitbox.Intersect(CollisionBox, Location.Z, Location.Z + Graphic.Height) == true)
                {
                    Velocity = new Vector2(weapon.GetFacingDir == false ? 6 : -6, 0f);
                    Hitbox.Direction = Velocity.X > 0f ? true : false;
                    GotHit = true;
                }
            }
        }

        private void MoveX(float velocity)
        {
            Location.X += velocity;
            Hitbox.Follow(velocity, 0, 0);
        }

        private void MoveY(float velocity)
        {
            Location.Y += velocity;
            Hitbox.Follow(0, velocity, 0);
        }

        private void HitWallX()
        {
            if (GotHit == true)
            {
                GotHit = false;
                Velocity = new Vector2((int)-(Velocity.X / 3f), 2);
                Hitbox.Reset();
            }
        }

        private void HitWallY()
        {
            if (GotHit == true)
            {
                GotHit = false;
                Velocity = new Vector2(0f, (int)-(Velocity.Y / 3f));
                Hitbox.Reset();
            }
        }

        private void ObjectFall(float activeTime, TileEngine TileEngine, List<Collideable> Solids)
        {
            Location.Z += Velocity.Y;
            Location.Z = (float)Math.Round(Location.Z, 2);
            Hitbox.Follow(0, 0, Velocity.Y);
            Velocity.Y -= GravityValue;
            Velocity.Y = (float)Math.Round(Velocity.Y, 2);

            //If the rock lands on the floor, make it blink for a bit and disappear
            if (Location.Z <= ObjectTile.Z)
            {
                Location.Z = ObjectTile.Z;
                Hitbox.Reset();
                status = new Status((int)Status.Statuses.Invincible, 1000);
                if (ObjectTile.TypeHeight > 0f && ObjectTile.TypeHeight <= 15f) LoadSounds.Play(LoadSounds.WaterLand);
                else Effects.AddDustAnim(activeTime, new Vector3(Location.X + Graphic.Width / 2, Location.Y + Graphic.Height, Location.Z));
            }
        }

        private void Move(float activeTime, Vector2 velocity, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        {
            //Check moving on surfaces from the X position
            Movement.ObjectMoveX(GetLocationHeight, OffSet, FeetLoc, new Vector3(velocity, Velocity.Y), TileEngine, Solids, MoveX, HitWallX, HitWallX);

            //Check moving on surfaces from the Y position
            Movement.ObjectMoveY(GetLocationHeight, OffSet, FeetLoc, new Vector3(velocity, Velocity.Y), TileEngine, Solids, MoveY, HitWallY, HitWallY);

            if (velocity.X != 0f || velocity.Y != 0f) ObjectTile = TileEngine.CurTile(FeetLoc);

            TouchWater(activeTime, new Vector2(-1, -1), ObjectTile.TouchedWater(Location.Z));

            //Check landing on a tile if the rock is in the air
            if (GotHit == false && InteractionGraphic.IsAnimationEnd() == true && Movement.ShouldObjectFall(Location.Z, ObjectTile, null) == true)
            {
                ObjectFall(activeTime, TileEngine, Solids);
            }
        }

        //Make the rock fall
        public override void Update(float activeTime, SubLevel level)
        {
            //If the rock started falling, initiate its behavior
            if (StartFalling == true)
            {
                //If the little dust animation isn't finished and the rock isn't on the floor yet, make the rock keep falling
                if (InteractionGraphic.IsAnimationEnd() == true)
                {
                    if ((activeTime - PrevHitLag) >= NewCollision.HitLag)
                    {
                        if (Location.Z != ObjectTile.Z)
                            Move(activeTime, new Vector2(Velocity.X, 0f), level.GCameraOffSet, level.GTileEngine, level.GetSolids);

                        Hitbox.Update(activeTime);
                    }
                }
                else
                {
                    CurShake = (CurShake + 1) % Shakes.Length;
                    InteractionGraphic.Update(activeTime);
                    if (InteractionGraphic.IsAnimationEnd() == true)
                        CurShake = 1;
                }
            }

            Effects.Update(activeTime);
            status.ObjectStatUpdate();
        }

        public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        {
            //Draw the shadow only once the rock started falling and is in the air
            //NOTE: The Main.LevelEditor check is there only so I can easily see the height with the level editor - DON'T FORGET TO TAKE IT OUT!!!
            //NOTE: The true check is in there so I can see the height with the new level editor; again, don't forget to take this out
            if ((StartFalling == true || Main.CheckState(Main.GameState.LevelEditor) == true || true) && Location.Z > ObjectTile.Z)
            {
                float ShadowScale = (float)(1f - ((Location.Z - ObjectTile.Z) / 100f));
                if (ShadowScale < .4f || (Location.Z - ObjectTile.Z) > 99) ShadowScale = .4f;

                //Draw a shadow where the rock is going to fall
                spriteBatch.Draw(LoadGraphics.PlayerShadow, new Vector2(Location.X + OffSet.X, FeetLoc().Bottom + OffSet.Y - ObjectTile.Z), null, Color.White, 0f, new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2), ShadowScale, SpriteEffects.None, ObjectTile.TypeHeight <= 0f ? 0.0019f : 0.0001f);
            }

            Effects.Draw(spriteBatch, OffSet);

            //If the rock is invisible when invincible, don't draw it
            if (status.GStatusColor.A != 0)
                Animation.DrawSprite(spriteBatch, Graphic, new Vector2(Location.X + Shakes[CurShake] + OffSet.X, Location.Y - Location.Z + OffSet.Y), false, status.GStatusColor, 0f, GetDrawDepth(OffSet, true));
                //spriteBatch.Draw(Graphic, new Vector2(Location.X + Shakes[CurShake] + OffSet.X, Location.Y - Location.Z + OffSet.Y), null, status.GStatusColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, Location.Y / 1000f);

            if (StartFalling == true && InteractionGraphic.IsAnimationEnd() == false)
                InteractionGraphic.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y - Location.Z + OffSet.Y), false, Color.White, 0f, GetDrawDepth(OffSet, true) - .0001f);

            DebugDraw(spriteBatch, OffSet);
        }

        public override void DebugDraw(SpriteBatch spriteBatch, Vector2 OffSet)
        {
            base.DebugDraw(spriteBatch, OffSet);

            //Draw hitbox
            if (Hitbox != null && Debug.HitboxDraw == true)
                Debug.DrawRectangle(spriteBatch, new Rectangle(Hitbox.GBox.X, Hitbox.GBox.Y - (int)ObjectTile.Z, Hitbox.GBox.Width, Hitbox.GBox.Height), OffSet, Color.White, .998f);
        }*/
    }
}
