﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A personal minecart for enemies - it moves forwards until it reaches the player minecart then goes back until it reaches the end of the screen
    //Used as scenery
    public class Minecart : Hazard
    {
        //Minecart properties

        //The enemy to follow
        private Enemy Enemy;

        //Constructor
        public Minecart(Enemy enemy, float velocity)
        {
            InteractionGraphic = new Animation(LoadGraphics.BagOfJewels, false, 4, 500);
            Velocity = new Vector2(velocity, 0f);
            Hurtbox = new Hurtbox(InteractionGraphic.GetFrameSize());

            Enemy = enemy;

            SetLocation(new Vector3(Enemy.GetLocationHeight.X, Enemy.GetLocationHeight.Y, 0f));
            FacingRight = !Enemy.FacingRight;
        }

        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    return (Location.X < -(TileEngine.TileSize * 3) || Location.X > Main.ScreenSize.X + (TileEngine.TileSize * 3));
        //}

        //public override void Update(float activeTime, SubLevel level)
        //{
        //    //If there is an enemy on the minecart...
        //    if (Enemy != null)
        //    {
        //        //If the enemy is still on the minecart, follow it; otherwise, remove the reference to the enemy and start retreating
        //        if (Enemy.gIsKnockedDown == false && Enemy.gGrabbox.GSIsGrabbed == false && Enemy.gsMinHeight == Location.Z) Location.X = Enemy.gLocation.X;
        //        else
        //        {
        //            Enemy = null;
        //            if (FacingRight == true) Velocity = -Velocity;
        //        }
        //    }
        //    //If the enemy is no longer on the minecart, make the minecart retreat offscreen
        //    else Location.X += Velocity.X;
        //
        //    InteractionGraphic.Update(activeTime);
        //}
        //
        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    InteractionGraphic.Draw(spriteBatch, new Vector2(Location.X, Location.Y), FacingRight, Color.White, Animation.DefaultOrigin(InteractionGraphic.GetFrameSize()), 0f, GetDrawDepth(OffSet) - .0001f);
        //    DebugDraw(spriteBatch, OffSet);
        //}
    }
}
