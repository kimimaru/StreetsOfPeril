﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A gas shooter that poisons players that touch the gas; it can shoot constantly or in increments
    //Only place them against walls in the background
    //Used rarely except for harder Obstacle Course levels
    //NOTE: Maybe this can just be a gas shooter that can give different status effects? It wouldn't be as menacing so this probably won't work well
    //ALSO: It seems a bit illogical for the gas to knock down (not like most games make sense anyway) unless it's coming out at an extremely high velocity (which it isn't); maybe just give the status? I'm not sure...
    //There's also the issue of coming into contact with poison gas when you already have another status effect other than Invincible; should it get rid of it and poison you?
    public class PoisonGasShooter : Hazard
    {
        //Tells if the poison gas is being released or not
        private bool IsShooting;

        private int ShootLength;
        private Hitbox Hitbox;

        //The timer for increasing/decreasing the length of the hitbox and the current length of the hitbox; when it reaches the max length or 0, the shoot/stop timer starts
        private int CurLength;
        //private float PrevLength;

        //How long the poison stays active
        private float ShootTime;
        //private float PrevShoot;

        //How long the poison stays inactive
        private float StopTime;
        //private float PrevStop;

        public PoisonGasShooter(Vector3 location, bool shooting, int shootlength, float shoottime, float stoptime)
        {
            Graphic = LoadGraphics.PoisonShooter;
            InteractionGraphic = new Animation(LoadGraphics.PoisonGas, false, 1, 200);
            SetLocation(location);

            //Hitbox = new Hitbox(0, 0, 0, 0, 0, 0, 0, 0, 0, false, false, LoadSounds.Kick);
            Hurtbox = new Hurtbox(Graphic.Width, Graphic.Height);

            IsShooting = shooting;

            ShootLength = shootlength;

            CurLength = IsShooting == true ? 0 : ShootLength;
            //PrevLength = 0f;

            ShootTime = shoottime;
            //PrevShoot = 0f;

            StopTime = stoptime;
            //PrevStop = 0f;
        }

        //public override Hitbox GHitbox
        //{
        //    get { return Hitbox; }
        //}

        public bool CurrentlyShooting
        {
            get { return IsShooting; }
        }

        public int GetShootLength
        {
            get { return ShootLength; }
        }

        public float GetShootTime
        {
            get { return ShootTime; }
        }

        public float GetStopTime
        {
            get { return StopTime; }
        }

        //The width and height of the hitbox that shoots the gas (technically the length, but the PoisonGasShooter has it reversed since it's facing vertically)
        private int ShootWidthHeight
        {
            get { return 10; }
        }

        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    return (Location.X + OffSet.X < -100);
        //}
        //
        //public override void InteractPlayer(float activeTime, Player player, TileEngine TileEngine, List<Collideable> Solids, List<Hazard> HazardList)
        //{
        //    //Check if the poison gas' hitbox hit the player
        //    if (IsShooting == true && player.CanGetHit(player.GCollisionBox, Hitbox) == true)
        //    {
        //        player.TakeDamage(activeTime, player.GFacingRight, Hitbox.Strength, new Status((int)Status.Statuses.Poison, 3000), Hitbox, TileEngine, Solids);
        //    }
        //}
        //
        //public override void InteractEnemy(float activeTime, Enemy enemy, TileEngine TileEngine)
        //{
        //    //Check if the poison gas' hitbox hit an enemy
        //    if (IsShooting == true && enemy.CanGetHurt(enemy.gCollisionBox, Hitbox) == true)
        //    {
        //        enemy.GetHurt(activeTime, enemy.gFacingLeft, enemy.gTarget, enemy.gLastAttacked, Hitbox.Strength, new Status((int)Status.Statuses.Poison, 4000), Hitbox, TileEngine);
        //    }
        //}

        //public override void Update(float activeTime, SubLevel level)
        //{
        //    //If the shooter is releasing poison, check to see if it should stop
        //    if (IsShooting == true)
        //    {
        //        //Slowly extend the hitbox of the gas shooter until it reaches the max length
        //        //if (CurLength < ShootLength && (activeTime - PrevLength) > 5f)
        //        //{
        //        //    PrevLength = activeTime;
        //        //    CurLength += 2;
        //        //    if (CurLength > ShootLength) CurLength = ShootLength;
        //        //    PrevShoot = activeTime;
        //        //}
        //        
        //        //Hitbox = new Hitbox((int)Location.X + (Graphic.Width / 2) - 5, (int)Location.Y + Graphic.Height, 10, CurLength, (Location.Z + Location.W) - (Graphic.Height/2), 10, 4, -1, 1, activeTime, true, false, LoadSounds.Kick, 0, false);
        //
        //        //Check if it should stop releasing poison; otherwise, create more hitboxes
        //        if (/*CurLength == ShootLength && */(activeTime - PrevShoot) >= ShootTime)
        //        {
        //            IsShooting = false;
        //            PrevLength = activeTime;
        //            PrevStop = activeTime;
        //        }
        //        else Hitbox = new Hitbox((int)Location.X - (ShootWidthHeight / 2), (int)Location.Y, ShootWidthHeight, ShootLength, Location.Z + (ObjectHeight / 2), ShootWidthHeight, 4, -1, 1, true, false, LoadSounds.Kick, 0, false);
        //    }
        //    else
        //    {
        //        //Slowly curtail the hitbox of the gas shooter until it reaches 0
        //        //if (CurLength > 0 && (activeTime - PrevLength) > 5f)
        //        //{
        //        //    PrevLength = activeTime;
        //        //    CurLength -= 2;
        //        //    if (CurLength < 0) CurLength = 0;
        //        //    Hitbox = new Hitbox((int)Location.X + (Graphic.Width / 2) - 5, (int)Location.Y + Graphic.Height, 10, CurLength, (Location.Z + Location.W) - (Graphic.Height / 2), 10, 4, -1, 1, activeTime, true, false, LoadSounds.Kick, 0, false);
        //        //    PrevStop = activeTime;
        //        //}
        //
        //        //Check if it should start releasing poison again
        //        if (/*CurLength == 0 && */(activeTime - PrevStop) >= StopTime)
        //        {
        //            IsShooting = true;
        //            PrevLength = activeTime;
        //            PrevShoot = activeTime;
        //        }
        //    }
        //
        //    InteractionGraphic.Update(activeTime);
        //    Hitbox.Update(activeTime);
        //}
        //
        ////PoisonGasShooters are always supposed to be against walls, so players and enemies cannot be above them in the Y direction
        ////The poison gas is going to be partially transparent because it's smoke, so if it's drawn above the player when the shooter itself isn't drawn above isn't going to look too strange
        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    spriteBatch.Draw(Graphic, new Vector2(Location.X + OffSet.X, Location.Y + OffSet.Y - Location.Z), null, Color.White, 0f, Animation.DefaultOrigin(Graphic), 1f, SpriteEffects.None, GetDrawDepth(OffSet));
        //
        //    if (IsShooting == true)
        //    {
        //        float depth = ((Location.Y + Graphic.Height + ShootLength + OffSet.Y) / 1000f);
        //
        //        InteractionGraphic.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y + ShootLength - Location.Z + OffSet.Y), InteractionGraphic.GetOrigin, new Vector2(1, ShootLength), depth);
        //        if (Location.Z > 0f) InteractionGraphic.Draw(spriteBatch, new Vector2(Location.X + OffSet.X, Location.Y + ShootLength + OffSet.Y), InteractionGraphic.GetOrigin, new Vector2(1, ShootLength), false, new Color(0, 0, 0, 115), ObjectTile.TypeHeight <= 0f ? 0.0019f : 0.0001f/*depth - .0001f*/);
        //    }
        //
        //   DebugDraw(spriteBatch, OffSet);
        //}
        //
        ////For drawing debug information
        //public override void DebugDraw(SpriteBatch spriteBatch, Vector2 OffSet)
        //{
        //    base.DebugDraw(spriteBatch, OffSet);
        //    
        //    if (Debug.HitboxDraw == true)
        //        Hitbox.Draw(spriteBatch, OffSet);
        //}
    }
}
