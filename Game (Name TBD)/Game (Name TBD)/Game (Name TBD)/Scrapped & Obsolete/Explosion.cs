﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A general explosion that can be used for a variety of things, including: objects being destroyed, things blowing up, graphical effect, and more
    //It's classified under Hazards because it can have a hitbox attached to it and cause damage
    public class Explosion : Hazard
    {
        //A hitbox that can make the explosion deal damage; set to null if no damage should be done
        private Hitbox Hitbox;

        public Explosion(Animation ExplosionAnim, Vector3 location, Hitbox hitbox, Status stat, TileEngine.Tile hazardtile)
        {
            InteractionGraphic = ExplosionAnim;
            InteractionGraphic.Reset(Main.GetActiveTime);

            ObjectLength = (int)InteractionGraphic.GetFrameSize().X;
            ObjectHeight = (int)InteractionGraphic.GetFrameSize().Y;

            SetUpHurtbox();

            LoadSounds.Play(LoadSounds.Explosion);

            Location = location;
            ObjectTile = hazardtile;
            Hitbox = hitbox;

            status = stat;
        }

        public Explosion(Animation ExplosionAnim, Vector3 location, Hitbox hitbox, Status stat, TileEngine TileEngine)
        {
            InteractionGraphic = ExplosionAnim;
            InteractionGraphic.Reset(Main.GetActiveTime);

            ObjectLength = (int)InteractionGraphic.GetFrameSize().X;
            ObjectHeight = (int)InteractionGraphic.GetFrameSize().Y;

            SetUpHurtbox();

            LoadSounds.Play(LoadSounds.Explosion);

            Location = location;

            ObjectTile = TileEngine.CurTile(CollisionBox);
            Hitbox = hitbox;

            status = stat;
        }

        //public override Hitbox GHitbox
        //{
        //    get { return Hitbox; }
        //}

        //The explosion is finished when its animation is finished
        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    return Finished();
        //}

        public bool Finished()
        {
            return InteractionGraphic.IsAnimationEnd();
        }

        //public override void InteractPlayer(float activeTime, Player player, TileEngine TileEngine, List<Collideable> Solids, List<Hazard> HazardList)
        //{
        //    //Check if hitbox hit the player, provided it isn't null
        //    if (Hitbox != null && player.CanGetHit(player.GCollisionBox, Hitbox) == true)
        //    {
        //        //Make the player get hit from the explosion
        //        player.TakeDamage(activeTime, player.GFacingRight, Hitbox.Strength, status, Hitbox, TileEngine, Solids);
        //        InteractionGraphic.DelayHit();
        //    }
        //}
        //
        //public override void InteractEnemy(float activeTime, Enemy enemy, TileEngine TileEngine)
        //{
        //    //Check if hitbox hit the enemy, provided it isn't null
        //    if (Hitbox != null && enemy.CanGetHurt(enemy.gCollisionBox, Hitbox) == true)
        //    {
        //        //Make the enemy get hit from the explosion
        //        enemy.GetHurt(activeTime, enemy.gFacingLeft, enemy.gTarget, enemy.gLastAttacked, Hitbox.Strength, status, Hitbox, TileEngine);
        //        InteractionGraphic.DelayHit();
        //    }
        //}
        //
        ////If a weapon is thrown and hits the explosion, it should fall back to the ground (Might not be possible to easily check since weapons don't have hurtboxes)
        //public override void InteractWeapon(float activeTime, Weapon weapon, TileEngine TileEngine)
        //{
        //    
        //}

        //public override void Update(float activeTime, SubLevel level)
        //{
        //    InteractionGraphic.Update(activeTime);
        //
        //    if (Hitbox != null)
        //        Hitbox.Update(activeTime);
        //}
        //
        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    //Draw the shadow if the explosion is in the air
        //    if (Location.Z > ObjectTile.Z)
        //    {
        //        float ShadowScale = (float)(1f - ((Location.Z - ObjectTile.Z) / 100f));
        //        if (ShadowScale < .4f || (Location.Z - ObjectTile.Z) > 99) ShadowScale = .4f;
        //
        //        spriteBatch.Draw(LoadGraphics.PlayerShadow, new Vector2(CollisionBox.Center.X + OffSet.X, FeetLoc.Bottom + OffSet.Y - ObjectTile.Z), null, Color.White, 0f, new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2), ShadowScale, SpriteEffects.None, ObjectTile.TypeHeight <= 0f ? 0.0019f : (Location.Y / 1000f - .0001f));
        //    }
        //
        //    InteractionGraphic.Draw(spriteBatch, new Vector2(Location.X, Location.Y - Location.Z) + OffSet, false, status.GStatusColor, 0f, GetDrawDepth(OffSet) - .0001f);
        //
        //    DebugDraw(spriteBatch, OffSet);
        //}
        //
        //public override void DebugDraw(SpriteBatch spriteBatch, Vector2 OffSet)
        //{
        //    base.DebugDraw(spriteBatch, OffSet);
        //
        //    //Draw hitbox
        //    if (Hitbox != null && Debug.HitboxDraw == true) Hitbox.Draw(spriteBatch, OffSet);
        //}
    }
}
