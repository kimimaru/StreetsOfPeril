﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //An interface for all solid objects to implement - it provides methods for checking if a moving object touched, jumped under, or jumped on top of the solid object
    //NOTE: Maybe make a get hurt function for all solids, allowing for more destructable objects!!
    public interface Collideable
    {
        //Collision box of the collideable object
        Rectangle CollideBox
        {
            get;
        }

        //The height of the object
        float MaxHeight();

        //Check if a moving object touched this solid one from the X direction
        bool TouchedX(Vector4 playerloc, Vector2 playervelocity, Rectangle playerrec);

        //Check if a moving object touched this solid one from the Y direction
        bool TouchedY(Vector4 playerloc, Vector2 playervelocity, Rectangle playerrec);

        //Check if a moving object is jumping from under this solid one
        bool JumpedUnder(Vector4 playerloc, float playervelocityZ, Rectangle playerrec);

        //Check if a moving object is jumping on this solid one
        bool JumpedOnTop(Vector4 playerloc, float playervelocityZ, Rectangle playerrec);

        //Check if a moving object is on this solid one
        bool IsOn(Vector4 playerloc, Rectangle playerrec);

        //void Update(float activeTime);
    }
}
