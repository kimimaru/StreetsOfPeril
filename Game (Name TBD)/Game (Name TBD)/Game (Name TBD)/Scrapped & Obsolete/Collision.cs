﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //This class handles collisions between players, enemies, items, etc. and does screen bounds checking
    public static class Collision
    {
        //The amount of hitlag objects experience when hitting something
        public const float HitLag = 100f;

        //The number of pixels in height equating to each real-world foot in height
        private const int PixelPerFoot = 14;

        //The number of inches in a foot; used for getting the in-game height of an object
        private const int InchesInFoot = 12;

        //Gets the object's in-game height when given its real world height: Graham is a reference point; at 5'8, this translates to 79 pixels in height, so 14 pixels for each foot
        //The height in this case would be passed in as 5.8
        /*public static int GetInGameHeight(float realworldheight)
        {
            //Round the height to one decimal place (floats are inaccurate)
            float gameheight = (float)Math.Round(realworldheight, 1);

            //Get the difference in inches
            int inches = (int)((gameheight - (int)gameheight) * 10);

            //Get how many pixels the inches alone equate to
            float difference = (inches / (float)InchesInFoot) * PixelPerFoot;

            //The height, taking only how tall the character is in feet
            int flatheight = (int)gameheight * PixelPerFoot;

            //The character's total height, with inches added
            int totalgameheight = flatheight + (int)difference;

            return totalgameheight;
        }*/

        /*public static void Collide(float activeTime, SubLevel level)
        {
            //Collisions for players and enemies
            PlayerEnemyCollisions(activeTime, level);

            //Collisions among players
            PlayerPlayerCollisions(activeTime, level);

            //Collisions for players and objects (picking up)
            PlayerObjectCollisions(activeTime, level);
            
            //Collisions for enemies and objects
            EnemyObjectCollisions(activeTime, level);

            //Collisions for weapons hitting enemies and other stuff
            WeaponObjectCollisions(activeTime, level);
        }*/

        /*public static void PlayerEnemyCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<Enemy> enemlist = level.GetEnemies;
            Vector2 OffSet = level.GCameraOffSet;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            int[][] playerhits = SetUpTradeValues(Players.Count, enemlist.Count);
            bool[][] enemyhits = new bool[enemlist.Count][];
            for (int i = 0; i < enemyhits.Length; i++) enemyhits[i] = new bool[Players.Count];

            //Enemy and player damage collisions
            for (int p = 0; p < Players.Count; p++)
            {
                for (int i = 0; i < enemlist.Count; i++)
                {
                    //Check if player grabs enemy
                    if (enemlist[i].CanGetGrabbed(TileEngine) == true && Players[p].CheckCanGrab(enemlist[i], level) == true)
                    {
                        //If you grab an enemy, reset the previous HUD so it doesn't show up so you can draw the new one
                        HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                        //Make the player grab the enemy, and make the enemy get grabbed
                        Players[p].GrabEnemy(enemlist[i], level);
                        enemlist[i].GetGrabbed(activeTime, Players[p].GetFacingDir, Players[p], Players[p], Players[p].FeetLoc, Players[p].CurHeight, level);
                    }

                    //Check if Wil's gun bullets hit enemies
                    if (Players[p].GBullets != null)
                    {
                        for (int j = 0; j < Players[p].GBullets.Count; j++)
                        {
                            //Check if Wil's bullets hit enemy tanks
                            if (enemlist[i].GetOxygenTank != null && enemlist[i].GetOxygenTank.CanBreak(activeTime, Players[p].GBullets[j].GHitbox) == true)
                            {
                                //If Wil's bullets hit an enemy's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                                //Make the enemy's tank take damage
                                enemlist[i].GetOxygenTank.Break(activeTime, 1, Players[p].GBullets[j].GHitbox, TileEngine);

                                //Make the enemy unable to get hurt by the hitbox that hit the tank
                                enemlist[i].GetHurtbox.SetHitBox(activeTime, Players[p].GBullets[j].GHitbox);

                                //Make Wil lose Health or a free bullet if the bullet hit the tank
                                Players[p].SpecialAttackHit(activeTime, j);

                                //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                                Players[p].GBullets[j].Intersect();
                            }

                            //Check if Wil's bullets hit enemies
                            if (enemlist[i].CanGetHurt(enemlist[i].CollisionBox, Players[p].GBullets[j].GHitbox) == true)
                            {
                                //If Wil's bullet hits an enemy, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                                Status newstat = new Status();
                                if (Players[p].GBullets[j].InflictsStatus == true && (Players[p].GetStatus != (int)Status.Statuses.Invincible && Players[p].GetStatus != (int)Status.Statuses.Rainbow && Players[p].GetStatus != (int)Status.Statuses.Poison))
                                    newstat = new Status(Players[p].GetStatus.GCond, Players[p].GetStatus.GStatusDur, Players[p].GetStatus.GPercentage);

                                //Make the enemy take damage
                                enemlist[i].GetHurt(activeTime, Players[p].GBullets[j].GHitbox.Direction, Players[p], Players[p], Players[p].GetDamage + Players[p].GBullets[j].GHitbox.Strength, newstat, Players[p].GBullets[j].GHitbox, TileEngine);

                                //Give the player points for attacking enemies
                                Players[p].AddScore(Players[p].GBullets[j].GHitbox.Score);

                                //Make Wil lose Health or a free bullet if the bullet hit the enemy
                                Players[p].SpecialAttackHit(activeTime, j);

                                //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                                //Maybe Wil's can go through enemies?
                                Players[p].GBullets[j].Intersect();
                            }
                        }
                    }

                    //Check if enemy projectiles hits player
                    if (enemlist[i].gProjectiles != null)
                    {
                        for (int j = 0; j < enemlist[i].gProjectiles.Count; j++)
                        {
                            //Check if enemy projectiles hit the player tanks
                            if (Players[p].GetOxygenTank != null && Players[p].GetOxygenTank.CanBreak(activeTime, enemlist[i].gProjectiles[j].GHitbox) == true)
                            {
                                //If an enemy's projectile hits a player's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                                Players[p].GetOxygenTank.Break(activeTime, 1, enemlist[i].gProjectiles[j].GHitbox, TileEngine);

                                //Make the player unable to get hurt by the hitbox that hit the tank
                                Players[p].GetHurtbox.SetHitBox(activeTime, enemlist[i].gProjectiles[j].GHitbox);
                            }

                            //Check if enemy projectiles hit players
                            if (Players[p].CanGetHit(Players[p].CollisionBox, enemlist[i].gProjectiles[j].GHitbox) == true)
                            {
                                //If an enemy's projectile hits the player, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                                Status newstat = new Status();
                                if (enemlist[i].gProjectiles[j].GetStatus != (int)Status.Statuses.Invincible)
                                    newstat = new Status(enemlist[i].gProjectiles[j].GetStatus.GCond, enemlist[i].gProjectiles[j].GetStatus.GStatusDur, enemlist[i].gProjectiles[j].GetStatus.GPercentage);

                                Players[p].TakeDamage(activeTime, !enemlist[i].gProjectiles[j].GHitbox.Direction, enemlist[i].gProjectiles[j].GHitbox.Strength, newstat, enemlist[i].gProjectiles[j].GHitbox, TileEngine, Solids);

                                //Make the projectile do something when it hits (Ex. bullet bouncing off or disappearing)
                                enemlist[i].gProjectiles[j].Intersect();
                            }
                        }
                    }

                    //Check if player hits enemy
                    for (int h = 0; h < Players[p].GetHitboxes.Count; h++)
                    {
                        //Make the enemy's tank take damage if the player hit it
                        if (enemlist[i].GetOxygenTank != null && enemlist[i].GetOxygenTank.CanBreak(activeTime, Players[p].GetHitboxes[h]) == true)
                        {
                            //If you hit an enemy's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                            HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                            enemlist[i].GetOxygenTank.Break(activeTime, 1, Players[p].GetHitboxes[h], TileEngine);

                            //Make the enemy unable to get hurt by the hitbox that hit the tank
                            enemlist[i].GetHurtbox.SetHitBox(activeTime, Players[p].GetHitboxes[h]);

                            //Make the player experience hitlag for hitting the enemy's tank
                            Players[p].EnterHitLag();
                        }

                        if (enemlist[i].CanGetHurt(enemlist[i].CollisionBox, Players[p].GetHitboxes[h]) == true)
                        {
                            playerhits[p][i] = h;
                            break;
                        }
                    }

                    //Check if enemy hits another enemy (specific attacks only, like throws)
                    if (enemlist.Count > 1)
                    {
                        for (int j = 0; j < enemlist.Count; j++)
                        {
                            if (enemlist[i].gIsKnockedDown == false) break;

                            //Make sure the enemy checking to be hit isn't the same as the one with the hitbox
                            if (j != i && enemlist[j].CanGetHurt(enemlist[j].CollisionBox, enemlist[i].gHitbox) == true)
                            {
                                enemlist[j].GetHurt(activeTime, enemlist[i].gHitbox.Direction, null, enemlist[i].gLastAttacked, enemlist[i].TotalDamageDealt(enemlist[i].gHitbox), new Status(), enemlist[i].gHitbox, TileEngine);

                                //If an enemy hits another enemy, reset the previous HUD, if it belonged to a player, so it doesn't show up so you can draw the new one
                                if (enemlist[i].gLastAttacked != null)
                                    HUD.ChangeHUD(activeTime, enemlist[i].gLastAttacked.GPlayerNum, enemlist[j].GetHUD);

                                //Make the enemy experience hitlag for hitting the other enemy
                                enemlist[i].SetHitLag(activeTime);
                            }
                        }
                    }

                    //Check if the enemy hits the player's tank
                    if (Players[p].GetOxygenTank != null && Players[p].GetOxygenTank.CanBreak(activeTime, enemlist[i].gHitbox) == true)
                    {
                        //If an enemy hits a player's oxygen tank, reset the previous HUD so it doesn't show up so you can draw the new one
                        HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                        Players[p].GetOxygenTank.Break(activeTime, 1, enemlist[i].gHitbox, TileEngine);

                        //Make the player unable to get hurt by the hitbox that hit the tank
                        Players[p].GetHurtbox.SetHitBox(activeTime, enemlist[i].gHitbox);

                        //Make the enemy experience hitlag for hitting the player's tank
                        enemlist[i].SetHitLag(activeTime);
                    }

                    //Check if enemy hits player
                    if (Players[p].CanGetHit(Players[p].CollisionBox, enemlist[i].gHitbox) == true)
                    {
                        enemyhits[i][p] = true;
                    }
                }
            }

            //Store a copy of each player and enemy's hitboxes because they will lose their own if they get hurt
            List<Hitbox>[] hitboxlist = new List<Hitbox>[Players.Count];
            List<Hitbox> enemhitboxes = new List<Hitbox>();
            for (int i = 0; i < Players.Count; i++)
            {
                hitboxlist[i] = new List<Hitbox>();
                hitboxlist[i].AddRange(Players[i].GetHitboxes);
            }
            
            for (int i = 0; i < enemlist.Count; i++) enemhitboxes.Add(enemlist[i].gHitbox);

            //Implement all the player-enemy collisions at once, allowing hits to trade
            for (int p = 0; p < playerhits.Length; p++)
            {
                for (int i = 0; i < playerhits[p].Length; i++)
                {
                    //Check if player hit the enemy
                    if (hitboxlist[p].Count > 0 && playerhits[p][i] != -1 && enemlist[i].CanGetHurt(enemlist[i].CollisionBox, hitboxlist[p][playerhits[p][i]]) == true)
                    {
                        //If you hit an enemy, reset the previous HUD so it doesn't show up so you can draw the new one
                        HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);

                        //Check if an enemy is grabbed by a character (or carried by Jeff) and do the appropriate action depending on the state of the character
                        if (enemlist[i] == Players[p].GEnem)
                        {
                            //Perform a throw or throw-like move on the enemy
                            if (hitboxlist[p][playerhits[p][i]].Throw == true) Players[p].ThrowEnemy(activeTime, Players[p].TotalDamageDealt(hitboxlist[p][playerhits[p][i]]), hitboxlist[p][playerhits[p][i]], TileEngine);
                            //Get hit by a grab attack
                            else enemlist[i].GetHurt(activeTime, hitboxlist[p][playerhits[p][i]].Direction, Players[p], Players[p], Players[p].TotalDamageDealt(hitboxlist[p][playerhits[p][i]]), new Status(), hitboxlist[p][playerhits[p][i]], TileEngine);

                            //Make the player experience hitlag for hitting the enemy
                            Players[p].EnterHitLag();

                            //Give the player points for hitting enemies
                            Players[p].AddScore(hitboxlist[p][playerhits[p][i]].Score);
                        }
                        else if (hitboxlist[p][playerhits[p][i]].Throw == false)
                        {
                            enemlist[i].GetHurt(activeTime, hitboxlist[p][playerhits[p][i]].Direction, Players[p], Players[p], Players[p].TotalDamageDealt(hitboxlist[p][playerhits[p][i]]), new Status(), hitboxlist[p][playerhits[p][i]], TileEngine);

                            //Make sure it doesn't increase the hit counter if you're hitting with something other than standard attacks
                            Players[p].SetNextAttack();

                            //Make the player experience hitlag for hitting the enemy
                            Players[p].EnterHitLag();

                            //Give the player points for hitting enemies
                            Players[p].AddScore(hitboxlist[p][playerhits[p][i]].Score);
                        }
                    }

                    //Check if the enemy hit the player
                    if (enemyhits[i][p] == true && Players[p].CanGetHit(Players[p].CollisionBox, enemhitboxes[i]) == true)
                    {
                        //Throw the player
                        if (enemhitboxes[i].Throw == true)
                        {
                            enemlist[i].ThrowPlayer(activeTime, Players[p], level);
                        }
                        //Hit the player with a normal attack
                        else
                        {
                            Status newstat = new Status();
                            if (enemlist[i].gIsKnockedDown == false && enemlist[i].GetStatus != (int)Status.Statuses.Invincible)
                                newstat = new Status(enemlist[i].GetStatus.GCond, enemlist[i].GetStatus.GStatusDur, enemlist[i].GetStatus.GPercentage);

                            Players[p].TakeDamage(activeTime, enemlist[i].gIsKnockedDown == false ? enemhitboxes[i].Direction : !enemhitboxes[i].Direction, enemlist[i].TotalDamageDealt(enemhitboxes[i]), newstat, enemhitboxes[i], TileEngine, Solids);
                        }

                        //Make the enemy experience hitlag for hitting the player
                        enemlist[i].SetHitLag(activeTime);

                        //If an enemy hits the player, reset the previous HUD so it doesn't show up so you can draw the new one
                        HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, enemlist[i].GetHUD);
                    }
                }
            }
        }*/

        //For collisions among players
        /*public static void PlayerPlayerCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            //You shouldn't be able to hit other players in bonus stages (unless there's a bonus stage that involves them fighting each other)
            if (Players.Count > 1 || Main.CheckState(Main.GameState.InBonus) == false)
            {
                int[][] hits = SetUpTradeValues(Players.Count, Players.Count);

                for (int i = 0; i < Players.Count; i++)
                {
                    for (int j = 0; j < Players.Count; j++)
                    {
                        //Make sure the player doesn't hurt itself or try to give itself a weapon
                        if (i == j) continue;

                        //Players shouldn't be able to hurt each other if the option to damage each other is disabled (unless a player is thrown)
                        if (Main.PlayerDamage == true || Players[i].GIsKnockedDown == true)
                        {
                            if (Main.PlayerDamage == true)
                            {
                                //Check if Wil's bullets hit other players
                                if (Players[i].GBullets != null)
                                {
                                    for (int g = 0; g < Players[i].GBullets.Count; g++)
                                    {
                                        //Check if Wil's bullets hit another player's oxygen tank
                                        if (Players[j].GetOxygenTank != null && Players[j].GetOxygenTank.CanBreak(activeTime, Players[i].GBullets[g].GHitbox) == true)
                                        {
                                            HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                                            HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                                            Players[j].GetOxygenTank.Break(activeTime, 1, Players[i].GBullets[g].GHitbox, TileEngine);

                                            //Make the player unable to get hurt by the hitbox that hit the tank
                                            Players[j].GetHurtbox.SetHitBox(activeTime, Players[i].GBullets[g].GHitbox);
                                        }

                                        //Check if Wil's bullets hit another player
                                        if (Players[j].CanGetHit(Players[j].CollisionBox, Players[i].GBullets[g].GHitbox) == true)
                                        {
                                            //If a player hits another player, display each player's limited HUD (Health, Name, and Portrait) under their own respective ones
                                            HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                                            HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                                            int totaldamage = (Players[i].GetDamage + Players[i].GBullets[g].GHitbox.Strength) / 2;

                                            //Make the player take damage
                                            Players[j].TakeDamage(activeTime, !Players[i].GBullets[g].GHitbox.Direction, totaldamage, Players[i].GBullets[g].GetStatus, Players[i].GBullets[g].GHitbox, TileEngine, Solids);

                                            //Make Wil lose Health or a free bullet if the bullet hit
                                            Players[i].SpecialAttackHit(activeTime, g);

                                            //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                                            Players[i].GBullets[g].Intersect();
                                        }
                                    }
                                }
                            }

                            for (int h = 0; h < Players[i].GetHitboxes.Count; h++)
                            {
                                //Check if a player hit another player's oxygen tank
                                if (Players[j].GetOxygenTank != null && Players[j].GetOxygenTank.CanBreak(activeTime, Players[i].GetHitboxes[h]) == true)
                                {
                                    HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                                    HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                                    Players[j].GetOxygenTank.Break(activeTime, 1, Players[i].GetHitboxes[h], TileEngine);

                                    //Make the player unable to get hurt by the hitbox that hit the tank
                                    Players[j].GetHurtbox.SetHitBox(activeTime, Players[i].GetHitboxes[h]);

                                    //Make the player experience hitlag for hitting the other player's tank
                                    Players[i].EnterHitLag();
                                }

                                if (Players[j].CanGetHit(Players[j].CollisionBox, Players[i].GetHitboxes[h]) == true)
                                {
                                    hits[i][j] = h;
                                    break;
                                }
                            }
                        }

                        //(SCRAPPED) If one player has a weapon and the other doesn't, and both players are standing near each other and are able to pick up items, let the player with the weapon give it to the player without one
                        //If both players have weapons, let them swap
                        //if (Players[i].GSWeapon != null && Players[i].GCollisionBox.Intersects(Players[j].GCollisionBox) && Players[i].CheckCanPickUp() == true && Players[j].CheckCanPickUp() == true)
                        //{
                        //    Weapon receivingweapon = Players[j].GSWeapon;
                        //    Players[i].GSWeapon.HandOff(activeTime, Players[j], j);
                        //    if (receivingweapon != null) receivingweapon.HandOff(activeTime, Players[i], i);
                        //    else Players[i].GSWeapon = null;
                        //}
                    }
                }

                //Players shouldn't be able to hurt each other if the option to damage each other is disabled
                if (hits != null)
                {
                    //Store a copy of each player's hitboxes because they will lose their own if they get hurt
                    List<Hitbox>[] hitboxlist = new List<Hitbox>[Players.Count];
                    for (int i = 0; i < Players.Count; i++)
                    {
                        hitboxlist[i] = new List<Hitbox>();
                        hitboxlist[i].AddRange(Players[i].GetHitboxes);
                    }

                    //Implement all the player collisions at once, allowing hits to trade
                    for (int i = 0; i < hits.Length; i++)
                    {
                        for (int j = 0; j < hits[i].Length; j++)
                        {
                            if (hitboxlist[i].Count > 0 && hits[i][j] != -1 && Players[j].CanGetHit(Players[j].CollisionBox, hitboxlist[i][hits[i][j]]) == true)
                            {
                                //If a player hits another player, display each player's limited HUD (Health, Name, and Portrait) under their own respective ones
                                HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                                HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                                //Make sure it doesn't increase the hit counter if you're hitting with something other than standard attacks
                                Players[i].SetNextAttack();

                                //Make the player experience hitlag for hitting the other player
                                Players[i].EnterHitLag();

                                //Players do half damage to each other (adjust later if needed)
                                Players[j].TakeDamage(activeTime, !hitboxlist[i][hits[i][j]].Direction, Players[i].TotalDamageDealt(hitboxlist[i][hits[i][j]]) / 2, new Status(), hitboxlist[i][hits[i][j]], TileEngine, Solids);
                            }
                        }
                    }
                }
            }
        }

        public static void PlayerObjectCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<ItemContainer> containerlist = level.GetContainers;
            List<Item> itemlist = level.GetItems;
            List<Weapon> weaponlist = level.GetWeapons;
            List<Hazard> hazards = level.GetHazards;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            //Container collisions
            PlayerContCollisions(activeTime, level);

            //Item collisions
            PlayerItemCollisions(activeTime, level);

            //Can't implement standard Weapon collisions here - conflicts with IsPickedUp
            if (weaponlist != null)
            {
                for (int p = 0; p < Players.Count; p++)
                {
                    for (int i = 0; i < weaponlist.Count; i++)
                    {
                        if (Players[p].CheckCanPickUp() == true && weaponlist[i].CanBePickedUp(Players[p]) == true)
                        {
                            //If you pick up a weapon, reset the previous HUD so it doesn't show up so you can draw the new one
                            HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, weaponlist[i].GetHUD);

                            //Make the player pick up the weapon
                            weaponlist[i].GetPickedUp(Players[p], level);
                        }
                    }
                }
            }

            //Hazard Collisions
            //if (hazards != null)
            //{
            //    for (int p = 0; p < Players.Count; p++)
            //    {   
            //        for (int i = 0; i < hazards.Count; i++)
            //        {
            //            //Do hazard stuff (water slows, fire hurts, etc.)
            //            //The check for colliding is inside so each one can be more diverse
            //            hazards[i].InteractPlayer(activeTime, Players[p], TileEngine, Solids, hazards);
            //
            //            //If the hazard is a Scarecrow, check to see if the weapon it drops should be spawned
            //            Scarecrow scarecrow = hazards[i] as Scarecrow;
            //            if (scarecrow != null && scarecrow.GSSpawned == false && scarecrow.HitLimit() == true)
            //            {
            //                int listcount = weaponlist.Count;
            //
            //                //Spawn the item and set its height and hitboxes
            //                weaponlist.Add(new Weapon("Strong Weapon", 500f, 55, scarecrow.WeaponSpawnLoc()new Vector2(Players[p].XYLoc.X + (Players[p].FeetLoc().Width / 2), Players[p].FeetLoc().Y), new Vector2(3), 15000f, new Status(), (int)Weapon.WeaponTypes.Swing, LoadSounds.SharpSwing, LoadSounds.SharpHit, true));
            //                weaponlist[listcount].SetLocation(new Vector3(weaponlist[listcount].GetLocation.X, weaponlist[listcount].GetLocation.Y, 100f));
            //                weaponlist[listcount].SetHitbox(activeTime, new Vector2(weaponlist[listcount].CollisionBox.X, weaponlist[listcount].CollisionBox.Y), 0, -1f, null);
            //                scarecrow.GSSpawned = true;
            //            }
            //        }
            //    }
            //}
        }

        public static void PlayerContCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<ItemContainer> containerlist = level.GetContainers;
            List<Item> itemlist = level.GetItems;
            List<Weapon> weaponlist = level.GetWeapons;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            if (containerlist != null)
            {
                for (int p = 0; p < Players.Count; p++)
                {
                    //Container collisions
                    for (int i = 0; i < containerlist.Count; i++)
                    {
                        //Check if container was broken
                        for (int h = 0; h < Players[p].GetHitboxes.Count; h++)
                        {
                            if (Players[p].GetHitboxes[h].Throw == false && containerlist[i].CanBreak(Players[p].GetHitboxes[h]) == true)
                            {
                                //If you hit a container, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, containerlist[i].GetHUD);

                                //Make the container take damage
                                containerlist[i].Break(activeTime, Players[p].TotalDamageDealt(Players[p].GetHitboxes[h]), Players[p].GetHitboxes[h], itemlist, weaponlist, TileEngine, Solids);

                                Players[p].EnterHitLag();
                            }
                        }

                        //Check if Wil's bullets hit item containers
                        if (Players[p].GBullets != null)
                        {
                            for (int j = 0; j < Players[p].GBullets.Count; j++)
                            {
                                if (containerlist[i].CanBreak(Players[p].GBullets[j].GHitbox) == true)
                                {
                                    //If you hit a container, reset the previous HUD so it doesn't show up so you can draw the new one
                                    HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, containerlist[i].GetHUD);

                                    //Make the container take damage
                                    containerlist[i].Break(activeTime, Players[p].GetDamage + Players[p].GBullets[j].GHitbox.Strength, Players[p].GBullets[j].GHitbox, itemlist, weaponlist, TileEngine, Solids);

                                    //Make Wil lose Health or a free bullet if the bullet hit the enemy
                                    Players[p].SpecialAttackHit(activeTime, j);

                                    //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                                    Players[p].GBullets[j].Intersect();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void PlayerItemCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<Item> itemlist = level.GetItems;
            TileEngine TileEngine = level.GetTileEngine;

            if (itemlist != null)
            {
                for (int p = 0; p < Players.Count; p++)
                {
                    for (int i = 0; i < itemlist.Count; i++)
                    {
                        //Check if player or enemy picked up item and give stats
                        //This uses the player's Location rather than hurtbox - fixes the problem with small hurtboxes making you unable to pick up items at your feet
                        if (itemlist[i].CanBePickedUp(Players[p]) == true)
                        {
                            //Show how much health an item restores when the player stands over it, if it heals anything
                            if (Players[p].IsDead == false)// && Players[p].GTempBar <= 0f)
                                Players[p].GetHUD.SetHealIndicator(activeTime, Players[p].GetHealth, Players[p].CurMaxHealth, itemlist[i].GetHealth);

                            if (Players[p].CheckCanPickUp() == true)
                            {
                                //If you pick up an item, reset the previous HUD so it doesn't show up so you can draw the new one
                                HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, itemlist[i].GetHUD);

                                //Check if the player has an active healing over time item that needs to be overridden by the new item
                                Item.HealOverride(itemlist[i], Players[p], null, itemlist);

                                Players[p].PickUp(itemlist[i], null, level);
                            }
                        }
                    }
                }
            }
        }

        public static void EnemyObjectCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<Enemy> enemlist = level.GetEnemies;
            List<ItemContainer> containerlist = level.GetContainers;
            List<Item> itemlist = level.GetItems;
            List<Weapon> weaponlist = level.GetWeapons;
            List<Hazard> hazards = level.GetHazards;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            //Container collisions when enemies are thrown into them (or able to attack other things another way)
            for (int i = 0; i < enemlist.Count; i++)
            {
                //If an enemy isn't knocked down, it can't destroy containers
                if (enemlist[i].gIsKnockedDown == true)
                {
                    for (int j = 0; j < containerlist.Count; j++)
                    {
                        if (containerlist[j].CanBreak(enemlist[i].gHitbox))
                        {
                            //If the container gets hit, reset the previous HUD, if it belonged to a player, so it doesn't show up so you can draw the new one
                            if (enemlist[i].gLastAttacked != null)
                                HUD.ChangeHUD(activeTime, enemlist[i].gLastAttacked.GPlayerNum, containerlist[j].GetHUD);

                            //Make the container take damage
                            containerlist[j].Break(activeTime, enemlist[i].GetDamage + enemlist[i].gHitbox.Strength, enemlist[i].gHitbox, itemlist, weaponlist, TileEngine, Solids);

                            //Make the enemy experience hitlag from hitting the container
                            enemlist[i].SetHitLag(activeTime);
                        }
                    }
                }
            }

            //Item collisions - the enemy contemplates picking up the item if it's near one
            for (int i = 0; i < enemlist.Count; i++)
            {
                for (int j = 0; j < itemlist.Count; j++)
                {
                    if (itemlist[j].CanBePickedUp(enemlist[i]) == true)
                    {
                        if (enemlist[i].CheckCanPickUp() == true && enemlist[i].CheckShouldPickUp(itemlist[j], null) == true)
                        {
                            //Check if the enemy has an active healing over time item that needs to be overridden by the new item
                            Item.HealOverride(itemlist[j], null, enemlist[i], itemlist);

                            enemlist[i].PickUp(itemlist[j], null, level);
                        }
                    }
                }
            }

            //Weapon collisions - the enemy contemplates picking up the weapon if it can and is near one
            for (int i = 0; i < enemlist.Count; i++)
            {
                for (int j = 0; j < weaponlist.Count; j++)
                {
                    if (weaponlist[j].CanBePickedUp(enemlist[i]) == true && enemlist[i].CheckCanPickUp() == true && enemlist[i].CheckShouldPickUp(null, weaponlist[j]) == true)
                    {
                        //Pick up the weapon
                        enemlist[i].PickUp(null, weaponlist[j], level);
                    }
                }
            }

            //for (int i = 0; i < enemlist.Count; i++)
            //{
            //    //Hazard Collisions
            //    if (hazards != null)
            //    {
            //        for (int j = 0; j < hazards.Count; j++)
            //        {
            //            //Do hazard stuff (water slows, fire hurts, etc.)
            //            //The check for colliding is inside so each one can be more diverse
            //            hazards[j].InteractEnemy(activeTime, enemlist[i], TileEngine);
            //        }
            //    }
            //}
        }

        //Checks if a weapon's hitbox hits an enemy
        public static void WeaponObjectCollisions(float activeTime, SubLevel level)
        {
            List<Player> Players = level.GetPlayers;
            List<Enemy> enemlist = level.GetEnemies;
            List<ItemContainer> containerlist = level.GetContainers;
            List<Item> itemlist = level.GetItems;
            List<Weapon> weaponlist = level.GetWeapons;
            List<Hazard> hazards = level.GetHazards;
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            //Check through each weapon and see if it hit something
            for (int i = 0; i < weaponlist.Count; i++)
            {
                //Weapon collisions with players - Don't allow players to hit each other with weapons unless damage among players is enabled
                //If no one has the weapon or an enemy has the weapon, the player is able to get hit by the weapon
                if ((Main.PlayerDamage == true && weaponlist[i].GetPlayer() != null) || weaponlist[i].GetEnemy() != null || (weaponlist[i].GetEnemy() == null && weaponlist[i].GetPlayer() == null))
                {
                    for (int p = 0; p < Players.Count; p++)
                    {
                        if (Players[p].CanGetHit(Players[p].CollisionBox, weaponlist[i].GHitbox) == true)
                        {
                            //Make the player get hit by the weapon
                            Players[p].TakeDamage(activeTime, !weaponlist[i].GHitbox.Direction, weaponlist[i].TotalDamageDealt(weaponlist[i].GHitbox), weaponlist[i].GetStatus, weaponlist[i].GHitbox, TileEngine, Solids);

                            //Make the weapon bounce off a player if it was thrown
                            if (weaponlist[i].IsThrown == true) weaponlist[i].Bounce();
                            //If it can hit while falling, it means it's the strongest weapon in the game and should disappear
                            //else if (weaponlist[i].IsPickedUp == false && weaponlist[i].CurHeight > weaponlist[i].GetObjectTile.Z) weaponlist[i].FallDisappear();

                            //If the weapon is picked up, check if it should give points
                            if (weaponlist[i].IsPickedUp == true)
                            {
                                if (weaponlist[i].GetPlayer() != null)
                                {
                                    //If another player is holding the weapon and it hits the player, show the player's HUD
                                    HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, weaponlist[i].GetPlayer().GetHUD);
                                    weaponlist[i].GetPlayer().AddScore(weaponlist[i].GHitbox.Score);
                                    weaponlist[i].GetPlayer().EnterHitLag();
                                }
                                else if (weaponlist[i].GetEnemy() != null)
                                {
                                    //If an enemy is holding the weapon and it hits the player, show the enemy's HUD
                                    HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, weaponlist[i].GetEnemy().GetHUD);
                                    weaponlist[i].GetEnemy().SetHitLag(activeTime);
                                }
                            }
                            //Otherwise, if the weapon is thrown or flying in the air with a hitbox, show the weapon's HUD
                            else HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, weaponlist[i].GetHUD);
                        }
                    }
                }

                //Weapon collisions with enemies - enemies are unable to hit each other with weapons
                if (weaponlist[i].GetEnemy() == null)
                {
                    for (int j = 0; j < enemlist.Count; j++)
                    {
                        if (enemlist[j].CanGetHurt(enemlist[j].CollisionBox, weaponlist[i].GHitbox))
                        {
                            //If a player hits an enemy with a weapon, reset the previous HUD so it doesn't show up so you can draw the new one
                            HUD.ChangeHUD(activeTime, weaponlist[i].GetPlayer().GPlayerNum, enemlist[j].GetHUD);

                            enemlist[j].GetHurt(activeTime, weaponlist[i].GHitbox.Direction, weaponlist[i].GetPlayer(), weaponlist[i].GetPlayer(), weaponlist[i].TotalDamageDealt(weaponlist[i].GHitbox), weaponlist[i].GetStatus, weaponlist[i].GHitbox, TileEngine);

                            //Make the weapon bounce off an enemy if it was thrown
                            if (weaponlist[i].IsThrown == true) weaponlist[i].Bounce();
                            //If it can hit while falling, it means it's the strongest weapon in the game and should disappear
                            //else if (weaponlist[i].IsPickedUp == false && weaponlist[i].CurHeight > weaponlist[i].GetObjectTile.Z) weaponlist[i].FallDisappear();

                            //Give the player points for attacking enemies with a weapon
                            if (weaponlist[i].GetPlayer() != null)
                            {
                                weaponlist[i].GetPlayer().AddScore(weaponlist[i].GHitbox.Score);
                                if (weaponlist[i].IsPickedUp == true) weaponlist[i].GetPlayer().EnterHitLag();
                            }
                        }
                    }
                }

                //Weapon collisions with containers
                for (int j = 0; j < containerlist.Count; j++)
                {
                    if (containerlist[j].CanBreak(weaponlist[i].GHitbox) == true)
                    {
                        //If you hit a container, reset the previous HUD so it doesn't show up so you can draw the new one
                        if (weaponlist[i].GetPlayer() != null) HUD.ChangeHUD(activeTime, weaponlist[i].GetPlayer().GPlayerNum, containerlist[j].GetHUD);

                        //Make the container take damage
                        containerlist[j].Break(activeTime, weaponlist[i].TotalDamageDealt(weaponlist[i].GHitbox), weaponlist[i].GHitbox, itemlist, weaponlist, TileEngine, Solids);

                        //Make the weapon bounce off a container if it was thrown
                        if (weaponlist[i].IsThrown == true) weaponlist[i].Bounce();

                        //Set the hitlag for the player or enemy with the weapon
                        if (weaponlist[i].IsPickedUp == true)
                        {
                            if (weaponlist[i].GetPlayer() != null) weaponlist[i].GetPlayer().EnterHitLag();
                            else if (weaponlist[i].GetEnemy() != null) weaponlist[i].GetEnemy().SetHitLag(activeTime);
                        }
                    }
                }

                //Weapon collisions with hazards
                //for (int j = 0; j < hazards.Count; j++)
                //{
                //    hazards[j].InteractWeapon(activeTime, weaponlist[i], TileEngine);
                //}
            }
        }

        //For collisions among players in Versus mode
        public static void VersusPlayerCollisions(float activeTime, List<Player> Players, int[] colors, bool teamattack, int GameMode, int[] lasthit, float damageratio, SubLevel level)
        {
            TileEngine TileEngine = level.GetTileEngine;
            List<BeatEmUpObj> Solids = level.GetSolids;

            int[][] hits = SetUpTradeValues(Players.Count, Players.Count);

            for (int i = 0; i < Players.Count; i++)
            {
                for (int j = 0; j < Players.Count; j++)
                {
                    //Make sure the player doesn't hurt itself; additionally, if players are on the same team and team attack is off, don't let them attack one another
                    if (i == j) continue;
                    if (teamattack == false)
                    {
                        if (colors != null && colors[Players[i].GPlayerNum] == colors[Players[j].GPlayerNum]) continue;
                    }

                    //Check if Wil's bullets hit other players
                    if (Players[i].GBullets != null)
                    {
                        for (int g = 0; g < Players[i].GBullets.Count; g++)
                        {
                            if (Players[j].CanGetHit(Players[j].CollisionBox, Players[i].GBullets[g].GHitbox) == true)
                            {
                                //If a player hits another player, display each player's limited HUD (Health, Name, and Portrait) under their own respective ones
                                HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                                HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                                int totaldamage = GameMode != (int)VersusScreen.GameModes.Avoid ? (int)Math.Ceiling((Players[i].GetDamage + Players[i].GBullets[g].GHitbox.Strength) * damageratio) : 0;

                                //Track which player's bullets hit this one last to give the appropriate player a point if this player lost a life
                                if (GameMode == (int)VersusScreen.GameModes.Score) lasthit[Players[j].GPlayerNum] = Players[i].GPlayerNum;

                                //Make the enemy's tank take damage
                                Players[j].TakeDamage(activeTime, !Players[i].GBullets[g].GHitbox.Direction, totaldamage, Players[i].GBullets[g].GetStatus, Players[i].GBullets[g].GHitbox, TileEngine, Solids);

                                //Make Wil lose Health or a free bullet if the bullet hit the tank
                                Players[i].SpecialAttackHit(activeTime, g);

                                //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                                Players[i].GBullets[g].Intersect();
                            }
                        }
                    }

                    for (int h = 0; h < Players[i].GetHitboxes.Count; h++)
                    {
                        //Make the player's tank take damage
                        if (Players[i].GetOxygenTank != null && Players[j].GetOxygenTank.CanBreak(activeTime, Players[i].GetHitboxes[h]) == true)
                        {
                            //If you hit another player's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                            HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                            HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                            //Track which player hit this one's oxygen tank last to give the appropriate player a point if this player lost a life
                            if (GameMode == (int)VersusScreen.GameModes.Score) lasthit[Players[j].GPlayerNum] = Players[i].GPlayerNum;
                            Players[j].GetOxygenTank.Break(activeTime, GameMode != (int)VersusScreen.GameModes.Avoid ? 1 : 0, Players[i].GetHitboxes[h], TileEngine);

                            //Make the enemy unable to get hurt by the hitbox that hit the tank
                            Players[j].GetHurtbox.SetHitBox(activeTime, Players[i].GetHitboxes[h]);

                            Players[i].EnterHitLag();
                        }

                        if (Players[j].CanGetHit(Players[j].CollisionBox, Players[i].GetHitboxes[h]) == true)
                        {
                            hits[i][j] = h;
                            break;
                        }
                    }
                }
            }

            //Store a copy of each player's hitboxes because they will lose their own if they get hurt
            List<Hitbox>[] hitboxlist = new List<Hitbox>[Players.Count];
            for (int i = 0; i < Players.Count; i++)
            {
                hitboxlist[i] = new List<Hitbox>();
                hitboxlist[i].AddRange(Players[i].GetHitboxes);
            }

            //Implement all the player collisions at once, allowing hits to trade
            for (int i = 0; i < hits.Length; i++)
            {
                for (int j = 0; j < hits[i].Length; j++)
                {
                    if (hitboxlist[i].Count > 0 && hits[i][j] != -1 && Players[j].CanGetHit(Players[j].CollisionBox, hitboxlist[i][hits[i][j]]) == true)
                    {
                        //Calculate the total damage
                        int newdamage = 0;
                        
                        //Players can't do any damage to each other in the Avoid Enemy game mode
                        if (GameMode != (int)VersusScreen.GameModes.Avoid)
                        {
                            newdamage = Players[i].TotalDamageDealt(hitboxlist[i][hits[i][j]]);

                            newdamage = (int)Math.Ceiling(newdamage * damageratio);
                        }

                        //If a player hits another player, display each player's limited HUD (Health, Name, and Portrait) under their own respective ones
                        HUD.ChangeHUD(activeTime, Players[i].GPlayerNum, Players[j].GetHUD);
                        HUD.ChangeHUD(activeTime, Players[j].GPlayerNum, Players[i].GetHUD);

                        //Make sure it doesn't increase the hit counter if you're hitting with something other than standard attacks
                        Players[i].SetNextAttack();

                        //Make the player experience hitlag from hitting another player
                        Players[i].EnterHitLag();

                        //Track which player hit this one last to give the appropriate player a point if this player lost a life
                        if (GameMode == (int)VersusScreen.GameModes.Score) lasthit[Players[j].GPlayerNum] = Players[i].GPlayerNum;
                        Players[j].TakeDamage(activeTime, !hitboxlist[i][hits[i][j]].Direction, newdamage, new Status(), hitboxlist[i][hits[i][j]], TileEngine, Solids);
                    }
                }
            }
        }

        //Checks if an object touched a solid object from the X direction
        public static bool TouchedSolidsX(Vector4 Location, Vector3 Velocity, Rectangle CollisionBox, List<Collideable> Solids)
        {
            if (Solids != null)
            {
                for (int i = 0; i < Solids.Count; i++)
                {
                    if (Solids[i].TouchedX(Location, new Vector2(Velocity.X, Velocity.Y), CollisionBox) == true) return true;
                }
            }

            return false;
        }

        //Checks if an object touched a solid object from the Y direction
        public static bool TouchedSolidsY(Vector4 Location, Vector3 Velocity, Rectangle CollisionBox, List<Collideable> Solids)
        {
            if (Solids != null)
            {
                for (int i = 0; i < Solids.Count; i++)
                {
                    if (Solids[i].TouchedY(Location, new Vector2(Velocity.X, Velocity.Y), CollisionBox) == true) return true;
                }
            }

            return false;
        }

        //Checks if an object jumped underneath a solid object
        public static bool JumpedUnderSolids(Vector4 Location, Vector3 Velocity, Rectangle CollisionBox, List<Collideable> Solids)
        {
            if (Solids != null)
            {
                for (int i = 0; i < Solids.Count; i++)
                {
                    if (Solids[i].JumpedUnder(Location, Velocity.Z, CollisionBox) == true) return true;
                }
            }

            return false;
        }

        //Checks if an object jumped on top of a solid object
        public static int JumpedOnSolids(Vector4 Location, Vector3 Velocity, Rectangle CollisionBox, List<Collideable> Solids)
        {
            if (Solids != null)
            {
                for (int i = 0; i < Solids.Count; i++)
                {
                    if (Solids[i].JumpedOnTop(Location, Velocity.Z, CollisionBox) == true) return i;
                }
            }

            return -1;
        }

        //Sets up a double array of integers for checking collisions that will be used to determine trades
        private static int[][] SetUpTradeValues(int playercount, int enemycount)
        {
            int[][] hits = new int[playercount][];
            for (int i = 0; i < hits.Length; i++)
            {
                hits[i] = new int[enemycount];
                for (int j = 0; j < hits[i].Length; j++)
                    hits[i][j] = -1;
            }

            return hits;
        }

        //Values for the screen boundaries
        public static float TopBounds
        {
            get { return 10; }
        }

        public static float BottomBounds
        {
            get { return (Main.ScreenSize.Y - 10); }
        }

        public static float LeftBounds
        {
            get { return 10; }
        }

        public static float RightBounds
        {
            get { return (Main.ScreenSize.X - 10); }
        }

        //Checks for if the object is past the screen
        public static bool ScreenBoundsTop(float Location, Vector2 velocity)
        {
            if ((Location + velocity.Y) >= TopBounds) return false;

            return true;
        }

        public static bool ScreenBoundsBottom(float Location, Vector2 velocity)
        {
            if ((Location + velocity.Y) <= BottomBounds) return false;

            return true;
        }

        public static bool ScreenBoundsLeft(float Location, Vector2 velocity)
        {
            if ((Location + velocity.X) >= LeftBounds) return false;

            return true;
        }

        public static bool ScreenBoundsRight(float Location, Vector2 velocity)
        {
            if ((Location + velocity.X) <= RightBounds) return false;

            return true;
        }

        public static bool ScreenBoundsAll(Vector2 Location, Vector2 velocity, Rectangle FeetLoc)
        {
            if (ScreenBoundsTop(Location.Y, velocity) == false && ScreenBoundsBottom(FeetLoc.Bottom, velocity) == false && ScreenBoundsLeft(Location.X, velocity) == false && ScreenBoundsRight(FeetLoc.Right, velocity) == false) return false;

            return true;
        }*/
    }
}