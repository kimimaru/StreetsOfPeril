﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A class for animating a series of sprites
    public sealed class Animation
    {
        //Spritesheet with animation frames - All frames have a uniform height and width - the entire animation's width is the frame width multiplied by the number of frames
        private Texture2D Anim;

        //The width of each animation frame
        private int FrameWidth;

        //Frame information for looping
        private int CurFrame;
        private int MaxFrames;
        private int CurLoops;
        private int MaxLoops;
        private bool AnimationEnd;

        //Checks if an animation should be reversed after playing forwards first or is currently being played backwards
        private bool ShouldReverse;
        private bool Reverse;

        private float PrevFrame;
        private float[] FrameDurations;

        //The amount of time the duration of the current animation frame is extended by due to hitlag
        private float HitDelay;

        //The origin to draw the animation; this should be at the bottom-middle of each frame, allowing for easy flipping
        private Vector2 Origin;

        //The location and rotation of a weapon on a specific frame - For players and enemies
        //The X is the X position, the Y is the Z position, and the Z is the rotation amount
        private Vector3[] WeaponLoc;

        //For playing sounds at certain points in an animation
        private int[] FrameSounds;
        private SoundEffect[] Sounds;
        private bool PlayedSound;

        //Constructor - supports multiple frame durations and reversing an animation
        public Animation(Texture2D anim, bool shouldreverse, int maxframes, params float[] framedurations)
        {
            Anim = anim;
            FrameWidth = Anim.Width / maxframes;

            CurFrame = 0;
            MaxFrames = maxframes - 1;

            CurLoops = 0;
            if (MaxLoops <= 0) MaxLoops = 1;
            AnimationEnd = false;

            ShouldReverse = shouldreverse;
            Reverse = false;

            Origin = DefaultOrigin(new Vector2(FrameWidth, Anim.Height));

            PrevFrame = 0f;
            FrameDurations = framedurations;

            HitDelay = 0f;

            PlayedSound = false;
        }

        public Animation(Texture2D anim, int maxloops, bool shouldreverse, int maxframes, params float[] framedurations) : this(anim, shouldreverse, maxframes, framedurations)
        {
            MaxLoops = maxloops;
        }

        public Animation(Texture2D anim, bool shouldreverse, int maxframes, int[] framesounds, SoundEffect[] sounds, params float[] framedurations) : this(anim, shouldreverse, maxframes, framedurations)
        {
            FrameSounds = framesounds;
            Sounds = sounds;
        }

        public Animation(Texture2D anim, bool shouldreverse, int maxframes, Vector3[] weaponloc, params float[] framedurations) : this(anim, shouldreverse, maxframes, framedurations)
        {
            WeaponLoc = weaponloc;
        }

        //The default origin for drawing a sprite; the bottom-middle of the sprite
        public static Vector2 DefaultOrigin(Texture2D Texture)
        {
            return (new Vector2(Texture.Width / 2, Texture.Height));
        }

        //The default origin for drawing a sprite, given a Vector2 value
        public static Vector2 DefaultOrigin(Vector2 TextureDimensions)
        {
            return (new Vector2((int)TextureDimensions.X / 2, (int)TextureDimensions.Y));
        }

        public int CurrentFrame
        {
            get { return CurFrame; }
        }

        public int MaxFrame
        {
            get { return MaxFrames; }
        }

        public Vector2 GetOrigin
        {
            get { return Origin; }
        }

        //These methods get the length of frames in an animation; the ones that take hit delay and water into account are NOT to be used when creating hitboxes!!!
        //This is because when updating the hitbox, we already pass in the slowrate, so it already knows how much to delay it by; the lengths that take water and hit delay into account are just for reference if they're ever needed
        //Gets the length of the current frame in an animation, taking hit delay and water into account
        public float GetCurFrameLength(float slowrate)
        {
            return GetFrameLength(CurFrame, slowrate);//(FrameDurations[CurFrame >= FrameDurations.Length ? 0 : CurFrame] / Math.Max(slowrate / .2f, .85f)) + HitDelay;
        }

        //Gets the length of the current frame in an animation, not taking hit delay or water into account
        public float GetCurFrameLength()
        {
            return GetFrameLength(CurFrame);//(FrameDurations[CurFrame >= FrameDurations.Length ? 0 : CurFrame]);
        }

        //Gets the length of a frame in an animation, taking hit delay and water into account
        public float GetFrameLength(int frame, float slowrate)
        {
            float rate = 1f;
            if (slowrate < TileEngine.Gravity) rate = NewAnimation.WaterSlow;
            else if (slowrate > TileEngine.Gravity) rate = NewAnimation.StunDownSpeed;

            return (int)((FrameDurations[frame >= FrameDurations.Length ? 0 : frame] * rate/*/ Math.Max(slowrate / TileEngine.Gravity, .85f)*/) + HitDelay);
        }

        //Gets the length of a frame in an animation, not taking hit delay or water into account
        public float GetFrameLength(int frame)
        {
            return (FrameDurations[frame >= FrameDurations.Length ? 0 : frame]);
        }

        //Gets the combined length of several frames in an animation, taking hit delay and water into account
        public float GetFrameLengths(float slowrate, params int[] frames)
        {
            float combinedframelength = 0f;

            //Add up the length of each specified frame
            for (int i = 0; i < frames.Length; i++)
                combinedframelength += GetFrameLength(frames[i], slowrate);

            return combinedframelength;
        }

        //Gets the combined length of several frames in an animation, not taking hit delay or water into account
        public float GetFrameLengths(params int[] frames)
        {
            float combinedframelength = 0f;

            //Add up the length of each specified frame
            for (int i = 0; i < frames.Length; i++)
                combinedframelength += GetFrameLength(frames[i]);

            return combinedframelength;
        }

        //Gets the length of the entire animation, taking hit delay and water into account
        public float FullDuration(float slowrate)
        {
            float animlength = 0f;

            //Add up the length of each individual frame
            for (int i = 0; i <= MaxFrames; i++)
                animlength += GetFrameLength(i, slowrate);

            return animlength;
        }

        //Gets the length of the entire animation, not taking hit delay or water into account
        public float FullDuration()
        {
            float animlength = 0f;

            //Add up the length of each individual frame
            for (int i = 0; i <= MaxFrames; i++)
                animlength += GetFrameLength(i);

            return animlength;
        }

        //Gets the length of the entire animation, not taking hit delay or water into account, excluding the duration of specific frames
        public float FullDurationExcluding(params int[] frames)
        {
            float animlength = 0f;

            //Add up the length of each individual frame
            for (int i = 0; i <= MaxFrames; i++)
            {
                //Exclude the frames listed
                if (CheckExcludedFrames(i, frames) == false)
                    animlength += GetFrameLength(i);
            }

            return animlength;
        }

        private bool CheckExcludedFrames(int index, int[] frames)
        {
            for (int i = 0; i < frames.Length; i++)
            {
                if (frames[i] == index) return true;
            }

            return false;
        }

        //Gets the size of the full animation
        public Vector2 GetAnimSize()
        {
            return new Vector2(Anim.Width, Anim.Height);
        }

        //Gets the size of a frame in the animation
        public Vector2 GetFrameSize()
        {
            return (new Vector2(FrameWidth/*Anim.Width / (MaxFrames + 1)*/, Anim.Height));
        }

        //Draws a sprite affected by water depth
        //NOTE: While this works very well without rotations, with rotations it's a nightmare to get working flawlessly; I've gotten far but unfortunately not far enough - scrap any water heights above 0 and less than 900 (underwater)
        //Unfortunately this also means that this method will have to be scrapped

        /*HOWEVER, I can bring this back if I decide to remove any rotations in the game...I don't know how I'm going to do stuff like rotating enemies enemies when thrown and such yet, 
          but I'll decide when I do know!*/

        //public static void DrawSpriteWater(SpriteBatch spriteBatch, Texture2D Anim, int CurFrame, int AnimWidth, Vector2 Position, bool facingright, Color statuscolor, Vector2 origin, float rotation, float waterheight, float Layer)
        //{
        //    SpriteEffects spriteeffect = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
        //
        //    if (waterheight < 0 || waterheight > 900) waterheight = 0;
        //    if (waterheight > Anim.Height) waterheight = Anim.Height;
        //
        //    //Get the size of the unsubmerged part
        //    Rectangle sourcerect = new Rectangle((CurFrame * AnimWidth), 0, AnimWidth, Anim.Height - (int)waterheight);
        //
        //    if (waterheight < Anim.Height) spriteBatch.Draw(Anim, Position, sourcerect, statuscolor, rotation, origin, 1f, spriteeffect, Layer);
        //    if (waterheight > 0)
        //    {
        //        float Y = (float)(waterheight * Math.Cos(rotation));
        //
        //        //Get the size of the submerged part
        //        Rectangle submergedsource = new Rectangle((CurFrame * AnimWidth), Anim.Height - (int)waterheight, AnimWidth, (int)waterheight);
        //
        //        //Set the submerged part's location to be the location of the unsubmerged part, then move it to the end of the unsubmerged part depending on how the unsubmerged part is rotated by changing the X and Y
        //        Vector2 newloc = Position;
        //        newloc.X += (float)(sourcerect.Height * -Math.Sin(rotation));
        //        newloc.Y += (float)(sourcerect.Height * Math.Cos(rotation));
        //
        //        //Change the height to be dependent on position but also have it under the water
        //        Layer /= 1000f;
        //        spriteBatch.Draw(Anim, newloc, submergedsource, statuscolor, rotation, origin, 1f, spriteeffect, Layer);
        //    }
        //}

        //This method is used in the Camera class to set sounds to the vertical Go animation if the horizontal one becomes null - used for multidirectional scrolling so the sounds play only once
        public void SetSounds(int[] framesounds, SoundEffect[] sounds)
        {
            FrameSounds = framesounds;
            Sounds = sounds;
            PlayedSound = true;
        }

        public Vector3 CurWeaponLoc(bool FacingRight, int spritewidth)
        {
            if (WeaponLoc == null || CurFrame >= WeaponLoc.Length) return new Vector3(0, 10, 0);

            if (FacingRight == true) return WeaponLoc[CurFrame];
            //This doesn't apply right now, but when animations are finished and the origin for drawing is at the center of the sprites, simply flipping WeaponLoc[CurFrame].X should work
            else return new Vector3(-WeaponLoc[CurFrame].X - spritewidth, WeaponLoc[CurFrame].Y, -WeaponLoc[CurFrame].Z);
        }

        public float GPrevFrame
        {
            get { return PrevFrame; }
        }

        private void Animate(float activeTime, float slowrate)
        {
            if (FrameSounds != null && Sounds != null && PlayedSound == false && CurFrame == FrameSounds[CurFrame])
            {
                LoadSounds.Play(Sounds[CurFrame]);
                PlayedSound = true;
            }

            if (Math.Round(activeTime - PrevFrame, 1) >= GetCurFrameLength(slowrate))
            {
                if (CurFrame >= MaxFrames)
                {
                    if (ShouldReverse == false)
                    {
                        CurFrame = 0;
                        CurLoops++;
                        if (CurLoops >= MaxLoops)
                        {
                            AnimationEnd = true;
                            CurLoops = 0;
                        }
                    }
                    else
                    {
                        CurFrame = MaxFrames - 1;
                        Reverse = true;
                    }
                    HitDelay = 0f;
                }
                else
                {
                    CurFrame++;
                    AnimationEnd = false;
                    //PlayedSound = false;
                }
                PrevFrame = activeTime;
                HitDelay = 0f;
                PlayedSound = false;
            }
            else AnimationEnd = false;
        }

        //Animates an animation in reverse
        private void AnimateReverse(float activeTime, float slowrate)
        {
            if (FrameSounds != null && Sounds != null && PlayedSound == false && CurFrame == FrameSounds[CurFrame])
            {
                LoadSounds.Play(Sounds[CurFrame]);
                PlayedSound = true;
            }

            if (Math.Round(activeTime - PrevFrame, 1) >= GetCurFrameLength(slowrate))
            {
                if (CurFrame <= 0)
                {
                    //For animations that repeat constantly, we need to set the current frame back to 0 so it smoothly transitions to the next frame when it restarts
                    /*NOTE: In constantly reversing animations, it currently stays on the first frame 2 times since it moves to first frame instead of the second; we need something to track
                    whether it keeps reversing or not and whether it started out reversed or not so we can know when to actually end the animation*/
                    CurFrame = 0;//MaxFrames;
                    CurLoops++;
                    if (CurLoops >= MaxLoops)
                    {
                        AnimationEnd = true;
                        CurLoops = 0;
                    }
                    Reverse = false;
                }
                else
                {
                    CurFrame--;
                    AnimationEnd = false;
                    //PlayedSound = false;
                }
                PrevFrame = activeTime;
                HitDelay = 0f;
                PlayedSound = false;
            }
            else AnimationEnd = false;
        }

        //Skips to a certain frame of the animation
        public void SkipToFrame(int frame)
        {
            if (frame <= MaxFrames && frame >= 0)
                CurFrame = frame;
        }

        //Checks if the animation is currently being played in reverse
        public bool IsReversing()
        {
            return Reverse;
        }

        //Checks if the animation is done - when curframe is set back to 0 after updating that means its done
        public bool IsAnimationEnd()
        {
            return AnimationEnd;
        }

        //Checks if the animation is active
        public bool IsActive(float slowrate = .2f)
        {
            //If the frame is in the middle of the animation, the animation is still active
            if ((Reverse == false && CurFrame > 0) || (Reverse == true && CurFrame < MaxFrames))
            {
                return true;
            }
            //If the animation is on the starting frame, check if the animation isn't finished with the first frame, otherwise it's inactive
            else
            {
                return (Math.Round(Main.GetActiveTime - PrevFrame, 1) < GetCurFrameLength(slowrate));
            }
        }

        //Checks if a particular frame of the animation is done
        public bool IsFrameEnd(int frame)
        {
            //If the animation isn't playing in reverse...
            if (Reverse == false)
            {
                //Check if the frame is less than the max frames, in which case the current frame in the animation would be the next frame if the frame specified was done
                if (frame < MaxFrames) return (CurFrame == (frame + 1));
                //Otherwise, simply check if the animation is over since the current frame would've reset to 0
                else return AnimationEnd;
            }
            //Otherwise...
            else
            {
                //Check if the frame is greater than 0, in which case the current frame in the animation would be the previous frame if the frame specified was done
                if (frame > 0) return (CurFrame == (frame - 1));
                //Otherwise, simply check if the animation is over since the current frame would've reset to the last frame
                else return AnimationEnd;
            }
        }

        //Resets animation when it's not playing or right before it plays
        public void Reset(float activeTime)
        {
            CurFrame = 0;
            PrevFrame = activeTime;
            HitDelay = 0f;
            CurLoops = 0;
            AnimationEnd = false;
            PlayedSound = false;
            Reverse = false;
        }

        //Resets the animation in reverse when it's not playing or right before it plays
        public void ResetReverse(float activeTime)
        {
            CurFrame = MaxFrames;
            PrevFrame = activeTime;
            HitDelay = 0f;
            CurLoops = 0;
            AnimationEnd = false;
            PlayedSound = false;
            Reverse = true;
        }

        //Restarts the animation at a particular frame
        public void RestartAtFrame(int frame)
        {
            Reset(Main.GetActiveTime);
            SkipToFrame(frame);
        }

        //Ends the animation
        public void End()
        {
            AnimationEnd = true;
        }

        public void DelayHit()
        {
            HitDelay += NewCollision.HitLag;
        }

        //Slowrate is for slowing down animations underwater or with hitlag - decide whether to keep this for underwater levels or not based on how it affects the flow of the game
        public void Update(float activeTime, float slowrate = .2f)
        {
            if (Reverse == false) Animate(activeTime, slowrate);
            else AnimateReverse(activeTime, slowrate);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, float Layer)
        {
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, Layer);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, Vector2 origin, Vector2 Scale, float Layer)
        {
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), Color.White, 0f, origin, Scale, SpriteEffects.None, Layer);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, Vector2 origin, Vector2 Scale, bool facingright, Color statuscolor, float Layer)
        {
            SpriteEffects spriteeffects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), statuscolor, 0f, origin, Scale, spriteeffects, Layer);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, Vector2 origin, bool facingright, Color statuscolor, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), statuscolor, 0f, origin, 1f, effects, Layer);
        }

        //Draw with the default origin
        public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), statuscolor, rotation, Origin, 1f, effects, Layer);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, Vector2 origin, float rotation, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), statuscolor, rotation, origin, 1f, effects, Layer);
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, Vector2 origin, float rotation, double scale, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(Anim, Position, new Rectangle((CurFrame * FrameWidth), 0, FrameWidth, Anim.Height), statuscolor, rotation, origin, (float)scale, effects, Layer);
        }

        //For drawing individual sprites
        //Draws a sprite with the default origin
        public static void DrawSprite(SpriteBatch spriteBatch, Texture2D sprite, Vector2 Position, bool facingright, Color statuscolor, float rotation, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(sprite, Position, null, statuscolor, rotation, DefaultOrigin(sprite), 1f, effects, Layer);
        }

        //Draws a sprite with a custom origin
        public static void DrawSprite(SpriteBatch spriteBatch, Texture2D sprite, Vector2 Position, bool facingright, Color statuscolor, float rotation, Vector2 origin, float Layer)
        {
            SpriteEffects effects = facingright == false ? SpriteEffects.None : SpriteEffects.FlipHorizontally;
            spriteBatch.Draw(sprite, Position, null, statuscolor, rotation, origin, 1f, effects, Layer);
        }

        //Draw with a custom origin
        //public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, Vector2 origin, float rotation, float waterheight, float Layer)
        //{
        //    DrawSpriteWater(spriteBatch, Anim, CurFrame, FrameWidth, Position, facingright, statuscolor, origin, rotation, waterheight, Layer);
        //}
        //
        ////Draw with the default origin
        //public void Draw(SpriteBatch spriteBatch, Vector2 Position, bool facingright, Color statuscolor, float rotation, float waterheight, float Layer)
        //{
        //    DrawSpriteWater(spriteBatch, Anim, CurFrame, FrameWidth, Position, facingright, statuscolor, Origin, rotation, waterheight, Layer);
        //}
    }
}
