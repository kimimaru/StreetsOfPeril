﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //Simple test object for testing the BeatEmUpObj base class
    public class TestBeatEmUpObj : BeatEmUpObj
    {
        private AnimFrame sprite;
        private KeyboardState state;

        public TestBeatEmUpObj()
        {
            Location = new Vector3(Main.ScreenHalf.X, Main.ScreenHalf.Y, 0f);

            Name = "TestObject";

            SpriteSheet = LoadGraphics.ItemSprite;
            Icon = LoadGraphics.ItemIcon;
            
            ObjectLength = LoadGraphics.ItemSprite.Width;
            ObjectHeight = LoadGraphics.ItemSprite.Height;

            SetUpHurtbox();
            SetUpHealth(MaxHealthInBar);

            sprite = new AnimFrame(new Rectangle(0, 0, LoadGraphics.ItemSprite.Width, LoadGraphics.ItemSprite.Height), 0f);
            state = new KeyboardState();

            hud = new HUD(this, new Vector2(225, 15));
        }

        protected override void ObjectUpdate()
        {
            //Fully heal the object
            if (Input.CheckKeyPress(state, Keys.P) == true)
                FullHeal();

            //Make the object lose health
            if (Input.CheckKeyPress(state, Keys.E) == true)
                LoseHealth(80);

            //Heal the object a bit
            if (Input.CheckKeyPress(state, Keys.R) == true)
                Heal(15);

            //Experience Hitlag
            if (Input.CheckKeyPress(state, Keys.Y) == true)
                EnterHitLag();

            //Make the object jump
            if (Input.CheckKeyPress(state, Keys.K) == true)
                Jump(new Vector3(0f, 1f, 2f));
            else if (Input.CheckKeyPress(state, Keys.I) == true)
                Jump(new Vector3(0f, -1f, 2f));
            else if (Input.CheckKeyPress(state, Keys.J) == true)
                Jump(new Vector3(-1f, 0f, 2f));//Move(new Vector2(-2f, 0f), level);
            else if (Input.CheckKeyPress(state, Keys.L) == true)
                Jump(new Vector3(1f, 0f, 2f));//Move(new Vector2(2f, 0f), level);

            //Create a hitbox around the object
            if (Input.CheckKeyPress(state, Keys.B) == true)
                CreateHitboxSurrounding(6, 20, 500f, 1000f, Hitbox.HitboxTypes.Standard, FacingRight, null, true);

            //Cycle through statuses; testing the HUD
            if (Input.CheckKeyPress(state, Keys.U) == true)
            {
                int statcond = status.GCond + 1;
                if (statcond >= Enum.GetValues(typeof(Status.Statuses)).Length) statcond = 0;
                status = new Status(statcond, 10000f);
            }

            //Change directions
            if (Input.CheckKeyPress(state, Keys.C) == true)
                FlipDirection();

            //Don't move the object if it doesnt move in the X or Y direction
            if (AirVelocity.X != 0f || AirVelocity.Y != 0f)
                ObjectMove(GetAirVelocityVec2);

            state = Keyboard.GetState();
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            sprite.Draw(spriteBatch, SpriteSheet, GetDrawLoc(cameraloc), FacingRight, status.GStatusColor, GetRotation, GetDrawDepth(cameraloc));

            hud.Draw(spriteBatch);
        }
    }
}
