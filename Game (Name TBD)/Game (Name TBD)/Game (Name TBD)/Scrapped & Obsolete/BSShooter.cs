﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A bullet or spike shooter that fires the specified projectile at players
    //Bullet shooters have 4 slots for faster firing and less health, and spike shooters have 2 slots for firing and more health along with having slower, but stronger, projectiles
    //When it gets destroyed, it blinks red a little bit then blows up
    //The shooter MAY be able to move but it's undecided yet
    public class BSShooter : Hazard
    {
        //The projectiles the shooter shoots
        private List<Projectile> Projectiles;

        //For effects like getting hit and more
        private EffectsManager Effects;

        //The locations (slots) the projectiles come from - they're offsets from the shooter's current location
        private Vector3[] ProjectileLocations;

        //The base projectile damage
        private int ProjectileDamage;

        private float ShootRate;
        //private float PrevShoot;

        //The original height the shooter is at; if it falls from very high up it will start exploding immediately
        private float OrigHeight;

        private float PrevExplode;
        private bool Exploded;

        //Makes the shooter flash red when it is about to explode
        //private int FlashCounter;

        //If bullet is true, then it'll be a bullet shooter; otherwise, it'll be a spike shooter
        public BSShooter(bool bullet, Vector3 location, bool facingright, float shootrate = 500f, int projectiledamage = 7)
        {
            //Bullet Shooter
            if (bullet == true)
            {
                Graphic = LoadGraphics.SpikeShooter;//LoadGraphics.BulletShooter;
                ProjectileLocations = new Vector3[4] { new Vector3(5, 0, 5), new Vector3(10, -5, 5), new Vector3(5, 0, 10), new Vector3(10, -5, 10) }; //new Vector3(15, 10, 5), new Vector3(30, 15, 5), new Vector3(15, 30, 10), new Vector3(30, 25, 10) };
                Health = 15;
                //ProjectileDamage = projectiledamage;
                //ShootRate = shootrate;
            }
            //Spike Shooter
            else
            {
                Graphic = LoadGraphics.SpikeShooter;
                ProjectileLocations = new Vector3[2] { new Vector3(5, 0, 10), new Vector3(10, 5, 15) }; //new Vector2(15, 10), new Vector2(30, 15) };
                Health = 30;
                //ProjectileDamage = (int)Math.Ceiling(projectiledamage * 1.5f);
                //ShootRate = shootrate + 250f;
            }

            ProjectileDamage = projectiledamage;
            ShootRate = shootrate;

            Projectiles = new List<Projectile>();

            SetLocation(location);
            OrigHeight = Location.Z;
            Velocity = Vector2.Zero;
            Hurtbox = new Hurtbox(Graphic.Width, Graphic.Height / 2);
            Effects = new EffectsManager();

            FacingRight = facingright;

            //PrevShoot = 0f;
            PrevExplode = 0f;

            Exploded = false;
            //FlashCounter = 0;
        }

        //If the shooter is dead and its explosion is done then it should be removed from the list
        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    //Ensure that the shooter blows up even if it is far offscreen
        //    if (Health <= 0f && Exploded == false) return false;
        //    else if (Health > 0)
        //        return (Location.X + OffSet.X <= -100);
        //    else
        //        return (Projectiles.Count == 0);
        //}

        public bool GFacingRight
        {
            get { return FacingRight; }
        }

        public bool IsBulletShooter
        {
            get { return (ProjectileLocations.Length >= 4); }
        }

        public float GetShootRate
        {
            get { return ShootRate; }
        }

        public int GetProjectileDamage
        {
            get { return ProjectileDamage; }
        }

        //The true shoot rate of the BSShooter; if it's a spike shooter, it shoots 250 ms slower
        public float TrueShootRate
        {
            get
            {
                if (IsBulletShooter == true)
                    return (ShootRate + 250f);
                else return ShootRate;
            }
        }

        //The true projectile damage of the BSShooter; if it's a spike shooter, it deals 1.5x more damage
        public int TrueProjectileDamage
        {
            get
            {
                if (IsBulletShooter == true)
                    return (int)Math.Ceiling(ProjectileDamage * 1.5f);
                else return ProjectileDamage;
            }
        }

        //Check if something touched the shooter from the X direction
        /*public bool TouchedX(Vector4 playerloc, Vector2 playervelocity, Rectangle playerrec)
        {
            return (Exploded == false && new Rectangle((int)(playerrec.X + playervelocity.X), (int)playerrec.Y, playerrec.Width, playerrec.Height).Intersects(CollideBox) && (playerloc.Z < MaxHeight() && (playerloc.Z + playerloc.W) > Location.Z));
        }

        //Check if something touched the item container from the Y direction
        public bool TouchedY(Vector4 playerloc, Vector2 playervelocity, Rectangle playerrec)
        {
            return (Exploded == false && new Rectangle((int)playerrec.X, (int)(playerrec.Y + playervelocity.Y), playerrec.Width, playerrec.Height).Intersects(CollideBox) && (playerloc.Z < MaxHeight() && (playerloc.Z + playerloc.W) > Location.Z));
        }

        //Check if something jumped and touched the underside of the shooter
        public bool JumpedUnder(Vector4 playerloc, float playervelocityZ, Rectangle playerrec)
        {
            float val = (playerloc.Z + playerloc.W + playervelocityZ);

            return (Exploded == false && playerrec.Intersects(CollideBox) && playervelocityZ > 0f && val >= Location.Z && val <= MaxHeight());
        }

        //Check if something jumped over and landed on the top of the shooter
        public bool JumpedOnTop(Vector4 playerloc, float playervelocityZ, Rectangle playerrec)
        {
            return (Exploded == false && playerrec.Intersects(CollideBox) && ((playerloc.Z + playervelocityZ) <= MaxHeight() && playerloc.Z > Location.Z));
        }

        //Check if something is currently on top of the shooter
        public bool IsOn(Vector4 playerloc, Rectangle playerrec)
        {
            return (Exploded == false && playerrec.Intersects(CollideBox) && playerloc.Z == MaxHeight());
        }*/

        private void TouchWater(float activeTime, Vector2 slowamount, bool into)
        {
            if (into == true)
            {
                if (GravityValue != TileEngine.WaterGravity)
                {
                    GravityValue = TileEngine.WaterGravity;

                    //Soften the impact of the shooter's fall upon hitting the water by slowing it down slightly; if the Y velocity will be negative after touching the water, set it to 0
                    if (Velocity.Y < 0f)
                    {
                        //Make sure the shooter's Y velocity gets set to 0 if its Y velocity will turn from negative to positive
                        if (Velocity.Y - slowamount.Y > 0f) Velocity.Y = 0f;
                        else Velocity.Y -= slowamount.Y;
                    }

                    //if (Velocity.Y != 0f) Effects.AddAnim(activeTime, new Vector3(Location.X + (Graphic.Width / 2), Location.Y, Location.Z), new Animation(LoadGraphics.Splash, false, 1, 300));
                }
            }
            else
            {
                if (GravityValue != TileEngine.Gravity)
                {
                    GravityValue = TileEngine.Gravity;

                    //if (Velocity.Y != 0f) Effects.AddAnim(activeTime, new Vector3(Location.X + (Graphic.Width / 2), Location.Y, Location.Z), new Animation(LoadGraphics.Splash, false, 1, 300));
                }
            }
        }

        //public override void InteractPlayer(float activeTime, Player player, TileEngine TileEngine, List<Collideable> Solids, List<Hazard> HazardList)
        //{
        //    //Check if any of the shooter's projectiles hit the player
        //    for (int i = 0; i < Projectiles.Count; i++)
        //    {
        //        if (player.CanGetHit(player.GCollisionBox, Projectiles[i].GHitbox) == true)
        //        {
        //            player.TakeDamage(activeTime, !Projectiles[i].GHitbox.Direction, Projectiles[i].GHitbox.Strength, new Status(), Projectiles[i].GHitbox, TileEngine, Solids);
        //
        //            //Make the projectile do something when it hits (Ex. bullet bouncing off or disappearing)
        //            Projectiles[i].Intersect();
        //        }
        //    }
        //
        //    //If the shooter is still alive, check for collisions
        //    if (Health > 0f)
        //    {
        //        //Check if the player hit the shooter
        //        for (int h = 0; h < player.GHitbox.Count; h++)
        //        {
        //            if (player.GHitbox[h].Throw == false && CanGetHit(player.GHitbox[h]) == true)
        //            {
        //                Health -= player.TotalDamage(player.GHitbox[h]);
        //                player.SetHitLag(activeTime);
        //                Hurtbox.SetHitBox(activeTime, player.GHitbox[h]);
        //                Effects.AddHitAnim(activeTime, Rectangle.Intersect(player.GHitbox[h].GBox, CollisionBox), Location.Z);
        //                LoadSounds.Play(LoadSounds.Punch1);
        //            }
        //        }
        //
        //        //Check if the Wil's bullets hit the shooter
        //        if (player.GBullets != null)
        //        {
        //            for (int j = 0; j < player.GBullets.Count; j++)
        //            {
        //                if (CanGetHit(player.GBullets[j].GHitbox) == true)
        //                {
        //                    Health -= player.GDamage + player.GBullets[j].GHitbox.Strength;
        //                    Hurtbox.SetHitBox(activeTime, player.GBullets[j].GHitbox);
        //
        //                    Effects.AddHitAnim(activeTime, Rectangle.Intersect(player.GBullets[j].GHitbox.GBox, CollisionBox), Location.Z);
        //
        //                    //Make Wil lose Health or a free bullet if the bullet hit the shooter
        //                    player.SpecialAttackHit(activeTime, j);
        //
        //                    //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
        //                    player.GBullets[j].Intersect();
        //
        //                    LoadSounds.Play(LoadSounds.Punch1);
        //                }
        //            }
        //        }
        //
        //        //Make the shooter start to blow up when it's destroyed
        //        if (Health <= 0f)
        //        {
        //            Health = 0f;
        //            PrevExplode = activeTime;
        //        }
        //    }
        //    //Make the shooter blow up after a few seconds
        //    else if (Exploded == false)
        //    {
        //        FlashCounter = (FlashCounter + 1) % 8;
        //
        //        if ((activeTime - PrevExplode) >= 1200f)
        //        {
        //            Exploded = true;
        //            Hitbox explosionbox = new Hitbox((int)Location.X, (int)Location.Y, CollisionBox.Width, CollisionBox.Height, Location.Z + 40f, 40, 8, 200, 500, true, false, LoadSounds.Kick);
        //            HazardList.Add(new Explosion(new Animation(LoadGraphics.Explosion, false, 1, 700), new Vector3(Location.X, Location.Y, Location.Z), explosionbox, new Status(), TileEngine));
        //        }
        //    }
        //}
        //
        //public override void InteractWeapon(float activeTime, Weapon weapon, TileEngine TileEngine)
        //{
        //    if (Health > 0)
        //    {
        //        if (CanGetHit(weapon.GHitbox) == true)
        //        {
        //            Health -= weapon.GHitbox.Strength;
        //            if (weapon.GetPlayer() != null) weapon.GetPlayer().SetHitLag(activeTime);
        //            Hurtbox.SetHitBox(activeTime, weapon.GHitbox);
        //            Effects.AddHitAnim(activeTime, Rectangle.Intersect(weapon.GHitbox.GBox, CollisionBox), Location.Z);
        //            LoadSounds.Play(LoadSounds.Punch1);
        //
        //            //Make the shooter start to blow up when it's destroyed
        //            if (Health <= 0f)
        //            {
        //                Health = 0f;
        //                PrevExplode = activeTime;
        //            }
        //        }
        //    }
        //}

        private void ObjectFall(float activeTime, TileEngine TileEngine, List<Collideable> Solids)
        {
            Location.Z += Velocity.Y;
            Location.Z = (float)Math.Round(Location.Z, 2);
            Velocity.Y -= GravityValue;
            Velocity.Y = (float)Math.Round(Velocity.Y, 2);

            //If the shooter lands on the floor...
            if (Location.Z <= ObjectTile.Z)
            {
                Location.Z = ObjectTile.Z;
                Velocity.Y = 0f;
                if (ObjectTile.TypeHeight > 0f && ObjectTile.TypeHeight <= 15f) LoadSounds.Play(LoadSounds.WaterLand);

                //Make the shooter start exploding if the height it fell from was very high
                if (OrigHeight - Location.Z >= 60)
                {
                    Health = 0;
                    PrevExplode = activeTime;
                }
                OrigHeight = Location.Z;
            }
        }

        /*private void Move(float activeTime, Vector2 OffSet, TileEngine TileEngine)
        {
            TouchWater(activeTime, new Vector2(-1, -1), ObjectTile.TouchedWater(Location.Z));

            //Check landing on a tile if the shooter is in the air
            if (Movement.ShouldObjectFall(Location.Z, ObjectTile, null) == true)
            {
                ObjectFall(activeTime, TileEngine, null);
            }
        }*/

        //public override void Update(float activeTime, SubLevel level)
        //{
        //    //If the shooter is dead, don't make it shoot
        //    //if (Health > 0f)
        //    //{
        //    //    //If the shooter is in the air make it fall
        //    //    Move(activeTime, level.GCameraOffSet, level.GTileEngine);
        //    //
        //    //    //Create a new projectile in one of the random locations when the shooter is supposed to shoot
        //    //    if ((activeTime - PrevShoot) >= TrueShootRate)
        //    //    {
        //    //        Random randomloc = new Random();
        //    //        int rand = randomloc.Next(ProjectileLocations.Length);
        //    //        Vector3 projectileloc = ProjectileLocations[rand];
        //    //        if (FacingRight == false) projectileloc.X = -projectileloc.X - LoadGraphics.projectile.Width;
        //    //        else projectileloc.X += Hurtbox.Width;
        //    //
        //    //        //Bullet shooter
        //    //        if (IsBulletShooter == false)
        //    //        {
        //    //            //Create a bullet
        //    //            Projectiles.Add(new Projectile(LoadGraphics.projectile, new Vector2(FacingRight == true ? 7 : -7, 0), new Vector2(Location.X, FeetLoc().Y) + new Vector2(projectileloc.X, projectileloc.Y), new Status(), TrueProjectileDamage, level.GTileEngine, true, 0f, Location.Z + projectileloc.Z, FacingRight));
        //    //        }
        //    //        //Spike shooter
        //    //        else
        //    //        {
        //    //            //Create a spike
        //    //            Projectiles.Add(new Projectile(LoadGraphics.Spike, new Vector2(FacingRight == true ? 4 : -4, 0), new Vector2(Location.X, FeetLoc().Y) + new Vector2(projectileloc.X, projectileloc.Y), new Status(), TrueProjectileDamage, level.GTileEngine, true, 0f, Location.Z + LoadGraphics.Spike.Height + projectileloc.Z, FacingRight));
        //    //        }
        //    //
        //    //        PrevShoot = activeTime;
        //    //    }
        //    //
        //    //    //Clear the hurtbox if the shooter didn't take damage for 5 seconds
        //    //    if ((activeTime - Hurtbox.PrevHit) >= 5000f)
        //    //        Hurtbox.Clear();
        //    //}
        //    //
        //    ////Update the projectiles
        //    //for (int i = 0; i < Projectiles.Count; i++)
        //    //{
        //    //    Projectiles[i].Update(activeTime, level);
        //    //    if (Projectiles[i].Done(level.GCameraOffSet) == true)
        //    //    {
        //    //        Projectiles.RemoveAt(i);
        //    //        i--;
        //    //    }
        //    //}
        //    //
        //    //Effects.Update(activeTime);
        //}
        //
        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    //if (Exploded == false)
        //    //{
        //    //    //Draw a shadow under the shooter if the shooter is in the air
        //    //    if (Location.Z > ObjectTile.Z)
        //    //    {
        //    //        float ShadowScale = (float)(1f - ((Location.Z - ObjectTile.Z) / 100f));
        //    //        if (ShadowScale < .4f || (Location.Z - ObjectTile.Z) > 99) ShadowScale = .4f;
        //    //
        //    //        //Draw a shadow where the shooter's shadow is
        //    //        spriteBatch.Draw(LoadGraphics.PlayerShadow, new Vector2(CollisionBox.Center.X + OffSet.X, FeetLoc().Bottom + OffSet.Y - ObjectTile.Z), null, Color.White, 0f, new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2), ShadowScale, SpriteEffects.None, ObjectTile.TypeHeight <= 0f ? 0.0019f : 0.0001f);
        //    //    }
        //    //
        //    //    Color color = Color.White;
        //    //    if (FlashCounter > 3) color = new Color(255, 90, 90);
        //    //    Animation.DrawSprite(spriteBatch, Graphic, new Vector2(Location.X + OffSet.X, Location.Y + OffSet.Y - Location.Z), FacingRight, color, 0f, GetDrawDepth(OffSet, true));
        //    //}
        //    //
        //    ////Draw all projectiles
        //    //for (int i = 0; i < Projectiles.Count; i++)
        //    //    Projectiles[i].Draw(spriteBatch, OffSet, TileEngine);
        //    ////RecCollision.Draw(spriteBatch, new Vector2(GCollision.X, GCollision.Y - Location.Z), OffSet);
        //    ////spriteBatch.Draw(LoadGraphics.FG, new Vector2(FeetLoc().X + OffSet.X, FeetLoc().Y + OffSet.Y), null, Color.White, 0f, Vector2.Zero, new Vector2((float)FeetLoc().Width / (float)LoadGraphics.FG.Width, (float)FeetLoc().Height / (float)LoadGraphics.FG.Height), SpriteEffects.None, .999f);
        //    //Effects.Draw(spriteBatch, OffSet);
        //    //
        //    //DebugDraw(spriteBatch, OffSet);
        //}
    }
}
