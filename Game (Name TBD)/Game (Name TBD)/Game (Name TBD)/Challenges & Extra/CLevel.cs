﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Challenge levels and Extra Modes
    //NOTE: Use more protected virtual functions and try to make this class a more solid base, like how the Player and Enemy classes are
    public abstract class CLevel : SubLevel
    {
        //The challenge rankings
        public static Ranking[][] ChallengeRanks;

        //The numerical value of each challenge
        public enum Challenges
        {
            Survival, Onslaught, BTO, RPS,/*Obstacle,*/ Snipe
        };

        //The difficulty level
        public enum CDifficulty
        {
            Easy, Normal, Hard
        };

        //Indicates if the challenge level can be restarted from the Pause menu
        protected bool Restartable;

        //The challenge number and difficulty (used for rankings)
        protected int ChallengeNum;
        protected int Difficulty;

        //A timer for some challenges
        protected float Time;
        protected float PrevTime;

        //Constructor
        public CLevel()
        {
            //OffSet = Vector2.Zero;

            Players = new List<Player>();

            Fade = new Fade(-5, 0, 255);

            Restartable = true;
            SubLevelFinished = false;
            
            Time = 0f;
            PrevTime = 0f;

            LevelCamera = null;

            EnemList = new List<Enemy>();
            ContainerList = new List<ItemContainer>();
            ItemList = new List<Item>();
            WeaponList = new List<Weapon>();

            Solids = new List<BeatEmUpObj>();

            Hazards = new List<Hazard>();
        }

        static CLevel()
        {
            ChallengeRanks = new Ranking[Enum.GetValues(typeof(Challenges)).Length][];

            //Initialize the challenge rankings to their default values
            ChallengeRanks[(int)Challenges.Survival] = new Ranking[] { new Ranking(200f, 1000f, 3000f, 4000f), new Ranking(3000f, 8000f, 10000f, 15000f), new Ranking(3000f, 8000f, 10000f, 15000f) };
            ChallengeRanks[(int)Challenges.Onslaught] = new Ranking[] { new Ranking(2, 5, 8, 10), new Ranking(2, 5, 8, 10), new Ranking(2, 5, 8, 10) };
            ChallengeRanks[(int)Challenges.BTO] = new Ranking[] { new Ranking(1f, 3f, 5f, 7f), new Ranking(1f, 3f, 5f, 7f), new Ranking(1f, 3f, 5f, 7f) };
            ChallengeRanks[(int)Challenges.RPS] = new Ranking[] { new Ranking(25f, 50f, 75f, 100f), new Ranking(30f, 60f, 75f, 100f), new Ranking(30f, 60f, 75f, 100f) };
            ChallengeRanks[(int)Challenges.Snipe] = new Ranking[] { new Ranking(1, 12, 13, 14), new Ranking(1, 12, 13, 14), new Ranking(1, 12, 13, 14) };
        }

        //protected void LoadInLevel(SublevelProp level)
        //{
        //    TileEngine = level.TileEngine;
        //
        //    //Add all items and weapons made in the level editor directly onto the sublevel; they will not spawn based on camera position
        //    for (int i = 0; i < level.LevelSpawns.Count; i++)
        //    {
        //        for (int j = 0; j < level.LevelSpawns[i].SpawnItems.Count; j++)
        //            ItemList.Add((Item)level.LevelSpawns[i].SpawnItems[j].ParseObject());
        //        for (int j = 0; j < level.LevelSpawns[i].SpawnWeapons.Count; j++)
        //            WeaponList.Add((Weapon)level.LevelSpawns[i].SpawnWeapons[j].ParseObject());
        //    }
        //
        //    //Make the level underwater if it's supposed to be
        //    if (level.Underwater == true) TileEngine.EditTileValuesAll(999f, (int)TileEngine.TileTypes.Water);
        //
        //    if (level.CameraType == false) LevelCamera = new Camera(BG, FG, new LevelManager(level.ConvertToLevelData()));
        //    else LevelCamera = new LCamera(BG, level.CameraRange);
        //}

        //Accessor
        public override bool SubLevelComplete()
        {
            return Fade.Finished() == true;
        }

        public bool CanRestart
        {
            get { return Restartable; }
        }

        //Challenge complete should be set to true when an exit transition is required
        public override bool CanPause()
        {
            return (SubLevelFinished == false && Fade.IsFading() == false && Main.CurrentFPS == Main.FPS);
        }

        //The location for players to respawn in the challenge stage when they lose a life (aside from Onslaught, I don't think players will have more than one life)
        public override Vector3 RespawnLoc()
        {
            return new Vector3(250 - GCameraOffSet.X, 250 - GCameraOffSet.Y, -1);
        }

        //The fading black intro and outro of challenges
        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
        }

        //Restarts the challenge
        public virtual void RestartChallenge(float activeTime)
        {
            //OffSet = Vector2.Zero;

            Fade = new Fade(-5, 0, 255);
            SubLevelFinished = false;

            EnemList.Clear();
            ContainerList.Clear();
            ItemList.Clear();
            WeaponList.Clear();

            Solids.Clear();

            Hazards.Clear();
        }

        protected override void EndSub(float activeTime, Main main)
        {
            CalculatePerformance(main);
            Fade.ContinueFade();
        }

        //Takes the health, lives, and time remaining (if there is time) and calculates it into the score
        protected virtual void CalculatePerformance(Main main)
        {
            /*//The subtotal of all the player scores shown before the other factors are taken into account
            int subtotalscore = 0;

            //The other values that influence the total score (damage taken, lives lost, etc.)
            int liveslost = 0;
            int healthremaining = 0;

            //Add all the individual player scores together and subtract the amount of lives and health lost
            for (int i = 0; i < Players.Count; i++)
            {
                subtotalscore += Players[i].GHud.GetScore();
                
                liveslost -= (maxlives - Players[i].GLives) * 1000;

                //Jeff has more max health, so subtract his max health from his current health
                if (Players[i] is Jeff)
                    healthremaining -= ((int)(200 - Players[i].GHealth)) * 10;
                else healthremaining -= ((int)(100 - Players[i].GHealth)) * 10;
            }

            //The final score; the sum of the subtotal and all the influential factors
            int totalscore = subtotalscore + (timeremaining * 100) + liveslost + healthremaining;

            //Get the current score/time of the challenge; if it changed after ranking the performance of the player(s), then it's a new record
            char rank = Ranking.LetterValue(ranking.GetCurrentPerformance(totalscore));
            char bestrank = Ranking.LetterValue(ranking.GetRank());
            float scoretime = ranking.GetScoreTime();
            Vector2 newrecordloc = Vector2.Zero;
            String newrecord = String.Empty;

            //Rank the average of all the players' performances
            ranking.RankPerformance(totalscore / Players.Count);
            ranking.RankScoreTime(totalscore);

            //If the previous best score isn't the same as the best score after ranking performance, it's a new record
            if (scoretime != ranking.GetScoreTime())
            {
                newrecordloc = new Vector2(133, 279);
                newrecord = "New Record!";
            }

            float X = 133;

            //Add the results screen, which shows the current and best rank, score/time, and whether there was a new record or not
            main.AddScreen(new ResultsScreen(null, challengename + " Results", null, new Vector2[] { new Vector2(X, 45), new Vector2(X, 70), new Vector2(X, 95), new Vector2(X, 120), new Vector2(X, 145), new Vector2(X, 194), new Vector2(X, 219), new Vector2(X, 244), newrecordloc }, 
                "Subtotal: " + subtotalscore, "Lives Lost: " + liveslost, "Health Remaining: " + healthremaining, "Time Bonus: " + (timeremaining * 100), "Final Score: " + totalscore, "Rank: " + rank, "Best Rank: " + bestrank, "Best Score: " + scoretime, newrecord));

            //Save the data
            SaveLoadData.SaveChallenge(ranking, ranknum, difficultynum);*/
        }

        protected override void OnPlayerDeath(ref int index)
        {
            base.OnPlayerDeath(ref index);

            //If all players are dead then end the challenge
            if (Players.Count == 0) Fade.ContinueFade();
        }

        //This is needed to prevent animations from messing up at the start of a challenge
        protected override void UpdatePlayerAnimations(float activeTime)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].UpdateAnimations();
                Players[i].status.UpdateTint();

                //This updates players' weapons during a transition so they continue following them
                if (Players[i].weapon != null && Players[i].weapon.IsThrown == false) Players[i].weapon.Update();
            }
        }

        //Updates the timer
        protected virtual void UpdateTimer(float activeTime)
        {
            //Get the time down to the nearest 2 digits for more accurate time
            Time = (PrevTime - activeTime) / 1000f;

            //If the time reaches 0, end the challenge stage
            if (Time <= 0)
            {
                Time = 0;
                Fade.ContinueFade();
            }
        }

        //public virtual void Update(float activeTime, Main main)
        //{
        //    //Don't update if you have to draw the challenge intro/outro
        //    if (Fade.IsFading() == false)
        //    {
        //        //Check whether to pause or not
        //        main.PauseGame(Players);
        //        if (Main.IsPaused == true) return;
        //
        //        //Check if the challenge is cleared
        //        if (IsCleared() == true)
        //        {
        //            //Does whatever needs to be done at the end of the challenge
        //            EndChallenge(activeTime, main);
        //        }
        //        else
        //        {
        //            UpdateCollisions(activeTime);
        //
        //            UpdateCamera(activeTime);
        //
        //            //Update lists
        //            UpdateEnemyList(activeTime, true);
        //            UpdateContainerList(activeTime);
        //            UpdateItemList(activeTime);
        //            UpdateWeaponList(activeTime);
        //            UpdateHazards(activeTime);
        //
        //            //Update players after those to let certain things occur first (like picking up an item but not dropping your current weapon at the same time)
        //            UpdatePlayers(activeTime);
        //
        //            //Does anything that needs to be done when the challenge isn't complete
        //            UpdateOtherNotComplete(activeTime);
        //        }
        //    }
        //    else
        //    {
        //        Transition(activeTime);
        //
        //        //Does anything that needs to be done when the challenge is complete
        //        UpdateOtherComplete(activeTime);
        //    }
        //
        //    //Does anything that needs to be done regardless of whether the challenge is complete or not
        //    UpdateOther(activeTime);
        //}

        protected virtual void DrawTimer(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Time: " + String.Format("{0:0.00}", Time), new Vector2(Main.ScreenHalf.X - 40, 50), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
        }
    }
}
