﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The Onslaught challenge
    //NOTE: Change to have the player move through different areas, indicating progress
    public class Onslaught : CLevel
    {
        //Creates enemies
        private EnemyGenerator EnemGenerator;

        //Constructor
        public Onslaught(Player[] player, Texture2D background, Texture2D foreground, Item[] items, Weapon[] weapons)
        {
            Players.AddRange(player);
            //Players.Add(CreateObjects.Wil(Players.Count));
            //Players.Add(CreateObjects.Crystal(Players.Count));
            //Players.Add(CreateObjects.Jeff(Players.Count));
            BG = background;
            FG = foreground;

            Restartable = false;

            ChallengeNum = (int)Challenges.Onslaught;
            Difficulty = OnslaughtScreen.Difficulty - 1;

            //NOTE: Change this to spawn all of the barrels at once
            LoadData(LevelEditor.LoadLevel("Onslaught"));
            
            EnemGenerator = new EnemyGenerator(this);

            ContainerList.Add(CreateObjects.Barrel(new Vector3(50, 100, 0f), 10, items[0], null)); ContainerList[ContainerList.Count - 1].SetHeldObj(items[0]);
            ContainerList.Add(CreateObjects.Barrel(new Vector3(80, 100, 0f), 10, items[1], null)); ContainerList[ContainerList.Count - 1].SetHeldObj(items[1]);
            ContainerList.Add(CreateObjects.Barrel(new Vector3(290, 100, 0f), 10, items[2], null)); ContainerList[ContainerList.Count - 1].SetHeldObj(items[2]);
            ContainerList.Add(CreateObjects.Barrel(new Vector3(320, 100, 0f), 10, null, weapons[0])); ContainerList[ContainerList.Count - 1].SetHeldObj(weapons[0]);
            ContainerList.Add(CreateObjects.Barrel(new Vector3(350, 100, 0f), 10, null, weapons[1])); ContainerList[ContainerList.Count - 1].SetHeldObj(weapons[1]);
            Solids.AddRange(ContainerList);

            foreach (ItemContainer cont in ContainerList)
            {
                cont.Spawn(this);
            }
        }

        //Onslaught will not have a completion status since Hard mode isn't supposed to be completed (but can be) and I don't want players to feel obliged to complete it if they don't want to
        protected override bool IsCleared()
        {
            if (Difficulty == 0)
            {
                if (EnemGenerator.GWaveNum >= 25)
                {
                    return true;
                }
            }
            else if (Difficulty == 1)
            {
                if (EnemGenerator.GWaveNum >= 50)
                {
                    return true;
                }
            }
            else
            {
                if (EnemGenerator.GWaveNum >= 100)
                {
                    return true;
                }
            }

            return false;
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0f, true, true);
                Players[i].SetStats(null, null, 3, null, null, null);
            }
        }

        //Ranks the player based on which wave number the player got to
        protected override void CalculatePerformance(Main main)
        {
            Ranking ranking = CLevel.ChallengeRanks[ChallengeNum][Difficulty];

            //Get the current score/time of the challenge; if it changed after ranking the performance of the player(s), then it's a new record
            char rank = Ranking.LetterValue(ranking.GetCurrentPerformance(EnemGenerator.GWaveNum));
            char bestrank = Ranking.LetterValue(ranking.GetCurrentPerformance(EnemGenerator.BestRecord));
            Vector2 newrecordloc = Vector2.Zero;
            String newrecord = String.Empty;

            //Rank the players performance
            ranking.RankPerformance(EnemGenerator.GWaveNum);
            ranking.RankScoreTime(EnemGenerator.GWaveNum);

            //If the current wave number is higher than the best wave number after ranking performance, it's a new record
            if (EnemGenerator.GWaveNum > EnemGenerator.BestRecord)
            {
                newrecordloc = new Vector2(155, 250);
                newrecord = "New Record!";
            }

            main.AddScreen(new ResultsScreen(null, "Onslaught Results", null, new Vector2[] { new Vector2(25, 100), new Vector2(233, 100), new Vector2(25, Main.ScreenHalf.Y + 10), new Vector2(233, Main.ScreenHalf.Y + 10), newrecordloc }, 
                "Your Rank: " + rank, "Best Rank: " + bestrank, "Your Wave: " + (EnemGenerator.GWaveNum + 1), "Best Wave: " + ((int)EnemGenerator.BestRecord + 1), newrecord));

            //Save the data
            SaveLoadData.SaveChallenge(ranking, ChallengeNum, Difficulty);
        }

        //Update all enemies
        protected override void UpdateEnemyList(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                EnemList[i].Update();

                if (EnemList[i].ShouldRemove == true)
                {
                    //Update bonus status
                    if (EnemList[i].gLastAttacked != null)
                    {
                        if (EnemList[i].gLastAttacked.GetBonusStatus != null)
                        {
                            EnemList[i].gLastAttacked.GetBonusStatus.IncreaseChanceStatus(EnemList[i].PossibleStatuses, EnemList[i].StartingStatus, EnemList[i].DeathStatus);
                            EnemList[i].gLastAttacked.GetBonusStatus.ChangeBonusMeter(BonusStatus.AmountAdded);
                        }
                    }
                    EnemList.RemoveAt(i);
                    EnemGenerator.EnemLeft--;
                    i--;
                }
            }
        }

        //Update all players
        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Update();

                if (Players[i].IsDead == true && Players[i].KnockedDown == false)
                {
                    Players[i].Respawn(activeTime, RespawnLoc(), TileEngine);
                    if (Players[i].IsCompletelyDead == true)
                    {
                        Players.RemoveAt(i);
                        i--;

                        //If all player are completely dead then end the challenge
                        if (Players.Count == 0)
                        {
                            //CalculatePerformance(main);
                            Fade.ContinueFade();
                        }
                    }
                }
            }
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            //Update level generator
            EnemGenerator.Update(activeTime);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            EnemGenerator.Draw(spriteBatch);
        }
    }
}
