﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //The Enemy Museum; walk around and check out enemy information!
    public class Museum : CLevel
    {
        private LCamera LimitedCamera;

        //The path to the Museum file
        private const String FileName = "Content\\SaveData\\MuseumData.xml";

        //The root of the XML storing all the enemy information
        private const String DataName = "EnemyInfo";

        //The Enemy's Museum Entry
        private EnemyInfo Entry;

        public Museum(Player player)
        {
            Players.Add(player);

            Restartable = false;

            LimitedCamera = new LCamera(LoadGraphics.BG, new Vector2(50, 0));

            TileEngine = LevelEditor.LoadLevelOld("EnemyMuseum").TileEngine;
        }

        protected override bool IsCleared()
        {
            return false;
        }

        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            //Players cannot die in the museum, so don't bother checking for it
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Update();

                //Check the tile the player is on; if it's a Museum tile, get all the information corresponding to that tile (via its typeheight) - read in from a file
                if (Players[i].ObjectTile.TileType == (int)TileEngine.TileTypes.Museum)
                {
                    //The typeheight of the Museum tile corresponds to a different enemy, so get that information and store it so it can be displayed
                    if (Entry == null || (int)Players[i].ObjectTile.TypeHeight != Entry.Enemy) Entry = new EnemyInfo((int)Players[i].ObjectTile.TypeHeight);
                }
                else if (Entry != null) Entry = null;
            }
        }

        protected override void DrawPlayers(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            if (Entry != null) Entry.Draw(spriteBatch);

            spriteBatch.Draw(LoadGraphics.BG, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .00001f);
        }

        //Reads an enemy's museum entry from an external file and stores and displays that information while a Player is on the corresponding tile
        private class EnemyInfo
        {
            //The number of the enemy you're viewing - used to change the entry displayed if two different Museum tiles are side by side
            public int Enemy;

            //The location on screen to display the enemy information
            private readonly Vector2 InfoDisplay;

            //The locations to display each piece of information
            private readonly Vector2 NameDisplay;
            private readonly Vector2 IconDisplay;
            private readonly Vector2 SpriteDisplay;
            private readonly Vector2 DescriptionDisplay;
            private readonly Vector2 StatDisplay;
            private readonly Vector2 StatusDisplay;
            private readonly Vector2 PrefersDisplay;
            private readonly Vector2 MovesDisplay;

            //The pieces of information to display
            private String EnemyName;
            private Texture2D Icon;
            private Texture2D EnemySprite;
            private String Description;
            private /*Texture2D[][]*/String[] Stats;
            private List<Texture2D> Statuses;
            private List<Texture2D> Preferences;
            private List<String> Moves;

            public EnemyInfo(int enemy)
            {
                Enemy = enemy;

                InfoDisplay = new Vector2(52, 40);

                NameDisplay = new Vector2(207, 58);
                IconDisplay = new Vector2(63, 55);
                SpriteDisplay = new Vector2(83, 71);
                DescriptionDisplay = new Vector2(156, 101);
                StatDisplay = new Vector2(104, 193);
                StatusDisplay = new Vector2(156, 192);
                PrefersDisplay = new Vector2(153, 249);
                MovesDisplay = new Vector2(257, 194);

                Stats = new String[4];
                Statuses = new List<Texture2D>();
                Preferences = new List<Texture2D>();
                Moves = new List<String>();

                //Read in this entry from the museum file
                LoadEntry(enemy);
            }

            //Load in the entry data
            private void LoadEntry(int enemy)
            {
                //Check if there is any enemy data - if not, don't try loading it
                if (File.Exists(FileName) == false)
                    return;

                //Load the entire museum file
                XDocument loadfile = XDocument.Load(FileName, LoadOptions.PreserveWhitespace);

                //Try to load the data
                XElement rootelement = loadfile.Element(DataName);

                //Load the type of enemy
                XElement sectionelement = rootelement.Element("EnemyType" + enemy);

                //If there's no info for that enemy, don't try loading it
                if (sectionelement == null) return;

                //If there's information on that enemy, start loading in the categories
                XElement info = sectionelement.Element("Info");
                if (info != null)
                {
                    EnemyName = info.Attribute("Name") != null ? info.Attribute("Name").Value : null;
                    if (enemy >= 0 && enemy < LoadGraphics.EnemyIcon.Count) Icon = LoadGraphics.EnemyIcon[enemy];
                    EnemySprite = LoadGraphics.enemsprite;//LoadGraphics.
                    Description = info.Attribute("Description") != null ? info.Attribute("Description").Value : null;
                }
                
                info = sectionelement.Element("Stats");
                if (info != null)
                {
                    for (int i = 0; i < Stats.Length; i++)
                        Stats[i] = info.Attribute("Stat" + i) != null ? info.Attribute("Stat" + i).Value : null;
                }

                info = sectionelement.Element("Statuses");
                if (info != null)
                {
                    for (int i = 0; info.Attribute("Status" + i) != null; i++)
                    {
                        Statuses.Add(LoadGraphics.StatusSprites[Status.StatNum(info.Attribute("Status" + i).Value) - 1]);
                    }
                }

                info = sectionelement.Element("Preferences");
                if (info != null)
                {
                    for (int i = 0; info.Attribute("Preference" + i) != null; i++)
                    {
                        switch (info.Attribute("Preference" + i).Value)
                        {
                            case "Points": Preferences.Add(LoadGraphics.PileOfMoney);
                                break;
                            case "Food": Preferences.Add(LoadGraphics.ItemSprite);
                                break;
                            case "Status": Preferences.Add(LoadGraphics.GrahamDoll);
                                break;
                            case "Weapon": Preferences.Add(LoadGraphics.WeaponSprite[2]);
                                break;
                        }
                    }
                }

                info = sectionelement.Element("Moves");
                if (info != null)
                {
                    for (int i = 0; info.Attribute("Move" + i) != null; i++)
                        Moves.Add(info.Attribute("Move" + i).Value);
                }
            }

            //Display all the information
            public void Draw(SpriteBatch spriteBatch)
            {
                spriteBatch.Draw(LoadGraphics.EnemyMuseumTemplate, /*InfoDisplay*/new Vector2(52, 40), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .398f);

                if (EnemyName != null)
                {
                    //Enemy name length
                    Vector2 namelength = LoadGraphics.MenuFont.MeasureString(EnemyName);

                    //Make sure the enemy's name doesn't extend past the space for it in the X direction
                    float scale = Helper.FitTextX(LoadGraphics.MenuFont, EnemyName, 84, .9f);

                    //If it does, adjust the scale and recheck
                    //while ((namelength.X * scale) >= 88)
                    //    scale -= .05f;

                    //Calculate how much to offset the Y position of the name by if it was reduced in size in order to center it
                    Vector2 XYdraw = Helper.OffSetScaledText(LoadGraphics.MenuFont, EnemyName, .9f, scale);//(float)Math.Round(((namelength.Y * .9f) - (namelength.Y * scale)) / 2, 1);
                    spriteBatch.DrawString(LoadGraphics.MenuFont, EnemyName, new Vector2(NameDisplay.X - ((namelength.X * scale) / 2), NameDisplay.Y - 7 + XYdraw.Y), Color.Black, 0f, Vector2.Zero, scale, SpriteEffects.None, .399f);
                }
                if (Icon != null)
                    spriteBatch.Draw(Icon, IconDisplay, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .399f);
                if (EnemySprite != null)
                    spriteBatch.Draw(EnemySprite, SpriteDisplay, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects./*None*/FlipHorizontally, .399f);
                if (Description != null)
                    spriteBatch.DrawString(LoadGraphics.ImgFont, Description, new Vector2(DescriptionDisplay.X, DescriptionDisplay.Y - 3), Color.White, 0f, Vector2.Zero, 1f/*.5f*/, SpriteEffects.None, .399f);
                if (Stats != null)
                {
                    for (int i = 0; i < Stats.Length; i++)
                    {
                        if (Stats[i] != null)
                            spriteBatch.DrawString(LoadGraphics.ImgFont, Stats[i], new Vector2(StatDisplay.X, StatDisplay.Y + (i * 17)), Color.White, 0f, Vector2.Zero, 2f/*.6f*/, SpriteEffects.None, .399f);
                    }
                }
                if (Statuses != null)
                {
                    for (int i = 0; i < Statuses.Count; i++)
                    {
                        spriteBatch.Draw(Statuses[i], new Vector2(StatusDisplay.X + (14 * (i % 5)), StatusDisplay.Y + (13 * (i / 5))), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .399f);
                    }
                }
                if (Preferences != null)
                {
                    for (int i = 0; i < Preferences.Count; i++)
                    {
                        spriteBatch.Draw(Preferences[i], new Vector2(PrefersDisplay.X + (18 * (i % 4)), PrefersDisplay.Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .399f);
                    }
                }
                if (Moves != null)
                {
                    for (int i = 0; i < Moves.Count; i++)
                    {
                        if (Moves[i] != null)
                            spriteBatch.DrawString(LoadGraphics.ImgFont, Moves[i], new Vector2(MovesDisplay.X, MovesDisplay.Y - 4 + (i * 14)), Color.White, 0f, Vector2.Zero, 1f/*.5f*/, SpriteEffects.None, .399f);
                    }
                }
            }
        }
    }
}
