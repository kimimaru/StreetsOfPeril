﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The Obstacle Course challenge
    public class ObstacleH : Obstacle
    {
        //Stores the location the player was before going into the air; used for respawning the player
        private Vector3 SpawnLoc;

        public ObstacleH(Player player)
        {
            Players.Add(player);
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            Difficulty = 2;

            LevelData level = LevelEditor.LoadLevelOld("ObstacleH");

            TileEngine = level.TileEngine;

            //LevelCamera = new Camera(BG, FG, new LevelManager(level));
        }

        //Respawn the character where he or she was before going into the air
        public override Vector3 RespawnLoc()
        {
            return SpawnLoc;
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            //LevelCamera = new Camera(BG, FG, new LevelManager(LevelEditor.LoadLevelOld("ObstacleH")));

            LoadSounds.PlayChallengeMusic(0/*(int)CLevel.Challenges.Obstacle*/);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 70, 20, 0f, true);
                Players[i].ResetScore();
                Players[i].SetStats(null, null, 1, null, null, null);
            }
            LevelCamera.Update(this);
            SpawnLoc = new Vector3(Players[0].GetLocationVec2.X, Players[0].GetLocationVec2.Y, Players[0].CurHeight);
        }

        protected override void UpdateCamera(float activeTime)
        {
            //Update level camera - If the character is in the air, don't make the camera move since it can make the player get stuck when respawning
            if (Players[0].CurHeight == Players[0].ObjectTile.Z || Players[0].IsOnObject == true)
            {
                //Track where the character is and respawn him or her in that location if he or she loses a life
                SpawnLoc = new Vector3(Players[0].GetLocationVec2.X, Players[0].GetLocationVec2.Y, 280f);
                base.UpdateCamera(activeTime);
            }
        }
    }
}
