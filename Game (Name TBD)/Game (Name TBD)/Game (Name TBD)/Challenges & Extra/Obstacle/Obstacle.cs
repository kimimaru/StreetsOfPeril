﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The Obstacle Course challenge
    /*NOTE: This challenge and its other difficulties are SCRAPPED from the game because I have no idea what to do for Easy and Normal, Hard doesnn't seem very fun,
            and the assets and time required for them will just slow down development*/
    public class Obstacle : CLevel
    {
        //For when to spawn the projectiles
        protected List<Vector2> ProjectileOffsets;
        //Projectiles that can be shot at the player - Projectiles may be packed into other hazards so this may not be necessary
        protected List<Projectile> Projectiles;

        public Obstacle()
        {
            ChallengeNum = (int)Challenges.RPS;
            Difficulty = 0;
        }

        public Obstacle(Player player) : this()
        {
            Players.Add(player);
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            LevelData level = LevelEditor.LoadLevelOld("Obstacle");

            TileEngine = level.TileEngine;

            //LevelCamera = new Camera(BG, FG, new LevelManager(level));
            Projectiles = new List<Projectile>();
        }

        protected override bool IsCleared()
        {
            if (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false)
            {
                return true;
            }

            return false;
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            Projectiles.Clear();
            //LevelCamera = new Camera(BG, FG, new LevelManager(LevelEditor.LoadLevelOld("Obstacle")));

            LoadSounds.PlayChallengeMusic(0/*(int)CLevel.Challenges.Obstacle*/);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0f, true);
                Players[i].ResetScore();
                Players[i].SetStats(null, null, 3, null, null, null);
            }
            LevelCamera.Update(this);
        }

        private void UpdateProjectiles(float activeTime, Vector2 OffSet)
        {
            for (int i = 0; i < Projectiles.Count; i++)
            {
                Projectiles[i].Update();

                if (Projectiles[i].ShouldRemove == true)
                {
                    Projectiles.RemoveAt(i);
                    i--;
                }
            }
        }

        private void DrawProjectiles(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Projectiles.Count; i++)
                Projectiles[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
        }
    }
}
