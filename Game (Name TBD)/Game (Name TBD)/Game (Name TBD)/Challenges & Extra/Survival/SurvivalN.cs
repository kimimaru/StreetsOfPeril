﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The normal challenge for Survival - it's an underwater level, and every hit you take damages your oxygen tank
    public class SurvivalN : Survival
    {
        public delegate void PlayerTankHit(float activeTime, Status newstat, Player Player, Hitbox enemhitbox, bool hitboxdirection);
        public PlayerTankHit playerTankHit;

        //Tracks the player's health to see if the player got hit or not so you can make the oxygen tank get hurt as well
        //NOTE: Get rid of this because it clearly doesn't work and instead create a special character named SurvivalNGraham that has an overridden TakeDamage method that always subtracts OxygenTank health
        private float[] TempHealth;

        public SurvivalN()
        {
            Players.Clear();
            Players.Add(CreateObjects.SurvivalNGraham(0, 0));

            TempHealth = new float[Players.Count];

            Difficulty = 1;

            LevelData level = LevelEditor.LoadLevelOld("SurvivalN");

            TileEngine = level.TileEngine;
            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));//-1, ChallengeNum));
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(LevelEditor.LoadLevelOld("SurvivalN")));

            LoadSounds.PlayChallengeMusic(0);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0f, true);
                Players[i].ResetScore();
                Players[i].SetStats(null, null, 1, null, null, new Status());
                TempHealth[i] = Players[i].GetHealth;
            }
        }
    }
}
