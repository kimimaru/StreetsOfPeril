﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The survival challenge
    public class Survival : CLevel
    {
        //Constructor
        public Survival()
        {
            Players.Add(CreateObjects.Graham(0, 0));
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            ChallengeNum = (int)Challenges.Survival;
            Difficulty = 0;

            LevelData level = LevelEditor.LoadLevelOld("Survival");

            TileEngine = level.TileEngine;

            //LevelCamera = new Camera(BG, FG, new LevelManager(level));
        }

        protected override bool IsCleared()
        {
            if (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false)
            {
                return true;
            }

            return false;
        }

        //Restarts the challenge
        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            //LevelCamera = new Camera(BG, FG, new LevelManager(LevelEditor.LoadLevelOld("Survival")));

            LoadSounds.PlayChallengeMusic(0);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0, true);
                Players[i].ResetScore();
                Players[i].SetStats(null, null, 1, null, null, new Status((int)Status.Statuses.Poison, 1));
            }
        }

        protected override void CalculatePerformance(Main main)
        {
            Ranking ranking = CLevel.ChallengeRanks[ChallengeNum][Difficulty];

            //The subtotal of all the player scores shown before the other factors are taken into account
            int subtotalscore = 0;

            //The other values that influence the total score (damage taken, lives lost, etc.)
            int liveslost = 0;
            int healthremaining = 0;

            //Add all the individual player scores together and subtract the amount of lives and health lost
            for (int i = 0; i < Players.Count; i++)
            {
                subtotalscore += Players[i].Score;

                liveslost -= (1 - Players[i].GetLives) * 1000;

                //Subtract the character's max health from his/her current health
                healthremaining -= ((int)(BeatEmUpObj.MaxHealthInBar - Players[i].GetHealth)) * 10;
            }

            //The final score; the sum of the subtotal and all the influential factors
            int totalscore = subtotalscore + liveslost + healthremaining;

            //Get the current score/time of the challenge; if it changed after ranking the performance of the player(s), then it's a new record
            char rank = Ranking.LetterValue(ranking.GetCurrentPerformance(totalscore));
            char bestrank = Ranking.LetterValue(ranking.GetRank());
            float scoretime = ranking.GetScoreTime();
            Vector2 newrecordloc = Vector2.Zero;
            String newrecord = String.Empty;

            //Rank the average of all the players' performances
            ranking.RankPerformance(totalscore / Players.Count);
            ranking.RankScoreTime(totalscore);

            //If the previous best score isn't the same as the best score after ranking performance, it's a new record
            if (scoretime != CLevel.ChallengeRanks[ChallengeNum][Difficulty].GetScoreTime())
            {
                newrecordloc = new Vector2(133, 279);
                newrecord = "New Record!";
            }

            float X = 133;

            //Add the results screen, which shows the current and best rank, score/time, and whether there was a new record or not
            main.AddScreen(new ResultsScreen(null, "Survival" + " Results", null, new Vector2[] { new Vector2(X, 45), new Vector2(X, 70), new Vector2(X, 95), new Vector2(X, 120), new Vector2(X, 145), new Vector2(X, 194), new Vector2(X, 219), new Vector2(X, 244), newrecordloc },
                "Subtotal: " + subtotalscore, "Lives Lost: " + liveslost, "Health Remaining: " + healthremaining, "Time Bonus: " + (0 * 100), "Final Score: " + totalscore, "Rank: " + rank, "Best Rank: " + bestrank, "Best Score: " + scoretime, newrecord));

            //Save the data
            SaveLoadData.SaveChallenge(ranking, ChallengeNum, Difficulty);
        }
    }
}
