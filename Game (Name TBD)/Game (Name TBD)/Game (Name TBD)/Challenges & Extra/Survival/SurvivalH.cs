﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The hard challenge for Survival - one hit and you're dead!
    public class SurvivalH : Survival
    {
        //Constructor
        public SurvivalH()
        {
            Players.Clear();
            Players.Add(CreateObjects.Graham(0, 0));
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            Difficulty = 2;

            LevelData level = LevelEditor.LoadLevelOld("SurvivalH");

            TileEngine = level.TileEngine;

            //LevelCamera = new Camera(BG, FG, new LevelManager(level));
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(LevelEditor.LoadLevelOld("SurvivalH")));

            LoadSounds.PlayChallengeMusic(1);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0, true);
                Players[i].ResetScore();
                Players[i].SetStats(1, 0, 1, null, null, new Status());
            }
        }
    }
}
