﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The Rock, Paper, Status challenge
    //Damage beats Defense, Defense beats Speed, and Speed beats Damage; the Boost/Downs are Super (Dis)Advantageous against the opposite of the status they beat/lose to
    public class RPS : CLevel
    {
        //The values corresponding to each type of interaction
        protected enum DamageValues
        {
            SuperDisadvantageous = -2, Disadvantageous = -1, Neutral = 0, Advantageous = 1, SuperAdvantageous = 2
        }

        //The statuses the player can cycle through
        protected Status[] CycleStatuses;

        //The advantage values of each status
        protected Advantage[] Advantages;

        public RPS()
        {
            ChallengeNum = (int)Challenges.RPS;
            Difficulty = 0;

            CycleStatuses = new Status[6];
            for (int i = 0; i < CycleStatuses.Length; i++)
                CycleStatuses[i] = new Status((i + 1), Status.InfiniteStatus, 0);

            //Make the first one for None, even though all enemies should have statuses, so it's easier to access the status (not having to subtract 1 from the condition)
            Advantages = new Advantage[] { new Advantage((int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Neutral), 
                                           new Advantage((int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Advantageous, (int)DamageValues.SuperAdvantageous, (int)DamageValues.Disadvantageous, (int)DamageValues.SuperDisadvantageous),
                                           new Advantage((int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.SuperAdvantageous, (int)DamageValues.Advantageous, (int)DamageValues.SuperDisadvantageous, (int)DamageValues.Disadvantageous),
                                           new Advantage((int)DamageValues.Disadvantageous, (int)DamageValues.SuperDisadvantageous, (int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.Advantageous, (int)DamageValues.SuperAdvantageous),
                                           new Advantage((int)DamageValues.SuperDisadvantageous, (int)DamageValues.Disadvantageous, (int)DamageValues.Neutral, (int)DamageValues.Neutral, (int)DamageValues.SuperAdvantageous, (int)DamageValues.Advantageous),
                                           new Advantage((int)DamageValues.Advantageous, (int)DamageValues.SuperAdvantageous, (int)DamageValues.Disadvantageous, (int)DamageValues.SuperDisadvantageous, (int)DamageValues.Neutral, (int)DamageValues.Neutral),
                                           new Advantage((int)DamageValues.SuperAdvantageous, (int)DamageValues.Advantageous, (int)DamageValues.SuperDisadvantageous, (int)DamageValues.Disadvantageous, (int)DamageValues.Neutral, (int)DamageValues.Neutral) };
        }

        public RPS(Player player) : this()
        {
            Players.Add(player);
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            LoadData(LevelEditor.LoadLevel("RPS"));
        }

        protected override bool IsCleared()
        {
            if (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false)
            {
                return true;
            }

            return false;
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);

            LoadData(LevelEditor.LoadLevel("RPS"));

            LoadSounds.PlaySong((int)LoadSounds.Music.Chal4, true);
        }

        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0f, true, false);
                Players[i].SetStats(null, null, null, null, null, new Status(CycleStatuses[0]));
                Players[i].ResetScore();
            }
        }

        //The true amount of damage the victim should receive; we have to do this to manipulate the damage values the Defense Boost/Down modify so Super (Dis)Advantageous attacks always do more damage
        protected void TrueDamageReceived(Player player, float activeTime, bool FacingLeft, int damage, Status enemystat, Hitbox enembox, TileEngine TileEngine, List<BeatEmUpObj> Solids)
        {
            if (player.status == (int)Status.Statuses.DefenseBoost) damage *= 2;
            else if (player.status == (int)Status.Statuses.DefenseDown) damage /= 2;

            //player.TakeDamage(activeTime, FacingLeft, damage, enemystat, enembox, TileEngine, Solids);
        }

        protected void TrueDamageReceived(Enemy enemy, float activeTime, bool FacingRight, Player player, Player lastattacked, int damage, Status weaponstat, Hitbox playerbox, TileEngine TileEngine)
        {
            if (enemy.status == (int)Status.Statuses.DefenseBoost) damage *= 2;
            else if (enemy.status == (int)Status.Statuses.DefenseDown) damage /= 2;

            //enemy.GetHurt(activeTime, FacingRight, player, lastattacked, damage, weaponstat, playerbox, TileEngine);
        }

        //This is where we determine if the player or enemy gets hurt after damaging one another
        protected int StatusAdvantage(Status attacker, Status victim)
        {
            //If the attacker doesn't have one of the 6 main status effects for whatever reason (Ex. respawn invincibility), it is automatically advantageous
            //If the victim doesn't have one of the 6 main status effects for whatever reason, it is automatically disadvantageous
            //Otherwise, return the normal advantage values
            if (attacker.GCond < (int)Status.Statuses.DamageBoost || attacker.GCond > (int)Status.Statuses.SpeedDown) return (int)DamageValues.Advantageous;
            else if (victim.GCond < (int)Status.Statuses.DamageBoost || victim.GCond > (int)Status.Statuses.SpeedDown) return (int)DamageValues.Disadvantageous;
            else return (Advantages[attacker.GCond].AdvantageValue(victim));
        }

        //Since throws usually have special damage values, if there's a Neutral or Disadvantageous case, change the throw hitbox into a normal one
        /*protected void RPSPlayerEnemyCollisions(float activeTime)
        {
            //Enemy and player damage collisions
            for (int p = 0; p < Players.Count; p++)
            {
                for (int i = 0; i < EnemList.Count; i++)
                {
                    //Check if player hits enemy
                    /*for (int h = 0; h < Players[p].GetHitboxes.Count; h++)
                    {
                        //Make the enemy's tank take damage if the player hit it
                        if (EnemList[i].GetOxygenTank != null && EnemList[i].GetOxygenTank.CanBreak(activeTime, Players[p].GetHitboxes[h]) == true)
                        {
                            //If you hit an enemy's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                            //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);

                            EnemList[i].GetOxygenTank.Break(activeTime, 1, Players[p].GetHitboxes[h], TileEngine);

                            //Make the player experience hitlag for hitting the enemy's tank
                            Players[p].EnterHitLag();
                        }

                        if (EnemList[i].CanGetHit(Players[p], Players[p].GetHitboxes[h]) == true)
                        {
                            //If you hit an enemy, reset the previous HUD so it doesn't show up so you can draw the new one
                            //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);

                            int advantage = StatusAdvantage(Players[p].GetStatus, EnemList[i].GetStatus);
                            int damage = Players[p].GetDamage + Players[p].GetHitboxes[h].Strength;

                            //Check if an enemy is grabbed by a character (or carried by Jeff) and do the appropriate action depending on the state of the character
                            if (EnemList[i] == Players[p].GEnem)
                            {
                                if (advantage <= (int)DamageValues.Neutral) Players[p].GetHitboxes[h].Throw = false;

                                //Perform a throw or throw-like move on the enemy
                                if (Players[p].GetHitboxes[h].Throw == true)
                                {
                                    damage *= advantage;

                                    if (EnemList[i].GetStatus == (int)Status.Statuses.DefenseBoost) damage *= 2;
                                    else if (EnemList[i].GetStatus == (int)Status.Statuses.DefenseDown) damage /= 2;

                                    Players[p].RPSThrowEnemy(activeTime, damage, Players[p].GetHitboxes[h], TileEngine);
                                    Players[p].EnterHitLag();

                                    Players[p].AddScore(Players[p].GetHitboxes[h].Score);
                                }
                                //Get hit by a grab attack
                                else
                                {
                                    if (advantage <= (int)DamageValues.Disadvantageous)
                                    {
                                        Hitbox hitbox = Players[p].GetHitboxes[h];
                                        hitbox.HitSound = Players[p].GetHitboxes[h].HitSound;
                                        if (hitbox.HitSound == null) hitbox.HitSound = LoadSounds.Kick;

                                        if (advantage == (int)DamageValues.SuperDisadvantageous)
                                        {
                                            damage *= 2;
                                            hitbox.KnockDown = true;
                                        }
                                        TrueDamageReceived(Players[p], activeTime, Players[p].GetFacingDir, damage, new Status(), hitbox, TileEngine, Solids);
                                    }
                                    else if (advantage == (int)DamageValues.Neutral)
                                    {
                                        if (Players[p].GetHitboxes[h].HitSound == null) Players[p].GetHitboxes[h].HitSound = LoadSounds.Kick;

                                        TrueDamageReceived(EnemList[i], activeTime, Players[p].GetHitboxes[h].HitDirection, Players[p], Players[p], damage, new Status(), Players[p].GetHitboxes[h], TileEngine);
                                        TrueDamageReceived(Players[p], activeTime, Players[p].GetFacingDir, damage, new Status(), Players[p].GetHitboxes[h], TileEngine, Solids);
                                    }
                                    else if (advantage >= (int)DamageValues.Advantageous)
                                    {
                                        damage *= advantage;

                                        TrueDamageReceived(EnemList[i], activeTime, Players[p].GetHitboxes[h].Direction, Players[p], Players[p], damage, new Status(), Players[p].GetHitboxes[h], TileEngine);

                                        //Make the player experience hitlag for hitting the enemy
                                        Players[p].EnterHitLag();

                                        //Give the player points for hitting enemies
                                        Players[p].AddScore(Players[p].GetHitboxes[h].Score);
                                    }
                                }
                            }
                            else if (Players[p].GetHitboxes[h].Throw == false)
                            {
                                if (advantage <= (int)DamageValues.Disadvantageous)
                                {
                                    if (Players[p].GIsKnockedDown == false)
                                    {
                                        Hitbox hitbox = Players[p].GetHitboxes[h];
                                        if (advantage == (int)DamageValues.SuperDisadvantageous)
                                        {
                                            damage *= 2;
                                            hitbox.KnockDown = true;
                                        }
                                        TrueDamageReceived(Players[p], activeTime, Players[p].GetFacingDir, damage, new Status(), hitbox, TileEngine, Solids);
                                    }
                                }
                                else if (advantage == (int)DamageValues.Neutral)
                                {
                                    TrueDamageReceived(EnemList[i], activeTime, Players[p].GetHitboxes[h].Direction, Players[p], Players[p], damage, new Status(), Players[p].GetHitboxes[h], TileEngine);
                                    if (Players[p].GIsKnockedDown == false) TrueDamageReceived(Players[p], activeTime, Players[p].GetFacingDir, damage, new Status(), Players[p].GetHitboxes[h], TileEngine, Solids);
                                    else Players[p].EnterHitLag();
                                }
                                else if (advantage >= (int)DamageValues.Advantageous)
                                {
                                    damage *= advantage;

                                    TrueDamageReceived(EnemList[i], activeTime, Players[p].GetHitboxes[h].Direction, Players[p], Players[p], damage, new Status(), Players[p].GetHitboxes[h], TileEngine);

                                    //Tank will endlessly parry himself to death unless stopped, so stop only his action after this occurs
                                    EnemList[i].OverrideActions();

                                    //Make sure it doesn't increase the hit counter if you're hitting with something other than standard attacks
                                    Players[p].SetNextAttack();

                                    //Make the player experience hitlag for hitting the enemy
                                    Players[p].EnterHitLag();

                                    //Give the player points for hitting enemies
                                    Players[p].AddScore(Players[p].GetHitboxes[h].Score);
                                }
                            }
                        }
                    }

                    //Check if enemy hits another enemy (specific attacks only, like throws)
                    if (EnemList.Count > 1)
                    {
                        for (int j = 0; j < EnemList.Count; j++)
                        {
                            if (EnemList[i].gIsKnockedDown == false) break;

                            //Make sure the enemy checking to be hit isn't the same as the one with the hitbox
                            if (j != i && EnemList[j].CanGetHit(EnemList[i], EnemList[i].gHitbox) == true)
                            {
                                //Check for the enemy's status; if the enemy is dead, we use the status the enemy had before dying
                                Status checkstat = new Status(EnemList[i].GetStatus);
                                if (EnemList[i].IsDead == true) checkstat = new Status(EnemList[i].DeathStatus, checkstat.GStatusDur, checkstat.GPercentage);

                                int advantage = StatusAdvantage(checkstat, EnemList[j].GetStatus);
                                int damage = EnemList[i].GetDamage + EnemList[i].gHitbox.Strength;

                                //The thrown enemy wouldn't be able to hit the other one unless the advantage is Neutral or greater
                                //Disadvantageous isn't considered because the thrown enemy is invincible
                                if (advantage >= (int)DamageValues.Neutral)
                                {
                                    if (advantage == (int)DamageValues.SuperDisadvantageous) damage *= 2;

                                    TrueDamageReceived(EnemList[j], activeTime, EnemList[i].gHitbox.Direction, null, EnemList[i].gLastAttacked, damage, new Status(), EnemList[i].gHitbox, TileEngine);

                                    //If an enemy hits another enemy, reset the previous HUD, if it belonged to a player, so it doesn't show up so you can draw the new one
                                    //if (EnemList[i].gLastAttacked != null)
                                        //HUD.ChangeHUD(activeTime, EnemList[i].gLastAttacked.GPlayerNum, EnemList[j].GetHUD);

                                    //Make the enemy experience hitlag for hitting the other enemy
                                    EnemList[i].SetHitLag(activeTime);
                                }
                            }
                        }
                    }

                    //Check if the enemy hits the player's tank
                    if (Players[p].GetOxygenTank != null && Players[p].GetOxygenTank.CanBreak(activeTime, EnemList[i].gHitbox) == true)
                    {
                        //If an enemy hits a player's oxygen tank, reset the previous HUD so it doesn't show up so you can draw the new one
                        //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);

                        Players[p].GetOxygenTank.Break(activeTime, 1, EnemList[i].gHitbox, TileEngine);

                        //Make the enemy experience hitlag for hitting the player's tank
                        EnemList[i].SetHitLag(activeTime);
                    }

                    //Check if enemy hits player
                    if (Players[p].CanGetHit(Players[p].CollisionBox, EnemList[i].gHitbox) == true)
                    {
                        int advantage = StatusAdvantage(EnemList[i].GetStatus, Players[p].GetStatus);
                        int damage = EnemList[i].GetDamage + EnemList[i].gHitbox.Strength;

                        if (advantage <= (int)DamageValues.Neutral) EnemList[i].gHitbox.Throw = false;

                        //Throw the player
                        if (EnemList[i].gHitbox.Throw == true)
                        {
                            damage *= advantage;

                            if (Players[p].GetStatus == (int)Status.Statuses.DefenseBoost) damage *= 2;
                            else if (Players[p].GetStatus == (int)Status.Statuses.DefenseDown) damage /= 2;

                            EnemList[i].RPSThrowPlayer(activeTime, Players[p], damage, this);
                            EnemList[i].SetHitLag(activeTime);
                        }
                        //Hit the player with a normal attack
                        else
                        {
                            Status newstat = new Status();
                            if (EnemList[i].gIsKnockedDown == false && EnemList[i].GetStatus != (int)Status.Statuses.Invincible)
                                newstat = new Status(EnemList[i].GetStatus.GCond, EnemList[i].GetStatus.GStatusDur, EnemList[i].GetStatus.GPercentage);

                            if (advantage <= (int)DamageValues.Disadvantageous)
                            {
                                if (EnemList[i].gIsKnockedDown == false)
                                {
                                    Hitbox hitbox = EnemList[i].gHitbox;
                                    if (advantage == (int)DamageValues.SuperDisadvantageous)
                                    {
                                        damage *= 2;
                                        hitbox.KnockDown = true;
                                    }
                                    TrueDamageReceived(EnemList[i], activeTime, hitbox.Direction, Players[p], Players[p], damage, newstat, hitbox, TileEngine);

                                    //Tank will endlessly parry himself to death unless stopped, so stop only his action after this occurs
                                    EnemList[i].OverrideActions();
                                }
                            }
                            else if (advantage == (int)DamageValues.Neutral)
                            {
                                bool direction = EnemList[i].gHitbox.Direction;
                                if (EnemList[i].gIsKnockedDown == true) direction = !direction;

                                TrueDamageReceived(Players[p], activeTime, direction, damage, newstat, EnemList[i].gHitbox, TileEngine, Solids);
                                if (EnemList[i].gIsKnockedDown == false) TrueDamageReceived(EnemList[i], activeTime, EnemList[i].GetFacingDir, Players[p], Players[p], damage, new Status(), EnemList[i].gHitbox, TileEngine);
                                else EnemList[i].SetHitLag(activeTime);
                            }
                            else if (advantage >= (int)DamageValues.Advantageous)
                            {
                                damage *= advantage;
                                TrueDamageReceived(Players[p], activeTime, EnemList[i].gIsKnockedDown == false ? EnemList[i].gHitbox.Direction : !EnemList[i].gHitbox.Direction, damage, newstat, EnemList[i].gHitbox, TileEngine, Solids);

                                //Make the enemy experience hitlag for hitting the player
                                EnemList[i].SetHitLag(activeTime);
                            }
                        }

                        //If an enemy hits the player, reset the previous HUD so it doesn't show up so you can draw the new one
                        //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);
                    }
                }
            }
        }*/

        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                //Change statuses if the player presses the Item key
                if (Players[i].CheckCanPickUp() == true)
                {
                    int newstatus = Players[i].status.GCond + 1;
                    if (newstatus > (int)Status.Statuses.SpeedDown) newstatus = (int)Status.Statuses.DamageBoost;
                    Players[i].InflictStatus(CycleStatuses[newstatus - 1]);

                    //The player enters the pickup animation upon switching since this makes him/her act more like Changer (has lag and prevents the player from spamming it)
                    //Players[i].PickUp(null, null, this);
                }

                Players[i].Update();

                if (Players[i].IsDead == true && Players[i].KnockedDown == false)
                {
                    Players[i].Respawn(activeTime, RespawnLoc(), TileEngine);

                    //Change the player's status to DamageBoost instead of Invincibility, which greatly complicates things (Ex. Invincibility wearing off)
                    Players[i].InflictStatus(CycleStatuses[(int)Status.Statuses.DamageBoost]);
                    if (Players[i].IsCompletelyDead == true)
                    {
                        Players.RemoveAt(i);
                        i--;

                        //If all players are completely dead then end the challenge
                        if (Players.Count == 0) Fade.ContinueFade();
                    }
                }
            }
        }

        //The base class that specifies what each status has an advantage or disadvantage over
        protected class Advantage
        {
            //The advantage values
            protected int[] AdvantageValues;

            public Advantage()
            {
                AdvantageValues = new int[6];
            }

            //Constructor; there should be exactly 6 values
            public Advantage(params int[] values)
            {
                AdvantageValues = values;
            }

            //Gets the advantage value of a status
            public int AdvantageValue(Status opposingstatus)
            {
                return (AdvantageValues[opposingstatus.GCond - 1]);
            }

            //Tells if one Status is advantageous against another
            public bool IsAdvantageous(Status opposingstatus)
            {
                return (AdvantageValues[opposingstatus.GCond - 1] >= (int)DamageValues.Advantageous);
            }

            //Tells if one Status is disadvantageous against another
            public bool IsDisadvantageous(Status opposingstatus)
            {
                return (AdvantageValues[opposingstatus.GCond - 1] <= (int)DamageValues.Disadvantageous);
            }
        }
    }
}
