﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The Break The Objects challenge
    //Make Normal or Hard have targets that alternate between normal and invincible, requiring you to time your bullets more precisely, and include platforms!!!
    //Record the time for this (and some other challenges as well) and use it as the score
    public class BTO : CLevel
    {
        //Constructor
        public BTO(Texture2D background, Texture2D foreground)
        {
            Players.Add(CreateObjects.Wil(0, 0));
            BG = background;
            FG = foreground;

            ChallengeNum = (int)Challenges.BTO;
            Difficulty = 0;

            RestartChallenge(Main.GetActiveTime);
        }

        protected override bool IsCleared()
        {
            if (CheckBrokenContainers() == true)
            {
                return true;
            }

            return false;
        }

        private bool CheckBrokenContainers()
        {
            for (int i = 0; i < ContainerList.Count; i++)
            {
                if (ContainerList[i].IsDead == false) return false;
            }

            return true;
        }

        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
            else if (Fade.IsFading() == false && Fade.IsFadingIn() == false) PrevTime = activeTime + 15000f;
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);

            Time = 15f;
            PrevTime = 0f;

            LoadData(LevelEditor.LoadLevel("BTO"));

            LoadSounds.PlayChallengeMusic(3/*(int)CLevel.Challenges.BTO*/);
        }

        protected override void SetSpawnStats(Player player)
        {
            player.Spawn(this);
            player.SpawnSub(Main.GetActiveTime, TileEngine, StartLocations[player.GPlayerNum].X + StartingOffset.X, StartLocations[player.GPlayerNum].Y + StartingOffset.Y, StartLocations[player.GPlayerNum].Z, true, false);
            player.SetStats(null, null, 1, null, null, new Status((int)Status.Statuses.InfBullets, 1f, 0));
        }

        //Ranks the player based on how fast all the targets were broken
        protected override void CalculatePerformance(Main main)
        {
            Ranking ranking = CLevel.ChallengeRanks[ChallengeNum][Difficulty];

            //Get the current score/time of the challenge; if it changed after ranking the performance of the player(s), then it's a new record
            char rank = Ranking.LetterValue(ranking.GetCurrentPerformance(Time));
            char bestrank = Ranking.LetterValue(ranking.GetRank());
            float scoretime = ranking.GetScoreTime();
            Vector2 newrecordloc = Vector2.Zero;
            String newrecord = String.Empty;

            //Rank the players performance
            ranking.RankPerformance(Time);
            ranking.RankScoreTime(Time);

            //If the previous best score isn't the same as the best score after ranking performance, it's a new record
            if (scoretime != ranking.GetScoreTime())
            {
                newrecordloc = new Vector2(155, 250);
                newrecord = "New Record!";
            }

            main.AddScreen(new ResultsScreen(null, "Break The Objects Results", null, new Vector2[] { new Vector2(25, 100), new Vector2(233, 100), new Vector2(25, Main.ScreenHalf.Y + 10), new Vector2(233, Main.ScreenHalf.Y + 10), newrecordloc }, 
                "Your Rank: " + rank, "Best Rank: " + bestrank, "Your Time: " + String.Format("{0:0.00}", Time), "Best Time: " + String.Format("{0:0.00}", scoretime), newrecord));

            //Save the data
            SaveLoadData.SaveChallenge(ranking, ChallengeNum, Difficulty);
        }

        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            base.UpdatePlayers(activeTime, false);
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            UpdateTimer(activeTime);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            //Draw time
            DrawTimer(spriteBatch);

            spriteBatch.Draw(BG, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .00001f);
        }
    }
}
