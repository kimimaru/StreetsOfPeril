﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Versus mode
    //NOTE: Use inheritance for Versus mode! It's way too big right now; subclassing will definitely help support all these game modes
    //NOTE: Indicate to the players that they can control the VersusEnemy in the Avoid Enemy mode when they have enough SwitchKeys, and show victory animations when a team/player wins
    public class Versus : CLevel
    {
        //The minimum number of Switch Keys required to control the Versus Enemy
        public const int MinSwitchKeysControl = 5;

        //Win and lose text for characters (individual and teams)
        private static String[][][] WinDialogue;
        private static String[][][] LoseDialogue;
        private static String[][] FFAWinDialogue;
        private static String[][] FFALoseDialogue;
        private static String[][] TeamsWinDialogue;
        private static String[][] TeamsLoseDialogue;

        private Texture2D Background;

        //Temp list of players to use for updating the players; we need the normal list to keep player data stored to decide the win condition and display it on the results screen
        private List<Player> TempPlayers;

        //Tells if the timer should be reset when a player has been killed
        private bool KillReset;

        private bool TeamAttack;
        private int[] TeamColors;

        //Tells which team or player won the round
        private int TeamPlayerWon;

        //The game mode (Standard, Avoid Enemy, Score)
        private int GameMode;

        //The status players start and spawn with
        private Status[] SpawnStatus;

        //Tracks when a player just respawned so the player's status can be set after invincibility wears off
        private bool[] Respawned;

        //Avoid Enemy mode objects only
        private float PrevSwitch;

        //The damage ratio (from .5 to 2) - multiplies damage done by a specific amount (rounding up)
        private float DamageRatio;

        //Score mode objects only
        private int[] LastHit;
        private int ScoreLimit;

        //The versus enemy
        private VersusEnemy VersusEnemy;

        //Depending on the level number chosen, the players can play underwater
        public Versus(Texture2D background, int time, float damageratio, int scorelimit, int Mode, int level, bool teamattack, int[] teamcolors, Status[] spawnstatus, params Player[] players)
        {
            Background = background;
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;
            
            Time = time;
            PrevTime = 0f;

            KillReset = false;
            TeamPlayerWon = -1;

            Restartable = false;

            TeamAttack = teamattack;
            DamageRatio = damageratio;
            ScoreLimit = scorelimit;
            GameMode = Mode;
            SpawnStatus = spawnstatus;

            Respawned = new bool[players.Length];

            Players.AddRange(players);

            TempPlayers = new List<Player>();
            TempPlayers.AddRange(players);

            if (teamcolors != null)
                TeamColors = teamcolors;

            //if (level == 1) TileEngine = LevelEditor.LoadLevelOld("VersusShallow").TileEngine;
            //else if (level == 6)
            //{
            //    TileEngine = LevelEditor.LoadLevelOld("VersusWater").TileEngine;
            //    for (int i = 0; i < Players.Count; i++)
            //    {
            //        Texture2D oxygentankicon = OxygenTank.GetOxygenTankColor(Players[i].GPlayerNum);
            //        if (TeamColors != null) 
            //            oxygentankicon = OxygenTank.GetOxygenTankColorTeam(TeamOfPlayer(Players[i]));
            //
            //        //Players[i].GetOxygenTank = new OxygenTank(LoadGraphics.FallingRock, oxygentankicon, Players[i], null);
            //    }
            //}
            //else TileEngine = LevelEditor.LoadLevelOld("Versus").TileEngine;

            //The enemy if Avoid Enemy is selected
            if (GameMode == (int)VersusScreen.GameModes.Avoid)
            {
                VersusEnemy = new VersusEnemy();
                EnemList.Add(VersusEnemy);
                //VersusEnemy.GetObjectTile = TileEngine.CurTile(VersusEnemy.FeetLoc());
                PrevSwitch = 0f;
            }
            //Check who last hit who in Score mode
            else if (GameMode == (int)VersusScreen.GameModes.Score)
            {
                LastHit = new int[4] { 0, 1, 2, 3 };
            }

            SublevelProp levelprop = level != 6 ? LevelEditor.LoadLevel("Versus") : LevelEditor.LoadLevel("VersusWater");

            LoadData(levelprop);
            //ContainerList.Add(CreateObjects.Barrel(new Vector3(250, 200, 0f), 1, null, null));
            //Solids.Add(ContainerList[0]);
        }

        //Three different types of dialogues: 1v1, FFA, and Teams
        //Four win and lose dialogues per character for each, barring 1v1
        //1v1: Each character has 4 lines of win and lose dialogue for each other character (Ex. Graham winning to Wil is 1), totaling 16 lines of dialogue for each character
        //Example 1v1 access: WinDialogue[IndexOfCharacter(Players[i])][IndexOfCharacter(Players[(i + 1) % 2])][rand.Next(4)]
        //Example FFA and Teams access: WinDialogue[IndexOfCharacter(Players[i])][rand.Next(4)]
        static Versus()
        {
            WinDialogue = new String[][][] {
                //Graham winning against himself, Wil, Crystal, and Jeff
                new String [][]
                {
                    new String[] { "G0", "G1", "G2", "G3" },
                    new String[] { "W0", "W1", "W2", "W3" },
                    new String[] { "C0", "C1", "C2", "C3" },
                    new String[] { "J0", "J1", "J2", "J3" }
                },
                //Wil winning against Graham, himself, Crystal, and Jeff
                new String[][]
                {
                    new String[] { "G0", "G1", "G2", "G3" },
                    new String[] { "W0", "W1", "W2", "W3" },
                    new String[] { "C0", "C1", "C2", "C3" },
                    new String[] { "J0", "J1", "J2", "J3" }
                },
                //Crystal winning against Graham, Wil, herself, and Jeff
                new String[][]
                {
                    new String[] { "G0", "G1", "G2", "G3" },
                    new String[] { "W0", "W1", "W2", "W3" },
                    new String[] { "C0", "C1", "C2", "C3" },
                    new String[] { "J0", "J1", "J2", "J3" }
                },
                //Jeff winning against Graham, Wil, Crystal, and himself
                new String[][]
                {
                    new String[] { "G0", "G1", "G2", "G3" },
                    new String[] { "W0", "W1", "W2", "W3" },
                    new String[] { "C0", "C1", "C2", "C3" },
                    new String[] { "J0", "J1", "J2", "J3" }
                }
            };

            LoseDialogue = new String[][][] {
                //Graham losing against himself, Wil, Crystal, and Jeff
                new String [][]
                {
                    new String[] { "GL0", "GL1", "GL2", "GL3" },
                    new String[] { "WL0", "WL1", "WL2", "WL3" },
                    new String[] { "CL0", "CL1", "CL2", "CL3" },
                    new String[] { "JL0", "JL1", "JL2", "JL3" }
                },
                //Wil losing against Graham, himself, Crystal, and Jeff
                new String[][]
                {
                    new String[] { "GL0", "GL1", "GL2", "GL3" },
                    new String[] { "WL0", "WL1", "WL2", "WL3" },
                    new String[] { "CL0", "CL1", "CL2", "CL3" },
                    new String[] { "JL0", "JL1", "JL2", "JL3" }
                },
                //Crystal losing against Graham, Wil, herself, and Jeff
                new String[][]
                {
                    new String[] { "GL0", "GL1", "GL2", "GL3" },
                    new String[] { "WL0", "WL1", "WL2", "WL3" },
                    new String[] { "CL0", "CL1", "CL2", "CL3" },
                    new String[] { "JL0", "JL1", "JL2", "JL3" }
                },
                //Jeff losing against Graham, Wil, Crystal, and himself
                new String[][]
                {
                    new String[] { "GL0", "GL1", "GL2", "GL3" },
                    new String[] { "WL0", "WL1", "WL2", "WL3" },
                    new String[] { "CL0", "CL1", "CL2", "CL3" },
                    new String[] { "JL0", "JL1", "JL2", "JL3" }
                }
            };

            //WinDialogue = new String[4] { "You must believe in yourself \nif you ever want to defeat me.", "You want more of this!? \nHeh, I thought so.", "I won! I bet you \nunderestimated me, huh?", "Ahh, good fight! \nUnfortunately, there can be only one victor." };
            //LoseDialogue = new String[4] { "You have bested me in battle...\nthe victory was well earned.", "Ouch, that hurt...", "HEY PUNK! \nDon't think you won just yet!", "Next time, I'll win..." };

            TeamsWinDialogue = new String[][] { 
                //Graham
                new String[] { "It looks like our team is stronger!", "Working together to achieve victory is the \ngreatest thing a team can do.", "3", "4"},//, "Victory is ours!", "It didn't work the first time, so it won't work again!", "Nice try!" }, 
                //Wil
                new String[] { "Victory is ours!", "Close one! Don't fool yourselves \ninto thinking you have a better chance of winning now.", "Together, we are unstoppable!", "4" },  
                //Crystal
                new String[] { "Step it up!", "Do you think you had a chance?", "How does it feel to be defeated?", "It didn't work the first time \nso it won't work again!" }, 
                //Jeff
                new String[] { "Haha, your fancy tactics won't work against us!", "That was all me!", "Nice try!", "4" } 
            };

            TeamsLoseDialogue = new String[][] { 
                new String[] { "Apparently we're the underdogs this time around...", "They say those that have \nbeen defeated have more to learn.", "L3", "L4" } , 
                new String[] { "Drat! Let's get them next time!", "Well, you can't win them all I guess.", "How could we lose to a strategy like that!?", "L4" }, 
                new String[] { "I did all the work! \nMy teammates were just slacking!", "...", "Lucky hit.", "L4" }, 
                new String[] { "It looks like we're in trouble...", "Well team...?", "I can't believe we lost...", "L4" } 
            };

            FFAWinDialogue = new String[][] { 
                //Graham
                new String[] { "1", "2", "3", "4"},
                //Wil
                new String[] { "1", "2", "3", "4" },  
                //Crystal
                new String[] { "1", "2", "3", "4" }, 
                //Jeff
                new String[] { "1", "2", "3", "4" } 
            };

            FFALoseDialogue = new String[][] { 
                //Graham
                new String[] { "L1", "L2", "L3", "L4"},
                //Wil
                new String[] { "L1", "L2", "L3", "L4" },  
                //Crystal
                new String[] { "L1", "L2", "L3", "L4" }, 
                //Jeff
                new String[] { "L1", "L2", "L3", "L4" } 
            };
        }

        //Gets the color of a team in Versus mode
        public static Color TeamColor(int team)
        {
            switch (team)
            {
                case 0: return Color.Red;
                case 1: return Color.Green;
                default: return Color.Blue;
            }
        }

        //Gets the color of the team a particular player is on
        private Color TeamColorOfPlayer(Player player)
        {
            return TeamColor(TeamOfPlayer(player));
        }

        //Gets the team of a particular player
        private int TeamOfPlayer(Player player)
        {
            return TeamColors[player.GPlayerNum];
        }

        private bool FFAComplete()
        {
            //Standard Battle and Avoid Enemy
            if (GameMode != (int)VersusScreen.GameModes.Score)
            {
                //Check which player is still alive
                if (Time > 0)
                {
                    for (int i = 0; i < Players.Count; i++)
                        TeamPlayerWon = Players[i].GPlayerNum;

                    return (Players.Count <= 1);
                }
                //If the time is up, check who has the most lives, health, and healthbars
                else
                {
                    if (Time < 0) return false;

                    int highestlives = -1;
                    int highesthealthbars = -1;
                    float highesthealth = -1f;

                    for (int i = 0; i < Players.Count; i++)
                    {
                        //Check if the player has more lives than the highest found
                        if (Players[i].GetLives > highestlives)
                        {
                            TeamPlayerWon = i;
                            highestlives = Players[i].GetLives;
                            highesthealthbars = Players[i].HealthBars;
                            highesthealth = Players[i].GetHealth;
                        }
                        else if (Players[i].GetLives == highestlives)
                        {
                            //Check if the player has more health bars than the highest found
                            if (Players[i].HealthBars > highesthealthbars)
                            {
                                TeamPlayerWon = i;
                                highesthealthbars = Players[i].HealthBars;
                                highesthealth = Players[i].GetHealth;
                            }
                            //If the player has the same amount of health bars, check if the player has more health than the highest found max
                            else if (Players[i].HealthBars == highesthealthbars && Players[i].GetHealth > highesthealth)
                            {
                                TeamPlayerWon = i;
                                highesthealth = Players[i].GetHealth;
                            }
                        }
                    }

                    return true;
                }
            }
            //Score mode
            else
            {
                //Check if someone reached the score limit
                if (ScoreLimit > 0)
                {
                    for (int i = 0; i < Players.Count; i++)
                    {
                        if (Players[i].Score >= ScoreLimit)
                        {
                            TeamPlayerWon = i;
                            return true;
                        }
                    }
                }

                //If the time is up, check who has the highest score
                if (Time == 0)
                {
                    int highestscore = -999;
                    for (int i = 0; i < Players.Count; i++)
                    {
                        if (Players[i].Score > highestscore)
                        {
                            TeamPlayerWon = i;
                            highestscore = Players[i].Score;
                        }
                    }

                    return true;
                }

                return false;
            }
        }

        private bool TeamsComplete()
        {
            //Standard Battle and Avoid Enemy
            if (GameMode != (int)VersusScreen.GameModes.Score)
            {
                if (Time > 0)
                {
                    int teamcompare = -1;

                    //Go through all the other players and check which team each one is on; if there's someone on another team left, the game isn't done yet
                    for (int i = 0; i < Players.Count; i++)
                    {
                        //Store the first team color of the first player alive you find and compare all the other players' team colors with it
                        if (teamcompare == -1)
                        {
                            teamcompare = TeamOfPlayer(Players[i]);
                            TeamPlayerWon = TeamOfPlayer(Players[i]);
                        }

                        if (TeamOfPlayer(Players[i]) != teamcompare) return false;
                    }

                    return true;
                }
                //If the time is up, check which team has the most lives, health, and healthbars
                else
                {
                    if (Time < 0) return false;
                    TeamTimeCompleteSA();
                    return true;
                }
            }
            //Score mode
            else
            {
                if (Time < 0) return false;

                //The total scores for each of the teams
                int[] totalteamscores = new int[3] { -999, -999, -999 };

                //Go through all the players and check which team each one is on and add up all the scores for that team
                for (int i = 0; i < Players.Count; i++)
                {
                    //This is a safeguard to prevent the red team from winning if every team's total score is 0 or less and red isn't present
                    if (totalteamscores[TeamOfPlayer(Players[i])] <= -999) totalteamscores[TeamOfPlayer(Players[i])] = 0;
                    totalteamscores[TeamOfPlayer(Players[i])] += Players[i].Score;
                }

                //Check if the total score for a team is at or exceeds the max score
                if (ScoreLimit > 0)
                {
                    for (int i = 0; i < totalteamscores.Length; i++)
                    {
                        if (totalteamscores[i] >= ScoreLimit)
                        {
                            TeamPlayerWon = i;
                            return true;
                        }
                    }
                }

                //Check which team has the highest score if time ran out
                if (Time == 0)
                {
                    int highestscore = -999;
                    for (int i = 0; i < totalteamscores.Length; i++)
                    {
                        if (totalteamscores[i] > highestscore)
                        {
                            TeamPlayerWon = i;
                            highestscore = totalteamscores[i];
                        }
                    }

                    return true;
                }

                return false;
            }
        }

        //Ending game condition for teams:
        /* Check all the players that are alive
         * Compare those players' teams
         * If all of their teams are the same, the game is done; otherwise it's not*/
        protected override bool IsCleared()
        {
            //Check if only one player is remaining in FFA, otherwise check which team is alive
            //In Score mode, check if a player has the score required to win
            if (TeamColors == null)
            {
                return FFAComplete();
            }
            else
            {
                return TeamsComplete();
            }
        }

        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.Finished() == true) SubLevelFinished = true;
            else if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
            else if (Fade.IsFading() == false && Fade.IsFadingIn() == false) PrevTime = activeTime;
        }

        protected override void EndSub(float activeTime, Main main)
        {
            base.EndSub(activeTime, main);

            if (TeamColors == null)
                TempPlayers[TeamPlayerWon].Victory(activeTime);
            else
            {
                for (int i = 0; i < TempPlayers.Count; i++)
                {
                    if (TeamOfPlayer(TempPlayers[i]) == TeamPlayerWon) TempPlayers[i].Victory(activeTime);
                }
            }

            SetUpResults(main);
        }

        //Spawns Players in the right spots for challenges
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, 100, 200, 0f, true, false);
                if (SpawnStatus[i] != (int)Status.Statuses.None) Players[i].SetStats(null, null, null, null, null, new Status(SpawnStatus[i].GCond, SpawnStatus[i].GStatusDur, SpawnStatus[i].GPercentage));
            }
        }

        //Checks which team wins if the time is up in the Standard Battle and Avoid Enemy game modes
        private void TeamTimeCompleteSA()
        {
            //Add up all the lives, health, and healthbars for the teams
            int[] teamlives = new int[] { -1, -1, -1 };
            int[] teamhealthbars = new int[] { -1, -1, -1 };
            float[] teamhealth = new float[] { -1f, -1f, -1f };

            int highestlives = -1;
            int highesthealthbars = -1;
            float highesthealth = -1f;

            for (int i = 0; i < Players.Count; i++)
            {
                //Initialize the team values to 0 if the team color exists
                if (teamlives[TeamOfPlayer(Players[i])] == -1) teamlives[TeamOfPlayer(Players[i])] = 0;
                if (teamhealthbars[TeamOfPlayer(Players[i])] == -1) teamhealthbars[TeamOfPlayer(Players[i])] = 0;
                if (teamhealth[TeamOfPlayer(Players[i])] == -1f) teamhealth[TeamOfPlayer(Players[i])] = 0;

                teamlives[TeamOfPlayer(Players[i])] += Players[i].GetLives;
                teamhealthbars[TeamOfPlayer(Players[i])] += Players[i].HealthBars;
                teamhealth[TeamOfPlayer(Players[i])] += Players[i].GetHealth;
            }

            //Find which team has the highest of each value
            for (int i = 0; i < teamlives.Length; i++)
            {
                //Check if the team has more lives than the highest found
                if (teamlives[i] > highestlives)
                {
                    TeamPlayerWon = i;
                    highestlives = teamlives[i];
                    highesthealthbars = teamhealthbars[i];
                    highesthealth = teamhealth[i];
                }
                else if (teamlives[i] == highestlives)
                {
                    //Check if the team has more health bars than the highest found
                    if (teamhealthbars[i] > highesthealthbars)
                    {
                        TeamPlayerWon = i;
                        highesthealthbars = teamhealthbars[i];
                        highesthealth = teamhealth[i];
                    }
                    //If the team has the same amount of health bars, check if the team has more health than the highest found max
                    else if (teamhealthbars[i] == highesthealthbars && teamhealth[i] > highesthealth)
                    {
                        TeamPlayerWon = i;
                        highesthealth = teamhealth[i];
                    }
                }
            }
        }

        //Collisions for the enemy hitting the players - the default one doesn't provide what I need and is excessive because it checks for players hitting enemies which is impossible here
        private void EnemyPlayerCollisions(float activeTime)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                for (int p = 0; p < Players.Count; p++)
                {
                    /*if (EnemList[i].gHitbox.Intersect(Players[p].CollisionBox, Players[p].CurHeight, Players[p].MaxHeight))
                    {
                        //Prevent the enemy from hurting a teammate if team attack is off and the teammate is the enemy's target
                        if (TeamColors != null && TeamAttack == false && p != VersusEnemy.GetCommander())
                        {
                            if (TeamOfPlayer(Players[p]) == TeamColors[VersusEnemy.GetCommander()] && Players[p].GPlayerNum == VersusEnemy.GetTargetNum()) return;
                        }

                        //Make the player's tank take damage
                        if (Players[p].GetOxygenTank != null && Players[p].GetOxygenTank.CanBreak(activeTime, EnemList[i].gHitbox) == true)
                        {
                            //If you hit another player's tank, reset the previous HUD so it doesn't show up so you can draw the new one
                            //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);

                            Players[p].GetOxygenTank.Break(activeTime, 5, EnemList[i].gHitbox, TileEngine);

                            EnemList[i].SetHitLag(activeTime);
                        }

                        if (Players[p].CanGetHit(Players[p].CollisionBox, EnemList[i].gHitbox) == true)
                        {
                            //Throw the player
                            if (EnemList[i].gHitbox.Throw == true)
                            {
                                EnemList[i].ThrowPlayer(activeTime, Players[p], this);
                            }
                            //Hit the player with a normal attack
                            else
                            {
                                Status newstat;
                                if (EnemList[i].GetStatus == (int)Status.Statuses.Invincible)
                                    newstat = new Status();
                                else newstat = new Status(EnemList[i].GetStatus.GCond, EnemList[i].GetStatus.GStatusDur);

                                Players[p].TakeDamage(activeTime, EnemList[i].GetFacingDir, (EnemList[i].GetDamage + EnemList[i].gHitbox.Strength), newstat, EnemList[i].gHitbox, TileEngine, Solids);
                            }

                            //If an enemy hits the player, reset the previous HUD so it doesn't show up so you can draw the new one
                            //HUD.ChangeHUD(activeTime, Players[p].GPlayerNum, EnemList[i].GetHUD);

                            //Make the enemy experience hitlag for hitting the player
                            EnemList[i].SetHitLag(activeTime);
                        }
                    }*/
                }
            }
        }

        //Update all items
        protected override void UpdateItemList(float activeTime)
        {
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].Update();

                if (ItemList[i].ShouldRemove == true)
                {
                    //Don't make a new switch spawn right away; make it wait if there were already the maximum number allowed on the screen
                    if (ItemList.Count >= 8) PrevSwitch = activeTime;
                    ItemList.RemoveAt(i);
                    i--;
                }
            }
        }

        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                //Set the player's status to the start/spawn status after the player's invincibility wears out
                if (Respawned[i] == true && Players[i].status != (int)Status.Statuses.Invincible)
                {
                    Players[i].SetStats(null, null, null, null, null, new Status(SpawnStatus[i].GCond, SpawnStatus[i].GStatusDur, SpawnStatus[i].GPercentage));
                    Respawned[i] = false;
                }

                //Key inputs for switching the enemy's target in the Avoid Enemy mode - have to do it this way because of the VolumeUp and VolumeDown key glitch
                /*Due to a lack of enough buttons on a controller (which I wasn't planning on having support for when this was initially made), players may just have to hold down the Item key and...
                ...press the following direction to correspond with each player: 1 = Up, 2 = Left, 3 = Right, 4 = Down*/
                if (GameMode == (int)VersusScreen.GameModes.Avoid && Players[i].IsWell() == true)
                {
                    //Check if the player has at least 5 SwitchKeys
                    if (Players[i].Score >= MinSwitchKeysControl)
                    {
                        //Check if the item key is held
                        if (Input.KeyHeld(Player.GetActionKey(Players[i].GPlayerNum, (int)Player.Action.ItemKey)) == true)
                        {
                            //Check which was player was targetted
                            int? playernumtargetted = Players[i].VersusEnemyTarget();

                            //If the player targetted the player number of the player for the VersusEnemy to go for, find the player and make the VersusEnemy go for it
                            if (playernumtargetted != null)
                            {
                                Player targettedplayer = Player.PlayerOfPlayerNum(Players, (int)playernumtargetted);

                                //If a player with the player number was found, set the VersusEnemy to go for it and subtract 5 Switch Keys from the player who commanded the VersusEnemy
                                if (targettedplayer != null)
                                {
                                    VersusEnemy.SetTarget(activeTime, targettedplayer, Players[i].GPlayerNum, targettedplayer.GPlayerNum);
                                    Players[i].AddScore(-MinSwitchKeysControl);
                                }
                            }
                        }
                    }
                }

                Players[i].Update();

                //If the player just lost a life, make him respawn
                if (Players[i].IsDead == true && Players[i].KnockedDown == false)
                {
                    if (GameMode == (int)VersusScreen.GameModes.Score)
                    {
                        Players[i].AddScore(-1);

                        //Give the player that last hit this one a point if there are no teams or if the team that player is on is different from the one this one is on
                        if (TeamColors == null || TeamOfPlayer(Players[i]) != TeamColors[LastHit[Players[i].GPlayerNum]]) Players[LastHit[Players[i].GPlayerNum]].AddScore(1);
                    }

                    Players[i].Respawn(activeTime, new Vector3(Main.ScreenHalf.X, Main.ScreenHalf.Y - 40, -1), TileEngine);
                    //Give the character a life back if the current game mode is Score
                    if (GameMode == (int)VersusScreen.GameModes.Score) Players[i].SetStats(null, null, Players[i].GetLives + 1, null, null, null);

                    if (Players[i].IsCompletelyDead == true)
                    {
                        Players.RemoveAt(i);
                        i--;
                    }
                    else Respawned[i] = true;
                }
            }
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            UpdateTimer(activeTime);

            //Spawn switches only in the Avoid Enemy mode
            if (GameMode == (int)VersusScreen.GameModes.Avoid) SpawnSwitchKey(activeTime);
        }

        protected override void UpdateTimer(float activeTime)
        {
            if (Time > 0 && (activeTime - PrevTime) >= 1000f)
            {
                Time--;
                PrevTime = activeTime;
            }
        }

        //Spawns a switchkey for players to pick up in a random spot
        private void SpawnSwitchKey(float activeTime)
        {
            if (ItemList.Count < 8 && (activeTime - PrevSwitch) >= 3000f)
            {
                Random random = new Random();
                int itemcount = ItemList.Count;

                ItemList.Add(new SwitchKey(new Vector2(random.Next((int)Camera.LeftBounds + 15, (int)Camera.RightBounds - 15), random.Next((int)Camera.TopBounds + 15, (int)Camera.BottomBounds - 15))));
                ItemList[itemcount].SetLocationTile(new Vector3(ItemList[itemcount].CollisionBox.X, ItemList[itemcount].CollisionBox.Y, 0f), TileEngine);
                PrevSwitch = activeTime;
            }
        }

        //Finds the proper location to draw the switch key icons, switch key counts, or team colors depending on the player index
        private int KeyTeamDrawLoc(int index)
        {
            switch (index)
            {
                case 0: return 33;
                case 1: return 124;
                case 2: return 260;
                default: return 351;
            }
        }

        //Find the locations to display the information based on game mode and number of players
        //For example, if there are 3 players in teams, group the 2 players on the same team together in order of player number (Red is leftmost, Green is in the middle, Blue is rightmost)
        private void SetResultsLocationsTeams(int[] position, List<Vector2> iconloc, List<String> resultsinfo, List<Vector2> infoloc)
        {
            List<String> teamtext = new List<String>();
            List<Vector2> teamloc = new List<Vector2>();
            int[] teamindex = new int[3];

            //Check which position the player is in
            for (int i = 0; i < TempPlayers.Count; i++)
            {
                //Go through the position array
                for (int j = 0; j < position.Length; j++)
                {
                    //Check if this character's information should be drawn at this position
                    if (TeamOfPlayer(TempPlayers[i]) == j)
                    {
                        int modifier = 0;

                        //Check whether to move the middle team to the left or to the right based on how many teams there are
                        if (j == 1)
                        {
                            if (position[0] == 0) modifier = -1;
                            else if (position[2] == 0) modifier = 1;
                        }

                        int spacing = 0;

                        //Check how much to space each character's icon based on how many players are on that team
                        if (position[j] > 1)
                        {
                            if (teamindex[TeamOfPlayer(TempPlayers[i])] == 0) spacing = 12 * (position[j] - 1);
                            else if (teamindex[TeamOfPlayer(TempPlayers[i])] == (position[j] - 1)) spacing = -12 * (position[j] - 1);
                        }

                        //2 players, first one has to be at position - (1/2 * distance away) and second has to be at position + (1/2 * distance away)
                        //3 players, first has to be at position - (1/2 * distance away), second has to be at position, 3rd has to be at position + (1/2 * distance away)
                        iconloc.Add(new Vector2((Main.ScreenHalf.X - 100 + ((Main.ScreenHalf.X / 4) * (j + modifier))) - spacing, Main.ScreenHalf.Y - 40));
                        infoloc.Add(new Vector2(iconloc[iconloc.Count - 1].X, Main.ScreenHalf.Y + 10));
                        if (GameMode == (int)VersusScreen.GameModes.Score)
                            infoloc.Add(new Vector2(iconloc[iconloc.Count - 1].X, (Main.ScreenHalf.X / 2) - LoadGraphics.HUDFont.MeasureString(Convert.ToString(TempPlayers[i].Score)).Y));
                        iconloc.Add(new Vector2(iconloc[iconloc.Count - 1].X, Main.ScreenHalf.Y - 40));

                        teamindex[TeamOfPlayer(TempPlayers[i])]++;
                    }
                }
            }
        }

        //Sets up the versus results screen (Mario Kart 64-styled)
        //The last choices entries are the scores of the players if they're playing in score mode
        /*Algorithm:
         *Find out who belongs on the left side, who belongs on the right, and who belongs in the middle (if any) 
         *Based on how many players are on each side, space out the icons, dialogue, team color (if teams are enabled), and score (if in score mode)*/
        private void SetUpResults(Main main)
        {
            //3 possible positions; left, right, and middle (used only in teams)
            int[] position = TeamColors != null ? new int[3] : null;

            List<Texture2D> charicons = new List<Texture2D>();
            List<Vector2> iconloc = new List<Vector2>();
            List<String> resultsinfo = new List<String>();
            List<Vector2> infoloc = new List<Vector2>();

            Vector2[] ffalocations = new Vector2[4] { new Vector2((Main.ScreenHalf.X / 2) - 50, Main.ScreenHalf.Y - 40), new Vector2(Main.ScreenHalf.X - 100, Main.ScreenHalf.Y - 40), new Vector2(Main.ScreenSize.X - 150, Main.ScreenHalf.Y - 40), new Vector2(Main.ScreenHalf.X + 100, Main.ScreenHalf.Y - 40) };

            //Choose a random dialog
            Random random = new Random();

            for (int i = 0; i < TempPlayers.Count; i++)
            {
                int playernum = Player.IndexOfCharacter(TempPlayers[i]);
                charicons.Add(LoadGraphics.CharacterIcons[playernum]);

                //Check who won and who lost and display the proper dialogue
                if (TeamColors == null)
                {
                    if (TempPlayers[i].GPlayerNum == TeamPlayerWon)
                    {
                        if (TempPlayers.Count == 2) resultsinfo.Add(WinDialogue[playernum][Player.IndexOfCharacter(TempPlayers[i ^ 1])][random.Next(0, 4)]);
                        else resultsinfo.Add(FFAWinDialogue[playernum][random.Next(0, 4)]);
                    }
                    else
                    {
                        if (TempPlayers.Count == 2) resultsinfo.Add(LoseDialogue[playernum][Player.IndexOfCharacter(TempPlayers[i ^ 1])][random.Next(0, 4)]);
                        else resultsinfo.Add(FFALoseDialogue[playernum][random.Next(0, 4)]);//LoseDialogue[TempPlayer.IndexOfCharacter(Players[i])]);
                    }

                    iconloc.Add(ffalocations[i]);
                    infoloc.Add(new Vector2(iconloc[i].X, Main.ScreenHalf.Y + 10));

                    if (GameMode == (int)VersusScreen.GameModes.Score)
                    {
                        resultsinfo.Add(Convert.ToString(TempPlayers[i].Score));
                        infoloc.Add(new Vector2(infoloc[infoloc.Count - 1].X, (Main.ScreenHalf.X / 2) - LoadGraphics.HUDFont.MeasureString(Convert.ToString(TempPlayers[i].Score)).Y));
                    }
                }
                else
                {
                    if (TeamOfPlayer(TempPlayers[i]) == TeamPlayerWon) resultsinfo.Add(TeamsWinDialogue[playernum][random.Next(0, 4)]);//WinDialogue[Player.IndexOfCharacter(Players[i])]);
                    else resultsinfo.Add(TeamsLoseDialogue[playernum][random.Next(0, 4)]);//LoseDialogue[Player.IndexOfCharacter(Players[i])]);

                    if (GameMode == (int)VersusScreen.GameModes.Score) resultsinfo.Add(Convert.ToString(TempPlayers[i].Score));

                    charicons.Add(LoadGraphics.TeamGraphics[TeamOfPlayer(TempPlayers[i])]);
                    position[TeamOfPlayer(TempPlayers[i])]++;
                }
            }

            if (TeamColors != null)
            {
                SetResultsLocationsTeams(position, iconloc, resultsinfo, infoloc);
            }

            String modename;

            if (GameMode == (int)VersusScreen.GameModes.Standard) modename = "Standard Battle";
            else if (GameMode == (int)VersusScreen.GameModes.Avoid) modename = "Avoid Enemy";
            else modename = "Score";

            main.AddScreen(new ResultsScreen(modename, charicons.ToArray(), iconloc.ToArray(), infoloc.ToArray(), resultsinfo.ToArray()));
        }

        //protected override void UpdateCollisions(float activeTime)
        //{
        //    //These are separated to avoid normal player collisions
        //    //Maybe when players get hurt or knocked down they lose switchkeys? (Check if you should do this while playtesting)
        //    //Then again, it would really slow down the pace of the game because you should be PREVENTING others from getting them
        //    //If everyone keeps hitting others to make them LOSE their switchkeys, it'll be a long time before anyone manages to get 5, and when that player finally does, he or she can still lose them
        //    EnemyPlayerCollisions(activeTime);
        //    Collision.VersusPlayerCollisions(activeTime, Players, TeamColors, TeamAttack, GameMode, LastHit, DamageRatio, this);
        //    Collision.PlayerObjectCollisions(activeTime, this);
        //}

        protected override void DrawEnemyList(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                EnemList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);

                //Draw the player the VersusEnemy is going for if it's going for someone
                if (EnemList[i] is VersusEnemy && VersusEnemy.GetTargetNum() >= 0)
                {
                    spriteBatch.DrawString(LoadGraphics.HUDFont, Convert.ToString(VersusEnemy.GetTargetNum() + 1), new Vector2(EnemList[i].GetLocationHeight.X + ((EnemList[i].CollisionBox.Width + 10) / 2), EnemList[i].GetLocationHeight.Y - 30f), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, EnemList[i].GetLocationHeight.Y / 1000f);
                }
            }
        }

        protected override void DrawPlayers(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Draw(spriteBatch, Vector2.Zero, TileEngine);

                Color color = Color.White;

                //Show which team each player is on
                if (TeamColors != null)
                {
                    color = TeamColorOfPlayer(Players[i]);

                    //Draw the character names and icon outlines in the color of the team they're on
                    //Be sure to make the colors apparent on the characters themselves so players can stay engaged and still know which team the player they're attacking is on
                    spriteBatch.Draw(LoadGraphics.TeamGraphics[TeamOfPlayer(Players[i])], new Vector2(KeyTeamDrawLoc(Players[i].GPlayerNum) - 18, 15), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9961f);
                }

                //If you're in the Avoid Enemy mode, show how many switches each player has (the player's score)
                if (GameMode == (int)VersusScreen.GameModes.Avoid)
                {
                    //Players[i].GetHUD.DrawPlayerScore(spriteBatch, color);
                }
            }
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            if (TeamColors != null) spriteBatch.DrawString(LoadGraphics.HUDFont, "TA: " + (TeamAttack == true ? "On" : "Off"), new Vector2(Main.ScreenHalf.X - 30, 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);
            if (Time > -1) spriteBatch.DrawString(LoadGraphics.HUDFont, "Time: " + Time, new Vector2(Main.ScreenHalf.X - 40, 50), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);

            if (Background != null)
                spriteBatch.Draw(Background, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
