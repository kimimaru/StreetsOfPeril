﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    public class SnipeN : Snipe
    {
        public SnipeN()
        {
            Difficulty = 1;

            //LevelData level = LevelEditor.LoadLevelOld("SnipeN");
            //
            //TileEngine = level.TileEngine;
            //
            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));
            //
            //CreateSpawnNumbers(level.EnemPoints);
        }

        protected override String ResultName
        {
            get { return "Snipe - Normal"; }
        }

        //public override void RestartChallenge(float activeTime)
        //{
        //    base.RestartChallenge(activeTime);
        //    Time = 45f;
        //    PrevTime = 0f;
        //
        //    LevelData level = LevelEditor.LoadLevelOld("SnipeN");
        //    LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));
        //
        //    CreateSpawnNumbers(level.EnemPoints);
        //
        //    LoadSounds.PlayChallengeMusic(3);
        //}
    }
}
