﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Snipe challenge; defeat designated enemies in groups as quickly as possible to proceed!
    //For Normal, have more than one designated enemy, and for Hard have more than one designated enemy and have the enemies from previous groups remain!
    //IDEA ONLY: Although you lose after killing a non-designated enemy, the ranking can take how many non-designated enemies you killed into account for something similar that's implemented
    public class Snipe : CLevel
    {
        //A list storing the stoppoint each enemy spawned is on to ensure that if enemies in a previous group died after the current group spawned, the player(s) wouldn't lose
        protected List<Vector2> EnemSpawnNumbers;

        public Snipe()
        {
            BG = LoadGraphics.BG;
            FG = LoadGraphics.FG;

            Players.Add(CreateObjects.Crystal(0, 0));

            Time = 45f;
            PrevTime = 0f;

            ChallengeNum = (int)Challenges.Snipe;
            Difficulty = 0;

            RestartChallenge(Main.GetActiveTime);
        }

        protected override bool IsCleared()
        {
            return (LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == false);
        }

        protected virtual bool KillEnemies
        {
            get { return true; }
        }

        //Tells when the player loses; in Easy and Normal, the player loses if a non-designated enemy is killed before a designated enemy, and in Hard the player loses if a non-designated enemy
        //is killed at all
        protected virtual bool LoseCondition(Enemy enem, int index)
        {
            return (LevelCamera.DesignatedAlive == true && enem.Designated == false && EnemSpawnNumbers[index] == LevelCamera.GCameraLocation);
        }

        protected virtual String ResultName
        {
            get { return "Snipe"; }
        }

        //The fading black intro and outro of challenges
        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
            else if (Fade.IsFading() == false && Fade.IsFadingIn() == false) PrevTime = activeTime + (Time * 1000f);
        }

        public override void RestartChallenge(float activeTime)
        {
            base.RestartChallenge(activeTime);
            Time = 45f;
            PrevTime = 0f;

            SublevelProp level = LevelEditor.LoadLevel("Snipe");
            LoadData(level);

            LevelData leveldata = level.ConvertToLevelData();

            CreateSpawnNumbers(leveldata.EnemPoints);

            LoadSounds.PlayChallengeMusic(3/*(int)CLevel.Challenges.Snipe*/);
        }

        protected void CreateSpawnNumbers(List<ObjectSpawnPoint<Enemy>> EnemPoints)
        {
            EnemSpawnNumbers = new List<Vector2>();
            for (int i = 0; i < EnemPoints.Count; i++)
                EnemSpawnNumbers.Add(new Vector2(EnemPoints[i].AppearLocation.X, EnemPoints[i].AppearLocation.Y));
        }

        //Ranks the player based on how fast the designated enemies were killed
        protected override void CalculatePerformance(Main main)
        {
            Ranking ranking = CLevel.ChallengeRanks[ChallengeNum][Difficulty];

            //Get the current score/time of the challenge; if it changed after ranking the performance of the player(s), then it's a new record
            char rank = Ranking.LetterValue(ranking.GetCurrentPerformance(Time));
            char bestrank = Ranking.LetterValue(ranking.GetRank());
            float scoretime = ranking.GetScoreTime();
            Vector2 newrecordloc = Vector2.Zero;
            String newrecord = String.Empty;

            //Rank the players performance
            ranking.RankPerformance(Time);
            ranking.RankScoreTime(Time);

            //If the previous best score isn't the same as the best score after ranking performance, it's a new record
            if (scoretime != ranking.GetScoreTime())
            {
                newrecordloc = new Vector2(155, 250);
                newrecord = "New Record!";
            }

            main.AddScreen(new ResultsScreen(null, ResultName + " Results", null, new Vector2[] { new Vector2(25, 100), new Vector2(233, 100), new Vector2(25, Main.ScreenHalf.Y + 10), new Vector2(233, Main.ScreenHalf.Y + 10), newrecordloc },
                "Your Rank: " + rank, "Best Rank: " + bestrank, "Your Time: " + String.Format("{0:0.00}", Time), "Best Time: " + String.Format("{0:0.00}", scoretime), newrecord));

            //Save the data
            SaveLoadData.SaveChallenge(ranking, ChallengeNum, Difficulty);
        }

        protected override void OnEnemyDeath(ref int index)
        {
            if (Fade.IsFading() == false)
            {
                Enemy deadenemy = EnemList[index];

                //If the enemy died before the designated enemy, was in the same wave as the designated enemy, and wasn't designated itself, end the challenge; in Hard mode, if any non-designated dies, end the challenge
                if (LoseCondition(deadenemy, index) == true)
                {
                    Fade.ContinueFade();
                    return;
                }

                //Record the current state saying if any designated enemies are left
                bool Designated = LevelCamera.DesignatedAlive;

                //Update the number of designated enemies alive
                LevelCamera.CheckRemoveDesignated(deadenemy.Designated);

                EnemList.RemoveAt(index);
                EnemSpawnNumbers.RemoveAt(index);
                index--;

                //The last designated enemy died, so kill all the other non-designated enemies
                if (KillEnemies == true && Designated == true && LevelCamera.DesignatedAlive == false && LevelCamera.HasStopsRemaining == true)
                {
                    for (int j = 0; j < EnemList.Count; j++)
                    {
                        if (EnemList[j].IsDead == false)
                        {
                            EnemList[j].Die();
                        }
                    }
                }
            }
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            UpdateTimer(activeTime);
        }

        protected override void DrawEnemyList(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < EnemList.Count; i++)
            {
                EnemList[i].Draw(spriteBatch, GCameraOffSet, TileEngine);

                //If the enemy is designated, draw a symbol above its head
                if (EnemList[i].Designated == true && EnemList[i].IsDead == false)
                {
                    Vector2 symbolloc = new Vector2(EnemList[i].GetLocationVec2.X + GCameraOffSet.X, EnemList[i].GetLocationVec2.Y - EnemList[i].CurHeight + GCameraOffSet.Y - (int)(EnemList[i].GetLocationHeight.W * 1.2f));

                    //If the enemy has a NoStatus (NoJump, etc.) then draw the symbol to the side of it
                    if (EnemList[i].status.IsNoStatus() == true)
                        symbolloc.X += EnemList[i].ForwardVal(LoadGraphics.NoStatusSprites[0].Width);

                    spriteBatch.Draw(LoadGraphics.EnemDesignated, symbolloc, null, Color.White, 0f, new Vector2(Animation.DefaultOrigin(LoadGraphics.EnemDesignated).X, 0f), 1f, SpriteEffects.None, EnemList[i].GetDrawDepth(GCameraOffSet));
                }
            }
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            DrawTimer(spriteBatch);
        }
    }
}
