﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    public class SnipeH : Snipe
    {
        public SnipeH()
        {
            Time = 60f;

            Difficulty = 2;

            //LevelData level = LevelEditor.LoadLevelOld("SnipeH");
            //
            //TileEngine = level.TileEngine;
            //
            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));
            //
            //CreateSpawnNumbers(level.EnemPoints);
        }

        protected override bool KillEnemies
        {
            get { return false; }
        }

        protected override bool LoseCondition(Enemy enem, int index)
        {
            return (enem.Designated == false);
        }

        protected override string ResultName
        {
            get { return "Snipe - Hard"; }
        }

        //public override void RestartChallenge(float activeTime)
        //{
        //    base.RestartChallenge(activeTime);
        //    Time = 60f;
        //    PrevTime = 0f;
        //
        //    LevelData level = LevelEditor.LoadLevelOld("SnipeH");
        //    LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(level));
        //
        //    CreateSpawnNumbers(level.EnemPoints);
        //
        //    LoadSounds.PlayChallengeMusic(3/*(int)CLevel.Challenges.Snipe*/);
        //}
    }
}
