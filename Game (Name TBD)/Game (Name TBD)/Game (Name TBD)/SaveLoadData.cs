﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Game__Name_TBD_
{
    //Loads and saves various data, including unlockables, challenge ranks/times, and more
    public static class SaveLoadData
    {
        //The path to the save file
        private const String FileName = "Content\\SaveData\\Savedata.xml";

        //The root of the XML storing all the saved data
        private const String DataName = "SavedData";

        //Saves the player's progress for Rank Mode
        //public static void SaveRanks(int levelnum, String storename)
        //{
        //    Ranking Ranking = HighscoresScreen.LevelRanks[levelnum];
        //
        //    //Load the entire save file
        //    XDocument loadfile = new XDocument();
        //
        //    //Check if the save file exists; if not, create it, otherwise load it
        //    if (File.Exists(FileName) == false)
        //    {
        //        loadfile = new XDocument(new XElement(DataName));
        //        loadfile.Element(DataName).Add(new XElement("Levels"));
        //        loadfile.Save(FileName);
        //    }
        //    else loadfile = XDocument.Load(FileName);
        //
        //    //Check if there is save data for levels
        //    XElement sectionelement = loadfile.Root.Element("Levels");
        //
        //    //If there isn't, create the data
        //    if (sectionelement == null)
        //    {
        //        sectionelement = new XElement("Levels");
        //        loadfile.Root.Add(sectionelement);
        //    }
        //
        //    //Check if the level completed has data in the save file
        //    XElement element = sectionelement.Element(storename);
        //
        //    //If so, overwrite the old data with the new data
        //    if (element != null)
        //    {
        //        element.Attribute("Rank").SetValue(Convert.ToString(Ranking.GetRank()));
        //        element.Attribute("Score").SetValue(Convert.ToString((float)Math.Round(Ranking.GetScoreTime(), 2)));
        //    }
        //    //If not, create data for the completed level and save the information into the save file
        //    else
        //    {
        //        element = new XElement(storename);
        //
        //        sectionelement.Add(element);
        //
        //        element.SetAttributeValue("Rank", Convert.ToString(Ranking.GetRank()));
        //        element.SetAttributeValue("Score", Convert.ToString((float)Math.Round(Ranking.GetScoreTime(), 2)));
        //    }
        //
        //    //Overwrite the save file
        //    loadfile.Save(FileName);
        //}

        //public static void LoadRanks()
        //{
        //    //Check if there is any save data - if not, don't try loading it
        //    if (File.Exists(FileName) == false)
        //        return;
        //
        //    //Load the entire save file
        //    XDocument loadfile = XDocument.Load(FileName, LoadOptions.PreserveWhitespace);
        //
        //    //Try to load the level data
        //    XElement rootelement = loadfile.Element(DataName);
        //    XElement sectionelement = rootelement.Element("Levels");
        //
        //    //If not, create a section for levels in the save file
        //    if (sectionelement == null)
        //    {
        //        rootelement.Add(new XElement("Levels"));
        //
        //        loadfile.Save(FileName);
        //        return;
        //    }
        //
        //    //Go through all the challenges and load the save data
        //    for (int i = 0; i < HighscoresScreen.LevelRanks.Length; i++)
        //    {
        //        //Check if there is data for that challenge; if not, move on to check the next challenge
        //        XElement levelinfo = sectionelement.Element("Level" + i);
        //        if (levelinfo == null) continue;
        //
        //        //Load in the data
        //        HighscoresScreen.LevelRanks[i].Load(Convert.ToInt32(levelinfo.Attribute("Rank").Value), (float)Convert.ToDouble(levelinfo.Attribute("Score").Value));
        //    }
        //}

        public static void SaveChallenge(Ranking ranking, int challengenum, int difficultynum)
        {
            SaveData("Challenges", "Challenge" + challengenum + difficultynum, Ranking.RankRead, ranking.GetRank());
            SaveData("Challenges", "Challenge" + challengenum + difficultynum, Ranking.ScoreTimeRead, ranking.GetScoreTime());
        }

        public static void LoadChallengeRanking(Ranking ranking, int challengenum, int difficulty)
        {
            int rank = Convert.ToInt32(LoadData("Challenges", "Challenge" + challengenum + difficulty, Ranking.RankRead));
            float scoretime = (float)Convert.ToDouble(LoadData("Challenges", "Challenge" + challengenum + difficulty, Ranking.ScoreTimeRead));

            ranking.Load(rank, scoretime);
        }

        //Loads in a specific level's ranking (Rank Mode)
        public static Ranking LoadLevelRanking(int levelnum)
        {
            //Null is returned if the number entered isn't valid
            Ranking LevelRank = null;

            //Only levels 1-8 (0-7 internally) have rankings associated with them
            if (levelnum >= 0 && levelnum < 8)
            {
                //Load in the score differences for each rank
                switch (levelnum)
                {
                    case 0: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    case 1: LevelRank = new Ranking(9000, 20000, 30000, 40000);
                        break;
                    case 2: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    case 3: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    case 4: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    case 5: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    case 6: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                    default: LevelRank = new Ranking(10000, 20000, 30000, 40000);
                        break;
                }

                object rank = SaveLoadData.LoadData("Levels", "Level" + levelnum, Ranking.RankRead);
                object score = SaveLoadData.LoadData("Levels", "Level" + levelnum, "Score");

                //If the rank and score are present, load them in
                if (rank != null && score != null)
                    LevelRank.Load(Convert.ToInt32(rank), Convert.ToInt32(score));
            }

            return LevelRank;
        }

        //Loads all the level rankings (Rank Mode)
        public static Ranking[] LoadLevelRankings()
        {
            //Load up the default scores for each level
            Ranking[] LevelRank = new Ranking[8]; //{ new Ranking(10000, 20000, 30000, 40000), new Ranking(9000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000), new Ranking(10000, 20000, 30000, 40000) };

            //Go through all the levels in the save file and see if there's saved data for them
            for (int i = 0; i < LevelRank.Length; i++)
                LevelRank[i] = LoadLevelRanking(i);

            return LevelRank;
        }

        //Checks if an unlockable is unlocked or not
        public static bool CheckUnlocked(String unlockable)
        {
            //Check the unlockables section of the save file
            return Convert.ToBoolean(LoadData("Unlockables", unlockable, "Unlocked"));
        }

        //Saves data like unlockables, current level, local high scores, and more
        //Example: category = "Unlockables"; subcategory = "SoundTest"; storename = "Unlocked"; value = true;
        public static void SaveData(String category, String subcategory, String storename, object value)
        {
            //Load the entire save file
            XDocument loadfile = new XDocument();

            //Check if the save file exists; if not, create it, otherwise load it
            if (File.Exists(FileName) == false)
            {
                loadfile = new XDocument(new XElement(DataName));
                loadfile.Element(DataName).Add(new XElement(category));
                loadfile.Save(FileName);
            }
            else loadfile = XDocument.Load(FileName);

            //Check if there is save data for the category (Unlockable, CurrentLevel, etc.)
            XElement sectionelement = loadfile.Root.Element(category);

            //If there isn't, create the data
            if (sectionelement == null)
            {
                sectionelement = new XElement(category);
                loadfile.Root.Add(sectionelement);
            }

            //Check if the category has data in the save file
            XElement element = sectionelement.Element(subcategory);

            //If so, overwrite the old data with the new data
            if (element != null)
            {
                element.SetAttributeValue(storename, value);
            }
            //If not, create data for the information and save it into the save file
            else
            {
                element = new XElement(subcategory);

                sectionelement.Add(element);

                element.SetAttributeValue(storename, value);
            }

            //Overwrite the save file
            loadfile.Save(FileName);
        }

        //Loads data like unlockables, current level, local high scores, and more
        //Since they can vary so much, they may be tricky to read in, so it's possible that more specific methods will need to be made or some parameters will need to be passed in
        //Alternatively, they can be read in individually (Ex. reading a value from the save file in the static constructor to see if SoundTest is unlocked)
        public static object LoadData(String category, String subcategory, String storename)
        {
            //Check if there is any save data - if not, don't try loading it
            if (File.Exists(FileName) == false)
                return null;

            //Load the entire save file
            XDocument loadfile = XDocument.Load(FileName, LoadOptions.PreserveWhitespace);

            //Try to load the data
            XElement rootelement = loadfile.Element(DataName);

            //Start out loading the main category of data this is in
            XElement sectionelement = rootelement.Element(category);

            //If not, create a section for that category in the save file
            if (sectionelement == null)
            {
                rootelement.Add(new XElement(category));

                loadfile.Save(FileName);
                return null;
            }

            //Check if there is data for that section in that category
            XElement info = sectionelement.Element(subcategory);
            if (info == null) return null;

            if (info.Attribute(storename) == null) return null;
            else return info.Attribute(storename).Value;
        }

        //Erases specified data
        //Set each string to null if it's not needed in the hierarchy
        //Example: category = "Challenges"; subcategory = null; storename = null would result in erasing the "Challenges" element
        public static void EraseData(String category, String subcategory, String storename)
        {
            //Check if there is any save data - if not, don't try loading it
            if (File.Exists(FileName) == false)
                return;

            //Load the entire save file
            XDocument loadfile = XDocument.Load(FileName, LoadOptions.PreserveWhitespace);

            //Try to load the save data
            XElement rootelement = loadfile.Element(DataName);

            //See if there is a category to remove or not
            if (category != null)
            {
                //Load the category data
                XElement categoryelement = rootelement.Element(category);

                //Check if there's a valid category
                if (categoryelement != null)
                {
                    //See if there is a subcategory to remove or not
                    if (subcategory != null)
                    {
                        //Load the subcategory data
                        XElement subcategoryelement = categoryelement.Element(subcategory);

                        //Check if there's a valid subcategory
                        if (subcategoryelement != null)
                        {
                            //See if there's a property too remove
                            if (storename != null)
                            {
                                XElement storeelement = subcategoryelement.Element(storename);

                                //Check if there's a valid property
                                if (storeelement != null)
                                    storeelement.Remove();
                            }
                            else subcategoryelement.Remove();
                        }
                    }
                    else categoryelement.Remove();
                }
            }

            //Overwrite the save file
            loadfile.Save(FileName);
        }

        public static void EraseAutosave()
        {
            EraseData("Autosave", null, null);

            /*//Check if there is any save data - if not, don't try loading it
            if (File.Exists(FileName) == false)
                return;

            //Load the entire save file
            XDocument loadfile = XDocument.Load(FileName, LoadOptions.PreserveWhitespace);

            //Try to load the data
            XElement rootelement = loadfile.Element(DataName);

            //Load the autosave data
            XElement sectionelement = rootelement.Element("Autosave");

            if (sectionelement != null)
            {
                sectionelement.Remove();

                //Overwrite the save file
                loadfile.Save(FileName);
            }*/
        }
    }
}
