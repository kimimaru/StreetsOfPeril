﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A level suited for Rank Mode; it is identical to Level except that Rank Mode contains a timer
    public sealed class RankLevel : Level
    {
        //The level timer
        private HUD LevelTime;
        private bool TimerTick;

        //Tracks how many lives a player lost
        private int[] LivesLost;

        public RankLevel(List<Player> players, int levelnum)
        {
            LevelNum = levelnum;
            Players = players;
            LivesLost = new int[Players.Count];

            SubLevelCreate();

            //LevelTime = new HUD(null);
            TimerTick = false;

            LevelIntro = new Intro(new Texture2D[] { LoadGraphics.LevelIntro, LoadGraphics.LevelNumGraphic[LevelNum], LoadGraphics.LevelStart });
            LevelOutro = null;
        }

        public override HUD GetTimer
        {
            get { return LevelTime; }
        }

        //Increases the counter that keeps track of the number of lives a player lost - for Rank Mode ONLY
        public override void LostLife(int index, bool completelydead)
        {
            LivesLost[index]++;
        }

        //Stops the level timer (Rank Mode)
        public override void StopTime(float activeTime)
        {
            TimerTick = false;
        }

        //Resumes the level timer (Rank Mode)
        public override void ResumeTime(float activeTime)
        {
            TimerTick = true;
            
        }

        protected override bool IsGameOver()
        {
            return CheckAllPlayersDead() == true;
        }

        protected override void GameOver(float activeTime, Main main)
        {
            main.ResetLevels();
        }

        //Takes the health and lives and calculates it into the score - for Rank Mode ONLY
        private void CalculatePerformance(Main main)
        {
            //The subtotal of all the player scores shown before the other factors are taken into account
            int subtotalscore = 0;

            //The other values that influence the total score (damage taken, lives lost, etc.)
            int liveslost = 0;
            int healthremaining = 0;
            int difficultybonus = (Main.Difficulty * 10000);

            //Add all the individual player scores together and subtract the amount of lives and health lost
            for (int i = 0; i < Players.Count; i++)
            {
                subtotalscore += Players[i].Score;

                //Check how many lives the player lost and subtract that from the total score
                liveslost -= LivesLost[i] * 1000;

                //Subtract the player's max health from his/her current health
                healthremaining -= (100 - (int)Players[i].GetHealth) * 10;
            }

            //The final score; the sum of the subtotal and all the influential factors
            int totalscore = subtotalscore + liveslost + healthremaining;

            Ranking curranking = SaveLoadData.LoadLevelRanking(LevelNum);

            float X = 133;
            main.AddScreen(new ResultsScreen(null, "Level " + (LevelNum + 1), null, new Vector2[] { new Vector2(X, 45), new Vector2(X, 70), new Vector2(X, 95), new Vector2(X, 120), new Vector2(X, 145), new Vector2(X, 224), new Vector2(X, 249), new Vector2(X, 274) },
                "Subtotal: " + subtotalscore, "Lives Lost: " + liveslost, "Health Remaining: " + healthremaining, "Difficulty Bonus: " + difficultybonus, "Final Score: " + totalscore, "Rank: " + Ranking.LetterValue(curranking.GetCurrentPerformance(totalscore)), "Best Rank: " + Ranking.LetterValue(curranking.GetRank()), "Best Score: " + curranking.GetScoreTime()));

            //Rank the average of all the players' performances
            curranking.RankPerformance((totalscore / Players.Count) + difficultybonus);
            curranking.RankScoreTime(totalscore + difficultybonus);

            //Save the data
            SaveLoadData.SaveData("Levels", "Level" + LevelNum, Ranking.RankRead, curranking.GetRank());
            SaveLoadData.SaveData("Levels", "Level" + LevelNum, "Score", curranking.GetScoreTime());
        }

        //Update timer
        protected override void LevelAction(float activeTime)
        {
            //Update the timer if it should be updated

            //Kill all alive players if the time runs out (Rank Mode only)
            //NOTE: Get rid of this and just time the player(s); add the time it takes to complete the level to their score, the bonus points decreasing the longer it takes (sort of like the Sonic series' Time Bonus)
            
        }

        protected override void EndLevel(float activeTime, Main main)
        {
            base.EndLevel(activeTime, main);
            CalculatePerformance(main);
        }

        protected override void DrawInfo(float activeTime, SpriteBatch spriteBatch)
        {
            String timestring = String.Empty;//Convert.ToString(String.Format("{0:00}", LevelTime.Time));
            Vector2 levelnamesize = LoadGraphics.GetStringSize(LoadGraphics.HUDFont, LevelName, .75f);
            Vector2 timesize = LoadGraphics.GetStringSize(LoadGraphics.HUDFont, timestring, .75f);

            Vector2 timedrawloc = new Vector2(LevelDrawLoc.X + (levelnamesize.X / 2), LevelDrawLoc.Y + ((int)levelnamesize.Y + 2f));

            spriteBatch.Draw(LoadGraphics.LevelTime, new Vector2(timedrawloc.X + 2f, timedrawloc.Y + (LoadGraphics.LevelTime.Height - 5)), null, Color.White, 0f, Animation.DefaultOrigin(LoadGraphics.LevelTime), 1f, SpriteEffects.None, .997f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, timestring, timedrawloc, Color.Blue, 0f, new Vector2(timesize.X / 2, 0f), .75f, SpriteEffects.None, .997f);
        }
    }
}
