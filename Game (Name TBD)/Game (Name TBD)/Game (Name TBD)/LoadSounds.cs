﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class loads all the game's sounds and music on startup for access anywhere
    //NOTE: Do NOT use MP3s because that is a licensed format! Use WAV or Ogg Vorbis instead!
    public static class LoadSounds
    {
        //The music in the game
        public enum Music
        {
            Menu, Level1, Level2, Level3, Level4, Level5, Level6, Level7, Level8, Level9, Chal1, Chal2, Chal3, Chal4, Chal5, Bonus, Museum, CharSelect, Boss, LevelC, GOver, Reward
        };

        //The sounds
        public enum SoundEffects
        {
            
        };

        //The sound volume
        public static float SoundVolume;

        public static List<Song> Songs;
        public static List<SoundEffect> Sounds;

        public static Song[] MenuSong = new Song[1];
        public static Song[] LevelSong = new Song[7];
        public static Song[] ChallengeSong = new Song[5];
        public static Song BonusSong;
        public static Song MuseumSong;
        public static Song CharSelect;
        public static Song BossSong;
        public static Song LevelCompleteSong;
        public static Song GameOver;

        public static SoundEffect[] PDeath;

        //Environmental Interaction
        public static SoundEffect WaterWalk;
        public static SoundEffect WaterLand;

        //Wil
        public static SoundEffect Gunshot;

        //Crystal
        public static SoundEffect CThrow;
        public static SoundEffect CSpcAtk;

        public static SoundEffect Pause;
        public static SoundEffect NewRecord;
        public static SoundEffect ScoreAdd;
        public static SoundEffect Choose;
        public static SoundEffect Select;
        public static SoundEffect Attack;
        public static SoundEffect JumpLand;
        public static SoundEffect Break;
        public static SoundEffect Punch1;
        public static SoundEffect JumpKick;
        public static SoundEffect Kick;
        public static SoundEffect Land;
        public static SoundEffect Sweep;
        public static SoundEffect PSpcAtk;
        public static SoundEffect EnemyLaugh;
        public static SoundEffect Explosion;
        public static SoundEffect EDeath;
        public static SoundEffect PThrow;
        public static SoundEffect Restore;
        public static SoundEffect GetLife;
        public static SoundEffect GetBonusStatus;
        public static SoundEffect Go;

        //Weapon sounds
        public static SoundEffect SharpHit;
        public static SoundEffect SharpSwing;

        static LoadSounds()
        {
            MediaPlayer.Volume = .5f;
            SoundVolume = .5f;

            object musicvolume = SaveLoadData.LoadData("Settings", "Volume", "Music");
            object soundvolume = SaveLoadData.LoadData("Settings", "Volume", "Sound");

            if (musicvolume != null) MediaPlayer.Volume = (float)Convert.ToDouble(musicvolume);
            if (soundvolume != null) SoundVolume = (float)Convert.ToDouble(soundvolume);
        }

        //Load all sounds and music so they're ready to use at any time
        public static void LoadContent(ContentManager Content)
        {
            Songs = new List<Song>();
            Sounds = new List<SoundEffect>();

            Songs.Add(Content.Load<Song>("Songs\\Mega Man X - Title Screen"));
            Songs.Add(Content.Load<Song>("Songs\\Streets of Rage Remake - Fighting in the Street"));
            Songs.Add(Content.Load<Song>("Songs\\Final Fight 3 - Law And Disorder"));
            Songs.Add(Content.Load<Song>("Songs\\Final Fight 2 - Italy"));
            Songs.Add(Content.Load<Song>("Songs\\Secret of Mana - Into the Thick of It"));
            Songs.Add(Content.Load<Song>("Songs\\Pokémon Black & White - Chargestone Cave"));
            Songs.Add(Content.Load<Song>("Songs\\EarthBound - Save the Miners!"));
            Songs.Add(Content.Load<Song>("Songs\\Densetsu no Stafy 3 - World 8 (Undersea Ruins)"));
            Songs.Add(null);
            Songs.Add(null);
            Songs.Add(Content.Load<Song>("Songs\\Final Fight 3 - Law And Disorder"));
            Songs.Add(Content.Load<Song>("Songs\\Earthbound - Fourside"));
            Songs.Add(Content.Load<Song>("Songs\\Final Fight 2 - Creeping Wheeping"));
            Songs.Add(Content.Load<Song>("Songs\\Super Smash Bros. Melee - Stadium"));
            Songs.Add(Content.Load<Song>("Songs\\Turtles In Time - Versus Victory"));
            Songs.Add(Content.Load<Song>("Songs\\Final Fight 2 - Bonus Stage"));
            Songs.Add(Content.Load<Song>("Songs\\Game & Watch Gallery 4 - G&W History"));
            Songs.Add(Content.Load<Song>("Songs\\Streets of Rage 2 - Player Select V2"));
            Songs.Add(Content.Load<Song>("Songs\\Streets of Rage 2 - Never Return Alive"));

            Songs.Add(Content.Load<Song>("Songs\\Streets of Rage 2 - Level Complete"));
            Songs.Add(Content.Load<Song>("Songs\\Streets of Rage 2 - Game Over"));
            Songs.Add(Content.Load<Song>("Songs\\Mario Party - Minigame Victory #5"));

            //Songs.Add(MenuSong[0]);
            //Songs.AddRange(LevelSong);
            //Songs.Add(ChallengeSong[0]);
            //Songs.Add(ChallengeSong[1]);
            //Songs.Add(ChallengeSong[2]);
            //Songs.Add(ChallengeSong[3]);
            //Songs.Add(ChallengeSong[4]);
            //Songs.Add(BonusSong);
            //Songs.Add(MuseumSong);
            //Songs.Add(CharSelect);
            //Songs.Add(BossSong);
            //Songs.Add(LevelCompleteSong);
            //Songs.Add(GameOver);

            Pause = Content.Load<SoundEffect>("Sounds\\Pause");
            NewRecord = Content.Load<SoundEffect>("Sounds\\NewRecord");
            ScoreAdd = Content.Load<SoundEffect>("Sounds\\ScoreAdd");
            Choose = Content.Load<SoundEffect>("Sounds\\Choose");
            Select = Content.Load<SoundEffect>("Sounds\\Select");
            Attack = Content.Load<SoundEffect>("Sounds\\Attack");
            JumpLand = Content.Load<SoundEffect>("Sounds\\Land");
            Break = Content.Load<SoundEffect>("Sounds\\Break");
            Punch1 = Content.Load<SoundEffect>("Sounds\\Punch");
            JumpKick = Content.Load<SoundEffect>("Sounds\\JumpKick");
            Kick = Content.Load<SoundEffect>("Sounds\\Kick");
            Land = Content.Load<SoundEffect>("Sounds\\ThrowLand");
            Sweep = Content.Load<SoundEffect>("Sounds\\Sweep");
            PSpcAtk = Content.Load<SoundEffect>("Sounds\\SpcAtk");
            CSpcAtk = Content.Load<SoundEffect>("Sounds\\Crystal\\SpcAtk");
            Gunshot = Content.Load<SoundEffect>("Sounds\\Wil\\GunShot");
            EnemyLaugh = Content.Load<SoundEffect>("Sounds\\EnemyLaugh");
            Explosion = Content.Load<SoundEffect>("Sounds\\Explosion");
            EDeath = Content.Load<SoundEffect>("Sounds\\EnemDeath");
            PDeath = new SoundEffect[] { null, null, Content.Load<SoundEffect>("Sounds\\Crystal\\CrystalDeath"), null };
            PThrow = Content.Load<SoundEffect>("Sounds\\PlayerThrow");
            CThrow = Content.Load<SoundEffect>("Sounds\\Crystal\\Throw");
            Restore = Content.Load<SoundEffect>("Sounds\\HRestore");
            GetLife = Content.Load<SoundEffect>("Sounds\\Life");
            GetBonusStatus = Content.Load<SoundEffect>("Sounds\\BonusStatus");
            Go = Content.Load<SoundEffect>("Sounds\\Go");
            WaterWalk = Content.Load<SoundEffect>("Sounds\\WaterWalk");
            WaterLand = Content.Load<SoundEffect>("Sounds\\WaterLand");
            SharpHit = Content.Load<SoundEffect>("Sounds\\SharpHit");
            SharpSwing = Content.Load<SoundEffect>("Sounds\\SharpSwing");

            Sounds.Add(Pause);
            Sounds.Add(NewRecord);
            Sounds.Add(ScoreAdd);
            Sounds.Add(Choose);
            Sounds.Add(Select);
            Sounds.Add(Attack);
            Sounds.Add(JumpLand);
            Sounds.Add(Break);
            Sounds.Add(Punch1);
            Sounds.Add(JumpKick);
            Sounds.Add(Kick);
            Sounds.Add(Land);
            Sounds.Add(Sweep);
            Sounds.Add(PSpcAtk);
            Sounds.Add(CSpcAtk);
            Sounds.Add(Gunshot);
            Sounds.Add(EnemyLaugh);
            Sounds.Add(Explosion);
            Sounds.Add(EDeath);
            Sounds.AddRange(PDeath);
            Sounds.Add(PThrow);
            Sounds.Add(CThrow);
            Sounds.Add(Restore);
            Sounds.Add(GetLife);
            Sounds.Add(GetBonusStatus);
            Sounds.Add(Go);
            Sounds.Add(WaterWalk);
            Sounds.Add(WaterLand);
            Sounds.Add(SharpHit);
            Sounds.Add(SharpSwing);
        }

        //Plays a sound effect with the right properties (volume, etc.)
        public static void Play(SoundEffect Sound, float pitch = 0f, float pan = 0f)
        {
            if (Sound != null) Sound.Play(SoundVolume, 0.0f, 0.0f);
        }

        //Plays a song
        public static void PlaySong(Song Song, bool repeat = false)
        {
            if (Song != null)
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(Song);
                MediaPlayer.IsRepeating = repeat;
            }
        }

        public static void PlaySong(int Song, bool repeat = false)
        {
            if ((Song >= 0 && Song < Songs.Count) && Songs[Song] != null)
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(Songs[Song]);
                MediaPlayer.IsRepeating = repeat;
            }
        }

        //Plays a song on repeat
        public static void PlaySongRepeat(Song Song)
        {
            PlaySong(Song);
            MediaPlayer.IsRepeating = true;
        }

        //Play menu music
        public static void PlayMenuMusic(int menunum)
        {
            PlaySong(MenuSong[menunum]);
        }

        //Play level music
        public static void PlayLevelMusic(int levelnum)
        {
            PlaySong((int)Music.Level1 + levelnum, true);
        }

        //Play bonus level music
        public static void PlayBonusMusic()
        {
            PlaySong((int)Music.Bonus, true);
        }

        //Play challenge level music
        public static void PlayChallengeMusic(int cnum)
        {
            PlaySong((int)Music.Chal1 + cnum, true);
        }

        //Stop music
        public static void StopMusic()
        {
            MediaPlayer.Stop();
            MediaPlayer.IsRepeating = false;
        }

        //Play the level complete music
        public static void PlayLevelCompleteMusic()
        {
            PlaySong(Songs[(int)Music.LevelC]);
        }

        //Play the boss music
        public static void PlayBossMusic()
        {
            PlaySong(Songs[(int)Music.Boss], true);
        }

        //Play the game over music
        public static void PlayGameOverMusic()
        {
            PlaySong(Songs[(int)Music.GOver]);
        }
    }
}
