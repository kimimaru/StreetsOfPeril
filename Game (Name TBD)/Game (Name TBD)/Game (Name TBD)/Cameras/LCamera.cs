﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A more limited camera that pans left and right and/or up and down, between their respective points
    //A normal camera can transform into this at certain points of the level (Ex. against a boss like in Final Fight 2)
    public sealed class LCamera : Camera
    {
        //The max distance the camera can move in either direction from the starting point
        private Vector2 Distance;

        //The origin the camera starts moving from - it's mainly used after converting a normal camera to a limited one
        //private Vector2 StartOrigin;

        //Constructor
        public LCamera(Texture2D bg, Vector2 Dist)
        {
            BG = bg;

            Distance = Dist;
            CameraOrigin = new Vector2(Main.ScreenHalf.X, Main.ScreenHalf.Y);

            CameraType = CameraTypes.Limited;
        }

        //Constructor for objects that should be immediately spawned in the level
        /*public LCamera(Texture2D bg, Vector2 Dist, SublevelProp level) : base(bg, LoadGraphics.FG, level)
        {
            while (Spawns.Count > 0)
            {
                for (int i = 0; i < Spawns.Peek().Count; i++)
                {
                    
                }
            }
        }*/

        //Constructor for setting the camera origin when transforming a normal camera into a limited one
        public LCamera(Texture2D bg, Vector2 Dist, Vector2 Origin)
            : this(bg, Dist)
        {
            CameraOrigin = Origin;
            CameraLocation = CameraOrigin;
        }

        //Transforms a normal camera into a limited camera (can be used for boss fights)
        public static void TransformLimited(ref Camera camera, Texture2D bg, Vector2 dist)
        {
            camera = new LCamera(bg, dist, camera.GCameraLocation);
        }

        private bool CheckMove(List<Player> Players, bool? left, bool? up)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (left == true)
                {
                    if (Players[i].GetLocationHeight.X >= (CameraLocation.X - 30)) return false;
                }
                else if (left == false)
                {
                    if (Players[i].GetLocationHeight.X <= (CameraLocation.X + 30)) return false;
                }

                if (up == true)
                {
                    if (Players[i].GetLocationHeight.Y >= (CameraLocation.Y - 30)) return false;
                }
                else if (up == false)
                {
                    if (Players[i].GetLocationHeight.Y <= (CameraLocation.Y + 30)) return false;
                }
            }

            return true;
        }

        //Moves the camera
        private void Move(SubLevel level)
        {
            //Check if the camera can move left or right
            if (Distance.X != 0f)
            {
                //Check moving left
                if (CameraLocation.X > (CameraOrigin.X - Distance.X) && CheckMove(level.GetPlayers, true, null) == true)
                {
                    CameraLocation.X--;
                }
                //Check moving right
                else if (CameraLocation.X < (CameraOrigin.X + Distance.X) && CheckMove(level.GetPlayers, false, null) == true)
                {
                    CameraLocation.X++;
                }
            }

            //Check if the camera can move up or down
            if (Distance.Y != 0f)
            {
                //Check moving up
                if (CameraLocation.Y > (CameraOrigin.Y - Distance.Y) && CheckMove(level.GetPlayers, null, true) == true)
                {
                    CameraLocation.Y--;
                }
                //Check moving down
                else if (CameraLocation.Y < (CameraOrigin.Y + Distance.Y) && CheckMove(level.GetPlayers, null, false) == true)
                {
                    CameraLocation.Y++;
                }
            }
        }

        public override void Update(SubLevel level)
        {
            Move(level);
        }

        public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet)
        {
            spriteBatch.Draw(BG, new Rectangle((int)(CameraOrigin.X - CameraLocation.X), (int)(CameraOrigin.Y - CameraLocation.Y), BG.Width, BG.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
        }
    }
}
