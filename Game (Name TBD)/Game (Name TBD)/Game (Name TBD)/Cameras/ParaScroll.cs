﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Parallax scrolling on a background
    //NOTE: Add support for multiple layers, each with its own speed!
    public sealed class ParaScroll
    {
        //Background
        private Texture2D Background;

        //How fast the scrolling goes - it can be changed if something happens in the level (Ex. Minecart slows down)
        private Vector2 Velocity;

        //Position to draw the texture
        private Vector2 Position;

        //Direction to scroll - if null, don't scroll in that direction at all
        private bool? ScrollLeft;
        private bool? ScrollRight;
        private bool? ScrollUp;
        private bool? ScrollDown;

        //Constructor - enable only left or right or only up or down
        public ParaScroll(Texture2D bg, Vector2 velocity, bool? left, bool? up)
        {
            Background = bg;
            Position = Vector2.Zero;
            Velocity = velocity;

            ScrollLeft = left;
            ScrollRight = !ScrollLeft;
            ScrollUp = up;
            ScrollDown = !ScrollUp;

            if (ScrollLeft == true) Velocity.X = -Velocity.X;
            if (ScrollUp == true) Velocity.Y = -Velocity.Y;
        }

        //Set how fast the camera scrolls
        public void SetSpeed(Vector2 newspeed)
        {
            Velocity = newspeed;
        }

        //Change how fast the camera scrolls
        public void ChangeSpeed(Vector2 speedchange)
        {
            Velocity += speedchange;
        }

        //Change the position and mod it with the background width so it loops around
        public void Update()
        {
            if (ScrollLeft == true || ScrollRight == true)
            {
                Position.X += Velocity.X;
                Position.X %= Background.Width;
            }
            else if (ScrollUp == true || ScrollDown == true)
            {
                Position.Y += Velocity.Y;
                Position.Y %= Background.Height;
            }
        }

        //These all work fine as long as the background is bigger than the size of the screen
        public void Draw(SpriteBatch spriteBatch)
        {
            if (ScrollLeft == true)
            {
                if (Position.X < -(Background.Width - Main.ScreenSize.X))
                    spriteBatch.Draw(Background, new Vector2(Position.X + Background.Width, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }
            else if (ScrollRight == true)
            {
                if (Position.X > 0)
                    spriteBatch.Draw(Background, new Vector2(Position.X - Background.Width, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }
            else if (ScrollUp == true)
            {
                if (Position.Y < -(Background.Height - Main.ScreenSize.Y))
                    spriteBatch.Draw(Background, new Vector2(0, Position.Y + Background.Height), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }
            else if (ScrollDown == true)
            {
                if (Position.Y > 0)
                    spriteBatch.Draw(Background, new Vector2(0, Position.Y - Background.Height), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            }

            spriteBatch.Draw(Background, Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}