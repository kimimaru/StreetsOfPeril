﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //NOTE: For the new camera: Have a queue of StopPoints listed by Stop Number, each with a list of Spawns; loop through each Spawn in the StopPoints and see if the
    //CameraLocation is at the Spawn's Offset, and spawn the Spawn if so - this allows other Spawns to spawn even if another Spawn was not reached (Ex. Camera only goes left but Spawn's Offset is set to the right)
    public class Camera
    {
        //The types of Cameras there are
        public enum CameraTypes
        {
            Standard, Limited
        };

        //The distance from a side of the screen that the object can be away from when checking screen bounds (for Players)
        private const float DistFromSide = 10f;

        //Background and foreground
        protected Texture2D BG;
        protected Texture2D FG;

        //The amount of time between each complete GO! animation
        private const float BetweenGoTimer = 3250f;

        //Animation telling you to move forward
        private Animation GoHorizontal;
        private Animation GoVertical;
        protected bool IsLocked;

        //Tells the camera whether any designated objects are remaining or not
        private int DesignatedRemaining;

        //Stores the location of the camera when it stopped
        private Vector2 CameraStopped;

        //The rotation for the Go animation depending on the direction the camera scrolls in
        private double GoHorizontalRotation;
        private double GoVerticalRotation;

        //The type of camera this is
        protected CameraTypes CameraType;

        //The location of the camera - it centers itself on the player(s)
        protected Vector2 CameraLocation;
        protected Vector2 CameraOrigin;

        //The StopPoints that hold all the spawns in the level
        protected Queue<StopPoint> StopPoints;
        //The spawns in the level
        protected Queue<List<Spawn>> Spawns;

        //How many times the camera stopped since the start of the level
        private int StopCount;

        public Camera()
        {
            CameraOrigin = new Vector2(Main.ScreenHalf.X, Main.ScreenHalf.Y);
            CameraLocation = CameraOrigin;
            IsLocked = false;
            DesignatedRemaining = 0;

            CameraType = CameraTypes.Standard;
        }

        //Constructor
        public Camera(Texture2D bg, Texture2D fg, SublevelProp level) : this()
        {
            BG = bg;
            FG = fg;

            CameraStopped = CameraLocation;

            LoadSpawnData(level);

            GoHorizontalRotation = 0;
            GoVerticalRotation = 0;

            StopCount = 0;
        }

        //Loads level data into the camera
        public void LoadSpawnData(SublevelProp level)
        {
            //Add the stop points as a Queue and initialize the Queue of Spawn lists
            //StopPoints = new Queue<StopPoint>(level.StopPoints);
            //Spawns = new Queue<List<Spawn>>(StopPoints.Count);

            
            //Initialize StopPoints Queue
            StopPoints = new Queue<StopPoint>();
            
            //Add in all StopPoints that are enabled
            for (int i = 0; i < level.StopPoints.Count; i++)
            {
                if (level.StopPoints[i].Disabled == false)
                    StopPoints.Enqueue(level.StopPoints[i]);
            }
            
            //Initialize the Queue of Spawn lists with a capacity based on the number of StopPoints that were added
            Spawns = new Queue<List<Spawn>>(StopPoints.Count);

            //Loop through all the stop points 
            for (int i = 0; i < StopPoints.Count/*level.StopPoints.Count*/; i++)
            {
                List<Spawn> tempspawns = new List<Spawn>();

                //Loop through all the spawns, then see which spawns equal the current StopPoint's StopNumber and add it to the list
                for (int j = 0; j < level.LevelSpawns.Count; j++)
                {
                    //Find which Spawn should spawn at which StopPoint; this is effectively sorting Spawns by StopNumber
                    if (level.LevelSpawns[j].Disabled == false && level.LevelSpawns[j].StopPoint == level.StopPoints[i].StopNumber)
                    {
                        tempspawns.Add(level.LevelSpawns[j]);
                    }
                }

                //Add to the number of spawn lists, even if it is empty; this is because the number of spawn lists must equal the number of StopPoints
                Spawns.Enqueue(tempspawns);
            }
        }

        public bool HasStopsRemaining
        {
            get { return (CameraType != CameraTypes.Standard || (StopPoints != null && StopPoints.Count > 0)); }
        }

        public Vector2 GCameraLocation
        {
            get { return CameraLocation; }
        }

        public Vector2 CameraDrawLoc
        {
            get { return (CameraOrigin - CameraLocation); }
        }

        public Vector2 DistFromOrigin
        {
            get { return (CameraLocation - CameraOrigin); }
        }

        public int CurStopNum
        {
            get { return StopCount; }
        }

        //Gets the type of camera this is
        public CameraTypes GetCameraType
        {
            get { return CameraType; }
        }

        public bool DesignatedAlive
        {
            get { return (DesignatedRemaining > 0); }
        }

        //The direction the camera is currently moving
        public Vector2 CameraDirection
        {
            get
            {
                //The camera direction defaults to not moving
                Vector2 cameradir = Vector2.Zero;

                Vector2 curstoploc = StopPoints.Peek().TrueLocation;

                //X direction
                if (CameraStopped.X < curstoploc.X) cameradir.X = 1;
                else if (CameraStopped.X > curstoploc.X) cameradir.X = -1;

                //Y direction
                if (CameraStopped.Y < curstoploc.Y) cameradir.Y = 1;
                else if (CameraStopped.X > curstoploc.Y) cameradir.Y = -1;

                return cameradir;
            }
        }

        public bool IsMovingRight
        {
            get { return (CameraDirection.X > 0); }
        }

        public bool IsMovingDown
        {
            get { return (CameraDirection.Y > 0); }
        }

        //Values for the screen boundaries
        public static float TopBounds
        {
            get { return DistFromSide; }
        }

        public static float BottomBounds
        {
            get { return (Main.ScreenSize.Y - DistFromSide); }
        }

        public static float LeftBounds
        {
            get { return DistFromSide; }
        }

        public static float RightBounds
        {
            get { return (Main.ScreenSize.X - DistFromSide); }
        }

        //Gets how far an object is from all sides of the screen (positive for Left and Top and negative for Right and Bottom indicate that the object is offscreen)
        //This more accurately tells you how far each side of the screen is from the object
        public Vector4 ObjectDistFromScreen(Rectangle objfeetloc)
        {
            //Add the draw location to the object's FeetLoc
            objfeetloc.X += (int)CameraDrawLoc.X;
            objfeetloc.Y += (int)CameraDrawLoc.Y;

            return (new Vector4(Camera.LeftBounds - objfeetloc.Left, Camera.TopBounds - objfeetloc.Top, Camera.RightBounds - objfeetloc.Right, Camera.BottomBounds - objfeetloc.Bottom));
        }

        //Checks if an object is within the screen boundaries
        public bool ObjectInScreenBounds(Rectangle objfeetloc)
        {
            //Add the draw location to the object's FeetLoc
            objfeetloc.X += (int)CameraDrawLoc.X;
            objfeetloc.Y += (int)CameraDrawLoc.Y;

            //Check all 4 sides of the screen
            return (objfeetloc.X >= Camera.LeftBounds && objfeetloc.Right <= Camera.RightBounds && objfeetloc.Y >= Camera.TopBounds && objfeetloc.Bottom <= Camera.BottomBounds);
        }

        //Increments the number of designated objects remaining
        public void IncrementDesignated()
        {
            DesignatedRemaining++;
        }

        //Decrements the number of designated objects remaining
        public void DecrementDesignated()
        {
            DesignatedRemaining--;
            if (DesignatedRemaining < 0) DesignatedRemaining = 0;
        }

        //Checks to decrement the designated counter if the object that died was designated
        public void CheckRemoveDesignated(bool designated)
        {
            if (designated == true)
            {
                DesignatedRemaining--;
                //Precautionary measures; who knows what could happen?
                if (DesignatedRemaining < 0) DesignatedRemaining = 0;
            }
        }

        private void MoveCamera(SubLevel level)
        {
            //Check whether to stop the camera or not
            StopCamera();
            if (IsLocked == true) return;

            Vector2 curstopoffset = StopPoints.Peek().TrueLocation;

            //If the camera isn't already at the next stop point in the X direction, see if the player is heading towards the stoppoint and follow if this is the case
            if (CameraLocation.X != curstopoffset.X)
            {
                float distance = MathHelper.Distance(CameraLocation.X, curstopoffset.X);

                //The player is moving towards the stop point, so follow him/her
                int scrolldirection = CameraLocation.X < curstopoffset.X ? 1 : -1;
                int movespeedx = /*(int)CameraSpeed(Players).X*/ 2 * scrolldirection;

                if (distance < Math.Abs(movespeedx)) movespeedx = (int)distance * scrolldirection;

                if (CheckMove(level.GetPlayers, curstopoffset, true, movespeedx) == true)
                {
                    /*float distance = MathHelper.Distance(CameraLocation.X, StopPoints.X);

                    //The player is moving towards the stop point, so follow him/her
                    int scrolldirection = CameraLocation.X < StopPoints.X ? 1 : -1;
                    int movespeedx = (int)CameraSpeed(Players).X * scrolldirection;*/

                    //Check if the camera is going to overshoot the stop point at its current speed, and if so, change the speed to make it equal the stop point's Y value
                    //if (distance < Math.Abs(movespeedx)) movespeedx = (int)distance * scrolldirection;

                    CameraLocation.X += movespeedx;
                    //level.GCameraOffSet = new Vector2(level.GCameraOffSet.X - movespeedx, level.GCameraOffSet.Y);
                }
            }
            else if (GoHorizontal != null)
            {
                GoHorizontal = null;
                if (GoVertical != null) GoVertical.SetSounds(new int[] { 0, 1 }, new SoundEffect[] { LoadSounds.Go, LoadSounds.Go });
            }

            //If the camera isn't already at the next stop point in the Y direction, see if the player is heading towards the stoppoint and follow if this is the case
            if (CameraLocation.Y != curstopoffset.Y)
            {
                float distance = MathHelper.Distance(CameraLocation.Y, curstopoffset.Y);

                //The player is moving towards the stop point, so follow him/her
                int scrolldirection = CameraLocation.Y < curstopoffset.Y ? 1 : -1;
                int movespeedy = /*(int)CameraSpeed(Players).Y*/ 2 * scrolldirection;

                if (distance < Math.Abs(movespeedy)) movespeedy = (int)distance * scrolldirection;

                if (CheckMove(level.GetPlayers, curstopoffset, false, movespeedy) == true)
                {
                    /*float distance = MathHelper.Distance(CameraLocation.Y, StopPoints.Y);

                    //The player is moving towards the stop point, so follow him/her
                    int scrolldirection = CameraLocation.Y < StopPoints.Y ? 1 : -1;
                    int movespeedy = (int)CameraSpeed(Players).Y * scrolldirection;

                    //Check if the camera is going to overshoot the stop point at its current speed, and if so, change the speed to make it equal the stop point's Y value
                    if (distance < Math.Abs(movespeedy)) movespeedy = (int)distance * scrolldirection;*/
                    
                    CameraLocation.Y += movespeedy;
                    //level.GCameraOffSet = new Vector2(level.GCameraOffSet.X, level.GCameraOffSet.Y - movespeedy);
                }
            }
            else if (GoVertical != null) GoVertical = null;
        }

        //Checks if one player is in the scroll range and none are against the side of the screen
        private bool CheckMove(List<Player> Players, Vector2 StopPoints, bool XorY, float movespeed)
        {
            //Store if a player is in the scroll range or not
            bool[] cameramove = new bool[Players.Count];

            if (XorY == true)
            {
                for (int i = 0; i < Players.Count; i++)
                {
                    if (CameraLocation.X < StopPoints.X)
                    {
                        //If the player is against the left side of the screen when the camera is scrolling right, don't scroll
                        if ((Players[i].FeetLoc.X - movespeed + CameraDrawLoc.X) < LeftBounds) return false;
                        //Otherwise, if the player is closer to the stop point than the camera position, then the camera has the potential to scroll
                        else if (Players[i].GetLocationHeight.X > CameraLocation.X) cameramove[i] = true;
                    }
                    else if (CameraLocation.X > StopPoints.X)
                    {
                        //If the player is against the right side of the screen when the camera is scrolling left, don't scroll
                        if ((Players[i].FeetLoc.Right - movespeed + CameraDrawLoc.X) > RightBounds) return false;
                        //Otherwise, if the player is closer to the stop point than the camera position, then the camera has the potential to scroll
                        else if (Players[i].GetLocationHeight.X < CameraLocation.X) cameramove[i] = true;
                    }
                }
            }
            else if (XorY == false)
            {
                for (int i = 0; i < Players.Count; i++)
                {
                    if (CameraLocation.Y < StopPoints.Y)
                    {
                        //If the player is against the top side of the screen when the camera is scrolling down, don't scroll
                        if ((Players[i].FeetLoc.Y - movespeed + CameraDrawLoc.Y) < TopBounds) return false;
                        //Otherwise, if the player is closer to the stop point than the camera position, then the camera has the potential to scroll
                        else if (Players[i].GetLocationHeight.Y > CameraLocation.Y) cameramove[i] = true;
                    }
                    else if (CameraLocation.Y > StopPoints.Y)
                    {
                        //If the player is against the bottom side of the screen when the camera is scrolling up, don't scroll
                        if ((Players[i].FeetLoc.Bottom - movespeed + CameraDrawLoc.Y) > BottomBounds) return false;
                        //Otherwise, if the player is closer to the stop point than the camera position, then the camera has the potential to scroll
                        else if (Players[i].GetLocationHeight.Y < CameraLocation.Y) cameramove[i] = true;
                    }
                }
            }

            //No players are against the sides of the screen opposite the directions the camera is scrolling, so check if any of the players are in the scroll range and scroll the camera if so
            for (int i = 0; i < cameramove.Length; i++)
            {
                if (cameramove[i] == true) return true;
            }

            return false;
        }

        //Evaluates when to spawn a spawn based on which direction the camera is currently moving (X direction)
        private bool CheckSpawnX(Spawn spawn)
        {
            if (IsMovingRight == true) return (CameraLocation.X >= spawn.TrueLocation.X);
            else return (CameraLocation.X <= spawn.TrueLocation.X);
        }

        //Evaluates when to spawn a spawn based on which direction the camera is currently moving (Y direction)
        private bool CheckSpawnY(Spawn spawn)
        {
            if (IsMovingDown == true) return (CameraLocation.Y >= spawn.TrueLocation.Y);
            else return (CameraLocation.Y <= spawn.TrueLocation.Y);
        }

        //Evaluates when to spawn something or stop the camera based on which direction the camera is moving (X plane)
        private bool CheckDirectionX(float val1, float val2, LevelManager Manage)
        {
            if (CameraStopped.X < Manage.CurrentStop().X) return val1 >= val2;
            else return val1 <= val2;
        }

        //Evaluates when to spawn something or stop the camera based on which direction the camera is moving (Y plane)
        private bool CheckDirectionY(float val1, float val2, LevelManager Manage)
        {
            if (CameraStopped.Y < Manage.CurrentStop().Y) return val1 >= val2;
            else return val1 <= val2;
        }

        //Check when to stop the camera
        private void StopCamera()
        {
            if (CameraLocation == StopPoints.Peek().TrueLocation)//.X == Manage.CurrentStop().X && CameraLocation.Y == Manage.CurrentStop().Y)
            {
                IsLocked = true;
                //Manage.ContinueStop();
                StopPoints.Dequeue();
                Spawns.Dequeue();
                CameraStopped = CameraLocation;
                StopCount++;
                GoHorizontal = null;
                GoVertical = null;
            }
        }

        //Checks whether to unlock the camera or not - check if all designated objects are destroyed
        //To create an enemy or other object after the camera unlocks, simply place the object at the offset of the stop point and make it spawn at the stop point that would be 2 after
        //The same holds true for creating more enemies after one set at a stop point are defeated; the only difference is the next stop point is in the same spot
        private void UnlockCamera()
        {
            //If the camera is currently locked, all the designated objects (enemies in most instances) are gone, and there are stoppoints left, unlock the camera and allow the player(s) to move onto the next stoppoint
            if (IsLocked == true && StopPoints.Count > 0 && DesignatedAlive == false)
            {
                //Unlock the camera
                IsLocked = false;

                Vector2 curstopoffset = StopPoints.Peek().TrueLocation;

                if (curstopoffset.X != CameraLocation.X)
                {
                    GoHorizontal = new Animation(LoadGraphics.LevelGoAnim, false, 2, new int[] { 0, 1 }, new SoundEffect[] { LoadSounds.Go, LoadSounds.Go }, 500);
                    GoHorizontal.Reset(Main.GetActiveTime);
                    GoHorizontalRotation = curstopoffset.X > CameraLocation.X ? 0f : Math.PI;
                }
                else GoHorizontal = null;

                if (curstopoffset.Y != CameraLocation.Y)
                {
                    GoVertical = new Animation(LoadGraphics.LevelGoAnim, false, 2, 500);

                    //Don't set sounds to the vertical Go animation if the horizontal one is already up - prevents the sound from playing twice
                    if (GoHorizontal == null) GoVertical.SetSounds(new int[] { 0, 1 }, new SoundEffect[] { LoadSounds.Go, LoadSounds.Go });
                    GoVertical.Reset(Main.GetActiveTime);
                    GoVerticalRotation = curstopoffset.Y > CameraLocation.Y ? (Math.PI / 2) : ((3 * Math.PI) / 2);
                }
                else GoVertical = null;
            }
        }

        //Creates an enemy when the offset reaches a certain point
        //private void CreateEnem(List<Player> Players, Vector2 create, List<Enemy> EnemList, List<Weapon> WeaponList, TileEngine TileEngine)
        //{
        //    if (CheckDirectionX(CameraLocation.X, create.X, Manage) == true && CheckDirectionY(CameraLocation.Y, create.Y, Manage) == true)
        //    {
        //        while (Manage.EnemEmpty() == false && Manage.CurrentEnem().StopNumber == StopCount && (Manage.CurrentEnem().AppearLocation.X == create.X && Manage.CurrentEnem().AppearLocation.Y == create.Y))
        //        {
        //            //Check if the game's difficulty is too low to create the next enemy
        //            if (Manage.CurrentEnem().Difficulty > Main.Difficulty || Manage.CurrentEnem().MinPlayers > Players.Count)
        //            {
        //                Manage.ContinueEnem();
        //                continue;
        //            }
        //
        //            EnemList.Add(Manage.CurrentEnem().NewObject);
        //
        //            EnemList[EnemList.Count - 1].Designated = Manage.CurrentEnem().Designated;
        //            if (Manage.CurrentEnem().Designated == true) DesignatedRemaining++;
        //            if (EnemList[EnemList.Count - 1].IsBoss() == true) LoadSounds.PlayBossMusic();
        //
        //            //If the enemy spawns with a weapon (Ex. Weapon Wielders) then add that weapon to the weapon list
        //            if (WeaponList != null && EnemList[EnemList.Count - 1].gsWeapon != null) WeaponList.Add(EnemList[EnemList.Count - 1].gsWeapon);
        //
        //            Manage.ContinueEnem();
        //            GoHorizontal = null;
        //            GoVertical = null;
        //        }
        //    }
        //}

        //Creates a container when the offset reaches a certain point
        //private void CreateCont(List<Player> Players, Vector2 create, List<ItemContainer> ContainerList, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    if (CheckDirectionX(CameraLocation.X, create.X, Manage) == true && CheckDirectionY(CameraLocation.Y, create.Y, Manage) == true)
        //    {
        //        while (Manage.ContEmpty() == false && (Manage.CurrentContainer().StopNumber == StopCount || Manage.CurrentContainer().StopNumber == null) && (Manage.CurrentContainer().AppearLocation.X == create.X && Manage.CurrentContainer().AppearLocation.Y == create.Y))
        //        {
        //            //Check if the game's difficulty is too low to create the next container
        //            if (Manage.CurrentContainer().Difficulty > Main.Difficulty || Manage.CurrentContainer().MinPlayers > Players.Count)
        //            {
        //                Manage.ContinueCont();
        //                continue;
        //            }
        //
        //            ContainerList.Add(Manage.CurrentContainer().NewObject);
        //            Solids.Add(Manage.CurrentContainer().NewObject);
        //
        //            ContainerList[ContainerList.Count - 1].Designated = Manage.CurrentContainer().Designated;
        //            if (Manage.CurrentContainer().Designated == true) DesignatedRemaining++;
        //
        //            Manage.ContinueCont();
        //            GoHorizontal = null;
        //            GoVertical = null;
        //        }
        //    }
        //}

        //Creates a hazard when the offset reaches a certain point
        //private void CreateHazard(List<Player> Players, Vector2 create, List<Hazard> Hazards, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    if (CheckDirectionX(CameraLocation.X, create.X, Manage) == true && CheckDirectionY(CameraLocation.Y, create.Y, Manage) == true)
        //    {
        //        while (Manage.HazardEmpty() == false && (Manage.CurrentHazard().StopNumber == StopCount || Manage.CurrentHazard().StopNumber == null) && (Manage.CurrentHazard().AppearLocation.X == create.X && Manage.CurrentHazard().AppearLocation.Y == create.Y))
        //        {
        //            //Check if the game's difficulty is too low to create the next hazard
        //            if (Manage.CurrentHazard().Difficulty > Main.Difficulty || Manage.CurrentHazard().MinPlayers > Players.Count)
        //            {
        //                Manage.ContinueHazard();
        //                continue;
        //            }
        //
        //            Hazards.Add(Manage.CurrentHazard().NewObject);
        //            Collideable hazard = Manage.CurrentHazard().NewObject as Collideable;
        //            if (hazard != null)
        //                Solids.Add(hazard);
        //
        //            Hazards[Hazards.Count - 1].Designated = Manage.CurrentHazard().Designated;
        //            if (Manage.CurrentHazard().Designated == true) DesignatedRemaining++;
        //
        //            Manage.ContinueHazard();
        //            GoHorizontal = null;
        //            GoVertical = null;
        //        }
        //    }
        //}

        //Update the Go animation
        private void UpdateGo(Animation Go)
        {
            if (Go != null)
            {
                if (Go.IsAnimationEnd() == false)
                    Go.Update(Main.GetActiveTime);
                else
                {
                    if ((Main.GetActiveTime - Go.GPrevFrame) >= BetweenGoTimer)
                        Go.Reset(Main.GetActiveTime);
                }
            }
        }

        //Updates the camera - display is constant
        public virtual void Update(SubLevel level)//List<Player> Players, ref Vector2 OffSet, List<Enemy> EnemList, List<ItemContainer> ContainerList, List<Weapon> WeaponList, List<Hazard> Hazards, TileEngine TileEngine, List<Collideable> Solids)
        {
            //If there are no more StopPoints left, the level should be over
            if (StopPoints.Count > 0)
            {   
                //If the camera isn't locked, check if it should move
                if (IsLocked == false)
                {
                    if (Spawns.Count > 0)
                        SpawnGroup(level); 

                    MoveCamera(level);
                }
                //Otherwise check if it should be unlocked
                else UnlockCamera();

                //Update Go animations
                UpdateGo(GoHorizontal);
                UpdateGo(GoVertical);
            }
        }

        //Adds the objects in a spawn to the level
        private void AddSpawnToLevel(Spawn spawn, SubLevel level)
        {
            level.AddObjectList<Enemy>(spawn.GetAllEnemies, level.GetEnemies);
            level.AddObjectList<ItemContainer>(spawn.GetAllContainers, level.GetContainers);
            level.AddObjectList<Hazard>(spawn.GetAllHazards, level.GetHazards);
            level.AddObjectList<Item>(spawn.GetAllItems, level.GetItems);
            level.AddObjectList<Weapon>(spawn.GetAllWeapons, level.GetWeapons);
        }

        //Spawns a Spawn (group of objects) based on the current stop point
        private void SpawnGroup(SubLevel level)
        {
            for (int i = 0; i < Spawns.Peek().Count; i++)
            {
                Spawn spawn = Spawns.Peek()[i];

                if (CheckSpawnX(spawn) == true && CheckSpawnY(spawn) == true)
                {
                    //Spawn the Spawn!
                    AddSpawnToLevel(spawn, level);

                    //Remove the spawn from the current Spawn list
                    Spawns.Peek().RemoveAt(i);
                    i--;
                }
            }
        }

        private void DrawGo(SpriteBatch spriteBatch, double GoRotation, Animation Go)
        {
            if (Go != null && Go.IsAnimationEnd() == false)
            {
                Vector2 DrawLoc;

                if (Math.Cos(GoRotation) == 1) DrawLoc = new Vector2(Main.ScreenSize.X * .875f, Main.ScreenHalf.Y);
                else if (Math.Cos(GoRotation) == -1) DrawLoc = new Vector2((Main.ScreenHalf.X / 4), Main.ScreenHalf.Y);
                else if (Math.Sin(GoRotation) == -1) DrawLoc = new Vector2(Main.ScreenHalf.X, 40);
                else DrawLoc = new Vector2(Main.ScreenHalf.X, (Main.ScreenSize.Y / 1.2f));

                Go.Draw(spriteBatch, DrawLoc, false, Color.White, new Vector2(LoadGraphics.LevelGoAnim.Width / 4, LoadGraphics.LevelGoAnim.Height / 2), Go.CurrentFrame != 0 ? (float)GoRotation : 0f, .9972f);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch, Vector2 OffSet)
        {
            spriteBatch.Draw(BG, new Rectangle((int)(CameraOrigin.X - CameraLocation.X), (int)(CameraOrigin.Y - CameraLocation.Y), BG.Width, BG.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);

            if (StopPoints.Count > 0)
            {
                DrawGo(spriteBatch, GoHorizontalRotation, GoHorizontal);
                DrawGo(spriteBatch, GoVerticalRotation, GoVertical);
            }
            //spriteBatch.Draw(FG, new Rectangle((int) (Location.X + OffSet.X), (int) (Location.Y + OffSet.Y), FG.Width, FG.Height), Color.White);
        }
    }
}