﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The Player Options screen where players can see how many continues and lives they have as well as give lives to other players (dependent on who is controlling the pause screen) or drop out of the game
    public class PlayerOptionsScreen : PauseScreen
    {
        //Level reference
        private Level Level;

        //Player list reference
        private List<Player> Players;

        //Player number list; allows displaying the player numbers easier
        private List<int> PlayerNums;

        //The amount of lives given to each other player
        private int[] LivesGiven;

        //Whether the player wants to drop out of the game or not
        private bool DroppedOut;

        public PlayerOptionsScreen(int playercontrolling, int playercontrollingindex, Level level, List<Player> players)
        {
            Level = level;

            Players = players;
            PlayerNums = new List<int>();

            PlayerControlling = playercontrolling;
            PlayerControllingIndex = playercontrollingindex;

            LivesGiven = new int[Players.Count - 1];
            DroppedOut = false;

            //If there's only one player, don't show how many lives to give or allow the player to drop out or confirm changes (it's pointless since there's nothing to confirm)
            if (Players.Count > 1)
            {
                Choices = new String[Players.Count + 2];
                ChoiceLocations = new Vector2[Players.Count + 2];

                //Go through all the players and add the player number of the player to a list and sort the list to display the information in numerical order
                for (int i = 0; i < Players.Count; i++)
                {
                    if (i != PlayerControllingIndex)
                    {
                        PlayerNums.Add(Players[i].GPlayerNum);
                        PlayerNums.Sort();

                        Choices[PlayerNums.Count - 1] = "Give Player " + (Players[i].GPlayerNum + 1) + " Lives: " + LivesGiven[PlayerNums.Count - 1];
                    }
                }

                Choices[Players.Count - 1] = "Drop Out: " + YesNoOption(DroppedOut);
                Choices[Players.Count] = "Confirm Changes";
                Choices[Players.Count + 1] = "Back";
            }
            else
            {
                Choices = new String[Players.Count];
                ChoiceLocations = new Vector2[Players.Count];

                Choices[Players.Count - 1] = "Back";
            }

            SetChoices(new Vector2(Main.ScreenHalf.X - 60, Main.ScreenHalf.Y), 25);
        }

        protected override void UpdateChanges(float activeTime, Main main)
        {
            //These options don't appear if there's only one player
            if (Players.Count > 1)
            {
                //Update the life count
                for (int i = 0; i < LivesGiven.Length; i++)
                {
                    Choices[i] = "Give Player " + (PlayerNums[i] + 1) + " Lives: " + LivesGiven[i];
                }

                //Check if the player plans on dropping out or not
                Choices[Players.Count - 1] = "Drop Out: " + YesNoOption(DroppedOut);
            }
        }

        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            //These options don't appear if there's only one player
            if (Players.Count > 1)
            {
                //Have the controlling player give lives to another player; the controlling player cannot give lives to him or herself
                if (CurChoice >= 0 && CurChoice < (Players.Count - 1))
                {
                    //See how many more lives you can give to players
                    ChangeLimitValue(keypressed, Player.GetActionKey(PlayerControlling, (int)Player.Action.Right), ref LivesGiven[CurChoice], 1, (LivesGiven.Sum() + 1) >= Players[PlayerControllingIndex].GetLives, (LivesGiven[CurChoice] - 1) < 0);
                }
                //Decide whether to drop out or not; if there is only one player remaining, the player shouldn't be able to drop out, otherwise the game will be on the pause screen without any players (and crash because there are no players to give control back to in the list)
                else if (Players.Count > 1 && CurChoice == (Players.Count - 1))
                {
                    DroppedOut = !DroppedOut;

                    LoadSounds.Play(LoadSounds.Choose);
                }
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Player Options Screen to Pause screen; the current player gives the lives selected to each player and, if selected, drops out of the game and hands over control to the first player in the list
            if (Players.Count > 1 && CurChoice == Choices.Length - 2)
            {
                //Go back to the Pause screen
                main.RemoveScreen();

                //Give lives from the current player to each other player
                for (int i = 0; i < LivesGiven.Length; i++)
                {
                    //Don't give lives if there are none to give, and don't let a player give lives to him or herself
                    if (LivesGiven[i] > 0)
                    {
                        //Find the receiving player
                        Player receivingplayer = Player.PlayerOfPlayerNum(Players, PlayerNums[i]);

                        Player.GiveLives(activeTime, Players[PlayerControllingIndex], receivingplayer, LivesGiven[i]);
                    }
                }

                //Remove the current player if he or she no longer desires to play
                if (DroppedOut == true)
                {
                    Level.SetDeadState(PlayerControlling);
                    Level.FreeCostume(Players[PlayerControllingIndex]);
                    //Players[PlayerControllingIndex].DieCompletely();
                    Players.RemoveAt(PlayerControllingIndex);

                    //Hand control of the pause screen over to the first player
                    PauseScreen pausescreen = main.CurScreen() as PauseScreen;
                    if (pausescreen != null)
                    {
                        pausescreen.GetSetPlayerNumControlling = Players[0].GPlayerNum;
                        pausescreen.GetSetPlayerIndexControlling = 0;
                    }
                }

                LoadSounds.Play(LoadSounds.Select);
            }
            //Player Options Screen to Pause screen
            else if (CurChoice == Choices.Length - 1)
            {
                //Go back to the Pause screen
                main.RemoveScreen();

                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            base.Draw(activeTime, spriteBatch);
            
            //Draw the number of continues the players have
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Continues: " + Player.Continues, new Vector2(ChoiceLocations[0].X, ChoiceLocations[0].Y - 60), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
        }
    }
}
