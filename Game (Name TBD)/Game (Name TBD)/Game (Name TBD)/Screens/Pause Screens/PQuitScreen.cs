﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The quit screen that asks if you're sure you want to quit the game after selecting Quit on the pause screen
    public class PQuitScreen : PauseScreen
    {
        public PQuitScreen(int playercontrolling, int playercontrollingindex, int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.QuitScreen;

            PlayerControlling = playercontrolling;
            PlayerControllingIndex = playercontrollingindex;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Quit screen to pause screen
            if (CurChoice == 0)
            {
                main.RemoveScreen();
                main.AddScreen((int)Main.ScreenRef.Pause, PlayerControlling, PlayerControllingIndex);
            }
            //Quit screen to title/challenge screen
            else if (CurChoice == 1)
            {
                if (Main.CheckState(Main.GameState.InGame) == true || Main.CheckState(Main.GameState.InBonus) == true || Main.CheckState(Main.GameState.InRank) == true)
                    main.ResetLevels();
                else main.ResetChallenges();

                main.RemoveScreen();
                LoadSounds.StopMusic();

                LoadSounds.Play(LoadSounds.Select);
                //LoadSounds.PlayMenuMusic(0);
            }
        }

        //public override void Draw(float activeTime, SpriteBatch spriteBatch)
        //{
        //    spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
        //    DrawChoices(spriteBatch);
        //}
    }
}
