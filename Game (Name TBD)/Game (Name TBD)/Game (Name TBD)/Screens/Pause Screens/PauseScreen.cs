﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The pause screen when you pause while ingame or in a challenge level
    public class PauseScreen : Screen
    {
        //The player that is in control of the Pause Screen
        protected int PlayerControlling;
        protected int PlayerControllingIndex;

        public PauseScreen()
        {
            ScreenGraphic = LoadGraphics.PauseScreen;
        }

        public PauseScreen(int playercontrolling, int playercontrollingindex, int locdiff, Vector2 firstchoiceloc, params String[] choices) : this()
        {
            PlayerControlling = playercontrolling;
            PlayerControllingIndex = playercontrollingindex;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Gets or sets the player number of the player controlling the screen
        public int GetSetPlayerNumControlling
        {
            get { return PlayerControlling; }
            set { PlayerControlling = value; }
        }

        //Gets or sets the player index in the player list of the player controlling the screen
        public int GetSetPlayerIndexControlling
        {
            get { return PlayerControllingIndex; }
            set { PlayerControllingIndex = value; }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Continue game
            if (CurChoice == 0)
            {
                main.RemoveScreen();
                Main.UnPauseGame();
                MediaPlayer.Resume();
                LoadSounds.Play(LoadSounds.Select);
            }
            //Pause to options screen if inchallenge is false or the choice isn't "Restart"; pause to restart challenge if inchallenge is true and the choice is "Restart"
            else if (CurChoice == 1)
            {
                if (Main.CheckState(Main.GameState.InChallenge) == false || Choices[CurChoice] != "Restart")
                    main.AddScreen((int)Main.ScreenRef.Options);
                else
                {
                    main.RestartChallenges();
                    main.RemoveScreen();
                    Main.UnPauseGame();
                    MediaPlayer.Resume();
                }
                
                LoadSounds.Play(LoadSounds.Select);
            }
            //Pause to quit screen if ingame is false; pause to autosave (title screen) if ingame is true; pause to options if option 2 is "Restart"
            else if (CurChoice == 2)
            {
                if (Main.CheckState(Main.GameState.InGame) == false)
                {
                    if (Choices[1] != "Restart")
                    {
                        main.RemoveScreen();
                        main.AddScreen((int)Main.ScreenRef.PQuit, PlayerControlling, PlayerControllingIndex);
                    }
                    else main.AddScreen((int)Main.ScreenRef.Options);

                    LoadSounds.Play(LoadSounds.Select);
                }
                //Don't let the player autosave again if all of them were used or the player is at level 1
                else if (main.GetCurrentLevel().GetLevelNum > 0 && main.GetAutosaved() == false)
                {
                    //Don't remove the autosave if the player chose to use the autosave
                    main.ResetLevels(true);
                    main.RemoveScreen();
                    LoadSounds.StopMusic();

                    LoadSounds.Play(LoadSounds.Select);
                }
            }
            //Pause to player settings screen if ingame is true
            else if (Main.CheckState(Main.GameState.InGame) == true && CurChoice == 3)
            {
                main.AddScreen((int)Main.ScreenRef.POptions, PlayerControlling, PlayerControllingIndex);

                LoadSounds.Play(LoadSounds.Select);
            }
            //Pause to quit screen if ingame is true
            else if (CurChoice == Choices.Length - 1)
            {
                main.RemoveScreen();
                main.AddScreen((int)Main.ScreenRef.PQuit, PlayerControlling, PlayerControllingIndex);

                LoadSounds.Play(LoadSounds.Select);
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            //Check the keys of the current player in control
            if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerControlling, (int)Player.Action.Up)) == true)
            {
                CurChoice--;
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerControlling, (int)Player.Action.Down)) == true)
            {
                CurChoice++;
                LoadSounds.Play(LoadSounds.Choose);
            }
            //Give lives on the Player Options Screen
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerControlling, (int)Player.Action.Left)) == true)
            {
                SwitchOption(activeTime, Player.GetActionKey(PlayerControlling, (int)Player.Action.Left), main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerControlling, (int)Player.Action.Right)) == true)
            {
                SwitchOption(activeTime, Player.GetActionKey(PlayerControlling, (int)Player.Action.Right), main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerControlling, (int)Player.Action.Pause)) == true)
            {
                ChooseOption(activeTime, main);
            }

            if (CurChoice < 0)
                CurChoice = Choices.Length - 1;
            else if (CurChoice >= Choices.Length)
                CurChoice = 0;
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            DrawChoices(spriteBatch);
        }
    }
}
