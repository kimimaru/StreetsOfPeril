﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //A results screen that displays information after a game mode; it's shown after each level in Rank Mode and after each challenge
    /*Method of adding scores:
     *Add the screen before calculating the ranking and pass in the current and current best scores
     *If the best score changes after the calculation, say that it's a new record*/
    public class ResultsScreen : Screen
    {
        //Multiple graphics to draw
        private Texture2D[] Graphics;

        //The locations to draw the graphics
        private Vector2[] GraphicsLoc;

        //The name of the game mode that was just completed
        private String ModeName;

        //How large the text for each choice should be
        private float[] Scales;

        //Checks if a new record was obtained
        private bool NewRecord;

        public ResultsScreen(Texture2D Graphic, String modename, float[] scales, Vector2[] choicelocations, params String[] choices)
        {
            ScreenGraphic = Graphic;
            Choices = choices;
            ChoiceLocations = choicelocations;
            NewRecord = Choices[Choices.Length - 1] == "New Record!";

            ModeName = modename;
            Scales = scales;
        }

        //Constructor for Versus mode's results screen
        public ResultsScreen(String modename, Texture2D[] icons, Vector2[] graphicsloc, Vector2[] choicelocations, params String[] choices)
        {
            ScreenGraphic = null;

            //Icons and icon locations
            Graphics = icons;
            GraphicsLoc = graphicsloc;

            //Text
            Choices = choices;
            ChoiceLocations = choicelocations;

            ModeName = modename;
            Scales = null;
        }

        public override void Update(Main main, float activeTime)
        {
            //Play the sound for a new record if a new record was obtained
            if (NewRecord == true)
            {
                LoadSounds.Play(LoadSounds.NewRecord);
                NewRecord = false;
            }

            //Go back to the previous screen when the player presses Enter
            if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                main.RemoveScreen();
                LoadSounds.StopMusic();
            }

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            if (ScreenGraphic != null) spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f); 
            if (String.IsNullOrEmpty(ModeName) == false) spriteBatch.DrawString(LoadGraphics.HUDFont, ModeName, new Vector2(Main.ScreenHalf.X - (int)(LoadGraphics.HUDFont.MeasureString(ModeName).X / 2f), 5), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);

            if (Choices != null)
            {
                for (int i = 0; i < Choices.Length; i++)
                {
                    float scale = 1f;
                    if (Scales != null && i < Scales.Length) scale = Scales[i];
                    spriteBatch.DrawString(LoadGraphics.HUDFont, Choices[i], ChoiceLocations[i], Choices[i] != "New Record!" ? Color.Black : Color.Blue, 0f, Vector2.Zero, scale, SpriteEffects.None, .998f);
                }
            }

            if (Graphics != null)
            {
                for (int i = 0; i < Graphics.Length; i++)
                {
                    float graphicabove = 0f;
                    if (i > 0 && GraphicsLoc[i] == GraphicsLoc[i - 1]) graphicabove = .0001f;
                    spriteBatch.Draw(Graphics[i], GraphicsLoc[i], null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .997f + graphicabove);
                }
            }

        }
    }
}
