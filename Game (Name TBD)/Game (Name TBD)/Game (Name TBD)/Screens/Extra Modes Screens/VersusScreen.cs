﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The versus screen, where you can specify the settings for and choose to play Versus Mode (2-4 players)
    //Customization options include: Teams, FFA, Avoid Enemy, Score, starting Health, Lives, starting Status
    public class VersusScreen : Screen
    {
        //The game modes
        public enum GameModes
        {
            Standard, Avoid, Score
        };

        //The three screens that make up the Versus screen
        private Screen[] VersusScreens;
        private int CurScreen;

        //For moving the menu selections quickly
        //private float PrevChange;

        public VersusScreen()
        {
            VersusScreens = new Screen[3] { new VsBeginScreen(this), new VsCustomizeScreen(this), new VsPlayerScreen(this) };
            CurScreen = 0;
        }

        public VersusScreen(int locdiff, Vector2 firstchoiceloc, Vector2 customchoiceloc, params String[] choices) : this()
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            //PrevChange = 0f;
        }

        //These accessors get the three different screens that make up the Versus screen - for use in each of the screens to get values from the other screens
        private VsBeginScreen BeginScreen
        {
            get { return (VsBeginScreen)VersusScreens[0]; }
        }

        private VsCustomizeScreen CustomizeScreen
        {
            get { return (VsCustomizeScreen)VersusScreens[1]; }
        }

        private VsPlayerScreen PlayerScreen
        {
            get { return (VsPlayerScreen)VersusScreens[2]; }
        }

        //Converts the numerical value of the team color into the name of the team color
        public static String TeamColorName(int team)
        {
            switch (team)
            {
                case 0: return "Red";
                case 1: return "Green";
                default: return "Blue";
            }
        }

        public override void Update(Main main, float activeTime)
        {
            VersusScreens[CurScreen].Update(main, activeTime);
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            VersusScreens[CurScreen].Draw(activeTime, spriteBatch);
        }

        //The separate screens for the Versus screen

        //This screen lets you start Versus mode, change settings for Versus mode, and exit the Versus screen
        protected class VsBeginScreen : Screen
        {
            //A reference to the Versus screen
            private VersusScreen VerseScreen;

            public VsBeginScreen(VersusScreen vsscreen)
            {
                VerseScreen = vsscreen;

                ScreenGraphic = LoadGraphics.ChallengeScreen;
                Choices = new String[] { "Begin", "Customize", "Exit" };
                ChoiceLocations = new Vector2[Choices.Length];

                SetChoices(new Vector2(Main.ScreenHalf.X - 60, Main.ScreenHalf.Y), 30);
            }

            private Player CreatePlayer(int value, int index)
            {
                int lives = VerseScreen.CustomizeScreen.CurMode != (int)GameModes.Score ? VerseScreen.PlayerScreen.VersusLives[index] : 1;

                switch (value)
                {
                    case 1: return new Graham(VerseScreen.PlayerScreen.VersusHealth[index], VerseScreen.PlayerScreen.VersusHealthBars[index], index, 0, VerseScreen.CustomizeScreen.CurMode == (int)GameModes.Avoid);
                    case 2: return new Wil(VerseScreen.PlayerScreen.VersusHealth[index], VerseScreen.PlayerScreen.VersusHealthBars[index], index, 0, VerseScreen.CustomizeScreen.CurMode == (int)GameModes.Avoid);
                    case 3: return new Crystal(VerseScreen.PlayerScreen.VersusHealth[index], VerseScreen.PlayerScreen.VersusHealthBars[index], index, 0, VerseScreen.CustomizeScreen.CurMode == (int)GameModes.Avoid);
                    default: return new Jeff(VerseScreen.PlayerScreen.VersusHealth[index], VerseScreen.PlayerScreen.VersusHealthBars[index], index, 0, VerseScreen.CustomizeScreen.CurMode == (int)GameModes.Avoid);
                }
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                //Versus screen to Versus
                //NOTE: Find a way to make this lead into the Character Selection Screen so it's easier to choose characters - their stats would then be set in the Versus constructor
                //The problem may be checking teams and stuff; see what you can do
                if (CurChoice == 0)
                {
                    //Check if there are at least 2 players
                    int chars = 0;

                    List<Player> players = new List<Player>();
                    List<Status> spawnstatus = new List<Status>();

                    //Check the character teams; if all the characters are on the same team, don't begin the game
                    int samecolor = VerseScreen.PlayerScreen.TeamColors[0];
                    bool allsameteam = true;
                    for (int i = 0; i < VerseScreen.PlayerScreen.CurCharacters.Length; i++)
                    {
                        if (VerseScreen.PlayerScreen.CurCharacters[i] != 0) 
                        {
                            chars++;
                            if (VerseScreen.PlayerScreen.TeamColors[i] != samecolor) allsameteam = false;

                            int lives = VerseScreen.CustomizeScreen.CurMode != (int)GameModes.Score ? VerseScreen.PlayerScreen.VersusLives[i] : 1;

                            players.Add(CreatePlayer(VerseScreen.PlayerScreen.CurCharacters[i], i));
                            players[players.Count - 1].SetStats(null, null, lives, null, null, null);

                            spawnstatus.Add(new Status(VerseScreen.PlayerScreen.VersusStatus[i], Status.InfiniteStatus, 100));
                        }
                    }

                    //If the proper conditions to start the game are not met, don't start the game
                    if (chars < 2 || chars >= 2 && VerseScreen.CustomizeScreen.Teams == true && allsameteam == true) return;

                    bool avoidenemy = VerseScreen.CustomizeScreen.CurMode == (int)GameModes.Avoid;

                    //Make sure to still pass in the team colors if team attack is on because you have to know which team won in the end
                    main.SetUpChallenges(new Versus(LoadGraphics.BG/*LoadGraphics.BG[LevelSetting]*/, VerseScreen.CustomizeScreen.VersusTime > 24 ? VerseScreen.CustomizeScreen.VersusTime : -1, VerseScreen.CustomizeScreen.DamageRatio, VerseScreen.CustomizeScreen.ScoreLimit, VerseScreen.CustomizeScreen.CurMode, VerseScreen.CustomizeScreen.LevelSetting, VerseScreen.CustomizeScreen.TeamAttack, VerseScreen.CustomizeScreen.Teams == true ? VerseScreen.PlayerScreen.TeamColors : null, spawnstatus.ToArray(), players.ToArray()));
                    LoadSounds.PlayChallengeMusic(4);
                    LoadSounds.Play(LoadSounds.Select);
                }
                //Versus Begin Screen to Versus Customization Screen
                else if (CurChoice == 1)
                {
                    VerseScreen.CurScreen = 1;
                    LoadSounds.Play(LoadSounds.Select);
                }
                //Versus screen to extra modes screen
                else if (CurChoice == 2)
                {
                    main.RemoveScreen();
                    LoadSounds.Play(LoadSounds.Select);
                }
            }
        }

        //This screen lets you customize general Versus mode settings such as Game Mode, Damage Ratio, and more
        protected class VsCustomizeScreen : Screen
        {
            //A reference to the Versus screen
            private VersusScreen VerseScreen;

            //The amount of time the players have to fight each other
            public int VersusTime;

            //The damage ratio (from .5 to 2) - multiplies damage done by a specific amount
            public float DamageRatio;

            //The score needed to win the game in the Score game mode
            public int ScoreLimit;

            //Other values, like game mode (Standard Battle, Avoid Enemy, Score) - Players can choose teams after starting if teams is selected
            public String[] Mode;
            public int CurMode;

            //Determines if teams are enabled or not
            public bool Teams;

            //Determines if players in teams can attack their own team members
            public bool TeamAttack;

            //The setting where you fight; this determines the background used in the mode
            public int LevelSetting;

            public VsCustomizeScreen(VersusScreen vsscreen)
            {
                VerseScreen = vsscreen;

                ScreenGraphic = LoadGraphics.ChallengeScreen;
                Choices = new String[] { "Time: 99", String.Empty, "Mode: Standard Battle", "Teams: Off", "Team Attack: On", "Level: 1", "Damage Ratio: 1", "Players", "Exit" };
                ChoiceLocations = new Vector2[Choices.Length];

                SetChoices(new Vector2(Main.ScreenHalf.X - 60, (Main.ScreenHalf.Y / 2) - 10), 25);

                VersusTime = 99;

                Mode = new String[] { "Standard Battle", "Avoid Enemy", "Score" };
                CurMode = (int)GameModes.Standard;
                DamageRatio = 1f;
                ScoreLimit = 3;
                LevelSetting = 0;

                Teams = false;
                TeamAttack = true;
            }

            protected override void UpdateChanges(float activeTime, Main main)
            {
                String score = ScoreLimit == 0 ? "None" : Convert.ToString(ScoreLimit);

                Choices[0] = "Time: " + (VersusTime == 24 ? "Infinite" : Convert.ToString(VersusTime));
                Choices[1] = (CurMode != (int)GameModes.Score ? String.Empty : "Score to win: " + score);
                Choices[2] = "Mode: " + Mode[CurMode];
                Choices[3] = "Teams: " + (Teams == true ? "On" : "Off");
                Choices[4] = "Team Attack: " + (TeamAttack == true ? "On" : "Off");
                Choices[5] = "Level: " + (LevelSetting + 1);
                Choices[6] = "Damage Ratio: " + Convert.ToString(Math.Round(DamageRatio, 2));
            }

            protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
            {
                //Change how much time you have to fight
                if (CurChoice == 0)
                {
                    ChangeLimitValue(keypressed, Keys.Right, ref VersusTime, 1, 99, 24);
                }
                //Change the score requirement to win in the Score game mode
                else if (CurMode == (int)GameModes.Score && CurChoice == 1)
                {
                    ChangeLimitValue(keypressed, Keys.Right, ref ScoreLimit, 1, 99, 0);
                }
                //Change the current mode
                else if (CurChoice == 2)
                {
                    ChangeLimitValue(keypressed, Keys.Right, ref CurMode, 1, Mode.Length - 1, 0);
                }
                //Toggle teams on or off
                else if (CurChoice == 3)
                {
                    Teams = !Teams;

                    LoadSounds.Play(LoadSounds.Choose);
                }
                //Toggle team attack on or off
                else if (CurChoice == 4)
                {
                    TeamAttack = !TeamAttack;

                    LoadSounds.Play(LoadSounds.Choose);
                }
                //Change the level BG
                else if (CurChoice == 5)
                {
                    ChangeLimitValue(keypressed, Keys.Right, ref LevelSetting, 1, 7, 0);
                }
                //Change the damage ratio
                else if (CurChoice == 6)
                {
                    if (keypressed == Keys.Right)
                        DamageRatio = Math.Round(DamageRatio + .1f, 2) > 2f ? DamageRatio : DamageRatio = (float)Math.Round(DamageRatio + .1f, 2);
                    else DamageRatio = Math.Round(DamageRatio - .1f, 2) < .5f ? DamageRatio : DamageRatio = (float)Math.Round(DamageRatio - .1f, 2);

                    LoadSounds.Play(LoadSounds.Choose);
                }
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                //Versus Customize Screen to Versus Player Screen
                if (CurChoice == 7)
                {
                    VerseScreen.CurScreen = 2;
                    LoadSounds.Play(LoadSounds.Select);
                }
                //Versus Customize Screen to Versus Begin Screen
                else if (CurChoice == 8)
                {
                    CurChoice = 0;
                    VerseScreen.CurScreen = 0;
                    LoadSounds.Play(LoadSounds.Select);
                }
            }

            protected override void ChangeOptions(float activeTime, Main main)
            {
                int curchoice = CurChoice;
                base.ChangeOptions(activeTime, main);

                //If the game mode isn't Score, don't let players go to the score options or change the score
                if (CurMode != (int)GameModes.Score && CurChoice == 1)
                {
                    CurChoice += CurChoice > curchoice ? 1 : -1;
                }
            }
        }

        //This screen lets you customize player-related settings in Versus mode, such as Character, starting Status, Teams/Team Color, and more
        protected class VsPlayerScreen : Screen
        {
            //A reference to the Verses screen
            private VersusScreen VerseScreen;

            //The previous selection, used when pressing up or down from Exit
            private int PrevSelection;

            //The character of each player
            public String[] Characters;
            public int[] CurCharacters;

            //Some starting values for each player
            public int[] VersusHealth;
            public int[] VersusHealthBars;
            public int[] VersusLives;
            public int[] VersusStatus;

            //The team colors of the players
            public int[] TeamColors;

            public VsPlayerScreen(VersusScreen vsscreen)
            {
                VerseScreen = vsscreen;

                ScreenGraphic = LoadGraphics.ChallengeScreen;
                PrevSelection = 0;
                Characters = new String[5] { "N/A", "Graham", "Wil", "Crystal", "Jeff" };
                CurCharacters = new int[4] { 0, 0, 0, 0 };

                VersusHealth = new int[4] { 100, 100, 100, 100 };
                VersusHealthBars = new int[4] { 1, 1, 1, 1 };
                VersusLives = new int[4] { 1, 1, 1, 1 };
                VersusStatus = new int[4] { (int)Status.Statuses.None, (int)Status.Statuses.None, (int)Status.Statuses.None, (int)Status.Statuses.None };

                TeamColors = new int[4] { 0, 0, 0, 0 };

                List<String> choices = new List<String>();

                //Set up the vertical columns for the information for each character
                for (int i = 0; i < CurCharacters.Length; i++)
                {
                    choices.Add("N/A");
                    choices.Add("Lives: " + VersusLives[i]);
                    choices.Add("Health: " + VersusHealth[i]);
                    choices.Add("Healthbars: " + VersusHealthBars[i]);
                    choices.Add("Status: " + Status.StatName(VersusStatus[i]));
                    choices.Add("Team: " + TeamColorName(TeamColors[i]));
                }

                choices.Add("Exit");
                Choices = choices.ToArray();
                SetChoices();
            }

            private void SetChoices()
            {
                ChoiceLocations = new Vector2[Choices.Length];

                int modifier = 0;
                for (int i = 0; i < CurCharacters.Length; i++)
                {
                    int imodifier = i + modifier;

                    ChoiceLocations[imodifier] = new Vector2(40 + (modifier * 30), 80);
                    ChoiceLocations[imodifier + 1] = new Vector2(40 + (modifier * 30), 110);
                    ChoiceLocations[imodifier + 2] = new Vector2(40 + (modifier * 30), 140);
                    ChoiceLocations[imodifier + 3] = new Vector2(40 + (modifier * 30), 170);
                    ChoiceLocations[imodifier + 4] = new Vector2(40 + (modifier * 30), 200);
                    ChoiceLocations[imodifier + 5] = new Vector2(40 + (modifier * 30), 230);

                    modifier += 5;
                }

                ChoiceLocations[ChoiceLocations.Length - 1] = new Vector2(Main.ScreenHalf.X, 300);
            }

            protected override void UpdateChanges(float activeTime, Main main)
            {
                for (int i = 0; i < CurCharacters.Length; i++)
                {
                    int index = i * 6;

                    Choices[index] = Characters[CurCharacters[i]];
                    Choices[index + 1] = "Lives: " + (VerseScreen.CustomizeScreen.CurMode != (int)GameModes.Score ? VersusLives[i].ToString() : "Infinite");
                    Choices[index + 2] = "Health: " + VersusHealth[i];
                    Choices[index + 3] = "Healthbars: " + VersusHealthBars[i];
                    Choices[index + 4] = "Status: " + Status.StatName(VersusStatus[i]);
                    Choices[index + 5] = VerseScreen.CustomizeScreen.Teams == true ? "Team: " + TeamColorName(TeamColors[i]) : "Team: None";
                }
            }

            protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
            {
                //Change the character the player is
                if ((CurChoice % 6) == 0)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref CurCharacters[CurChoice / 6], 1, 4, 0);
                }
                //Change how many lives you start out with
                else if (VerseScreen.CustomizeScreen.CurMode != (int)GameModes.Score && (CurChoice % 6) == 1)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref VersusLives[CurChoice / 6], 1, 9, 1);
                }
                //Change how much health you start out with
                else if ((CurChoice % 6) == 2)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref VersusHealth[CurChoice / 6], 1, 100, 1);
                }
                //Change how many health bars you start out with
                else if ((CurChoice % 6) == 3)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref VersusHealthBars[CurChoice / 6], 1, 3, 1);
                }
                //Change the status you start out with
                else if ((CurChoice % 6) == 4)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref VersusStatus[CurChoice / 6], 1, (int)Status.Statuses.SpeedDown, (int)Status.Statuses.None);
                }
                //Change the team the characters are on
                else if (VerseScreen.CustomizeScreen.Teams == true && (CurChoice % 6) == 5)
                {
                    ChangeLimitValue(keypressed, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], ref TeamColors[CurChoice / 6], 1, 2, 0);
                }
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                //Versus Player Screen to to Versus Customize Screen
                if (CurChoice == (Choices.Length - 1))
                {
                    CurChoice = 0;
                    VerseScreen.CurScreen = 1;
                    LoadSounds.Play(LoadSounds.Select);
                }
            }

            //Cycles through the options available on the screen
            protected override void ChangeOptions(float activeTime, Main main)
            {
                if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
                {
                    if (CurChoice == (Choices.Length - 1))
                        CurChoice = ((PrevSelection * 6) + 5);
                    else if ((CurChoice % 6) == 0)
                    {
                        PrevSelection = (CurChoice / 6);
                        CurChoice = (Choices.Length - 1);
                    }
                    else CurChoice--;
                    LoadSounds.Play(LoadSounds.Choose);
                }
                else if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
                {
                    if (CurChoice == (Choices.Length - 1))
                        CurChoice = (PrevSelection * 6);
                    else if (((CurChoice + 1) % 6) == 0)
                    {
                        PrevSelection = (CurChoice / 6);
                        CurChoice = (Choices.Length - 1);
                    }
                    else CurChoice++;
                    LoadSounds.Play(LoadSounds.Choose);
                }
                else if (CurChoice != (Choices.Length - 1) && Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
                {
                    CurChoice -= 6;
                    if (CurChoice < 0) CurChoice = (6 * (CurCharacters.Length - 1)) + (CurChoice + 6);
                    LoadSounds.Play(LoadSounds.Choose);
                }
                else if (CurChoice != (Choices.Length - 1) && Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
                {
                    CurChoice = (CurChoice + 6) % (6 * CurCharacters.Length);
                    LoadSounds.Play(LoadSounds.Choose);
                }
                //Checks if you pressed the attack button or the jump button - used to change settings
                else if (CurChoice != (Choices.Length - 1) && Input.CheckKeyPress(KeyboardState, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Jump]) == true)
                {
                    SwitchOption(activeTime, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Jump], main);
                }
                else if (CurChoice != (Choices.Length - 1) && Input.CheckKeyPress(KeyboardState, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack]) == true)
                {
                    SwitchOption(activeTime, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], main);
                }
                else if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
                {
                    ChooseOption(activeTime, main);
                }

                if (CurChoice < 0)
                    CurChoice = Choices.Length - 1;
                else if (CurChoice >= Choices.Length)
                    CurChoice = 0;
            }

            private void DrawChoices(SpriteBatch spriteBatch)
            {
                for (int i = 0; i < Choices.Length; i++)
                {
                    Color SelectColor = CurChoice == i ? Color.Green : Color.Black;

                    if (i == (Choices.Length - 1)) spriteBatch.DrawString(LoadGraphics.HUDFont, Choices[i], ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 0) spriteBatch.DrawString(LoadGraphics.HUDFont, Characters[CurCharacters[i / 6]], ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 1) spriteBatch.DrawString(LoadGraphics.HUDFont, "Lives: " + (VerseScreen.CustomizeScreen.CurMode != (int)GameModes.Score ? VersusLives[i / 6].ToString() : "Infinite"), ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 2) spriteBatch.DrawString(LoadGraphics.HUDFont, "Health: " + VersusHealth[i / 6], ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 3) spriteBatch.DrawString(LoadGraphics.HUDFont, "Healthbars: " + VersusHealthBars[i / 6], ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 4) spriteBatch.DrawString(LoadGraphics.HUDFont, "Status: " + Status.StatName(VersusStatus[i / 6]), ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                    else if ((i % 6) == 5) spriteBatch.DrawString(LoadGraphics.HUDFont, VerseScreen.CustomizeScreen.Teams == true ? "Team: " + TeamColorName(TeamColors[i / 6]) : "Team: None", ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                }

                spriteBatch.Draw(LoadGraphics.ScreenSelect, new Vector2(ChoiceLocations[CurChoice].X - 38, ChoiceLocations[CurChoice].Y - 2), null, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
            }

            public override void Draw(float activeTime, SpriteBatch spriteBatch)
            {
                spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
                DrawChoices(spriteBatch);
            }
        }
    }
}
