﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The screen where you select which level in the main game you want to play to get a rank
    public class RankScreen : Screen
    {
        public RankScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.ChallengeScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float ActiveTime, Main main)
        {
            //Rank screen to the Level you choose
            if (CurChoice == 0 || CurChoice == 1 || CurChoice == 2 || CurChoice == 5 || CurChoice == 6)
            {
                main.AddScreen((int)Main.ScreenRef.CharSelect);

                (main.CurScreen() as CharSelectScreen).menuexit = delegate(float activeTime, Player[] player)
                {
                    main.SetUpRankLevelPlayers(activeTime, CurChoice, player);
                    LoadSounds.StopMusic();
                    LoadSounds.PlayLevelMusic(CurChoice);
                };

                LoadSounds.PlaySong((int)LoadSounds.Music.CharSelect, true);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Rank screen to extra modes screen
            else
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }
    }
}
