﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The screen where you choose the extra mode you want to play; examples include Rank Mode and Boss Rush
    public class ExtraModesScreen : Screen
    {
        //Checks if Rank Mode is unlocked
        private bool RankModeUnlocked;

        //Checks if Boss Rush is unlocked (all the rank levels are completed) - move once you get a Boss Rush screen
        private bool BossRushUnlocked;

        //Checks if the Enemy Museum is unlocked
        private bool EnemyMuseumUnlocked;

        public ExtraModesScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.ChallengeScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        public override void CheckUnlocks()
        {
            RankModeUnlocked = SaveLoadData.CheckUnlocked("RankMode");
            BossRushUnlocked = SaveLoadData.CheckUnlocked("BossRush");
            EnemyMuseumUnlocked = SaveLoadData.CheckUnlocked("Museum");
        }

        protected override void ChooseOption(float activeTime, Main main)
        {
            //Extra modes screen to Rank Mode
            if (RankModeUnlocked == true && CurChoice == 0)
            {
                main.AddScreen((int)Main.ScreenRef.Rank);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Extra modes screen to Boss Rush screen
            else if (BossRushUnlocked == true && CurChoice == 1)
            {
                //Get rid of this later! This is just for testing bonus stages right now!
                main.SetUpLevelsPlayers(activeTime, CreateObjects.Graham(0, 0));
                main.StartBonus();

                LoadSounds.Play(LoadSounds.Select);
            }
            //Extra modes screen to Enemy Museum
            else if (EnemyMuseumUnlocked == true && CurChoice == 2)
            {
                main.AddScreen((int)Main.ScreenRef.CharSelect);

                (main.CurScreen() as CharSelectScreen).menuexit = delegate(float activetime, Player[] player)
                {
                    main.SetUpChallenges(new Museum(player[0]));
                    LoadSounds.PlaySong((int)LoadSounds.Music.Museum, true);
                };

                CurChoice = 0;
                LoadSounds.PlaySong((int)LoadSounds.Music.CharSelect, true);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Extra modes screen to versus screen
            else if (CurChoice == 3)
            {
                main.AddScreen((int)Main.ScreenRef.Versus);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Extra modes screen to title screen
            else if (CurChoice == 4)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch, new int[] { 0, 1, 2 }, RankModeUnlocked, BossRushUnlocked, EnemyMuseumUnlocked);
        }
    }
}
