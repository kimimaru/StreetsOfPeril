﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //This is the Data Screen; a screen that displays the high scores in the main game, ranks in rank mode, and ranks and times in challenges; in other words, it summarizes your performance
    //Easter Egg: holding down the key that's the first letter of each character's name (Ex. G for Graham) for a while will make that character walk across the screen; once walking, pressing that key will stop the character, and pressing it again will make the character move again!
    //I'll see later how detailed I want to get with the information, like total # of deaths, damage taken, damage received, etc. (NOT LIKELY)
    public class DataScreen : Screen
    {
        //The maximum number of high scores
        public const int MaxHighScores = 10;

        //Current screen reference; it points to one of the individual screens within the Data Screen
        private Screen CurDataScreen;

        //Easter Egg - Characters walking across the screen
        private NewAnimation[] CharAnims;
        private Keys[] CharKeys;
        private Vector2[] CharLocations;
        private float[] CharSpeeds;
        private bool[] CharStopped;
        private float[] KeyHeld;

        public enum ChallengeRankings
        {
            Survival, Onslaught, BTO, Obstacle, Snipe
        };

        //CurChoice functions as the current type of information you're viewing in this screen

        public DataScreen()
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            CurDataScreen = null;

            Choices = new String[] { "Highscores", "Level Ranks", "Challenge Ranks", "Exit" };
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(new Vector2(Main.ScreenHalf.X - 60, Main.ScreenHalf.Y - 60), 30);

            CharAnims = new NewAnimation[4] { null, null, null, null };
            CharKeys = new Keys[4] { Keys.G, Keys.W, Keys.C, Keys.J };
            CharLocations = new Vector2[4] { new Vector2(-50, Main.ScreenHalf.Y - 60), new Vector2(-50, Main.ScreenHalf.Y - 60), new Vector2(-50, Main.ScreenHalf.Y - 60), new Vector2(-50, Main.ScreenHalf.Y - 60) };
            CharSpeeds = new float[4] { 4f, 3f, 5f, 4f };
            CharStopped = new bool[4];
            KeyHeld = new float[4] { 0f, 0f, 0f, 0f };
        }

        //Called when the current screen exits from itself to enable the player to choose another screen
        public void BackToSelection()
        {
            CurDataScreen = null;
        }

        //Loads the highscores
        public static Highscore[] LoadHighScores()
        {
            //Load in the default high scores
            Highscore[] Scores = new Highscore[10] { new Highscore(100, 0), new Highscore(1000, 1), new Highscore(10000, 2), new Highscore(20000, 3), new Highscore(30000, 0), new Highscore(40000, 1), 
                                           new Highscore(50000, 2), new Highscore(60000, 3), new Highscore(70000, 0), new Highscore(80000, 1) };

            //Go through the save file and find the high scores
            for (int i = 0; i < Scores.Length; i++)
            {
                object highscoredata = SaveLoadData.LoadData("Highscores", "Highscores" + i, "Score");
                object chardata = SaveLoadData.LoadData("Highscores", "Highscores" + i, "Character0");

                //If a high score or character isn't present, save the default values for each (it's normally impossible to have just a character or just a score, so it'd be invalid)
                if (highscoredata == null || chardata == null)
                {
                    SaveLoadData.SaveData("Highscores", "Highscores" + i, "Score", Scores[i].GetHighScore);

                    //Alternate among Graham, Wil, Crystal, and Jeff as the default characters
                    SaveLoadData.SaveData("Highscores", "Highscores" + i, "Character0", Scores[i].GetCharacters[0]);//i % (Enum.GetValues(typeof(Player.Characters)).Length));
                }
                //Neither are null, so load in the highscore
                else
                {
                    List<int> characters = new List<int>();
                    characters.Add(Convert.ToInt32(chardata));

                    //Load the rest of the characters
                    for (int j = 1; ; j++)
                    {
                        chardata = SaveLoadData.LoadData("Highscores", "Highscores" + i, "Character" + j);

                        //If the character was not found or there are more than 4 characters for some reason, break
                        if (chardata == null || characters.Count >= 4) break;
                        //Otherwise, add the character to the character list
                        else characters.Add(Convert.ToInt32(chardata));
                    }

                    //Load in the highscore with the values read from the save file
                    Scores[i] = new Highscore(Convert.ToInt32(highscoredata), characters.ToArray());
                }
            }

            return Scores;
        }

        //Check if the new score at the end of the main game beats a current top score; if it does, move all the other scores accordingly
        //For multiple players, add up all the scores and denote how many players were used for that score (as well as characters)
        public static void CheckNewHighscore(List<Player> Players, Main main)
        {
            Highscore[] Highscores = LoadHighScores();

            int playerscore = 0;

            //Add all the player scores together
            for (int p = 0; p < Players.Count; p++)
                playerscore += Players[p].Score;

            //Unlock (insert unlockable here when decided) when the player(s) get at least 1,000,000 points (number subject to change)
            //if (WhateverIsUnlocked == false && playerscore >= 1000000)
            //{
                //WhateverIsUnlocked = true;
                //SaveLoadData.SaveData("Unlockables", "WhateverIsUnlocked", "Unlocked", WhateverIsUnlocked);
                //Show the congratulatory screen (which will just be the results screen with a graphic)
            //}

            //Go through all the high scores from top to bottom
            for (int i = Highscores.Length - 1; i >= 0; i--)
            {
                //If the new score beats one of the current highscores...
                if (playerscore > Highscores[i].GetHighScore)
                {
                    //Move all the scores the new score beats down and add the new score
                    for (int j = 0; j < i; j++)
                    {
                        //Save the new score order
                        SaveLoadData.SaveData("Highscores", "Highscores" + j, "Score", Highscores[j + 1]);
                        Highscores[j] = Highscores[j + 1];
                    }

                    //Add the new score into the list
                    SaveLoadData.SaveData("Highscores", "Highscores" + i, "Score", playerscore);

                    //Save the characters used to obtain the high score
                    for (int p = 0; p < Players.Count; p++)
                        SaveLoadData.SaveData("Highscores", "Highscores" + p, "Character" + p, Player.IndexOfCharacter(Players[p]));

                    break;
                }
            }
        }

        //Changes the character's animation from walking to standing and vice versa
        private void ChangeCharAnimation(float activeTime, int index, bool created = false)
        {
            //If the character is walking, change to its respective standing animation
            if (created == false && CharStopped[index] == false)
            {
                if (index == 0) CharAnims[index] = new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
                else if (index == 1) CharAnims[index] = new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
                else if (index == 2) CharAnims[index] = new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));
                else if (index == 3) CharAnims[index] = new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0)));

                CharAnims[index].Reset();
                CharStopped[index] = true;
            }
            //If the character is standing or the animations are being created for the first time, change to its respective walking animation
            else
            {
                if (index == 0) CharAnims[index] = new NewAnimation(true, false, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
                else if (index == 1) CharAnims[index] = new NewAnimation(true, false, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
                else if (index == 2) CharAnims[index] = new NewAnimation(true, false, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));
                else if (index == 3) CharAnims[index] = new NewAnimation(true, false, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(240, 9, 46, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(200, 8, 29, 80), 125, new Vector2(2, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(376, 9, 45, 79), 125, new Vector2(1, 0)), new AnimFrame(new Rectangle(336, 8, 28, 80), 125, new Vector2(1, 0)));

                CharAnims[index].Reset();
                CharStopped[index] = false;
            }
        }

        //Change which type of information you want to see with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            //ChangeLimitValue(keypressed, Keys.Right, ref CurChoice, 1, Information.Length - 1, 0);
        }

        protected override void ChooseOption(float activeTime, Main main)
        {
            //Highscores
            if (CurChoice == 0)
                CurDataScreen = new HighscoreScreen(this);
            //Level rankings
            else if (CurChoice == 1)
                CurDataScreen = new RankScoreScreen(this);
            //Challenge rankings
            else if (CurChoice == 2)
                CurDataScreen = new ChallengeScoreScreen(this);
            //Data Screen to title screen
            else
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        private void CharacterAnimationMovement(float activeTime)
        {
            //Check if the button corresponding to each character's first letter of his or her name was held for a while
            for (int i = 0; i < CharKeys.Length; i++)
            {
                if (Input.KeyHeld(CharKeys[i]))
                {
                    if (CharAnims[i] == null)
                    {
                        if (Input.CheckKeyPress(KeyboardState, CharKeys[i]) == true) KeyHeld[i] = activeTime;
                        else if ((activeTime - KeyHeld[i]) >= 5000f) ChangeCharAnimation(activeTime, i, true);
                    }
                    else if (CharLocations[i].X >= 0 && CharLocations[i].X < Main.ScreenSize.X && Input.CheckKeyPress(KeyboardState, CharKeys[i]) == true) ChangeCharAnimation(activeTime, i);
                }
            }

            //Update the character animations and make the characters walk
            for (int i = 0; i < CharAnims.Length; i++)
            {
                if (CharAnims[i] != null)
                {
                    CharAnims[i].Update(activeTime);

                    if (CharStopped[i] == false)
                    {
                        CharLocations[i].X += CharSpeeds[i];
                        if (CharLocations[i].X > (Main.ScreenSize.X + 50))
                        {
                            CharAnims[i] = null;
                            CharLocations[i].X = -50;
                            KeyHeld[i] = activeTime;
                        }
                    }
                }
            }
        }

        public override void Update(Main main, float activeTime)
        {
            CharacterAnimationMovement(activeTime);
            if (CurDataScreen == null)
                base.Update(main, activeTime);
            else CurDataScreen.Update(main, activeTime);

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();
        }

        private void DrawLeftArrow(SpriteBatch spriteBatch)
        {
            //Draw an arrow on the bottom left of the screen indicating that you can go to the left to view more information
            spriteBatch.Draw(LoadGraphics.ScreenSelect, new Vector2(18, 286), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.FlipHorizontally, .996f);
        }

        private void DrawRightArrow(SpriteBatch spriteBatch)
        {
            //Draw an arrow on the bottom right of the screen indicating that you can go to the right to view more information
            spriteBatch.Draw(LoadGraphics.ScreenSelect, new Vector2(Main.ScreenSize.X - LoadGraphics.ScreenSelect.Width - 18, 286), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .996f);
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            if (CurDataScreen == null)
                DrawChoices(spriteBatch);
            else CurDataScreen.Draw(activeTime, spriteBatch);

            //Draw the characters if the easter egg is activated
            for (int i = 0; i < CharAnims.Length; i++)
            {
                if (CharAnims[i] != null) CharAnims[i].Draw(spriteBatch, CharLocations[i], true, Color.White, 0f, .998f);
            }
        }

        //This screen displays the high scores in the main game; this may also be displayed at the end of the game when it shows if you beat any previous high scores or not
        protected class HighscoreScreen : Screen
        {
            //The high scores
            private Highscore[] Highscores;
            private DataScreen DataScreen;

            public HighscoreScreen(DataScreen datascreen)
            {
                Highscores = LoadHighScores();
                DataScreen = datascreen;
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                DataScreen.BackToSelection();
            }

            public override void Update(Main main, float activeTime)
            {
                //If this is also displayed at the end of the game, you will need to be able to press something to exit it; for now do nothing
                if (Input.CheckKeyPress(DataScreen.KeyboardState, Keys.Enter) == true)
                    ChooseOption(activeTime, main);
            }

            public override void Draw(float activeTime, SpriteBatch spriteBatch)
            {
                spriteBatch.DrawString(LoadGraphics.HUDFont, "Highscores", new Vector2((int)(Main.ScreenHalf.X * .75f), -3), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);

                //Draw all the high scores and character icons for the character(s) involved in obtaining the scores (more than one character indicates more players)
                for (int i = Highscores.Length - 1; i >= 0; i--)
                {
                    float ydraw = 28 + (Math.Abs(i - 9) * 30);

                    String highscorenumber = (Math.Abs(i - 9) + 1) + ".";

                    spriteBatch.DrawString(LoadGraphics.HUDFont, highscorenumber.PadLeft(3), new Vector2(133, ydraw), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                    Highscores[i].Draw(spriteBatch, new Vector2(166, ydraw));
                }
            }
        }

        //A highscore structure for easily storing and displaying a highscore and the characters used for the highscore
        public struct Highscore
        {
            //The score
            private int Score;

            //The characters used to get the score
            private int[] Characters;

            public Highscore(int score, params int[] characters)
            {
                Score = score;

                Characters = characters;
            }

            public int GetHighScore
            {
                get { return Score; }
            }

            public int[] GetCharacters
            {
                get { return Characters; }
            }

            public void Draw(SpriteBatch spriteBatch, Vector2 Location)
            {
                //Draw the score
                spriteBatch.DrawString(LoadGraphics.HUDFont, Score.ToString(), Location, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);

                //Draw the characters to the right side after the text; all highscores and their respective characters should be lined up on the same X position
                for (int i = 0; i < Characters.Length; i++)
                {
                    int xdrawloc = (int)(Location.X + 100 + (i * LoadGraphics.CharacterIcons[Characters[i]].Width) + (i * 3));

                    spriteBatch.Draw(LoadGraphics.CharacterIcons[Characters[i]], new Vector2(xdrawloc, Location.Y + (LoadGraphics.CharacterIcons[Characters[i]].Height / 3)), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                }
            }
        }

        //This screen displays how well the player did in each level in Rank Mode
        protected class RankScoreScreen : Screen
        {
            //The level rankings
            private Ranking[] LevelRankings;
            private DataScreen DataScreen;

            public RankScoreScreen(DataScreen datascreen)
            {
                LevelRankings = SaveLoadData.LoadLevelRankings();
                DataScreen = datascreen;
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                DataScreen.BackToSelection();
            }

            public override void Update(Main main, float activeTime)
            {
                if (Input.CheckKeyPress(DataScreen.KeyboardState, Keys.Enter) == true)
                    ChooseOption(activeTime, main);
            }

            public override void Draw(float activeTime, SpriteBatch spriteBatch)
            {
                spriteBatch.DrawString(LoadGraphics.HUDFont, "Rank Mode", new Vector2(145, 10), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);

                //Draw all the rank mode ranks
                for (int i = 0; i < LevelRankings.Length; i++)
                {
                    spriteBatch.DrawString(LoadGraphics.HUDFont, "Level " + (i + 1) + /*": Rank - " + Ranking.LetterValue(LevelRankings[i].GetRank()) +*/ " Score - " + LevelRankings[i].GetScoreTime(), new Vector2(40, 55 + (i * 30)), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                    spriteBatch.Draw(LoadGraphics.LetterGraphic[LevelRankings[i].GetRank()], new Vector2(300, 55 + (i * 30)), null, Color.White, 0f, Vector2.Zero, .5f, SpriteEffects.None, .996f);
                }
            }
        }

        //This screen displays how well the player did in each challenge (only accessible when challenges are unlocked)
        //NOTE: Maybe have a separate screen for each challenge instead?
        protected class ChallengeScoreScreen : Screen
        {
            private DataScreen DataScreen;

            public ChallengeScoreScreen(DataScreen datascreen)
            {
                DataScreen = datascreen;
            }

            protected override void ChooseOption(float activeTime, Main main)
            {
                DataScreen.BackToSelection();
            }

            public override void Update(Main main, float activeTime)
            {
                if (Input.CheckKeyPress(DataScreen.KeyboardState, Keys.Enter) == true)
                    ChooseOption(activeTime, main);
            }

            public override void Draw(float activeTime, SpriteBatch spriteBatch)
            {
                spriteBatch.DrawString(LoadGraphics.HUDFont, "Challenges", new Vector2(170, 5), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);

                for (int i = 0; i < CLevel.ChallengeRanks.Length; i++)
                {
                    for (int j = 0; j < CLevel.ChallengeRanks[i].Length; j++)
                    {
                        String rankdiffnum = Enum.GetName(typeof(CLevel.Challenges), i) + " - " + Enum.GetName(typeof(CLevel.CDifficulty), j);

                        spriteBatch.DrawString(LoadGraphics.HUDFont, rankdiffnum + /*": Rank: " + Ranking.LetterValue(CLevel.ChallengeRanks[i][j].GetRank()) +*/ " Score/Time: " + CLevel.ChallengeRanks[i][j].GetScoreTime(), new Vector2(0, 30 + (i * 60) + (j * 15)), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .993f);
                        spriteBatch.Draw(LoadGraphics.LetterGraphic[CLevel.ChallengeRanks[i][j].GetRank()], new Vector2(380, 35 + (i * 60) + (j * 15)), null, Color.White, 0f, Vector2.Zero, .3f, SpriteEffects.None, .996f);
                    }
                }
            }
        }
    }
}
