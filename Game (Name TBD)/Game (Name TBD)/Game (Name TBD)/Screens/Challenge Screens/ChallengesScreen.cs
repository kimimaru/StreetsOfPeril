﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The screen where you select the challenge you want to play
    public class ChallengesScreen : Screen
    {
        /*NOTE: Right now it seems that it may be better to not have these static and check them from the save file each time the screen is created
        The only problem is it'll be a bit more cumbersome to load from the save file when checking for unlockables, but that happens infrequently and thus shouldn't be a problem
        Make sure to also do it when you come back to the screen, since the player could have unlocked something after playing a challenge*/
        //Tells if challenges are unlocked
        //public static bool Unlocked;

        //For displaying the appropriate difficulty level
        public static String[] DiffLevel;

        //Tells if Onslaught is unlocked (complete Survival - Easy and Survival - Normal)
        private bool OnslaughtUnlocked;

        //Tells if the Snipe challenge is unlocked
        private bool SnipeUnlocked;

        //Tells if the Rock, Paper, Status challenge is unlocked (complete *Unknown*)
        private bool RPSUnlocked;

        public ChallengesScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.ChallengeScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        static ChallengesScreen()
        {
            DiffLevel = new String[3] { "Easy", "Normal", "Hard" };
        }

        public override void CheckUnlocks()
        {
            OnslaughtUnlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", "Onslaught", "Unlocked"));
            SnipeUnlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", "Snipe", "Unlocked"));
            RPSUnlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", "RPS", "Unlocked"));
        }

        protected override void ChooseOption(float activeTime, Main main)
        {
            //Challenge screen to Survival screen
            if (CurChoice == 0)
            {
                main.AddScreen((int)Main.ScreenRef.Survival);
            }
            //Challenge screen to Onslaught screen
            else if (OnslaughtUnlocked == true && CurChoice == 1)
            {
                main.AddScreen((int)Main.ScreenRef.Onslaught);
            }
            //Challenge screen to Break The Objects screen
            else if (CurChoice == 2)
            {
                main.AddScreen((int)Main.ScreenRef.BTO);
            }
            //Challenge screen to Snipe screen
            else if (SnipeUnlocked == true && CurChoice == 3)
            {
                main.AddScreen((int)Main.ScreenRef.Snipe);
            }
            //Challenge screen to RPS screen
            else if (RPSUnlocked == true && CurChoice == 4)
            {
                main.AddScreen((int)Main.ScreenRef.RPS);
            }
            //Challenge screen to title screen
            else if (CurChoice == 5)
            {
                main.RemoveScreen();
            }

            LoadSounds.Play(LoadSounds.Select);
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch, new int[] { 1, 3, 4 }, OnslaughtUnlocked, SnipeUnlocked, RPSUnlocked);
        }
    }
}
