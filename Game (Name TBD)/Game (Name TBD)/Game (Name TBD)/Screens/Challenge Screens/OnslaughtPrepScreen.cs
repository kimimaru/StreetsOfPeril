﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This is the screen where the player chooses the items he/she wants to use for Onslaught
    public class OnslaughtPrepScreen : Screen
    {
        //Possible stat item choices
        private Item[] StatChoices;

        //Possible healing item choices
        private Item[] HealChoices;

        //Possible weapon choices
        private Weapon[] WeaponChoices;

        //Player item choices
        private int[] ItemChoices;

        public OnslaughtPrepScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.TitleScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            StatChoices = new Item[] { CreateObjects.BearClaw(), CreateObjects.ClamShell(), CreateObjects.GrahamTrophy() };
            HealChoices = new Item[] { CreateObjects.RiceBowl(), CreateObjects.Biscuit(new Status()), CreateObjects.EnergyDrink(), CreateObjects.Turkey(new Status()) };
            WeaponChoices = new Weapon[] { CreateObjects.Katana(new Status()), CreateObjects.Knife(new Status()), CreateObjects.BrassKnuckles(new Status()) };
            ItemChoices = new int[] { 0, 1, 0, 0, 1 };

            SetChoices(firstchoiceloc, locdiff);
        }

        //Returns new instances of the player's item choices for use in Onslaught
        private Item Stat(bool first)
        {
            int num = first == true ? 0 : 1;

            return (new Item(StatChoices[ItemChoices[num]]));
        }

        private Item Heal()
        {
            return (new Item(HealChoices[ItemChoices[2]]));
        }

        private Weapon Weapon(bool first)
        {
            int num = first == true ? 3 : 4;

            return (new Weapon(WeaponChoices[ItemChoices[num]]));
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Item 1: " + StatChoices[ItemChoices[0]].Name;
            Choices[1] = "Item 2: " + StatChoices[ItemChoices[1]].Name;
            Choices[2] = "Health Item: " + HealChoices[ItemChoices[2]].Name;
            Choices[3] = "Weapon 1: " + WeaponChoices[ItemChoices[3]].Name;
            Choices[4] = "Weapon 1: " + WeaponChoices[ItemChoices[4]].Name;
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            //Item choices
            if (CurChoice == 0)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref ItemChoices[0], 1, 2, 0);
            }
            else if (CurChoice == 1)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref ItemChoices[1], 1, 2, 0);
            }
            else if (CurChoice == 2)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref ItemChoices[2], 1, 3, 0);
            }
            else if (CurChoice == 3)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref ItemChoices[3], 1, 2, 0);
            }
            else if (CurChoice == 4)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref ItemChoices[4], 1, 2, 0);
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float ActiveTime, Main main)
        {
            //Onslaught preparation screen to Onslaught
            if (CurChoice == 5)
            {
                main.AddScreen((int)Main.ScreenRef.CharSelect);

                (main.CurScreen() as CharSelectScreen).menuexit = delegate(float activeTime, Player[] player)
                {
                    main.SetUpChallenges(new Onslaught(player, LoadGraphics.BG, LoadGraphics.FG, new Item[] { Stat(true), Stat(false), Heal() }, new Weapon[] { Weapon(true), Weapon(false) }));
                    LoadSounds.PlayChallengeMusic(2);
                };

                CurChoice = 0;
                LoadSounds.PlaySong((int)LoadSounds.Music.CharSelect, true);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Onslaught preparation screen to Onslaught screen
            else if (CurChoice == 6)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch);

            int Y = (int)(Main.ScreenHalf.Y / 1.5f);
            int X2 = (int)Main.ScreenHalf.X;

            //Draw the selected item sprites
            spriteBatch.Draw(LoadGraphics.ItemSprite, new Vector2(120, Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
            spriteBatch.Draw(LoadGraphics.ItemSprite, new Vector2(141, Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
            spriteBatch.Draw(LoadGraphics.ItemSprite, new Vector2(X2 + 30, Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
            spriteBatch.Draw(LoadGraphics.WeaponSprite[WeaponChoices[ItemChoices[3]].GetWeaponType], new Vector2(X2 + 66, Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
            spriteBatch.Draw(LoadGraphics.WeaponSprite[WeaponChoices[ItemChoices[4]].GetWeaponType], new Vector2(X2 + 101, Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
        }
    }
}
