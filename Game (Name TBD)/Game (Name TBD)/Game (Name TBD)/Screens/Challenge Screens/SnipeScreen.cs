﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The screen for the Snipe challenge mode
    public class SnipeScreen : Screen
    {
        //The difficulty level of the challenge
        private int Difficulty;

        public SnipeScreen()
        {
            ScreenGraphic = LoadGraphics.SnipeScreen;

            Difficulty = 1;

            Choices = new String[] { "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1], "Back" };
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(new Vector2(65, 213), 30);
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref Difficulty, 1, 3, 1);
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Snipe screen to Snipe
            if (CurChoice == 0)
            {
                if (Difficulty == 1) main.SetUpChallenges(new Snipe());
                else if (Difficulty == 2) main.SetUpChallenges(new SnipeN());
                else main.SetUpChallenges(new SnipeH());
                LoadSounds.StopMusic();
                LoadSounds.PlayChallengeMusic(3);
                LoadSounds.Play(LoadSounds.Select);
            }
            //To challenge screen
            else if (CurChoice == 1)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            base.Draw(activeTime, spriteBatch);
            String besttime = String.Format(" - {0:0.00}s", CLevel.ChallengeRanks[(int)CLevel.Challenges.Snipe][Difficulty - 1].GetScoreTime());
            spriteBatch.DrawString(LoadGraphics.MenuFont, CLevel.ChallengeRanks[(int)CLevel.Challenges.Snipe][Difficulty - 1].Completed == false ? "Not Complete!" : ("Complete!" + besttime), new Vector2(Main.ScreenHalf.X + 43, (Main.ScreenSize.Y / 1.5f) - 22), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, 0.4f);
        }
    }
}
