﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The screen where you can choose to play the Break The Objects challenge mode
    public class BTOScreen : Screen
    {
        //The difficulty level of the challenge
        private int Difficulty;

        public BTOScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.BTOScreen;

            Difficulty = 1;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref Difficulty, 1, 3, 1);
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Break The Objects screen to Break The Objects
            if (CurChoice == 0)
            {
                main.SetUpChallenges(new BTO(LoadGraphics.BG, LoadGraphics.FG));
                LoadSounds.StopMusic();
                LoadSounds.PlayChallengeMusic(3/*(int)CLevel.Challenges.BTO*/);
                LoadSounds.Play(LoadSounds.Select);
            }
            //To challenge screen
            else if (CurChoice == 1)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            base.Draw(activeTime, spriteBatch);

            String besttime = String.Format(" - {0:0.00}s", CLevel.ChallengeRanks[(int)CLevel.Challenges.BTO][Difficulty - 1].GetScoreTime());
            spriteBatch.DrawString(LoadGraphics.MenuFont, CLevel.ChallengeRanks[(int)CLevel.Challenges.BTO][Difficulty - 1].Completed == false ? "Not Complete!" : ("Complete!" + besttime), new Vector2(Main.ScreenHalf.X + 43, (Main.ScreenSize.Y / 1.5f) - 22), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, 0.4f);
        }
    }
}
