﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The screen where you can choose to play the Rock, Paper, Status challenge mode
    public sealed class RPSScreen : Screen
    {
        //The difficulty level of the challenge
        private int Difficulty;

        public RPSScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.RPSScreen;

            Difficulty = 1;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref Difficulty, 1, 3, 1);
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float ActiveTime, Main main)
        {
            //Obstacle screen to Obstacle Course
            if (CurChoice == 0)
            {
                main.AddScreen(new CharSelectScreen(1));

                (main.CurScreen() as CharSelectScreen).menuexit = delegate(float activeTime, Player[] player)
                {
                    if (Difficulty == 1)
                        main.SetUpChallenges(new RPS(player[0]));
                    else if (Difficulty == 2) main.SetUpChallenges(new RPS(player[0]));
                    else if (Difficulty == 3) main.SetUpChallenges(new RPS(player[0]));
                    LoadSounds.PlaySong((int)LoadSounds.Music.Chal4, true);
                };

                LoadSounds.PlaySong((int)LoadSounds.Music.CharSelect, true);
                LoadSounds.Play(LoadSounds.Select);
            }
            //To challenge screen
            else if (CurChoice == 1)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch);

            spriteBatch.DrawString(LoadGraphics.MenuFont, CLevel.ChallengeRanks[(int)CLevel.Challenges.RPS][Difficulty - 1].Completed == false ? "Not Complete!" : "Complete!", new Vector2(Main.ScreenHalf.X + 43, (Main.ScreenSize.Y / 1.5f) - 22), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, 0.4f);
        }
    }
}
