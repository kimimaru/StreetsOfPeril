﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The screen where you can choose to play the Survival challenge mode
    public class SurvivalScreen : Screen
    {
        //The difficulty level of the challenge
        private int Difficulty;

        public SurvivalScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            Difficulty = 1;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Play: " + ChallengesScreen.DiffLevel[Difficulty - 1];
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                ChangeLimitValue(keypressed, Keys.Right, ref Difficulty, 1, 3, 1);
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Survival screen to Survival
            if (CurChoice == 0)
            {
                if (Difficulty == 1) main.SetUpChallenges(new Survival());
                else if (Difficulty == 2) main.SetUpChallenges(new SurvivalN());
                else main.SetUpChallenges(new SurvivalH());
                LoadSounds.StopMusic();
                LoadSounds.PlayChallengeMusic(Difficulty == 3 ? 1 : 0);
                LoadSounds.Play(LoadSounds.Select);
            }
            //To challenge screen
            else if (CurChoice == 1)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(LoadGraphics.SurvivalScreen[Difficulty - 1], Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch);
            spriteBatch.DrawString(LoadGraphics.MenuFont, CLevel.ChallengeRanks[(int)CLevel.Challenges.Survival][Difficulty - 1].Completed == false ? "Not Complete!" : "Complete!", new Vector2(Main.ScreenHalf.X + 43, (Main.ScreenSize.Y / 1.5f) - 22), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, 0.4f);
        }
    }
}
