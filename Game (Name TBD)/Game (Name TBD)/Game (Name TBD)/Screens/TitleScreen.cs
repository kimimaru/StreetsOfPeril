﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The game's Title screen - REMOVE THE LEVEL EDITOR WHEN YOU'RE DONE WITH IT!
    //NOTE: Reorganize the menus to be like the Super Smash Bros. series; that is, have Single Player and Multiplayer options on this screen and branch off from there
    //Story Mode and Rank Mode should be their own things since you can select your level and play it with one or more players
    //Challenges, Boss Rush, and the Enemy Museum should be in Single Player
    //Versus mode should be in Multiplayer
    public class TitleScreen : Screen
    {
        //The secret code for accessing the secret screen (KIMI) - the letters must be pressed sequentially
        private List<Keys> SecretCode;
        private float PrevCode;

        private bool ChallengesUnlocked;

        public TitleScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.TitleScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);

            SecretCode = new List<Keys>();
            PrevCode = 0f;
        }

        public override void CheckUnlocks()
        {
            ChallengesUnlocked = SaveLoadData.CheckUnlocked("Challenges");
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Title to character select screen or story mode, depending if autosave is used or not
            if (CurChoice == 0)
            {
                //Go to the character selection screen if autosave isn't used; otherwise, load the players and go directly to the level stopped at by the autosave
                if (main.LoadAutosave() == false)
                {
                    main.AddScreen((int)Main.ScreenRef.CharSelect);

                    (main.CurScreen() as CharSelectScreen).menuexit = delegate(float activetime, Player[] player)
                    {
                        main.SetUpLevelsPlayers(activetime, player);
                        LoadSounds.PlayLevelMusic(main.GetCurrentLevel().GetLevelNum);
                    };

                    LoadSounds.PlaySong((int)LoadSounds.Music.CharSelect, true);
                    LoadSounds.Play(LoadSounds.Select);
                }
                else
                {
                    main.SetUpLevelsPlayers(activeTime, null);
                    LoadSounds.PlayLevelMusic(main.GetCurrentLevel().GetLevelNum);
                }
            }
            //Title to challenge screen
            else if (ChallengesUnlocked == true && CurChoice == 1)
            {
                main.AddScreen((int)Main.ScreenRef.Challenges);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Title to extra modes screen
            else if (CurChoice == 2)
            {
                main.AddScreen((int)Main.ScreenRef.Extra);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Title to highscores screen (Will probably change the location of the highscores screen)
            else if (CurChoice == 3)
            {
                main.AddScreen((int)Main.ScreenRef.Data);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Title to options screen
            else if (CurChoice == 4)
            {
                main.AddScreen((int)Main.ScreenRef.Options);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Exit the game
            else if (CurChoice == 5)
            {
                main.GameExit();
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            //Go to the level editor when you press the space bar - DEVELOPER PURPOSES ONLY
            if (Input.CheckKeyPress(KeyboardState, Keys.Space) == true)
            {
                Main.SetState(Main.GameState.LevelEditor);
                return;
            }

            base.ChangeOptions(activeTime, main);
        }

        //Checks if the player input the secret code
        private void CheckSecretCode(float activeTime, Main main)
        {
            //If the player presses any letter characters, store that character and check for the secret code

            //Check if any of the keys pressed were from A to Z - since the VolumeUp and VolumeDown keys may glitch on some computers, this way will always work
            for (int i = 0; i < Keyboard.GetState().GetPressedKeys().Length; i++)
            {
                Keys pressedkey = Keyboard.GetState().GetPressedKeys()[i];

                if (pressedkey >= Keys.A && pressedkey <= Keys.Z && Input.CheckKeyPress(KeyboardState, pressedkey) == true)
                {
                    SecretCode.Add(pressedkey);
                    PrevCode = activeTime;

                    //If the player pressed enough keys, check if the secret code was entered
                    if (SecretCode.Count == 4)
                    {
                        //If the code was input properly, go to the secret screen
                        if (SecretCode[0] == Keys.K && SecretCode[1] == Keys.I && SecretCode[2] == Keys.M && SecretCode[3] == Keys.I)
                        {
                            main.AddScreen((int)Main.ScreenRef.Secret);
                            CurChoice = 0;
                            LoadSounds.Play(LoadSounds.GetLife);
                            SecretCode.Clear();
                        }
                    }
                    break;
                }
            }

            //The player must press the keys within half a second of each other for it to be successful
            if ((activeTime - PrevCode) >= 500f)
                SecretCode.Clear();
        }

        public override void Update(Main main, float activeTime)
        {
            CheckSecretCode(activeTime, main);

            //Ensure that you cannot be in the Secret screen and another screen at the same time
            if (main.CurScreen() != this) return;
            base.Update(main, activeTime);
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            //Debug code for testing the secret code!
            /*if (SecretCode.Count > 0)
            {
                for (int i = 0; i < SecretCode.Count; i++)
                {
                    if (i >= 4) break;
                    spriteBatch.DrawString(LoadGraphics.HUDFont, Convert.ToString(SecretCode[i]), new Vector2(i * 50, (Main.ScreenSize.Y / 1.5f) + 20), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .999f);
                }
            }*/
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch, new int[] { 1 }, ChallengesUnlocked);
        }
    }
}
