﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //This is a screen found in the Options screen where the player can configure his/her buttons for jumping, attacking, picking up/dropping items, and using special attacks
    public class ControlsScreen : Screen
    {
        //The new keys to be mapped to the player's controls
        private Keys[] NewKeys;

        //The player that the controls are going to be set for
        private int PlayerNumber;

        //Tracks the key that is going to be set
        private int CurKey;

        //Tells if the screen is waiting for the player to input a key or not
        private bool KeyInput;

        //Timer automatically stopping the key input if no keys are pressed
        private float KeyInputTimer;

        public ControlsScreen(int locdiff, Vector2 firstchoiceloc, int playernumber, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            PlayerNumber = playernumber;
            CurKey = 0;
            KeyInput = false;
            KeyInputTimer = 0f;

            NewKeys = new Keys[Player.GetActionKeys((int)PlayerIndex.One).Length];

            //Initialize keys
            Player.GetActionKeys(PlayerNumber).CopyTo(NewKeys, 0);

            SetChoices(firstchoiceloc, locdiff);
        }

        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Player: " + (PlayerNumber + 1);
            Choices[1] = "Attack - " + Convert.ToString(NewKeys[0]);
            Choices[2] = "Jump - " + Convert.ToString(NewKeys[1]);
            Choices[3] = "Special Attack - " + Convert.ToString(NewKeys[2]);
            Choices[4] = "Pick up/Drop - " + Convert.ToString(NewKeys[3]);
        }

        //Change the player that you're setting controls for
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            //If you accessed the Controls screen from the Pause screen, don't let the player that accessed it change other players' controls - (May Scrap)
            if (/*Main.IsPaused == false && */CurChoice == 0)
            {
                int playernum = PlayerNumber;

                ChangeLimitValue(keypressed, Keys.Right, ref PlayerNumber, 1, (int)PlayerIndex.Four, (int)PlayerIndex.One);

                //Check if a different player is selected and if so change the controls displayed
                if (playernum != PlayerNumber)
                    Player.GetActionKeys(PlayerNumber).CopyTo(NewKeys, 0);
            }
        }

        protected override void ChooseOption(float activeTime, Main main)
        {
            //Set attack key
            if (CurChoice == 1)
            {
                CurKey = 0;
                KeyInput = true;
                KeyInputTimer = activeTime;
                LoadSounds.Play(LoadSounds.Select);
            }
            //Set jump key
            else if (CurChoice == 2)
            {
                CurKey = 1;
                KeyInput = true;
                KeyInputTimer = activeTime;
                LoadSounds.Play(LoadSounds.Select);
            }
            //Set special attack key
            else if (CurChoice == 3)
            {
                CurKey = 2;
                KeyInput = true;
                KeyInputTimer = activeTime;
                LoadSounds.Play(LoadSounds.Select);
            }
            //Set pick up/drop key
            else if (CurChoice == 4)
            {
                CurKey = 3;
                KeyInput = true;
                KeyInputTimer = activeTime;
                LoadSounds.Play(LoadSounds.Select);
            }
            //Reset back to default keys
            else if (CurChoice == 5)
            {
                NewKeys = Player.GetDefaultActionKeys(PlayerNumber);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Back to options screen with saving keys
            else if (CurChoice == 6)
            {
                main.RemoveScreen();
                Player.SetActionKeys(PlayerNumber, NewKeys);
                LoadSounds.Play(LoadSounds.Select);
                for (int i = 0; i < NewKeys.Length; i++)
                {
                    SaveLoadData.SaveData("Settings", "Controls", "PlayerKeys" + PlayerNumber + i, (int)NewKeys[i]);
                }
            }
            //Back to options screen without saving keys
            else if (CurChoice == 7)
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            //Handle the key input
            //If keyinput is true, check if any keys were pressed or if 5 seconds passed and no keys were pressed
            //In the first case assign the first key pressed as the new key, and in the latter case keep the key as is
            if (KeyInput == true)
            {
                //Check if any of the keys pressed were from A to Z - since the VolumeUp and VolumeDown keys may glitch on some computers, this way will always work
                for (int i = 0; i < Keyboard.GetState().GetPressedKeys().Length; i++)
                {
                    Keys pressedkey = Keyboard.GetState().GetPressedKeys()[i];

                    if (((pressedkey >= Keys.A && pressedkey <= Keys.Z) || pressedkey == Keys.Enter) && Input.CheckKeyPress(KeyboardState, pressedkey) == true)
                    {
                        //Pressing Enter simply gets you out of having to enter a key without changing the selected key
                        if (pressedkey != Keys.Enter)
                        {
                            //If you didn't press enter, check if the key you're going to assign this action to is already assigned to another action and assign the other action to the current one's key if so, effectively swapping the keys for the actions
                            for (int k = 0; k < NewKeys.Length; k++)
                            {
                                if (CurKey == k) continue;
                                if (NewKeys[k] == pressedkey)
                                {
                                    NewKeys[k] = NewKeys[CurKey];
                                    break;
                                }
                            }
                            NewKeys[CurKey] = pressedkey;
                        }
                        KeyInput = false;
                        KeyInputTimer = 0f;
                        LoadSounds.Play(LoadSounds.Select);
                        return;
                    }
                }

                if ((activeTime - KeyInputTimer) >= 5000f)
                {
                    KeyInput = false;
                    KeyInputTimer = 0f;
                    LoadSounds.Play(LoadSounds.Select);
                }

                return;
            }

            if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
            {
                CurChoice--;
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
            {
                CurChoice++;
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
            {
                SwitchOption(activeTime, Keys.Left, main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
            {
                SwitchOption(activeTime, Keys.Right, main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                ChooseOption(activeTime, main);
            }

            if (CurChoice < 0)
                CurChoice = Choices.Length - 1;
            else if (CurChoice >= Choices.Length)
                CurChoice = 0;
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            if (Main.IsPaused == false) spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            else spriteBatch.Draw(LoadGraphics.PauseScreen, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            DrawChoices(spriteBatch);

            if (KeyInput == true)
                spriteBatch.DrawString(LoadGraphics.HUDFont, "Please press a key...", new Vector2((Main.ScreenHalf.X / 2), (Main.ScreenHalf.Y / 1.5f) - 23), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
        }
    }
}
