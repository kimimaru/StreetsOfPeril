﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The Options screen - there are two versions of it; one on the pause menu, and another on the on the main menu
    public class OptionsScreen : Screen
    {
        //Checks if the Sound Test is unlocked (all the challenges are completed)
        private bool SoundTestUnlocked;

        //The screen resolution
        private int ScreenResolution;

        public OptionsScreen(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            SoundTestUnlocked = SaveLoadData.CheckUnlocked("SoundTest");

            ScreenResolution = Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "Resolution", 0);

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];
            
            SetChoices(firstchoiceloc, locdiff);
        }

        //Converts difficulty numbers to strings for displaying the difficulty level
        public static String DisplayDifficulty()
        {
            //As the numbers get higher, so does the difficulty
            switch (Main.Difficulty) 
            {
                case (int)Main.DifficultyLevel.VeryEasy: return "Very Easy";
                case (int)Main.DifficultyLevel.Easy: return "Easy";
                case (int)Main.DifficultyLevel.Normal: return "Normal";
                case (int)Main.DifficultyLevel.Hard: return "Hard";
                default: return "Very Hard";
            }
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "Music Volume: " + Convert.ToString(Math.Round(MediaPlayer.Volume, 1) * 10);
            Choices[1] = "Sound Volume: " + Convert.ToString(Math.Round(LoadSounds.SoundVolume, 1) * 10);
            if (Main.Paused == Main.InGameState.UnPaused)
            {
                Choices[2] = "Difficulty: " + DisplayDifficulty();
                //Choices[3] = "Autograb: " + YesNoOption(Main.AutoGrab);
                Choices[3] = "Player Damage: " + YesNoOption(Main.PlayerDamage);
                Choices[4] = "Pause Losing Focus: " + YesNoOption(Main.LoseFocusPause);
                Choices[5] = "Resolution: " + Enum.GetName(typeof(Main.ScreenResolution), ScreenResolution);
            }
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                if (keypressed == Keys.Right)
                    MediaPlayer.Volume += .1f;
                else MediaPlayer.Volume -= .1f;

                LoadSounds.Play(LoadSounds.Choose);
                SaveLoadData.SaveData("Settings", "Volume", "Music", Math.Round(MediaPlayer.Volume,1));
            }
            else if (CurChoice == 1)
            {
                if (keypressed == Keys.Right)
                    LoadSounds.SoundVolume += .1f;
                else LoadSounds.SoundVolume -= .1f;

                if (LoadSounds.SoundVolume < 0f) LoadSounds.SoundVolume = 0f;
                else if (LoadSounds.SoundVolume > 1f) LoadSounds.SoundVolume = 1f;

                LoadSounds.Play(LoadSounds.Choose);
                SaveLoadData.SaveData("Settings", "Volume", "Sound", Math.Round(LoadSounds.SoundVolume, 1));
            }
            else if (Main.IsPaused == false)
            {
                if (CurChoice == 2)
                {
                    if (keypressed == Keys.Right)
                        Main.Difficulty++;
                    else Main.Difficulty--;

                    if (Main.Difficulty < 0) Main.Difficulty = 0;
                    else if (Main.Difficulty > 4) Main.Difficulty = 4;
                    else SaveLoadData.SaveData("Settings", "Gameplay", "Difficulty", Main.Difficulty);

                    LoadSounds.Play(LoadSounds.Choose);
                }
                /*else if (CurChoice == 3)
                {
                    Main.AutoGrab = !Main.AutoGrab;
                    LoadSounds.Play(LoadSounds.Choose);
                    SaveLoadData.SaveData("Settings", "Gameplay", "Autograb", Main.AutoGrab);
                }*/
                else if (CurChoice == 3)
                {
                    Main.PlayerDamage = !Main.PlayerDamage;
                    LoadSounds.Play(LoadSounds.Choose);
                }
                else if (CurChoice == 4)
                {
                    Main.LoseFocusPause = !Main.LoseFocusPause;
                    LoadSounds.Play(LoadSounds.Choose);
                    SaveLoadData.SaveData("Settings", "Gameplay", "LoseFocus", Main.LoseFocusPause);
                }
                //Change screen resolution
                else if (CurChoice == 5)
                {
                    if (keypressed == Keys.Right) ScreenResolution = (ScreenResolution + 1) % Enum.GetValues(typeof(Main.ScreenResolution)).Length;
                    else 
                    {
                        ScreenResolution -= 1;
                        if (ScreenResolution < 0) ScreenResolution = (int)Main.ScreenResolution.Full;
                    }

                    main.ChangeScreenResolution(ScreenResolution);
                    LoadSounds.Play(LoadSounds.Choose);
                }
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Options to controls screen
            if ((Main.IsPaused == false && CurChoice == 6) || (Main.IsPaused == true && CurChoice == 2))
            {
                main.AddScreen((int)Main.ScreenRef.Controls);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Options to sound test
            else if (Main.IsPaused == false && CurChoice == 7 && SoundTestUnlocked == true)
            {
                main.AddScreen((int)Main.ScreenRef.Sound);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Options to Unlockables (NOTE: Temporary! Create a new screen for holding information like this and the Highscores)
            else if (Main.IsPaused == false && CurChoice == 8)
            {
                main.AddScreen((int)Main.ScreenRef.Unlock);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Options to title screen or pause screen
            else if ((Main.IsPaused == false && CurChoice == 9) || (Main.IsPaused == true && CurChoice == 3))
            {
                main.RemoveScreen();
                LoadSounds.Play(LoadSounds.Select);
            }
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            if (Main.IsPaused == false) spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            else spriteBatch.Draw(LoadGraphics.PauseScreen, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            DrawChoices(spriteBatch, new int[] { 7 }, SoundTestUnlocked);
        }
    }
}
