﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The Sound Test, accessed from the options screen
    public class SoundTest : Screen
    {
        //The current Song and Sound effects that will be played
        private int SongNum;
        private int SoundNum;

        public SoundTest(int locdiff, Vector2 firstchoiceloc, params String[] choices)
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            SongNum = 0;
            SoundNum = 0;

            LoadSounds.StopMusic();

            Choices = choices;
            ChoiceLocations = new Vector2[Choices.Length];

            SetChoices(firstchoiceloc, locdiff);
        }

        //Updates different screens that have the same information
        protected override void UpdateChanges(float activeTime, Main main)
        {
            Choices[0] = "BGM: " + SongNum;
            Choices[1] = "SE: " + SoundNum;
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected override void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            if (CurChoice == 0)
            {
                if (keypressed == Keys.Right)
                    SongNum += SongNum + 1 < LoadSounds.Songs.Count ? 1 : 0;
                else if (keypressed == Keys.Left) SongNum -= SongNum - 1 >= 0 ? 1 : 0;
                else if (keypressed == Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack]) SongNum += SongNum + 5 < LoadSounds.Songs.Count ? 5 : (LoadSounds.Songs.Count - 1) - SongNum;
                else SongNum -= SongNum - 5 >= 0 ? 5 : SongNum;
            }
            else if (CurChoice == 1)
            {
                if (keypressed == Keys.Right)
                    SoundNum += SoundNum + 1 < LoadSounds.Sounds.Count ? 1 : 0;
                else if (keypressed == Keys.Left) SoundNum -= SoundNum - 1 >= 0 ? 1 : 0;
                else if (keypressed == Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack]) SoundNum += SoundNum + 5 < LoadSounds.Sounds.Count ? 5 : (LoadSounds.Sounds.Count - 1) - SoundNum;
                else SoundNum -= SoundNum - 5 >= 0 ? 5 : SoundNum;
            }
        }

        //Decides what to do when the player selects an option based on which screen the player is on and which option was chosen
        protected override void ChooseOption(float activeTime, Main main)
        {
            //Play a song
            if (CurChoice == 0)
            {
                LoadSounds.PlaySong(SongNum, true);
            }
            //Play a sound
            else if (CurChoice == 1)
            {
                LoadSounds.Play(LoadSounds.Sounds[SoundNum]);
            }
            //Stop a song
            else if (CurChoice == 2)
            {
                MediaPlayer.Stop();
            }
            //Sound test to options screen
            else if (CurChoice == 3)
            {
                main.RemoveScreen();
                LoadSounds.StopMusic();
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
            {
                CurChoice--;
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
            {
                CurChoice++;
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
            {
                SwitchOption(activeTime, Keys.Left, main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
            {
                SwitchOption(activeTime, Keys.Right, main);
            }
            //Checks if you pressed the attack button or the jump button - used to skip over songs and sound effects by 5
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack]) == true)
            {
                SwitchOption(activeTime, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Attack], main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Jump]) == true)
            {
                SwitchOption(activeTime, Player.GetActionKeys((int)PlayerIndex.One)[(int)Player.Action.Jump], main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                ChooseOption(activeTime, main);
            }

            if (CurChoice < 0)
                CurChoice = Choices.Length - 1;
            else if (CurChoice >= Choices.Length)
                CurChoice = 0;
        }
    }
}
