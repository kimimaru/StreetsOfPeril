﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A screen that shows up when something has been unlocked, such as Rank Mode, a new Challenge Mode, or more
    //These screens stack, so if more than one thing has been unlocked after completing something, more than one will appear, one after the other
    //This screen is also unique in that it lasts a certain amount of time and can only be removed by player input after that time has passed

    //Structure it like this: 5 lines, with the 4th one being an image
    //1st: Always says "Congratulations!" in big, shiny letters (this will likely be an image changing colors)
    //2nd: States what you unlocked (Ex. New Challenge: Onslaught!)
    //3rd: States how to unlock it (Ex. "Complete the Easy and Normal difficulties of Survival")
    //4th: Image of the unlockable (also displayed on the unlockables screen)
    //5th: States how to access it (Ex. "Play it on the Challenges menu!")
    public sealed class RewardScreen : Screen
    {
        //The time the Reward Screen lasts
        private const float ScreenTime = 4000f;
        private float PrevScreenTime;

        //Tells if the congratulatory song has been played or not
        private bool SongPlayed;

        //The unlockable used (also used in the Unlockables screen)
        private UnlockablesScreen.Unlockable Unlockable;

        //The colors to cycle through
        private static Color[] Colors;

        //The current color the screen is on
        private int CurColor;
        private const float ColorCycleTime = 160f;
        private float PrevColorCycle;

        public RewardScreen(int unlockable)
        {
            ScreenGraphic = LoadGraphics.BlackFade;

            SongPlayed = false;

            PrevScreenTime = 0f;
            CurColor = 0;
            PrevColorCycle = 0f;

            Unlockable = UnlockablesScreen.LoadUnlockable(unlockable);

            Choices = new String[] { RewardText[unlockable], Unlockable.UnlockedText, AccessText[unlockable] };
            ChoiceLocations = new Vector2[] { new Vector2(Main.ScreenHalf.X, 70f), new Vector2(Main.ScreenHalf.X, 95f), new Vector2(Main.ScreenHalf.X, 265f) };
        }

        static RewardScreen()
        {
            Colors = new Color[] { Color.Gold, Color.Blue, Color.Green, Color.Red };
        }

        private static String[] RewardText
        {
            get { return new String[] { "You unlocked Rank Mode!", "You unlocked Challenge Mode!", "You unlocked Boss Rush !", "You unlocked the Enemy Museum!", 
                "You unlocked the Sound Test!", "You unlocked the Onslaught challenge!", "You unlocked the Obstacle challenge!", 
                "You unlocked the Snipe challenge!" }; }
        }

        private static String[] AccessText
        {
            get { return new String[] { "Access it on the Title Screen!", "Access it on the Title Screen!", "Access it on the Extra Modes Screen!",
                "Access it on the Extra Modes Screen!", "Access it on the Extra Modes Screen!", "Access it on the Challenges Screen!", "Access it on the Challenges Screen!",
                "Access it on the Challenges Screen!" }; }
        }

        public override void Update(Main main, float activeTime)
        {
            //Play a congratulatory song if it has not been played yet
            if (SongPlayed == false)
            {
                LoadSounds.PlaySong((int)LoadSounds.Music.Reward);
                PrevScreenTime = activeTime + ScreenTime;
                PrevColorCycle = activeTime + ColorCycleTime;
                SongPlayed = true;
            }

            //Go back to the previous screen when the player presses Enter
            if (activeTime >= PrevScreenTime && Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                main.RemoveScreen();
                LoadSounds.StopMusic();
            }

            if (activeTime >= PrevColorCycle)
            {
                CurColor = (CurColor + 1) % Colors.Length;
                PrevColorCycle = activeTime + ColorCycleTime;
            }

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);

            for (int i = 0; i < Choices.Length; i++)
            {
                spriteBatch.DrawString(LoadGraphics.ImgFont, Choices[i], new Vector2(Helper.DrawTextCenter(LoadGraphics.ImgFont, Choices[i]).X, ChoiceLocations[i].Y), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
            }

            spriteBatch.DrawString(LoadGraphics.ImgFont, "0123456789!?{}|=~`^/<>:;[]", new Vector2(0, 290), Color.White * .5f, 0f, Vector2.Zero, 2f, SpriteEffects.None, .4f);

            Vector2 congratsorigin = new Vector2(LoadGraphics.RewardCongratulations.Width / 2, LoadGraphics.RewardCongratulations.Height / 2);

            spriteBatch.Draw(LoadGraphics.RewardCongratulations, new Vector2(Main.ScreenHalf.X, 30), null, Colors[CurColor], 0f, congratsorigin, 1f, SpriteEffects.None, .4f);
            spriteBatch.Draw(Unlockable.UnlockedGraphic, new Vector2(Main.ScreenHalf.X, Main.ScreenHalf.Y + 40f), null, Color.White, 0f, new Vector2(Unlockable.UnlockedGraphic.Width / 2, Unlockable.UnlockedGraphic.Height / 2), 1f, SpriteEffects.None, .4f);
        }
    }
}
