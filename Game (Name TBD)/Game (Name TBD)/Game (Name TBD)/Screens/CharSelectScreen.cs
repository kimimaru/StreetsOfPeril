﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The character selection screen
    //NOTE: PLEASE make separate classes for the player selections; it'll be SO much easier to manage everything
    //ALSO! Restrict colors on Versus mode for teams so that players can choose among Red, Blue, and Green on this screen to determine teams instead!
    public class CharSelectScreen : Screen
    {
        //Delegates for doing specific things with the character selection screen after the character is chosen
        //This enables the character selection screen to be used anywhere, like in Challenges and Rank Mode, without having to change itself to serve the desired functionality
        public delegate void MenuExit(float activeTime, Player[] players);
        public MenuExit menuexit;

        //Character information; which character and costume each player is selected on
        private CharInfo[] Selectors;

        //The character costumes that are taken; true indicates taken, false indicates available
        private bool[][] CostumesTaken;

        //Variables for making the text that tells a player to press start to join in blink
        private float PrevBlink;

        //The character portraits
        private Texture2D[] CharPortraits;

        //Character stat strings
        private String[] CharStats;

        //The max number of players for this game mode
        private int MaxPlayers;
        private int CurPlayers;

        //Character standing animations (lit up and animating when the player hovers over that character's option)
        private NewAnimation[][] CharAnimations;

        //The Y location to draw the character portraits
        private readonly float PortraitDrawY;

        //The X spacing for the character portraits
        private readonly int PortraitXSpacing;

        //The blink rates for the text for the players that aren't playing
        private readonly float PlayBlinkRate;

        //The brightness value for the portraits when they're highlighted
        private Fade PortraitFade;

        public CharSelectScreen(int maxplayers)
        {
            ScreenGraphic = LoadGraphics.CharSelectScreen;

            Choices = new String[] { "Graham", "Wil", "Crystal", "Jeff", "Exit" };
            ChoiceLocations = new Vector2[Choices.Length];

            Selectors = new CharInfo[] { new CharInfo(0, 0, 0, (int)CharInfo.CharSelectState.Choosing), new CharInfo(1, 1, 0), new CharInfo(2, 2, 0), new CharInfo(3, 3, 0) };
            CostumesTaken = new bool[Player.MaxCharacters][];

            PrevBlink = 0f;

            CharPortraits = new Texture2D[Player.MaxPlayers] { LoadGraphics.GrahamPortrait, LoadGraphics.WilPortrait, LoadGraphics.CrystalPortrait, LoadGraphics.JeffPortrait };
            CharStats = new String[4] { "Power: B\nHealth: C\nSpeed: B\nGrab: C"/*\nSpecial: B"*/, "Power: A\nHealth: B\nSpeed: C\nGrab: C"/*\nSpecial: A"*/, "Power: C\nHealth: C\nSpeed: A\nGrab: B\n"/*Special: B"*/, "Power: C\nHealth: B\nSpeed: B\nGrab: A"/*\nSpecial: C"*/ };

            //Ensure that there will always be one player on the Character Selection Screen
            if (maxplayers < 1) maxplayers = 1;
            MaxPlayers = maxplayers;
            CurPlayers = 1;

            //All characters have the same animations for now until they get unique graphics
            CharAnimations = new NewAnimation[Player.MaxPlayers][];
            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                CharAnimations[i] = new NewAnimation[] { new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0))),
                new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0))), 
                new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0))),
                new NewAnimation(true, true, 0, LoadGraphics.CharSheets[(int)Player.Characters.Graham][0], new AnimFrame(new Rectangle(8, 11, 42, 77), 145), new AnimFrame(new Rectangle(64, 13, 39, 75), 135, new Vector2(1, 0)), new AnimFrame(new Rectangle(112, 15, 39, 73), 135, new Vector2(2, 0))), };

                //Initialize costumes taken array
                CostumesTaken[i] = new bool[Player.MaxCostumes];
            }

            //The first player is playing, so set that costume as taken
            CostumesTaken[0][0] = true;

            PortraitDrawY = (int)Main.ScreenHalf.Y - 100;
            PortraitXSpacing = (LoadGraphics.PortraitFrame.Width - LoadGraphics.GrahamPortrait.Width) + 2;

            PlayBlinkRate = 2000f;

            PortraitFade = new Fade(-5, 100, 180, 6);

            SetChoices();
        }

        //NOTE: Maybe reposition these based on how many max players there are
        //This is currently a problem because in say a mode with only 2 players, 3rd player and 4th player must be able to join in and appear in either location (1st location if someone else joins and 1st player drops out)
        private void SetChoices()
        {
            /*if (MaxPlayers == 1)
            {
                for (int i = 0; i < (ChoiceLocations.Length - 1); i++)
                    ChoiceLocations[i] = new Vector2(Main.ScreenHalf.X - 30, Main.ScreenHalf.Y - 5);
            }
            if (MaxPlayers == 4)
            {*/
                ChoiceLocations[0] = new Vector2(51, Main.ScreenHalf.Y + 50);
                ChoiceLocations[1] = new Vector2(143, Main.ScreenHalf.Y + 50);
                ChoiceLocations[2] = new Vector2(235, Main.ScreenHalf.Y + 50);
                ChoiceLocations[3] = new Vector2(327, Main.ScreenHalf.Y + 50);
            //}
            ChoiceLocations[4] = new Vector2(Main.ScreenSize.X - 50, 10);
        }

        //Takes a costume
        private void TakeCostume(CharInfo player)
        {
            CostumesTaken[player.GetPrevCharacter][player.GetCostumeNum] = true;
        }

        //Frees up a costume
        private void FreeCostume(CharInfo player)
        {
            CostumesTaken[player.GetPrevCharacter][player.GetCostumeNum] = false;
        }

        //Change the state of all CharInfos
        protected void ChangeAllStates(int minstate, int state)
        {
            for (int i = 0; i < Selectors.Length; i++)
            {
                if (Selectors[i].GetState >= minstate) Selectors[i].ChangeState(state);
            }
        }

        //Occurs when the attack key is pressed
        private void AttackOption(CharInfo player, Main main)
        {
            //Don't enable players to drop out while selecting the Exit option
            if (player.GetCharacterNum != LastChoice)
            {
                //Drop out
                if (player.GetState == (int)CharInfo.CharSelectState.Choosing)
                {
                    FreeCostume(player);
                    player.ChangeCharNum(player.GetPlayerNum, true);
                    player.ChangeCostume(0);
                    player.ChangeState((int)CharInfo.CharSelectState.NotPlaying);

                    ChangeAllStates((int)CharInfo.CharSelectState.Selected, (int)CharInfo.CharSelectState.Choosing);
                    LoadSounds.Play(LoadSounds.Select);
                    CurPlayers--;

                    //Exit the screen if no players are playing
                    if (CurPlayers <= 0)
                    {
                        main.RemoveScreen();
                        LoadSounds.StopMusic();
                    }
                }
                else if (player.GetState == (int)CharInfo.CharSelectState.Selected)
                {
                    ChangeAllStates((int)CharInfo.CharSelectState.Selected, (int)CharInfo.CharSelectState.Choosing);
                    LoadSounds.Play(LoadSounds.Select);
                }
            }
        }

        //Occurs when the pause key is pressed
        private void PauseOption(CharInfo player, Main main)
        {
            //Join in
            if (player.GetState == (int)CharInfo.CharSelectState.NotPlaying)
            {
                CurPlayers++;
                player.ChangeState((int)CharInfo.CharSelectState.Choosing);
                player.ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, player, true));
                TakeCostume(player);
                LoadSounds.Play(LoadSounds.Select);
            }
            //Select character
            else if (player.GetState == (int)CharInfo.CharSelectState.Choosing)
            {
                //Exit screen
                if (player.GetCharacterNum == LastChoice)
                {
                    LoadSounds.StopMusic();
                    LoadSounds.Play(LoadSounds.Select);

                    main.RemoveScreen();
                }
                else
                {
                    //The player selected his/her character
                    player.ChangeState((int)CharInfo.CharSelectState.Selected);

                    bool allchose = true;

                    //Check if all players chose their characters and start the game if so
                    for (int i = 0; i < Selectors.Length; i++)
                    {
                        if (Selectors[i].GetState == (int)CharInfo.CharSelectState.Choosing)
                        {
                            allchose = false;
                            break;
                        }
                    }

                    //All players chose, so start the game with their characters
                    if (allchose == true)
                    {
                        //The characters that will join the game
                        List<Player> players = new List<Player>();

                        //Character select screen to play game - character chosen: 0 = Graham, 1 = Wil, 2 = Crystal, 3 = Jeff
                        for (int i = 0; i < Selectors.Length; i++)
                        {
                            if (Selectors[i].GetState != (int)CharInfo.CharSelectState.NotPlaying)
                            {
                                int character = Selectors[i].GetCharacterNum;
                                int costume = Selectors[i].GetCostumeNum;

                                if (character == (int)Player.Characters.Graham) players.Add(CreateObjects.Graham(i, costume));
                                else if (character == (int)Player.Characters.Wil) players.Add(CreateObjects.Wil(i, costume));
                                else if (character == (int)Player.Characters.Crystal) players.Add(CreateObjects.Crystal(i, costume));
                                else players.Add(CreateObjects.Jeff(i, costume));
                            }
                        }

                        menuexit(Main.GetActiveTime, players.ToArray());
                        main.RemoveScreen();
                    }
                    LoadSounds.Play(LoadSounds.Select);
                }
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            //Check if any player pressed something
            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                //Go up to the Exit option
                if (Selectors[i].PressedUp(KeyboardState) == true)
                {
                    if (Selectors[i].GetState == (int)CharInfo.CharSelectState.Choosing && Selectors[i].GetCharacterNum == Selectors[i].GetPrevCharacter)
                    {
                        Selectors[i].ChangeCharNum(LastChoice);
                        LoadSounds.Play(LoadSounds.Choose);
                    }
                }
                //Go down from the Exit option
                else if (Selectors[i].PressedDown(KeyboardState) == true)
                {
                    if (Selectors[i].GetState == (int)CharInfo.CharSelectState.Choosing && Selectors[i].GetCharacterNum != Selectors[i].GetPrevCharacter)
                    {
                        Selectors[i].ChangeCharNum(Selectors[i].GetPrevCharacter);
                        LoadSounds.Play(LoadSounds.Choose);
                    }
                }
                //Select character to the left; don't allow cycling
                else if (Selectors[i].PressedLeft(KeyboardState) == true)
                {
                    if (Selectors[i].GetState == (int)CharInfo.CharSelectState.Choosing && Selectors[i].GetCharacterNum != LastChoice && Selectors[i].GetCharacterNum > 0)
                    {
                        FreeCostume(Selectors[i]);
                        Selectors[i].DecrementCharNum();
                        Selectors[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, Selectors[i], true));
                        TakeCostume(Selectors[i]);
                        CharAnimations[Selectors[i].GetPlayerNum][Selectors[i].GetCharacterNum].Reset();
                        LoadSounds.Play(LoadSounds.Choose);
                    }
                }
                //Select character to the right; don't allow cycling
                else if (Selectors[i].PressedRight(KeyboardState) == true)
                {
                    if (Selectors[i].GetState == (int)CharInfo.CharSelectState.Choosing && Selectors[i].GetCharacterNum < (LastChoice - 1))
                    {
                        FreeCostume(Selectors[i]);
                        Selectors[i].IncrementCharNum();
                        Selectors[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, Selectors[i], true));
                        TakeCostume(Selectors[i]);
                        CharAnimations[Selectors[i].GetPlayerNum][Selectors[i].GetCharacterNum].Reset();
                        LoadSounds.Play(LoadSounds.Choose);
                    }
                }
                else if (Selectors[i].PressedSpecAttack(KeyboardState) == true)
                {
                    if (Selectors[i].GetState != (int)CharInfo.CharSelectState.NotPlaying && Selectors[i].GetCharacterNum != LastChoice)
                    {
                        //Don't play the sound if the player wasn't able to switch costumes
                        int curcostume = Selectors[i].GetCostumeNum;

                        FreeCostume(Selectors[i]);
                        Selectors[i].ChangeCostume(CharInfo.NextAvailableCostume(CostumesTaken, Selectors[i]));
                        TakeCostume(Selectors[i]);
                        if (curcostume != Selectors[i].GetCostumeNum) LoadSounds.Play(LoadSounds.Choose);
                    }
                }
                else if (Selectors[i].PressedAttack(KeyboardState) == true)
                    AttackOption(Selectors[i], main);
                else if (Selectors[i].PressedPause(KeyboardState) == true)
                    PauseOption(Selectors[i], main);
            }
        }

        public override void Update(Main main, float activeTime)
        {
            ChangeOptions(activeTime, main);

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();

            //Update the character animations depending on the choice you're selected on
            for (int i = 0; i < Selectors.Length; i++)
            {
                if (Selectors[i].GetState != (int)CharInfo.CharSelectState.NotPlaying && Selectors[i].GetCharacterNum < (Choices.Length - 1)) CharAnimations[i][Selectors[i].GetPrevCharacter].Update(activeTime);
            }

            //Update the blink for the text when players aren't playing
            if ((activeTime - PrevBlink) >= PlayBlinkRate)
                PrevBlink = activeTime;

            //Update the color of the character portraits (darker when they're selected)
            PortraitFade.UpdateContinuous(activeTime);
        }

        //Shows how good each character is in each general stat (Power, Health, Speed, Grab, Special) if a player is selected on that character
        private void DrawStats(SpriteBatch spriteBatch, int index)
        {
            //Graham - Power: B, Health: C, Speed: B, Grab: C, Special: B
            //Wil - Power: A, Health: B, Speed: C, Grab: C, Special: A
            //Crystal - Power: C, Health: C, Speed: A, Grab: B, Special: B
            //Jeff - Power: C, Health: B, Speed: B, Grab: A, Special: C

            spriteBatch.DrawString(LoadGraphics.ImgFont, CharStats[Selectors[index].GetPrevCharacter], new Vector2(ChoiceLocations[index].X, ChoiceLocations[index].Y + 30), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .5f);
        }

        private int PortraitSpacing(int index)
        {
            switch (index)
            {
                case 0: return (int)(PortraitXSpacing * -1.5f);
                case 1: return (PortraitXSpacing / -2);
                case 2: return (PortraitXSpacing / 2);
                default: return (int)(PortraitXSpacing * 1.5f);
            }
        }

        private void DrawAnimations(float activeTime, SpriteBatch spriteBatch)
        {
            Color SelectColor = Color.Black;

            //Go through all the player numbers and draw information on the screen
            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                //The color to display the character portrait
                Color portraitframecolor = Color.White;
                Color portraitcolor = new Color(PortraitFade.MinFadeVal, PortraitFade.MinFadeVal, PortraitFade.MinFadeVal);
                Vector2 portraitdrawloc = new Vector2(Helper.DrawRowCenter(LoadGraphics.GrahamPortrait, i, Player.MaxPlayers, 0, 0).X + PortraitSpacing(i), PortraitDrawY);

                //If the character portrait should be lit because it is selected, check if any of the players are selected on the portrait
                int numhovered = CharInfo.GetSameCharNum(Selectors, i);
                if (numhovered > 0)
                {
                    //A player is hovering on the portrait, so light it up and make the portrait frame blink
                    portraitcolor = Color.White;
                    portraitframecolor = new Color(PortraitFade.CurFadeVal, PortraitFade.CurFadeVal, PortraitFade.CurFadeVal);

                    int numselected = CharInfo.GetSameCharState(Selectors, i, (int)CharInfo.CharSelectState.Selected);
                    //Light the portrait frame; if a player selected the character corresponding to the portrait, draw it at its normal color
                    //However, if a portrait is selected and at least one other player is hovering on it, make the portrait frame glow again
                    if (numselected == numhovered) portraitframecolor = Color.White;

                    //Draw the portrait frame around the portrait
                    Vector2 portraitdraw = new Vector2(portraitdrawloc.X - ((LoadGraphics.PortraitFrame.Width - LoadGraphics.GrahamPortrait.Width) / 2), portraitdrawloc.Y - ((LoadGraphics.PortraitFrame.Height - LoadGraphics.GrahamPortrait.Height) / 2));

                    spriteBatch.Draw(LoadGraphics.PortraitFrame, portraitdraw, null, portraitframecolor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .35f);
                }

                //Draw the Graham, Wil, Crystal, and Jeff portraits
                spriteBatch.Draw(CharPortraits[i], portraitdrawloc, null, portraitcolor, 0f, Vector2.Zero, 1f, SpriteEffects.None, .35f);

                //Draw the character if the player is playing
                if (Selectors[i].GetState != (int)CharInfo.CharSelectState.NotPlaying)
                {
                    int characternum = Selectors[i].GetCharacterNum;
                    int prevcharacter = Selectors[i].GetPrevCharacter;
                    int costume = Selectors[i].GetCostumeNum;

                    Color drawcolor = characternum == (Choices.Length - 1) ? new Color(150, 150, 150) : Color.White;

                    //Draw the character animations
                    CharAnimations[i][prevcharacter].Draw(spriteBatch, LoadGraphics.CharSheets[(int)Player.Characters.Graham][costume], new Vector2(ChoiceLocations[i].X + 25, ChoiceLocations[i].Y/* - 75*/), true, drawcolor, 0f, 0.5f);
                    spriteBatch.DrawString(LoadGraphics.HUDFont, Choices[prevcharacter], new Vector2(ChoiceLocations[i].X - 15, ChoiceLocations[i].Y), Choices[prevcharacter] == Choices[characternum] ? Color.Green : Color.Black, 0f, Vector2.Zero, .95f, SpriteEffects.None, 0.4f);

                    //Draw character stats
                    DrawStats(spriteBatch, i);

                    Vector2 drawloc = new Vector2(ChoiceLocations[i].X - 47, ChoiceLocations[characternum].Y + 1);
                    if (characternum == (Choices.Length - 1))
                    {
                        SelectColor = Color.Green;
                        drawloc = new Vector2(ChoiceLocations[characternum].X - 32, ChoiceLocations[characternum].Y);
                    }
                    //Draw the player selection icon; which character the player is selecting
                    else
                    {
                        Vector2 pselectiondrawloc = new Vector2(Helper.DrawRowCenter(LoadGraphics.GrahamPortrait, characternum, Player.MaxPlayers, 0, 0).X + PortraitSpacing(characternum) + 1, PortraitDrawY + (i * LoadGraphics.PlayerNumSelection[i].Height) + (LoadGraphics.PlayerNumSelection[i].Height / 5) - 1);

                        spriteBatch.Draw(LoadGraphics.PlayerNumSelection[i], pselectiondrawloc, null, Selectors[i].GetState == (int)CharInfo.CharSelectState.Selected ? Color.White : Color.White * .7f, 0f, Vector2.Zero, 1f, SpriteEffects.None, .41f);
                    }

                    spriteBatch.Draw(LoadGraphics.ScreenSelect, drawloc, null, Player.ColorOfPlayer(i), 0f, Vector2.Zero, .8f, SpriteEffects.None, 0.4f - (i * .0001f));
                }
                else if (CurPlayers < MaxPlayers && (activeTime - PrevBlink) <= (PlayBlinkRate * .75f))
                {
                    spriteBatch.DrawString(LoadGraphics.HUDFont, "Press\nStart", ChoiceLocations[i], Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .4f);
                }
            }

            spriteBatch.DrawString(LoadGraphics.HUDFont, Choices[Choices.Length - 1], ChoiceLocations[Choices.Length - 1], SelectColor, 0f, Vector2.Zero, .95f, SpriteEffects.None, 0.4f);
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);

            //Draw the characters' portraits, standing animations, and stats
            DrawAnimations(activeTime, spriteBatch);
        }
    }
}
