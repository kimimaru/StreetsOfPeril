﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A secret screen accessed by inputting a secret code (received in game or just a plain secret) on the title screen - the code currently is: KIMI (Maybe require mapping ingame controls to certain keys?)
    //The screen contains a nice message from me (the developer) thanking them for playing and (MAYBE) giving them a code for some secret or an easter egg (UNDECIDED)
    public class SecretScreen : Screen
    {
        public SecretScreen()
        {
            ScreenGraphic = LoadGraphics.SecretScreen;
        }

        public override void Update(Main main, float activeTime)
        {
            //Go back to the title screen when the user presses Enter (For now, as there's nothing special here yet)
            if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                main.RemoveScreen();
            }

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            String welcome = "Welcome, you found the Secret Screen!";
            String description = "The creator of this game is me, Thomas Deeb,\naka Kimimaru. I hope that you like(d) what the\ngame offers and continue to play quality games\nthat innovate and push the boundaries for what\nplayers expect. Games like those should be\nhonored for respecting the art of game design \ninstead of the mediocre games made only to\nmake money, shoving overpriced DLC and \nmicro-transactions down your throat on an\nincomplete game. This game was made with\nnothing short of pure passion and love, and I\nhope I have inspired you to make great games \nas well.\n                Thanks for playing!";

            spriteBatch.DrawString(LoadGraphics.HUDFont, welcome, new Vector2(Helper.DrawTextCenter(LoadGraphics.HUDFont, welcome).X, 5), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .999f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, description, new Vector2(5, 40), Color.Blue, 0f, Vector2.Zero, .8f, SpriteEffects.None, .999f);
            //spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
        }
    }
}
