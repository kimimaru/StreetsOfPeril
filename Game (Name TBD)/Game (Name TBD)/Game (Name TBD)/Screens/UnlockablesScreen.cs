﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A screen displaying what the player has unlocked in a style similar to Super Smash Bros. Brawl's Challenges screen
    public class UnlockablesScreen : Screen
    {
        //The unlocked values; these names should be the same as the ones in the Save File
        //The challenges unlocked are also here; Survival and Break The Objects are unlocked when challenges are unlocked
        public enum Unlocks
        {
            RankMode, Challenges, BossRush, Museum, SoundTest, Onslaught, RPS, Snipe
        };

        //The unlockables
        //NOTE: Read in the unlockable entries from a file; the Save File already has what's unlocked, so all you'd need to do is reference the text description and unlocked icon from elsewhere
        private Unlockable[] Unlockables;

        //The current row number the player is on; this gives us the accessibility of a double array without using one
        private int CurRow;

        //The max number of rows
        private readonly int MaxRows;

        //The number of unlockables on each row
        private const int RowLimit = 5;

        //The start of the part of the screen that says the unlock text (what you did to unlock it or ??? if not unlocked)
        private readonly Vector2 TextStart;

        public UnlockablesScreen()
        {
            ScreenGraphic = LoadGraphics.OptionsScreen;

            LoadUnlockables();

            MaxRows = ((Unlockables.Length - 1) / RowLimit);
            CurRow = 0;
            TextStart = new Vector2(Main.ScreenHalf.X, (int)(Main.ScreenSize.Y / 1.5f));
        }

        //The current selection
        private int CurrentSelection
        {
            get { return (CurChoice + (CurRow * RowLimit)); }
        }

        //Loads the data for unlockables
        private void LoadUnlockables()
        {
            Unlockables = new Unlockable[Enum.GetValues(typeof(Unlocks)).Length];

            //Get the names of all the unlockables
            String[] unlockablenames = Enum.GetNames(typeof(Unlocks));

            //Look for the save data for each unlockable
            for (int i = 0; i < Unlockables.Length; i++)
            {
                Unlockables[i] = LoadUnlockable(i);

                //Try to find the data
                //bool unlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", unlockablenames[i], "Unlocked"));
                //Texture2D unlockgraphic = LoadGraphics.UnlockablesLocked;
                //String description = "???";
                //
                ////If the data was found, see if the unlockable is unlocked, find the icon for it, and find the description saying how to unlock it
                //if (unlocked == true)
                //{
                //    unlockgraphic = (LoadGraphics.UnlockableIcons[i] ?? LoadGraphics.UnlockablesLocked);
                //    
                //    //Find the string saying how to unlock the challenge
                //    object howtounlock = SaveLoadData.LoadData("Unlockables", unlockablenames[i], "HowTo");
                //    if (howtounlock != null) description = (String)howtounlock;
                //}
                //
                //Unlockables[i] = new Unlockable(unlockgraphic, unlocked, description);
            }
        }

        public static Unlockable LoadUnlockable(int unlockable)
        {
            String unlockablename = Enum.GetName(typeof(Unlocks), unlockable);

            //Try to find the data
            bool unlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", unlockablename, "Unlocked"));
            Texture2D unlockgraphic = LoadGraphics.UnlockablesLocked;
            String description = "???";

            //If the data was found, see if the unlockable is unlocked, find the icon for it, and find the description saying how to unlock it
            if (unlocked == true)
            {
                unlockgraphic = (LoadGraphics.UnlockableIcons[unlockable] ?? LoadGraphics.UnlockablesLocked);

                //Find the string saying how to unlock the challenge
                object howtounlock = SaveLoadData.LoadData("Unlockables", unlockablename, "HowTo");
                if (howtounlock != null) description = (String)howtounlock;
            }

            return (new Unlockable(unlockgraphic, unlocked, description));
        }

        //Ensures that the selection is in bounds
        private void CheckChoice(int increaseamount, bool row)
        {
            //If the player went left or right a column, see if it's in range and loop around if not
            if (row == false)
            {
                CurChoice += increaseamount;

                if (CurChoice < 0)
                {
                    CurChoice = RowLimit - 1;
                    //NOTE: There should be a complete square of unlockables in the final, so put this check in place for now to stop the game from crashing
                    if (CurrentSelection >= Unlockables.Length) CurChoice -= ((CurrentSelection - Unlockables.Length) + 1);
                }
                else if (CurChoice >= RowLimit || CurrentSelection >= Unlockables.Length) CurChoice = 0;
            }
            //If the player went up or down a row, see if it's in range and loop around if not
            else
            {
                CurRow += increaseamount;

                if (CurRow < 0)
                {
                    CurRow = MaxRows;
                    //NOTE: There should be a complete square of unlockables in the final, so put this check in place for now to stop the game from crashing
                    if (CurrentSelection >= Unlockables.Length) CurRow = MaxRows - 1;
                }
                else if (CurRow > MaxRows || CurrentSelection >= Unlockables.Length) CurRow = 0;
            }
        }

        //Cycles through the options available on the screen
        protected override void ChangeOptions(float activeTime, Main main)
        {
            if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
            {
                CheckChoice(-1, true);
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
            {
                CheckChoice(1, true);
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
            {
                CheckChoice(-1, false);
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
            {
                CheckChoice(1, false);
                LoadSounds.Play(LoadSounds.Choose);
            }
            //Exit out of the screen by pressing the Attack button
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey((int)PlayerIndex.One, (int)Player.Action.Attack)) == true)
            {
                main.RemoveScreen();
                LoadSounds.StopMusic();
            }
            //Will possibly be used for viewing more information about the unlockable
            /*else if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                ChooseOption(activeTime, main);
            }*/
        }

        public override void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);

            //Center the entire row of unlockables on the screen
            int startingx = (int)((Main.ScreenSize.X - (LoadGraphics.UnlockablesLocked.Width * RowLimit)) / 2);

            //Draw all the unlockables
            for (int i = 0; i < Unlockables.Length; i++)
            {
                Vector2 drawloc = new Vector2(Helper.DrawRowCenter(LoadGraphics.UnlockablesLocked, (i % RowLimit), RowLimit, (i / RowLimit), MaxRows + 1).X, 75 + ((i / RowLimit) * LoadGraphics.UnlockablesLocked.Height));

                Unlockables[i].Draw(spriteBatch, drawloc, CurrentSelection == i ? Color.White : new Color(120, 120, 120));
            }

            Unlockables[CurrentSelection].DrawUnlockedText(spriteBatch, TextStart);
        }

        //An unlockable; it has a graphic and boolean checking if it's unlocked - if this boolean is false, the locked graphic is displayed
        public struct Unlockable
        {
            public Texture2D UnlockedGraphic;
            public bool Unlocked;
            public String UnlockedText;

            public Unlockable(Texture2D unlockedgraphic, bool unlocked, String unlockedtext)
            {
                UnlockedGraphic = unlockedgraphic;
                Unlocked = unlocked;
                UnlockedText = unlockedtext;
            }

            public void Draw(SpriteBatch spriteBatch, Vector2 Location, Color color)
            {
                if (Unlocked == true) spriteBatch.Draw(LoadGraphics.UnlockablesLocked, Location, null, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, .993f);
                else spriteBatch.Draw(UnlockedGraphic, Location, null, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, .993f);
            }

            //Draws the text when it's unlocked
            public void DrawUnlockedText(SpriteBatch spriteBatch, Vector2 location)
            {
                Vector2 textlength = LoadGraphics.HUDFont.MeasureString(UnlockedText);

                spriteBatch.DrawString(LoadGraphics.HUDFont, UnlockedText, location - new Vector2((int)(textlength.X / 2f), 0), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .993f);
            }
        }
    }
}
