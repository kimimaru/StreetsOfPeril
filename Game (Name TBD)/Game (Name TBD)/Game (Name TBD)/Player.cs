﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //The player(s) in the game
    public abstract class Player : Fighter
    {
        //The numerical value of each character
        public enum Characters
        {
            Graham, Wil, Crystal, Jeff
        };

        //The controls for the players
        public enum Action
        {
            Attack, Jump, SpecAttack, ItemKey, Up, Down, Left, Right, Pause
        };

        //The maximum number of players possible at once and the maximum number of playable characters currently in the game
        public const int MaxPlayers = 4;
        public static readonly int MaxCharacters;

        //The max number of costumes per character
        public const int MaxCostumes = 4;

        //The maximum and minimum scores players can obtain
        public const int MaxScore = 9999999;
        public const int MinScore = -999999;

        //The total number of continues all players share
        public static int Continues;

        //Player actions
        protected Queue<PAction> Actions;

        //A counter for saying how many times the standing animation must loop; after a certain number of loops, an Idle animation will play (Ex. Sonic tapping his foot impatiently in Sonic 1)
        protected const int MaxStandingLoops = 60;//45;

        //Animations
        protected NewAnimation WalkingAnim;
        protected NewAnimation RunningAnim;
        protected List<NewAnimation> AttackingAnim;
        protected NewAnimation JumpingAnim;
        protected NewAnimation DefSpecAnim;
        protected NewAnimation OffSpecAnim;
        protected List<NewAnimation> GrabAttackingAnim;
        protected NewAnimation GrabAnim;
        protected NewAnimation Grabbedanim;
        protected NewAnimation LandAnim;
        protected NewAnimation DashAtkAnim;
        protected NewAnimation PickUpAnim;
        protected NewAnimation DownJumpAtkAnim;
        protected NewAnimation JumpAtkAnim;
        protected NewAnimation RunJumpAttackAnim;
        protected NewAnimation SwitchSidesAnim;
        protected NewAnimation FThrowAnim;
        protected NewAnimation SwingAnim;
        protected NewAnimation StabAnim;
        protected NewAnimation VictoriousAnim;

        //The number representing the character
        protected int CharNum;

        //Combo counter and timeframes
        protected const float ComboTimeFrame = 800f;
        protected float PrevComboTime;
        protected int ComboCounter;
        //Checks if we should progress through the combo or not
        protected bool ProgressCombo;

        //Timeframe for releasing grabs
        protected float GrabRelease;
        protected bool GrabReleased;

        //Action Keys - Attacking, Special Attacking, Jumping, and Picking Up/Dropping Items/Weapons - configurable in Controls screen for each player
        protected static Keys[][] Controls;

        //The player number
        protected int PlayerNum;

        //State machine
        protected PlayerStateMachine StateMachine;

        //Booleans for animations and actions
        protected bool IsWalking;
        protected bool IsRunning;
        protected bool IsAttacking;
        protected bool IsGrabbing;
        protected bool IsGrabbed;
        protected bool GrabAttacking;
        protected bool IsSpecialAttacking;
        protected bool JumpAttacking;
        protected bool DownJumpAttacking;
        protected bool CanJump;
        protected bool IsForwardThrowing;
        protected bool IsBackwardThrowing;
        protected bool IsStabbing;
        protected bool IsSwinging;
        protected bool IsJumpLand;
        protected bool IsPickingUp;
        protected bool IsSwitchingSide;
        protected bool IsVictorious;

        //For more realistic properties like sliding and playing a sound when you land
        protected float SlideSpeed;
        protected float DashAttackSpeed;
        protected float DashAttackSpeedChange;
        protected float PrevAttackMove;
        protected float PrevRun;

        //Determines whether the player can control his/her character or not (used when the player's character is forced to move or do something)
        protected bool Controlled;

        //Booleans checking if you hit an enemy with a basic attack or a special attack to move onto the next animation or subtract health respectively
        protected bool SpecialAtkHit;
        protected bool SpecialAtkInvincible;

        //A keyboardstate reference for if you already did an action (prevents action from occurring if you keep holding the button)
        protected KeyboardState KeyboardState;

        //Physics Info - Weight determines how much damage a player does when thrown and how far the player slides when knocked down
        protected int Weight;

        //Player stats
        protected int Lives;
        
        //The HUD of the object that last interacted with this player; it is drawn underneath this Player's HUD
        protected HUD InteractionHUD;

        //The Bonus Status Meter
        protected BonusStatus BonusStatus;

        //Constructor
        public Player()
        {
            Location = new Vector3(100, 200, 0f);

            CameraBlocked = true;
            ObjType = ObjectType.Player;

            //Default icon and spritesheets; default to Graham
            Icon = LoadGraphics.CharacterIcons[(int)Characters.Graham];
            SpriteSheet = LoadGraphics.CharSheets[(int)Characters.Graham][0];
            ObjectLength = 42;
            ObjectHeight = 79;

            //Combo-related
            ComboCounter = 0;
            ProgressCombo = false;

            //Attack timers
            PrevComboTime = 0;
            GrabRelease = 0f;

            //Boolean values
            IsWalking = false;
            IsRunning = false;
            IsAttacking = false;
            IsGrabbing = false;
            IsGrabbed = false;
            GrabAttacking = false;
            IsSpecialAttacking = false;
            JumpAttacking = false;
            DownJumpAttacking= false;
            CanJump = true;
            FacingRight = true;
            IsHit = false;
            IsKnockedDown = false;
            IsForwardThrowing = false;
            IsBackwardThrowing = false;
            IsStabbing = false;
            IsSwinging = false;
            IsPickingUp = false;
            IsJumpLand = false;
            IsSwitchingSide = false;
            IsVictorious = false;

            KeyboardState = new KeyboardState(Controls[PlayerNum][(int)Action.Up], Controls[PlayerNum][(int)Action.Down], Controls[PlayerNum][(int)Action.Left], Controls[PlayerNum][(int)Action.Right], Controls[PlayerNum][(int)Action.Attack], Controls[PlayerNum][(int)Action.Jump], Controls[PlayerNum][(int)Action.SpecAttack], Controls[PlayerNum][(int)Action.ItemKey]);

            GrabReleased = false;
            SlideSpeed = 0f;
            PrevAttackMove = 0f;
            PrevRun = 0f;
            
            Controlled = true;

            SpecialAtkHit = false;
            SpecialAtkInvincible = false;

            PrevKnockInv = 0f;

            Name = "Player";

            PrevMoveInv = 0f;

            DeathSound = LoadSounds.EDeath;
            InteractionHUD = null;

            BonusStatus = new BonusStatus(this);

            //Stats
            PrevStun = 0;

            Alternate = 0;
        }

        static Player()
        {
            MaxCharacters = Enum.GetValues(typeof(Characters)).Length;

            Controls = new Keys[4][];

            ResetActionKeys((int)PlayerIndex.One);
            ResetActionKeys((int)PlayerIndex.Two);
            ResetActionKeys((int)PlayerIndex.Three);
            ResetActionKeys((int)PlayerIndex.Four);

            Keys[][] LoadedKeys = new Keys[Controls.Length][];

            for (int i = 0; i < Controls.Length; i++)
            {
                LoadedKeys[i] = new Keys[Controls[i].Length];

                for (int j = 0; j < Controls[i].Length; j++)
                {
                    object key = SaveLoadData.LoadData("Settings", "Controls", "PlayerKeys" + i + j);
                    if (key == null) LoadedKeys[i][j] = Controls[i][j];
                    else LoadedKeys[i][j] = (Keys)Convert.ToInt32((String)key);
                }
            }

            Controls = LoadedKeys;
        }

        protected override Vector2 ObjVelocityFactor()
        {
            if (IsRunning == true) return new Vector2(1, 0);
            else return Vector2.Zero;
        }

        public int GetLives
        {
            get { return Lives; }
        }

        public int GWeight
        {
            get { return Weight; }
        }

        public bool GIsHit
        {
            get { return IsHit; }
        }

        public bool GIsGrabbed
        {
            get { return IsGrabbed; }
        }

        public bool GSIsPickingUp
        {
            get { return IsPickingUp; }
            set { IsPickingUp = value; }
        }

        public bool GIsGrabbing
        {
            get { return IsGrabbing; }
        }

        public bool GIsBackwardThrowing
        {
            get { return IsBackwardThrowing; }
        }

        public bool GIsForwardThrowing
        {
            get { return IsForwardThrowing; }
        }

        public override bool KnockedDown
        {
            get
            {
                //NOTE: TEMPORARY NULL CHECK
                if (StateMachine != null)
                {
                    return (StateMachine.CurState == PlayerStateMachine.State.KnockedDown);
                }

                return IsKnockedDown;
            }
        }

        public int GPlayerNum
        {
            get { return PlayerNum; }
        }

        public int GetCharNum
        {
            get { return CharNum; }
        }

        public virtual List<WBullet> GBullets
        {
            get { return null; }
        }

        public BonusStatus GetBonusStatus
        {
            get { return BonusStatus; }
        }

        /*public Enemy GEnem
        {
            get { return Enem; }
        }*/

        public Enemy Enem
        {
            get { return GrabbedFighter as Enemy; }
        }

        public bool HasActions
        {
            get { return (Actions.Count > 0); }
        }

        public PAction CurAction
        {
            get
            {
                if (HasActions == true)
                    return Actions.Peek();
                else return null;
            }
        }

        //The invincibility you get when respawning
        public static Status RespawnInvincibility
        {
            get { return new Status((int)Status.Statuses.Invincible, 2500f, 0); }
        }

        public Keys Forwards
        {
            get { return FacingRight == true ? Controls[PlayerNum][(int)Action.Right] : Controls[PlayerNum][(int)Action.Left]; }
        }

        public Keys Backwards
        {
            get { return FacingRight == true ? Controls[PlayerNum][(int)Action.Left] : Controls[PlayerNum][(int)Action.Right]; }
        }

        //Gets the buttons the player has pressed
        public KeyboardState GetInputState
        {
            get { return KeyboardState; }
        }

        protected NewAnimation CurAttackAnim
        {
            get { return AttackingAnim[ComboCounter]; }
        }

        protected NewAnimation CurGrabAttackAnim
        {
            get { return GrabAttackingAnim[ComboCounter]; }
        }

        public bool IsCompletelyDead
        {
            get { return (Lives <= 0); }
        }

        private bool HasInteractionHUD
        {
            get { return (InteractionHUD != null); }
        }

        //Completely kill the player
        public void DieCompletely()
        {
            Lives = 0;
            Die();
        }

        //Kill player
        public override void Die()
        {
            base.Die();

            if (BonusStatus != null) BonusStatus.ResetStatusMeter();
        }

        //Makes a character assume the victory pose
        public void Victory(float activeTime)
        {
            ResetActions();
            VictoriousAnim.Reset();
            IsVictorious = true;
        }

        //Changes the player's alternate outfit
        public void ChangeCostume(int costume)
        {
            if (LoadGraphics.CharSheets[CharNum] != null && costume >= 0 && costume < LoadGraphics.CharSheets[CharNum].Length && LoadGraphics.CharSheets[CharNum][costume] != null)
            {
                Alternate = costume;
                SpriteSheet = LoadGraphics.CharSheets[CharNum][Alternate];
            }
        }

        //Clears the player's interaction HUD
        protected void ClearInteractionHUD()
        {
            InteractionHUD = null;
        }

        //Sets the HUD of the object that interacted with the player and displays it underneath the player's HUD
        public override void SetInteractionHUD(HUD objecthud)
        {
            //Set the current InteractionHUD to the one passed in - note that we can also pass in null
            InteractionHUD = objecthud;
            
            //Restart the HUD if it's not null
            if (HasInteractionHUD == true) InteractionHUD.Restart();
        }

        //Gives lives from one player to another player (multiplayer Story Mode only)
        public static void GiveLives(float activeTime, Player givingplayer, Player receivingplayer, int liveamount)
        {
            //Make sure the alive player doesn't give more lives than he/she has and ends up killing him/herself
            if ((givingplayer.GetLives - liveamount) > 0 && liveamount > 0)
            {
                givingplayer.SetStats(null, null, givingplayer.Lives - liveamount, null, null, null);
                receivingplayer.SetStats(null, null, receivingplayer.GetLives + liveamount, null, null, null);
            }
        }

        //Changes a variety of stats for a player
        public void SetStats(float? health, int? healthbars, int? lives, int? damage, int? defense, Status newstatus)
        {
            if (health != null) Health = (int)health;
            if (healthbars != null) HealthBars = (int)healthbars;
            if (lives != null) Lives = (int)lives;
            if (damage != null) Damage = (int)damage;
            if (defense != null) Defense = (int)defense;
            if (newstatus != null) InflictStatus(newstatus);
        }

        //Changes whether the player has control or not over the character - used when the screen fades at the start and end of levels
        public void ChangeControl(bool control)
        {
            if (control == false)
                IsWalking = false;
            Controlled = control;
        }

        //Simply moves the player (used mainly in the minecart level)
        public void PureMove(float velocityX, float velocityY, bool changedirection = true)
        {
            if (changedirection == true)
            {
                if (velocityX > 0f) FacingRight = true;
                else if (velocityX < 0f) FacingRight = false;
            }
            Location.X += velocityX;
            Location.Y += velocityY;
        }

        //Spawns the player at the start of a sublevel
        public virtual void SpawnSub(float activeTime, TileEngine TileEngine, float X = 100, float Y = 200, float Z = 0f, bool firstsub = false, bool bonusstatuson = true)
        {
            ResetActions();
            //Status effects aren't preserved between sublevels (subject to change with playtesting)
            ResetStatus();

            Location = new Vector3(X, Y, Z);

            //For spawning at the first sublevel
            if (firstsub == true)
            {
                FullHeal();
                TempBar = 0;
                if (BonusStatus != null) BonusStatus.ResetStatusMeter();
                hud.ResetDepth();

                //Create an oxygen tank if the level is underwater
                if (TileEngine.IsUnderWater() == true)
                {
                    if (OxygenTank == null)
                        OxygenTank = new OxygenTank(LoadGraphics.FallingRock, OxygenTank.GetOxygenTankColor(PlayerNum), this);
                    else OxygenTank.HealTank(false);

                    MaxFallSpeed = -.3f;
                }

                //Allow players to keep weapons between sublevels but not between levels
                if (weapon != null)
                    weapon = null;
            }

            Active = true;

            IsKnockedDown = false;
            IsHit = false;

            //This is temporary; change it when you decide what to do with player direction when spawning (it'll be dependent on which direction the players need to go at the start)
            FacingRight = true;

            //Reset jump velocity
            ClearAirVelocity();

            //If the Bonus Status Meter is enabled for this level, make a new one if it was disabled previously
            if (bonusstatuson == true)
            {
                if (BonusStatus == null) BonusStatus = new BonusStatus(this);
            }
            else BonusStatus = null;

            //If the level isn't underwater and the player still has an oxygen tank, remove the oxygen tank
            if (OxygenTank != null)
            {
                //The level isn't underwater so remove the oxygen tank, but the player can still spawn in high water
                if (TileEngine.IsUnderWater() == false)
                {
                    OxygenTank = null;
                }
                //The level is underwater, so make sure the player spawns underwater and update the oxygen tank's position so it doesn't appear where the player was in the previous level while transitioning in
                else
                {
                    //OxygenTank.Update(activeTime);
                }
            }

            ObjectTile = TileEngine.CurTile(FeetLoc);

            //For spawning in the air
            if (Z > ObjectTile.Z)
            {
                IsJumpLand = false;
                CanJump = false;
            }

            PrevStun = 0;
        }

        //Respawns the player after losing a life
        public virtual void Respawn(float activeTime, Vector3 Position, TileEngine TileEngine)
        {
            Lives--;

            FullHeal();

            Location.X = Position.X;
            Location.Y = Position.Y;

            UpdateTile(TileEngine);
            if (Position.Z < 0) Location.Z = 260f;//PlayerTile.Z;
            else Location.Z = Position.Z;

            if (Location.Z > ObjectTile.Z)
            {
                BeginFall();
                CanJump = false;
            }

            if (OxygenTank != null) OxygenTank.HealTank(false);

            ClearAirVelocity();
            InflictStatus(RespawnInvincibility);
        }

        //Recover from getting grabbed
        public override void GrabRecover()
        {
            base.GrabRecover();

            //Although it's unknown if being grabbed in the air will be possible in the future, let's handle it anyway
            //If the player is in the air when recovering from a grab, make him/her start falling
            if (IsInAir == true)
            {
                BeginFall();
            }
            //NOTE: TEMPORARY NULL CHECK
            else if (StateMachine != null) StateMachine.ChangeStateIdle();
            
            IsGrabbed = false;
        }

        //For resetting the knock down animation 
        //This is used in Sublevel 6-3, the Minecart rush level, after touching the minecart tracks because changing the jump velocity doesn't work if the player is already knocked down after dying since it doesn't reset the animation in that case
        public void ResetKnockDownAnim()
        {
            CurKnockDownAnim.Reset();
        }

        //Resets actions - for when you get hit, knocked down, die, or complete a level
        protected override void ResetActions()
        {
            base.ResetActions();

            //If the character is doing a dash attack, make sure that the character's X value ends up as a whole number
            if (IsRunning == true && IsAttacking == true) Location.X = (int)Location.X;
            IsWalking = false;
            IsRunning = false;
            IsAttacking = false;
            IsGrabbing = false;
            IsGrabbed = false;
            IsForwardThrowing = false;
            IsBackwardThrowing = false;
            IsStabbing = false;
            IsSwinging = false;
            GrabAttacking = false;
            IsSpecialAttacking = false;
            IsJumpLand = false;
            CanJump = true;
            JumpAttacking = false;
            DownJumpAttacking = false;
            IsJumpLand = false;
            IsSwitchingSide = false;
            IsVictorious = false;
            SpecialAtkHit = false;
            ComboCounter = 0;
            ProgressCombo = false;
            IsPickingUp = false;
        }

        protected override void EnterHitstun()
        {
            base.EnterHitstun();

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                StateMachine.ChangeState(PlayerStateMachine.State.Hurt, HurtAnim, UpdateHurtAnim);
            }
        }

        public override void KnockDownRecover()
        {
            base.KnockDownRecover();

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null) StateMachine.ChangeStateIdle();
        }

        protected override void KnockDownActions()
        {
            base.KnockDownActions();

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null) StateMachine.ChangeState(PlayerStateMachine.State.KnockedDown, CurKnockDownAnim, UpdateKnockedDownAnim);
        }

        protected override void AltTakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.AltTakeDamageActions(objecthitby, damage, hitbox);

            //If the Player can set its InteractionHUD to the object that hit it's HUD, do so
            if (objecthitby.CanSetHUD() == true) SetInteractionHUD(objecthitby.hud);

            //Subtract from the Bonus Status regardless if the player actually lost health or not
            if (BonusStatus != null) BonusStatus.ChangeBonusMeter(BonusStatus.AmountLost);
        }

        public override bool ShouldRemove
        {
            //Although the Player's HUD is always shown, we'll reset its activity so we can remove the Player, guaranteed, without having to rely on him/her recovering from a knock down
            get { return (base.ShouldRemove == true && Lives <= 0 && IsInAir == false); }
        }

        //Sets the Player's Score directly; this is used for loading autosaves and such
        public void SetScore(int score)
        {
            Score = score;
        }

        //Adds to the Player's Score
        public void AddScore(int score)
        {
            Score += score;
            Score = Helper.Clamp(Score, MinScore, MaxScore);
        
            //Check for obtaining more lives based on the Player's current Score
            /*Code here*/
        }

        //Resets a Player's Score (used for restarting challenges)
        public void ResetScore()
        {
            Score = 0;
        }

        //Adds to the Player's Lives
        public void AddLives(int numlives)
        {
            Lives += numlives;
        }

        //Gets the numerical value of each character
        public static int IndexOfCharacter(Player player)
        {
            return player.CharNum;
        }

        //Finds a player in the player list corresponding to the PlayerNum of the player
        public static Player PlayerOfPlayerNum(List<Player> Players, int playernum)
        {
            //Look through the player list to find the player
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].GPlayerNum == playernum) return Players[i];
            }

            return null;
        }

        //Gets the color of the character depending on the character's player number
        public static Color ColorOfCharacter(Player player)
        {
            return ColorOfPlayer(player.GPlayerNum);
        }

        //Gets the color of the player depending on the player number
        public static Color ColorOfPlayer(int playernum)
        {
            switch (playernum)
            {
                case (int)PlayerIndex.One: return Color.Red;
                case (int)PlayerIndex.Two: return Color.Blue;
                case (int)PlayerIndex.Three: return Color.Green;
                default: return Color.Yellow;
            }
        }

        //Gets a particular action key
        public Keys GetActionKey(int action)
        {
            return Controls[PlayerNum][action];
        }

        public static Keys GetActionKey(int playernum, int action)
        {
            return Controls[playernum][action];
        }

        //Gets the action keys to display in the Controls screen
        public static Keys[] GetActionKeys(int playernum)
        {
            return Controls[playernum];
        }

        //Gets the default controls for a player
        public static Keys[] GetDefaultActionKeys(int playernum)
        {
            switch(playernum)
            {
                case (int)PlayerIndex.One: return new Keys[] { Keys.X, Keys.Z, Keys.A, Keys.S, Keys.Up, Keys.Down, Keys.Left, Keys.Right, Keys.Enter };
                case (int)PlayerIndex.Two: return new Keys[] { Keys.H, Keys.N, Keys.M, Keys.U, Keys.I, Keys.K, Keys.J, Keys.L, Keys.V };
                case (int)PlayerIndex.Three: return new Keys[] { Keys.V, Keys.B, Keys.N, Keys.C, Keys.F, Keys.G, Keys.R, Keys.T, Keys.B };
                default: return new Keys[] { Keys.Q, Keys.E, Keys.M, Keys.U, Keys.I, Keys.K, Keys.J, Keys.OemPeriod, Keys.Y };
            }
        }

        //Resets the action keys back to their default values
        public static void ResetActionKeys(int playernum)
        {
            Controls[playernum] = GetDefaultActionKeys(playernum);
        }

        //Maps the keys selected in the Controls screen to the Player's keys
        public static void SetActionKeys(int playernum, Keys[] keys)
        {
            for (int i = 0; (i < keys.Length && i < Controls.Length); i++)
                Controls[playernum][i] = keys[i];
        }

        //Instance methods for checking input
        public bool PressedButton(int action, bool checkheld = false)
        {
            if (checkheld == false)
                return (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerNum, action)));
            else return (Input.KeyHeld(Player.GetActionKey(PlayerNum, action)));
        }

        public bool PressedAttack(bool checkheld = false)
        {
            return PressedButton((int)Action.Attack, checkheld);
        }

        public bool PressedJump(bool checkheld = false)
        {
            return PressedButton((int)Action.Jump, checkheld);
        }

        public bool PressedSpecAttack(bool checkheld = false)
        {
            return PressedButton((int)Action.SpecAttack, checkheld);
        }

        public bool PressedItem(bool checkheld = false)
        {
            return PressedButton((int)Action.ItemKey, checkheld);
        }

        public bool PressedUp(bool checkheld = false)
        {
            return PressedButton((int)Action.Up, checkheld);
        }

        public bool PressedDown(bool checkheld = false)
        {
            return PressedButton((int)Action.Down, checkheld);
        }

        public bool PressedLeft(bool checkheld = false)
        {
            return PressedButton((int)Action.Left, checkheld);
        }

        public bool PressedRight(bool checkheld = false)
        {
            return PressedButton((int)Action.Right, checkheld);
        }

        protected bool PressedPause()
        {
            return PressedButton((int)Action.Pause);
        }

        protected bool PressedForwards(bool checkheld = false)
        {
            int action = ForwardVal((int)Action.Right, (int)Action.Left);
            return PressedButton(action, checkheld);
        }

        protected bool PressedBackwards(bool checkheld = false)
        {
            int action = ForwardVal((int)Action.Left, (int)Action.Right);
            return PressedButton(action, checkheld);
        }

        //Offensive special attack input
        protected virtual bool OffSpecInput()
        {
            return (PressedSpecAttack() == true);
        }

        //Checks if the player pressed Up, Down, Left, or Right to select a player for the Versus Enemy to target in the Avoid Enemy versus mode
        public int? VersusEnemyTarget()
        {
            //Check which button the player pressed and return a player number that corresponds to the player targetted
            if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerNum, (int)Player.Action.Up)) == true) return (int)PlayerIndex.One;
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerNum, (int)Player.Action.Left)) == true) return (int)PlayerIndex.Two;
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerNum, (int)Player.Action.Right)) == true) return (int)PlayerIndex.Three;
            else if (Input.CheckKeyPress(KeyboardState, Player.GetActionKey(PlayerNum, (int)Player.Action.Down)) == true) return (int)PlayerIndex.Four;
            else return null;
        }

        //This is for sliding after getting knocked down or thrown - some characters slide further than others due to their weight
        protected void PlayerSlide()
        {
            //Make the character slide a certain amount if the character is on the ground and still has momentum left
            ObjectMove(new Vector2(ForwardVal(-SlideSpeed), 0));

            if (AirVelocity.Z == 0f)
            {
                float decelerate = (float)(Math.Round(((Weight - OrigJumpVelocity.X) / 304f), 2));
                SlideSpeed -= decelerate;
                if (SlideSpeed <= 0f)
                {
                    SlideSpeed = 0f;
                    Location.X = (int)Location.X;
                }
            }
        }

        public override bool CanGetGrabbed()
        {
            //NOTE: TEMPORARY NULL CHECK
            return (base.CanGetGrabbed() == true && ((StateMachine == null && IsGrabbed == false) || StateMachine.CurState != PlayerStateMachine.State.Grabbed));
        }

        //Checks if the player has knock down invincibility
        protected bool HasKnockDownInvincibility()
        {
            return (Main.GetActiveTime - PrevKnockInv) < Fighter.KnockDownInvincibility;
        }

        //Player-specific property for checking if a move grants the player invincibility
        protected virtual bool PlayerMoveInvincibility
        {
            get { return false; }
        }

        protected override bool HasMoveInvincibility
        {
            get { return (base.HasMoveInvincibility == true || IsForwardThrowing == true || IsBackwardThrowing == true || IsSpecialAttacking == true || PlayerMoveInvincibility == true); }
        }

        //The center of the weapon's drawing location (standing position) - the weapon moves and rotates around this point depending on the animation
        /*Create pre-defined positions and rotation values for each frame in each animation! Vector3 - X, Z, and Rotation*/
        public Vector3 WeaponLoc(bool FacingRight, int spritewidth)
        {
            return Vector3.Zero;

            //if (IsStabbing == true) return StabbingAnim.CurWeaponLoc(FacingRight, spritewidth);
            //else if (IsJumpLand == true) return LandingAnim.CurWeaponLoc(FacingRight, spritewidth);
            //else return StandingAnim.CurWeaponLoc(FacingRight, spritewidth);
        }

        //Take damage after hitting something or multiple things with a special attack - doesn't apply when you're invincible
        public virtual void SpecialAttackHit(float activeTime, int index)
        {
            
        }

        //Mention that the next attack should change
        public void SetNextAttack()
        {
            ProgressCombo = true;
        }

        //Checks if the player attacked within the given time frame to continue the combo
        protected void CheckComboTimeFrame()
        {
            if (Main.GetActiveTime >= PrevComboTime)
                ComboCounter = 0;
        }

        protected virtual void WilBulletChecks(float activeTime)
        {

        }

        //Check if the player should run or not
        protected bool CheckPlayerRun(float activeTime, Keys keypressed, bool facingright)
        {
            //If the player just pressed the direction to run in, start the timer for running
            if (CheckCanRun() == true && (activeTime - PrevRun) >= 230f) PrevRun = activeTime;
            //If the player double-tapped the direction quickly without turning around, start running
            else if (IsRunning == true || Input.CheckKeyPress(KeyboardState, keypressed) == true && (activeTime - PrevRun) < 230f && FacingRight == facingright)
                return true;

            return false;
        }

        private void CheckPlayerMove()
        {
            PlayerStateMachine.State state = StateMachine.CurState;
            if (state != PlayerStateMachine.State.Run) state = PlayerStateMachine.State.Walk;
            if (PrevRun != 0f && Main.GetActiveTime >= PrevRun) PrevRun = 0f;

            Vector2 moveamount = Vector2.Zero;

            if (PressedUp(true) == true)
            {
                moveamount.Y -= TrueVelocity.Y;
            }
            if (PressedDown(true) == true)
            {
                moveamount.Y += TrueVelocity.Y;
            }
            if (PressedLeft(true) == true)
            {
                moveamount.X -= TrueVelocity.X;

                if (PressedLeft() == true)
                {
                    if (PrevRun == 0f || FacingRight == true)
                        PrevRun = Main.GetActiveTime + 230f;
                    else
                    {
                        //Start running
                        if (Main.GetActiveTime < PrevRun)
                        {
                            state = PlayerStateMachine.State.Run;
                        }

                        PrevRun = 0f;
                    }
                }
            }
            if (PressedRight(true) == true)
            {
                moveamount.X += TrueVelocity.X;

                if (PressedRight() == true)
                {
                    if (PrevRun == 0f || FacingRight == false)
                        PrevRun = Main.GetActiveTime + 230f;
                    else
                    {
                        //Start running
                        if (Main.GetActiveTime < PrevRun)
                        {
                            state = PlayerStateMachine.State.Run;
                        }

                        PrevRun = 0f;
                    }
                }
            }

            if (moveamount != Vector2.Zero)
            {
                if (moveamount.X != 0)
                {
                    //Change the direction you're facing
                    ChangeDirection(moveamount.X > 0);
                }
                else
                {
                    PrevRun = 0f;
                    state = PlayerStateMachine.State.Walk;
                }

                if (StateMachine.CurState != PlayerStateMachine.State.Swim && StateMachine.CurState != state)
                {
                    if (state == PlayerStateMachine.State.Walk)
                        StateMachine.ChangeState(PlayerStateMachine.State.Walk, WalkingAnim, UpdateWalkingAnim);
                    else if (state == PlayerStateMachine.State.Run)
                        StateMachine.ChangeState(PlayerStateMachine.State.Run, RunningAnim, UpdateRunningAnim);
                }

                int runmodifier = 0;
                if (state == PlayerStateMachine.State.Run) runmodifier = ForwardVal(1);
                ObjectMove(moveamount + new Vector2(runmodifier, 0));
            }
            else if (StateMachine.CurState != PlayerStateMachine.State.Idle && StateMachine.CurState != PlayerStateMachine.State.Swim)
            {
                StateMachine.ChangeStateIdle();
            }
        }

        private void CheckPlayerAttack()
        {
            if (PressedAttack() == true)
            {
                //Basic attack
                //TEMPORARY SWIM CHECK HERE
                if (StateMachine.CurState == PlayerStateMachine.State.Swim || StateMachine.CurState == PlayerStateMachine.State.Idle || StateMachine.CurState == PlayerStateMachine.State.Walk)
                {
                    //Weapon moves if the player has a weapon
                    if (HasWeapon == true)
                    {
                        //Swing 2-handed weapons
                        if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
                        {
                            weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), SwingAnim.GetFrameLength(0), SwingAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, true);
                            StateMachine.ChangeState(PlayerStateMachine.State.SwingWeapon, SwingAnim, null);
                        }
                        //Stab with 1-handed weapons
                        else if (weapon.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
                        {
                            weapon.CreateHitboxFeet(true, weapon.GetRange, 20, weapon.Damage, (ObjectHeight / 4), StabAnim.GetFrameLength(0), StabAnim.FullDurationExcluding(0), Hitbox.HitboxTypes.KnockDown, TowardsDir, weapon.GWeaponHitSound, true);
                            StateMachine.ChangeState(PlayerStateMachine.State.StabWeapon, StabAnim, null);
                        }
                    }
                    else
                    {
                        //Check if the player attacked within the combo time frame to continue the combo
                        CheckComboTimeFrame();

                        //Allow the player to turn around before the attack
                        if (PressedLeft(true) == true)
                            ChangeDirection(false);
                        else if (PressedRight(true) == true)
                            ChangeDirection(true);

                        //TEMPORARY HITBOX
                        CreateHitboxSurrounding(5, 30, 0, CurAttackAnim.FullDuration(), Hitbox.HitboxTypes.Standard, TowardsDir, LoadSounds.Punch1, true);

                        StateMachine.ChangeState(PlayerStateMachine.State.Attack, CurAttackAnim, UpdateAtkAnim);
                    }
                }
                //Dash attack
                else if (StateMachine.CurState == PlayerStateMachine.State.Run)
                {
                    DashAttackSpeed = KnockDownVelocity + 3f;

                    StateMachine.ChangeState(PlayerStateMachine.State.DashAttack, DashAtkAnim, UpdateDashAtkAnim);
                }
                //Aerial moves
                else if (StateMachine.CurState == PlayerStateMachine.State.Airborne)
                {
                    //Down jump attack
                    if (PressedDown(true) == true)
                    {
                        StateMachine.ChangeState(PlayerStateMachine.State.DJumpAtk, DownJumpAtkAnim, UpdateJumpAtkAnim);
                    }
                    //Standing jump attack
                    else if (AirVelocity.X == 0)
                    {
                        StateMachine.ChangeState(PlayerStateMachine.State.NJumpAtk, JumpAtkAnim, UpdateJumpAtkAnim);   
                    }
                    //Running jump attack
                    else
                    {
                        StateMachine.ChangeState(PlayerStateMachine.State.WalkJumpAtk, RunJumpAttackAnim, UpdateJumpAtkAnim);
                    }
                }
                //Grab moves
                else if (StateMachine.CurState == PlayerStateMachine.State.GrabIdle)
                {
                    //Check for pressing forward for forward throws or back for backwards throws
                    if (PressedForwards(true) == true)
                    {
                        //TEMPORARY HITBOX

                        StateMachine.ChangeState(PlayerStateMachine.State.FThrow, FThrowAnim, UpdateThrowingAnim);
                    }
                    else if (PressedBackwards(true) == true)
                    {
                        StateMachine.ChangeState(PlayerStateMachine.State.BThrow, FThrowAnim, UpdateThrowingAnim);
                    }
                    else
                    {
                        //Standard pummels if the Player is facing the Fighter, otherwise Back Grapple
                        if (GrabbedFighter == null || FacingRight != GrabbedFighter.FacingRight)
                        {
                            StateMachine.ChangeState(PlayerStateMachine.State.GrabAttack, CurGrabAttackAnim, UpdateGrabAtkAnim);
                        }
                        else StateMachine.ChangeState(PlayerStateMachine.State.BGrapple, null, null);
                    }
                }
            }
        }

        private void CheckPlayerJump()
        {
            if (PressedJump() == true)
            {
                if (StateMachine.CurState == PlayerStateMachine.State.GrabIdle && GrabbedFighter != null) ClearGrabbedFighter();

                if (IsInWater == false)
                {
                    StateMachine.ChangeState(PlayerStateMachine.State.StartJump, LandAnim, UpdateLandAnim);
                }
                else
                {
                    Jump(new Vector3(0f, 0f, OrigJumpVelocity.Z / 1.5f));

                    StateMachine.ChangeState(PlayerStateMachine.State.Swim, SwimAnim, UpdateSwimmingAnim);
                }
            }
        }

        //Default to Graham's
        private void CheckPlayerOffSpecial()
        {
            if (OffSpecInput() == true)
            {
                StateMachine.ChangeState(PlayerStateMachine.State.OffSpec, OffSpecAnim, UpdateOffSpecAnim);
            }
        }

        private void CheckPlayerDefSpecial()
        {
            if (PressedSpecAttack() == true)
            {
                if (StateMachine.CurState == PlayerStateMachine.State.GrabIdle && GrabbedFighter != null) ClearGrabbedFighter();

                //No delegate for now; will swap with character-specific ones

                StateMachine.ChangeState(PlayerStateMachine.State.DefSpec, DefSpecAnim, UpdateDefSpecAnim);
            }
        }

        //Double-tap a horizontal direction to start running
        //NOTE: REFACTOR THIS SO YOU CALL OBJECTMOVE ONLY ONCE!
        protected void PlayerMove(float activeTime, SubLevel level)
        {
            //This conditional checks for if the player is holding up and down or left and right at the same time and prevents the character from doing the walk animation in place
            if ((Input.KeyHeld(GetActionKey((int)Action.Up)) && Input.KeyHeld(GetActionKey((int)Action.Down))) || (Input.KeyHeld(GetActionKey((int)Action.Left)) && Input.KeyHeld(GetActionKey((int)Action.Right))))
            {
                IsWalking = false;
                IsRunning = false;
                return;
            }

            //Check if the player is about to start walking or running so you can reset those animations
            bool runwalking = IsWalking | IsRunning;

            //Check if the player is moving only up or down and stop the player from running if so
            bool verticalcheck = false;

            if (Input.KeyHeld(GetActionKey((int)Action.Up)))
            {
                if (IsRunning == false) IsWalking = true;
                ObjectMove(new Vector2(0, -TrueVelocity.Y));
            }
            if (Input.KeyHeld(GetActionKey((int)Action.Down)))
            {
                if (IsRunning == false) IsWalking = true;
                ObjectMove(new Vector2(0, TrueVelocity.Y));
            }
            if (Input.KeyHeld(GetActionKey((int)Action.Left)))
            {
                //See if the player should run
                if (CheckPlayerRun(activeTime, GetActionKey((int)Action.Left), false) == true)
                {
                    IsWalking = false;
                    IsRunning = true;
                    PrevRun = 0f;
                }
                //Otherwise make the player walk
                else IsWalking = true;

                FacingRight = false;
                verticalcheck = true;
                ObjectMove(new Vector2(-TrueVelocity.X, 0));
            }
            if (Input.KeyHeld(GetActionKey((int)Action.Right)))
            {
                //See if the player should run
                if (CheckPlayerRun(activeTime, GetActionKey((int)Action.Right), true) == true)
                {
                    IsWalking = false;
                    IsRunning = true;
                    PrevRun = 0f;
                }
                //Otherwise make the player walk
                else IsWalking = true;

                FacingRight = true;
                verticalcheck = true;
                ObjectMove(new Vector2(TrueVelocity.X, 0));
            }

            //If the character is walking or running and the player is not holding a movement direction, stop the character from walking and running
            if ((IsWalking == true || IsRunning == true) && Input.KeyHeld(GetActionKey((int)Action.Right)) == false && Input.KeyHeld(GetActionKey((int)Action.Left)) == false && Input.KeyHeld(GetActionKey((int)Action.Up)) == false && Input.KeyHeld(GetActionKey((int)Action.Down)) == false)
            {
                IsWalking = false;

                //Allow the character's running speed to carry over when jumping
                if (CanJump == true) IsRunning = false;
            }

            //If the player isn't holding forwards or backwards and the character is running, stop the character from running
            if (verticalcheck == false && IsRunning == true)
            {
                IsRunning = false;
                IsWalking = true;
            }

            //Reset the walking animation if the player just started walking or running
            if (runwalking == false && (IsWalking == true || IsRunning == true))
            {
                WalkingAnim.Reset();
                RunningAnim.Reset();
            }
        }

        //Make the character's dash attack move smoothly
        protected void DashAttack()
        {
            ObjectMove(new Vector2(ForwardVal(DashAttackSpeed), 0));

            DashAttackSpeed = DashAttackSpeed <= .3f ? .3f : (float)Math.Round(DashAttackSpeed - DashAttackSpeedChange, 1);
        }

        //The player's main attacks (basic attacks, jump attacks, dash attacks, grab attacks, weapon attacks, etc.)
        protected virtual void PlayerAttack(float activeTime, SubLevel level)
        {
            
        }
        
        //The player's special attack - can be used in hitstun and while grabbing an enemy
        protected virtual void PlayerSpecialAttack()
        {
            
        }

        protected virtual void PlayerJump(float activeTime, SubLevel level)
        {
            if (CanJump == true)
            {
                if (Controlled == true && status != (int)Status.Statuses.NoJump && Input.CheckKeyPress(KeyboardState, Player.Controls[PlayerNum][(int)Player.Action.Jump]) == true)
                {
                    if (CheckJump() == true)
                    {
                        LandAnim.Reset();

                        IsJumpLand = true;
                        IsWalking = false;
                        CanJump = false;

                        //If you're grabbing an enemy but not doing anything with the enemy, you can jump out of the grab
                        if (Enem != null)
                            Enem.GrabRecover();
                    }
                }
            }
            //Rise, then fall until you land on top of something (tile or solid)
            else if (IsJumpLand == false)
            {
                //Allow the player to control the height of the character's jump depending on how long the jump key is held
                if (Controlled == true && AirVelocity.Z > 1.4f && AirVelocity.Z < 2.4f && Keyboard.GetState().IsKeyUp(Controls[PlayerNum][(int)Action.Jump]))
                    AirVelocity.Z = 1.4f;

                ObjectMove(new Vector2(AirVelocity.X, 0));
            }
        }

        //For when the player is grabbing an enemy and switches over to the other side of the enemy via the Item key (also called a Vault)
        protected void SwitchSide()
        {
            SwitchSidesAnim.Reset();
            IsSwitchingSide = true;
            //Enem.gGrabbox.Halt();
        }

        //Check if the player can pick up an item or weapon
        protected virtual void CheckPickUp(float activeTime, SubLevel level)
        {
            if (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.ItemKey]) == true)
            {
                //If you're grabbing an enemy, switch to the other side of the enemy when you press the Item key
                if (CheckSwitchSide(level) == true)
                {
                    SwitchSide();
                }

                //Throw or drop a weapon if the player has one
                if (weapon != null)
                {
                    //If the weapon is held rather than worn, check if you can throw it and check if the player is already holding the attack button; throw it if so
                    if (weapon.GetWeaponType != (int)Weapon.WeaponTypes.None && CheckCanThrow() == true && Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Attack]))
                    {
                        //Throw the weapon and make sure you can't get hurt by the same weapon you just threw
                        weapon.Throw();

                        StabAnim.Reset();
                        IsStabbing = true;
                        IsRunning = false;

                        weapon = null;
                    }
                    //Otherwise, drop the weapon if you can
                    else if (CheckCanPickUp() == true)
                    {
                        weapon.Drop();
                    }
                }
            }
        }

        //Makes the player pick up an item or weapon
        public override void Pickup(PickupObj pickupobj)
        {
            base.Pickup(pickupobj);

            //TEMPORARY NULL CHECK
            if (StateMachine != null) StateMachine.ChangeState(PlayerStateMachine.State.Pickup, PickUpAnim, null);

            //The player should stop running after picking something up
            PickUpAnim.Reset();
            IsPickingUp = true;
            IsRunning = false;
        }

        public override void DamageObject(BeatEmUpObj victim, Hitbox hitbox)
        {
            base.DamageObject(victim, hitbox);  
       
            //If the Player is using a special attack, subtract health for damaging the object


            //If the object is comboable and the current move can combo, move onto the next sequence in the combo
            if (CanCombo(victim) == true)
                SetNextAttack();

            //Add the Hitbox's score value to the Player's points
            AddScore(hitbox.GetScoreValue);
       
            //If the Player can set its InteractionHUD to the victim's HUD, do so
            if (victim.CanSetHUD() == true) SetInteractionHUD(victim.hud);
        }

        protected override void HitLagActions(float diff)
        {
            base.HitLagActions(diff);

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null) StateMachine.Anim.DelayAnim(diff);
        }

        protected override void BeginFall()
        {
            base.BeginFall();

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                if (IsInWater == false)
                {
                    StateMachine.ChangeState(PlayerStateMachine.State.Airborne, JumpingAnim, UpdateJumpingAnim);
                }
                else StateMachine.ChangeState(PlayerStateMachine.State.Swim, SwimAnim, UpdateSwimmingAnim);
            }

            CanJump = false;
        }

        protected override void LandKnockedDown()
        {
            ClearRotation();

            if (ObjectTile.Z >= 0f)
                LoadSounds.Play(LoadSounds.Land);
        }

        protected override void LandNormal()
        {
            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                if (IsInWater == false)
                {
                    StateMachine.ChangeState(PlayerStateMachine.State.Landing, LandAnim, UpdateLandAnim);
                }
                else StateMachine.ChangeStateIdle();
            }
            else
            {
                LandAnim.Reset();
                IsJumpLand = true;
                CanJump = true;
            }
                
            if (ObjectTile.Z >= 0f)
                LoadSounds.Play(LoadSounds.JumpLand);            
        }

        //Make the player grab an enemy
        public virtual void GrabEnemy(Enemy enem, SubLevel level)
        {
            ResetActions();
            IsGrabbing = true;
            //Enem = enem;
            GrabbedFighter = enem;
            KeyboardState = Keyboard.GetState();
        }

        public override void GrabFighter(Fighter fighter)
        {
            base.GrabFighter(fighter);
            ResetActions();
            IsGrabbing = true;

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                StateMachine.ChangeState(PlayerStateMachine.State.GrabIdle, GrabAnim, UpdateGrabbingAnim);
            }

            //If the Player can set its InteractionHUD to the victim's HUD, do so
            if (fighter.CanSetHUD() == true) SetInteractionHUD(fighter.hud);
        }

        public override void GetGrabbed(BeatEmUpObj opponent)
        {
            base.GetGrabbed(opponent);

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                StateMachine.ChangeState(PlayerStateMachine.State.Grabbed, Grabbedanim, null);
            }
            else
            {
                Grabbedanim.Reset();
                IsGrabbed = true;
            }
        }

        //Release the enemy from the grab if you're holding backwards while grabbing
        protected void ReleaseEnemy(float activeTime)
        {
            //If you're not holding backwards, check if you're holding it
            if (GrabReleased == false)
            {
                //Check if you're grabbing an enemy, not attacking or throwing, and holding backwards
                if (IsGrabbing == true && GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsSwitchingSide == false && Keyboard.GetState().IsKeyDown(Backwards))
                {
                    GrabReleased = true;
                    GrabRelease = activeTime;
                }
            }
            else
            {
                //Check if the backwards button is up or another action is taken while grabbing
                if (GrabAttacking == true || IsBackwardThrowing == true || IsForwardThrowing == true || IsSwitchingSide == true || Keyboard.GetState().IsKeyUp(Backwards))
                {
                    GrabReleased = false;
                    return;
                }

                //Check if you were holding backwards long enough to break the grab
                if (IsOnGround == true && (activeTime - GrabRelease) >= 400f)
                {
                    //Break the grab
                    ClearGrabbedFighter();
                    GrabReleased = false;
                }
            }
        }

        //Checks what the player is holding
        protected virtual void CheckHolds(float activeTime, SubLevel level)
        {
            
        }

        //Animation updates
        private void UpdateIdleAnim()
        {
            if (StandAnim.IsAnimationEnd() == false)
            {
                StandAnim.Update(Main.GetActiveTime, GravityValue);
                if (StandAnim.IsAnimationEnd() == true)
                {
                    //StateMachine.ChangeState(VictoriousAnim, PlayerStateMachine.State.Victory, () => VictoriousAnim.Update(Main.GetActiveTime));
                }
            }
        }

        protected void UpdateStandingAnim()
        {
            //Update the character's standing animation if the character isn't in the idle animation yet
            if (StandAnim.IsAnimationEnd() == false)/*if (StandAnim.IsAnimationEnd() == false) - StandAnim can just be set to loop 100 times, in which case it'll end then*/
            {
                StandAnim.Update(Main.GetActiveTime, GravityValue);
                if (StandAnim.IsAnimationEnd() == true)
                {
                    IsVictorious = true;
                    VictoriousAnim.Reset();
                }
            }
            //If the character has been standing for too long, start the idle animation
            else
            {
                VictoriousAnim.Update(Main.GetActiveTime, GravityValue);
            }
        }

        protected void ResetStandingAnim()
        {
            StandAnim.Reset();
            IsVictorious = false;
        }

        protected void UpdateVictoryAnim()
        {
            //Make it stay on the final frame of the victory animation
            if (IsVictorious == true && VictoriousAnim.CurrentFrame != VictoriousAnim.MaxFrame)
                VictoriousAnim.Update(Main.GetActiveTime, GravityValue);
        }

        private void UpdateWalkingAnim()
        {
            if (StateMachine.CurState == PlayerStateMachine.State.Walk)
            {
                WalkingAnim.Update(Main.GetActiveTime, GravityValue);
            }
        }

        private void UpdateRunningAnim()
        {
            RunningAnim.Update(Main.GetActiveTime, GravityValue);
        }

        protected void UpdateWalkAnim()
        {
            if (IsWalking == true)
            {
                WalkingAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();
            }
            else if (IsRunning == true)
            {
                RunningAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();
            }
        }

        private void UpdateAtkAnim()
        {
            CurAttackAnim.Update(Main.GetActiveTime, GravityValue);
            if (CurAttackAnim.IsAnimationEnd() == true)
            {
                //NOTE: TEMPORARY ISINWATER CHECK
                if (IsInAir == true && IsInWater == true) StateMachine.ChangeState(PlayerStateMachine.State.Swim, SwimAnim, UpdateSwimmingAnim);
                else StateMachine.ChangeStateIdle();

                PrevAttackMove = Main.GetActiveTime + 150f;
                if (ProgressCombo == true)
                {
                    if ((ComboCounter + 1) >= AttackingAnim.Count)
                    {
                        ComboCounter = 0;
                        PrevComboTime = 0f;
                    }
                    else
                    {
                        ComboCounter++;
                        PrevComboTime = Main.GetActiveTime + ComboTimeFrame;
                    }

                    ProgressCombo = false;
                }
                else
                {
                    ComboCounter = 0;
                    PrevComboTime = 0f;
                }
            }
        }

        protected void UpdateAttackAnim()
        {
            if (IsAttacking == true && IsRunning == false)
            {
                CurAttackAnim.Update(Main.GetActiveTime, GravityValue);
                if (CurAttackAnim.IsAnimationEnd() == true)
                {
                    IsAttacking = false;
                    PrevAttackMove = Main.GetActiveTime;
                    if (ProgressCombo == true)
                    {
                        if ((ComboCounter + 1) >= AttackingAnim.Count)
                        {
                            ComboCounter = 0;
                            PrevComboTime = 0f;
                        }
                        else
                        {
                            ComboCounter++;
                            PrevComboTime = Main.GetActiveTime + ComboTimeFrame;
                        }

                        ProgressCombo = false;
                    }
                    else
                    {
                        ComboCounter = 0;
                        PrevComboTime = 0f;
                    }
                }

                ResetStandingAnim();
            }
        }

        private void UpdateGrabAtkAnim()
        {
            CurGrabAttackAnim.Update(Main.GetActiveTime, GravityValue);
            if (CurGrabAttackAnim.IsAnimationEnd() == true)
            {
                int combocounterinc = ComboCounter + 1;

                //If we're not at the end of the grab attacks, increment the combo counter and change back to GrabIdle
                if (combocounterinc < GrabAttackingAnim.Count)
                {
                    ComboCounter = combocounterinc;

                    if (GrabbedFighter != null)
                        StateMachine.ChangeState(PlayerStateMachine.State.GrabIdle, GrabAnim, UpdateGrabbingAnim);
                    else StateMachine.ChangeStateIdle();
                }
                //We're done with the grab attacks, so change back to idle
                else
                {
                    ComboCounter = 0;
                    ClearGrabbedFighter();
                    StateMachine.ChangeStateIdle();
                }
            }
        }

        protected void UpdateGrabAttackAnim()
        {
            if (GrabAttacking == true)
            {
                CurGrabAttackAnim.Update(Main.GetActiveTime, GravityValue);
                if (CurGrabAttackAnim.IsAnimationEnd() == true)
                {
                    GrabAttacking = false;

                    //Safeguard; the last hit of a grab attack should always knock the victim down and end the combo, but in case it doesn't we'll restart the combo
                    if ((ComboCounter + 1) < GrabAttackingAnim.Count)
                        ComboCounter++;
                    else ComboCounter = 0;
                }
                ResetStandingAnim();
            }
        }

        protected void UpdateSwitchSideAnim()
        {
            if (IsSwitchingSide == true)
            {
                bool reverse = SwitchSidesAnim.IsReversing();

                SwitchSidesAnim.Update(Main.GetActiveTime, GravityValue);
                if (SwitchSidesAnim.IsAnimationEnd() == true)
                {
                    IsSwitchingSide = false;
                    /*if (Enem != null)
                    {
                        Enem.gGrabbox.Switched();
                        Enem.gGrabbox.UnHalt();
                        if (Enem.gGrabbox.ShouldBreakGrab() == false) Enem.GetGrabbed(activeTime, FacingRight, this, null, FeetLoc, CurHeight, level);
                    }*/
                }
                //Change direction when reversing the animation
                else if (reverse != SwitchSidesAnim.IsReversing())
                {
                    if (GrabbedFighter != null)
                    {
                        Vector2 movevel = new Vector2(BrushUpHorizontal(GrabbedFighter, FacingRight), MatchFeetTop(GrabbedFighter));

                        //Make the player go to the other side of the enemy after switching sides
                        ObjectMove(movevel);
                    }

                    FlipDirection();
                }

                ResetStandingAnim();
            }
        }

        protected virtual void UpdateDashAtkAnim()
        {
            DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
            if (DashAtkAnim.IsAnimationEnd() == true)
            {
                //Make sure the character is on a whole number X value
                Location.X = (int)Location.X;

                StateMachine.ChangeStateIdle();
            }
            else if (DashAtkAnim.CurrentFrame > 0)
                DashAttack();
            else Location.X = (int)Location.X;
        }

        protected virtual void UpdateDashAttackAnim()
        {
            if (IsRunning == true && IsAttacking == true)
            {
                DashAtkAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();

                if (DashAtkAnim.IsAnimationEnd() == true)
                {
                    IsRunning = false;
                    IsAttacking = false;

                    //Make sure the character is on a whole number X value
                    Location.X = (int)Location.X;
                }
                //Make the character's dash attack move smoothly
                else if (DashAtkAnim.CurrentFrame > 0)
                    DashAttack();
                else Location.X = (int)Location.X;
            }
        }

        private void UpdateSwimmingAnim()
        {
            SwimAnim.Update(Main.GetActiveTime);
        }

        private void UpdateJumpingAnim()
        {
            //Move the player during the jumping animation
            ObjectMove(GetAirVelocityVec2);

            if (JumpingAnim.CurrentFrame == 0 && AirVelocity.Z <= 0f)
                JumpingAnim.Update(Main.GetActiveTime);
        }

        protected void UpdateJumpAnim()
        {
            if (CanJump == false)
            {
                if (JumpingAnim.CurrentFrame == 0 && AirVelocity.Z <= 0f)
                    JumpingAnim.Update(Main.GetActiveTime);
            }
        }

        private void UpdateGrabbingAnim()
        {
            //Check if the player wants to release a grabbed enemy
            ReleaseEnemy(Main.GetActiveTime);

            //Change to idle if the grabbed fighter broke from your grab
            //NOTE: This may be moved to the instant the fighter breaks from your grab
            if (GrabbedFighter == null)
            {
                StateMachine.ChangeStateIdle();
            }
        }

        protected void UpdateGrabbedAnim()
        {
            if (IsGrabbed == true)
            {
                Grabbedanim.Update(Main.GetActiveTime, GravityValue);
                if (Grabbedanim.IsAnimationEnd() == true)
                    IsGrabbed = false;
                ResetStandingAnim();
            }
        }

        private void UpdateHurtAnim()
        {
            HurtAnim.Update(Main.GetActiveTime, GravityValue);
            if (IsInHitstun == false)
                StateMachine.ChangeStateIdle();
        }

        protected void UpdateHitAnim()
        {
            if (IsHit == true)
            {
                HurtAnim.Update(Main.GetActiveTime, GravityValue);
                if (IsInHitstun == false)
                    IsHit = false;
                ResetStandingAnim();
            }
        }

        protected void UpdatePickUpAnim()
        {
            if (IsPickingUp == true)
            {
                PickUpAnim.Update(Main.GetActiveTime, GravityValue);
                if (PickUpAnim.IsAnimationEnd() == true)
                    IsPickingUp = false;
                ResetStandingAnim();
            }
        }

        private void UpdateKnockedDownAnim()
        {
            //If the player is on the second frame of the knock down animation (thus on the ground), start making the player recover
            if (CurKnockDownAnim.CurrentFrame != 0)
            {
                //If the player is on the ground, update the knock down animation; otherwise, start the animation over
                if (IsOnGround == true)
                {
                    PlayerSlide();
                    CurKnockDownAnim.Update(Main.GetActiveTime, status == (int)Status.Statuses.StunDown ? .4f : .2f);
                }
                else
                {
                    CurKnockDownAnim.Reset();
                    AirVelocity.X = ForwardVal(-SlideSpeed);
                }

                //Check if the character is dead, on the floor, and not invincible yet
                if (IsDead == true && status != (int)Status.Statuses.Invincible)
                {
                    InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));

                    SoundEffect deathsound = LoadSounds.PDeath[CharNum];
                    if (deathsound != null) LoadSounds.Play(deathsound);
                }

                //If the player's jump velocity is 0, the player is on the floor; once that's true, check if the animation is done or if the player is dead, which ends the animation earlier
                if (AirVelocity.Z == 0f && (CurKnockDownAnim.IsAnimationEnd() == true || IsDead == true && CurKnockDownAnim.CurrentFrame > 1))
                {
                    KnockDownRecover();
                }
            }
            else
            {
                //Move in the air
                ObjectMove(GetAirVelocityVec2);
            }
        }

        protected void UpdateKnockDownAnim()
        {
            //If the player is knocked down, update the animation
            if (IsKnockedDown == true)
            {
                //If the player is on the second frame of the knock down animation (thus on the ground), start making the player recover
                if (CurKnockDownAnim.CurrentFrame != 0)
                {
                    PlayerSlide();

                    //If the player is on the ground, update the knock down animation; otherwise, start the animation over
                    if (IsInAir == false)
                    {
                        CurKnockDownAnim.Update(Main.GetActiveTime, status == (int)Status.Statuses.StunDown ? .4f : .2f);
                    }
                    else
                    {
                        CurKnockDownAnim.Reset();
                        AirVelocity.X = FacingRight == false ? SlideSpeed : -SlideSpeed;
                    }

                    //Check if the character is dead, on the floor, and not invincible yet
                    if (IsDead == true && status != (int)Status.Statuses.Invincible)
                    {
                        InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));

                        SoundEffect deathsound = LoadSounds.PDeath[CharNum];
                        if (deathsound != null) LoadSounds.Play(deathsound);
                    }

                    //If the player's jump velocity is 0, the player is on the floor; once that's true, check if the animation is done or if the player is dead, which ends the animation earlier
                    if (AirVelocity.Z == 0f && (CurKnockDownAnim.IsAnimationEnd() == true || IsDead == true && CurKnockDownAnim.CurrentFrame == 2))
                    {
                        KnockDownRecover();
                    }
                }
                else
                {
                    ObjectMove(new Vector2(AirVelocity.X, 0));
                }

                ResetStandingAnim();
            }
        }

        private void UpdateThrowingAnim()
        {
            StateMachine.Anim.Update(Main.GetActiveTime, GravityValue);
            if (StateMachine.Anim.IsAnimationEnd() == true)
            {
                //Clear the grabbed fighter at the end of the throw
                ClearGrabbedFighter();
                StateMachine.ChangeStateIdle();
            }
        }

        protected void UpdateThrowAnim()
        {
            if (IsBackwardThrowing == true || IsForwardThrowing == true)
            {
                FThrowAnim.Update(Main.GetActiveTime, GravityValue);
                if (FThrowAnim.IsAnimationEnd() == true)
                {
                    IsBackwardThrowing = false;
                    IsForwardThrowing = false;
                }

                ResetStandingAnim();
            }
        }

        protected virtual void UpdateOffSpecAnim()
        {
            ObjectMove(GetAirVelocityVec2);
            OffSpecAnim.Update(Main.GetActiveTime, GravityValue);
        }

        protected virtual void UpdateDefSpecAnim()
        {
            DefSpecAnim.Update(Main.GetActiveTime, GravityValue);
            if (DefSpecAnim.IsAnimationEnd() == true)
            {
                StateMachine.ChangeStateIdle();
            }
        }

        protected virtual void UpdateSpecialAttackAnim()
        {
            if (IsSpecialAttacking == true)
            {
                DefSpecAnim.Update(Main.GetActiveTime, GravityValue);
                ResetStandingAnim();
                if (DefSpecAnim.IsAnimationEnd() == true)
                {
                    IsSpecialAttacking = false;
                    SpecialAtkHit = false;
                }
            }
        }

        private void UpdateJumpAtkAnim()
        {
            ObjectMove(GetAirVelocityVec2);

            if (StateMachine.Anim.IsAnimationEnd() == false)
                StateMachine.Anim.Update(GravityValue);
        }

        protected void UpdateJumpAttackAnim()
        {
            if (JumpAttacking == true)
            {
                if (AirVelocity.X != 0f)
                {
                    if (RunJumpAttackAnim.IsAnimationEnd() == false)
                        RunJumpAttackAnim.Update(GravityValue);
                }
                else
                {
                    if (JumpAtkAnim.IsAnimationEnd() == false)
                        JumpAtkAnim.Update(GravityValue);
                }
            }
            else if (DownJumpAttacking == true)
            {
                if (DownJumpAtkAnim.IsAnimationEnd() == false)
                    DownJumpAtkAnim.Update(GravityValue);
            }
        }

        protected void UpdateStabSwingAnim()
        {
            if (IsStabbing == true)
            {
                StabAnim.Update(Main.GetActiveTime, GravityValue);
                if (StabAnim.IsAnimationEnd() == true)
                    IsStabbing = false;
                ResetStandingAnim();
            }
            else if (IsSwinging == true)
            {
                SwingAnim.Update(Main.GetActiveTime, GravityValue);
                if (SwingAnim.IsAnimationEnd() == true)
                    IsSwinging = false;
                ResetStandingAnim();
            }
        }

        private void UpdateLandAnim()
        {
            LandAnim.Update(Main.GetActiveTime);
            if (LandAnim.IsAnimationEnd() == true)
            {
                //Check for the start of the jump
                if (StateMachine.CurState == PlayerStateMachine.State.StartJump)
                {
                    Vector3 airvelocity = new Vector3(0f, 0f, OrigJumpVelocity.Z);

                    //If the player was running before starting the jump, add the speed modifier to it
                    int runmodifier = (StateMachine.PrevState == PlayerStateMachine.State.Run) ? 1 : 0;

                    //Running jumps
                    if (PressedLeft(true) == true)
                    {
                        FacingRight = false;
                        airvelocity.X = -TrueVelocity.X - runmodifier;
                    }
                    else if (PressedRight(true) == true)
                    {
                        FacingRight = true;
                        airvelocity.X = TrueVelocity.X + runmodifier;
                    }

                    Jump(airvelocity);
                    StateMachine.ChangeState(PlayerStateMachine.State.Airborne, JumpingAnim, UpdateJumpingAnim);
                }
                //Otherwise we're landing on the ground
                else
                {
                    //Allow the player to jump again if he/she is holding jump when landing
                    if (PressedJump(true) == true)
                    {
                        StateMachine.ChangeState(PlayerStateMachine.State.StartJump, LandAnim, UpdateLandAnim, false);
                    }
                    else StateMachine.ChangeStateIdle();
                }
            }
        }

        protected void UpdateLandingAnim()
        {
            if (IsJumpLand == true)
            {
                LandAnim.Update(Main.GetActiveTime);
                if (LandAnim.IsAnimationEnd() == true)
                {
                    IsJumpLand = false;

                    //For changing your direction right before the jump - only applies while starting the jump
                    if (CanJump == false)
                    {
                        //Standing jump by default
                        Vector3 airvelocity = new Vector3(0f, 0f, OrigJumpVelocity.Z);
                        JumpingAnim.Reset();

                        //If the character is jumping for a cutscene, prevent this from overriding the direction the character is jumping in
                        if (Controlled == true)
                        {
                            //Running jumps
                            if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Left]))
                            {
                                FacingRight = false;
                                airvelocity.X = -TrueVelocity.X;
                            }
                            else if (Keyboard.GetState().IsKeyDown(Controls[PlayerNum][(int)Action.Right]))
                            {
                                FacingRight = true;
                                airvelocity.X = TrueVelocity.X;
                            }
                        }

                        Jump(airvelocity);
                    }
                }

                ResetStandingAnim();
            }
        }
        //End updates

        //Checks if the character can use a special attack or not
        protected virtual bool CheckCanUseSpecialAttack()
        {
            float healthvalue = Health;

            //Check if the player has a temp health bar and use that value plus the player's health instead of just the health
            if (TempBar > 0f)
            {
                healthvalue = TempBar + Health;
            }

            return (status != (int)Status.Statuses.NoSpecial && CheckSpecialAttacking() == true && (status == (int)Status.Statuses.Invincible || (healthvalue >= 11 || HealthBars > 0)));
        }

        //Checks for animations
        protected virtual bool CheckAttacking()
        {
            return (IsWell() == true && IsAttacking == false && JumpAttacking == false && DownJumpAttacking == false && GrabAttacking == false && IsBackwardThrowing == false && IsForwardThrowing == false && IsSwitchingSide == false && IsSpecialAttacking == false && IsPickingUp == false && IsJumpLand == false && IsSwinging == false && IsStabbing == false);
        }

        protected virtual bool CheckSpecialAttacking()
        {
            return (IsKnockedDown == false && IsGrabbed == false && IsSwitchingSide == false && IsPickingUp == false && IsAttacking == false && IsJumpLand == false && IsSwinging == false && IsStabbing == false && IsForwardThrowing == false && IsBackwardThrowing == false && GrabAttacking == false);
        }

        protected virtual bool CheckCanRun()
        {
            return (IsWalking == false && IsRunning == false);
        }

        protected virtual bool CheckWalking()
        {
            return (IsWell() == true && IsPickingUp == false && IsJumpLand == false && IsGrabbing == false && IsAttacking == false && GrabAttacking == false && IsSwinging == false && IsStabbing == false && IsSpecialAttacking == false && IsForwardThrowing == false && IsBackwardThrowing == false && CanJump == true);
        }

        protected virtual bool CheckJump()
        {
            return (IsWell() == true && IsSwitchingSide == false && IsPickingUp == false && IsSwinging == false && IsStabbing == false && IsAttacking == false && IsSpecialAttacking == false && IsForwardThrowing == false && IsBackwardThrowing == false && GrabAttacking == false);
        }

        public override bool CanPickUp()
        {
            return (base.CanPickUp() == true && Controlled == true && IsWell() == true && IsGrabbing == false && IsSwinging == false && IsStabbing == false && IsJumpLand == false && IsAttacking == false && IsSpecialAttacking == false && CanJump == true && IsPickingUp == false && Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.ItemKey]) == true && (weapon == null || (weapon != null && Keyboard.GetState().IsKeyUp(Controls[PlayerNum][(int)Action.Attack]))));
        }

        //Checks if the player can pick up or drop an item - if the player has a weapon, make sure that throwing it takes precedence over picking up an item
        public virtual bool CheckCanPickUp()
        {
            return (Controlled == true && status != (int)Status.Statuses.NoGrab && IsWell() == true && IsGrabbing == false && IsSwinging == false && IsStabbing == false && IsJumpLand == false && IsAttacking == false && IsSpecialAttacking == false && CanJump == true && IsPickingUp == false && (Input.CheckKeyPress(KeyboardState, Controls[PlayerNum][(int)Action.ItemKey]) == true && (weapon == null || (weapon != null && (weapon.GetWeaponType == (int)Weapon.WeaponTypes.None || Keyboard.GetState().IsKeyUp(Controls[PlayerNum][(int)Action.Attack]))))));
        }

        //Checks if the player can switch sides
        protected virtual bool CheckSwitchSide(SubLevel level)
        {
            return (Enem != null && IsSwitchingSide == false && IsForwardThrowing == false && IsBackwardThrowing == false && GrabAttacking == false);
        }

        //Checks if the player can throw a weapon
        protected virtual bool CheckCanThrow()
        {
            return (IsWell() == true && IsGrabbing == false && IsSwitchingSide == false && IsSwinging == false && IsStabbing == false && IsJumpLand == false && IsAttacking == false && IsSpecialAttacking == false && CanJump == true && IsPickingUp == false);
        }

        public bool CanCombo(BeatEmUpObj victim)
        {
            //Check for object types that can be comboed
            if (IsObjectFighter(victim) == true)
            {
                //NOTE: TEMPORARY NULL CHECK
                if (StateMachine != null)
                {
                    return (StateMachine.CurState == PlayerStateMachine.State.Attack);
                }

                if (IsAttacking == true && IsRunning == false)
                {
                    return true;
                }
            }

            return false;
        }

        public bool CanGrabEnem()
        {
            if (StateMachine == null)
                return (base.CanGrab() == true && Controlled == true && (IsWalking == true || IsRunning == true) && IsAttacking == false && IsSpecialAttacking == false && IsSwinging == false && IsStabbing == false && IsJumpLand == false && IsGrabbing == false && IsForwardThrowing == false && IsBackwardThrowing == false && GrabAttacking == false);
            else return (base.CanGrab() == true && StateMachine.CanGrab() == true);
        }

        //Checks if the player is hit, grabbed, or knocked down
        public bool IsWell()
        {
            return (IsKnockedDown == false && IsGrabbed == false && IsHit == false);
        }
        //End checks

        //General animations that all characters draw; character-specific ones are drawn in their respective classes
        protected virtual void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, float waterheight, Color drawcolor, float Depth)
        {
            Vector2 drawloc = GetDrawLoc(OffSet);

            if (IsKnockedDown == true)
                CurKnockDownAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsGrabbed == true)
                Grabbedanim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsHit == true)
                HurtAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsPickingUp == true)
                PickUpAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsJumpLand == true)
                LandAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (DownJumpAttacking == true)
                DownJumpAtkAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (JumpAttacking == true && AirVelocity.X != 0f)
                RunJumpAttackAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (JumpAttacking == true && AirVelocity.X == 0f)
                JumpAtkAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsSpecialAttacking == true)
                DefSpecAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsBackwardThrowing == true || IsForwardThrowing == true)
                FThrowAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (GrabAttacking == true)
                CurGrabAttackAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsSwinging == true)
                SwingAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsStabbing == true)
                StabAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsAttacking == true && IsRunning == true)
                DashAtkAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsAttacking == true)
                CurAttackAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (CanJump == false)
                JumpingAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsRunning == true)
                RunningAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsWalking == true)
                WalkingAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsSwitchingSide == true)
                SwitchSidesAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsGrabbing == true)
                GrabAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
            else if (IsVictorious == true)
                VictoriousAnim.Draw(spriteBatch, SpriteSheet, drawloc, false, drawcolor, 0f, Depth);
            //Play a special animation after the standing animation loops a certain number of times
            else StandAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, 0f, Depth);
        }

        //Updates Animations
        public override void UpdateAnimations()
        {
            //NOTE: TEMPORARY NULL CHECK!
            if (StateMachine == null)
            {
                UpdateStandingAnim();
                UpdateVictoryAnim();
                UpdateSpecialAttackAnim();
                UpdateThrowAnim();
                UpdateGrabAttackAnim();
                UpdateSwitchSideAnim();
                UpdateDashAttackAnim();
                UpdateStabSwingAnim();
                UpdateAttackAnim();
                UpdateWalkAnim();
                UpdateJumpAnim();
                UpdateJumpAttackAnim();
                UpdatePickUpAnim();
                UpdateLandingAnim();
            }
            else
            {
                StateMachine.Update();
            }
        }

        //Checks if a Player is on an object that can be picked up
        private bool IsOnPickup(PickupObj pickup)
        {
            return (IsInAir == false && pickup.IsDead == false && CurHeight == pickup.CurHeight && CollisionBox.Intersects(pickup.CollisionBox) == true);
        }

        protected override void HandleObjPickup()
        {
            //NOTE: TEMPORARY NULL CHECK
            bool canpickup = StateMachine == null ? CanPickUp() : StateMachine.CanPickup();
            for (int i = 0; i < SubLvl.GetItems.Count; i++)
            {
                //Check if the Player is on an item so you can show the Item's HUD
                if (IsOnPickup(SubLvl.GetItems[i]) == true)
                {
                    //Set the Player's InteractionHUD to the Item's HUD if the Player isn't grabbing a fighter
                    //This follows the HUD priority, detailed in comments in PlayerHUD
                    if (HasInteractionHUD == false && GrabbedFighter == null)
                    {
                        SetInteractionHUD(SubLvl.GetItems[i].hud);
                        InteractionHUD.RestartWithDur(1);
                    }
                }

                //Pick up the item if you can
                if (canpickup == true && SubLvl.GetItems[i].CanBePickedUp(this) == true)
                {
                    //NOTE: TEMPORARY NULL CHECK
                    if (StateMachine == null || PressedItem() == true)
                    {
                        Pickup(SubLvl.GetItems[i]);
                        return;
                    }
                }
            }

            for (int i = 0; i < SubLvl.GetWeapons.Count; i++)
            {
                //Check if the Player is on a weapon so you can show the Weapon's HUD
                if (IsOnPickup(SubLvl.GetWeapons[i]) == true)
                {
                    //Set the Player's InteractionHUD to the Item's HUD if the Player isn't grabbing a fighter
                    //This follows the HUD priority, detailed in comments in PlayerHUD
                    if (HasInteractionHUD == false && GrabbedFighter == null)
                    {
                        SetInteractionHUD(SubLvl.GetWeapons[i].hud);
                        InteractionHUD.RestartWithDur(1);
                    }
                }

                //Pick up the weapon if you can
                if (canpickup == true && SubLvl.GetWeapons[i].CanBePickedUp(this) == true)
                {
                    //NOTE: TEMPORARY NULL CHECK
                    if (StateMachine == null || PressedItem() == true)
                    {
                        Pickup(SubLvl.GetWeapons[i]);
                        return;
                    }
                }
            }
        }

        protected void HandleGrabEnemy()
        {
            //Check if we can grab
            if (CanGrabEnem() == true)
            {
                //Go through all the enemies and see if we should grab any
                for (int i = 0; i < SubLvl.GetEnemies.Count; i++)
                {
                    Enemy enem = SubLvl.GetEnemies[i];

                    if (enem.CanGetGrabbed() == true && Location.Z == enem.GetLocationHeight.Z && CollisionBox.Intersects(enem.GrabBox) == true)
                    {
                        //Grab the enemy
                        GrabFighter(enem);

                        //Exit since Players can grab only one Fighter at a time
                        break;
                    }
                }
            }
        }

        protected override void FighterUpdate()
        {
            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine == null)
            {
                //Update the grabbed animation and see if the player shouldn't be grabbed any longer
                UpdateGrabbedAnim();

                //Update the hit animation and see if the player is out of hitstun
                UpdateHitAnim();

                //Update knock down animation to see if the player is back up
                UpdateKnockDownAnim();

                //Let the player use a special attack - can use if in hitstun
                PlayerSpecialAttack();
            }

            //NOTE: TEMPORARY NULL CHECK
            if (StateMachine != null)
            {
                if (StateMachine.CanOffSpec() == true)
                {
                    CheckPlayerOffSpecial();
                }

                if (StateMachine.CanWalk() == true)
                {
                    //If player is attacking, don't let the player move until a little bit after the attack is done
                    if (Main.GetActiveTime >= PrevAttackMove)
                        CheckPlayerMove();
                }

                if (StateMachine.CanAttack() == true)
                {
                    CheckPlayerAttack();
                }

                if (StateMachine.CanJump() == true)
                {
                    CheckPlayerJump();
                }

                if (StateMachine.CanDefSpec() == true)
                {
                    CheckPlayerDefSpecial();
                }

                //Throw weapon; this is probably a temporary spot for this, but we'll see how it works
                if (StateMachine.CanThrowWeapon() == true)
                {
                    if (PressedAttack(true) == true && PressedItem() == true)
                    {
                        weapon.Throw();

                        StateMachine.ChangeState(PlayerStateMachine.State.StabWeapon, StabAnim, null);
                    }
                }
            }
            else
            {
                //Checks if player can pick up items or weapons
                if (Controlled == true)
                    CheckPickUp(Main.GetActiveTime, SubLvl);

                //If player is attacking, don't let the player move until a little bit after the attack is done
                if (Controlled == true && (Main.GetActiveTime - PrevAttackMove) >= 150f && CheckWalking() == true)
                    PlayerMove(Main.GetActiveTime, SubLvl);

                //Make the player attack
                PlayerAttack(Main.GetActiveTime, SubLvl);

                //Make the player jump
                PlayerJump(Main.GetActiveTime, SubLvl);
            }

            #if DEBUG
                //REMOVE AFTERWARDS; DO NOT LEAVE THIS HERE FOR THE RELEASE BUILD
                DebugActions();
            #endif

            //Update the InteractionHUD
            UpdateInteractionHUD();

            //Handle picking up items and weapons in the level
            HandleObjPickup();

            //Handles grabbing Fighters
            HandleGrabEnemy();

            //Update the player's inputs
            KeyboardState = Keyboard.GetState();

            //Update Animations
            UpdateAnimations();

            if (StateMachine == null)
            {
                CheckHolds(Main.GetActiveTime, SubLvl);
            }

            WilBulletChecks(Main.GetActiveTime);

            //Check for giving the Bonus Status if the player should receive it
            if (BonusStatus != null)
                BonusStatus.Update(Main.GetActiveTime);
        }

        //Check if the InteractionHUD is finished and clear it, then check which HUD should show up next based on CheckHUDPriority()
        private void UpdateInteractionHUD()
        {
            //Check if we have an interaction HUD
            if (HasInteractionHUD == true)
            {
                //Check if the interaction HUD is finished
                if (InteractionHUD.IsInactive == true)
                {
                    //Clear the InteractionHUD by setting it to null
                    ClearInteractionHUD();

                    //Determine which HUD gets prioritized based on the Player's state (grabbing enemy, holding weapon, being healed by HoT item, etc.)
                    CheckHUDPriority();
                }
            }
        }

        //Here's part of where we determine "HUD priority," detailed in comments in PlayerHUD
        private void CheckHUDPriority()
        {
            //If the Player is grabbing another Fighter, set the InteractionHUD to the Fighter's HUD
            if (GrabbedFighter != null) SetInteractionHUD(GrabbedFighter.hud);
            //Otherwise if the Player has a Weapon, set the InteractionHUD to the Weapon's HUD
            else if (HasWeapon == true) SetInteractionHUD(weapon.hud);
            //Otherwise if the Player has a HoT item, set the InteractionHUD to the HoT item's HUD
            else if (HasHoTItem == true) SetInteractionHUD(HoTItem.hud);
        }

        //Stuff used to EASILY test things! REMOVE WHEN YOU'RE DONE DEBUGGING!
        protected void DebugActions()
        {
            if (Input.KeyHeld(Keys.LeftControl) == true)
            {
                //Change status
                Status changestatus = null;

                if (Input.CheckKeyPress(KeyboardState, Keys.D1) == true) changestatus = new Status();
                else if (Input.CheckKeyPress(KeyboardState, Keys.D2) == true) changestatus = new Status((int)Status.Statuses.DamageBoost, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D3) == true) changestatus = new Status((int)Status.Statuses.DamageDown, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D4) == true) changestatus = new Status((int)Status.Statuses.DefenseBoost, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D5) == true) changestatus = new Status((int)Status.Statuses.DefenseDown, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D6) == true) changestatus = new Status((int)Status.Statuses.SpeedBoost, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D7) == true) changestatus = new Status((int)Status.Statuses.SpeedDown, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D8) == true) changestatus = new Status((int)Status.Statuses.StunDown, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D9) == true) changestatus = new Status((int)Status.Statuses.Poison, 10000f, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D0) == true) changestatus = new Status((int)Status.Statuses.Invincible, 10000f, 0);

                if (changestatus != null) 
                    SetStats(null, null, null, null, null, changestatus);

                //Teleport (Temporary, brush up later!)
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    SetLocation(new Vector3(Mouse.GetState().X, Mouse.GetState().Y, Location.Z));

            }
            //Change costumes
            if (Input.KeyHeld(Keys.LeftShift) == true)
            {
                if (Input.CheckKeyPress(KeyboardState, Keys.D1) == true) ChangeCostume(0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D2) == true) ChangeCostume(1);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D3) == true) ChangeCostume(2);
                else if (Input.CheckKeyPress(KeyboardState, Keys.D4) == true) ChangeCostume(3);

                //Make the player rise
                if (Input.KeyHeld(Keys.A) == true)
                {
                    if (StartedFalling == true) 
                        BeginFall();

                    Location.Z += 2;
                    AirVelocity.Z = 0f;
                }

                //Knock down player (helps test enemy Idle states)
                if (Input.CheckKeyPress(KeyboardState, Keys.K) == true)
                {
                    //If D is held, kill the player
                    if (Input.KeyHeld(Keys.D) == true)
                        Die();
                    else KnockDown(KnockedDownAnim);
                }

                //Grab the player
                if (Input.CheckKeyPress(KeyboardState, Keys.G) == true)
                {
                    GetGrabbed(this);
                }

                //Hurt the player
                if (Input.CheckKeyPress(KeyboardState, Keys.H) == true)
                {
                    //Hurt the player
                    TakeDamage(this, 60, Hitbox.Empty);
                }

                //Kill all enemies on screen
                if (Input.CheckKeyPress(KeyboardState, Keys.OemComma) == true)
                {
                    for (int i = 0; i < SubLvl.GetEnemies.Count; i++)
                    {
                        SubLvl.GetEnemies[i].Die();
                    }
                }
            }
        }

        protected override void FighterDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            //Don't draw the character if the character is invisible when invincible
            if (status.GStatusColor.A != 0)
            {
                //NOTE: TEMPORARY NULL CHECK!
                if (StateMachine == null)
                {
                    //Draw the status color if the player isn't invincible from a move, otherwise draw the move invincibility color
                    DrawAnimations(spriteBatch, cameraloc, ObjectTile.GetObjectDepthInTile(CurHeight), DrawColor, GetDrawDepth(cameraloc));
                }
                else StateMachine.Draw(spriteBatch, GetDrawLoc(cameraloc), SpriteSheet, FacingRight, DrawColor, GetDrawDepth(cameraloc));

                //If the player has the NoJump, NoAttack, NoGrab, or NoSpecial status, draw the NoStatus symbol above the player's head
                status.DrawNoStatusSymbol(spriteBatch, new Vector2(Location.X + cameraloc.X - (Hurtbox.Width / 2), Location.Y - Location.Z + cameraloc.Y - (int)(ObjectHeight * 1.3f)), GetDrawDepth(cameraloc) + .001f);
            }

            //Draw the player's Bonus Status
            //if (BonusStatus != null)
            //{
            //    //If the Bonus Status is deactivated because of an infinite status, show it, otherwise draw the meter normally
            //    /*if (BonusStatus.Activated == false) hud.DrawBonusStatus(spriteBatch, false);
            //    else*/ BonusStatus.Draw(spriteBatch);
            //}
            
            //Draw the Player's HUD
            hud.Draw(spriteBatch);

            //Draw the HUD of the object the Player recently interacted with
            if (HasInteractionHUD == true)
                InteractionHUD.DrawInteractionHUD(spriteBatch, hud.GetPosition + new Vector2(0, PlayerHUD.PlayerHUDDiff));
        }

        //Draws symbols, such as a !, ?, or more above the character's head to show something significant (Ex. train approaching tunnel in Level 3-4)
        public void DrawExpression(SpriteBatch spriteBatch, Vector2 OffSet, int expression)
        {
            //! expression
            if (expression == 0)
            {
                Texture2D graphic = LoadGraphics.EnemDesignated;
                Vector2 origin = new Vector2(-graphic.Width / 2, graphic.Height / 2);

                spriteBatch.Draw(graphic, new Vector2(Location.X + OffSet.X - (Hurtbox.Width / 2), Location.Y - Location.Z + OffSet.Y - (int)(ObjectHeight * 1.3f)), null, Color.White, 0f, origin, 1f, SpriteEffects.None, GetDrawDepth(OffSet) + .002f);
            }
        }
    }
}