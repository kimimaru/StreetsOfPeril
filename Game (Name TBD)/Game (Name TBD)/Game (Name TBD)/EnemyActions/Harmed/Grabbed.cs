﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The Grabbed state an enemy goes in after getting grabbed
    public sealed class Grabbed : Action
    {
        public Grabbed(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Grabbed, finishedstate, interruptstate, unfinishedstate)
        {
            
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(activeTime);
            Enemy.gGrabbox.Update(activeTime);
            //End the action if the enemy is no longer grabbed
            if (Enemy.gGrabbox.GSIsGrabbed == false)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
