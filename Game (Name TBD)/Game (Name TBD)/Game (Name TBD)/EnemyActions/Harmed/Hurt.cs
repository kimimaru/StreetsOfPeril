﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The Hurt state an enemy goes in after taking damage from a Hitbox that doesn't knock down
    public sealed class Hurt : Action
    {
        public Hurt(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Hurt, finishedstate, interruptstate, unfinishedstate)
        {
            
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(activeTime);

            //End the action if the enemy is no longer in Hitstun
            if (Enemy.IsInHitstun == false)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
