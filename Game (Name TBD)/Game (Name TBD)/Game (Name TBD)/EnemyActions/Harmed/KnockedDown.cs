﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The KnockedDown state an enemy goes in after taking damage from a Hitbox that knocks down
    public sealed class KnockedDown : Action
    {
        public KnockedDown(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.KnockedDown, finishedstate, interruptstate, unfinishedstate)
        {
            
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //If the enemy is on the second frame of the knock down animation (thus on the ground), start making the enemy recover
            if (NewAnim.CurrentFrame != 0)
            {
                //If the enemy is on the ground, update the knock down animation; otherwise, if the enemy got into the air again, start the animation over
                if (Enemy.IsOnGround == true)
                {
                    Enemy.EnemySlide();
                    NewAnim.Update(Main.GetActiveTime, Enemy.status == (int)Status.Statuses.StunDown ? .4f : .2f);
                }
                else
                {
                    NewAnim.Reset();
                    Enemy.ChangeAirVelocity(new Vector3(Enemy.ForwardVal(-Enemy.GetSlideSpeed), Enemy.GetAirVelocity.Y, Enemy.GetAirVelocity.Z));
                }

                //Check if the enemy is dead, on the floor, and not invincible yet
                if (Enemy.IsDead == true && Enemy.status != (int)Status.Statuses.Invincible)
                {
                    Enemy.InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));
                    LoadSounds.Play(LoadSounds.EDeath);
                }

                //If the enemy's jump velocity is 0, the enemy is on the floor; once that's true, check if the animation is done or if the enemy is dead, which ends the animation earlier
                if (Enemy.GetAirVelocity.Z == 0f && (NewAnim.IsAnimationEnd() == true || Enemy.IsDead == true && NewAnim.CurrentFrame > 1))
                {
                    //Recover from a knock down
                    Enemy.KnockDownRecover();

                    Enemy.gsAction.EndAction(Main.GetActiveTime);

                    //Make the enemy stay on the last frame of the animation
                    //NOTE: This will be fixed (and this code will be unnecessary) when we keep completed animations on their last frames
                    NewAnim.SkipToFrame(NewAnim.MaxFrame, false);

                    //If the enemy is dead, disable its AI and Draw states
                    if (Enemy.IsDead == true)
                    {
                        Enemy.SetAIEnabled(false);
                        Enemy.SetDrawing(false);
                    }
                }
            }
            else
            {
                Enemy.ObjectMove(Enemy.GetAirVelocityVec2);

                //Check for the Diver falling back to its original height
                //Enemy.DiverHeightCheck();
            }
        }
    }
}
