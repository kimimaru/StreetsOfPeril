﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for enemies to approach the target to prepare for an attack
    public class Approach : Action
    {
        private Vector2 DistanceStop;
        private bool StopFar;

        //Uses the enemy's walk animation
        public Approach(Enemy enem, Vector2 distancestop, bool stopfar, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            DistanceStop = distancestop;
            StopFar = stopfar;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Vector2 distancestop = new Vector2(DistanceStop.X <= 0f ? Enemy.TrueVelocity.X : DistanceStop.X, DistanceStop.Y <= 0f ? Enemy.TrueVelocity.Y : DistanceStop.Y);
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
            Enemy.FaceTarget();

            //Move towards the player
            if (Enemy.TargetProximityX() > distancestop.X)
            {
                ConditionMove(new Vector2(-Enemy.TrueVelocity.X, 0f), Enemy.TargetBehind() == true, Enemy.TargetInFront() == true, level);
            }

            if (Enemy.TargetProximityY() > distancestop.Y)
            {
                ConditionMove(new Vector2(0f, -Enemy.TrueVelocity.Y), Enemy.TargetAbove() == true, Enemy.TargetBelow() == true, level);
            }

            //If the enemy is close, stop and go into the Offense state
            if (Enemy.TargetProximityX() <= distancestop.X && Enemy.TargetProximityY() <= distancestop.Y)
            {
                Enemy.gsAction.EndAction(activeTime);
            }
            //Otherwise stop and go back into Movement state
            else if (StopFar == true && (Enemy.TargetProximityX() > 90 || Enemy.TargetProximityY() > 90))
            {
                Enemy.gsAction.EndAction(activeTime, true);
            }
        }
    }
}
