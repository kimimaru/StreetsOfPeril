﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for an enemy to retreat from the player but stay within a certain range away
    public class Retreat : Action
    {
        private float RetreatDuration;
        private float PrevDuration;

        private int DistanceAway;

        public Retreat(Enemy enem, float retreatduration, int distanceaway, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            RetreatDuration = retreatduration;
            PrevDuration = Main.GetActiveTime;

            DistanceAway = distanceaway;
        }

        private Rectangle RectAway
        {
            get { return new Rectangle((int)Enemy.gTarget.GetLocationVec2.X - DistanceAway, (int)Enemy.gTarget.GetLocationVec2.Y - 25, DistanceAway * 2, 50); }
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Make the enemy face the target
            Enemy.FaceTarget();

            //Move away from the target in the X direction
            if (Enemy.gTarget.GetLocationVec2.X < Enemy.GetLocationVec2.X)
            {
                //Keep the enemy inside the rectangle around the target
                //If the enemy is outside the rectangle, move back in
                ConditionMove(new Vector2(Enemy.TrueVelocity.X, 0f), (Enemy.GetLocationVec2.X + Enemy.TrueVelocity.X) < RectAway.Right, (Enemy.GetLocationVec2.X - Enemy.TrueVelocity.X) > RectAway.Right, level);
            }
            else if (Enemy.gTarget.GetLocationVec2.X > Enemy.GetLocationVec2.X)
            {
                //Keep the enemy inside the rectangle around the target
                //If the enemy is outside the rectangle, move back in
                ConditionMove(new Vector2(-Enemy.TrueVelocity.X, 0f), (Enemy.GetLocationVec2.X - Enemy.TrueVelocity.X) > RectAway.X, (Enemy.GetLocationVec2.X + Enemy.TrueVelocity.X) < RectAway.X, level);
            }

            //Move away from the target in the Y direction
            if (Enemy.gTarget.GetLocationVec2.Y < Enemy.GetLocationVec2.Y)
            {
                //Keep the enemy inside the rectangle around the target
                //If the enemy is outside the rectangle, move back in
                ConditionMove(new Vector2(0f, Enemy.TrueVelocity.Y), (Enemy.GetLocationVec2.Y + Enemy.TrueVelocity.Y) < RectAway.Bottom, (Enemy.GetLocationVec2.Y - Enemy.TrueVelocity.Y) > RectAway.Bottom, level);
            }
            else if (Enemy.gTarget.GetLocationVec2.Y > Enemy.GetLocationVec2.Y)
            {
                //Keep the enemy inside the rectangle around the target
                //If the enemy is outside the rectangle, move back in
                ConditionMove(new Vector2(0f, -Enemy.TrueVelocity.Y), (Enemy.GetLocationVec2.Y - Enemy.TrueVelocity.Y) > RectAway.Y, (Enemy.GetLocationVec2.Y + Enemy.TrueVelocity.Y) < RectAway.Y, level);
            }

            //After the retreat duration, end the action
            if ((activeTime - PrevDuration) >= RetreatDuration)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
