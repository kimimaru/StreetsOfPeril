﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //An enemy behavior to move around the player in a circle and choose a point
    public class CircleAround : Action
    {
        //How far to stay around the target
        private Vector2 DistanceAround;

        //Tells the enemy which direction to go in
        private bool? Left;
        private bool? Up;

        //Tells the enemy how to rotate around the perimeter of the rectangle
        private bool Clockwise;

        //The point to stop at on the rectangle; it can be the corners or the left and right side halfway down the height
        private int EndPoint;

        public CircleAround(Enemy enem, Vector2 distancearound, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            DistanceAround = distancearound;
            EndPoint = 0;
            Clockwise = true;

            //Find out where the enemy should be moving along the rectangle; start out going left
            Left = Math.Abs(Enemy.GetLocationVec2.X - RectPlayer.X) < Math.Abs(Enemy.GetLocationVec2.X - RectPlayer.Right);
            Up = null;//Math.Abs(Enemy.XYLoc.Y - RectPlayer.Y) < Math.Abs(Enemy.XYLoc.Y - RectPlayer.Bottom);

            //Choose a random endpoint
            EndPoint = new Random().Next(0, GetEndPoints.Length);

            //Choose a random direction to rotate around the player
            Clockwise = new Random().Next(0, 2) == 0;
        }

        //The rectangle around the player to border
        private Rectangle RectPlayer
        {
            get { return new Rectangle((int)(Enemy.gTarget.GetLocationVec2.X - DistanceAround.X), (int)(Enemy.gTarget.GetLocationVec2.Y - DistanceAround.Y), (int)DistanceAround.X * 2, (int)DistanceAround.Y * 2); }
        }

        private Vector2[] GetEndPoints
        {
            get
            {
                return (new Vector2[] { new Vector2(RectPlayer.X, RectPlayer.Y), new Vector2(RectPlayer.Right, RectPlayer.Y), new Vector2(RectPlayer.Right, RectPlayer.Bottom),
                    new Vector2(RectPlayer.X, RectPlayer.Bottom), new Vector2(RectPlayer.X, RectPlayer.Bottom - ((RectPlayer.Bottom - RectPlayer.Y) / 2)), new Vector2(RectPlayer.Right, RectPlayer.Bottom - ((RectPlayer.Bottom - RectPlayer.Y) / 2)) });
            }
        }

        //Walk around the perimeter of the rectangle surrounding the player
        public override void Update(float activeTime, SubLevel level)
        {
            //If the enemy is too far from the target, stop
            if (Enemy.TargetProximity() > 150)
            {
                Enemy.gsAction.ActionDone = true;
                return;
            }

            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
            Enemy.FaceTarget();

            //Walk upwards if you're on the left side of the rectangle
            if (Up == true)
            {
                if ((Enemy.GetLocationVec2.Y - Enemy.TrueVelocity.Y) >= RectPlayer.Y)
                {
                    Enemy.ObjectMove(new Vector2(0f, -Enemy.TrueVelocity.Y));
                }
                else
                {
                    Left = !Clockwise;
                    Up = null;
                }
            }
            else if (Up != null)
            {
                if ((Enemy.GetLocationVec2.Y + Enemy.TrueVelocity.Y) <= RectPlayer.Bottom)
                {
                    Enemy.ObjectMove(new Vector2(0f, Enemy.TrueVelocity.Y));
                }
                else
                {
                    Left = Clockwise;
                    Up = null;
                }
            }

            //Walk upwards if you're on the left side of the rectangle
            if (Left == true)
            {
                if ((Enemy.GetLocationVec2.X - Enemy.TrueVelocity.X) >= RectPlayer.X)
                {
                    Enemy.ObjectMove(new Vector2(-Enemy.TrueVelocity.X, 0f));
                }
                else
                {
                    Up = Clockwise;
                    Left = null;
                }
            }
            else if (Left != null)
            {
                if ((Enemy.GetLocationVec2.X + Enemy.TrueVelocity.X) <= RectPlayer.Right)
                {
                    Enemy.ObjectMove(new Vector2(Enemy.TrueVelocity.X, 0f));
                }
                else
                {
                    Up = !Clockwise;
                    Left = null;
                }
            }

            //Move the enemy
            if (Enemy.GetLocationVec2.X < RectPlayer.X) Enemy.ObjectMove(new Vector2(Enemy.TrueVelocity.X, 0f));
            else if (Enemy.GetLocationVec2.X > RectPlayer.Right) Enemy.ObjectMove(new Vector2(-Enemy.TrueVelocity.X, 0f));

            if (Enemy.GetLocationVec2.Y < RectPlayer.Y) Enemy.ObjectMove(new Vector2(0f, Enemy.TrueVelocity.Y));
            else if (Enemy.GetLocationVec2.Y > RectPlayer.Bottom) Enemy.ObjectMove(new Vector2(0f, -Enemy.TrueVelocity.Y));

            //If the enemy reached the endpoint, stop
            if (Math.Abs(Enemy.GetLocationVec2.X - GetEndPoints[EndPoint].X) <= Enemy.TrueVelocity.X && Math.Abs(Enemy.GetLocationVec2.Y - GetEndPoints[EndPoint].Y) <= Enemy.TrueVelocity.Y)
            {
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
