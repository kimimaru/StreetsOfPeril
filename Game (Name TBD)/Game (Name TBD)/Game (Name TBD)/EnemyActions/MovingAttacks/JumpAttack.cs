﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //A generic jump attack that an enemy can perform
    public class JumpAttack : Action
    {
        //This can also function as a normal jump by passing in null as the hitbox
        public JumpAttack(Enemy enem, NewAnimation anim, Vector3 jumpspeed, Hitbox hitbox, SoundEffect attacksound, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.AddHitbox(hitbox);

            Enemy.Jump(jumpspeed.X, jumpspeed.Y, jumpspeed.Z);
            LoadSounds.Play(attacksound);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Enemy.ObjectMove(new Vector2(Enemy.GetAirVelocity.X, 0));
            if (NewAnim != null) NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Once the enemy hits the ground, the action is completed
            if (Enemy.IsOnGround == true)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
