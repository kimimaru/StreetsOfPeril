﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Enemy behavior in which the enemy performs a slide attack
    public class Slide : Action
    {
        private float SlideSpeed;
        private float EnemyOrigHeight;

        //Tells if the slide is a forwards or backwards slide
        private bool ForwardSlide;

        public Slide(Enemy enem, NewAnimation anim, bool forward, float slidespeed, Hitbox hitbox, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.MovingAttack, finishedstate, interruptstate, unfinishedstate)
        {
            SlideSpeed = slidespeed;
            EnemyOrigHeight = Enemy.GetLocationHeight.W;
            ForwardSlide = forward;

            //Cut the height of the enemy in half because the enemy is on the floor sliding
            //Enemy.GetLocationHeight = new Vector4(Enemy.GetLocationVec2, Enemy.CurHeight, Enemy.GetLocationHeight.W / 2);

            //Set the hitbox for the slide; set a default hitbox if the one passed in is null, otherwise use the one passed in
            if (hitbox == null)
                Enemy.CreateHitboxFeet(true, 40, 25, 5, 25, 100, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, Enemy.TowardsDir, LoadSounds.Kick, true);
            else Enemy.AddHitbox(hitbox);
            
            LoadSounds.Play(LoadSounds.Sweep);
        }

        public override void Reset()
        {
            Enemy.SetLocation(new Vector3((int)Enemy.GetLocation.X, Enemy.GetLocation.Y, Enemy.CurHeight));
        }

        public override void Update(float activeTime, SubLevel level)
        {
            if (NewAnim != null) NewAnim.Update(activeTime, Enemy.GetGravityValue);

            //Make the enemy go backwards if the slide is backwards
            float slidespeed = SlideSpeed;
            if (ForwardSlide == false) slidespeed = -slidespeed;

            Enemy.ObjectMove(new Vector2(Enemy.ForwardVal(slidespeed), 0));

            //Slow the slide due to friction
            SlideSpeed -= .1f;

            //If the slide stopped or the animation finished, end the action
            if (SlideSpeed <= 0f || (NewAnim != null && NewAnim.IsAnimationEnd() == true))
            {
                Enemy.gsAction.EndAction(activeTime);
                Reset();
                Enemy.ClearHitboxes();
            }
        }
    }
}
