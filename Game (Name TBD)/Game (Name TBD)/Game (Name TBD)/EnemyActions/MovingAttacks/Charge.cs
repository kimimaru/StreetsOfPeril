﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A general charge attack that enemies can do; enemies run faster in a straight line - if they hit the player, it knocks the player down
    public class Charge : Action
    {
        private float Direction;

        public Charge(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.MovingAttack, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.FaceTarget();
            Direction = Enemy.ForwardVal(1);

            //Have a constant hitbox out while charging
            Enemy.CreateHitboxFeet(true, 15, 20, 5, Enemy.ObjectHeight, 0, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, Enemy.TowardsDir, LoadSounds.Kick, true);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            if (NewAnim != null) NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Run horizontally towards the player with increased speed
            Enemy.ObjectMove(new Vector2((Enemy.TrueVelocity.X + 2f) * Direction, 0f));

            //Check if the enemy is where the player is (or passed the player) and end the charge if necessary
            if ((Enemy.GetLocationHeight.X * -Direction) <= (Enemy.gTarget.GetLocationHeight.X * -Direction))
            {
                Enemy.gsAction.EndAction(activeTime);
                Enemy.ClearHitboxes();
            }
        }
    }
}
