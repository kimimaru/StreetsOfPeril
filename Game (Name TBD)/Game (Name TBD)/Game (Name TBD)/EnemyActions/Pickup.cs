﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A behavior for picking up things off the ground, like items and weapons
    public class Pickup : Action
    {
        public Pickup(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Utility, finishedstate, interruptstate, unfinishedstate)
        {
            
        }
    }
}
