﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The Weapon Wielders' Uppercut Swipe move performed after a roll; Gerald/Kate holds his/her weapon and jumps straight up, slashing on the way
    public class UppercutSwipe : Action
    {
        public UppercutSwipe(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.Jump(0f, 0f, Enemy.GetOrigJumpVelocity.Z);

            Enemy.weapon.CreateHitboxFeet(true, Enemy.weapon.GetRange, 20, Enemy.weapon.Damage, Enemy.ObjectHeight / 2, 100, 200, Hitbox.HitboxTypes.KnockDown, Enemy.FacingRight, Enemy.weapon.GWeaponHitSound, true);

            LoadSounds.Play(LoadSounds.JumpKick);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Enemy.ObjectMove(Vector2.Zero);

            if (Enemy.IsOnGround == true)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
