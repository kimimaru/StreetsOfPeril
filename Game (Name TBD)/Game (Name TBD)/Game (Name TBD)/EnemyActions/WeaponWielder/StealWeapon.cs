﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Gerald or Kate (Weapon Wielders) to steal a weapon from the player
    public class StealWeapon : Action
    {
        public StealWeapon(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Utility, finishedstate, interruptstate, unfinishedstate)
        {
            
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime);
            if (NewAnim.IsAnimationEnd() == true)
            {
                //Steal the weapon from the player
                Weapon.EnemyStealWeapon(Enemy.gTarget as Player, Enemy, level);

                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
