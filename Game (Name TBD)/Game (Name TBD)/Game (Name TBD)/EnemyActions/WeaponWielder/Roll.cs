﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A quick roll that the Weapon Wielders (Gerald/Kate) do before performing the Uppercut Swipe move to attack the player; they roll to the other side of the player, and it grants them move invincibility
    public class Roll : Action
    {
        private Vector2 Destination;
        private int Direction;

        public Roll(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.DefSpecial, finishedstate, interruptstate, unfinishedstate)
        {
            HasInvincibility = true;

            Destination = new Vector2(Enemy.gTarget.FighterCenter.X + (Enemy.TargetInFront() == true ? 40 : -40), Enemy.GetLocationVec2.Y);
            Direction = (Destination.X < Enemy.GetLocationVec2.X) ? -1 : 1;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Update the rolling animation
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Move to the other side of the target
            Enemy.ObjectMove(new Vector2((Enemy.TrueVelocity.X + 2) * Direction, 0));

            //Check if the Weapon Wielder reached its destination or can't move further and finish the action if so
            if ((Direction < 0 && Enemy.GetLocationVec2.X <= Destination.X) || (Direction > 0 && Enemy.GetLocationVec2.X >= Destination.X))
            {
                //Face the player after the roll
                Enemy.FaceTarget();

                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
