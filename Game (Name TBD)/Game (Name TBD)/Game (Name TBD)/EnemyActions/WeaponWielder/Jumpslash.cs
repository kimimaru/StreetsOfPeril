﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The Weapon Wielders' (Gerald/Kate) Downwards Jump Slash
    //The hitbox starts high and goes lower and forwards as Gerald/Kate moves
    public class Jumpslash : Action
    {
        private int XMovedAmount;
        private float ZMovedAmount;

        //The jumpslash hitbox; used for moving it during the animation
        private Hitbox JumpslashHitbox;

        public Jumpslash(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.Jump(Enemy.ForwardVal(Enemy.TrueVelocity.X), 0f, Enemy.GetOrigJumpVelocity.Z);

            //The hitbox starts above Gerald/Kate's head, just like what Link does in the Zelda series
            //Enemy.GetWeapon.SetHitbox(new Vector2(Enemy.GetHitboxXValue(Enemy.GetWeapon.GetRange), Enemy.FeetLoc.Y - 10), Enemy.MaxHeight, /*Anim.GetFrameLength(0)*/100, Hitbox.InfiniteHitbox, false, null, true);
            JumpslashHitbox = Enemy.weapon.CreateHitboxFeet(true, Enemy.weapon.GetRange, 0, (int)Enemy.weapon.Damage, (int)Enemy.ObjectHeight, 100f, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, false, Enemy.weapon.GWeaponHitSound, true);

            XMovedAmount = 0;
            ZMovedAmount = 0f;
        }

        //The max amount the hitbox moves in the X direction (this happens because the Weapon Wielder slashes down and forwards)
        private int MaxHitboxXMove
        {
            get { return 20; }
        }

        //The max amount of height the hitbox decreases by (the hitbox gets lower because the Weapon Wielder slashes downwards); the hitbox starts above the enemy's head, so it should travel the distance from its head to its feet
        private float MaxHitboxZMove
        {
            get { return Enemy.GetLocationHeight.W; }
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //If the enemy isn't on the ground, move it and the weapon hitbox
            if (Enemy.IsOnGround == false)
            {
                Enemy.ObjectMove(new Vector2(Enemy.GetAirVelocity.X, 0));

                //The hitbox starts at the top and moves down and forwards until it eventually ends up held out in front of the Weapon Wielder on the floor
                //NOTE: This will change once the final animation is in place! The X position and height of the hitbox will simply be decreased after each frame to match the position of the sword in the animation!
                if (JumpslashHitbox.IsActive(Enemy.IsInWater) == true)
                {
                    if (XMovedAmount < MaxHitboxXMove)
                    {
                        Enemy.weapon.HitboxFollow(Enemy.ForwardVal(1), 0, 0);
                        XMovedAmount += 1;
                    }
                    //The X and Z movements have been separated to be easier to read
                    if (ZMovedAmount < MaxHitboxZMove)
                    {
                        //See how much the hitbox is supposed to move each frame, excluding the startup of the animation (the first frame of the animation doesn't have a hitbox)
                        //float amount = MaxHitboxZMove / Movement.TotalAirTime(Enemy.gOrigJumpVelocity.Y, Enemy.gJumpVelocityChange);

                        Enemy.weapon.HitboxFollow(0, 0, -3f);
                        ZMovedAmount += 3f;
                    }
                }

                //Keep Gerald/Kate on the last frame of the animation; the last frame is the endlag of the move, which occurs after Gerald/Kate touches the floor
                if (NewAnim.CurrentFrame != NewAnim.MaxFrame)
                {
                    //int frame = Anim.CurrentFrame;

                    NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

                    //Change the X position and height of the hitbox when a new frame is reached
                    //if (frame != Anim.CurrentFrame)
                    //{
                    //    //Enemy.gsWeapon.GHitbox.Follow(, 0, );
                    //}
                }

                //If the enemy landed, reset the jump animation and skip to the end if it isn't there already; this avoids it showing the enemy swinging the sword down in a jumping motion when it is already on the floor
                //Other games do this as well, like in The Legend of Zelda: Ocarina of Time when Link jumpslashes onto a steep hill
                if (Enemy.IsOnGround == true)
                {
                    NewAnim.SkipToFrame(NewAnim.MaxFrame, true);
                }
            }
            //Don't update the last frame of the animation until Gerald/Kate are on the floor; this is so they retain their lag at the end even if they jump from high up
            else
            {
                NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
                if (NewAnim.IsAnimationEnd() == true)
                {
                    Enemy.gsAction.EndAction(activeTime);
                }
            }
        }
    }
}
