﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Rogue to sneak out after shooting at the player
    public class SneakOut : Action
    {
        public SneakOut(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            //Move back towards the edge that Rogue just came from
        }

        //Reset the Rogue's target so he can choose a new one to attack
        public override void Reset()
        {
            Enemy.gsAction.TargetSet = false;
            (Enemy as Rogue).SetSneakTimer();
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Sneak out
            Enemy.PureMove(Enemy.ForwardVal(-2), 0);

            //When back at the edge of the screen, end the action
            if (Enemy.GetLocationHeight.X + level.GCameraOffSet.X <= -30 || Enemy.GetLocationHeight.X + level.GCameraOffSet.X >= Main.ScreenSize.X + 10)
            {
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
