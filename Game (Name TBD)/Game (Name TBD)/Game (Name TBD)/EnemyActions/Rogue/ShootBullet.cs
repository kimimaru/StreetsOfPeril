﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Rogue to shoot a bullet
    public class ShootBullet : Action
    {
        public ShootBullet(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Projectile, finishedstate, interruptstate, unfinishedstate)
        {

        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Make Rogue face the target just in case he's not
            Enemy.FaceTarget();

            Enemy.ShootProjectile(new Projectile(Enemy, LoadGraphics.projectile, new Vector3(Enemy.ForwardVal(5), 0, 0), new Vector3(Enemy.ForwardVal(Enemy.GetLocationVec2.X + Enemy.CollisionBox.Width, Enemy.GetLocationVec2.X - 10), Enemy.FeetLoc.Y, Enemy.CurHeight + 45f), new Status(), 6, true));
            Enemy.gsAction.EndAction(activeTime);
        }
    }
}
