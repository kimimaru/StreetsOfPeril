﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
namespace Game__Name_TBD_
{
    //Behavior for Rogue to sneak in to before shooting at the player
    public class SneakIn : Action
    {
        public SneakIn(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            //Choose a direction to sneak in from based on which side of the screen Rogue is on
        }

        public override void Reset()
        {
            (Enemy as Rogue).SetSneakTimer();
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Sneak in
            Enemy.FaceTarget();
            Enemy.PureMove(Enemy.ForwardVal(2), 0);

            //When you're a bit out of range (MAKE THIS VALUE A CONSTANT AS WELL AS THE SCREEN EDGES) from the edge of the screen, end the action
            if ((Enemy.GetLocationHeight.X + level.GCameraOffSet.X >= 10 && Enemy.GetLocationHeight.X + level.GCameraOffSet.X <= 15) || (Enemy.GetLocationHeight.X + level.GCameraOffSet.X >= 387 && Enemy.GetLocationHeight.X + level.GCameraOffSet.X <= 390))
            {
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
