﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Makes Rogue run to the nearest side of the screen
    public class RunTo : Action
    {
        public RunTo(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetWalkAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            //Decide which way to run
            //NOTE: Change this after all the animations have been confirmed to work fine and there are no glitches
        }

        public override void Reset()
        {
            //If Rogue finished the action and got to the side of the screen, reset the sneak timer so he doesn't match the target's Y value too quickly after finishing
            if (Enemy.gsAction.ActionDone == true)
                (Enemy as Rogue).SetSneakTimer();
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Make Rogue run to the nearest side
            if (Enemy.GetDrawLoc(level.GCameraOffSet).X >= Main.ScreenHalf.X)
            {
                Enemy.ObjectMove(new Vector2(Enemy.TrueVelocity.X + 1f, 0));
                Enemy.ChangeDirection(true);
            }
            else
            {
                Enemy.ObjectMove(new Vector2(-Enemy.TrueVelocity.X - 1f, 0));
                Enemy.ChangeDirection(false);
            }

            float drawlocX = Enemy.GetDrawLoc(level.GCameraOffSet).X;

            //When Rogue is on either side of the screen, end the action
            if (drawlocX <= -10 || drawlocX >= (Main.ScreenSize.X - 10))
            {
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
