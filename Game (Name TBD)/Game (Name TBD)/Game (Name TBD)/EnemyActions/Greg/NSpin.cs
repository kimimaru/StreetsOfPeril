﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Greg's Nunchuck Spin attack; Greg spins his nunchucks around him - he uses it when surrounded by 2 or more players or when close to his target
    public class NSpin : Action
    {
        private int FrameNum;

        public NSpin(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Attack, finishedstate, interruptstate, unfinishedstate)
        {
            FrameNum = 0;
            CreateHitbox();
        }

        //Creates a hitbox for Greg depending on the frame of the animation he's at
        private void CreateHitbox()
        {
            Vector2 HitboxLoc = Vector2.Zero;

            int hitboxadd = (Enemy.CollisionBox.Width / 2);

            //Set the location of the hitbox depending on the current frame of animation (Ex. If Greg is on the part of the animation where he's facing up, make a hitbox a bit further up)
            switch (FrameNum)
            {
                case 0: HitboxLoc = new Vector2(Enemy.FacingRight == true ? Enemy.CollisionBox.X + hitboxadd - 30 : Enemy.CollisionBox.X + hitboxadd, Enemy.FeetLoc.Y - 15);
                    break;
                case 1: HitboxLoc = new Vector2(Enemy.CollisionBox.X - 15, Enemy.CollisionBox.Y - 30);
                    break;
                case 2: HitboxLoc = new Vector2(Enemy.FacingRight == false ? Enemy.CollisionBox.X + hitboxadd - 30 : Enemy.CollisionBox.X + hitboxadd, Enemy.FeetLoc.Y - 15);
                    break;
                case 3: HitboxLoc = new Vector2(Enemy.CollisionBox.X - 15, Enemy.CollisionBox.Y + Enemy.CollisionBox.Height);
                    break;
            }

            Enemy.AddHitbox(new Hitbox(new Rectangle((int)HitboxLoc.X, (int)HitboxLoc.Y, 30, 30), 3, Enemy.CurHeight, 15, 0, NewAnim.GetCurFrameLength(), Hitbox.HitboxTypes.KnockDown, FrameNum == 1 ? !Enemy.FacingRight : Enemy.FacingRight, LoadSounds.Kick));

            FrameNum = NewAnim.CurrentFrame;
        }

        //This move has 4 (may be 8 for diagonals) different hitboxes in different spots
        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            if (NewAnim.IsAnimationEnd() == true)
            {
                Enemy.gsAction.EndAction(activeTime);
            }
            else if (FrameNum != NewAnim.CurrentFrame)
            {
                CreateHitbox();
            }
        }
    }
}
