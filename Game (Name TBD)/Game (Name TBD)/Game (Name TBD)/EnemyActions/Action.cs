﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The base action class - contains a reference to the enemy so the action can easily access/change all the information it needs
    public abstract class Action
    {
        public enum ActionType
        {
            None, Idle, Movement, Jumping, Attack, MovingAttack, MovingGrab, Grab, GrabAttack, Defend, Throw, DefSpecial, OffSpecial, Grabbed, Hurt, KnockedDown, Projectile, Transform, Utility, Instruct
        };

        //The reference to the enemy
        protected Enemy Enemy;

        //The animation that corresponds with the action; this is updated and drawn by the action itself
        protected NewAnimation NewAnim;

        //The type of action this is
        protected ActionType TypeAction;

        //Tells if the action has Move Invincibility (Ex. changing forms, Defensive Specials, etc.)
        protected bool HasInvincibility;

        //The behavior states (Movement, Offense, Defense, Preset) to go into if an action is finished, interrupted (getting hurt, etc.), or unfinished (player got too far away, etc.)
        //A value of null means to stay on the current behavior
        protected int? FinishedState;
        protected int? InterruptState;
        protected int? UnfinishedState;

        //Constructor
        public Action()
        {
            TypeAction = ActionType.None;

            //Very few actions have invincibility, so this is set to false by default
            HasInvincibility = false;

            FinishedState = null;
            InterruptState = null;
            UnfinishedState = null;
        }

        public Action(Enemy enem, NewAnimation anim, ActionType typeaction, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : this()
        {
            Enemy = enem;
            NewAnim = anim;
            if (NewAnim != null) NewAnim.Reset();

            TypeAction = typeaction;

            FinishedState = finishedstate;
            InterruptState = interruptstate;
            UnfinishedState = unfinishedstate;
        }

        public int? FinishBehavior
        {
            get { return FinishedState; }
        }

        public int? InterruptBehavior
        {
            get { return InterruptState; }
        }

        public int? UnfinishBehavior
        {
            get { return UnfinishedState; }
        }

        //NOTE: Polish since an enemy's action persists even after they've been hit or knocked down; also test rigorously later once the new animations are complete
        public bool GrantsInvincibility
        {
            get { return (HasInvincibility == true && Enemy.gsAction.IsActionDone() == false); }
        }

        public NewAnimation Animation
        {
            get { return NewAnim; }
        }

        public ActionType GetActionType
        {
            get { return TypeAction; }
        }

        public Vector4 EnemyLoc
        {
            get { return Enemy.GetLocationHeight; }
        }

        //Checks if the action's animation can be drawn: the Enemy and Animation references can't be null, the Enemy must have chosen an action and the action must be in progress
        public virtual bool CanDraw()
        {
            return (Enemy != null && NewAnim != null);// && Enemy.IsDead == false); && Enemy.gsAction.ActionChose == true && Enemy.gsAction.ActionDone == false && Enemy.gsAction.ActionNotDone == false);
        }

        //Resets the current action either once it's done or the enemy gets interrupted
        public virtual void Reset()
        {

        }

        //Do something if the enemy touches a wall or solid during the action (Ex. CircleAround changes the direction the enemy moves)
        public virtual void HitWallSolid(bool hitx)
        {

        }

        //Does something involving the action or the enemy if the action has been completed or cannot be finished
        public virtual void Complete()
        {

        }

        //Makes the enemy move based on one condition and its negative if the other condition is true
        protected void ConditionMove(Vector2 moveval, bool condition1, bool condition2, SubLevel level)
        {
            if (condition1 == true)
                Enemy.ObjectMove(moveval);
            else if (condition2 == true) Enemy.ObjectMove(-moveval);
        }

        //By default, end the action after the animation finishes (this is done in A LOT of actions)
        public virtual void Update(float activeTime, SubLevel level)
        {
            if (NewAnim != null) NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
            if (NewAnim == null || NewAnim.IsAnimationEnd() == true)
                Enemy.gsAction.EndAction(activeTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine, float waterheight, Color drawcolor, float rotation, float depth)
        {
            Vector2 drawloc = new Vector2(Enemy.GetLocationHeight.X + OffSet.X, Enemy.GetLocationHeight.Y - (int)Enemy.CurHeight + OffSet.Y);

            NewAnim.Draw(spriteBatch, Enemy.SpriteSheet, drawloc, Enemy.FacingRight, drawcolor, rotation, depth);
        }
    }
}
