﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A behavior for the Diver to swim in from the side of the screen; if the Diver is set to throw a bomb, he'll do that instead of using his harpoon
    public sealed class SwimIn : Action
    {
        private bool StartedSwimming;
        private float DiverConstantZ;
        private bool Bomb;
        public float Direction;

        //prevdirection is the previous direction of the Diver; it's needed to check the direction the Diver was moving before being interrupted
        public SwimIn(Enemy enem, float prevdirection, bool bomb, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, null, ActionType.MovingAttack, finishedstate, interruptstate, unfinishedstate)
        {
            StartedSwimming = false;
            Direction = prevdirection;
            DiverConstantZ = (Enemy as Diver).ConstantZ;
            Bomb = bomb;
        }

        //Don't reset the direction so the Diver knows which direction to move after being hit
        public override void Reset()
        {
            //Set the wait timer only if the action has been completed
            if (Enemy.gsAction.ActionDone == true)
            {
                Diver diver = Enemy as Diver;
                if (diver != null) diver.SetActionWaitTimer();
            }
        }

        private Hitbox GetHarpoonHitbox()
        {
            return Enemy.CreateHitboxFeet(true, 35, 10, 7, 15, 0, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, Enemy.FacingRight, LoadSounds.Kick, true);
        }

        private TimedBomb GetDiverBomb(TileEngine TileEngine)
        {
            return new TimedBomb(new Vector3(Enemy.ForwardVal(Enemy.TrueVelocity.X), 0f, 0f), new Vector3(Enemy.GetLocationVec2.X, Enemy.GetLocationVec2.Y, Enemy.CurHeight), new Status(), 500, 5, TileEngine, false);
        }

        private void Finish()
        {
            Enemy.gsAction.EndAction(Main.GetActiveTime);
            StartedSwimming = false;
            Direction = 0f;

            Enemy.SetLocation(new Vector3(0f, 0f, DiverConstantZ));
        }

        public override void Update(float activeTime, SubLevel level)
        {
            if (StartedSwimming == false)
            {
                //Check the direction of the Diver; if it's 0, then the diver started the action normally
                if (Direction == 0f)
                {
                    if ((Enemy.GetLocationHeight.X + level.GCameraOffSet.X) >= Main.ScreenSize.X)
                    {
                        Direction = -1f;

                        //Make the Diver match his target's Y position so he can attack that player with his harpoon
                        Enemy.SetLocation(new Vector3(Enemy.GetLocation.X, Enemy.gTarget.GetLocation.Y, Enemy.CurHeight));
                    }
                    else if ((Enemy.GetLocationHeight.X + level.GCameraOffSet.X <= 0))
                    {
                        Direction = 1f;

                        //Make the Diver match his target's Y position so he can attack that player with his harpoon
                        Enemy.SetLocation(new Vector3(Enemy.GetLocation.X, Enemy.gTarget.GetLocation.Y, Enemy.CurHeight));
                    }
                    //In this case, the Diver is somewhere in the middle of the screen after having come from above (another action that leads into this one), so make him head towards the player)
                    else Direction = Enemy.gTarget.GetLocationHeight.X > Enemy.GetLocationHeight.X ? 1 : -1;

                    Enemy.ChangeDirection(Direction != -1f);
                    if (Bomb == false)
                    {
                        Enemy.AddHitbox(GetHarpoonHitbox());
                    }
                    else Enemy.ShootProjectile(GetDiverBomb(level.GetTileEngine));
                }
                //If the direction isn't 0, then the diver recovered from getting hit and needs to renew its hitbox and continue
                else
                {
                    //If the Diver was hit back to the side of the screen he initially started at, make him come in from the other side
                    if (Direction <= -1f && (Enemy.CollisionBox.X + level.GCameraOffSet.X) >= Main.ScreenSize.X)
                    {
                        Enemy.SetLocation(new Vector3(0f, Enemy.GetLocationHeight.Y, Enemy.CurHeight));
                        Direction = 1f;
                    }
                    else if (Direction >= 1f && (Enemy.CollisionBox.Right + level.GCameraOffSet.X) <= 0f)
                    {
                        Enemy.SetLocation(new Vector3(Main.ScreenSize.X - level.GCameraOffSet.X, Enemy.GetLocation.Y, Enemy.CurHeight));
                        Direction = -1f;
                    }

                    Enemy.ChangeDirection(Direction != -1f);
                    if (Bomb == false)
                    {
                        Enemy.AddHitbox(GetHarpoonHitbox());
                    }
                    else Enemy.ShootProjectile(GetDiverBomb(level.GetTileEngine));
                }

                StartedSwimming = true;
            }
            else
            {
                //Make sure the Diver moved, otherwise it means he's stuck against a solid
                float location = Enemy.GetLocationHeight.X;

                Enemy.ObjectMove(new Vector2(Enemy.TrueVelocity.X * Direction, 0f));

                //Since the Enemy Move function will naturally make the enemy fall, keep resetting the fall speed to 0
                Enemy.Jump(null, null, 0f, false);

                //If the Diver didn't move at all, then make him move up to get over the solid
                if (location == Enemy.GetLocationHeight.X)
                {
                    Enemy.SetLocation(new Vector3(0, 0, Enemy.CurHeight + 2f));
                    Enemy.HitboxFollow(0, 0, 2f);
                }

                //If the Diver got knocked down and fell, make the Diver and his hitbox rise (currently unused)
                if (Enemy.CurHeight < DiverConstantZ)
                {
                    Enemy.SetLocation(new Vector3(0, 0, Enemy.CurHeight + 2f));
                    Enemy.HitboxFollow(0, 0, 2f);

                    //If the Diver's height when rising happens to exceed the original height, set the Diver and his hitbox's height to the original height
                    if (Enemy.CurHeight > DiverConstantZ)
                    {
                        Enemy.HitboxFollow(0, 0, DiverConstantZ - Enemy.CurHeight);
                        Enemy.SetLocation(new Vector3(0, 0, DiverConstantZ));
                    }
                } 

                //Check if the Diver is off the screen
                if (Direction > 0f)
                {
                    if ((Enemy.GetLocationHeight.X + level.GCameraOffSet.X) >= (Main.ScreenSize.X + TileEngine.TileSize)) Finish();
                }
                else
                {
                    if ((Enemy.GetLocationHeight.X + level.GCameraOffSet.X) <= -TileEngine.TileSize) Finish();
                }
            }
        }
    }
}
