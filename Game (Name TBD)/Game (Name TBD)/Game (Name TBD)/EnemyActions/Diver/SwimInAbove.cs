﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A behavior for the diver to swim down on the player from above
    public sealed class SwimInAbove : Action
    {
        private bool Dived;
        private float DiverConstantZ;
        private float DiveRate;
        private float PrevDive;

        public SwimInAbove(Enemy enem, float diverate, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, null, ActionType.MovingAttack, finishedstate, interruptstate, unfinishedstate)
        {
            Dived = false;
            DiveRate = diverate;
            PrevDive = 0f;
            DiverConstantZ = (Enemy as Diver).ConstantZ;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            if (Dived == false)
            {
                if (PrevDive == 0f) PrevDive = activeTime;

                //Follow the target's location for a few seconds and come down from above; otherwise renew the hitbox and continue because the Diver was interrupted by getting hit
                Enemy.SetLocation(new Vector3(Enemy.gTarget.GetLocation.X, Enemy.gTarget.GetLocation.Y, Enemy.gTarget.GetLocationCamera(level.GCameraOffSet).Y + 30f));

                //After following the player for a few seconds, dive where he/she is
                if ((activeTime - PrevDive) >= DiveRate)
                {
                    Enemy.CreateHitboxSurrounding(4, 10, 0f, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.Standard, Enemy.FacingRight, LoadSounds.Punch1, true);
                    Dived = true;
                }
            }
            else
            {
                //Keep going down (work against gravity) until you reach the Diver's original height
                Enemy.SetLocation(Enemy.GetLocation - new Vector3(0, 0, Enemy.CurHeight - Enemy.TrueVelocity.Y));
                Enemy.HitboxFollow(0, 0, -Enemy.TrueVelocity.Y);

                //For checking if the enemy is on an object sooner; smooths out the movement so it doesn't noticeably move the enemy up upon touching a solid from above
                Enemy.ObjectMove(Vector2.Zero);
                Enemy.Jump(null, null, 0f, false);

                //If the Diver is on an object or is below or equal to the original height, end the action
                if (Enemy.CurHeight <= DiverConstantZ || Enemy.IsOnObject == true)
                {
                    //If the Diver got below the original height, move him and his hitbox back up to it
                    if (Enemy.CurHeight < DiverConstantZ)
                    {
                        Enemy.HitboxFollow(0, 0, DiverConstantZ - Enemy.CurHeight);
                        Enemy.SetLocation(new Vector3(0, 0, DiverConstantZ));
                    }
                    Enemy.gsAction.EndAction(activeTime);
                }
            }
        }
    }
}
