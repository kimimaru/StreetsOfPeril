﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A general Throw behavior for enemies; enemies can throw a player
    public class Throw : Action
    {
        public Throw(Enemy enem, NewAnimation anim, Hitbox hitbox, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Throw, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.AddHitbox(hitbox);
        }

        //public override void Update(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    Anim.Update(Main.GetActiveTime, Enemy.gJumpVelocityChange);
        //    if (Anim.IsAnimationEnd() == true)
        //    {
        //        Enemy.gsAction.EndAction(activeTime);
        //    }
        //}
    }
}
