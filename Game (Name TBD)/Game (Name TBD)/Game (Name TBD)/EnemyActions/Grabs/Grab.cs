﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A general Grab behavior for enemies; enemies grab the player and can follow up with something else after (usually a throw)
    public class Grab : Action
    {
        private bool Grabbed;

        public Grab(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Grab, finishedstate, interruptstate, unfinishedstate)
        {
            Grabbed = false;
        }

        public override void Reset()
        {
            Grabbed = false;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            if (Grabbed == false)
            {
                //Check if the enemy's target is at the same height and the the enemy's target is currently able to get grabbed
                if (Enemy.GetLocationHeight.Z == Enemy.gTarget.GetLocationHeight.Z && Enemy.gTarget.CanGetGrabbed() == true)
                {
                    //NOTE: Create a new throw hitbox here and set a grab animation! Grabs may or may not be instant, so we can cover both cases by doing this!

                    Enemy.gTarget.GetGrabbed(Enemy);
                    Enemy.gsAction.EndAction(activeTime);
                    Grabbed = true;
                    if (NewAnim != null) NewAnim.Reset();
                }
                //Otherwise, the enemy didn't finish its action
                //SUGGESTION: If the enemy is this close to the target and the target can get hurt, the target must be in the air, so now's a good time to follow up with an uppercut or other anti-air move!
                else Enemy.gsAction.EndAction(activeTime, true);
            }
            else
            {
                base.Update(activeTime, level);
            }
        }
    }
}
