﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Generates a status for Changer
    public class Generate : Action
    {
        public Generate(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Transform, finishedstate, interruptstate, unfinishedstate)
        {
            GenerateNewStatus();
        }

        public override void Reset()
        {
            (Enemy as Changer).SetGenerateTimer();
        }

        private void GenerateNewStatus()
        {
            Random random = new Random();

            if (NewAnim != null) NewAnim.Reset();

            int randomstat = (int)Status.Statuses.None;
            int randomduration = random.Next(4, 9) * 1000;
            int randompercentage = random.Next(2, 9) * 10;
            int invertorgenerate = random.Next(0, 2);

            if ((Enemy.status != (int)Status.Statuses.None && Enemy.status != (int)Status.Statuses.StunDown) && invertorgenerate == 0)
            {
                Status invertedstat = new Status(Enemy.status.GCond, Enemy.status.GStatusDur, Enemy.status.GPercentage);

                Enemy.ResetStatus();
                Enemy.InflictStatus(Status.Invert(invertedstat));
            }
            else
            {
                int randstat = 7;

                //Check Changer's health; if it's 1/3 or below his max health for his last health bar, then increase his chances of generating a positive status
                if (Enemy.HealthBars == 0 && Enemy.GetHealth <= (Enemy.CurMaxHealth / 3f))
                    randstat = 11;

                //Generate a random status effect
                switch (random.Next(randstat))
                {
                    case 0:
                    case 7: randomstat = (int)Status.Statuses.SpeedBoost;
                        break;
                    case 1: randomstat = (int)Status.Statuses.SpeedDown;
                        break;
                    case 2:
                    case 8: randomstat = (int)Status.Statuses.DamageBoost;
                        break;
                    case 3: randomstat = (int)Status.Statuses.DamageDown;
                        break;
                    case 4:
                    case 9: randomstat = (int)Status.Statuses.DefenseBoost;
                        break;
                    case 5: randomstat = (int)Status.Statuses.DefenseDown;
                        break;
                    case 6:
                    case 10: randomstat = (int)Status.Statuses.StunDown;
                        break;
                }

                Enemy.ResetStatus();
                Enemy.InflictStatus(new Status(randomstat, randomduration, randompercentage));
            }
        }

        //public override void Update(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    Anim.Update(Main.GetActiveTime);
        //    if (Anim.IsAnimationEnd() == true)
        //    {
        //        Enemy.gsAction.EndAction(activeTime);
        //    }
        //}
    }
}
