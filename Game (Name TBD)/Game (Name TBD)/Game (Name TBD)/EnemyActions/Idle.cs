﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Idle behavior for enemies
    public class Idle : Action
    {
        public Idle(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Idle, finishedstate, interruptstate, unfinishedstate)
        {
            //Enemy = enem;
            //Anim = Enemy.GetIdleAnim;
            //if (Anim != null) Anim.Reset(Main.GetActiveTime);
            //
            //FinishedState = finishedstate;
            //InterruptState = interruptstate;
            //UnfinishedState = unfinishedstate;
        }

        //Don't do anything - enemy is idle
        //public override void Update(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    if (NewAnim != null) NewAnim.Update(Main.GetActiveTime, Enemy.gJumpVelocityChange);
        //    if (Anim != null) Anim.Update(Main.GetActiveTime, Enemy.gJumpVelocityChange);
        //    if (Anim == null || Anim.IsAnimationEnd() == true)
        //        Enemy.gsAction.EndAction(activeTime);
        //}
    }
}
