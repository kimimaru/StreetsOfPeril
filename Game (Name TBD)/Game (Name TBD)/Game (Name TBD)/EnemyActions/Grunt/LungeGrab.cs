﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A behavior for Grunt that makes him lunge at the player; if he touches the player, he grabs him/her
    public class LungeGrab : Action
    {
        public LungeGrab(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.FaceTarget();
            Enemy.Jump(Enemy.ForwardVal(Enemy.TrueVelocity.X), 0f, Enemy.GetOrigJumpVelocity.Z - 1f);

            Enemy.CreateHitboxFeet(true, 40, 20, 0, 20, 200, 100, Hitbox.HitboxTypes.KnockDown, Enemy.FacingRight, LoadSounds.Kick, true);
            //Enemy.CreateThrowHitbox(true, 40, 20, 0, 20, 200, 100, Enemy.GetFacingDir, 0, , true);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Enemy.ObjectMove(new Vector2(Enemy.GetAirVelocity.X, 0));
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //If Grunt landed, then he didn't grab onto a player and thus the action was not completed
            if (Enemy.IsOnGround == true)
                Enemy.gsAction.EndAction(activeTime, true);
        }
    }
}
