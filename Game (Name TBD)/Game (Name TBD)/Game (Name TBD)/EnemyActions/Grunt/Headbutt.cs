﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The Headbutt move Grunt performs after landing his Lunge Grab on a player
    public class Headbutt : Action
    {
        public Headbutt(Enemy enem, NewAnimation anim, Hitbox hitbox, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.GrabAttack, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.AddHitbox(hitbox);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
            if (NewAnim.IsAnimationEnd() == true)
            {
                Enemy.gsIsJumping = false;
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
