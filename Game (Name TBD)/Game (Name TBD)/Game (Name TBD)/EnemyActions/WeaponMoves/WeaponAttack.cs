﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //An attack with a weapon that an enemy can perform, which includes a swing or slash
    public class WeaponAttack : Action
    {
        public WeaponAttack(Enemy enem, NewAnimation anim, Hitbox weaponhitbox, SoundEffect attacksound, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Attack, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.weapon.AddHitbox(weaponhitbox);
        }
    }
}
