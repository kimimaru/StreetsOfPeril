﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Commander to switch the behavior of his minions
    public sealed class CommandMinions : Action
    {
        //The behavior the minions are set to perform
        private int MinionBehavior;

        //Tells if Commander switched the behavior of his minions yet or not
        private bool Commanded;

        public CommandMinions(Enemy enem, NewAnimation anim, int minionbehavior, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Instruct, finishedstate, interruptstate, unfinishedstate)
        {
            if (minionbehavior != (int)ActionManager.ActionStates.Command) Enemy.FaceAwayTarget();
            else Enemy.FaceTarget();

            MinionBehavior = minionbehavior;
            Commanded = false;

            //Make sure the specified behavior is a proper one
            if (Helper.WithinEnumRange(typeof(ActionManager.ActionStates), MinionBehavior) == false)
                MinionBehavior = (int)ActionManager.ActionStates.Movement;
        }

        public override bool CanDraw()
        {
            return (base.CanDraw() == true && Animation.IsAnimationEnd() == false);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //At the start of the last frame, where Commander is directing his minions to switch to a behavior, is when the enemies go switch their behaviors
            NewAnim.Update(Main.GetActiveTime);
            if (Commanded == false && NewAnim.CurrentFrame == NewAnim.MaxFrame)
            {
                Commander commander = Enemy as Commander;

                if (commander != null)
                {
                    commander.SetMinionCommand(MinionBehavior);

                    ////Switch the minions' behavior to the designated one
                    //for (int i = 0; i < commander.GetMinions.Count; i++)
                    //{
                    //    commander.GetMinions[i].gsAction.ActionChose = true;
                    //    commander.GetMinions[i].gsAction.SwitchBehavior(MinionBehavior);
                    //    commander.GetMinions[i].gsAction.ActionChose = true;

                    //    //If the enemy is set to the Command behavior, set the enemy's current action to defend the commander and help
                    //    //This is needed because it stores the commander the enemies have, which they'll need when they have to rechoose an action to perform after getting hit
                    //    if (MinionBehavior == (int)ActionManager.ActionStates.Command)
                    //    {
                    //        commander.GetMinions[i].gsAction.CurrentAction = new DefendCommander(commander.GetMinions[i], null, commander);
                    //    }
                    //    else commander.GetMinions[i].gsAction.ActionDone = true;
                    //}
                }

                Commanded = true;
            }
            //If the animation is done, Commander is finished with the action
            else if (NewAnim.IsAnimationEnd() == true) Enemy.gsAction.EndAction(activeTime);
        }
    }
}
