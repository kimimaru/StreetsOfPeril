﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //An action for Commander to randomly choose a minion out of the ones he has and throw it at the player; this occurs only if the minions are currently defending Commander
    //The thrown minion does not take damage

    //NOTE: Grab the minion and put it in front of Commander when it's chosen
    //If the minion dies during the throw, end the action
    public class ThrowMinion : Action
    {
        private Commander Commander;
        private Enemy ThrownMinion;

        public ThrowMinion(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Attack, finishedstate, interruptstate, unfinishedstate)
        {
            Commander = Enemy as Commander;

            //Choose a minion to throw
            ChooseMinion();
        }

        public override bool CanDraw()
        {
            return (base.CanDraw() && NewAnim.IsAnimationEnd() == false);
        }

        //Gets the thrown minion
        public Enemy MinionThrown
        {
            get { return ThrownMinion; }
        }

        //Chooses a random minion to throw
        private void ChooseMinion()
        {
            //Just make sure the enemy performing this action is actually Commander and Commander has minions; he should only choose it if he does
            if (Commander.HasMinions() == true)
            {
                //Possible minion list
                List<Enemy> PotentialMinions = new List<Enemy>();

                //Go through all of Commander's minions and store the ones that are well and thus can be selected
                for (int i = 0; i < Commander.GetMinions.Count; i++)
                {
                    if (Commander.GetMinions[i].IsWell() == true) PotentialMinions.Add(Commander.GetMinions[i]);
                }

                //Make sure there is at least one minion to choose from
                if (PotentialMinions.Count > 0)
                {
                    int randminion = new Random().Next(0, PotentialMinions.Count);

                    ThrownMinion = PotentialMinions[randminion];
                }
                //If a minion is unable to be chosen, Commander is unable to continue the action
                else Commander.gsAction.EndAction(Main.GetActiveTime, true);
            }
            //If Commander has no minions, he can't carry out the action
            else Commander.gsAction.EndAction(Main.GetActiveTime, true);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Grab the minion that is supposed to be thrown (we need it here to access the level's OffSet, TileEngine, and the list of Solids)
            if (ThrownMinion != null && ThrownMinion.gGrabbox.GSIsGrabbed == false)
                ThrownMinion.GetGrabbed(Enemy);

            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //If the minion is chosen and the animation reaches the final frame (just for now, change later), throw the minion and set that minion reference to null so it doesn't get thrown again
            if (ThrownMinion != null && NewAnim.CurrentFrame == NewAnim.MaxFrame)
            {
                Hitbox hitbox = Hitbox.Empty;
                hitbox.SetHitboxType(Hitbox.HitboxTypes.KnockDown);

                //ThrownMinion.Move(activeTime, new Vector2(), OffSet, TileEngine, Solids, false);
                Enemy.ThrowFighter(ThrownMinion, 0, hitbox);

                //Add the thrown minion's hitbox to Commander and his other minions' hurtboxes so they don't get hit by it
                ThrownMinion.Hitboxes[ThrownMinion.Hitboxes.Count - 1].AddHurtbox(Commander.Hurtbox);
                for (int i = 0; i < Commander.GetMinions.Count; i++)
                    ThrownMinion.Hitboxes[ThrownMinion.Hitboxes.Count - 1].AddHurtbox(Commander.GetMinions[i].Hurtbox);

                ThrownMinion = null;
            }

            //If the animation is done or the thrown minion is dead, then end the action
            if ((ThrownMinion != null && ThrownMinion.IsDead == true) || NewAnim.IsAnimationEnd() == true)
            {
                Enemy.gsAction.EndAction(Main.GetActiveTime);
            }
        }
    }
}
