﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //The behavior that enemies perform when in the Command behavior; enemies surround and protect Commander (they usually walk towards him)
    //This is the only action that doesn't actually end on its own
    //Since Commander is a boss, all the enemies will automatically die when he dies, so we don't need to worry about switching their behaviors back
    public sealed class DefendCommander : Action
    {
        //The commander that the enemies should be defending
        private Commander Commander;

        //The index of the minion; this determines where the minion will be around the commander
        private int MinionIndex;

        public DefendCommander(Enemy enem, NewAnimation anim, Commander commander, int minionindex) : base(enem, anim, ActionType.Instruct, null, null, null)
        {
            Commander = commander;
            MinionIndex = minionindex;
        }

        public Commander GetCommander
        {
            get { return Commander; }
        }

        public int GetMinionIndex
        {
            get { return MinionIndex; }
        }

        //Gets a spot around the commander to defend based on the index of the enemy
        private Vector2 DefendLocation()
        {
            Vector2 DefendLoc = Commander.GetLocationVec2;

            //Based on the index of the minion, choose a spot to defend
            switch (MinionIndex)
            {
                case 0: DefendLoc.X -= 40;
                        DefendLoc.Y += 10;
                    break;
                case 1: DefendLoc.X -= 40;
                        DefendLoc.Y += 30;
                    break;
                case 2: DefendLoc.Y += 30;
                    break;
                case 3: DefendLoc.X += 40;
                        DefendLoc.Y += 30;
                    break;
                default: DefendLoc.X += 40;
                         DefendLoc.Y += 10;
                    break;
            }

            return DefendLoc;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Have the enemies follow and surround Commander
            if (NewAnim != null) NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            Enemy.FaceTarget();

            //Use Commander's velocity so his minions can follow him
            Vector2 commandervelocity = Commander.TrueVelocity;

            //Make the minions move to their positions around Commander based on their minion indexes
            Vector2 defendloc = DefendLocation();

            ConditionMove(new Vector2(commandervelocity.X, 0), (Enemy.GetLocationVec2.X + commandervelocity.X) < defendloc.X, (Enemy.GetLocationVec2.X - commandervelocity.X) > defendloc.X, level);
            ConditionMove(new Vector2(0, commandervelocity.Y), (Enemy.GetLocationVec2.Y + commandervelocity.Y) < defendloc.Y, (Enemy.GetLocationVec2.Y - commandervelocity.Y) > defendloc.Y, level);
        }
    }
}
