﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Commander to spawn more minions
    public sealed class SpawnMinions : Action
    {
        //The number of unique enemies Commander can have for minions
        private const int NumberUniqueMinions = 5;

        //The level's enemy list is passed in so you can add Commander's newly spawned minions to it
        public SpawnMinions(Enemy enem, NewAnimation anim, Vector2 OffSet, List<Enemy> levelenemlist, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Instruct, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.FaceAwayTarget();

            Commander commander = Enemy as Commander;

            //Spawn an amount of random minions equal to Commander's maximum amount of minions
            if (commander != null)
            {
                Random random = new Random();

                //Give Commander a fresh set of minions, and tell them to defend Commander by default
                for (int i = 0; i < commander.MaxMinions; i++)
                {
                    //NOTE: Make sure that wherever you fight Commander, the surrounding area is entirely flat; this reduces complexity and ensures that the minions behave as intended
                    Vector2 spawnloc = new Vector2(commander.ForwardVal(OffSet.X - TileEngine.TileSize, Main.ScreenSize.X + OffSet.X + TileEngine.TileSize), Main.ScreenHalf.Y + OffSet.Y);

                    //Spawn a random type of the selectable enemies from behind Commander
                    commander.GetMinions.Add(SpawnMinion(random.Next(0, NumberUniqueMinions), spawnloc));
                    Enemy minion = commander.GetMinions[commander.GetMinions.Count - 1];
                    minion.SetDesignated(false);

                    //Check if you should choose a random status or no status; it's a 1/4 chance of a minion spawning with a status
                    if (minion.HasSpawnableStatuses == true && random.Next(0, 4) == 0)
                    {
                        Status[] minionstatuses = minion.PossibleStatuses;

                        //Choose a random status to spawn with out of the possible ones
                        minion.SetSpawnStatus(new Status(minionstatuses[random.Next(0, minionstatuses.Length)]));
                    }
                    
                    //Defend Commander
                    minion.gsAction.CurState = (int)ActionManager.ActionStates.Command;
                    minion.gsAction.CurrentAction = new DefendCommander(minion, minion.GetWalkAnim, commander, i);
                    levelenemlist.Add(minion);
                }
            }
            else Enemy.gsAction.ActionDone = true;
        }

        public override bool CanDraw()
        {
            return (base.CanDraw() == true && NewAnim.IsAnimationEnd() == false);
        }

        //Choose from one of the Basic enemies and the following Intermediate enemies: Greg, Grunt
        //NOTE: I'm not sure how to handle their statuses yet since they all have different statuses, so for now just make them have none (Grunt is a problem in this regard since he relies on statuses)
        //NOTE: For handling statuses, make sure the possible statuses (condition, duration, and percentage) are stored in each enemy class so you can reference one of them; right now only the possible status conditions is stored, not the duration or percentage
        private Enemy SpawnMinion(int randenemy, Vector2 spawnloc)
        {
            Vector3 SpawnLoc = new Vector3(spawnloc, 0f);

            switch (randenemy)
            {
                case 0: return CreateObjects.Mark(SpawnLoc, new Status());
                case 1: return CreateObjects.Christopher(SpawnLoc, new Status());
                case 2: return CreateObjects.Paul(SpawnLoc, new Status());
                case 3: return CreateObjects.Greg(SpawnLoc, new Status());
                case 4:
                default: return CreateObjects.Grunt(SpawnLoc, new Status());
            }
        }

        //public override void Update(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        //{
        //    Anim.Update(Main.GetActiveTime);
        //    if (Anim.IsAnimationEnd() == true)
        //        Enemy.gsAction.EndAction(activeTime);
        //}
    }
}
