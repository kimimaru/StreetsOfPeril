﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A swinging down kick that Commander performs; Commander moves his leg straight up, then swings it down hard (like Samus's Up Tilt from the Super Smash Bros. series)
    public class SDownKick : Action
    {
        public SDownKick(Enemy enem, NewAnimation anim, Hitbox hitbox, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Attack, finishedstate, interruptstate, unfinishedstate)
        {
            //Enemy.gHitbox = hitbox;
            Enemy.AddHitbox(hitbox);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            int animframe = NewAnim.CurrentFrame;

            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Move the hitbox down as the attack progresses
            //NOTE: These values are temporary
            if (animframe != NewAnim.CurrentFrame)
            {
                Enemy.HitboxFollow(Enemy.ForwardVal(5), 0, 5);
            }

            //End the move when the animation is complete
            if (NewAnim.IsAnimationEnd() == true)
                Enemy.gsAction.EndAction(Main.GetActiveTime);
        }
    }
}
