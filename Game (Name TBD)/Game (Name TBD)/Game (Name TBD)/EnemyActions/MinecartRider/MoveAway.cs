﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Makes the Minecart Rider move away from the player minecart
    public class MoveAway : Action
    {
        public MoveAway(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            HasInvincibility = true;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Move the Minecart Rider away from the player minecart
            Enemy.PureMove(Enemy.ForwardVal(-Enemy.TrueVelocity.X), 0f);
        }
    }
}
