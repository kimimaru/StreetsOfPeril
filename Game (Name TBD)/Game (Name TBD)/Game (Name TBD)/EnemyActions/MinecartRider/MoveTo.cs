﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Makes the Minecart Rider (and others in minecarts) go towards the player minecart
    public class MoveTo : Action
    {
        public MoveTo(Enemy enem, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
    
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Move the enemy towards the player minecart
            Enemy.PureMove(Enemy.ForwardVal(Enemy.TrueVelocity.X), 0f);

            //If the enemy is close to the player minecart, stop
            if (Enemy.GetLocationHeight.X >= (Main.ScreenHalf.X / 2) && Enemy.GetLocationHeight.X <= (Main.ScreenSize.X * .75f))
            {
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
