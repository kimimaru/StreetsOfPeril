﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for the Minecart Rider to throw the explosive bottle
    public sealed class ThrowBottle : Action
    {
        private Vector2 BottleVelocity;

        public ThrowBottle(Enemy enem, Vector2 bottlevelocity, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Projectile, finishedstate, interruptstate, unfinishedstate)
        {
            BottleVelocity = bottlevelocity;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Enemy.ShootProjectile(new ExplosiveBottle(BottleVelocity, new Vector2(Enemy.ForwardVal(Enemy.GetLocationVec2.X + Enemy.CollisionBox.Width, Enemy.GetLocationVec2.X - 10), Enemy.GetLocationVec2.Y + 10), new Status(), level.GetTileEngine, !Enemy.FacingRight));
            LoadSounds.Play(LoadSounds.EnemyLaugh);
            Enemy.gsAction.EndAction(activeTime);
        }
    }
}
