﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //A simple attack most enemies can do, which can includes a kick or uppercut
    public class SimpleAttack : Action
    {
        public SimpleAttack(Enemy enem, NewAnimation anim, Hitbox hitbox, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Attack, finishedstate, interruptstate, unfinishedstate)
        {
            Enemy.AddHitbox(hitbox);
        }
    }
}
