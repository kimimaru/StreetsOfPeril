﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Theodore's enrage action - it boosts his Damage and Defense by 1.5x, his speed by 1, and lets him do actions previously unavailable to him
    public class Enrage : Action
    {
        //The length of Theodore's enraging animation
        private const float EnrageTime = 3000f;
        private float PrevEnrage;

        public Enrage(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Transform, finishedstate, interruptstate, unfinishedstate)
        {
            HasInvincibility = true;

            //Set a hitbox around Theodore so players are discouraged from camping right next to him until he finishes enraging
            Enemy.CreateHitboxSurrounding(5, Enemy.ObjectHeight / 2, 1500, 500, Hitbox.HitboxTypes.KnockDown, Enemy.FacingRight, LoadSounds.Punch1, true);

            PrevEnrage = Main.GetActiveTime;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime);
            if (NewAnim.IsAnimationEnd() == true)
            {
                Theodore theodore = Enemy as Theodore;
                if (theodore != null) theodore.Enrage(Main.GetActiveTime);

                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
