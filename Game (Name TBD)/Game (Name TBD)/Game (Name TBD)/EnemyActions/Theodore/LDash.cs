﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Theodore's Lightning Dash attack - Theodore dashes very quickly behind the player (follows up with a kick in the opposite direction of the dash afterwards)
    //Enraged only action
    public class LDash : Action
    {
        private float Destination;
        private int Direction;

        public LDash(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Movement, finishedstate, interruptstate, unfinishedstate)
        {
            //Find which direction to move in
            Direction = Enemy.ForwardVal(1);

            //Set the destination Theodore goes; this makes it so he doesn't keep going further as the player moves
            if (Enemy.TargetInFront() == true) Destination = Enemy.gTarget.CollisionBox.Right + Enemy.gTarget.Hurtbox.Width;
            else Destination = Enemy.gTarget.CollisionBox.X - Enemy.gTarget.Hurtbox.Width;
        }

        public override void Reset()
        {
            Destination = 0f;
            Direction = 1;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Dash towards the target
            Enemy.ObjectMove(new Vector2(Enemy.ForwardVal(Enemy.TrueVelocity.X + 3f), 0f));

            //Check if Theodore reached the end; if so, then end the action
            if ((Enemy.GetLocationVec2.X * -Direction) <= (Destination * -Direction))
            {
                //Face the target
                Enemy.FaceTarget();
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
