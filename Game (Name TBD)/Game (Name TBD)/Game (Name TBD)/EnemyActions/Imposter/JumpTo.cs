﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Imposter, wearing the Speed mask, to jump to a spot near the target; this can match the target's X or Y position or simply be used to jump forwards or backwards
    public class JumpTo : Action
    {
        //The location Imposter should land
        private Vector2 LandLocation;

        public JumpTo(Enemy enem, Vector2 landlocation, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, enem.GetIdleAnim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            LandLocation = landlocation;

            //Calculate the total air time
            float TimeInAir = Movement.TotalAirTime(Enemy.GetOrigJumpVelocity.Z, Enemy.GetGravityValue);

            //Figure out how fast to move to the destination
            Enemy.Jump((float)Math.Round(((LandLocation.X - Enemy.GetLocationVec2.X) / TimeInAir), 1), (float)Math.Round(((LandLocation.Y - Enemy.GetLocationVec2.Y) / TimeInAir), 1), Enemy.GetOrigJumpVelocity.Z);
            Enemy.FaceTarget();
        }

        public override void Update(float activeTime, SubLevel level)
        {
            //Move to the destination
            Enemy.ObjectMove(new Vector2(Enemy.GetAirVelocity.X, Enemy.GetAirVelocity.Y));

            //Once the enemy touches the floor, end the action
            if (Enemy.IsOnGround == true)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
