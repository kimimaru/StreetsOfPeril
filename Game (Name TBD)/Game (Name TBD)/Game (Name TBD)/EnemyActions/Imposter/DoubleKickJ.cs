﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Imposter's Double Kick in the air when she's wearing the Speed Mask
    public class DoubleKickJ : Action
    {
        public DoubleKickJ(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Jumping, finishedstate, interruptstate, unfinishedstate)
        {
            LoadSounds.Play(LoadSounds.JumpKick);
            Enemy.FaceTarget();
            Enemy.Jump(Enemy.ForwardVal(Enemy.TrueVelocity.X), 0f, Enemy.GetOrigJumpVelocity.Z);
            
            Enemy.CreateHitboxFeet(true, 39, 30, 0, 25, 50, 300, Hitbox.HitboxTypes.Standard, Enemy.FacingRight, LoadSounds.Punch1, true);
            Enemy.CreateHitboxFeet(true, 39, 30, 0, 25, 350, Hitbox.InfiniteHitbox, Hitbox.HitboxTypes.KnockDown, Enemy.FacingRight, LoadSounds.Kick, true);
        }

        public override bool CanDraw()
        {
            return (base.CanDraw() && Enemy.IsOnGround == false);
        }

        public override void Update(float activeTime, SubLevel level)
        {
            Enemy.ObjectMove(new Vector2(Enemy.GetAirVelocity.X, 0));
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);

            //Once Imposter hits the ground, the action is completed
            if (Enemy.IsOnGround == true)
                Enemy.gsAction.EndAction(activeTime);
        }
    }
}
