﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //Behavior for Imposter to change masks
    public class SwitchMask : Action
    {
        public SwitchMask(Enemy enem, NewAnimation anim, int? finishedstate = null, int? interruptstate = null, int? unfinishedstate = null) : base(enem, anim, ActionType.Transform, finishedstate, interruptstate, unfinishedstate)
        {
            HasInvincibility = true;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(activeTime, Enemy.GetGravityValue);
            if (NewAnim.IsAnimationEnd() == true)
            {
                Imposter imposter = Enemy as Imposter;

                if (imposter != null)
                {
                    imposter.ChangeMask();
                }

                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
