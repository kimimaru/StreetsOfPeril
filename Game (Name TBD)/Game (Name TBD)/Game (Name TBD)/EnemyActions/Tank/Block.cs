﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Game__Name_TBD_
{
    //This action is for the Tank enemy; it makes him slide after blocking an attack
    public class Block : Action
    {
        float SlideAmount;

        public Block(Enemy enem, NewAnimation anim) : base(enem, anim, ActionType.Defend, null, null, null)
        {
            SlideAmount = 3f;
        }

        public override void Update(float activeTime, SubLevel level)
        {
            NewAnim.Update(Main.GetActiveTime, Enemy.GetGravityValue);
            Enemy.ObjectMove(new Vector2(Enemy.ForwardVal(-SlideAmount), 0f));
            SlideAmount -= .2f;
            SlideAmount = (float)Math.Round(SlideAmount, 1);

            //If Tank is done sliding, end the action
            if (SlideAmount <= 0)
            {
                //Make sure Tank is on a whole number value after sliding
                Enemy.SetLocation(new Vector3((int)Enemy.GetLocationVec2.X, Enemy.GetLocationVec2.Y, Enemy.CurHeight));
                Enemy.gsAction.EndAction(activeTime);
            }
        }
    }
}
