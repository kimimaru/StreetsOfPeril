﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class is used to quickly generate each kind of enemy, item, item container, weapon, etc. in the game; it's static so that it can be called anywhere
    //NOTE: Think of the best way to create alternative enemies (recolors)!
    public static class CreateObjects
    {
        //Checks if enemy stats should be scaled on the game's difficulty based on the state the game is in; if the player is in a challenge mode, they shouldn't scale
        private static int CheckStatScale(int scaleamount)
        {
            if (Main.CheckState(Main.GameState.InChallenge) == true) return 0;
            else return scaleamount;
        }

        //Characters
        public static Graham Graham(int playernum, int costume, bool avoidenemy = false)
        {
            return (new Graham(100, 1, playernum, costume, avoidenemy));
        }

        public static Wil Wil(int playernum, int costume, bool avoidenemy = false)
        {
            return (new Wil(100, 1, playernum, costume, avoidenemy));
        }

        public static Crystal Crystal(int playernum, int costume, bool avoidenemy = false)
        {
            return (new Crystal(100, 1, playernum, costume, avoidenemy));
        }

        public static Jeff Jeff(int playernum, int costume, bool avoidenemy = false)
        {
            return (new Jeff(100, 1, playernum, costume, avoidenemy));
        }

        public static Graham SurvivalNGraham(int playernum, int costume, bool avoidenemy = false)
        {
            return (new SurvivalNGraham(100, 1, playernum, costume, avoidenemy));
        }
        //End characters

        //Enemies - change their stats based off the current level and the difficulty level
        //Test enemy
        public static Mark CreateGalsia(Vector3 Location, int levelnum = 0)
        {
            return (new Mark("Galsia", 30, 1, 7, 2, new Status((int)Status.Statuses.SpeedBoost, 5000), Location, new Vector2(2, 2), null));
        }

        //Bosses
        public static Commander Commander(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Commander("Commander", 100, 2, 8, 2, status, Location, new Vector2(3, 3), null, alternate));
        }

        //All enemies - General Enemies
        public static MinecartRider MinecartRider(String Name, Vector3 Location, Status status, Texture2D Sprite, Vector2 bottlevelocity, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new MinecartRider(Name, 15 + CheckStatScale(Main.Difficulty * 4), 1, 0 + CheckStatScale(Main.Difficulty), 0, status, Location, null, Sprite, bottlevelocity, alternate));
        }

        public static Diver Diver(String Name, Vector3 Location, Status status, Texture2D Sprite, float diverate, int behavior, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Diver(Name, 20 + CheckStatScale(Main.Difficulty * 5), 1, 6 + CheckStatScale(Main.Difficulty), 1, status, Location, null, Sprite, diverate, behavior, alternate));
        }

        //Basic Enemies
        public static Mark Mark(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            /*if (alternate == 0)*/
            return (new Mark("Mark", 20 + CheckStatScale(Main.Difficulty * 5), 1, 3 + CheckStatScale(Main.Difficulty / 2), 0, status, Location, new Vector2(2, 2), null, alternate));
            //else return (new Mark("Hector", 20 + CheckStatScale(Main.Difficulty * 5), 1, 3 + CheckStatScale(Main.Difficulty / 2), 0, status, Location, new Vector2(2, 2), LoadGraphics.CreateHUD(LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Mark])));
        }

        public static Christopher Christopher(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Christopher("Christopher", 25 + CheckStatScale(Main.Difficulty * 3), 1, 4 + CheckStatScale(Main.Difficulty / 2), 0, status, Location, new Vector2(2, 2), null, alternate));
        }

        public static Paul Paul(Vector3 Location, Status status, int Behavior = 0, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Paul("Paul", 30 + CheckStatScale(Main.Difficulty * 5), 1, 2 + CheckStatScale(Main.Difficulty / 2), 1, status, Location, null, Behavior, alternate));
        }

        //Intermediate Enemies
        public static Tank Tank(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Tank("Tank", 60 + CheckStatScale(Main.Difficulty * 10), 1, 3 + CheckStatScale(Main.Difficulty), 2 + CheckStatScale(Main.Difficulty), status, Location, null, alternate));
        }

        public static Greg Greg(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Greg("Greg", 80 + CheckStatScale(Main.Difficulty * 15), 1, 3 + CheckStatScale(Main.Difficulty), 0 + CheckStatScale(Main.Difficulty / 2), status, Location, new Vector2(3, 3), null, alternate));
        }

        public static Grunt Grunt(Vector3 Location, Status status, int Behavior = 0, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Grunt("Grunt", 40 + CheckStatScale(Main.Difficulty * 10), 1, 0, 0 + CheckStatScale(Main.Difficulty / 2), status, Location, new Vector2(4, 4), null, Behavior, alternate));
        }

        public static Rogue Rogue(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Rogue("Rogue", 20 + CheckStatScale(Main.Difficulty * 5), 1, 4 + CheckStatScale(Main.Difficulty), 1, status, Location, null, alternate));
        }

        public static Changer Changer(Vector3 Location, Status status, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Changer("Changer", 70 + CheckStatScale(Main.Difficulty * 15), 1, 7 + CheckStatScale(Main.Difficulty * 2), 1, status, Location, new Vector2(3, 3), null, 0, alternate));
        }

        public static Imposter Imposter(Vector3 Location, Status status, int startingmask = 0, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Imposter("Imposter", 65 + CheckStatScale(Main.Difficulty * 30), 1, 6, 0, status, Location, new Vector2(3, 3), startingmask, alternate));
        }

        public static WeaponWielder WeaponWielder(Vector3 Location, Status status, bool Male, Weapon spawnweapon, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            //The male version of the Weapon Wielder is Gerald, and the female version is Kate
            String name = "Gerald";
            int healthscale = CheckStatScale(Main.Difficulty * 20);
            if (Male == false)
            {
                name = "Kate";
                healthscale = CheckStatScale(Main.Difficulty * 15);
            }

            return (new WeaponWielder(name, 70 + healthscale, 1, 5, 1, status, Location, new Vector2(3, 3), spawnweapon, Male, alternate));
        }

        //Strong enemies
        public static Samson Samson(Vector3 Location, Status status, int Behavior = 0, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Samson("Samson", 100 + CheckStatScale(Main.Difficulty * 20)/* + (levelnum * 10)*/, 1, 10 + CheckStatScale(Main.Difficulty), 2, status, Location, null, Behavior, alternate));
        }

        public static Theodore Theodore(Vector3 Location, Status status, int Behavior = 0, int levelnum = 0, int diffnum = 0, int alternate = 0)
        {
            return (new Theodore("Theodore", 80 + CheckStatScale(Main.Difficulty * 15)/* + (levelnum * 5)*/, 1, 8 + CheckStatScale(Main.Difficulty), 1, status, Location, null, Behavior, alternate));
        }
        //End enemies

        //Levels
        public static Level CreateLevel(List<Player> Players, int levelnum)
        {
            return (new Level(Players, LoadGraphics.BG, LoadGraphics.FG, levelnum));
        }

        public static RankLevel CreateRankLevel(List<Player> Players, int levelnum)
        {
            return (new RankLevel(Players, levelnum));
        }
        //End levels

        //Item containers
        public static ItemContainer Barrel(Vector3 Location, int Health, Item helditem, Weapon heldweapon, int alternate = 0)
        {
            return (new ItemContainer(LoadGraphics.ContainerSprite, "Barrel", Location, Health, helditem, heldweapon, 0, alternate, false, new Vector2(32, 49), new Vector2(32, 52)));
        }
        //End item containers

        //Weapons
        public static Weapon Katana(Status status)
        {
            return (new Weapon("Katana", 8, 50, Vector2.Zero, new Vector2(4, 2), 10000, status, (int)Weapon.WeaponTypes.Swing, LoadSounds.SharpSwing, LoadSounds.SharpHit));
        }

        public static Weapon Knife(Status status)
        {
            return (new Weapon("Knife", 6, 30, Vector2.Zero, new Vector2(5, 3), 10000, status, (int)Weapon.WeaponTypes.Stab, null, LoadSounds.SharpHit));
        }

        public static Weapon BrassKnuckles(Status status)
        {
            return (new Weapon("Brass Knuckles", 4, 0, Vector2.Zero, new Vector2(2, 2), 10000, status, (int)Weapon.WeaponTypes.None, null, null));
        }

        //Weapon Wielders' signature weapon
        public static Weapon SwiftBlade(Status status)
        {
            return (new Weapon("Swift Blade", 12, 60, Vector2.Zero, new Vector2(4, 2), 10000, status, (int)Weapon.WeaponTypes.Swing, LoadSounds.SharpSwing, LoadSounds.SharpHit));
        }
        //End weapons

        //Items - Healing
        public static Item RiceBowl()
        {
            return (new Item(LoadGraphics.ItemSprite, "Rice Bowl", 10, 4000, 3, false, 0, new Status(), Vector2.Zero, Vector2.Zero));
        }

        public static Item Biscuit(Status status)
        {
            return (new Item(LoadGraphics.ItemSprite, "Biscuit", 10, 0, 1, false, 1000, status, Vector2.Zero, Vector2.Zero));
        }

        public static Item EnergyDrink()
        {
            return (new Item(LoadGraphics.ItemSprite, "Energy Drink", 10, 2000, 4, false, 0, new Status(), Vector2.Zero, Vector2.Zero));
        }

        public static HealthCandy HealthCandy()
        {
            return (new HealthCandy());
        }

        public static Item Turkey(Status status)
        {
            return (new Item(LoadGraphics.Turkey, "Turkey", 70, 0, 1, false, 0, status, Vector2.Zero, Vector2.Zero));
        }

        //Items - Points
        public static Item PileOfMoney()
        {
            return (new Item(LoadGraphics.PileOfMoney, "Pile of Money", 0, 0, 1, false, 3000, new Status(), Vector2.Zero, Vector2.Zero));
        }

        public static Item PocketWatch()
        {
            return (new Item(LoadGraphics.PocketWatch, "Pocket Watch", 0, 0, 1, false, 8000, new Status(), Vector2.Zero, Vector2.Zero));
        }

        public static Item BagofJewels()
        {
            return (new Item(LoadGraphics.BagOfJewels, "Bag of Jewels", 0, 0, 1, false, 15000, new Status(), Vector2.Zero, Vector2.Zero));
        }

        //Items - Other
        public static Item PoisonRiceBowl()
        {
            return (new Item(LoadGraphics.ItemSprite, "Poison Rice Bowl", 0, 0, 1, false, 0, new Status((int)Status.Statuses.Poison, 6000, 0), Vector2.Zero, Vector2.Zero));
        }

        public static Item BearClaw()
        {
            return (new Item(LoadGraphics.ItemSprite, "Bear Claw", 0, 0, 1, false, 0, new Status((int)Status.Statuses.DamageBoost, 10000, 0), Vector2.Zero, Vector2.Zero)); 
        }

        public static Item ClamShell()
        {
            return (new Item(LoadGraphics.ItemSprite, "Clam Shell", 0, 0, 1, false, 0, new Status((int)Status.Statuses.DefenseBoost, 10000, 0), Vector2.Zero, Vector2.Zero));
        }

        public static Item GrahamTrophy()
        {
            return (new Item(LoadGraphics.ItemSprite, "Graham Trophy", 0, 0, 1, false, 0, new Status((int)Status.Statuses.Invincible, 15000), Vector2.Zero, Vector2.Zero));
        }

        public static Item GrahamDoll()
        {
            return (new Item(LoadGraphics.GrahamDoll, "Graham Doll", 0, 0, 1, true, 0, new Status(), Vector2.Zero, new Vector2(500, 400)));
        }

        public static HealthJuice HealthJuice()
        {
            return (new HealthJuice(new Vector2(500, 400)));
        }

        public static BToken BonusToken()
        {
            return (new BToken(Vector2.Zero, new Vector2(500, 400)));
        }

        public static OTReplace OTReplace()
        {
            return (new OTReplace(new Vector2(500, 400)));
        }
        //End items

        //Hazards
        public static FallingRock FallingRock(Texture2D Sprite, Texture2D dust, Vector3 Location, Status stat, int hitboxstrength)
        {
            return (new FallingRock(Sprite, dust, Location, stat, hitboxstrength));
        }

        public static BSShooter BSShooter(bool bullet, Vector3 Location, bool facingright, float shootrate = 500f, int projectiledamage = 7)
        {
            return (new BSShooter(bullet, Location, facingright, shootrate, projectiledamage));
        }

        public static PoisonGasShooter PoisonGasShooter(Vector3 Location, bool shooting, int shootlength, float shoottime, float stoptime)
        {
            return (new PoisonGasShooter(Location, shooting, shootlength, shoottime, stoptime));
        }

        public static Scarecrow Scarecrow(Vector3 Location)
        {
            return (new Scarecrow(Location));
        }

        public static Platform Platform(Vector3 Location, bool jumpthrough)
        {
            return (new Platform(Location, jumpthrough));
        }
        //End hazards
    }
}