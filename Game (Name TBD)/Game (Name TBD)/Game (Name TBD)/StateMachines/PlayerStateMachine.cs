﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A state machine to handle player "states"
    //This will handle whether a player can do an action given its current action, utilizing inclusive checks to make conditions as short as possible
    public class PlayerStateMachine
    {
        public enum State
        {
            Idle, Walk, Run,
            Attack, DashAttack, OffSpec, DefSpec,
            StartJump, Airborne, Landing, NJumpAtk, DJumpAtk, WalkJumpAtk,
            StabWeapon, SwingWeapon,
            Pickup,
            Grabbed, Hurt, KnockedDown,
            GrabIdle, GrabAttack, FThrow, BThrow, BGrapple, Vault,
            Swim,
            Victory
        };

        //Delegate definition and reference; this delegate is called during the state
        public delegate void StateUpdate();
        public StateUpdate UpdateState;

        //Current animation and state
        public NewAnimation Anim { get; private set; }
        public State CurState { get; private set; }

        //Previous state reference, animation, and delegate; useful for running jumps and other actions
        public State PrevState { get; private set; }
        private NewAnimation PrevAnim;
        private StateUpdate PrevDelegate;

        //Determines whether the player has control over the character or not; often set to false for level transitions
        //public bool PlayerHasControl;

        //Idle state and delegate; since Idle is the base state, many actions lead back to here - this is for easier transitions using less code
        private NewAnimation IdleAnim;
        private StateUpdate IdleDelegate;

        private PlayerStateMachine()
        {
            CurState = State.Idle;
            PrevState = State.Idle;
        }

        public PlayerStateMachine(NewAnimation idleanim, StateUpdate idledelegate) : this()
        {
            IdleAnim = idleanim;
            IdleDelegate = idledelegate;

            ChangeStateIdle();
            //PlayerHasControl = true;
        }

        public State GetPrevState
        {
            get { return PrevState; }
        }

        //NOTE: This needs to be in a method that we wrap other checks around, similar to BeatEmUpObj.StatusCondition(bool)
        /*public bool HasControl
        {
            get { return PlayerHasControl; }
        }*/

        public void ChangeState(State state, NewAnimation anim, StateUpdate updatestate, bool resetanim = true)
        {
            PrevState = CurState;
            PrevAnim = Anim;
            PrevDelegate = UpdateState;

            CurState = state;
            Anim = anim;
            if (Anim != null && resetanim == true) Anim.Reset();
            UpdateState = updatestate;
        }

        public void ChangeStateIdle()
        {
            ChangeState(State.Idle, IdleAnim, IdleDelegate);
        }

        public void ChangePrevState()
        {
            ChangeState(PrevState, PrevAnim, PrevDelegate);
        }

        public void Update()
        {
            //Default behavior; update the animation and go back to idle when it ends
            if (UpdateState == null)
            {
                if (Anim != null)
                    Anim.Update(Main.GetActiveTime);
                if (CurState != State.Idle && (Anim == null || Anim.IsAnimationEnd() == true))
                    ChangeStateIdle();
            }
            //Otherwise invoke the delegate
            else
            {
                UpdateState();
            }
        }

        //Gives the player control again
        /*public void GrantControl()
        {
            PlayerHasControl = true;
        }

        //Removes control from the player
        public void TakeControl()
        {
            PlayerHasControl = false;
        }*/

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, Texture2D spritesheet, bool facingright, Color statuscolor, float layer)
        {
            Anim.Draw(spriteBatch, spritesheet, Position, facingright, statuscolor, 0f, layer);
        }

        //Condition checks
        public bool CanWalk()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run || CurState == State.Swim);
        }

        public bool CanAttack()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run || CurState == State.Airborne || CurState == State.GrabIdle || CurState == State.Swim);
        }

        public bool CanJump()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run || CurState == State.GrabIdle || CurState == State.Swim);
        }

        //Default to Graham's
        public virtual bool CanOffSpec()
        {
            return (CurState == State.Airborne);
        }

        public bool CanDefSpec()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run || CurState == State.GrabIdle || CurState == State.Hurt);
        }

        public bool CanPickup()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run);
        }

        public bool CanThrowWeapon()
        {
            return (CurState == State.Idle || CurState == State.Walk || CurState == State.Run);
        }

        public bool CanGrab()
        {
            return (CurState == State.Walk || CurState == State.Run);
        }

        public bool CanThrow()
        {
            return (CurState == State.GrabIdle);
        }
    }
}
