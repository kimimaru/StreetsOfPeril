﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class handles the screens in the game, like the title screen, options screen, etc. - each instance is a new screen
    //NOTE: May add mouse support after playtesting; if so, you may need to create a class called Option that can be of different types (Switchable: modifies a value, Selectable: can select it to do something, etc.)
    public class Screen
    {
        //Delegates called when the screen is entered and exited; allows custom behavior per screen
        public delegate void ScreenEnter();
        public delegate void ScreenExit();

        public ScreenEnter EnterScreen;
        public ScreenExit ExitScreen;

        //Image used for the screen
        protected Texture2D ScreenGraphic;

        //Number of choices the player can choose from on the screen
        protected String[] Choices;
        protected Vector2[] ChoiceLocations;

        //Counter for choices
        protected int CurChoice;

        //For checking if buttons were pressed already
        protected KeyboardState KeyboardState;
        //protected MouseState MouseState;

        public Screen()
        {
            CurChoice = 0;

            EnterScreen = null;
            ExitScreen = null;

            ResetInput();
            CheckUnlocks();
        }

        //Checks a bool value and returns Yes or No depending on whether the value is true or false
        public static String YesNoOption(bool value)
        {
            return (value == true ? "Yes" : "No");
        }

        protected int LastChoice
        {
            get { return (Choices.Length - 1); }
        }

        //Called when the screen is pushed
        public void ScreenPushed()
        {
            if (EnterScreen != null)
                EnterScreen();
        }

        //Called when the screen is popped
        public void ScreenPopped()
        {
            if (ExitScreen != null)
                ExitScreen();
        }

        //Sets certain keys on the keyboard as held after going to or exiting from a screen so any held buttons do not do something on the screen you go or return to
        public void ResetInput()
        {
            //The buttons for each player
            List<Keys> Buttons = new List<Keys>();

            //Set the keys for all the players
            for (int i = (int)PlayerIndex.One; i <= (int)PlayerIndex.Four; i++)
            {
                Buttons.AddRange(Player.GetActionKeys(i));
            }

            //Add the buttons for accessing the Secret Screen
            Buttons.AddRange(new Keys[] { Keys.K, Keys.I, Keys.M } );

            KeyboardState = new KeyboardState(Buttons.ToArray());
            //MouseState = new MouseState(Mouse.GetState().X, Mouse.GetState().Y, Mouse.GetState().ScrollWheelValue, ButtonState.Pressed, Mouse.GetState().MiddleButton, Mouse.GetState().RightButton, Mouse.GetState().XButton1, Mouse.GetState().XButton2);
        }

        //For checking unlocks after going to or coming back from a screen
        public virtual void CheckUnlocks()
        {

        }

        protected void SetChoices(Vector2 firstchoiceloc, int locdiff)
        {
            ChoiceLocations[0] = firstchoiceloc;

            for (int i = 1; i < ChoiceLocations.Length; i++)
                ChoiceLocations[i] = new Vector2(ChoiceLocations[i - 1].X, ChoiceLocations[i - 1].Y + locdiff);
        }

        //Changes a value on a screen that should be within certain values
        protected void ChangeLimitValue(Keys keypressed, Keys keychecked, ref int valuechanged, int valuedifference, int highvalue, int lowvalue)
        {
            //Check if the value changed
            int didchange = valuechanged;

            //If the key pressed was the key you check for, add to the value, otherwise subtract
            if (keypressed == keychecked)
                valuechanged += valuechanged + valuedifference > highvalue ? 0 : valuedifference;
            else valuechanged -= valuechanged - valuedifference < lowvalue ? 0 : valuedifference;

            //If the value didn't change, don't play a sound
            if (didchange != valuechanged) LoadSounds.Play(LoadSounds.Choose);
        }

        //Changes a value on a screen that should be within certain values; this overload checks two boolean expressions
        protected void ChangeLimitValue(Keys keypressed, Keys keychecked, ref int valuechanged, int valuedifference, bool highcheck, bool lowcheck)
        {
            //Check if the value changed
            int didchange = valuechanged;

            //If the key pressed was the key you check for, add to the value, otherwise subtract
            if (keypressed == keychecked)
                valuechanged += (highcheck == true) ? 0 : valuedifference;
            else valuechanged -= (lowcheck == true) ? 0 : valuedifference;

            //If the value didn't change, don't play a sound
            if (didchange != valuechanged) LoadSounds.Play(LoadSounds.Choose);
        }

        //Updates different screens that have the same information
        protected virtual void UpdateChanges(float activeTime, Main main)
        {
            
        }

        //Changes a particular setting that has more than one value with the left or right keys
        protected virtual void SwitchOption(float activeTime, Keys keypressed, Main main)
        {
            
        }

        //Decides what to do when the player chooses an option based on which option was chosen
        protected virtual void ChooseOption(float activeTime, Main main)
        {
            
        }

        //Cycles through the options available on the screen
        protected virtual void ChangeOptions(float activeTime, Main main)
        {
            if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
            {
                CurChoice--;
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
            {
                CurChoice++;
                LoadSounds.Play(LoadSounds.Choose);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
            {
                SwitchOption(activeTime, Keys.Left, main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
            {
                SwitchOption(activeTime, Keys.Right, main);
            }
            else if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
            {
                ChooseOption(activeTime, main);
            }

            if (CurChoice < 0)
                CurChoice = Choices.Length - 1;
            else if (CurChoice >= Choices.Length)
                CurChoice = 0;
        }

        protected void DrawChoices(SpriteBatch spriteBatch, float depth = .4f)
        {
            for (int i = 0; i < Choices.Length; i++)
            {
                Color SelectColor = CurChoice == i ? Color.Green : Color.Black;
                spriteBatch.DrawString(LoadGraphics.HUDFont, Choices[i], ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, depth);
            }
            spriteBatch.Draw(LoadGraphics.ScreenSelect, new Vector2(ChoiceLocations[CurChoice].X - 38, ChoiceLocations[CurChoice].Y - 2), null, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, depth);
        }

        protected void DrawChoices(SpriteBatch spriteBatch, int[] unlockedindexes, params bool[] unlocked)
        {
            int unlockindex = 0;
            for (int i = 0; i < Choices.Length; i++)
            {
                Color SelectColor = CurChoice == i ? Color.Green : Color.Black;
                String newstring = Choices[i];

                if (unlockindex < unlockedindexes.Length && i == unlockedindexes[unlockindex])
                {
                    if (unlocked[unlockindex] == false)
                        newstring = "???";
                    unlockindex++;
                }

                spriteBatch.DrawString(LoadGraphics.HUDFont, newstring, ChoiceLocations[i], SelectColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
            }
            spriteBatch.Draw(LoadGraphics.ScreenSelect, new Vector2(ChoiceLocations[CurChoice].X - 38, ChoiceLocations[CurChoice].Y - 2), null, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.4f);
        }

        public virtual void Update(Main main, float activeTime)
        {
            UpdateChanges(activeTime, main);
            ChangeOptions(activeTime, main);

            //Make sure you can't keep holding a button and move through/select options
            KeyboardState = Keyboard.GetState();
            //MouseState = Mouse.GetState();
        }

        public virtual void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ScreenGraphic, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
            DrawChoices(spriteBatch);
        }
    }
}
