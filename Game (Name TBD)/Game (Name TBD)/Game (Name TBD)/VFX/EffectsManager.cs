﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Manages Effects in a SubLevel; the effects can be SFX, VFX, or other types of effects
    //All effects created by individual BeatEmUpObjs are sent here for management
    public class EffectsManager
    {
        private List<VisualEffect> Effects;

        public EffectsManager()
        {
            Effects = new List<VisualEffect>();
        }

        public void AddVisualEffect(BeatEmUpObj beatemupobj, Texture2D vfxsheet, Color vfxcolor, params AnimFrame[] frames)
        {
            VisualEffect effect = new VisualEffect(beatemupobj, new NewAnimation(vfxsheet, frames), vfxcolor);
            Effects.Add(effect);
        }

        //Removes all effects
        public void ClearEffects()
        {
            Effects.Clear();
        }

        public void Update(SubLevel level)
        {
            for (int i = 0; i < Effects.Count; i++)
            {
                Effects[i].Update(level);
                if (Effects[i].Finished == true)
                {
                    Effects.RemoveAt(i);
                    i--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, SubLevel level)
        {
            for (int i = 0; i < Effects.Count; i++)
                Effects[i].Draw(spriteBatch, level);
        }
    }
}
