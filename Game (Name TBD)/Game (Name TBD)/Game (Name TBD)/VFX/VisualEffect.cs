﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A VisualEffect for the EffectManager
    public class VisualEffect
    {
        //BeatEmUpObj reference; if this isn't null, the Effect is drawn here
        private BeatEmUpObj BeatEmUpObject;

        //The animation to draw; always pass a SpriteSheet in to this one
        private NewAnimation Anim;

        //The location to draw the Effect if BeatEmUpObject is null
        private Vector3 Location;

        //The Color to draw the Effect with
        private Color EffectColor;

        private VisualEffect()
        {
            BeatEmUpObject = null;
            Anim = null;
            Location = Vector3.Zero;

            EffectColor = Color.White;
        }

        //Constructor; the animation passed in should not be null
        public VisualEffect(NewAnimation anim, Vector3 location, Color effectcolor) : this()
        {
            BeatEmUpObject = null;
            Anim = anim;
            Anim.Reset();
            Location = location;

            EffectColor = effectcolor;
        }

        public VisualEffect(BeatEmUpObj beatemupobj, NewAnimation anim, Color effectcolor) : this(anim, Vector3.Zero, effectcolor)
        {
            BeatEmUpObject = beatemupobj;
        }

        //Tells if the Effect is finished or not
        public bool Finished
        {
            get { return (Anim.IsAnimationEnd() == true); }
        }

        public void Update(SubLevel level)
        {
            //Update the animation
            if (Anim.IsAnimationEnd() == false)
                Anim.Update(Main.GetActiveTime);
        }

        public void Draw(SpriteBatch spriteBatch, SubLevel level)
        {
            //Default to the Effect's Location
            Vector2 drawloc = new Vector2(Location.X, Location.Y - Location.Z);
            float depth = BeatEmUpObj.GetObjectDrawDepth(Location.Y, level.GCameraOffSet);

            //If the effect is attached to a BeatEmUpObj, draw the Effect at the location of the BeatEmUpObj, using its depth as well
            if (BeatEmUpObject != null)
            {
                drawloc = new Vector2(BeatEmUpObject.GetLocationHeight.X, BeatEmUpObject.GetLocationHeight.Y - BeatEmUpObject.GetLocationHeight.Z);
                depth = BeatEmUpObject.GetDrawDepth(level.GCameraOffSet);
            }

            //Add the camera location after everything else
            drawloc += level.GCameraOffSet;

            //Draw the animation
            Anim.Draw(spriteBatch, drawloc, true, EffectColor, 0f, depth);
        }
    }
}
