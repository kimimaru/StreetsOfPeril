﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Health Juice - An item that, upon pick up, gives a player or enemy a temporary new bar of health
    public sealed class HealthJuice : Item
    {
        public HealthJuice(Vector2 location)
        {
            Location = new Vector3(location, 0f);

            Name = "Health Juice";
        }

        protected sealed override void PickedUp()
        {
            //Grant the Fighter a Temporary Health Bar
            fighter.GrantTempHealthBar();
        }
    }
}
