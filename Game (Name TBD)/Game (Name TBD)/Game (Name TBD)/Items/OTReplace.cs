﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A replacement oxygen tank that is found in item containers in underwater levels; it basically heals your current oxygen tank
    //It is possible that this may go unused depending on how much it may be needed in underwater levels
    //However, chances are it will at least be used in that challenge mode where your only health is oxygen tank health
    public sealed class OTReplace : Item
    {
        public OTReplace(Vector2 location)
        {
            SpriteSheet = LoadGraphics.ReplacementTank;
            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            Location = new Vector3(location, 0f);

            Name = "Replacement Tank";

            SetUpHurtbox();
        }

        protected sealed override void PickedUp()
        {
            //Heal the Fighter's Oxygen Tank
            fighter.OxygenTank.HealTank();
        }
    }
}
