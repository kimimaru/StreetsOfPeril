﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Health Candy - An item that rapidly restores a total of 50 HP; a new class is needed to prevent the sound from playing and the HUD from drawing how many times left it'll heal
    public sealed class HealthCandy : Item
    {
        public HealthCandy()
        {
            Location = Vector3.Zero;

            Name = "Health Candy";
            Health = 1;
            HRestoreDur = 200f;
            PrevHRestoreDur = 0f;
            TimesHeal = 50;
            Score = 0;
        }

        public sealed override void GetPickedUp(Fighter fight)
        {
            base.GetPickedUp(fight);

            //Set the heal sound to null so it doesn't rapidly play it
            HealSound = null;
        }
    }
}
