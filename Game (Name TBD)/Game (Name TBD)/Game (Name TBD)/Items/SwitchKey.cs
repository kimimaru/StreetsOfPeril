﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A switch key in the Versus mode, Avoid Enemy
    public sealed class SwitchKey : Item
    {
        public SwitchKey(Vector2 location)
        {
            Location = new Vector3(location, 0f);

            Name = "Switch Key";

            Score = 1;
        }
    }
}
