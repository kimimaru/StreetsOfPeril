﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //The Bonus Token - A special item that grants access to a bonus stage at the end of a level
    public sealed class BToken : Item
    {
        public BToken(Vector2 velocity, Vector2 location)
        {
            Velocity = velocity;
            Location = new Vector3(location, 0f);

            Name = "Bonus Token";

            HealSound = LoadSounds.GetLife;
        }

        protected override void PickedUp()
        {
            Level.StartBonus = true;
        }
    }
}
