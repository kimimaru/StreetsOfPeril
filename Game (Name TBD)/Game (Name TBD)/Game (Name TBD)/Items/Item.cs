﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    public class Item : PickupObj
    {
        //The Item's sprite
        protected AnimFrame ItemSprite;

        //The distance offscreen before the Item is considered dead; this is changed in the GrabItem bonus stage
        protected Vector2 OffScreenDist;

        //The number of Lives this Item grants to Players; this is an int for flexibility, as the only item that grants lives, the Graham Doll, grants only one life
        protected int LivesGranted;

        //The amount of time in between each heal
        protected float HRestoreDur;
        protected float PrevHRestoreDur;

        //The amount of health the item heals each time it heals
        protected int HealthHealed;

        //The max and current number of times the Item heals, respectively
        protected int MaxTimesHeal;
        protected int TimesHeal;

        //The sound to play when the item heals the Fighter; it defaults to the healing sound
        protected SoundEffect HealSound;

        protected Item()
        {
            ObjType = ObjectType.Item;
            StatForever = true;

            //The default value of the offscreen distance
            OffScreenDist = (Main.ScreenSize * 2);

            //Default icon and spritesheets; default to Rice Bowl
            Icon = LoadGraphics.ItemIcon;
            SpriteSheet = LoadGraphics.ItemSprite;
            ItemSprite = new AnimFrame(new Rectangle(0, 0, SpriteSheet.Width, SpriteSheet.Height), 0f);
            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            //Items cannot hit anything, so they have no need for hitboxes
            Hitboxes = null;

            //All items heal at least 1 time so they can activate properly
            MaxTimesHeal = 1;
            TimesHeal = 1;

            //Default to the healing sound
            HealSound = LoadSounds.Restore;

            //Set up the Item's hurtbox and health to default values
            SetUpHurtbox();
            SetUpHealth(0);

            hud = new HUD(this, Vector2.Zero);
        }

        //Constructor - pass in a new status with the second constructor if you want an item to change your status, otherwise use the first constructor
        public Item(Texture2D sprite, String name, int healthrestore, float hrestoredur, int timesheal, bool givelife, int scoreadd, Status stat, Vector2 velocity, Vector2 location) : this()
        {
            Velocity = velocity;
            
            Icon = LoadGraphics.ItemIcon;
            SpriteSheet = sprite;
            ItemSprite = new AnimFrame(new Rectangle(0, 0, SpriteSheet.Width, SpriteSheet.Height), 0f);
            ObjectLength = SpriteSheet.Width;
            ObjectHeight = SpriteSheet.Height;

            SetUpHurtbox();

            Location = new Vector3(location, 0f);

            if (givelife == true)
            {
                LivesGranted = 1;
                HealSound = LoadSounds.GetLife;
            }

            status = stat;
            Name = name;

            HealthHealed = healthrestore;

            MaxTimesHeal = Helper.Clamp(timesheal, 1, 100);
            TimesHeal = MaxTimesHeal;

            HRestoreDur = hrestoredur;
            PrevHRestoreDur = 0f;

            SetUpHealth(HealthHealed * MaxTimesHeal);

            Score = scoreadd;
        }

        //Copy constructor for Onslaught
        public Item(Item olditem) : this()
        {
            this.Location = olditem.Location;
            this.Velocity = olditem.Velocity;

            this.Name = olditem.Name;
            this.SpriteSheet = olditem.SpriteSheet;
            this.Icon = olditem.Icon;
            this.ItemSprite = olditem.ItemSprite;
            this.ObjectLength = olditem.ObjectLength;
            this.ObjectHeight = olditem.ObjectHeight;

            this.SetUpHurtbox();

            this.GravityValue = olditem.GravityValue;
            this.ObjectTile = olditem.ObjectTile;

            this.LivesGranted = olditem.LivesGranted;
            this.HealSound = olditem.HealSound;

            this.HealthHealed = olditem.HealthHealed;

            this.MaxTimesHeal = olditem.MaxTimesHeal;
            this.TimesHeal = this.MaxTimesHeal;

            this.SetUpHealth(this.HealthHealed * this.MaxTimesHeal);
            this.HRestoreDur = olditem.HRestoreDur;
            this.PrevHRestoreDur = 0f;

            this.Score = olditem.Score;

            this.status = new Status(olditem.status.GCond, olditem.status.GStatusDur);
        }

        //The possible statuses a Biscuit can have
        public static Status[] BiscuitStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.SpeedBoost, 10000f, 0) }; }
        }

        //The possible statuses a Turkey can have
        public static Status[] TurkeyStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DefenseBoost, 12000f, 0), new Status((int)Status.Statuses.DamageBoost, 12000f, 0) }; }
        }

        //The possible statuses a Mystery Pudding can have
        public static Status[] MysteryPuddingStatuses
        {
            get { return new Status[] { new Status((int)Status.Statuses.DamageBoost, 5000f, 0), new Status((int)Status.Statuses.DamageDown, 5000f, 0), new Status((int)Status.Statuses.DefenseBoost, 5000f, 0), new Status((int)Status.Statuses.DefenseDown, 5000f, 0), new Status((int)Status.Statuses.SpeedBoost, 5000f, 0), new Status((int)Status.Statuses.SpeedDown, 5000f, 0), new Status((int)Status.Statuses.StunDown, 5000f, 0), new Status((int)Status.Statuses.Poison, 5000f, 0), new Status((int)Status.Statuses.NoJump, 5000f, 0), new Status((int)Status.Statuses.NoAttack, 5000f, 0), new Status((int)Status.Statuses.NoGrab, 5000f, 0), new Status((int)Status.Statuses.NoSpecial, 5000f, 0), new Status((int)Status.Statuses.Invincible, 5000f, 0) }; }
        }

        //Gets the amount of health that the item heals each time it heals
        public int GetHealthHeal
        {
            get { return HealthHealed; }
        }

        //Gets the remaining number of times the item heals
        public int GetTimesHeal
        {
            get { return TimesHeal; }
        }

        public sealed override bool IsDead
        {
            get { return (TimesHeal <= 0); }
        }

        //Check if the item is already healing a player or enemy
        public bool IsHealing(Player player, Enemy enemy)
        {
            return (HealthHealed != 0 && ((player != null && player == (fighter as Player)) || (enemy != null && enemy == (fighter as Enemy))));
        }

        //Disables an item if it heals over time, is currently healing a fighter, and the fighter or item died or the fighter picked up another item HoT item
        public void DisableHoT()
        {
            TimesHeal = 0;
            HealthHealed = 0;
        }

        //Checks if an item heals over time
        public bool HealsOverTime()
        {
            return (HealthHealed != 0);
        }

        //Sets the distance that the item dies at when being offscreen
        public void SetOffScreenDist(Vector2 offscreendist)
        {
            OffScreenDist = offscreendist;
        }

        protected sealed override void ObjectUpdate()
        {
            if (IsPickedUp == false)
            {
                //If the Item has a velocity, make it move
                if (Velocity != Vector2.Zero)
                    ObjectMove(Velocity);

                //Check if the Item is offscreen; if it is and hasn't been picked up yet, remove it
                if (IsDead == false && IsOffScreenRange(SubLvl.GCameraOffSet, (int)OffScreenDist.X, (int)OffScreenDist.Y) == true)
                {
                    Die();

                    //Don't draw the Item if it died from being too far offscreen
                    DrawEnabled = false;
                }
            }
            else if (IsDead == false)
            {
                //If the Item has a Fighter and should heal it over time, do so
                if (ShouldHealFighter() == true)
                {
                    HealFighter();
                }
            }
        }

        public override void Die()
        {
            //base.Die();

            //Restart the object's HUD so it can get removed properly, if it depends on the HUD being inactive
            hud.Restart();

            //Set the number of times the Item heals to 0, marking it as dead
            TimesHeal = 0;
         
            //If this Item heals over time and is the Fighter's current HoT Item, clear the Fighter's HoT Item
            if (HealsOverTime() == true && IsPickedUp == true && fighter.HoTItem == this) fighter.ClearHoTItem();
        }
        
        public override void GetPickedUp(Fighter fight)
        {
            base.GetPickedUp(fight);

            //Do whatever the item should do the instant it gets picked up
            PickedUp();

            //Heal the Fighter
            HealFighter();
        }

        //Item-specific implementation for when the item is picked up; the default behavior is to inflict a Fighter with a Status, set its HoT item, and more
        protected virtual void PickedUp()
        {
            //If this item heals over time, set the Fighter's HoT Item
            if (HealsOverTime() == true)
                fighter.SetHoTItem(this);

            //If the Item's Status isn't None, inflict the Fighter with it
            if (status != (int)Status.Statuses.None) fighter.InflictStatus(status);

            //If the Fighter is a Player, check for more things
            if (fighter.ObjType == ObjectType.Player)
            {
                Player player = (Player)fighter;

                //If the Item has Score value and Life values, add them to the Player's Score and Lives, respectively
                if (Score != 0) player.AddScore(Score);
                if (LivesGranted != 0) player.AddLives(LivesGranted);
            }
        }

        private bool ShouldHealFighter()
        {
            return (TimesHeal > 0 && Main.GetActiveTime >= PrevHRestoreDur);
        }

        protected void HealFighter()
        {
            //If the Fighter is a Player, set the HUD underneath the Player to this item's HUD
            fighter.SetInteractionHUD(hud);
            hud.Restart();

            //Heal the fighter and subtract health from the Item
            if (HealthHealed > 0)
            {
                fighter.Heal(GetHealthHeal);
                LoseHealth(GetHealthHeal);

                //Reset the timer for healing the Fighter again
                PrevHRestoreDur = Main.GetActiveTime + HRestoreDur;
            }
         
            //Subtract from the number of times remaining for the Item to heal
            TimesHeal--;
         
            //Play the heal sound
            LoadSounds.Play(LoadSounds.Restore);
         
            //If the Item is no longer healing the Fighter, clear the Fighter's HoT Item reference
            if (TimesHeal == 0) Die();
        }

        protected sealed override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            if (IsPickedUp == false && IsDead == false)
                ItemSprite.Draw(spriteBatch, SpriteSheet, GetDrawLoc(cameraloc), FacingRight, status.GStatusColor, GetRotation, GetDrawDepth(cameraloc));
        }

        protected sealed override bool ShouldDrawShadow()
        {
            return (base.ShouldDrawShadow() == true && IsPickedUp == false);
        }

        public sealed override bool ShouldDrawDeadIcon()
        {
            //Never draw the dead icon for Items
            return false;
        }

        public override string ToString()
        {
            return (Name + "," + status.ToString());
        }
    }
}