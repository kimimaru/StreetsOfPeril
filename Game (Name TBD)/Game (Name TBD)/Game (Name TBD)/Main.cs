using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;

namespace Game__Name_TBD_
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public sealed class Main : Microsoft.Xna.Framework.Game
    {
        //Graphics device and spritebatch for drawing
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        //The default number of frames per second, or the rate the game runs
        public const float FPS = 60f;

        //The time the game is active (not paused)
        private static float activeTime;

        //Enumeration of screen numbers (for easier use if the screen numbers are rearranged or changed)
        public enum ScreenRef { Title, Options, Pause, PQuit, POptions, CharSelect, Challenges, Survival, Onslaught, OnslaughtPrep, RPS, BTO, Snipe, Data, Controls, Sound, Unlock, Extra, Rank, Results, Versus, Secret };

        //Enumeration of difficulty levels
        public enum DifficultyLevel
        {
            VeryEasy, Easy, Normal, Hard, VeryHard
        };

        //Enumeration of screen resolutions
        public enum ScreenResolution
        {
            Normal, Double, Full
        };

        //The game state; tells which game mode you're in
        public enum GameState
        {
            Screen, StartUp, InGame, InRank, InBonus, InChallenge, LevelEditor
        };

        //The in-game state; paused or unpaused
        public enum InGameState
        {
            UnPaused, Paused
        };

        //The game screens - the one at the top of the stack is the screen that's currently up
        private Stack<Screen> Screens;

        //The game's difficulty - starts out at Easy
        public static int Difficulty;

        //The setting determining if players can grab enemies just by running up to them or not - true by default
        public static bool AutoGrab;

        //The setting determining if players can hit each other or not - false by default
        public static bool PlayerDamage;

        //The setting determining if the game will pause when it loses focus (player clicks off the window or minimizes it) - true by default
        public static bool LoseFocusPause;

        //For exiting the game from the title screen
        private bool ExitGame;

        //Developer tools
        private LevelEditor Editor;

        //The current FPS of the game
        public static float CurrentFPS;

        //The size of the game screen
        public static readonly Vector2 ScreenSize;

        //Checks if the game has been completed (may be redundant since Rank Mode is unlocked when the game is completed so you can just check for that instead; keep this here just in case you'll need it)
        //public static bool GameComplete;

        //The current game mode and whether the game is paused or not
        public static GameState GameMode;
        public static InGameState Paused;

        //For the start up screen
        private float StartUpColor;

        //The levels in the game
        private Level Level;
        private int CurrentLevel;
        private int MaxLevels;

        //Bonus stages
        private Queue<BStage> Bonuses;

        //Challenge levels
        private List<CLevel> Challenges;
        private int CurrentChallenge;

        //Cutscenes in the game
        private Stack<Cutscene> Cutscenes;

        //The list of players in the game
        private List<Player> Players;

        //Check if enter was pressed - for skipping the intro, skipping cutscenes, and pausing the game
        private KeyboardState KeyboardState;

        public Main()
        {
            Content.RootDirectory = "SoPLibraryContent";
            graphics = new GraphicsDeviceManager(this);

            //Backbuffer and full screen - the screen size is 416x320 and fullscreen is an option that can be toggled on or off in the Options screen
            graphics.PreferredBackBufferWidth = (int)ScreenSize.X;
            graphics.PreferredBackBufferHeight = (int)ScreenSize.Y;
            ChangeScreenResolution(Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "Resolution", 0));

            ExitGame = false;
            StartUpColor = 0f;
            Bonuses = new Queue<BStage>();
            Challenges = new List<CLevel>();
            Cutscenes = new Stack<Cutscene>();
            Screens = new Stack<Screen>();
            Players = new List<Player>();
            CurrentLevel = 0;
            MaxLevels = 3;
            CurrentChallenge = 0;
            Player.Continues = int.MaxValue;//1;
            KeyboardState = new KeyboardState(Keys.Enter);
        }

        static Main()
        {
            CurrentFPS = FPS;
            ScreenSize = new Vector2(416, 320);

            activeTime = 0f;

            //Load the game's difficulty from the save file
            Difficulty = Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "Difficulty", 0);

            //Autograb is a little debatable; at least its still in here though
            /*AutoGrab = Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "Autograb", true);
            
            object autograb = SaveLoadData.LoadData("Settings", "Gameplay", "Autograb");
            if (autograb == null) AutoGrab = true;
            else AutoGrab = Convert.ToBoolean(autograb);*/
            AutoGrab = true;
            PlayerDamage = false;

            //The default value of a bool is false, so to default pausing while losing focus to true we need to check if the saved setting exists first and if not set it to true
            LoseFocusPause = Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "LoseFocus", true);

            GameMode = GameState.StartUp;
            Paused = InGameState.UnPaused;
        }

        public static bool CheckState(GameState gamestate)
        {
            return (GameMode == gamestate);
        }

        public static void SetState(GameState gamestate)
        {
            GameMode = gamestate;
        }

        public static bool IsPaused
        {
            get { return (Paused == InGameState.Paused); }
        }

        public static void PauseGame()
        {
            Paused = InGameState.Paused;
        }

        public static void UnPauseGame()
        {
            Paused = InGameState.UnPaused;
        }

        //Initializes materials before the game runs
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //SaveLoadData.LoadRanks();

            //Load challenges
            for (int i = 0; i < CLevel.ChallengeRanks.Length; i++)
            {
                for (int j = 0; j < CLevel.ChallengeRanks[i].Length; j++)
                {
                    SaveLoadData.LoadChallengeRanking(CLevel.ChallengeRanks[i][j], i, j);
                }
            }
            //SaveLoadData.LoadChallenges();

            //Only for the level editor
            this.IsMouseVisible = true;

            base.Initialize();
        }

        //Loads all content at the start
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            LoadGraphics.LoadContent(Content);
            LoadSounds.LoadContent(Content);

            AddScreen((int)ScreenRef.Title);

            /* Optimally, 1 player per computer, so the others will be through LAN or online
             * 4 players maximum - maybe even controller support*/
            Cutscenes.Push(new Cutscene(LoadGraphics.Cut1, LoadGraphics.Text1, new List<Vector2>() { new Vector2(150, 0), new Vector2(104, 66), new Vector2(104, 66) }, new List<Vector2>() { new Vector2(156, 186), new Vector2(156, 186), new Vector2(156, 186) },
                new List<float>() { 5000, 4000, 4000, 4000, 3000, 4000}, new List<float>() { 5000, 5000, 4000, 3000, 3000, 3000}));

            Editor = new LevelEditor();
        }

        //Pause the game if it loses focus
        protected override void OnDeactivated(object sender, EventArgs args)
        {
            //Check if the setting to pause the game when losing focus is enabled and whether the game is already paused or not
            if (LoseFocusPause == true && Paused == InGameState.UnPaused)
            {
                //If not, check if the players are in a game mode and can pause the game, and pause the game if so
                if (((GameMode == GameState.InGame || GameMode == GameState.InRank) && Level.CanPause() == true) || (GameMode == GameState.InChallenge && Challenges[CurrentChallenge].CanPause() == true) || (GameMode == GameState.InBonus && Bonuses.Peek().CanPause() == true))
                {
                    MediaPlayer.Pause();
                    AddScreen((int)ScreenRef.Pause);
                    Paused = InGameState.Paused;
                }
            }
        }

        //Do stuff when the player exits the game 
        protected override void OnExiting(object sender, EventArgs args)
        {
            //Do things if the player exited the game while in Story mode
            /*if (InGame == true)
            {
                //If the player exited after using the autosave, erase the autosave
                if (Level.AutoSaved == true)
                {
                    SaveLoadData.EraseAutosave();
                }
            }*/
        }

        //Changes the speed of the game; it's only current use is to slow down the game for a little bit after defeating a boss
        public static void ChangeGameSpeed(float newspeed)
        {
            CurrentFPS = newspeed;
        }

        //Resets the current game speed to the default FPS
        public static void ResetGameSpeed()
        {
            Enemy.PrevBossTimer = 0f;
            CurrentFPS = FPS;
        }

        //How much game time passes in one frame
        //Scale the elapsed game time depending on the current FPS of the game, since all of the timing is based off 60 FPS
        private static float ElapsedTimeOneFrame(GameTime gameTime)
        {
            return ((float)Math.Ceiling(gameTime.ElapsedGameTime.TotalMilliseconds * (CurrentFPS / FPS)));
        }

        //Finds how many frames would be in an amount of milliseconds
        private static int FramesInMilliseconds(GameTime gameTime, float milliseconds)
        {
            return (int)(milliseconds / ElapsedTimeOneFrame(gameTime));
        }

        //Saves data for the autosave
        //NOTE: Also allow up to 3 autosaves per playthrough; this is much more reasonable and forgiving!
        //NOTE: Store which and how many Bonus Stages the players were supposed to have before they quit the game; restart them on the first Bonus Stage after they continue their game
        //NOTE: Maybe store how long each playthrough is (including autosaves) so players have a better estimate for how long it takes to complete the game; it's also great for speedrunners!
        public void SaveAutosave()
        {
            //Store the level the player(s) is/are on
            SaveLoadData.SaveData("Autosave", "Level", "Saved", Level.GetLevelNum);

            //Store the amount of times the player(s) autosaved
            SaveLoadData.SaveData("Autosave", "Autosaves", "Uses", Level.AutoSaved);

            //Store the character(s) and stats like Lives, Score, and Continues (this is a global player value)
            for (int i = 0; i < Players.Count; i++)
            {
                SaveLoadData.SaveData("Autosave", "Player" + i, "Character", Player.IndexOfCharacter(Players[i]));
                SaveLoadData.SaveData("Autosave", "Player" + i, "Lives", Players[i].GetLives);
                SaveLoadData.SaveData("Autosave", "Player" + i, "Score", Players[i].Score);
                SaveLoadData.SaveData("Autosave", "Player" + i, "PlayerNum", Players[i].GPlayerNum);
                SaveLoadData.SaveData("Autosave", "Player" + i, "Costume", Players[i].Alternate);
            }

            //Store the bonus stages the players were on
            if (Bonuses.Count > 0)
            {
                BStage[] bonuses = new BStage[Bonuses.Count]; 
                Bonuses.CopyTo(bonuses, 0);

                for (int i = 0; i < bonuses.Length; i++)
                {
                    SaveLoadData.SaveData("Autosave", "Bonuses" + i, "Name", bonuses[i].GetType().AssemblyQualifiedName);
                }
            }
        }

        //Loads data from the autosave and tells if it was successful, depending on whether the data was valid or not
        public bool LoadAutosave()
        {
            object autosaved = SaveLoadData.LoadData("Autosave", "Level", "Saved");
            int levelsaved = Convert.ToInt32((String)autosaved);

            object autosaveuses = SaveLoadData.LoadData("Autosave", "Autosaves", "Uses");

            //Load the level the player(s) is/are on
            if (autosaved != null && autosaveuses != null && levelsaved > 0)
            {
                //The level is valid, so now load in the players and check if their data is valid
                for (int i = 0; i <= Players.Count; i++)
                {
                    object playernum = SaveLoadData.LoadData("Autosave", "Player" + i, "PlayerNum");

                    //If the player number is valid, then start converting the rest
                    if (playernum != null)
                    {
                        object character = SaveLoadData.LoadData("Autosave", "Player" + i, "Character");
                        object charactercostume = SaveLoadData.LoadData("Autosave", "Player" + i, "Costume");

                        //If the character and its costume is valid, add the character to the player list after finding out the rest of the information
                        if (character != null && charactercostume != null)
                        {
                            //Get the character's lives
                            object lives = SaveLoadData.LoadData("Autosave", "Player" + i, "Lives");
                            if (lives != null)
                            {
                                object score = SaveLoadData.LoadData("Autosave", "Player" + i, "Score");
                                if (score != null)
                                {
                                    int characternum = Convert.ToInt32((String)character);
                                    int charcostume = Convert.ToInt32((String)charactercostume);

                                    //If everything is good, add the character to the list and set the character's lives and score
                                    if (characternum == 0) Players.Add(CreateObjects.Graham(Convert.ToInt32((String)playernum), charcostume));
                                    else if (characternum == 1) Players.Add(CreateObjects.Wil(Convert.ToInt32((String)playernum), charcostume));
                                    else if (characternum == 2) Players.Add(CreateObjects.Crystal(Convert.ToInt32((String)playernum), charcostume));
                                    else Players.Add(CreateObjects.Jeff(Convert.ToInt32((String)playernum), charcostume));

                                    Players[Players.Count - 1].SetStats(null, null, Convert.ToInt32((String)lives), null, null, null);
                                    Players[Players.Count - 1].SetScore(Convert.ToInt32((String)score));
                                }
                                else return false;
                            }
                            else return false;
                        }
                    }
                }

                //Load the bonuses
                for (int i = 0; i <= Bonuses.Count; i++)
                {
                    object bonus = SaveLoadData.LoadData("Autosave", "Bonuses" + i, "Name");

                    if (bonus != null)
                    {
                        //NOTE: Just check the name of the type of the bonus stage using a switch instead of doing this...
                        Type bonustype = Type.GetType((String)bonus);
                        System.Reflection.ConstructorInfo bonusconstructor = bonustype.GetConstructor(new Type[] { typeof(List<Player>) });
                        Bonuses.Enqueue((BStage)bonusconstructor.Invoke(new object[] { Players }));
                    }
                }

                //If no players were loaded, the data wasn't valid
                if (Players.Count == 0) return false;
                else
                {
                    CurrentLevel = levelsaved;
                    return true;
                }
            }

            return false;
        }

        //Returns half of the screen total screen size
        public static Vector2 ScreenHalf
        {
            get { return new Vector2(Main.ScreenSize.X / 2, Main.ScreenSize.Y / 2); }
        }

        //Returns the total active game time (time the game isn't paused)
        public static float GetActiveTime
        {
            get { return activeTime; }
        }

        public void GameExit()
        {
            ExitGame = true;
        }

        //Gets the current screen
        public Screen CurScreen()
        {
            //Trying to return the top element when there are none will result in a runtime error, so we have to make sure there are elements in the stack first
            if (Screens.Count > 0) return Screens.Peek();
            else return null;
        }

        //Pushes a new screen onto the stack; there is also an option to have a specific player control the screen in question
        public void AddScreen(int screennum, int playernum = 0, int playerindex = 0)
        {
            switch (screennum)
            {
                case (int)ScreenRef.Title: 
                    Screens.Push(new TitleScreen(30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y - 60), "Play", "Challenges", "Extra Modes", "Data", "Options", "Exit"));
                    break;
                case (int)ScreenRef.Options:
                    //Create a different options screen for the pause menu that omits difficulty, player damage, and the sound test
                    if (IsPaused == true) Screens.Push(new OptionsScreen(30, new Vector2(ScreenHalf.X - 60, (ScreenHalf.Y / 1.5f) + 50), "Music Volume: " + Convert.ToString(Math.Round(MediaPlayer.Volume, 1) * 10), "Sound Volume: " + Convert.ToString(Math.Round(LoadSounds.SoundVolume, 1) * 10), "Controls", "Exit"));
                    else Screens.Push(new OptionsScreen(25, new Vector2(ScreenHalf.X - 60, (ScreenHalf.Y / 1.5f) - 25), "Music Volume: " + Convert.ToString(Math.Round(MediaPlayer.Volume, 1) * 10), "Sound Volume: " + Convert.ToString(Math.Round(LoadSounds.SoundVolume, 1) * 10), "Difficulty: " + OptionsScreen.DisplayDifficulty(), "Player Damage: " + Screen.YesNoOption(PlayerDamage), "Pause Losing Focus: " + Screen.YesNoOption(LoseFocusPause), "Resolution: " + Enum.GetName(typeof(Main.ScreenResolution), Helper.LoadFromSaveElseDefault("Settings", "Gameplay", "Resolution", 0)), "Controls", "Sound Test", "Unlockables", "Exit"));
                    break;
                case (int)ScreenRef.Pause:
                    //Create a different pause screen depending on which game mode the player is on; Autosave for Story, Restart for challenge modes that support it, and the normal options for everything else
                    if (GameMode == GameState.InGame) Screens.Push(new PauseScreen(playernum, playerindex, 30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y), "Continue", "Options", "Autosave: " + Level.AutoSaved, "Player Settings", "Quit"));
                    else if (GameMode == GameState.InChallenge && Challenges[CurrentChallenge].CanRestart == true) Screens.Push(new PauseScreen(playernum, playerindex, 30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y), "Continue", "Restart", "Options", "Quit"));
                    else Screens.Push(new PauseScreen(playernum, playerindex, 30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y), "Continue", "Options", "Quit"));
                    break;
                case (int)ScreenRef.PQuit:
                    Screens.Push(new PQuitScreen(playernum, playerindex, 30, new Vector2(195, 186), "No", "Yes"));
                    break;
                case (int)ScreenRef.POptions:
                    Screens.Push(new PlayerOptionsScreen(playernum, playerindex, Level, Players));
                    break;
                case (int)ScreenRef.CharSelect:
                    Screens.Push(new CharSelectScreen(4));
                    break;
                case (int)ScreenRef.Challenges:
                    Screens.Push(new ChallengesScreen(30, new Vector2(ScreenHalf.X - 60, 130), "Survival", "Onslaught", "Break The Objects", "Snipe", "RPS", "Exit"));
                    break;
                case (int)ScreenRef.Survival:
                    Screens.Push(new SurvivalScreen(30, new Vector2(65, 213), "Play", "Back"));
                    break;
                case (int)ScreenRef.Onslaught:
                    Screens.Push(new OnslaughtScreen(30, new Vector2(65, 213), "Play", "Back"));
                    break;
                case (int)ScreenRef.OnslaughtPrep:
                    Screens.Push(new OnslaughtPrepScreen(25, new Vector2(140, 140), "Item 1: ", "Item 2: ", "Health Item: ", "Weapon 1: ", "Weapon 2: ", "GO!", "Back"));
                    break;
                case (int)ScreenRef.RPS:
                    Screens.Push(new RPSScreen(30, new Vector2(65, 213), "Play", "Back"));
                    break;
                case (int)ScreenRef.BTO:
                    Screens.Push(new BTOScreen(30, new Vector2(65, 213), "Play", "Back"));
                    break;
                case (int)ScreenRef.Snipe:
                    Screens.Push(new SnipeScreen());
                    break;
                case (int)ScreenRef.Data:
                    Screens.Push(new DataScreen());
                    break;
                case (int)ScreenRef.Controls:
                    Screens.Push(new ControlsScreen(25, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y - 40), (int)PlayerIndex.One, "Player: " + (int)PlayerIndex.One, "Attack - ", "Jump - ", "Special Attack - ", "Pick up/Drop - ", "Restore Defaults", "Apply Changes", "Back"));
                    break;
                case (int)ScreenRef.Sound:
                    Screens.Push(new SoundTest(30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y), "BGM: 0", "SE: 0", "Stop", "Exit"));
                    break;
                case (int)ScreenRef.Unlock:
                    Screens.Push(new UnlockablesScreen());
                    break;
                case (int)ScreenRef.Extra:
                    Screens.Push(new ExtraModesScreen(30, new Vector2(ScreenHalf.X - 60, 130), "Rank Mode", "Boss Rush", "Museum", "Versus", "Exit"));
                    break;
                case (int)ScreenRef.Rank:
                    Screens.Push(new RankScreen(26, new Vector2(ScreenHalf.X - 60, 75), "Level 1", "Level 2", "Level 3", "Level 4", "Level 5", "Level 6", "Level 7", "Level 8", "Exit"));
                    break;
                case (int)ScreenRef.Versus:
                    Screens.Push(new VersusScreen(30, new Vector2(ScreenHalf.X - 60, ScreenHalf.Y), new Vector2(ScreenHalf.X - 60, (ScreenHalf.Y / 2) - 10), "Begin", "Customize", "Exit"));
                    break;
                case (int)ScreenRef.Secret:
                    Screens.Push(new SecretScreen());
                    break;
            }
        }

        //Pushes a new screen onto the stack
        public void AddScreen(Screen Screen)
        {
            Screens.Push(Screen);
        }

        //Removes the current screen from the stack
        public void RemoveScreen()
        {
            Screens.Pop();
            if (CurScreen() != null)
            {
                CurScreen().ResetInput();
                CurScreen().CheckUnlocks();
            }
        }

        //Changes the screen resolution (either to Normal (416x320), Double (832x640), or Fullscreen)
        public void ChangeScreenResolution(int screensize)
        {
            //Make sure the value input is within range
            if (Helper.WithinEnumRange(typeof(ScreenResolution), screensize) == true)
            {
                switch (screensize)
                {
                    case (int)ScreenResolution.Normal:
                        graphics.PreferredBackBufferWidth = (int)Main.ScreenSize.X;
                        graphics.PreferredBackBufferHeight = (int)Main.ScreenSize.Y;
                        graphics.IsFullScreen = false;
                        break;
                    case (int)ScreenResolution.Double:
                        graphics.PreferredBackBufferWidth = (int)Main.ScreenSize.X * 2;
                        graphics.PreferredBackBufferHeight = (int)Main.ScreenSize.Y * 2;
                        graphics.IsFullScreen = false;
                        break;
                    default:
                        graphics.PreferredBackBufferWidth = (int)Main.ScreenSize.X;
                        graphics.PreferredBackBufferHeight = (int)Main.ScreenSize.Y;
                        graphics.IsFullScreen = true;
                        break;
                }

                graphics.ApplyChanges();
                SaveLoadData.SaveData("Settings", "Gameplay", "Resolution", screensize);
            }
        }

        public Level GetCurrentLevel()
        {
            return Level;
        }

        //Check if the player already used all of his/her autosaves in that level or not
        public bool GetAutosaved()
        {
            if (Level != null) return (Level.AutoSaved <= 0);
            else return false;
        }

        //Pauses the game when the pause button is pressed
        public void PauseGame(List<Player> Players)
        {
            //Check if a player pressed pause
            for (int i = 0; i < Players.Count; i++)
            {
                //If a player presses the Pause button...
                if (Input.CheckKeyPress(KeyboardState, Player.GetActionKeys(Players[i].GPlayerNum)[(int)Player.Action.Pause]) == true)
                {
                    //Pause the game; unpausing is handled by the Pause screen
                    AddScreen((int)Main.ScreenRef.Pause, Players[i].GPlayerNum, i);

                    PauseGame();
                    LoadSounds.Play(LoadSounds.Pause);
                    MediaPlayer.Pause();
                    break;
                }
            }
        }

        //Sets up the players and their characters
        public void SetUpPlayers(Player[] players)
        {
            if (players != null)
            {
                Players.AddRange(players);
                //Players.Add(CreateObjects.Wil(1));
                //Players.Add(CreateObjects.Crystal(2));
                //Players.Add(CreateObjects.Jeff(3));
            }
        }

        //Sets up the game's levels and players as well as the bonus stages
        public void SetUpLevelsPlayers(float activeTime, params Player[] players)
        {
            SetUpPlayers(players);

            Level = CreateObjects.CreateLevel(Players, CurrentLevel);
            //If the players are ingame and the loaded level isn't the first level, then the players started from an autosave, so use up one and delete the save if there are none left in case the players want to end the process to preserve it
            if (CurrentLevel > 0)
            {
                Level.AutoSaved = Convert.ToInt32((String)SaveLoadData.LoadData("Autosave", "Autosaves", "Uses"));
                Level.AutoSaved--;

                if (Level.AutoSaved <= 0) SaveLoadData.EraseAutosave();
                else SaveLoadData.SaveData("Autosave", "Autosaves", "Uses", Level.AutoSaved);
            }
            
            //For testing bonuses
            //Bonuses.Enqueue(new GrabItem(Players));
            //Bonuses.Enqueue(new GBarrel(Players));
            //Bonuses.Enqueue(new AvoidRock(Players));

            //If there's a bonus stage carried over from an autosave, start in a bonus stage
            if (Bonuses.Count > 0)
            {
                StartBonus();
            }
            //Otherwise, you're in game
            else SetState(GameState.InGame);

            Level.SpawnPlayers(activeTime);
        }

        //Sets up a level for rank mode
        public void SetUpRankLevelPlayers(float activeTime, int levelnum, params Player[] players)
        {
            SetUpPlayers(players);
            Level = CreateObjects.CreateRankLevel(Players, levelnum);
            Level.SpawnPlayers(activeTime);

            GameMode = GameState.InRank;
        }

        public void StartBonus()
        {
            if (Bonuses.Count > 0)
            {
                SetState(GameState.InBonus);
                Bonuses.Peek().SpawnPlayers(activeTime);
            }
        }

        //Sets up the challenge levels
        //For setting up multiple sublevels in challenge levels like Obstacle Course, just pass in more Challenges
        public void SetUpChallenges(params CLevel[] level)
        {
            SetState(GameState.InChallenge);
            Challenges.AddRange(level);
            Challenges[CurrentChallenge].SpawnPlayers(activeTime);
        }

        //This is called when returning to the main menu from the game; it resets level values in the game
        public void ResetLevels(bool autosaved = false)
        {
            Level = null;
            Bonuses.Clear();
            Players.Clear();

            CurrentLevel = 0;

            //Reset the autosave if the player was in game and exited Story mode
            if (CheckState(GameState.InGame) == true && autosaved == false)
            {
                SaveLoadData.EraseAutosave();
            }
            
            Player.Continues = 1;
            Level.StartBonus = false;
            SetState(GameState.Screen);
            UnPauseGame();
        }

        //Resets challenge levels
        public void ResetChallenges()
        {
            Challenges.Clear();
            Players.Clear();

            SetState(GameState.Screen);
            CurrentChallenge = 0;
            UnPauseGame();
        }

        //Restarts the challenge level
        public void RestartChallenges()
        {
            //If a challenge level consists of more sublevels, restart them all
            for (int i = 0; i < Challenges.Count; i++)
                Challenges[i].RestartChallenge(activeTime);
            CurrentChallenge = 0;
            Challenges[CurrentChallenge].SpawnPlayers(activeTime);
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        //Called for the LevelEditor
        public static void LevelEditorUpdate(GameTime gameTime)
        {
            activeTime += ElapsedTimeOneFrame(gameTime);
        }

        //Runs every clock tick
        protected override void Update(GameTime gameTime)
        {
            #if DEBUG
                Debug.Update();
                if (Debug.DebugPause == true && Debug.AdvanceFrame == false)
                    return;
            #endif

            //Update active game time - scale the addition with the elapsed game time depending on the current FPS of the game, since all of the timing is based off 60 FPS
            if (IsPaused == false)
                activeTime += ElapsedTimeOneFrame(gameTime);

            if (CurrentFPS != FPS && (activeTime - Enemy.PrevBossTimer) >= 1500f)
                ResetGameSpeed();

            //The intro screen - "Whatever Presents"
            if (CheckState(GameState.StartUp) == true)
            {
                if (Input.CheckKeyPress(KeyboardState, Keys.Enter) == true)
                {
                    SetState(GameState.Screen);
                }
                else if (StartUpColor == 1f)
                {
                    LoadGraphics.IntroPresents.Update(activeTime);
                    if (LoadGraphics.IntroPresents.IsAnimationEnd() == true)
                    {
                        SetState(GameState.Screen);
                        //LoadSounds.PlayMenuMusic(0);
                    }
                }
                else
                {
                    StartUpColor += .004f;
                    if (StartUpColor >= 1f)
                    {
                        StartUpColor = 1f;
                        LoadGraphics.IntroPresents.Reset(activeTime);
                    }
                }
            }
            //Update everything else
            else
            {
                //If you're doing the main game, update the levels
                if (CheckState(GameState.InGame) == true || CheckState(GameState.InRank) == true)
                {
                    //If the game isn't paused, update everything related to levels; otherwise update the pause screen
                    if (IsPaused == false)
                    {
                        //Update players, enemies, etc.
                        Level.Update(activeTime, this);

                        //When a level is complete - the first check ensures that the game doesn't crash when getting a game over in rank mode or the last level because the levels are reset from the Level class
                        if (Level != null && Level.LevelComplete() == true)
                        {
                            //If there are no more levels left, play the ending and restart the game; if you're in rank mode, don't play the ending
                            if (CheckState(GameState.InRank) == true || (CheckState(GameState.InRank) == false && CurrentLevel + 1 > MaxLevels))
                            {
                                if (CheckState(GameState.InRank) == false)
                                {
                                    //Ending here
                                    DataScreen.CheckNewHighscore(Players, this);

                                    //Check Story mode unlockables
                                    Unlockables.CheckStoryUnlocked(this);
                                }
                                else
                                {
                                    //Check Rank Mode unlockables
                                    Unlockables.CheckRanksUnlocked(this);
                                }
                                ResetLevels();
                                LoadSounds.StopMusic();

                                //LoadSounds.PlayMenuMusic(0);
                            }
                            //Otherwise, move onto the next level
                            else
                            {
                                CurrentLevel++;

                                //Save the player's progress after each level for the autosave
                                SaveAutosave();

                                //Have a bonus stage if the player collected the Bonus Token in the level
                                if (Level.StartBonus == true)
                                {
                                    Bonuses.Enqueue(BStage.RandomBonusStage(Players));
                                    StartBonus();
                                }
                                //Otherwise move onto the next level
                                else
                                {
                                    Level = CreateObjects.CreateLevel(Players, CurrentLevel);
                                    Level.SpawnPlayers(activeTime);

                                    LoadSounds.PlayLevelMusic(CurrentLevel);
                                }
                            }
                        }
                    }
                    else Screens.Peek().Update(this, activeTime);
                }
                //If you're doing a bonus stage, update those
                else if (CheckState(GameState.InBonus) == true)
                {
                    //If the game isn't paused, update everything related to bonus stages; otherwise update the pause screen
                    if (IsPaused == false)
                    {
                        Bonuses.Peek().Update(activeTime, this);

                        if (Bonuses.Peek().SubLevelComplete() == true)
                        {
                            Bonuses.Dequeue();

                            if (Bonuses.Count == 0)
                            {
                                SetState(GameState.InGame);

                                Level = CreateObjects.CreateLevel(Players, CurrentLevel);

                                Level.StartBonus = false;
                                Level.SpawnPlayers(activeTime);

                                LoadSounds.PlayLevelMusic(CurrentLevel);
                            }
                            else
                            {
                                Bonuses.Peek().SpawnPlayers(activeTime);
                                LoadSounds.StopMusic();
                            }
                        }
                    }
                    else Screens.Peek().Update(this, activeTime);
                }
                //If you're doing a challenge mode, update the challenge levels
                else if (CheckState(GameState.InChallenge) == true)
                {
                    //If the game isn't paused, update everything related to the challenge levels; otherwise update the pause screen
                    if (IsPaused == false)
                    {
                        //Updates contents of the challenge level
                        Challenges[CurrentChallenge].Update(activeTime, this);

                        //When the challenge level is complete
                        if (Challenges[CurrentChallenge].SubLevelComplete() == true)
                        {
                            if (CurrentChallenge + 1 == Challenges.Count)
                            {
                                //Check Challenge Mode unlockables
                                Unlockables.CheckChallengesUnlocked(this);

                                ResetChallenges();
                                LoadSounds.StopMusic();
                                //LoadSounds.PlayMenuMusic(0);
                            }
                            else
                            {
                                CurrentChallenge++;
                                Challenges[CurrentChallenge].SpawnPlayers(activeTime);
                            }
                        }
                    }
                    else Screens.Peek().Update(this, activeTime);
                }
                //Update a cutscene
                else if (Cutscenes.Count > 0)
                {
                    Cutscenes.Peek().Update(activeTime);
                    if (Cutscenes.Peek().IsFinished() == true) Cutscenes.Pop();
                }
                else if (GameMode == GameState.LevelEditor) Editor.Update(activeTime, this);
                //If you're not in a challenge, cutscene, or other game mode then update the current menu screen
                else
                {
                    Screens.Peek().Update(this, activeTime);
                }
            }

            KeyboardState = Keyboard.GetState();

            //Exit the game if the player chose to exit
            if (ExitGame == true) Exit();
            base.Update(gameTime);

            //The TargetElapsedTime sets how many times Update calls in a second; the default is 60
            //To halve the game's FPS, set TargetElapsedTime to TimeSpan.FromSeconds(1f / 30f) - the divisor (second number) determines how many times per second Update will be called
            this.TargetElapsedTime = TimeSpan.FromSeconds(1f / CurrentFPS);
        }

        //Draws graphics each clock tick
        protected override void Draw(GameTime gameTime)
        {
            if (GameMode != GameState.StartUp)
                GraphicsDevice.Clear(Color.CornflowerBlue);
            else GraphicsDevice.Clear(Color.Black);

            //Scale the graphics to fit with different resolutions
            Matrix ScalingMatrix = Matrix.CreateScale(new Vector3(/*(int)*/((float)graphics.GraphicsDevice.PresentationParameters.BackBufferWidth / ScreenSize.X), /*(int)*/((float)graphics.GraphicsDevice.PresentationParameters.BackBufferHeight / ScreenSize.Y), 1));

            //Begin drawing
            spriteBatch.Begin(SpriteSortMode.FrontToBack, null, SamplerState.PointClamp, null, null, null, ScalingMatrix);

            //Draw the start up screen - "Whatever Presents"
            if (CheckState(GameState.StartUp) == true)
            {
                LoadGraphics.IntroPresents.Draw(spriteBatch, new Vector2(-10, 100), false, Color.White * StartUpColor, Vector2.Zero, 0f, .8, 0.1f);
            }
            else
            {
                //If you're playing the main levels, draw those
                if (CheckState(GameState.InGame) == true || CheckState(GameState.InRank) == true)
                {
                    //Draw the level
                    Level.Draw(activeTime, spriteBatch);
                    if (IsPaused == true)
                        Screens.Peek().Draw(activeTime, spriteBatch);
                }
                //If you're playing a bonus stage, draw those
                else if (CheckState(GameState.InBonus) == true)
                {
                    Bonuses.Peek().Draw(activeTime, spriteBatch);
                    if (IsPaused == true)
                        Screens.Peek().Draw(activeTime, spriteBatch);
                }
                //If you're playing a challenge level, draw that
                else if (CheckState(GameState.InChallenge) == true)
                {
                    Challenges[CurrentChallenge].Draw(activeTime, spriteBatch);
                    if (IsPaused == true)
                        Screens.Peek().Draw(activeTime, spriteBatch);
                }
                else if (Cutscenes.Count > 0)
                    Cutscenes.Peek().Draw(spriteBatch);
                else if (CheckState(GameState.LevelEditor) == true) Editor.Draw(activeTime, spriteBatch);
                else Screens.Peek().Draw(activeTime, spriteBatch);
            }
            //End drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}