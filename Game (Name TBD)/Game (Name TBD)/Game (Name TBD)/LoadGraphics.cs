﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class loads all the game's graphics on startup; it's static so that these graphics can be accessed from anywhere!
    //Be sure to make the HUDs in here too to shorten the player and enemy constructors and simplify them!
    public static class LoadGraphics
    {
        //Debug graphics
        public static Texture2D ScalableBox;

        public static Texture2D StartUp;

        //Items
        public static Texture2D ItemSprite;
        public static Texture2D BagOfJewels;
        public static Texture2D PileOfMoney;
        public static Texture2D PocketWatch;
        public static Texture2D Turkey;
        public static Texture2D GrahamDoll;
        public static Texture2D ReplacementTank;

        public static Texture2D ItemIcon;

        //Item Containers
        public static Texture2D ContainerSprite;
        public static Texture2D BarrelIcon;

        public static Texture2D[] CurHealthTex;
        public static Texture2D[] HealthTex;
        public static Texture2D[] HealthTexEnd;
        public static Texture2D TempBarTex;
        public static Texture2D DeadIcon;

        //Rank graphics
        public static Texture2D[] LetterGraphic;

        //Unlockables graphics
        public static Texture2D UnlockablesLocked;
        public static Texture2D RewardCongratulations;
        public static Texture2D[] UnlockableIcons;

        public static Texture2D[] StatusSprites;
        public static Texture2D[] NoStatusSprites;
        public static Texture2D StatusBonus;
        public static Texture2D StatusTime;
        public static Texture2D CurStatusTime;
        public static Texture2D NewStatusTime;
        public static Texture2D NewCurStatusTime;
        public static Texture2D PlayerShadow;

        public static Texture2D[] CharacterIcons;
        public static Texture2D[] PlayerNumSelection;

        //Characters
        public static Texture2D[][] CharSheets;
        public static Texture2D PortraitFrame;

        //Graham
        public static Texture2D GrahamPortrait;
        public static Texture2D GrahamIcon;

        //Wil
        public static Texture2D WilPortrait;
        public static Texture2D WilIcon;
        public static Texture2D FreeBulletIcon;

        //Crystal
        public static Texture2D CrystalPortrait;
        public static Texture2D CrystalIcon;

        //Jeff
        public static Texture2D JeffPortrait;
        public static Texture2D JeffIcon;

        public static List<Texture2D> Cut1;
        public static List<Texture2D> Text1;

        //Enemies
        public static Texture2D[][] EnemySheets;
        public static Texture2D enemsprite;

        //Diver


        //Mark

        //Christopher

        //Paul


        //Tank

        //Greg

        //Grunt


        //Rogue


        //Changer


        //Imposter

        //Gerald


        //Samson


        //Theodore


        //Commander

        //Spritefonts
        public static SpriteFont HUDFont;
        public static SpriteFont MenuFont;
        public static SpriteFont ImgFont;

        public static Texture2D BG, FG;
        public static List<Texture2D> WeaponSprite;
        public static List<Texture2D> WeaponIcons;

        //For the Snipe challenge mode
        public static Texture2D EnemDesignated;

        //Level graphics
        public static Texture2D LevelTime;

        //Intro & Outro
        public static Texture2D LevelIntro;
        public static Texture2D LevelStart;
        public static Texture2D LevelClear;
        public static Texture2D[] LevelNumGraphic;

        public static List<Texture2D> EnemyIcon;
        public static Texture2D LevelGoAnim;

        public static Texture2D BlackFade;
        public static Texture2D continuedialog;
        public static Texture2D GameOver;

        //Projectiles
        public static Texture2D projectile;
        public static Texture2D ExplosiveBottle;
        public static Texture2D Spike;

        //For the intro screen
        public static Animation IntroPresents;

        public static Texture2D TitleScreen;
        public static Texture2D OptionsScreen;
        public static Texture2D PauseScreen;
        public static Texture2D QuitScreen;
        public static Texture2D CharSelectScreen;
        public static Texture2D ChallengeScreen;
        public static Texture2D[] SurvivalScreen;
        public static Texture2D HitScreen;
        public static Texture2D OnslaughtScreen;
        public static Texture2D RPSScreen;
        public static Texture2D BTOScreen;
        public static Texture2D SnipeScreen;
        public static Texture2D SecretScreen;
        public static Texture2D ScreenSelect;

        public static Texture2D EnemyMuseumTemplate;

        //Hazard and scenery stuff
        public static Texture2D Water;
        public static Texture2D Splash;
        public static Texture2D FallingRock;
        public static Texture2D FallingRockDust;
        public static Texture2D PoisonShooter;
        public static Texture2D PoisonGas;
        public static Texture2D SpikeShooter;
        public static Texture2D Explosion;

        //Equipment graphics
        public static Texture2D OxygenTankOutline;
        public static Texture2D OxygenTankFillBlue;
        public static Texture2D OxygenTankFillRed;
        public static Texture2D OxygenTankFillGreen;
        public static Texture2D OxygenTankFillYellow;
        
        //Versus Mode graphics
        public static Texture2D VersusSwitchIcon;
        public static Texture2D[] TeamGraphics;

        //General graphics, like hit graphics and such
        public static Texture2D Hit;
        public static Texture2D Block;
        public static Texture2D Dust;

        static LoadGraphics()
        {
            CurHealthTex = new Texture2D[3];
            HealthTex = new Texture2D[3];
            HealthTexEnd = new Texture2D[3];

            LetterGraphic = new Texture2D[5];

            LevelNumGraphic = new Texture2D[9];
            PlayerNumSelection = new Texture2D[(int)PlayerIndex.Four + 1];

            //The None status doesn't have an icon
            StatusSprites = new Texture2D[Enum.GetNames(typeof(Status.Statuses)).Length - 1];
            NoStatusSprites = new Texture2D[4];
        }

        public static void LoadContent(ContentManager Content)
        { 
            LoadCharSprites(Content);
            LoadEnemySprites(Content);
            LoadContainerSprites(Content);
            LoadItemSprites(Content);
            LoadWeaponSprites(Content);
            LoadSpriteFonts(Content);
            LoadLevelGraphics(Content);
            LoadStatusGraphics(Content);
            LoadScreenGraphics(Content);
            LoadCutsceneGraphics(Content);
            LoadProjectileGraphics(Content);
            LoadHazardGraphics(Content);
            LoadGeneralGraphics(Content);
            LoadDebugGraphics(Content);

            //Load current health textures (the graphic showing how much health you have)
            //Load health textures (the graphic showing the health bar you're at and the bar under the current one - red if none)
            //Load the ends of the health textures (stored and loaded separately so they can be placed at different spots based on the length of the health bar)
            for (int i = 0; i < CurHealthTex.Length; i++)
            {
                if (i == 0)
                {
                    CurHealthTex[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\CurHealthTex");
                    HealthTex[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\HealthTex");
                    HealthTexEnd[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\HealthTexEnd");
                }
                else
                {
                    int texturenum = i + 1;

                    CurHealthTex[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\CurHealthTex" + texturenum);
                    HealthTex[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\HealthTex" + texturenum);
                    HealthTexEnd[i] = Content.Load<Texture2D>("CharSprites\\HealthSprites\\HealthTexEnd" + texturenum);
                }
            }

            TempBarTex = Content.Load<Texture2D>("CharSprites\\HealthSprites\\TempBarTex");
            DeadIcon = Content.Load<Texture2D>("General\\DeadIcon");

            OxygenTankOutline = Content.Load<Texture2D>("Equipment\\OxygenTankIconOutlineSmall");
            OxygenTankFillBlue = Content.Load<Texture2D>("Equipment\\OxygenTankIconFillSmall");
            OxygenTankFillRed = Content.Load<Texture2D>("Equipment\\OxygenTankColors\\OxygenTankRed");
            OxygenTankFillGreen = Content.Load<Texture2D>("Equipment\\OxygenTankColors\\OxygenTankGreen");
            //OxygenTankFillYellow = Content.Load<Texture2D>("Equipment\\OxygenTankColors\\OxygenTankYellow");

            VersusSwitchIcon = Content.Load<Texture2D>("Equipment\\VersusSwitchIcon");
            TeamGraphics = new Texture2D[3] { Content.Load<Texture2D>("General\\RedTeam"), Content.Load<Texture2D>("General\\GreenTeam"), Content.Load<Texture2D>("General\\BlueTeam") };
        }

        public static Vector2 GetStringSize(SpriteFont spritefont, String String, float scale = 1f)
        {
            Vector2 stringsize = spritefont.MeasureString(String) * scale;
            stringsize.X = (int)stringsize.X;
            stringsize.Y = (int)stringsize.Y;

            return stringsize;
        }

        private static void LoadCharSprites(ContentManager Content)
        {
            CharacterIcons = new Texture2D[4] { Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamIcon"), Content.Load<Texture2D>("CharSprites\\WilSprites\\WilIcon")
            , Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalIcon"), Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffIcon") };

            //Load all the player selection numbers on the character selection screen
            for (int i = 0; i < PlayerNumSelection.Length; i++)
            {
                PlayerNumSelection[i] = Content.Load<Texture2D>("CharSprites\\Portraits\\" + (i + 1) + "P Selection");
            }

            //Character Spritesheets
            CharSheets = new Texture2D[][] { new Texture2D[] { Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamSheet"), Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamAlt1"), Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamAlt2"), Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamAlt3") }, 
                                             new Texture2D[] { Content.Load<Texture2D>("CharSprites\\WilSprites\\WilSheet"), Content.Load<Texture2D>("CharSprites\\WilSprites\\WilAlt1"), Content.Load<Texture2D>("CharSprites\\WilSprites\\WilAlt2"), Content.Load<Texture2D>("CharSprites\\WilSprites\\WilAlt3") }, 
                                             new Texture2D[] { Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalSheet"), Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalAlt1"), Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalAlt2"), Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalAlt3") }, 
                                             new Texture2D[] { Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffSheet"), Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffAlt1"), Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffAlt2"), Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffAlt3") } };

            PortraitFrame = Content.Load<Texture2D>("CharSprites\\Portraits\\PortraitFrame");

            //Graham
            GrahamPortrait = Content.Load<Texture2D>("CharSprites\\Portraits\\GrahamPortraitLit");
            GrahamIcon = Content.Load<Texture2D>("CharSprites\\GrahamSprites\\GrahamIcon");

            //Wil
            WilPortrait = Content.Load<Texture2D>("CharSprites\\Portraits\\WilPortraitLit");
            WilIcon = Content.Load<Texture2D>("CharSprites\\WilSprites\\WilIcon");

            //Crystal
            CrystalPortrait = Content.Load<Texture2D>("CharSprites\\Portraits\\CrystalPortraitLit");
            CrystalIcon = Content.Load<Texture2D>("CharSprites\\CrystalSprites\\CrystalIcon");

            //Jeff
            JeffPortrait = Content.Load<Texture2D>("CharSprites\\Portraits\\JeffPortraitLit");
            JeffIcon = Content.Load<Texture2D>("CharSprites\\JeffSprites\\JeffIcon");

            PlayerShadow = Content.Load<Texture2D>("CharSprites\\Shadow");
        }

        private static void LoadEnemySprites(ContentManager Content)
        {
            //Enemy spritesheets
            EnemySheets = new Texture2D[][] { new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkAlt1"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Christopher\\ChristopherSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Tank\\TankSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Imposter\\ImposterSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreSheet"), Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreEnraged"), Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreEnraged"), Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreEnraged"), Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreEnraged") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") },
                                              new Texture2D[] { Content.Load<Texture2D>("EnemSprites\\Commander\\CommanderSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet"), Content.Load<Texture2D>("EnemSprites\\Mark\\MarkSheet") }
            };

            //Diver


            //Mark

            //Christopher

            //Paul

            //Tank

            //Greg

            //Grunt
            

            //Rogue
            

            //Changer
            

            //Imposter

            //Gerald
            

            //Samson
            

            //Theodore
            

            //Commander
            

            EnemyIcon = new List<Texture2D>();

            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Mark\\MarkIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Christopher\\ChristopherIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Paul\\PaulIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Tank\\TankIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Greg\\GregIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Grunt\\GruntIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Rogue\\RogueIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Changer\\ChangerIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Imposter\\ImposterIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Gerald\\GeraldIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Samson\\SamsonIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Theodore\\TheodoreIcon"));
            EnemyIcon.Add(null);
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Diver\\DiverIcon"));
            EnemyIcon.Add(Content.Load<Texture2D>("EnemSprites\\Commander\\CommanderIcon"));

            enemsprite = Content.Load<Texture2D>("EnemSprites\\1");

            EnemDesignated = Content.Load<Texture2D>("EnemSprites\\DesignatedMark");
        }

        private static void LoadContainerSprites(ContentManager Content)
        {
            ContainerSprite = Content.Load<Texture2D>("ContainerSprites\\Barrel");
            BarrelIcon = Content.Load<Texture2D>("ContainerSprites\\Icons\\BarrelIcon");
        }

        private static void LoadItemSprites(ContentManager Content)
        {
            ItemSprite = Content.Load<Texture2D>("ItemSprites\\Apple");
            BagOfJewels = Content.Load<Texture2D>("ItemSprites\\BagOfJewels");
            PileOfMoney = Content.Load<Texture2D>("ItemSprites\\PileOfMoney");
            PocketWatch = Content.Load<Texture2D>("ItemSprites\\PocketWatch");
            Turkey = Content.Load<Texture2D>("ItemSprites\\Turkey");
            GrahamDoll = Content.Load<Texture2D>("ItemSprites\\GrahamDoll");
            ReplacementTank = Content.Load<Texture2D>("ItemSprites\\ReplacementTank");

            ItemIcon = Content.Load<Texture2D>("ItemSprites\\Icons\\ItemIcon");
        }

        private static void LoadWeaponSprites(ContentManager Content)
        {
            WeaponSprite = new List<Texture2D>();
            WeaponIcons = new List<Texture2D>();
            WeaponSprite.Add(Content.Load<Texture2D>("WeaponSprites\\BrassKnuckles"));
            WeaponSprite.Add(Content.Load<Texture2D>("WeaponSprites\\Knife"));
            WeaponSprite.Add(Content.Load<Texture2D>("WeaponSprites\\Katana"));

            WeaponIcons.Add(Content.Load<Texture2D>("WeaponSprites\\Icons\\BrassKnucklesIcon"));
            WeaponIcons.Add(Content.Load<Texture2D>("WeaponSprites\\Icons\\KnifeIcon"));
            WeaponIcons.Add(Content.Load<Texture2D>("WeaponSprites\\Icons\\KatanaIcon"));
        }

        private static void LoadLevelGraphics(ContentManager Content)
        {
            LevelIntro = Content.Load<Texture2D>("LevelObj\\IntroOutro\\LevelIntro");
            LevelStart = Content.Load<Texture2D>("LevelObj\\IntroOutro\\LevelStart");
            LevelClear = Content.Load<Texture2D>("LevelObj\\IntroOutro\\LevelClear");

            LevelNumGraphic = new Texture2D[] { Content.Load<Texture2D>("LevelObj\\IntroOutro\\LevelNum0"), null, null, null, null, null, null, null, null };

            LevelTime = Content.Load<Texture2D>("LevelObj\\TimeGraphic");

            //for (int i = 0; i < LevelNumGraphic.Length; i++)
            //{
            //    LevelNumGraphic[i] = Content.Load<Texture2D>("LevelObj\\IntroOutro\\LevelNum" + i);
            //}

            LevelGoAnim = Content.Load<Texture2D>("LevelObj\\GoSprites\\Go");

            BG = Content.Load<Texture2D>("LevelObj\\BG2");
            FG = Content.Load<Texture2D>("LevelObj\\FG");
            continuedialog = Content.Load<Texture2D>("LevelObj\\Continue");
            GameOver = Content.Load<Texture2D>("LevelObj\\Game Over");
            BlackFade = Content.Load<Texture2D>("LevelObj\\Transition");
        }

        private static void LoadStatusGraphics(ContentManager Content)
        {
            //Thanks to "Servant of the Lord" on www.gamedev.net for the latest StunDown icon (as of 12/12/14)!
            for (int i = 0; i < StatusSprites.Length; i++)
            {
                StatusSprites[i] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\" + Status.StatName(i + 1));
            }

            for (int i = (int)Status.Statuses.NoJump; i <= (int)Status.Statuses.NoSpecial; i++)
            {
                NoStatusSprites[i - (int)Status.Statuses.NoJump] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\No-Status Cross-Outs\\" + Status.StatName(i));
            }

            //StatusSprites[0] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\SpeedBoost");
            //StatusSprites[1] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\SpeedDown");
            //StatusSprites[2] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\DamageBoost");
            //StatusSprites[3] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\DamageDown");
            //StatusSprites[4] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\DefenseBoost");
            //StatusSprites[5] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\DefenseDown");
            //StatusSprites[6] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\Poison");
            //StatusSprites[7] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\Invincible");
            //StatusSprites[8] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\Rainbow");
            //StatusSprites[9] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\StunDown");
            //StatusSprites[10] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\NoJump");
            //StatusSprites[11] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\NoAttack");
            //StatusSprites[12] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\NoGrab");
            //StatusSprites[13] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\NoSpecial");
            //StatusSprites[14] = Content.Load<Texture2D>("StatusIcons\\NewIcons\\InfBullets");
            //StatusSprites[15] = Content.Load<Texture2D>("StatusIcons\\Normal");
            StatusBonus = Content.Load<Texture2D>("StatusIcons\\NewIcons\\StatusBonus");
            StatusTime = Content.Load<Texture2D>("StatusIcons\\StatusTime");
            CurStatusTime = Content.Load<Texture2D>("StatusIcons\\CurStatusTime");
            NewStatusTime = Content.Load<Texture2D>("StatusIcons\\NewStatusTime");
            NewCurStatusTime = Content.Load<Texture2D>("StatusIcons\\NewCurStatusTime");
        }

        private static void LoadSpriteFonts(ContentManager Content)
        {
            HUDFont = Content.Load<SpriteFont>("SpriteFonts\\HUDFont");
            MenuFont = Content.Load<SpriteFont>("SpriteFonts\\MenuFont");

            ImgFont = Content.Load<SpriteFont>("SpriteFonts\\SOR2 Bitmap Font");
        }

        private static void LoadScreenGraphics(ContentManager Content)
        {
            StartUp = Content.Load<Texture2D>("Screens\\Start Up");
            IntroPresents = new Animation(StartUp, false, 1, 500);
            SurvivalScreen = new Texture2D[3];

            RewardCongratulations = Content.Load<Texture2D>("Screens\\UnlockablesGraphics\\RewardCongratulations");
            UnlockableIcons = new Texture2D[Enum.GetValues(typeof(UnlockablesScreen.Unlocks)).Length];

            TitleScreen = Content.Load<Texture2D>("Screens\\Title Screen");
            OptionsScreen = Content.Load<Texture2D>("Screens\\Options Screen");
            PauseScreen = Content.Load<Texture2D>("Screens\\Pause Screen");
            QuitScreen = Content.Load<Texture2D>("Screens\\Quit Screen");
            CharSelectScreen = Content.Load<Texture2D>("Screens\\Character Select Screen");
            ChallengeScreen = Content.Load<Texture2D>("Screens\\Challenge Screen");
            SurvivalScreen[0] = Content.Load<Texture2D>("Screens\\Survival Screen");
            SurvivalScreen[1] = Content.Load<Texture2D>("Screens\\Survival (Normal) Screen");
            SurvivalScreen[2] = Content.Load<Texture2D>("Screens\\Survival (Hard) Screen");
            OnslaughtScreen = Content.Load<Texture2D>("Screens\\Onslaught Screen");
            RPSScreen = Content.Load<Texture2D>("Screens\\Rock, Paper, Status Screen");
            BTOScreen = Content.Load<Texture2D>("Screens\\Break The Objects Screen");
            SnipeScreen = Content.Load<Texture2D>("Screens\\Snipe Screen");
            SecretScreen = Content.Load<Texture2D>("Screens\\Secret Screen");
            ScreenSelect = Content.Load<Texture2D>("Screens\\SelectionArrow");

            EnemyMuseumTemplate = Content.Load<Texture2D>("Screens\\Enemy Museum\\Enemy Museum Display Template");

            for (int i = LetterGraphic.Length - 1; i >= 0; i--)
            {
                LetterGraphic[i] = Content.Load<Texture2D>("Screens\\RankGraphics\\" + Ranking.LetterValue(i));
            }

            UnlockablesLocked = Content.Load<Texture2D>("Screens\\UnlockablesGraphics\\Unlockables Locked");
            
            /*for (int i = 0; i < UnlockableIcons.Length; i++)
            {
                UnlockableIcons[i] = Content.Load<Texture2D>("Screens\\UnlockablesGraphics\\" + Enum.GetName(typeof(UnlockablesScreen.Unlocks), i));
            }*/
        }

        private static void LoadCutsceneGraphics(ContentManager Content)
        {
            Cut1 = new List<Texture2D>();
            Text1 = new List<Texture2D>();

            Cut1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Graphic1"));
            Cut1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Graphic2"));
            Cut1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Graphic3"));

            Text1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Text1"));
            Text1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Text2"));
            Text1.Add(Content.Load<Texture2D>("Cutscenes\\Cutscene 1\\Text3"));
        }

        private static void LoadProjectileGraphics(ContentManager Content)
        {
            projectile = Content.Load<Texture2D>("Projectiles\\Bullet");
            ExplosiveBottle = Content.Load<Texture2D>("Projectiles\\ExplodingBottle");
            FreeBulletIcon = Content.Load<Texture2D>("Projectiles\\FreeBullet");

            Spike = Content.Load<Texture2D>("Projectiles\\Spike");
        }

        private static void LoadHazardGraphics(ContentManager Content)
        {
            Water = Content.Load<Texture2D>("Hazards & Scenery\\WaterSprite");
            Splash = Content.Load<Texture2D>("Hazards & Scenery\\Splash");
            FallingRock = Content.Load<Texture2D>("Hazards & Scenery\\FallingRock");

            FallingRockDust = Content.Load<Texture2D>("Hazards & Scenery\\Dust");

            PoisonShooter = Content.Load<Texture2D>("Hazards & Scenery\\PoisonGasShooter\\PoisonShooter");
            PoisonGas = Content.Load<Texture2D>("Hazards & Scenery\\PoisonGasShooter\\PoisonGas");

            SpikeShooter = Content.Load<Texture2D>("Hazards & Scenery\\SpikeShooter");

            Explosion = Content.Load<Texture2D>("Hazards & Scenery\\Explosion");
        }

        private static void LoadGeneralGraphics(ContentManager Content)
        {
            Hit = Content.Load<Texture2D>("General\\HitSprites");
            Block = Content.Load<Texture2D>("General\\BlockGraphic");
            Dust = Content.Load<Texture2D>("General\\DustSprites");
        }

        private static void LoadDebugGraphics(ContentManager Content)
        {
            ScalableBox = Content.Load<Texture2D>("DebugAssets\\ScalableBox");
        }

        //Splits a texture into multiple ones of equivalent sizes, given the width and height of each desired texture and the position to start splitting
        //vertical tells whether to take parts horizontally or vertically in the original texture
        //CREDIT FOR MAIN ALGORITHM (http://gamedev.stackexchange.com/a/11590)
        //NOTE: Not tested, and not sure if it works properly or not! This may need revising later if it is ever used
        /*public static Texture2D[] SplitTexture(Texture2D originaltexture, bool vertical, Vector2 startingpos, int texturewidth, int textureheight, int numtextures)
        {
            //The split textures
            Texture2D[] SplitTextures = new Texture2D[numtextures];

            //Get the original pixel data from original textures
            Color[] originalcolors = new Color[originaltexture.Width * originaltexture.Height];
            originaltexture.GetData<Color>(originalcolors);

            //Go through the specified number of textures
            for (int i = 0; i < numtextures; i++)
            {
                //The texture and pixel data for the part
                Texture2D originalpart = new Texture2D(originaltexture.GraphicsDevice, vertical == false ? texturewidth * (i + 1) : texturewidth, vertical == true ? textureheight * (i + 1) : textureheight);
                Color[] partdata = new Color[texturewidth * textureheight];

                //The starting location of the part
                Vector2 partstart = new Vector2((int)startingpos.X * i, (int)startingpos.Y * i);

                //Find each pixel in the X direction in the part of the texture
                for (int x = 0; x < originalpart.Width; x++)
                {
                    //Find each pixel in the Y direction in the part of the texture
                    for (int y = 0; y < originalpart.Height; y++)
                    {
                        //The index in the single array
                        int index = (x + y) * textureheight;

                        //If a part goes outside of the original texture, then fill the space with transparency
                        if (partstart.X + x >= originaltexture.Width || partstart.Y + y >= originaltexture.Height)
                        {
                            partdata[index] = Color.Transparent;
                        }
                        //Otherwise get the texture data like normal
                        else partdata[index] = originalcolors[((int)partstart.X + x) + ((int)partstart.Y + y) * originaltexture.Height];
                    }
                }

                //Fill in the texture's colors
                originalpart.SetData<Color>(partdata);

                //Put the part into the array of parts
                SplitTextures[i] = originalpart;
            }

            return SplitTextures;
        }*/
    }
}
