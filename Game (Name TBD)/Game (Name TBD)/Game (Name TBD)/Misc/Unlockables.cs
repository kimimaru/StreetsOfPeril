﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game__Name_TBD_
{
    //This class provides methods that check if game modes, settings, etc. need to be unlocked
    public static class Unlockables
    {
        //Checks if various story mode-related unlockables need to be unlocked if they haven't already
        public static void CheckStoryUnlocked(Main main)
        {
            bool rankunlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", "RankMode", "Unlocked"));
            bool challengesunlocked = Convert.ToBoolean(SaveLoadData.LoadData("Unlockables", "Challenges", "Unlocked"));

            //Check if Rank Mode and Challenges need to be unlocked if they haven't been already (after completing the game)
            if (rankunlocked || challengesunlocked == false)
            {
                SaveLoadData.SaveData("Unlockables", "RankMode", "Unlocked", true);
                SaveLoadData.SaveData("Unlockables", "RankMode", "HowTo", "Complete the game at least once");
                SaveLoadData.SaveData("Unlockables", "Challenges", "Unlocked", true);
                SaveLoadData.SaveData("Unlockables", "Challenges", "HowTo", "Complete the game at least once");
                main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.RankMode));
                main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.Challenges));
            }
        }

        //Checks if various rank mode-related unlockables need to be unlocked if they haven't already
        public static void CheckRanksUnlocked(Main main)
        {
            bool bossunlock = SaveLoadData.CheckUnlocked("BossRush");

            //Check if Boss Rush should be unlocked if it isn't already (all of the Ranks have been completed)
            if (bossunlock == false)
            {
                /*BossRushScreen.Unlocked = */
                bossunlock = Unlockables.BossRushUnlock();
                if (bossunlock == true)
                {
                    SaveLoadData.SaveData("Unlockables", "BossRush", "Unlocked", true);
                    SaveLoadData.SaveData("Unlockables", "BossRush", "HowTo", "Complete all the levels in Rank Mode");
                    main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.BossRush));
                }
            }
        }

        //Checks if various challenge-related unlockables need to be unlocked if they haven't already
        public static void CheckChallengesUnlocked(Main main)
        {
            bool soundtestunlocked = SaveLoadData.CheckUnlocked("SoundTest");

            //Check if the Sound Test should be unlocked if it isn't already (all of the challenges have been completed)
            if (soundtestunlocked == false)
            {
                soundtestunlocked = Unlockables.SoundTestUnlock();
                if (soundtestunlocked == true)
                {
                    SaveLoadData.SaveData("Unlockables", "SoundTest", "Unlocked", true);
                    SaveLoadData.SaveData("Unlockables", "SoundTest", "HowTo", "Complete all the challenges");
                    main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.SoundTest));
                }
            }

            bool onslaughtunlocked = SaveLoadData.CheckUnlocked("Onslaught");

            //Check if the Onslaught challenge should be unlocked
            if (onslaughtunlocked == false)
            {
                onslaughtunlocked = OnslaughtUnlocked();
                if (onslaughtunlocked == true)
                {
                    SaveLoadData.SaveData("Unlockables", "Onslaught", "Unlocked", true);
                    SaveLoadData.SaveData("Unlockables", "Onslaught", "HowTo", "Complete Easy and Normal of Survival");
                    main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.Onslaught));
                }
            }

            bool snipeunlocked = SaveLoadData.CheckUnlocked("Snipe");

            //Check if the Snipe challenge should be unlocked
            if (snipeunlocked == false)
            {
                snipeunlocked = SnipeUnlocked();
                if (snipeunlocked == true)
                {
                    SaveLoadData.SaveData("Unlockables", "Snipe", "Unlocked", true);
                    SaveLoadData.SaveData("Unlockables", "Snipe", "HowTo", "Complete Easy and Normal of BTO");
                    main.AddScreen(new RewardScreen((int)UnlockablesScreen.Unlocks.Snipe));
                }
            }
        }

        //Check if Survival Easy and Normal have been completed
        private static bool OnslaughtUnlocked()
        {
            return (SaveLoadData.LoadData("Challenges", "Challenge" + (int)CLevel.Challenges.Survival + 0, "Rank") != null && SaveLoadData.LoadData("Challenges", "Challenge" + (int)CLevel.Challenges.Survival + 1, "Rank") != null);
        }

        //Check if BTO Easy and Normal have been completed
        private static bool SnipeUnlocked()
        {
            return (SaveLoadData.LoadData("Challenges", "Challenge" + (int)CLevel.Challenges.BTO + 0, "Rank") != null && SaveLoadData.LoadData("Challenges", "Challenge" + (int)CLevel.Challenges.BTO + 1, "Rank") != null);
        }

        //Check if all of the levels in rank mode are completed
        private static bool BossRushUnlock()
        {
            //Check the save file for the levels
            for (int i = 0; i < 8; i++)
            {
                if (SaveLoadData.LoadData("Levels", "Level" + i, "Rank") == null) return false;
            }

            return true;
        }

        //Check if all the challenges barring Onslaught - Hard are completed
        private static bool SoundTestUnlock()
        {
            //Check the save file for all the challenges
            for (int i = (int)CLevel.Challenges.Survival; i < Enum.GetValues(typeof(CLevel.Challenges)).Length; i++)
            {
                //Check the difficulties for each challenge
                for (int j = 0; j < 3; j++)
                {
                    //Disregard Onslaught - Hard
                    if (i == (int)CLevel.Challenges.Onslaught && j == (int)CLevel.CDifficulty.Hard) continue;

                    object challenge = SaveLoadData.LoadData("Challenges", "Challenge" + i + j, "Rank");

                    if (challenge == null) return false;
                }
            }

            return true;
        }
    }
}
