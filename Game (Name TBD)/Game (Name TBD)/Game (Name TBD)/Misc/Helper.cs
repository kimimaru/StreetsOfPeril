﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A class that contains a set of helper functions, such as calculations and etc.
    public static class Helper
    {
        //Restricts a value so that it cannot be below the min value or above the max value - int version
        public static int Clamp(int value, int min, int max)
        {
            if (value < min) return min;
            else if (value > max) return max;
            else return value;
        }

        //Restricts a value so that it cannot be below the min value or above the max value - float version
        public static float Clamp(float value, float min, float max)
        {
            if (value < min) return min;
            else if (value > max) return max;
            else return value;
        }

        //Returns a Vector2 that will center an image on the screen; the numinrows and numincolumns parameter helps with drawing rows of the same graphic that need to all be centered
        public static Vector2 DrawCenter(Texture2D graphic, int numinrows = 1, int numincolumns = 1)
        {
            int xdraw = (int)((Main.ScreenSize.X - (graphic.Width * numinrows)) / 2);
            int ydraw = (int)((Main.ScreenSize.Y - (graphic.Height * numincolumns)) / 2);

            return new Vector2(xdraw, ydraw);
        }

        //Returns a Vector2 that will return the proper location of an image in the row of same-sized images that are overall centered on the screen
        //(Ex. The first image in the row will be the same distance from the left side of the screen as the last image in the row will be from the right side of the screen)
        public static Vector2 DrawRowCenter(Texture2D graphic, int numinrow, int maxrows, int numincolumn, int maxcolumns)
        {
            Vector2 drawloc = DrawCenter(graphic, maxrows, maxcolumns);

            drawloc.X += (numinrow * graphic.Width);
            drawloc.Y += (numincolumn * graphic.Height);

            return drawloc;
        }

        //Returns a Vector2 that will center text on the screen
        public static Vector2 DrawTextCenter(SpriteFont spritefont, String text)
        {
            Vector2 widthheight = spritefont.MeasureString(text);

            int xdraw = (int)((Main.ScreenSize.X - widthheight.X) / 2);
            int ydraw = (int)((Main.ScreenSize.X - widthheight.Y) / 2);

            return new Vector2(xdraw, ydraw);
        }

        //Returns a scale for text to fit within a certain X value; rounds to 2 digits
        public static float FitTextX(SpriteFont spritefont, String text, int xfit, float origscale)
        {
            Vector2 textsize = spritefont.MeasureString(text);
            float newscale = (float)Math.Round(xfit / textsize.X, 2);

            //If the text is already smaller than the space to fit in, return the original scale; if we don't, we might end up with a scale bigger than the original scale (usually for shorter text)
            if ((textsize.X * origscale) < xfit) newscale = origscale;
            return newscale;
        }

        //Finds how much to offset scaled text to more closely match the original position, assuming it was drawn with an origin of (0,0)
        public static Vector2 OffSetScaledText(SpriteFont spritefont, String text, float origscale, float newscale)
        {
            Vector2 textsize = spritefont.MeasureString(text);

            int xoffset = (int)(((textsize.X * origscale) - (textsize.X * newscale)) / 2);
            int yoffset = (int)(((textsize.Y * origscale) - (textsize.Y * newscale)) / 2);

            return new Vector2(xoffset, yoffset);
        }

        //Chooses a value, based on cumulative probability, out of an array of percentage values that add up to 1 and returns the index of the value chosen in the array
        public static int ChooseValuePercentage(double[] percentages)
        {
            //Choose a random value to obtain
            Random random = new Random();
            double percentage = random.NextDouble();

            //The cumulative probability
            double cumulativepercentages = 0;

            //Make sure the sum adds up to 1 (100%)
            if (percentages.Sum() >= 1d)
            {
                //Go through all the percentages and find one to choose based on the cumulative probability of obtaining it
                for (int i = 0; i < percentages.Length; i++)
                {
                    //Add the percentage of obtaining each value; this gives us the cumulative probability
                    cumulativepercentages += percentages[i];

                    //If our cumulative probability is greater than the random number chosen, this index is the one in the array chosen
                    //This works because it ensures that it's a higher probability of getting a value with a lower percentage when a higher one is knocked out (some code and statistics knowledge taken from here: http://www.vcskicks.com/random-element.php)
                    if (cumulativepercentages > percentage)
                    {
                        return i;
                    }
                }
            }

            //Nothing was found; return an invalid index
            return -1;
        }

        //Finds the lowest number in an array and returns that index
        public static int CheckLowestValueInArray(IList<int> array)
        {
            int lowestnum = int.MaxValue;
            int index = -1;

            for (int i = 0; i < array.Count; i++)
            {
                if (array[i] < lowestnum)
                {
                    lowestnum = array[i];
                    index = i;
                }
            }

            return index;
        }

        //Finds the highest number in an array and returns that index
        public static int CheckHighestValueInArray(IList<int> array)
        {
            int highestnum = int.MinValue;
            int index = -1;

            for (int i = 0; i < array.Count; i++)
            {
                if (array[i] > highestnum)
                {
                    highestnum = array[i];
                    index = i;
                }
            }

            return index;
        }

        //Checks if any value in an array is equal to a specific value; both the array and the value should be the same type
        public static bool CheckAnyValueInArray<T>(IList<T> array, T value)
        {
            //If an index was found, that means it's true
            return (GetIndexValueInArray<T>(array, value) >= 0);
        }

        //Checks if any value in an array is equal to a specific value and returns the first array index where the comparison was true; if there was no comparison, return -1
        public static int GetIndexValueInArray<T>(IList<T> array, T value)
        {
            for (int i = 0; i < array.Count; i++)
            {
                //Check the default equality comparer for the two values
                //This method of comparing generics was obtained here: http://stackoverflow.com/a/390974
                if (EqualityComparer<T>.Default.Equals(array[i], value) == true) return i;
            }

            return -1;
        }

        //Checks if all values in an array are equal to a specific value; both the array and the value should be the same type
        public static bool CheckAllValuesInArray<T>(IList<T> array, T value)
        {
            for (int i = 0; i < array.Count; i++)
            {
                //Check the default equality comparer for the two values
                //This method of comparing generics was obtained here: http://stackoverflow.com/a/390974
                if (EqualityComparer<T>.Default.Equals(array[i], value) == false) return false;
            }

            return true;
        }

        //Finds the number of values in an array that are equal to a specific value; both the array and the value should be the same type
        public static int GetNumberValuesInArray<T>(IList<T> array, T value)
        {
            int comparisons = 0;

            for (int i = 0; i < array.Count; i++)
            {
                //Check the default equality comparer for the two values
                //This method of comparing generics was obtained here: http://stackoverflow.com/a/390974
                if (EqualityComparer<T>.Default.Equals(array[i], value) == true) comparisons++;
            }

            return comparisons;
        }

        //Sets all values in an array to a specific value; both the array and the value should be the same type
        public static void SetAllValuesInArray<T>(T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = value;
            }
        }

        //Checks if a value is within an Enum range
        public static bool WithinEnumRange(Type enumtype, int value)
        {
            return (value >= 0 && value < Enum.GetValues(enumtype).Length);
        }

        //Checks if a value for an integer in the save file is null and returns a specified default value if so; otherwise, it returns the value from the save file
        public static int LoadFromSaveElseDefault(String category, String subcategory, String storename, int defaultvalue)
        {
            object loadval = SaveLoadData.LoadData(category, subcategory, storename);

            if (loadval != null) return Convert.ToInt32(loadval);
            else return defaultvalue;
        }

        //Checks if a value for a boolean in the save file is null and returns a specified default value if so; otherwise, it returns the value from the save file
        //This is different from the other types since a value of null converts to false for a boolean, but those that want to default to true will need this
        public static bool LoadFromSaveElseDefault(String category, String subcategory, String storename, bool defaultvalue)
        {
            object loadval = SaveLoadData.LoadData(category, subcategory, storename);

            if (loadval != null) return Convert.ToBoolean(loadval);
            else return defaultvalue;
        }

        //Checks if a value for a float in the save file is null and returns a specified default value if so; otherwise, it returns the value from the save file
        public static float LoadFromSaveElseDefault(String category, String subcategory, String storename, float defaultvalue)
        {
            object loadval = SaveLoadData.LoadData(category, subcategory, storename);

            if (loadval != null) return (float)Convert.ToDouble(loadval);
            else return defaultvalue;
        }

        //Swaps two values or references
        public static void Swap<T>(ref T value1, ref T value2)
        {
            //Store one of the values or references
            T tempval = value1;

            //Set one of the values or references to the other then set the other value or reference to the stored one
            value1 = value2;
            value2 = tempval;
        }

        //Swaps two values or references in a collection that has direct indexing, such as an array or list
        public static void SwapInCollection<T>(IList<T> array, int index1, int index2)
        {
            //Store one of the indices
            T tempval = array[index1];
            
            //Set one of the indices to the other then set the other index to the stored one
            array[index1] = array[index2];
            array[index2] = tempval;
        }

        //Chooses a random element in a collection that excludes variables by removing them from the collection afterwards
        public static T ChooseRandomExcluding<T>(IList<T> included)
        {
            int randnum = new Random().Next(0, included.Count);
            T returnedval = included[randnum];

            included.RemoveAt(randnum);
            return returnedval;
        }
    }
}
