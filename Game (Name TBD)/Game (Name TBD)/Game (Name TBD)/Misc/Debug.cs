﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A class for easily debugging certain features of the game
    public static class Debug
    {
        //Halts the game for testing frame-by-frame
        public static bool DebugPause;
        public static bool AdvanceFrame;

        public static bool HitboxDraw;
        public static bool HurtboxDraw;
        public static bool GrabboxDraw;
        public static bool FeetLocDraw;

        public static bool TileEngineDraw;

        public static KeyboardState KeyboardState;

        static Debug()
        {
            DebugPause = AdvanceFrame = HitboxDraw = HurtboxDraw = GrabboxDraw = FeetLocDraw = TileEngineDraw = false;

            KeyboardState = new KeyboardState();
        }

        //Draws a rectangle
        public static void DrawRectangle(SpriteBatch spriteBatch, Rectangle rectangle, Vector2 OffSet, Color color, float Depth = .999f)
        {
            spriteBatch.Draw(LoadGraphics.ScalableBox, new Vector2(rectangle.X + OffSet.X, rectangle.Y + OffSet.Y), null, color, 0f, Vector2.Zero, new Vector2(rectangle.Width, rectangle.Height), SpriteEffects.None, Depth);
        }

        //Displays a value or object to the Console
        public static void OutputValue(object value)
        {
            //Figure out where in the stack we printed this value
            StackFrame stackFrame = new StackFrame(1, true);
            string filename = stackFrame.GetFileName();
            string methodname = stackFrame.GetMethod().ToString();
            int linenumber = stackFrame.GetFileLineNumber();

            Console.WriteLine(value + "\nIn " + filename + " in " + methodname + " at line " + linenumber);
        }

        public static void Update()
        {
            AdvanceFrame = false;

            //Frame advance
            if (DebugPause == true && Input.CheckKeyPress(KeyboardState, Keys.OemSemicolon) == true)
                AdvanceFrame = true;

            if (Input.KeyHeld(Keys.LeftControl) == true)
            {
                //Enable hitbox drawing
                if (Input.CheckKeyPress(KeyboardState, Keys.Up) == true)
                    HitboxDraw = !HitboxDraw;

                //Enable hurtbox drawing
                if (Input.CheckKeyPress(KeyboardState, Keys.Left) == true)
                    HurtboxDraw = !HurtboxDraw;

                //Enable feetloc drawing
                if (Input.CheckKeyPress(KeyboardState, Keys.Right) == true)
                    FeetLocDraw = !FeetLocDraw;

                //Enable grabbox drawing
                if (Input.CheckKeyPress(KeyboardState, Keys.Down) == true)
                    GrabboxDraw = !GrabboxDraw;

                //Further commands for holding Shift
                if (Input.KeyHeld(Keys.LeftShift) == true)
                {
                    //Pause the game
                    if (Input.CheckKeyPress(KeyboardState, Keys.P) == true)
                        DebugPause = !DebugPause;

                    //Toggle tile engine drawing
                    if (Input.CheckKeyPress(KeyboardState, Keys.T) == true)
                        TileEngineDraw = !TileEngineDraw;
                }
            }

            KeyboardState = Keyboard.GetState();
        }
    }
}
