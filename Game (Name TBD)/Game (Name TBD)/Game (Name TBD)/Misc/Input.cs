﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //This class checks for key inputs to determine if something involving a key press can be performed
    public static class Input
    {
        public static bool CheckKeyUnPress(KeyboardState keyboardstate, Keys key)
        {
            return (Keyboard.GetState().IsKeyUp(key) && !keyboardstate.IsKeyUp(key));
        }

        //Checks if a key was pushed on this frame
        public static bool CheckKeyPress(KeyboardState keyboardstate, Keys key)
        {
            return (Keyboard.GetState().IsKeyDown(key) && !keyboardstate.IsKeyDown(key));
        }

        //Checks if a player pushed one of their buttons on this frame
        public static bool CheckPlayerPress(KeyboardState keyboardstate, List<Player> Players, int playeraction)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (CheckKeyPress(keyboardstate, Player.GetActionKey(Players[i].GPlayerNum, playeraction)) == true) return true;
            }

            return false;
        }

        //Checks if a button is held
        public static bool KeyHeld(Keys key)
        {
            return (Keyboard.GetState().IsKeyDown(key) == true);
        }

        //Level editor mouse input
        public static bool CheckMouseInput(ButtonState mousestatebutton, ButtonState mousebutton)
        {
            return (mousebutton == ButtonState.Pressed && mousestatebutton != ButtonState.Pressed);
        }
    }
}
