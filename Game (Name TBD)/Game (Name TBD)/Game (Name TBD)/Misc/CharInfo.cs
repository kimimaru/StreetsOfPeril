﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A general class that stores information about a player, such as which character and costume the player is selecting
    //This is used in the Character Select Screen and Levels, when players are selecting characters and costumes through some sort of menu
    public sealed class CharInfo
    {
        public enum CharSelectState
        {
            NotPlaying, Choosing, Selected
        };

        public enum LevelState
        {
            GameOver = -3, PressStart, NumContinues, ChooseCharacter, Playing
        };

        private int PlayerNum;
        private int CharacterNum;
        private int CostumeNum;

        //Used in determining what part of a selection the player is at (Ex. not playing, choosing, selected)
        private int SelectionState;

        //The previous character that was chosen (used for char select screen)
        private int PrevCharacter;

        public CharInfo(int playernum)
        {
            PlayerNum = playernum;
            CharacterNum = 0;
            CostumeNum = 0;

            SelectionState = 0;
            PrevCharacter = 0;
        }

        public CharInfo(int playernum, int initcharacter, int initcostume) : this(playernum)
        {
            CharacterNum = initcharacter;
            CostumeNum = initcostume;

            PrevCharacter = CharacterNum;
        }

        public CharInfo(int playernum, int initcharacter, int initcostume, int initstate) : this(playernum, initcharacter, initcostume)
        {
            SelectionState = initstate;
        }

        public int GetPlayerNum
        {
            get { return PlayerNum; }
        }

        public int GetCharacterNum
        {
            get { return CharacterNum; }
        }

        public int GetCostumeNum
        {
            get { return CostumeNum; }
        }

        public int GetState
        {
            get { return SelectionState; }
        }

        public int GetPrevCharacter
        {
            get { return PrevCharacter; }
        }

        public void IncrementCharNum()
        {
            CharacterNum++;
            if (CharacterNum >= Player.MaxCharacters) CharacterNum = 0;
            PrevCharacter = CharacterNum;
        }

        public void DecrementCharNum()
        {
            CharacterNum--;
            if (CharacterNum < 0) CharacterNum = (Player.MaxCharacters - 1);
            PrevCharacter = CharacterNum;
        }

        public void ChangeCharNum(int character, bool changeprev = false)
        {
            CharacterNum = character;
            if (changeprev == true) PrevCharacter = CharacterNum;
        }

        public void IncrementCostumeNum()
        {
            CostumeNum++;
            if (CostumeNum >= Player.MaxCostumes) CostumeNum = 0;
        }

        public void DecrementCostumeNum()
        {
            CostumeNum--;
            if (CostumeNum < 0) CostumeNum = (Player.MaxCostumes - 1);
        }

        //used when setting the costume to the next available one
        public void ChangeCostume(int costume)
        {
            CostumeNum = costume;
        }

        public void ChangeState(int selectionstate)
        {
            SelectionState = selectionstate;
        }

        //Methods for checking how many players are on the same character or state
        public static int GetSameCharNum(CharInfo[] selectors, int character)
        {
            int numchars = 0;

            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                if (selectors[i].GetState != (int)CharInfo.CharSelectState.NotPlaying && selectors[i].GetCharacterNum == character)
                {
                    numchars++;
                }
            }

            return numchars;
        }

        public static int GetSameState(CharInfo[] selectors, int state)
        {
            int numchars = 0;

            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                if (selectors[i].GetState == state)
                {
                    numchars++;
                }
            }

            return numchars;
        }

        public static int GetSameCharState(CharInfo[] selectors, int charnum, int state)
        {
            int numchars = 0;

            for (int i = 0; i < Player.MaxPlayers; i++)
            {
                if (selectors[i].GetState == state && selectors[i].GetCharacterNum == charnum)
                {
                    numchars++;
                }
            }

            return numchars;
        }

        //Finds the next available costume for a given character
        //Before looking for the next costume, mark the current costume as available so it'll wrap around if no others are available
        //The "changecharjoin" parameter is used when joining in or switching characters; it resets back to the first costume to find the first available one
        public static int NextAvailableCostume(bool[][] costumestaken, CharInfo player, bool changecharjoin = false)
        {
            int nextcostume = player.GetCostumeNum + 1;
            if (changecharjoin == true) nextcostume = 0;
            if (nextcostume >= costumestaken[player.GetCharacterNum].Length) nextcostume = 0;

            //This is a failsafe in case no available costumes exist for whatever reason; a duplicate color will indicate a problem without it freezing the game
            int loops = 0;

            while (costumestaken[player.GetCharacterNum][nextcostume] == true)
            {
                nextcostume++;
                if (nextcostume >= costumestaken[player.GetCharacterNum].Length) nextcostume = 0;
                
                //Increment the number of times this while loop has ran through
                loops++;
                //There's a problem; the worst case occurs when all costumes except the character's current costume are taken, which loops just once through the array - exit and use the first costume
                if (loops > Player.MaxCostumes)
                {
                    nextcostume = 0;
                    break;
                }
            }

            return nextcostume;
        }

        //Methods for easily checking if a certain player pressed a certain button
        private bool PressedActionKey(KeyboardState keyboard, int actionkey)
        {
            return (Input.CheckKeyPress(keyboard, Player.GetActionKey(PlayerNum, actionkey)));
        }

        public bool PressedPause(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Pause));
        }

        public bool PressedAttack(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Attack));
        }

        public bool PressedSpecAttack(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.SpecAttack));
        }

        public bool PressedJump(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Jump));
        }

        public bool PressedItem(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.ItemKey));
        }

        public bool PressedLeft(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Left));
        }

        public bool PressedRight(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Right));
        }

        public bool PressedUp(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Up));
        }

        public bool PressedDown(KeyboardState keyboard)
        {
            return (PressedActionKey(keyboard, (int)Player.Action.Down));
        }
    }
}
