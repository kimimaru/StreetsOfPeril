﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Runtime.Serialization;

namespace Game__Name_TBD_
{
    //The level editor for creating levels - for developing purposes only
    //NOTE: Once you polish it up a bit, make you able to set the spawn points of players!
    public class LevelEditor
    {
        private enum InterfaceState
        {
            Enemies, Containers, Hazards, Tiles, Stop
        };

        private Texture2D Background;

        private Vector2 CameraLoc;
        private Vector2 DrawOffSet;
        private Vector2 OffSet;
        private int StopPointSpawn;
        private Vector2 ScrollSpeed;
        private TileEngine TileEngine;
        private Vector2 LevelSize;
        private Vector2 TileOffset;

        private int Difficulty;
        private int MinPlayers;
        private bool Designated;

        //Tells where to insert something into the list (for more easily fixing design errors)
        private int InsertIndex;

        private KeyboardState KeyboardState;
        private MouseState MouseState;
        private PropertiesWindow PropWindow;

        //For saving and loading
        private bool? SaveLoadDialog;
        private String FileName;

        private bool StopEdit;
        private bool TileEdit;
        private bool? TilePropDraw;
        private TileEngine.Tile TileCopy;
        private bool DrawInterface;
        private bool Properties;
        private int EditIndex;
        private int CurrentInterface;
        private object SelectedObject;

        private List<ObjectData> EnemyParams;
        private List<ObjectData> ContainerParams;
        private List<ObjectData> HazardParams;

        private List<Vector2> StopPoints;
        private List<ObjectSpawnPoint<Enemy>> EnemPoints;
        private List<ObjectSpawnPoint<ItemContainer>> ContPoints;
        private List<ObjectSpawnPoint<Hazard>> HazardPoints;

        private Rectangle[] Selections = new Rectangle[Enum.GetValues(typeof(Enemy.Enemies)).Length];

        private Texture2D[] Enemies = new Texture2D[] { LoadGraphics.enemsprite };
        private Texture2D[] Hazards = new Texture2D[] { LoadGraphics.FallingRock, LoadGraphics.SpikeShooter, LoadGraphics.PoisonShooter, LoadGraphics.SpikeShooter, LoadGraphics.SpikeShooter };

        public LevelEditor()
        {
            Background = LoadGraphics.BG;

            FileName = "Untitled";
            CameraLoc = DrawOffSet = OffSet = Vector2.Zero;
            StopPointSpawn = 0;
            LevelSize = new Vector2(Main.ScreenSize.X, Main.ScreenSize.Y);
            TileOffset = Vector2.Zero;
            ScrollSpeed = new Vector2(2);
            TileEngine = new TileEngine(LevelSize);

            Difficulty = 0;
            MinPlayers = 1;
            Designated = true;

            InsertIndex = -1;

            KeyboardState = new KeyboardState(Keys.Enter);
            MouseState = new MouseState();

            SaveLoadDialog = null;

            TileEdit = StopEdit = false;
            TilePropDraw = true;
            TileCopy = new TileEngine.Tile();
            DrawInterface = false;
            Properties = false;
            EditIndex = -1;
            CurrentInterface = (int)InterfaceState.Enemies;
            SelectedObject = null;

            EnemyParams = new List<ObjectData>();
            ContainerParams = new List<ObjectData>();
            HazardParams = new List<ObjectData>();

            StopPoints = new List<Vector2>();
            EnemPoints = new List<ObjectSpawnPoint<Enemy>>();
            ContPoints = new List<ObjectSpawnPoint<ItemContainer>>();
            HazardPoints = new List<ObjectSpawnPoint<Hazard>>();

            for (int i = 0; i < Enum.GetValues(typeof(Enemy.Enemies)).Length; i++) Selections[i] = new Rectangle((int)Main.ScreenHalf.X + (50 * (i % 4)), 10 + (70 * (i / 4)), 50, 70);
        }

        //Conversion methods - converts strings into parameters for constructors
        public static Vector2 ConvertVector2(String value)
        {
            String[] vector2 = value.Split(',');

            return new Vector2((float)Convert.ToDouble(vector2[0]), (float)Convert.ToDouble(vector2[1]));
        }

        public static Vector3 ConvertVector3(String value)
        {
            String[] vector3 = value.Split(',');

            return new Vector3((float)Convert.ToDouble(vector3[0]), (float)Convert.ToDouble(vector3[1]), (float)Convert.ToDouble(vector3[2]));
        }

        private static Status ConvertStatus(String value)
        {
            String[] status = value.Split(',');
            return new Status(Status.StatNum(status[0]), Convert.ToInt32(status[1]), Convert.ToInt32(status[2]));
        }

        private static object[] ConvertItemWeapon(System.Reflection.ParameterInfo[] param, String value)
        {
            String[] contweapon = value.Split(',');

            //Not all items take in statuses as an argument, so check if one does and assign the status
            if (param.Length > 0)
            {
                String weapon;

                //If you just enter the name of the object, it'll give it no status (Ex. "Turkey" turns into "Turkey,None,0,0")
                if (contweapon.Length < 4)
                    weapon = "None,0,0";
                else
                    weapon = contweapon[1] + "," + contweapon[2] + "," + contweapon[3];

                return new object[] { ConvertStatus(weapon) };
            }
            else return null;
        }

        //Places an object when you click after its properties have been set
        private void PlaceObject()
        {
            String[] vals = PropWindow.GetValues();
            object[] parameters = new object[vals.Length];

            int selectedobject = (int)SelectedObject;

            //Create enemies
            if (CurrentInterface == (int)InterfaceState.Enemies)
            {
                if (selectedobject != (int)Enemy.Enemies.MinecartRider && selectedobject != (int)Enemy.Enemies.Diver)
                {
                    //Make sure all of the input is valid
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "0";
                    if (String.IsNullOrEmpty(vals[1]) == true || vals[1].Contains(',') == false) vals[1] = "None,0,0";

                    for (int i = 0; i < vals.Length; i++)
                    {
                        if (String.IsNullOrEmpty(vals[i]) == true && selectedobject != (int)Enemy.Enemies.WeaponWielder) vals[i] = "0";
                    }

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        if (i == 0)
                        {
                            parameters[i] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[0]));
                            vals[0] = vals[0].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y)) + ",");
                        }
                        else if (i == 1) parameters[i] = ConvertStatus(vals[i]);
                        else
                        {
                            if (selectedobject != (int)Enemy.Enemies.WeaponWielder || i == parameters.Length - 1) parameters[i] = Convert.ToInt32(vals[i]);
                            else
                            {
                                if (i == 2)
                                {
                                    if (String.IsNullOrEmpty(vals[i]) == true) vals[i] = "true";
                                    parameters[i] = Convert.ToBoolean(vals[i]);
                                }
                                else if (i == 3)
                                {
                                    parameters[i] = String.IsNullOrEmpty(vals[i]) == true ? null : (Weapon)typeof(CreateObjects).GetMethod(vals[i].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(vals[i].Split(',')[0]).GetParameters(), vals[i]));
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "Dummy";
                    if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "0";
                    if (String.IsNullOrEmpty(vals[2]) == true) vals[2] = "None,0,0";
                    if (String.IsNullOrEmpty(vals[3]) == true) vals[3] = "enemsprite";
                    if (selectedobject == (int)Enemy.Enemies.Diver)
                    {
                        if (String.IsNullOrEmpty(vals[4]) == true) vals[4] = "-1";
                        if (String.IsNullOrEmpty(vals[5]) == true) vals[5] = "0";
                    }
                    else if (selectedobject == (int)Enemy.Enemies.MinecartRider)
                    {
                        if (String.IsNullOrEmpty(vals[4]) == true) vals[4] = "4,4";
                    }

                    String[] status = vals[2].Split(',');

                    parameters[0] = vals[0];
                    parameters[1] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[1]));
                    vals[1] = vals[1].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y)) + ",");
                    parameters[2] = new Status(Status.StatNum(status[0]), Convert.ToInt32(status[1]), Convert.ToInt32(status[2]));
                    parameters[3] = typeof(LoadGraphics).GetField(vals[3]).GetValue(null);

                    if (selectedobject == (int)Enemy.Enemies.Diver)
                    {
                        parameters[4] = (float)Convert.ToDouble(vals[4]);
                        parameters[5] = Convert.ToInt32(vals[5]);
                    }
                    else if (selectedobject == (int)Enemy.Enemies.MinecartRider) parameters[4] = ConvertVector2(vals[4]);
                }

                System.Reflection.MethodInfo methodinfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(Enemy.Enemies), SelectedObject));

                if (EditIndex <= -1)
                {
                    EnemPoints.Insert(InsertIndex <= -1 ? EnemPoints.Count : InsertIndex, new ObjectSpawnPoint<Enemy>(OffSet, (Enemy)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, Designated));
                    EnemyParams.Insert(InsertIndex <= -1 ? EnemyParams.Count : InsertIndex, new ObjectData((int)SelectedObject, Enum.GetName(typeof(Enemy.Enemies), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, Designated, vals));
                }
                else
                {
                    EnemPoints[EditIndex] = new ObjectSpawnPoint<Enemy>(OffSet, (Enemy)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, Designated);
                    Enemy.Enemies enemtype;
                    Enum.TryParse<Enemy.Enemies>(EnemyParams[EditIndex].objecttype, out enemtype);
                    EnemyParams[EditIndex] = new ObjectData((int)enemtype, Enum.GetName(typeof(Enemy.Enemies), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, Designated, vals);
                }
            }
            //Create item containers
            else if (CurrentInterface == (int)InterfaceState.Containers)
            {
                //Make sure all of the input is valid
                if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "0";
                if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "1";

                parameters[0] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[0]));
                vals[0] = vals[0].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y)) + ",");
                parameters[1] = Convert.ToInt32(vals[1]);
                parameters[2] = String.IsNullOrEmpty(vals[2]) == true ? null : (Item)typeof(CreateObjects).GetMethod(vals[2].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(vals[2].Split(',')[0]).GetParameters(), vals[2]));
                parameters[3] = String.IsNullOrEmpty(vals[3]) == true ? null : (Weapon)typeof(CreateObjects).GetMethod(vals[3].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(vals[3].Split(',')[0]).GetParameters(), vals[3]));

                System.Reflection.MethodInfo methodinfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(ItemContainer.Containers), SelectedObject));

                if (EditIndex <= -1)
                {
                    ContPoints.Insert(InsertIndex <= -1 ? ContPoints.Count : InsertIndex, new ObjectSpawnPoint<ItemContainer>(OffSet, (ItemContainer)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, !Designated));
                    ContainerParams.Insert(InsertIndex <= -1 ? ContainerParams.Count : InsertIndex, new ObjectData((int)SelectedObject, Enum.GetName(typeof(ItemContainer.Containers), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, !Designated, vals));
                }
                else
                {
                    ContPoints[EditIndex] = new ObjectSpawnPoint<ItemContainer>(OffSet, (ItemContainer)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, !Designated);
                    ContainerParams[EditIndex] = new ObjectData((int)SelectedObject, Enum.GetName(typeof(ItemContainer.Containers), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, !Designated, vals);
                }
            }
            //Create hazards
            else if (CurrentInterface == (int)InterfaceState.Hazards)
            {
                if (selectedobject == (int)Hazard.Hazards.FallingRock)
                {
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "FallingRock";
                    if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "FallingRockDust";
                    if (String.IsNullOrEmpty(vals[2]) == true) vals[2] = "50";
                    if (String.IsNullOrEmpty(vals[3]) == true || vals[3].Contains(',') == false) vals[3] = "None,0,0";
                    if (String.IsNullOrEmpty(vals[4]) == true) vals[4] = "6";

                    parameters[0] = typeof(LoadGraphics).GetField(vals[0]).GetValue(null);
                    parameters[1] = typeof(LoadGraphics).GetField(vals[1]).GetValue(null);
                    parameters[2] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[2]));
                    vals[2] = vals[2].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y) + ","));

                    parameters[3] = ConvertStatus(vals[3]);
                    parameters[4] = Convert.ToInt32(vals[4]);
                }
                else if (selectedobject == (int)Hazard.Hazards.BSShooter)
                {
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "true";
                    if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "0";
                    if (String.IsNullOrEmpty(vals[2]) == true) vals[2] = "false";
                    if (String.IsNullOrEmpty(vals[3]) == true) vals[3] = "500";
                    if (String.IsNullOrEmpty(vals[4]) == true) vals[4] = "7";

                    parameters[0] = Convert.ToBoolean(vals[0]);
                    parameters[1] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[1]));
                    vals[1] = vals[1].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y) + ","));
                    parameters[2] = Convert.ToBoolean(vals[2]);
                    parameters[3] = (float)Convert.ToDouble(vals[3]);
                    parameters[4] = Convert.ToInt32(vals[4]);
                }
                else if (selectedobject == (int)Hazard.Hazards.PoisonGasShooter)
                {
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "0";
                    if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "true";
                    if (String.IsNullOrEmpty(vals[2]) == true) vals[1] = "10";
                    if (String.IsNullOrEmpty(vals[3]) == true) vals[2] = "700";
                    if (String.IsNullOrEmpty(vals[4]) == true) vals[3] = "700";

                    parameters[0] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[0]));
                    vals[0] = vals[0].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y) + ","));
                    parameters[1] = Convert.ToBoolean(vals[1]);
                    parameters[2] = Convert.ToInt32(vals[2]);
                    parameters[3] = (float)Convert.ToDouble(vals[3]);
                    parameters[4] = (float)Convert.ToDouble(vals[4]);
                }
                else if (selectedobject == (int)Hazard.Hazards.Scarecrow)
                {
                    //Make sure all of the input is valid
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "0";

                    parameters[0] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[0]));
                    vals[0] = vals[0].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y) + ","));
                }
                else if (selectedobject == (int)Hazard.Hazards.Platform)
                {
                    if (String.IsNullOrEmpty(vals[0]) == true) vals[0] = "0";
                    if (String.IsNullOrEmpty(vals[1]) == true) vals[1] = "true";

                    parameters[0] = new Vector3(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y, (float)Convert.ToDouble(vals[0]));
                    vals[0] = vals[0].Insert(0, Convert.ToString((int)(MouseState.X - DrawOffSet.X)) + "," + Convert.ToString((int)(MouseState.Y - DrawOffSet.Y) + ","));
                    parameters[1] = Convert.ToBoolean(vals[1]);
                }

                System.Reflection.MethodInfo methodinfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(Hazard.Hazards), SelectedObject));

                if (EditIndex <= -1)
                {
                    HazardPoints.Insert(InsertIndex <= -1 ? HazardPoints.Count : InsertIndex, new ObjectSpawnPoint<Hazard>(OffSet, (Hazard)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, !Designated));
                    HazardParams.Insert(InsertIndex <= -1 ? HazardParams.Count : InsertIndex, new ObjectData((int)SelectedObject, Enum.GetName(typeof(Hazard.Hazards), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, !Designated, vals));
                }
                else
                {
                    HazardPoints[EditIndex] = new ObjectSpawnPoint<Hazard>(OffSet, (Hazard)methodinfo.Invoke(null, parameters), StopPointSpawn, Difficulty, MinPlayers, !Designated);
                    HazardParams[EditIndex] = new ObjectData((int)SelectedObject, Enum.GetName(typeof(Hazard.Hazards), SelectedObject), StopPointSpawn, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), Difficulty, MinPlayers, !Designated, vals);
                }
            }

            InsertIndex = -1;
            Designated = true;
        }

        //Clears the level
        private void ClearLevel()
        {
            StopPoints.Clear();
            EnemPoints.Clear();
            ContPoints.Clear();
            HazardPoints.Clear();
        }

        //Saves the level
        private void SaveLevel(String LevelName)
        {
            LevelData level = new LevelData(StopPoints, EnemyParams, ContainerParams, HazardParams, TileEngine);
            DataContractSerializer writer = new DataContractSerializer(typeof(LevelData));
            using (FileStream stream = new FileStream("Content\\Levels\\" + LevelName, FileMode.Create))
            {
                writer.WriteObject(stream, level);
            }
        }

        //Loads the level using the new level editor (standalone application)
        public static SublevelProp LoadLevel(String LevelName)
        {
            String truelevelname = "Content\\NewLevels\\" + LevelName + ".SOP";

            if (File.Exists(truelevelname) == false) return new SublevelProp(LoadGraphics.BG);

            return MainProgram.LoadLevel(truelevelname);
        }

        //Loads the level - takes the type of all item containers, enemies, and hazards and converts their string parameters into actual objects for use in invoking their constructors
        public static LevelData LoadLevelOld(String LevelName)
        {
            if (File.Exists("Content\\Levels\\" + LevelName) == false) return new LevelData(new List<Vector2>(), new List<ObjectData>(), new List<ObjectData>(), new List<ObjectData>(), new TileEngine(new Vector2(Main.ScreenSize.X, Main.ScreenSize.Y)));

            //Find out the level number to increase the stats of the enemies without having to do it manually
            int dashpos = LevelName.IndexOf("-");
            int levelnum = dashpos > 0 ? Convert.ToInt32(LevelName.Substring(0, dashpos)) : 0;

            LevelData level;
            DataContractSerializer reader = new DataContractSerializer(typeof(LevelData));

            using (FileStream stream = new FileStream("Content\\Levels\\" + LevelName, FileMode.Open))
            {
                level = (LevelData)reader.ReadObject(stream);
            }

            //Convert the enemy parameters into actual data
            for (int i = 0; i < level.Enemies.Count; i++)
            {
                System.Reflection.MethodInfo newmethod = typeof(CreateObjects).GetMethod(level.Enemies[i].objecttype);
                object[] parameters = new object[level.Enemies[i].param.Length];

                if (level.Enemies[i].typeobject != (int)Enemy.Enemies.MinecartRider && level.Enemies[i].typeobject != (int)Enemy.Enemies.Diver)
                {
                    for (int j = 0; j < level.Enemies[i].param.Length; j++)
                    {
                        if (j == 0) parameters[j] = ConvertVector3(level.Enemies[i].param[0]);
                        else if (j == 1) parameters[j] = ConvertStatus(level.Enemies[i].param[1]);
                        else if (j == level.Enemies[i].param.Length - 1) parameters[j] = levelnum;
                        else
                        {
                            if (level.Enemies[i].typeobject != (int)Enemy.Enemies.WeaponWielder || j == level.Enemies[i].param.Length - 1)
                                parameters[j] = Convert.ToInt32(level.Enemies[i].param[j]);
                            else
                            {
                                if (j == 2) parameters[j] = Convert.ToBoolean(level.Enemies[i].param[j]);
                                else if (j == 3) parameters[j] = String.IsNullOrEmpty(level.Enemies[i].param[j]) == true ? null : (Weapon)typeof(CreateObjects).GetMethod(level.Enemies[i].param[j].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(level.Enemies[i].param[j].Split(',')[0]).GetParameters(), level.Enemies[i].param[j]));
                            }
                        }
                    }
                }
                else
                {
                    parameters[0] = level.Enemies[i].param[0];
                    parameters[1] = ConvertVector3(level.Enemies[i].param[1]);
                    parameters[2] = ConvertStatus(level.Enemies[i].param[2]);
                    parameters[3] = typeof(LoadGraphics).GetField(level.Enemies[i].param[3]).GetValue(null);
                    if (level.Enemies[i].typeobject == (int)Enemy.Enemies.Diver)
                    {
                        parameters[4] = (float)Convert.ToDouble(level.Enemies[i].param[4]);
                        parameters[5] = Convert.ToInt32(level.Enemies[i].param[5]);
                    }
                    else if (level.Enemies[i].typeobject == (int)Enemy.Enemies.MinecartRider)
                    {
                        parameters[4] = ConvertVector2(level.Enemies[i].param[4]);
                    }
                }

                level.EnemPoints.Add(new ObjectSpawnPoint<Enemy>(ConvertVector2(level.Enemies[i].objectoffset), (Enemy)newmethod.Invoke(null, parameters), level.Enemies[i].stoppoint, level.Enemies[i].difficulty, level.Enemies[i].minplayers, level.Enemies[i].Designated));
                int curenemy = level.EnemPoints.Count - 1;

                //Set the tile the enemy is on and see if the enemy spawns in water or not
                //level.EnemPoints[curenemy].NewObject.GetObjectTile = level.TileEngine.CurTile(level.EnemPoints[curenemy].NewObject.FeetLoc());

                //if (level.EnemPoints[curenemy].NewObject.GetObjectTile.TileType == (int)TileEngine.TileTypes.Water)
                //    level.EnemPoints[curenemy].NewObject.TouchWater(0f, new Vector2(-1, -1), level.EnemPoints[curenemy].NewObject.GetObjectTile.TouchedWater(level.EnemPoints[curenemy].NewObject.gLocation.Z));

                //If the level is underwater, create an oxygen tank and spawn the enemy underwater
                if (level.TileEngine.IsUnderWater() == true)
                {
                    level.EnemPoints[curenemy].NewObject.CreateOxygenTank();
                }
            }

            //Convert the container parameters into actual data
            for (int i = 0; i < level.Containers.Count; i++)
            {
                System.Reflection.MethodInfo newmethod = typeof(CreateObjects).GetMethod(level.Containers[i].objecttype);
                object[] parameters = new object[level.Containers[i].param.Length];

                parameters[0] = ConvertVector3(level.Containers[i].param[0]);
                parameters[1] = Convert.ToInt32(level.Containers[i].param[1]);
                parameters[2] = String.IsNullOrEmpty(level.Containers[i].param[2]) == true ? null : (Item)typeof(CreateObjects).GetMethod(level.Containers[i].param[2].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(level.Containers[i].param[2].Split(',')[0]).GetParameters(), level.Containers[i].param[2]));
                parameters[3] = String.IsNullOrEmpty(level.Containers[i].param[3]) == true ? null : (Weapon)typeof(CreateObjects).GetMethod(level.Containers[i].param[3].Split(',')[0]).Invoke(null, ConvertItemWeapon(typeof(CreateObjects).GetMethod(level.Containers[i].param[3].Split(',')[0]).GetParameters(), level.Containers[i].param[3]));

                level.ContPoints.Add(new ObjectSpawnPoint<ItemContainer>(ConvertVector2(level.Containers[i].objectoffset), (ItemContainer)newmethod.Invoke(null, parameters), level.Containers[i].stoppoint, level.Containers[i].difficulty, level.Containers[i].minplayers, level.Containers[i].Designated));
                //level.ContPoints[level.ContPoints.Count - 1].NewObject.GetObjectTile = level.TileEngine.CurTile(level.ContPoints[level.ContPoints.Count - 1].NewObject.FeetLoc);
            }

            //Convert the hazard parameters into actual data
            for (int i = 0; i < level.Hazards.Count; i++)
            {
                System.Reflection.MethodInfo newmethod = typeof(CreateObjects).GetMethod(level.Hazards[i].objecttype);
                object[] parameters = new object[level.Hazards[i].param.Length];

                if (level.Hazards[i].typeobject == (int)Hazard.Hazards.FallingRock)
                {
                    parameters[0] = typeof(LoadGraphics).GetField(level.Hazards[i].param[0]).GetValue(null);
                    parameters[1] = typeof(LoadGraphics).GetField(level.Hazards[i].param[1]).GetValue(null);
                    parameters[2] = ConvertVector3(level.Hazards[i].param[2]);
                    parameters[3] = ConvertStatus(level.Hazards[i].param[3]);
                    parameters[4] = Convert.ToInt32(level.Hazards[i].param[4]);
                }
                else if (level.Hazards[i].typeobject == (int)Hazard.Hazards.BSShooter)
                {
                    parameters[0] = Convert.ToBoolean(level.Hazards[i].param[0]);
                    parameters[1] = ConvertVector3(level.Hazards[i].param[1]);
                    parameters[2] = Convert.ToBoolean(level.Hazards[i].param[2]);
                    parameters[3] = (float)Convert.ToDouble(level.Hazards[i].param[3]);
                    parameters[4] = Convert.ToInt32(level.Hazards[i].param[4]);
                }
                else if (level.Hazards[i].typeobject == (int)Hazard.Hazards.PoisonGasShooter)
                {
                    parameters[0] = ConvertVector3(level.Hazards[i].param[0]);
                    parameters[1] = Convert.ToBoolean(level.Hazards[i].param[1]);
                    parameters[2] = Convert.ToInt32(level.Hazards[i].param[2]);
                    parameters[3] = (float)Convert.ToDouble(level.Hazards[i].param[3]);
                    parameters[4] = (float)Convert.ToDouble(level.Hazards[i].param[4]);
                }
                else if (level.Hazards[i].typeobject == (int)Hazard.Hazards.Scarecrow)
                {
                    parameters[0] = ConvertVector3(level.Hazards[i].param[0]);
                }
                else if (level.Hazards[i].typeobject == (int)Hazard.Hazards.Platform)
                {
                    parameters[0] = ConvertVector3(level.Hazards[i].param[0]);
                    parameters[1] = Convert.ToBoolean(level.Hazards[i].param[1]);
                }

                level.HazardPoints.Add(new ObjectSpawnPoint<Hazard>(ConvertVector2(level.Hazards[i].objectoffset), (Hazard)newmethod.Invoke(null, parameters), level.Hazards[i].stoppoint, level.Hazards[i].difficulty, level.Hazards[i].minplayers, level.Hazards[i].Designated));
                //level.HazardPoints[level.HazardPoints.Count - 1].NewObject.GetObjectTile = level.TileEngine.CurTile(level.HazardPoints[level.HazardPoints.Count - 1].NewObject.FeetLoc);
            }
            
            return level;
        }

        private void MouseInput(Main main)
        {
            //If the game window is active...
            if (main.IsActive == true)
            {
                //Check if you clicked on one of the objects on the interface
                if (SaveLoadDialog == null && SelectedObject == null)
                {
                    if (Input.CheckMouseInput(MouseState.LeftButton, Mouse.GetState().LeftButton) == true)
                    {
                        if (DrawInterface == true)
                        {
                            for (int i = 0; i < Selections.Length; i++)
                            {
                                //NOTE: The second check is there just so it doesn't crash when selecting the WeaponWielder enemy! The enemy will be put in at a later time
                                if (new Rectangle(MouseState.X, MouseState.Y, 1, 1).Intersects(Selections[i])/* && i != (int)EnemTypes.WeaponWielder*/)
                                {
                                    SelectedObject = i;
                                    Properties = true;
                                    DrawInterface = false;

                                    if (CurrentInterface == (int)InterfaceState.Enemies)
                                    {
                                        //Check the enemy type at i and pass in the values required for its CreateObjects method, which is called when the level is loaded in-game
                                        System.Reflection.ParameterInfo[] paraminfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(Enemy.Enemies), i)).GetParameters();
                                        PropWindow = new PropertiesWindow(paraminfo);
                                    }
                                    else if (CurrentInterface == (int)InterfaceState.Containers)
                                    {
                                        System.Reflection.ParameterInfo[] paraminfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(ItemContainer.Containers), i)).GetParameters();
                                        PropWindow = new PropertiesWindow(paraminfo);
                                    }
                                    else if (CurrentInterface == (int)InterfaceState.Hazards)
                                    {
                                        System.Reflection.ParameterInfo[] paraminfo = typeof(CreateObjects).GetMethod(Enum.GetName(typeof(Hazard.Hazards), i)).GetParameters();
                                        PropWindow = new PropertiesWindow(paraminfo);
                                    }
                                    break;
                                }
                            }
                        }
                        else if (TileEdit == true)
                        {
                            if (TileEngine.TileXYExists(new Rectangle((int)MouseState.X - (int)DrawOffSet.X, (int)MouseState.Y - (int)DrawOffSet.Y, 1, 1), Vector2.Zero) == true)
                                SelectedObject = TileEngine.TileAtMouseLoc(new Vector2(MouseState.X - DrawOffSet.X, MouseState.Y - DrawOffSet.Y));
                        }
                        else if (StopEdit == true && new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 1, 1).Intersects(new Rectangle(0, 0, (int)Main.ScreenSize.X, (int)Main.ScreenSize.Y)))
                        {
                            StopPoints.Add(new Vector2(OffSet.X, OffSet.Y));
                        }
                        //Check if you left-clicked on any of the objects on the level so you can edit them
                        //Edit existing enemies
                        else if (CurrentInterface == (int)InterfaceState.Enemies)
                        {
                            for (int i = 0; i < EnemPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(EnemPoints[i].NewObject.CollisionBox))
                                {
                                    //Edit Information
                                    Properties = true;
                                    SelectedObject = EnemyParams[i].typeobject;
                                    EditIndex = i;
                                    PropWindow = new PropertiesWindow(typeof(CreateObjects).GetMethod(EnemyParams[i].objecttype).GetParameters(), EnemyParams[i].param);
                                }
                            }
                        }
                        //Edit existing containers
                        else if (CurrentInterface == (int)InterfaceState.Containers)
                        {
                            for (int i = 0; i < ContPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(ContPoints[i].NewObject.CollisionBox))
                                {
                                    //Edit Information
                                    Properties = true;
                                    SelectedObject = ContainerParams[i].typeobject;
                                    EditIndex = i;
                                    PropWindow = new PropertiesWindow(typeof(CreateObjects).GetMethod(ContainerParams[i].objecttype).GetParameters(), ContainerParams[i].param);
                                }
                            }
                        }
                        //Edit existing hazards
                        else if (CurrentInterface == (int)InterfaceState.Hazards)
                        {
                            for (int i = 0; i < HazardPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(HazardPoints[i].NewObject.CollisionBox))
                                {
                                    //Edit Information
                                    Properties = true;
                                    SelectedObject = HazardParams[i].typeobject;
                                    EditIndex = i;
                                    PropWindow = new PropertiesWindow(typeof(CreateObjects).GetMethod(HazardParams[i].objecttype).GetParameters(), HazardParams[i].param);
                                }
                            }
                        }
                    }
                    else if (Input.CheckMouseInput(MouseState.RightButton, Mouse.GetState().RightButton) == true)
                    {
                        //Check if you right- clicked on any of the objects on the level so you can remove them
                        if (CurrentInterface == (int)InterfaceState.Enemies)
                        {
                            for (int i = 0; i < EnemPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(EnemPoints[i].NewObject.CollisionBox))
                                {
                                    EnemPoints.RemoveAt(i);
                                    EnemyParams.RemoveAt(i);
                                    i--;
                                    break;
                                }
                            }
                        }
                        else if (CurrentInterface == (int)InterfaceState.Containers)
                        {
                            for (int i = 0; i < ContPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(ContPoints[i].NewObject.CollisionBox))
                                {
                                    /*if (Input.CheckMouseInput(MouseState.LeftButton, Mouse.GetState().LeftButton) == true)
                                    {
                                        //Edit information
                                        Properties = true;
                                    }*/
                                    ContPoints.RemoveAt(i);
                                    ContainerParams.RemoveAt(i);
                                    i--;
                                    break;
                                }
                            }
                        }

                        else if (CurrentInterface == (int)InterfaceState.Hazards)
                        {
                            for (int i = 0; i < HazardPoints.Count; i++)
                            {
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(HazardPoints[i].NewObject.CollisionBox))
                                {
                                    /*if (Input.CheckMouseInput(MouseState.LeftButton, Mouse.GetState().LeftButton) == true)
                                    {
                                        //Edit information
                                        Properties = true;
                                    }*/
                                    HazardPoints.RemoveAt(i);
                                    HazardParams.RemoveAt(i);
                                    i--;
                                    break;
                                }
                            }
                        }
                        else if (CurrentInterface == (int)InterfaceState.Stop)
                        {
                            for (int i = 0; i < StopPoints.Count; i++)
                            {
                                Vector2 widthheight = LoadGraphics.HUDFont.MeasureString("S");
                                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(new Rectangle((int)((Main.ScreenSize.X / 2) + StopPoints[i].X), (int)((Main.ScreenSize.Y / 2) + StopPoints[i].Y), (int)widthheight.X, (int)widthheight.Y)))
                                {
                                    StopPoints.RemoveAt(i);
                                    i--;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (SelectedObject != null)
                {
                    if (SelectedObject.GetType() == typeof(TileEngine.Tile))
                    {
                        TileEngine.Tile tile = (TileEngine.Tile)SelectedObject;

                        //Edit tile properties such as Z value, TypeHeight, and TileType with the mouse scroll wheel
                        if (Mouse.GetState().ScrollWheelValue > MouseState.ScrollWheelValue)
                        {
                            if (TilePropDraw == true)
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTileZAll((int)ScrollSpeed.Y);
                                else TileEngine.EditTileZ(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), (int)ScrollSpeed.Y);
                            }
                            else if (TilePropDraw == false)
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTypeHeightAll((int)ScrollSpeed.Y);
                                else TileEngine.EditTypeHeight(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), (int)ScrollSpeed.Y);
                            }
                            else
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTileTypeAll(1);
                                else TileEngine.EditTileType(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), 1);
                            }
                        }
                        else if (Mouse.GetState().ScrollWheelValue < MouseState.ScrollWheelValue)
                        {
                            if (TilePropDraw == true)
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTileZAll((int)-ScrollSpeed.Y);
                                else TileEngine.EditTileZ(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), (int)-ScrollSpeed.Y);
                            }
                            else if (TilePropDraw == false)
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTypeHeightAll((int)-ScrollSpeed.Y);
                                else TileEngine.EditTypeHeight(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), (int)-ScrollSpeed.Y);
                            }
                            else
                            {
                                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) == true) TileEngine.EditTileTypeAll(-1);
                                else TileEngine.EditTileType(new Vector4(tile.Location.X, tile.Location.Y, tile.Location.Z, 0f), -1);
                            }
                        }
                    }
                    else if (Input.CheckMouseInput(MouseState.LeftButton, Mouse.GetState().LeftButton) == true)
                    {
                        //Click to place an object on the screen - check which object it is
                        if (((DrawInterface == true && MouseState.X < (Main.ScreenSize.X - 200)) || DrawInterface == false) && TileEngine.TileXYExists(new Rectangle((int)MouseState.X - (int)DrawOffSet.X, (int)MouseState.Y - (int)DrawOffSet.Y, 1, 1), Vector2.Zero) == true)
                        {
                            PlaceObject();
                            EditIndex = -1;
                            SelectedObject = null;
                        }
                    }

                    //Deselect a tile by right-clicking
                    if (Input.CheckMouseInput(MouseState.RightButton, Mouse.GetState().RightButton) == true)
                    {
                        //Remove an object by right-clicking it
                        if (SelectedObject is TileEngine.Tile)
                        {
                            TileCopy = (TileEngine.Tile)SelectedObject;
                            TileCopy = TileEngine.TileAtMouseLoc(new Vector2(TileCopy.Location.X, TileCopy.Location.Y));
                        }
                        SelectedObject = null;
                    }
                }
            }
        }

        private void KeyboardInput()
        {
            //Save or load a level - open a dialog for entering 
            if (Properties == false && Keyboard.GetState().IsKeyDown(Keys.S) && Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                SaveLoadDialog = true;
                PropWindow = new PropertiesWindow("File Name");
                return;
            }
            if (Properties == false && Keyboard.GetState().IsKeyDown(Keys.L) && Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                SaveLoadDialog = false;
                PropWindow = new PropertiesWindow("File Name");
                return;
            }

            //Scroll through the level with the DrawOffSet
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (TileEngine.TileXExists(new Rectangle((int)CameraLoc.X, (int)CameraLoc.Y, 0, 0), new Vector2(-ScrollSpeed.X, 0f)) == true)
                {
                    CameraLoc.X -= ScrollSpeed.X;
                    DrawOffSet.X += ScrollSpeed.X;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (TileEngine.TileXExists(new Rectangle((int)CameraLoc.X - 1 + (int)Main.ScreenSize.X, (int)CameraLoc.Y, 0, 0), new Vector2(ScrollSpeed.X, 0f)) == true)
                {
                    CameraLoc.X += ScrollSpeed.X;
                    DrawOffSet.X -= ScrollSpeed.X;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                if (TileEngine.TileYExists(new Rectangle((int)CameraLoc.X, (int)CameraLoc.Y, 0, 0), new Vector2(0f, -ScrollSpeed.Y)) == true)
                {
                    CameraLoc.Y -= ScrollSpeed.Y;
                    DrawOffSet.Y += ScrollSpeed.Y;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                if (TileEngine.TileYExists(new Rectangle((int)CameraLoc.X, (int)CameraLoc.Y - 1 + (int)Main.ScreenSize.Y, 0, 0), new Vector2(0f, ScrollSpeed.Y)) == true)
                {
                    CameraLoc.Y += ScrollSpeed.Y;
                    DrawOffSet.Y -= ScrollSpeed.Y;
                }
            }

            //Set the offset for the objects to spawn at
            if (Keyboard.GetState().IsKeyDown(Keys.W) && TileEngine.TileYExists(new Rectangle((int)OffSet.X, (int)OffSet.Y, 1, 1), new Vector2(0f, -ScrollSpeed.Y)) == true)
                OffSet.Y -= ScrollSpeed.Y;
            if (Keyboard.GetState().IsKeyDown(Keys.S) && TileEngine.TileYExists(new Rectangle((int)OffSet.X, (int)OffSet.Y, 1, 1), new Vector2(0f, ScrollSpeed.Y)) == true)
                OffSet.Y += ScrollSpeed.Y;
            if (Keyboard.GetState().IsKeyDown(Keys.A) && TileEngine.TileXExists(new Rectangle((int)OffSet.X, (int)OffSet.Y, 1, 1), new Vector2(-ScrollSpeed.X, 0f)) == true)
                OffSet.X -= ScrollSpeed.X;
            if (Keyboard.GetState().IsKeyDown(Keys.D) && TileEngine.TileXExists(new Rectangle((int)OffSet.X, (int)OffSet.Y, 1, 1), new Vector2(ScrollSpeed.X, 0f)) == true)
                OffSet.X += ScrollSpeed.X;

            //Press I to remove the interface and see the level
            if (TileEdit == false && Input.CheckKeyPress(KeyboardState, Keys.I) == true)
                DrawInterface = !DrawInterface;

            //Press E to bring up the enemy selection
            if (CurrentInterface != (int)InterfaceState.Enemies && Input.CheckKeyPress(KeyboardState, Keys.E) == true)
            {
                TileEdit = StopEdit = false;
                SelectedObject = null;
                CurrentInterface = (int)InterfaceState.Enemies;
                Properties = false;
                Selections = new Rectangle[Enum.GetValues(typeof(Enemy.Enemies)).Length];
                for (int i = 0; i < Selections.Length; i++) Selections[i] = new Rectangle((int)Main.ScreenHalf.X + (50 * (i % 4)), 10 + (70 * (i / 4)), 50, 70);
            }

            //Press C to bring up the container selection
            if (CurrentInterface != (int)InterfaceState.Containers && Input.CheckKeyPress(KeyboardState, Keys.C) == true)
            {
                TileEdit = StopEdit = false;
                SelectedObject = null;
                CurrentInterface = (int)InterfaceState.Containers;
                Properties = false;
                Selections = new Rectangle[Enum.GetValues(typeof(ItemContainer.Containers)).Length];
                for (int i = 0; i < Selections.Length; i++) Selections[i] = new Rectangle((int)Main.ScreenHalf.X + (50 * (i % 4)), 10 + (70 * (i / 4)), 50, 70);
            }

            //Press H to bring up the hazard selection
            if (CurrentInterface != (int)InterfaceState.Hazards && Input.CheckKeyPress(KeyboardState, Keys.H) == true)
            {
                TileEdit = StopEdit = false;
                SelectedObject = null;
                CurrentInterface = (int)InterfaceState.Hazards;
                Properties = false;
                Selections = new Rectangle[Enum.GetValues(typeof(Hazard.Hazards)).Length];
                for (int i = 0; i < Selections.Length; i++) Selections[i] = new Rectangle((int)Main.ScreenHalf.X + (50 * (i % 4)), 10 + (70 * (i / 4)), 50, 70);
            }

            //Press T to edit the tiles
            if (TileEdit == false && Input.CheckKeyPress(KeyboardState, Keys.T) == true)
            {
                TileEdit = true;
                StopEdit = false;
                SelectedObject = null;
                CurrentInterface = (int)InterfaceState.Tiles;
                DrawInterface = false;
                Properties = false;
            }

            //Press V to edit the stop points
            if (StopEdit == false && Input.CheckKeyPress(KeyboardState, Keys.V) == true)
            {
                if (TileEdit == true && SelectedObject != null && Keyboard.GetState().IsKeyDown(Keys.LeftControl))
                {
                    TileEngine.Tile newtilez = (TileEngine.Tile)SelectedObject;
                    if (TilePropDraw == true) newtilez.Location.Z = TileCopy.Z;
                    else if (TilePropDraw == false) newtilez.TypeHeight = TileCopy.TypeHeight;
                    SelectedObject = newtilez;

                    if (TilePropDraw == true) TileEngine.EditTileZ(new Vector4(newtilez.Location.X, newtilez.Location.Y, newtilez.Z, 0f), newtilez.Z);
                    else if (TilePropDraw == false) TileEngine.EditTypeHeight(new Vector4(newtilez.Location.X, newtilez.Location.Y, newtilez.Z, 0f), newtilez.TypeHeight);
                }
                else
                {
                    StopEdit = true;
                    TileEdit = false;
                    SelectedObject = null;
                    CurrentInterface = (int)InterfaceState.Stop;
                    DrawInterface = false;
                    Properties = false;
                }
            }

            //Reset the camera
            if (Input.CheckKeyPress(KeyboardState, Keys.OemQuotes) == true)
            {
                CameraLoc = DrawOffSet = Vector2.Zero;
            }

            //Reset the level offset
            if (Input.CheckKeyPress(KeyboardState, Keys.OemSemicolon) == true)
            {
                OffSet = Vector2.Zero;
            }

            //Move the camera's location to where the offset is
            if (Input.CheckKeyPress(KeyboardState, Keys.OemQuestion) == true)
            {
                CameraLoc = OffSet;
                DrawOffSet = -CameraLoc;
            }

            //Increase or decrease the stop point an object spawns at with the G and F keys respectively
            if (Input.CheckKeyPress(KeyboardState, Keys.F) == true)
                StopPointSpawn -= StopPointSpawn - 1 >= 0 ? 1 : 0;
            if (Input.CheckKeyPress(KeyboardState, Keys.G) == true)
                StopPointSpawn += 1;

            //Increase or decrease the minimum difficulty an object spawns at with the P and O keys respectively
            if (Input.CheckKeyPress(KeyboardState, Keys.O) == true)
                Difficulty -= Difficulty - 1 >= 0 ? 1 : 0;
            if (Input.CheckKeyPress(KeyboardState, Keys.P) == true)
                Difficulty += Difficulty + 1 < Enum.GetValues(typeof(Main.DifficultyLevel)).Length ? 1 : 0;

            //Increase or decrease the minimum number of players required for an object to spawn with the X and Z keys respectively
            if (Input.CheckKeyPress(KeyboardState, Keys.Z) == true)
                MinPlayers -= MinPlayers - 1 >= 1 ? 1 : 0;
            if (Input.CheckKeyPress(KeyboardState, Keys.X) == true)
                MinPlayers += MinPlayers + 1 < 5 ? 1 : 0;

            //Toggle whether an object is Designated or not with the L key
            if (Input.CheckKeyPress(KeyboardState, Keys.L) == true)
                Designated = !Designated;

            //Increase or decrease the index to insert an object with the End and Home keys respectively
            if (Input.CheckKeyPress(KeyboardState, Keys.Home))
                InsertIndex -= InsertIndex - 1 >= -1 ? 1 : 0;
            if (Input.CheckKeyPress(KeyboardState, Keys.End))
                InsertIndex += InsertIndex + 1 <= EnemPoints.Count ? 1 : 0;

            //Increase the size of the level
            if (TileEdit == true)
            {
                //The minimum length for a level is the size of the screen, so make sure you can't go lower than that
                if (TileEngine.GetTileLength(true) > (Main.ScreenSize.X / TileEngine.TileSize) && Input.CheckKeyPress(KeyboardState, Keys.OemMinus) == true)
                {
                    TileEngine.AddRemoveTiles(-1, 0);
                    LevelSize.X -= TileEngine.TileSize;
                    //Remove objects past that point
                    RemovePastBounds();
                }
                else if (Input.CheckKeyPress(KeyboardState, Keys.OemPlus) == true)
                {
                    TileEngine.AddRemoveTiles(1, 0);
                    LevelSize.X += TileEngine.TileSize;
                }

                if (TileEngine.GetTileLength(false) > (Main.ScreenSize.Y / TileEngine.TileSize) && Input.CheckKeyPress(KeyboardState, Keys.OemOpenBrackets) == true)
                {
                    TileEngine.AddRemoveTiles(0, -1);
                    LevelSize.Y -= TileEngine.TileSize;
                    RemovePastBounds();
                }
                else if (Input.CheckKeyPress(KeyboardState, Keys.OemCloseBrackets) == true)
                {
                    TileEngine.AddRemoveTiles(0, 1);
                    LevelSize.Y += TileEngine.TileSize;
                }

                if (Input.CheckKeyPress(KeyboardState, Keys.J) == true)
                    TileEngine.UpdateTileOffSet((int)-ScrollSpeed.X, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.K) == true)
                    TileEngine.UpdateTileOffSet((int)ScrollSpeed.X, 0);
                else if (Input.CheckKeyPress(KeyboardState, Keys.N) == true)
                    TileEngine.UpdateTileOffSet(0, (int)-ScrollSpeed.Y);
                else if (Input.CheckKeyPress(KeyboardState, Keys.M) == true)
                    TileEngine.UpdateTileOffSet(0, (int)ScrollSpeed.Y);

                //Change which tile property you're viewing; true is Z, false is TypeHeight, and null is TileType
                if (Input.CheckKeyPress(KeyboardState, Keys.O) == true)
                {
                    if (TilePropDraw == true) TilePropDraw = false;
                    else if (TilePropDraw == false) TilePropDraw = null;
                    else TilePropDraw = true;
                }
            }

            //Change the speed of scrolling through the map
            if (ScrollSpeed.X < 10f && Input.CheckKeyPress(KeyboardState, Keys.OemPeriod) == true)
                ScrollSpeed = new Vector2(++ScrollSpeed.X, ++ScrollSpeed.Y);

            if (ScrollSpeed.X > 1f && Input.CheckKeyPress(KeyboardState, Keys.OemComma) == true)
                ScrollSpeed = new Vector2(--ScrollSpeed.X, --ScrollSpeed.Y);

            if (Input.CheckKeyPress(KeyboardState, Keys.Escape) == true)
            {
                if (Properties == true) Properties = false;
                else if (SaveLoadDialog != null) SaveLoadDialog = null;
                else Main.SetState(Main.GameState.Screen);
            }

            //When editing an object, hold CTRL to set the position of the mouse to the object's location
            if (EditIndex > -1 && Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                if (CurrentInterface == (int)InterfaceState.Enemies)
                {
                    Mouse.SetPosition((int)EnemPoints[EditIndex].NewObject.GetLocationHeight.X + (int)DrawOffSet.X, (int)EnemPoints[EditIndex].NewObject.GetLocationHeight.Y + (int)DrawOffSet.Y);
                }
                else if (CurrentInterface == (int)InterfaceState.Containers)
                {
                    Mouse.SetPosition(ContPoints[EditIndex].NewObject.CollisionBox.X + (int)DrawOffSet.X, ContPoints[EditIndex].NewObject.CollisionBox.Y + (int)DrawOffSet.Y);
                }
                else if (CurrentInterface == (int)InterfaceState.Hazards)
                {
                    Mouse.SetPosition((int)HazardPoints[EditIndex].NewObject.GetLocationHeight.X + (int)DrawOffSet.X, (int)HazardPoints[EditIndex].NewObject.GetLocationHeight.Y + (int)DrawOffSet.Y);
                }
            }
        }

        public void Update(float activeTime, Main main)
        {
            if (Properties == false && SaveLoadDialog == null)
            {
                MouseInput(main);
                KeyboardInput();
            }
            else
            {
                PropWindow.Update(KeyboardState);
                
                if (PropWindow.Exited() == true)
                {
                    Properties = false;
                    SaveLoadDialog = null;
                    EditIndex = -1;
                }
                else if (PropWindow.Done() == true)
                {
                    Properties = false;
                    if (SaveLoadDialog == true)
                    {
                        //Make sure a valid filename is specified
                        if (String.IsNullOrEmpty(PropWindow.GetValues()[0]) == false)
                        {
                            SaveLevel(PropWindow.GetValues()[0]);

                            FileName = String.IsNullOrEmpty(PropWindow.GetValues()[0]) == true ? "Untitled" : PropWindow.GetValues()[0];
                        }
                    }
                    else if (SaveLoadDialog == false)
                    {
                        if ((File.Exists("Content\\Levels\\" + PropWindow.GetValues()[0]) == true))
                        {
                            LevelData loadedlvl = LoadLevelOld(PropWindow.GetValues()[0]);

                            StopPoints = loadedlvl.StopPoints;
                            EnemPoints = loadedlvl.EnemPoints;
                            EnemyParams = loadedlvl.Enemies;
                            ContPoints = loadedlvl.ContPoints;
                            ContainerParams = loadedlvl.Containers;
                            TileEngine = loadedlvl.TileEngine;
                            HazardPoints = loadedlvl.HazardPoints;
                            HazardParams = loadedlvl.Hazards;
                            LevelSize = new Vector2(loadedlvl.TileEngine.GetTileLength(true) * TileEngine.TileSize, loadedlvl.TileEngine.GetTileLength(false) * TileEngine.TileSize);

                            FileName = String.IsNullOrEmpty(PropWindow.GetValues()[0]) == true ? "Untitled" : PropWindow.GetValues()[0];
                        }
                    }
                    SaveLoadDialog = null;
                }
            }

            KeyboardState = Keyboard.GetState();
            MouseState = Mouse.GetState();

            foreach (ObjectSpawnPoint<Enemy> e in EnemPoints)
                e.NewObject.status.UpdateTint();
            foreach (ObjectSpawnPoint<Hazard> h in HazardPoints)
            {
                //Update the tint of the hazard's status (if it has one) so you can see which status the hazard has
                System.Reflection.FieldInfo status = h.NewObject.GetType().GetField("status", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (status != null)
                {
                    Status hazardstatus = (Status)status.GetValue(h.NewObject);
                    hazardstatus.UpdateTint();
                }
            }
        }

        //Removes an object past the level bounds if you reduce the size of the level
        private void RemovePastBounds()
        {
            for (int i = 0; i < EnemPoints.Count; i++)
            {
                if (EnemPoints[i].NewObject.FeetLoc.X  > (LevelSize.X + TileEngine.TileSize) || EnemPoints[i].NewObject.FeetLoc.Y > (LevelSize.Y + TileEngine.TileSize))
                {
                    EnemPoints.RemoveAt(i);
                    EnemyParams.RemoveAt(i);
                    i--;
                }
            }
        }

        private String DisplayDifficulty(int difficulty)
        {
            //As the numbers get higher, so does the difficulty
            switch (difficulty)
            {
                case 0: return "VE";
                case 1: return "E";
                case 2: return "N";
                case 3: return "H";
                default: return "VH";
            }
        }

        public void Draw(float activeTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Background, new Rectangle((int)DrawOffSet.X, (int)DrawOffSet.Y, LoadGraphics.BG.Width, LoadGraphics.BG.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, Convert.ToString(MouseState.X - (int)DrawOffSet.X) + "," + Convert.ToString(MouseState.Y - (int)DrawOffSet.Y), Vector2.Zero, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, Convert.ToString((int)OffSet.X) + "," + Convert.ToString((int)OffSet.Y), new Vector2(0, Main.ScreenSize.Y - LoadGraphics.HUDFont.MeasureString("0").Y), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "StopPoint:" + Convert.ToString(StopPointSpawn), new Vector2(70, Main.ScreenSize.Y + 5- LoadGraphics.HUDFont.MeasureString("0").Y), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Diff:" + DisplayDifficulty(Difficulty), new Vector2(180, Main.ScreenSize.Y + 5 - LoadGraphics.HUDFont.MeasureString("0").Y), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "MinP:" + MinPlayers, new Vector2(260, Main.ScreenSize.Y + 5 - LoadGraphics.HUDFont.MeasureString("0").Y), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Des:" + Designated.ToString(), new Vector2(325, Main.ScreenSize.Y + 5 - LoadGraphics.HUDFont.MeasureString("0").Y), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "InsInd:" + InsertIndex.ToString(), new Vector2(Main.ScreenSize.X - LoadGraphics.HUDFont.MeasureString("InsInd:" + InsertIndex.ToString()).X, 0), Color.Black, 0f, Vector2.Zero, .8f, SpriteEffects.None, .9992f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, FileName, new Vector2((Main.ScreenSize.X / 2) - (int)(LoadGraphics.HUDFont.MeasureString(FileName).X / 2), 0), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9992f);

            TileEngine.Draw(spriteBatch, DrawOffSet);

            if (TileEdit == false && SelectedObject != null)
            {
                //If the enemy is being edited, show the place in the list that the enemy currently is
                String placeinlist = String.Empty;
                if (EditIndex > -1) placeinlist = " - " + EditIndex;

                switch (CurrentInterface)
                {
                    case (int)InterfaceState.Enemies: spriteBatch.Draw(LoadGraphics.enemsprite, new Vector2(MouseState.X, MouseState.Y), null, Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
                        spriteBatch.DrawString(LoadGraphics.HUDFont, Enum.GetName(typeof(Enemy.Enemies), SelectedObject) + placeinlist, new Vector2(176, 80), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                        break;
                    case (int)InterfaceState.Containers: spriteBatch.Draw(LoadGraphics.ContainerSprite, new Vector2(MouseState.X, MouseState.Y), new Rectangle(0, 0, 32, 52), Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
                        spriteBatch.DrawString(LoadGraphics.HUDFont, Enum.GetName(typeof(ItemContainer.Containers), SelectedObject) + placeinlist, new Vector2(176, 80), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                        break;
                    case (int)InterfaceState.Hazards: spriteBatch.Draw(Hazards[(int)SelectedObject], new Vector2(MouseState.X, MouseState.Y), null, Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 1f);
                        spriteBatch.DrawString(LoadGraphics.HUDFont, Enum.GetName(typeof(Hazard.Hazards), SelectedObject) + placeinlist, new Vector2(176, 80), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                        break;
                }
            }

            if (DrawInterface == true)
            {
                spriteBatch.Draw(LoadGraphics.FG, new Vector2(Main.ScreenSize.X - 200, 0), null, Color.CornflowerBlue, 0f, Vector2.Zero, new Vector2(LoadGraphics.FG.Width / Main.ScreenSize.X, LoadGraphics.FG.Height * (Main.ScreenSize.Y / (float)LoadGraphics.FG.Height)), SpriteEffects.None, .999f);
                
                //Draw the selections
                if (CurrentInterface == (int)InterfaceState.Enemies)
                {
                    int values = Enum.GetValues(typeof(Enemy.Enemies)).Length;
                    for (int i = 0; i < values; i++)
                        spriteBatch.Draw(LoadGraphics.enemsprite, new Vector2(Main.ScreenHalf.X + 10 + (50 * (i%4)), 10 + (70 * (i/4))), null, Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, .9991f);
                }
                else if (CurrentInterface == (int)InterfaceState.Containers)
                {
                    int values = Enum.GetValues(typeof(ItemContainer.Containers)).Length;
                    for (int i = 0; i < values; i++)
                        spriteBatch.Draw(LoadGraphics.ContainerSprite, new Vector2(Main.ScreenHalf.X + 10 + (50 * (i % 4)), 10 + (70 * (i / 4))), new Rectangle(0, 0, 32, 52), Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, .9991f);
                }
                else if (CurrentInterface == (int)InterfaceState.Hazards)
                {
                    int values = Enum.GetValues(typeof(Hazard.Hazards)).Length;
                    for (int i = 0; i < values; i++)
                        spriteBatch.Draw(Hazards[i], new Vector2(Main.ScreenHalf.X + 10 + (50 * (i % 4)), 10 + (70 * (i / 4))), null, Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, .9991f);
                }
            }

            float scale = .8f;

            for (int i = 0; i < EnemPoints.Count; i++)
            {
                EnemPoints[i].NewObject.Draw(spriteBatch, DrawOffSet, TileEngine);

                //View the info for the enemy if the mouse is hovered over it
                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(EnemPoints[i].NewObject.CollisionBox))
                {
                    String type = EnemyParams[i].objecttype;
                    String behavior = EnemyParams[i].param.Length == 4 ? "\nBehavior: " + EnemyParams[i].param[2] : String.Empty;
                    if (type == "Imposter") behavior = "\nStartingMask: " + EnemyParams[i].param[2];
                    if (type == "MinecartRider") behavior = String.Empty;
                    if (type == "Diver") behavior = "\nDiveRate: " + EnemyParams[i].param[4] + "\nBehavior: " + EnemyParams[i].param[5];
                    String designated = "\nDesignated: " + EnemyParams[i].Designated;
                    String location = (type != "MinecartRider" && type != "Diver") ? "\nLocation: " + EnemyParams[i].param[0] : "\nLocation: " + EnemyParams[i].param[1];
                    String named = (type == "MinecartRider" || type == "Diver") ? "\nNamed: " + EnemyParams[i].param[0] : String.Empty;
                    String status = (type != "MinecartRider" && type != "Diver") ? "\nStatus: " + EnemyParams[i].param[1] : "\nStatus: " + EnemyParams[i].param[2];
                    String diffplay = "\nDifficulty: " + DisplayDifficulty(EnemyParams[i].difficulty) + "\nPlayers: " + EnemyParams[i].minplayers;

                    String drawstring = ("Name: " + type + " - " + i) + named + status + "\nOffset: " + EnemyParams[i].objectoffset + location + behavior + designated + "\nStoppoint: " + EnemyParams[i].stoppoint + diffplay;
                    
                    Vector2 stringlength = LoadGraphics.HUDFont.MeasureString(drawstring);
                    Vector2 DrawLoc = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                    if (Mouse.GetState().X + (stringlength.X * scale) > Main.ScreenSize.X) DrawLoc.X -= (stringlength.X * scale);
                    if (Mouse.GetState().Y + (stringlength.Y * scale) > Main.ScreenSize.Y)
                    {
                        DrawLoc.Y -= (stringlength.Y * scale);
                        if (DrawLoc.Y < 0) DrawLoc.Y += (stringlength.Y * scale) / 2;
                    }

                    spriteBatch.DrawString(LoadGraphics.HUDFont, drawstring, DrawLoc, Color.Black, 0f, new Vector2(-12, 0), scale, SpriteEffects.None, .998f);
                }
            }
            for (int i = 0; i < ContPoints.Count; i++)
            {
                ContPoints[i].NewObject.Draw(spriteBatch, DrawOffSet, TileEngine);
                
                //View the info for the container if the mouse is hovered over it
                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(ContPoints[i].NewObject.CollisionBox))
                {
                    String drawstring = ("Name: " + ContainerParams[i].objecttype + " - " + i) + "\nOffset: " + ContainerParams[i].objectoffset + "\nLocation: " + ContainerParams[i].param[0] + "\nDesignated: " + ContPoints[i].Designated +"\nStoppoint: " + ContainerParams[i].stoppoint + "\nHealth: " + ContainerParams[i].param[1] + "\nItem: " + ContainerParams[i].param[2] + "\nWeapon: " + ContainerParams[i].param[3] + "\nDifficulty: " + DisplayDifficulty(ContainerParams[i].difficulty) + "\nPlayers: " + ContainerParams[i].minplayers;

                    Vector2 stringlength = LoadGraphics.HUDFont.MeasureString(drawstring);
                    Vector2 DrawLoc = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                    if (Mouse.GetState().X + (stringlength.X * scale) > Main.ScreenSize.X) DrawLoc.X -= (stringlength.X * scale);
                    if (Mouse.GetState().Y + (stringlength.Y * scale) > Main.ScreenSize.Y)
                    {
                        DrawLoc.Y -= (stringlength.Y * scale);
                        if (DrawLoc.Y < 0) DrawLoc.Y += (stringlength.Y * scale) / 2;
                    }

                    spriteBatch.DrawString(LoadGraphics.HUDFont, drawstring, DrawLoc, Color.Black, 0f, new Vector2(-12, 0), scale, SpriteEffects.None, .998f);
                }
            }
            for (int i = 0; i < HazardPoints.Count; i++)
            {
                HazardPoints[i].NewObject.Draw(spriteBatch, DrawOffSet, TileEngine);

                //View the info for the hazard if the mouse is hovered over it
                if (new Rectangle((int)(MouseState.X + CameraLoc.X), (int)(MouseState.Y + CameraLoc.Y), 1, 1).Intersects(HazardPoints[i].NewObject.CollisionBox))
                {
                    String type = HazardParams[i].objecttype;
                    String designated = "\nDesignated: " + HazardPoints[i].Designated;
                    String location = String.Empty;
                    String status = String.Empty;
                    String hitboxstrength = String.Empty;
                    String shootertype = String.Empty;
                    String shootlength = String.Empty;

                    if (type == "FallingRock")
                    {
                        location = HazardParams[i].param[2];
                        status = "\nStatus: " + HazardParams[i].param[3];
                        hitboxstrength = "\nStrength: " + HazardParams[i].param[4];
                    }
                    else if (type == "BSShooter")
                    {
                        if (HazardParams[i].param[0] == "true") shootertype = "\nType: Bullet\nShootRate: " + HazardParams[i].param[3];
                        else shootertype = "\nType: Spike\nShootRate: " + HazardParams[i].param[3];
                        shootertype += "\nDamage: " + HazardParams[i].param[4];
                        location = HazardParams[i].param[1];
                    }
                    else if (type == "PoisonGasShooter")
                    {
                        location = HazardParams[i].param[0];
                        shootlength = "\nShootLength: " + HazardParams[i].param[2] + "\nShootTime: " + HazardParams[i].param[3] + "\nStopTime: " + HazardParams[i].param[4];
                    }
                    else if (type == "Scarecrow")
                    {
                        location = HazardParams[i].param[0];
                    }
                    else if (type == "Platform")
                    {
                        location = HazardParams[i].param[0];
                        shootlength = "\nJumpthrough: " + HazardParams[i].param[1];
                    }

                    String drawstring = ("Name: " + type + " - " + i) + status + "\nOffset: " + HazardParams[i].objectoffset + "\nLocation: " + location + designated + "\nStoppoint: " + HazardParams[i].stoppoint + hitboxstrength + shootertype + shootlength + "\nDifficulty: " + DisplayDifficulty(HazardParams[i].difficulty) + "\nPlayers: " + HazardParams[i].minplayers;

                    Vector2 stringlength = LoadGraphics.HUDFont.MeasureString(drawstring);
                    Vector2 DrawLoc = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                    if (Mouse.GetState().X + (stringlength.X * scale) > Main.ScreenSize.X) DrawLoc.X -= (stringlength.X * scale);
                    if (Mouse.GetState().Y + (stringlength.Y * scale) > Main.ScreenSize.Y)
                    {
                        DrawLoc.Y -= (stringlength.Y * scale);
                        if (DrawLoc.Y < 0) DrawLoc.Y += (stringlength.Y * scale) / 2;
                    }

                    spriteBatch.DrawString(LoadGraphics.HUDFont, drawstring, DrawLoc, Color.Black, 0f, new Vector2(-12, 0), scale, SpriteEffects.None, .998f);
                }
            }

            if (TileEdit == true || Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            {
                TileEngine.DrawHeight(spriteBatch, CameraLoc, DrawOffSet, TilePropDraw);
            }

            if (StopEdit == true)
            {
                //Draw lines showing the camera's location
                spriteBatch.Draw(LoadGraphics.FG, new Vector2((int)((Main.ScreenSize.X / 2)), 0), null, Color.CornflowerBlue, 0f, Vector2.Zero, new Vector2((float)1/(float)LoadGraphics.FG.Width, LoadGraphics.FG.Height * (Main.ScreenSize.Y / (float)LoadGraphics.FG.Height)), SpriteEffects.None, .995f);
                spriteBatch.Draw(LoadGraphics.FG, new Vector2(0, (int)((Main.ScreenSize.Y / 2))), null, Color.CornflowerBlue, 0f, Vector2.Zero, new Vector2(LoadGraphics.FG.Width * (Main.ScreenSize.X / (float)LoadGraphics.FG.Width), (float)1/(float)LoadGraphics.FG.Height), SpriteEffects.None, .994f);

                for (int i = 0; i < StopPoints.Count; i++)
                {
                    Vector2 drawloc = new Vector2(Main.ScreenHalf.X + StopPoints[i].X + DrawOffSet.X, Main.ScreenHalf.Y + StopPoints[i].Y + DrawOffSet.Y);

                    spriteBatch.DrawString(LoadGraphics.HUDFont, "S" + i, drawloc, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .993f);
                    if (new Rectangle(MouseState.X, MouseState.Y, 1, 1).Intersects(new Rectangle((int)drawloc.X, (int)drawloc.Y, (int)LoadGraphics.HUDFont.MeasureString("S" + i).X, (int)LoadGraphics.HUDFont.MeasureString("S" + i).Y)))
                        spriteBatch.DrawString(LoadGraphics.HUDFont, "OffSet:" + (int)StopPoints[i].X + "," + (int)StopPoints[i].Y, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                }
            }

            if ((Properties == true || SaveLoadDialog != null) && PropWindow != null)
                PropWindow.Draw(spriteBatch);
        }

        //For editing properties of objects, like health, etc; its values are passed into the constructors
        private class PropertiesWindow
        {
            private String[] DrawValues;
            private String[] Values;
            private int CurValue;
            private bool Exit;

            public PropertiesWindow()
            {
                CurValue = 0;
                Exit = false;
            }

            public PropertiesWindow(params String[] drawvalues)
            {
                DrawValues = drawvalues;
                Values = new String[DrawValues.Length];
                CurValue = 0;
                Exit = false;
            } 

            public PropertiesWindow(System.Reflection.ParameterInfo[] drawvalues, String[] values = null)
            {
                DrawValues = new String[drawvalues.Length];
                for (int i = 0; i < drawvalues.Length; i++)
                DrawValues[i] = drawvalues[i].Name;
                Values = new String[DrawValues.Length];
                if (values != null)
                {
                    for (int i = 0; i < Values.Length; i++)
                        Values[i] = values[i];
                }

                CurValue = 0;
                Exit = false;
            }

            public bool Exited()
            {
                return Exit;
            }

            public bool Done()
            {
                return CurValue >= DrawValues.Length;
            }

            public String[] GetValues()
            {
                return Values;
            }

            public void Update(KeyboardState KeyboardState)
            {
                Keys[] keys = Keyboard.GetState().GetPressedKeys();

                for (int i = 0; i < Keyboard.GetState().GetPressedKeys().Length; i++)
                {
                    Keys pressedkey = Keyboard.GetState().GetPressedKeys()[i];

                    if (((pressedkey >= Keys.D0 && pressedkey <= Keys.Z) || pressedkey == Keys.OemComma || pressedkey == Keys.OemMinus || pressedkey == Keys.Enter || pressedkey == Keys.Back || pressedkey == Keys.Escape) && Input.CheckKeyPress(KeyboardState, pressedkey) == true)
                    {
                        if (pressedkey != Keys.Enter && pressedkey != Keys.Back && pressedkey != Keys.Escape)
                        {
                            if (pressedkey >= Keys.D0 && pressedkey <= Keys.D9)
                            {
                                Values[CurValue] += (int)((int)pressedkey - Keys.D0);
                            }
                            else
                            {
                                if (pressedkey == Keys.OemComma)
                                    Values[CurValue] += ",";
                                else if (pressedkey == Keys.OemMinus)
                                    Values[CurValue] += "-";
                                else if (KeyboardState.IsKeyDown(Keys.LeftShift) || KeyboardState.IsKeyDown(Keys.RightShift))
                                    Values[CurValue] += pressedkey.ToString().ToUpper();
                                else Values[CurValue] += pressedkey.ToString().ToLower();
                            }
                        }
                        else if (pressedkey == Keys.Enter)
                        {
                            CurValue++;
                            break;
                        }
                        else if (pressedkey == Keys.Escape)
                        {
                            Exit = true;
                        }
                        else if (String.IsNullOrEmpty(Values[CurValue]) == false && pressedkey == Keys.Back)
                        {
                            Values[CurValue] = Values[CurValue].Remove(Values[CurValue].Length - 1, 1);
                            break;
                        }
                    }
                }
            }

            public void Draw(SpriteBatch spriteBatch)
            {
                if (DrawValues != null)
                {
                    for (int i = 0; i < DrawValues.Length; i++)
                    {
                        float scale = .9f;
                        if (DrawValues[i].ToLower() == "helditem" || DrawValues[i].ToLower() == "heldweapon") scale = .65f;
                        spriteBatch.DrawString(LoadGraphics.HUDFont, DrawValues[i] + ": " + Values[i], new Vector2(135, 133 + (i * 25)), CurValue == i ? Color.Green : Color.Black, 0f, Vector2.Zero, scale, SpriteEffects.None, 1f);
                    }
                }
            }
        }
    }

    [Serializable]
    //Store all the level data; that is, the tile engine, the stop points, and the enemy, container, and hazard spawn points
    public struct LevelData
    {
        public List<ObjectData> Enemies;
        public List<ObjectData> Containers;
        public List<ObjectData> Hazards;

        public List<Vector2> StopPoints;
        public List<ObjectSpawnPoint<Enemy>> EnemPoints;
        public List<ObjectSpawnPoint<ItemContainer>> ContPoints;
        public List<ObjectSpawnPoint<Hazard>> HazardPoints;
        public TileEngine TileEngine;

        public LevelData(List<Vector2> stop, List<ObjectData> enemies, List<ObjectData> containers, List<ObjectData> hazards, TileEngine tileengine)
        {
            StopPoints = stop;
            EnemPoints = new List<ObjectSpawnPoint<Enemy>>();
            Enemies = enemies;
            ContPoints = new List<ObjectSpawnPoint<ItemContainer>>();
            Containers = containers;
            HazardPoints = new List<ObjectSpawnPoint<Hazard>>();
            Hazards = hazards;
            TileEngine = tileengine;
        }
    }

    [DataContract]
    public struct ObjectData
    {
        [DataMember]
        //The parameters for the object's constructor
        public String[] param;

        [DataMember]
        //The stop point the object spawns at
        public int stoppoint;

        [DataMember]
        //The minimum difficulty level the object spawns at
        public int difficulty;

        //The minimum number of players required for the object to spawn (used for increasing the difficulty with more players)
        [DataMember]
        public int minplayers;

        //Whether the object needs to be destroyed for the camera to move on or not
        [DataMember]
        public bool Designated;

        [DataMember]
        //The camera location the object spawns at
        public String objectoffset;

        [DataMember]
        //The name and number representation of the object's type; this is used to determine which method to invoke
        public String objecttype;
        [DataMember]
        public int typeobject;
        
        public ObjectData(int typeobj, String type, int stop, String offset, int diff, int minplay, bool designated, params String[] parameters)
        {
            stoppoint = stop;
            objectoffset = offset;
            typeobject = typeobj;
            objecttype = type;
            param = parameters;
            difficulty = diff;
            minplayers = minplay;
            Designated = designated;
        }
    }
}
