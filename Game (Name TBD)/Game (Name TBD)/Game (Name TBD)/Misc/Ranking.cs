﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //Handles rankings, from E to A, for challenges and levels and are based on time remaining (for challenge levels only; normal levels already take time into account), remaining health and lives (and continues for main game), and total points
    //At the end of each level for the main game, calculate the rank; at the end of the game average the rankings to determine the final rank the player(s) got
    public class Ranking
    {
        //The rank; it goes from E to A (0 to 4)
        private int Rank;

        //The score or time; it shows how many points a challenge was completed with or how fast a challenge was completed
        private float ScoreTime;

        //The scores needed for each rank; the first one is what is needed to get a D, the second one is what is needed to get a C, and so on
        private float[] ScoreDiff;

        //The strings to look for when reading in the corresponding rank properties
        public const String RankRead = "Rank";
        public const String ScoreTimeRead = "ScoreTime";

        //Constructor - Rank starts out at E (0), and scorediff should always be an array of 4 integers since you don't need a score to get E rank
        public Ranking(params float[] scorediff)
        {
            Rank = 0;
            ScoreTime = 0f;

            ScoreDiff = scorediff;
        }

        //Gets the letter rank corresponding to its number value
        public static char LetterValue(int numericalvalue)
        {
            return (char)(69 - numericalvalue);
        }

        //Checks if the challenge is complete (this may vary from challenge to challenge, but right now this method works for all of them so change it later if necessary)
        public bool Completed
        {
            get { return (ScoreTime > 0f); }
        }

        //Loads data containing the ranks and scores/time for the challenge
        public void Load(int rank, float scoretime)
        {
            Rank = rank;
            ScoreTime = scoretime;
        }

        //Gets the current rank
        public int GetRank()
        {
            return Rank;
        }

        //Resets the current rank
        public void ResetRank()
        {
            Rank = 0;
        }

        //Gets the current score or time
        public float GetScoreTime()
        {
            return ScoreTime;
        }

        //Resets the current score or time
        public void ResetScoreTime()
        {
            ScoreTime = 0f;
        }

        //Gets the current ranking the player(s) got on that particular playthrough of the challenge
        public int GetCurrentPerformance(float playerscore, bool increasingscores = true)
        {
            int CurRank = 0;

            for (int i = 0; i < ScoreDiff.Length; i++)
            {
                bool scorecomparison = increasingscores == true ? playerscore >= ScoreDiff[CurRank] : playerscore <= ScoreDiff[CurRank];
                if (scorecomparison == false) break;

                CurRank++;
            }

            return CurRank;
        }

        //Checks the rank at the end of the level/challenge
        public void RankPerformance(float playerscore, bool increasingscores = true)
        {
            //If the player already has an A rank then don't bother checking for the new one
            if (Rank < 4)
            {
                //Get the score needed for the next rank from the rank the player is currently at
                float TempScore = ScoreDiff[Rank];

                //Some challenges may have ranks that increase as the scores decrease, so see which one is the case and compare it
                bool scorecomparison = increasingscores == true ? playerscore >= TempScore : playerscore <= TempScore;

                //Go through each score value past the score for the current rank and see if the player's score exceeds it
                while (scorecomparison == true)
                {
                    //If the player's score is higher than the score needed to rank up, increase the rank and get the next score value, if it exists
                    Rank++;
                    if (Rank == 4) break;
                    TempScore = ScoreDiff[Rank];
                    scorecomparison = increasingscores == true ? playerscore >= TempScore : playerscore <= TempScore;
                }
            }
        }

        //Checks the score or time at the end of the challenge
        public void RankScoreTime(float playerscoretime, bool decreasingscoretime = false)
        {
            //Some challenges may have scores or times that are better the higher they are, so see which one is the case and compare it
            if (decreasingscoretime == true)
            {
                if (playerscoretime < ScoreTime) ScoreTime = playerscoretime;
            }
            else
            {
                if (playerscoretime > ScoreTime) ScoreTime = playerscoretime;
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 Position, Color color)
        {
            //Draw the letter rank graphic
            //spriteBatch.Draw(LoadGraphics.LetterGraphic[Rank], new Vector2(Position.X + 20, Position.Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);

            //Temporary way of displaying the rank until I get proper letter graphics
            char Rankchar = LetterValue(Rank);

            spriteBatch.DrawString(LoadGraphics.HUDFont, "Ranking: " + Rankchar, Position, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
        }
    }
}
