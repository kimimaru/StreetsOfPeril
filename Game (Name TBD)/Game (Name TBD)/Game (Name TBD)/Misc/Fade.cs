﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    /// <summary>
    /// A class for fading things in and out
    /// </summary>
    public class Fade
    {
        //The current fade, the amount the current fade gets increased by, and whether the fade should be fading or not
        private int CurFade;
        private int FadeAmount;
        private bool ShouldFade;

        //The min and max fade values, and the time between fades
        private int MinFade;
        private int MaxFade;
        private float FadeTime;
        private float PrevFade;

        //Tells when a fade is done
        private bool DoneFading;

        public Fade(int faderate, int minfade, int maxfade, float fadetime = 0f)
        {
            FadeAmount = faderate;
            MinFade = minfade;
            MaxFade = maxfade;
            
            FadeTime = fadetime;
            PrevFade = 0f;

            if (FadeAmount < 0) CurFade = MaxFade;
            else CurFade = MinFade;

            ShouldFade = true;
            DoneFading = false;
        }

        //An empty fade
        public static Fade EmptyFade
        {
            get { return new Fade(0, 0, 0); }
        }

        public int CurFadeVal
        {
            get { return CurFade; }
        }

        public int MinFadeVal
        {
            get { return MinFade; }
        }

        public int MaxFadeVal
        {
            get { return MaxFade; }
        }

        public bool Finished()
        {
            return DoneFading;
        }

        public bool IsFading()
        {
            return ShouldFade;
        }

        public bool IsFadingIn()
        {
            return (FadeAmount < 0);
        }

        public void ContinueFade()
        {
            ShouldFade = !ShouldFade;
        }

        public void Update(float activeTime)
        {
            if (ShouldFade == true)
            {
                if ((activeTime - PrevFade) >= FadeTime)
                {
                    CurFade += FadeAmount;

                    //Fading out
                    if (CurFade >= MaxFade)
                    {
                        CurFade = MaxFade;
                        DoneFading = true;
                    }
                    //Fading in
                    else if (CurFade <= MinFade)
                    {
                        CurFade = MinFade;
                        FadeAmount = -FadeAmount;
                        ShouldFade = false;
                    }

                    PrevFade = activeTime;
                }
            }
        }

        //Update the fade without ever ending it
        public void UpdateContinuous(float activeTime)
        {
            if ((activeTime - PrevFade) >= FadeTime)
            {
                CurFade += FadeAmount;

                //Fading out
                if (CurFade >= MaxFade)
                {
                    CurFade = MaxFade;
                    FadeAmount = -FadeAmount;
                }
                //Fading in
                else if (CurFade <= MinFade)
                {
                    CurFade = MinFade;
                    FadeAmount = -FadeAmount;
                }

                PrevFade = activeTime;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (ShouldFade == true)
                spriteBatch.Draw(LoadGraphics.BlackFade, Vector2.Zero, new Rectangle(0, 0, 1, 1), new Color(0, 0, 0, CurFade), 0f, new Vector2(0, 0), new Vector2(Main.ScreenSize.X, Main.ScreenSize.Y), SpriteEffects.None, .998f);
        }
    }
}
