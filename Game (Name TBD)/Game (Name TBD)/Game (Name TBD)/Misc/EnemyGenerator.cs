﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //This class is for Onslaught, the challenge stage where enemies come at you in waves - this class handles the randomizing of enemies and the waves
    public class EnemyGenerator
    {
        private SubLevel subLevel;

        private Random RandomNumber;
        private int WaveNum;

        //The number of enemies created per wave - 10, 15, or a higher # of enemies per wave (10 for now)
        private int EnemWaveNum;
        public int EnemLeft;

        //The recovery time in between waves
        private float WaveTime;
        private float PrevWaveTime;

        //Saves the current highest wave
        public float BestRecord;

        //Constructor - Max number of waves is 50 or 100 (50 for now)
        public EnemyGenerator(SubLevel level)
        {
            subLevel = level;

            RandomNumber = new Random();
            
            WaveNum = 0;
            EnemWaveNum = 0;
            EnemLeft = 10;
            WaveTime = 5000f;
            PrevWaveTime = 0f;

            BestRecord = CLevel.ChallengeRanks[(int)CLevel.Challenges.Onslaught][OnslaughtScreen.Difficulty - 1].GetScoreTime();
        }

        public int GWaveNum
        {
            get { return WaveNum; }
        }

        //Creates a random enemy; harder enemies are refrained from being created until later waves
        private Enemy CreateRandomEnemy()
        {
            int newrand = RandomNumber.Next(4);
            int randloc = RandomNumber.Next(4);
            Vector3 enemlocation;

            //Increment the number of enemies created in this wave (if this method gets called, it is returning an enemy no matter what)
            EnemWaveNum++;

            //Check for locations
            if (randloc == 0) enemlocation = new Vector3(-TileEngine.TileSize * 2, 200, 0f);
            else if (randloc == 1) enemlocation = new Vector3(-TileEngine.TileSize * 2, 100, 0f);
            else if (randloc == 2) enemlocation = new Vector3(Main.ScreenSize.X + (TileEngine.TileSize * 2), 300, 0f);
            else if (randloc == 3) enemlocation = new Vector3((TileEngine.TileSize * 2), 270, 0f);
            else enemlocation = new Vector3(100, 100, 0f);

            //If statements are used because they can check for wave numbers easier
            if (newrand == 0)
                return CreateObjects.Mark(enemlocation, new Status());
            else if (newrand == 1)
                return CreateObjects.Christopher(enemlocation, new Status());
            else if (newrand == 2)
            {
                if (WaveNum >= 10)
                    return CreateObjects.Greg(enemlocation, new Status((int)Status.Statuses.Poison, 5000));
                else return CreateObjects.Christopher(enemlocation, new Status((int)Status.Statuses.DamageDown, 7000));
            }
            else if (newrand == 3)
            {
                if (WaveNum >= 20)
                    return CreateObjects.Grunt(enemlocation, new Status((int)Status.Statuses.Poison, 5000));
                else
                    return CreateObjects.Mark(enemlocation, new Status());
            }
            else if (newrand == 4)
                return CreateObjects.Paul(enemlocation, new Status());
            else
                return CreateObjects.Mark(enemlocation, new Status());
        }

        //Updates the generator
        public void Update(float activeTime)
        {
            //Check if a wave is over and set the recovery time
            if (EnemWaveNum >= 10 && subLevel.GetEnemies.Count == 0)
            {
                WaveNum++;

                //Automatically record the highest wave the player reached if the wave reached was greater than the previous highest wave reached
                if (CLevel.ChallengeRanks[(int)CLevel.Challenges.Onslaught][OnslaughtScreen.Difficulty - 1].GetScoreTime() < WaveNum)
                {
                    int challengenum = (int)CLevel.Challenges.Onslaught;
                    int difficulty = (OnslaughtScreen.Difficulty - 1);

                    //Calculate the rank and store the wave number
                    CLevel.ChallengeRanks[challengenum][difficulty].RankPerformance(WaveNum);
                    CLevel.ChallengeRanks[challengenum][difficulty].RankScoreTime(WaveNum);

                    //Save the rank and wave number
                    SaveLoadData.SaveChallenge(CLevel.ChallengeRanks[challengenum][difficulty], challengenum, difficulty);
                }

                EnemWaveNum = 0;
                EnemLeft = 10;
                PrevWaveTime = activeTime;
            }

            //Don't create enemies if the wave recovery time is still up
            if ((activeTime - PrevWaveTime) >= WaveTime)
            {
                //Helps keep things reasonable so the player doesn't get flooded with so many enemies
                if (EnemWaveNum < 10 && subLevel.GetEnemies.Count < 7)
                {
                    subLevel.AddObject<Enemy>(CreateRandomEnemy(), subLevel.GetEnemies);
                }
            }
        }

        //Draws the wave number on the top of the screen
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Wave: " + Convert.ToString(WaveNum + 1), new Vector2(195, 5), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Enemies Left: " + Convert.ToString(EnemLeft), new Vector2(169, 30), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
        }
    }
}