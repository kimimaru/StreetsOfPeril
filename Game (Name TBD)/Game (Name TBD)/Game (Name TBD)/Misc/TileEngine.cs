﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace Game__Name_TBD_
{
    //The tile engine managing all the tiles in the level
    [Serializable]
    public class TileEngine
    {
        public enum TileTypes
        {
            None, Water, Grass, Museum
        };

        //Gravity values; water has a lower gravity, so objects decelerate slower inside it
        public const float Gravity = .2f;

        //How fast things fall in water
        public const float WaterGravity = .12f;

        private Tile[][] Tiles;

        //The offset for where the tiles are created so you can create tiles in spots with negative locations
        private Vector2 TileOffSet;
        
        //The size of each tile
        public const int TileSize = 32;

        public TileEngine()
        {
            TileOffSet = Vector2.Zero;

            Tiles = new Tile[1][];
            Tiles[0] = new Tile[1];
            Tiles[0][0] = new Tile(new Vector3(0, 0, 0), (int)TileTypes.None, 0f);
        }

        //Default constructor - Z values are initialized to 0, tiletypes are None, and typeheights are 0
        //i makes up the columns, and j makes up the rows
        public TileEngine(Vector2 levelsize, int tileoffsetX = 0, int tileoffsetY = 0)
        {
            TileOffSet = new Vector2(tileoffsetX, tileoffsetY);

            Tiles = new Tile[(int)levelsize.X / TileSize][];

            for (int i = 0; i < Tiles.Length; i++)
            {
                Tiles[i] = new Tile[(int)levelsize.Y / TileSize];

                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    Tiles[i][j] = new Tile(new Vector3((i * TileSize) + TileOffSet.X, (j * TileSize) + TileOffSet.Y, /*i % 4 == 0 ? 0f : 40f*/0f), (int)TileTypes.None, 0f);
                }
            }
        }

        public Tile this[int x, int y]
        {
            get 
            {
                int xindex = x;
                int yindex = y;

                if (x < 0) xindex = 0;
                if (y < 0) yindex = 0;

                if (x >= Tiles.Length) xindex = NumTilesX - 1;
                if (y >= Tiles[xindex].Length) yindex = NumTilesY - 1;

                return Tiles[xindex][yindex];
            }
        }

        public int NumTilesX
        {
            get { return Tiles.Length; }
        }

        public int NumTilesY
        {
            get { return Tiles[0].Length; }
        }

        public bool XExists(float x)
        {
            float truex = (x - TileOffSet.X) / (float)TileSize;

            return (truex >= 0 && (int)truex < NumTilesX);
        }

        public bool YExists(float y)
        {
            float truey = (y - TileOffSet.Y) / (float)TileSize;

            return (truey >= 0 && (int)truey < NumTilesY);
        }

        public bool XYExists(Vector2 loc)
        {
            return (XExists(loc.X) == true && YExists(loc.Y) == true);
        }

        //Gets the tile at the highest Z value out of all the tiles an object is touching - it makes sense because if you were standing over other tiles, the tile you're on would have to have a higher Z value
        public Tile CurTile(Rectangle FeetLoc)
        {
            if (TileXYExists(FeetLoc, Vector2.Zero) == false) return new Tile(Vector3.Zero, (int)TileTypes.None, 0f);

            Tile LocTile = Tiles[(FeetLoc.X - (int)TileOffSet.X) / TileSize][(FeetLoc.Y - (int)TileOffSet.Y) / TileSize];

            //Search through all the tiles that the object could be touching by going through the width and height of the feet rectangle and seeing if it intersects a tile
            for (int i = FeetLoc.X; ; i += (i + TileSize > FeetLoc.X + FeetLoc.Width) ? (FeetLoc.X + FeetLoc.Width) - i : TileSize)
            {
                int tilex = (i - (int)TileOffSet.X) / TileSize;

                for (int j = FeetLoc.Y; ; j += (j + TileSize > FeetLoc.Y + FeetLoc.Height) ? (FeetLoc.Y + FeetLoc.Height) - j : TileSize)
                {
                    int tiley = (j - (int)TileOffSet.Y) / TileSize;

                    //If your feet intersect a tile within the feet rectangle, compare the tile's Z value to your tile's Z value
                    if (FeetLoc.Intersects(Tiles[tilex][tiley].TileRec) == true)
                    {
                        //If the tile's Z value is greater than your tile's, set your tile to be that tile
                        if (Tiles[tilex][tiley].Z > LocTile.Z)  LocTile = Tiles[tilex][tiley];
                    }
                    if (j == FeetLoc.Y + FeetLoc.Height) break;
                }
                if (i == FeetLoc.X + FeetLoc.Width) break;
            }

            return LocTile;
        }

        //Checks if the level is underwater or not
        public bool IsUnderWater()
        {
            //If the height of the water in one tile is absurdly high, then it must be an underwater level since the heights of all the other tiles will be just as high
            return (Tiles[0][0].TypeHeight > 900f);
        }

        //Checks if there is a tile where the player is trying to move to from the X position
        public bool TileXExists(Rectangle FeetLoc, Vector2 Velocity)
        {
            int xtrue = (int)(FeetLoc.X + (Velocity.X < 0f ? 0 : FeetLoc.Width) + Velocity.X - TileOffSet.X);

            //Check if an object will be before the first tile (Integer division lets objects move there if the value is negative and less than the tilesize)
            if ((xtrue / (float)TileSize) < 0)
                return false;

            float XTrue = xtrue / TileSize;

            return (XTrue >= 0f && XTrue < Tiles.Length);
        }

        //Checks if there is a tile where the player is trying to move to from the Y position
        public bool TileYExists(Rectangle FeetLoc, Vector2 Velocity)
        {
            int ytrue = (int)(FeetLoc.Y + (Velocity.Y < 0f ? 0 : FeetLoc.Height) + Velocity.Y - TileOffSet.Y);

            //Check if an object will be before the first tile (Integer division lets objects move there if the value is negative and less than the tilesize)
            if ((ytrue / (float)TileSize) < 0)
                return false;

            float YTrue = ytrue / TileSize;
            int xloc = (int)(FeetLoc.X - TileOffSet.X);
            if (xloc < 0) xloc = 0;

            int xtile = xloc / TileSize;

            return (YTrue >= 0f && xtile < Tiles.Length && YTrue < Tiles[xtile].Length);
        }

        public bool TileXYExists(Rectangle FeetLoc, Vector2 Velocity)
        {
            return (TileXExists(FeetLoc, Velocity) == true && TileYExists(FeetLoc, Velocity) == true);
        }

        //Checks if something can move from one location to another, given it's higher than the tile it wants to go to
        public bool CanMoveLocation(Vector4 Location, Rectangle FeetLoc, Vector4 NewLocation)
        {
            //Check if the tile the object wants to go to exists, then check if the object is high enough to go to that tile
            return (TileXYExists(FeetLoc, Vector2.Zero) == true && Location.Z >= CurTile(FeetLoc).Z);
        }

        /*****Functions specifically for the level editor and debug uses****/
        //Gets the tile itself
        public Tile TileAtMouseLoc(Vector2 mouseloc)
        {
            return (Tiles[(int)(mouseloc.X - TileOffSet.X) / TileSize][(int)(mouseloc.Y - TileOffSet.Y) / TileSize]);
        }

        public int GetTileLength(bool X)
        {
            return (X == true) ? Tiles.Length : Tiles[0].Length;
        }

        public Vector2 GetTileOffSet()
        {
            return TileOffSet;
        }

        //Updates the tile offset
        public void UpdateTileOffSet(int offsetx, int offsety)
        {
            TileOffSet.X += offsetx;
            TileOffSet.Y += offsety;

            for (int i = 0; i < Tiles.Length; i++)
            {
                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    Tiles[i][j].Location.X += offsetx;
                    Tiles[i][j].Location.Y += offsety;//new Tile(new Vector3((i * TileSize) + TileOffSet.X, (j * TileSize) + TileOffSet.Y, Tiles[i][j].Location.Z), Tiles[i][j].TileType, Tiles[i][j].TypeHeight);
                }
            }
        }

        //Edits a tile's values, including tile height, type, and typeheight
        public void EditTileValues(Vector4 Location, float tileheight, float typeheight, int tiletype)
        {
            int x = (int)(Location.X - TileOffSet.X) / TileSize;
            int y = (int)(Location.Y - TileOffSet.Y) / TileSize;

            Tiles[x][y].Location.Z = tileheight;
            Tiles[x][y].TypeHeight = typeheight;
            Tiles[x][y].TileType = tiletype;
        }

        //Edits all tile's values in a particular row
        public void EditTileValuesRow(Vector4 Location, float tileheight, float typeheight, int tiletype)
        {
            int y = (int)(Location.Y - TileOffSet.Y) / TileSize;

            for (int i = 0; i < Tiles.Length; i++)
            {
                Tiles[i][y].Location.Z = tileheight;
                Tiles[i][y].TypeHeight = typeheight;
                Tiles[i][y].TileType = tiletype;
            }
        }

        //Edits all tile's values in a particular column
        public void EditTileValuesColumn(Vector4 Location, float tileheight, float typeheight, int tiletype)
        {
            int x = (int)(Location.X - TileOffSet.X) / TileSize;

            for (int i = 0; i < Tiles[x].Length; i++)
            {
                Tiles[x][i].Location.Z = tileheight;
                Tiles[x][i].TypeHeight = typeheight;
                Tiles[x][i].TileType = tiletype;
            }
        }

        //Edits all of the tile's values in the Tile Engine
        public void EditTileValuesAll(float typeheight, int tiletype)
        {
            for (int i = 0; i < Tiles.Length; i++)
            {
                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    //Tiles[i][j].Location.Z = tileheight;
                    Tiles[i][j].TypeHeight = typeheight;
                    Tiles[i][j].TileType = tiletype;
                }
            }
        }

        //Used when changing the Z value of the tile
        public void EditTileZ(Vector4 Location, int changeamount)
        {
            Tiles[(int)(Location.X - TileOffSet.X) / TileSize][(int)(Location.Y - TileOffSet.Y) / TileSize].Location.Z += changeamount;
        }

        public void EditTileZ(Vector4 Location, float newvalue)
        {
            Tiles[(int)(Location.X - TileOffSet.X) / TileSize][(int)(Location.Y - TileOffSet.Y) / TileSize].Location.Z = newvalue;
        }

        public void EditTileZAll(int changeamount)
        {
            for (int i = 0; i < Tiles.Length; i++)
            {
                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    Tiles[i][j].Location.Z += changeamount;
                }
            }
        }

        //Used when changing the typeheight of the tile
        public void EditTypeHeight(Vector4 Location, int changeamount)
        {
            Vector2 TileLoc = new Vector2(Location.X - TileOffSet.X, Location.Y - TileOffSet.Y);
            int tilex = (int)TileLoc.X / TileSize;
            int tiley = (int)TileLoc.Y / TileSize;

            if ((Tiles[tilex][tiley].TileType == (int)TileTypes.Water || Tiles[tilex][tiley].TileType == (int)TileTypes.Museum) && Tiles[tilex][tiley].TypeHeight + changeamount >= 0)
                Tiles[tilex][tiley].TypeHeight += changeamount;
        }

        public void EditTypeHeight(Vector4 Location, float newvalue)
        {
            Vector2 TileLoc = new Vector2(Location.X - TileOffSet.X, Location.Y - TileOffSet.Y);
            int tilex = (int)TileLoc.X / TileSize;
            int tiley = (int)TileLoc.Y / TileSize;

            if ((Tiles[tilex][tiley].TileType == (int)TileTypes.Water || Tiles[tilex][tiley].TileType == (int)TileTypes.Museum) && newvalue >= 0)
                Tiles[tilex][tiley].TypeHeight = newvalue;
        }

        //For creating underwater levels easily
        public void EditTypeHeightAll(int changeamount)
        {
            for (int i = 0; i < Tiles.Length; i++)
            {
                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    if ((Tiles[i][j].TileType == (int)TileTypes.Water || Tiles[i][j].TileType == (int)TileTypes.Museum) && Tiles[i][j].TypeHeight + changeamount >= 0)
                        Tiles[i][j].TypeHeight += changeamount;
                }
            }
        }

        //Used when changing the type of tile
        public void EditTileType(Vector4 Location, int changeamount)
        {
            Vector2 TileLoc = new Vector2(Location.X - TileOffSet.X, Location.Y - TileOffSet.Y);
            Tile tile = Tiles[(int)(TileLoc.X) / TileSize][(int)(TileLoc.Y) / TileSize];

            if (tile.TileType + changeamount >= (int)TileTypes.None && tile.TileType + changeamount < Enum.GetValues(typeof(TileTypes)).Length)
            {
                Tiles[(int)(TileLoc.X) / TileSize][(int)(TileLoc.Y) / TileSize].TileType += changeamount;
                if (Tiles[(int)(TileLoc.X) / TileSize][(int)(TileLoc.Y) / TileSize].TileType == (int)TileTypes.None && Tiles[(int)(TileLoc.X) / TileSize][(int)(TileLoc.Y) / TileSize].TypeHeight > 0f)
                    Tiles[(int)(TileLoc.X) / TileSize][(int)(TileLoc.Y) / TileSize].TypeHeight = 0f;
            }
        }

        //For creating underwater levels easily
        public void EditTileTypeAll(int changeamount)
        {
            for (int i = 0; i < Tiles.Length; i++)
            {
                for (int j = 0; j < Tiles[i].Length; j++)
                {
                    if (Tiles[i][j].TileType + changeamount >= (int)TileTypes.None && Tiles[i][j].TileType + changeamount < Enum.GetValues(typeof(TileTypes)).Length)
                    {
                        Tiles[i][j].TileType += changeamount;
                        if (Tiles[i][j].TileType == (int)TileTypes.None && Tiles[i][j].TypeHeight > 0f)
                            Tiles[i][j].TypeHeight = 0f;
                    }
                }
            }
        }

        //Adds or removes tiles to the tile engine by creating a new array and copying the old one into the new one
        /*Steps
         *-----
         *1. Allocate all the memory first
         *2. Copy the Y tiles (2nd bracket) from Tiles to newtile
         *3. Find any Y tiles that are null and allocate them
         *4. Find any X tiles that are null and allocate them*/
        public void AddRemoveTiles(int columns, int rows)
        {
            //Create all the tiles we need
            Tile[][] newTileEngine = new Tile[Tiles.Length + columns][];
            for (int i = 0; i < newTileEngine.Length; i++) newTileEngine[i] = new Tile[Tiles[0].Length + rows];

            //Copy all the elements from the original array into the new array
            for (int i = 0; i < Tiles.Length; i++)
            {
                //If there are fewer elements in the new array then the older one, stop copying
                if (i >= newTileEngine.Length) break;
                //Otherwise, simply copy the element in the old array into the new one
                else
                {
                    for (int j = 0; j < Tiles[i].Length; j++)
                    {
                        if (j >= newTileEngine[i].Length) break;
                        else newTileEngine[i][j] = Tiles[i][j];
                    }
                }
            }

            //If there are more elements from the X direction in the new array than the older one, create new tiles in those places with the correct locations
            for (int i = newTileEngine.Length - 1; i > (Tiles.Length - 1); i--)
            {
                newTileEngine[i] = new Tile[Tiles[0].Length + rows];
                for (int j = 0; j < newTileEngine[i].Length; j++)
                    newTileEngine[i][j] = new Tile(new Vector3((i * TileSize) + TileOffSet.X, (j * TileSize) + TileOffSet.Y, 0f));
            }

            for (int i = 0; i < newTileEngine.Length; i++)
            {
                if (i >= Tiles.Length) break;
                for (int j = newTileEngine[i].Length - 1; j > (Tiles[i].Length - 1); j--)
                    newTileEngine[i][j] = new Tile(new Vector3((i * TileSize) + TileOffSet.X, (j * TileSize) + TileOffSet.Y, 0f));
            }

            Tiles = newTileEngine;
        }

        //Draws the tiles (debug use only)
        public void Draw(SpriteBatch spriteBatch, Vector2 OffSet)
        {
            //Get the leftmost tile to draw
            int Left = (int)Math.Floor((-OffSet.X - TileOffSet.X) / TileSize);

            //Get the topmost tile to draw
            int Top = (int)Math.Floor((-OffSet.Y - TileOffSet.Y) / TileSize);

            //Get the rightmost tile to draw - the tile furthest to the right of the screen
            int Right = (int)Math.Ceiling((double)(Left + (((-OffSet.X - TileOffSet.X) / TileSize) - Left) + (Main.ScreenSize.X / TileSize)));

            //Get the bottommost tile to draw - the tile furthest to the bottom of the screen
            int Bottom = (int)Math.Ceiling((double)(Top + (((-OffSet.Y - TileOffSet.Y) / TileSize) - Top) + (Main.ScreenSize.Y / TileSize)));

            for (int i = Left; i < Right; i++)
            {
                if (i < 0) continue;
                if (i >= Tiles.Length) break;
                for (int j = Top; j < Bottom; j++)
                {
                    if (j < 0) continue;
                    if (j >= Tiles[i].Length) break;
                    Tiles[i][j].Draw(spriteBatch, OffSet);
                }
            }
        }

        public void DrawHeight(SpriteBatch spriteBatch, Vector2 Position, Vector2 OffSet, bool? zdraw = true)
        {
            //Get the leftmost tile to draw
            int Left = (int)Math.Floor((Position.X - TileOffSet.X) / TileSize);

            //Get the topmost tile to draw
            int Top = (int)Math.Floor((Position.Y - TileOffSet.Y) / TileSize);

            //Get the rightmost tile to draw - the tile furthest to the right of the screen
            int Right = (int)Math.Ceiling((double)(Left + (((Position.X - TileOffSet.X) / TileSize) - Left) + (Main.ScreenSize.X / TileSize)));

            //Get the bottommost tile to draw - the tile furthest to the bottom of the screen
            int Bottom = (int)Math.Ceiling((double)(Top + (((Position.Y - TileOffSet.Y) / TileSize) - Top) + (Main.ScreenSize.Y / TileSize)));

            //Draw all the tiles and their heights
            for (int i = Left; i < Right; i++)
            {
                if (i < 0) continue;
                if (i >= Tiles.Length) break;
                for (int j = Top; j < Bottom; j++)
                {
                    if (j < 0) continue;
                    if (j >= Tiles[i].Length) break;
                    Tiles[i][j].DrawHeight(spriteBatch, TileOffSet, OffSet, zdraw);
                }
            }
        }

        //A tile; it has a location, type, and typeheight
        [Serializable]
        public struct Tile
        {
            public Vector3 Location;

            //The type of tile (water, grass, etc.)
            public int TileType;

            //The height of the tile type (height of water, grass, etc.)
            public float TypeHeight;

            public Tile(Vector3 location)
            {
                Location = location;
                TileType = (int)TileTypes.None;
                TypeHeight = 0f;
            }

            public Tile(Vector3 location, int tiletype, float typeheight)
            {
                Location = location;
                TileType = tiletype;
                TypeHeight = typeheight;
            }

            public static bool operator ==(Tile tile, Tile newtile)
            {
                return (tile.Location.X == newtile.Location.X && tile.Location.Y == newtile.Location.Y);
            }

            public static bool operator !=(Tile tile, Tile newtile)
            {
                return !(tile.Location.X == newtile.Location.X && tile.Location.Y == newtile.Location.Y);
            }

            //Gets the depth an object is in a specified tile, including the typeheight; this is essentially looking for an object inbetween TypeHeightMax and the tile's height (Z position)
            public static float GetObjectDepthInTile(Tile tile, float objectheight)
            {
                return (tile.TypeHeightMax - objectheight);
            }

            //Gets the depth an object is in the tile, including the typeheight; this is essentially looking for an object inbetween TypeHeightMax and the tile's height (Z position)
            public float GetObjectDepthInTile(float objectheight)
            {
                return (TypeHeightMax - objectheight);
            }

            //Gets the height of the tile, including the typeheight (Ex. total water height)
            public float TypeHeightMax
            {
                get { return (Location.Z + TypeHeight); }
            }

            public float Z
            {
                get { return Location.Z; }
            }

            public float Left
            {
                get { return Location.X; }
            }

            public float Right
            {
                get { return Location.X + TileEngine.TileSize; }
            }

            public float Top
            {
                get { return Location.Y; }
            }

            public float Bottom
            {
                get { return Location.Y + TileEngine.TileSize; }
            }

            public Rectangle TileRec
            {
                get { return new Rectangle((int)Left, (int)Top, TileSize, TileSize); }
            }

            //Checks if an object touched a water tile
            public bool TouchedWater(float Z)
            {
                //Check all the tiles the object is touching and see if it's in the water range
                if (TileType == (int)TileTypes.Water)
                {
                    //Check if the object is in the water - there's a height difference where the water starts interfering with your movement
                    if ((Location.Z + TypeHeight) >= Z && TypeHeight > 15f)
                    {
                        return true;
                    }
                }

                return false;
            }

            public void Draw(SpriteBatch spriteBatch, Vector2 OffSet)
            {
                //Draw water tiles
                if (TileType == (int)TileTypes.Water && TypeHeight > 0f)
                {
                    //Be sure to draw the tiles at the right height; that is, over the background and foreground (and higher based on height) but under objects like players, enemies, items, etc.
                    spriteBatch.Draw(LoadGraphics.Water, new Rectangle((int)Left + (int)OffSet.X, (int)Top + (int)OffSet.Y, (int)TileSize, (int)TileSize), new Rectangle(1, 1, 0, 0), new Color(255, 255, 255, 145), 0f, Vector2.Zero, SpriteEffects.None, TypeHeight >= 900 ? .9971f : .001f);
                }
                //Draw death tiles; tiles with a height less than 0
                else if (Location.Z < 0)
                {
                    spriteBatch.Draw(LoadGraphics.Water, new Rectangle((int)Left + (int)OffSet.X, (int)Top + (int)OffSet.Y, (int)TileSize, (int)TileSize), new Rectangle(1, 1, 0, 0), Color.LightGreen, 0f, Vector2.Zero, SpriteEffects.None, TypeHeight >= 900 ? .997f : .001f);
                }
                //Draw grass tiles
                /*else if (TileType == (int)TileTypes.Grass)
                {
                    spriteBatch.Draw(LoadGraphics.Grass, new Vector2(Left, Top), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, Bottom / 1000f);
                }*/
            }

            /***For Level editor and debugging purposes***/
            public void DrawHeight(SpriteBatch spriteBatch, Vector2 TileOffSet, Vector2 OffSet, bool? zdraw)
            {
                string drawval = zdraw == true ? Convert.ToString((int)Z) : Convert.ToString((int)TypeHeight);
                Color drawcolor;

                if (zdraw == true)
                {
                    drawval = Convert.ToString((int)Z);
                    drawcolor = Color.Black;
                }
                else if (zdraw == false)
                {
                    drawval = Convert.ToString((int)TypeHeight);
                    drawcolor = Color.Blue;
                }
                else
                {
                    drawval = Convert.ToString(TileType);
                    drawcolor = Color.Red;
                }

                spriteBatch.DrawString(LoadGraphics.HUDFont, drawval, new Vector2(((Location.X + OffSet.X) + (TileSize/2)) - (LoadGraphics.HUDFont.MeasureString(drawval).X / 2f), (Location.Y + OffSet.Y)), drawcolor, 0f, Vector2.Zero, .8f, SpriteEffects.None, .996f);
                spriteBatch.Draw(LoadGraphics.Water, new Rectangle((int)(Location.X + OffSet.X), (int)(Location.Y + OffSet.Y), TileSize, 1), new Rectangle(0, 0, 1, 1), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .996f);
                spriteBatch.Draw(LoadGraphics.Water, new Rectangle((int)(Location.X + OffSet.X), (int)(Location.Y + OffSet.Y), 1, TileSize), new Rectangle(0, 0, 1, 1), Color.White, 0f, Vector2.Zero, SpriteEffects.None, .996f);
            }
        }
    }
}
