﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A Screen Manager that manages the UI screen stack
    public static class ScreenManager
    {
        //Our screen stack
        private static readonly Stack<Screen> ScreenStack;

        static ScreenManager()
        {
            ScreenStack = new Stack<Screen>();
        }

        public static Screen CurScreen
        {
            get
            {
                Screen screen = null;

                //Get the current screen on the top of the stack if it exists
                if (ScreenStack.Count > 0)
                    screen = ScreenStack.Peek();

                return screen;
            }
        }

        public static void PushScreen(Screen screen)
        {
            if (screen != null)
            {
                screen.ScreenPushed();
                ScreenStack.Push(screen);
            }
            else Debug.OutputValue("Trying to push null screen!");
        }

        public static Screen PopScreen()
        {
            Screen screen = null;

            //If we have a screen on the top of the stack, pop it and return it
            if (ScreenStack.Count > 0)
            {
                screen = ScreenStack.Pop();
                screen.ScreenPopped();
            }

            return screen;
        }
    }
}
