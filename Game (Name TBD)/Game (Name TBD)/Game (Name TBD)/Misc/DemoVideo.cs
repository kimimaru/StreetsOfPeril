﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Plays gameplay videos if the player waits too long on the "Press Start" screen
    //Record short clips (~20 seconds) of some levels and show yourself playing through them; have some with multiple players!
    //NOTE: The quality of the video you'll see in-game is only as good as the quality of the video itself!
    public class DemoVideo
    {
        //The video player
        private VideoPlayer VideoPlayer;

        //The list of videos to play and the current video that's playing
        private List<Video> Videos;
        private int CurVideo;

        //The texture for the current frame in the video
        private Texture2D FrameTexture;

        //Constructor
        public DemoVideo(params Video[] videos)
        {
            VideoPlayer = new VideoPlayer();

            Videos = new List<Video>(videos);
            CurVideo = 0;

            FrameTexture = null;
        }

        //If there's no texture to draw, the video is done playing
        private bool DonePlaying
        {
            get { return FrameTexture == null; }
        }

        //Adds a video
        private void AddVideo(Video video)
        {
            Videos.Add(video);
        }

        //Play the video, then move onto the next video
        public void PlayVideo()
        {
            if (CurVideo >= 0 && CurVideo < Videos.Count)
            {
                VideoPlayer.Play(Videos[CurVideo]);

                //Loop back to the first video if the rest have already played
                CurVideo = (CurVideo + 1) % Videos.Count;
            }
        }

        public void Update(float activeTime)
        {
            //Set the texture to null each Update so it doesn't stay drawing something at the end
            FrameTexture = null;

            //If the video isn't stopped, get the video's current frame that's being displayed
            if (VideoPlayer.State != MediaState.Stopped)
                FrameTexture = VideoPlayer.GetTexture();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //If the video is playing, draw its frame
            if (FrameTexture != null)
            {
                spriteBatch.Draw(FrameTexture, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

                //Can also draw blinking text saying "Press Start" here to show that it's a demo video and you're not actually playing
            }
        }
    }
}
