﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A basic floating platform - there can be a variation that doesn't let objects go through it from underneath
    public class Platform : Hazard
    {
        //Tells if the platform can be jumped through or not
        private bool JumpThrough;

        public Platform(Vector3 location, bool jumpthrough)
        {
            BlocksMove = true;

            Graphic = LoadGraphics.SpikeShooter;
            Location = location;
            Hurtbox = new Hurtbox(Graphic.Width, Graphic.Height);
            JumpThrough = jumpthrough;
        }

        //public override bool ShouldRemove(Vector2 OffSet, TileEngine TileEngine)
        //{
        //    return (Location.X + OffSet.X <= -100);
        //}

        public bool CanJumpThrough
        {
            get { return JumpThrough; }
        }

        //If the platform can be jumped through, it shouldn't block objects coming from the X or Y direction
        public override bool TouchedX(BeatEmUpObj otherobj, int objvelocityx)
        {
            if (JumpThrough == true) return false;
            else return base.TouchedX(otherobj, objvelocityx);
        }

        public override bool TouchedY(BeatEmUpObj otherobj, int objvelocityy)
        {
            if (JumpThrough == true) return false;
            else return base.TouchedY(otherobj, objvelocityy);
        }

        public override bool JumpedUnder(BeatEmUpObj otherobj)
        {
            if (JumpThrough == true) return false;
            else return base.JumpedUnder(otherobj);
        }

        //Check if something jumped over and landed on the top of the platform
        //public bool JumpedOnTop(Vector4 playerloc, float playervelocityZ, Rectangle playerrec)
        //{
        //    //if (JumpThrough == true)
        //    return (playerrec.Intersects(CollideBox) && ((playerloc.Z + playervelocityZ) <= MaxHeight() && playerloc.Z > (JumpThrough == true ? MaxHeight() : Location.Z)));
        //}

        //Check if something is currently on top of the platform
        public override bool IsOn(BeatEmUpObj otherobj)
        {
            if (JumpThrough == false) return base.IsOn(otherobj);
            else return (BlocksMove == true && IsDead == false && otherobj.FeetLoc.Intersects(CollisionBox) && (otherobj.CurHeight == MaxHeight || (otherobj.CurHeight > MaxHeight && (otherobj.CurHeight + otherobj.GetAirVelocity.Z) <= MaxHeight)));
        }

        //public override void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        //{
        //    //Draw a shadow under the platform if the platform is in the air - the shadow is the shape of the platform (all platforms should be squares)
        //    if (Location.Z > ObjectTile.Z)
        //    {
        //        //Draw a shadow where the platform's shadow is
        //        spriteBatch.Draw(Graphic, new Vector2(Location.X + OffSet.X, Location.Y + OffSet.Y - ObjectTile.Z), null, Color.Black, 0f, Animation.DefaultOrigin(Graphic), 1f, SpriteEffects.FlipHorizontally, ObjectTile.TypeHeight <= 0f ? 0.0019f : 0.0001f);
        //    }
        //
        //    float waterheight = ObjectTile.TypeHeight <= 0f ? 0f : ((ObjectTile.TypeHeight + ObjectTile.Z) - Location.Z);
        //    if (ObjectTile.TypeHeight <= 0f || waterheight < MaxHeight()) waterheight = 0f;
        //
        //    Animation.DrawSprite(spriteBatch, Graphic, new Vector2(Location.X + OffSet.X, Location.Y + OffSet.Y - Location.Z), true, Color.White, 0f, GetDrawDepth(OffSet));
        //
        //    DebugDraw(spriteBatch, OffSet);
        //}
    }
}
