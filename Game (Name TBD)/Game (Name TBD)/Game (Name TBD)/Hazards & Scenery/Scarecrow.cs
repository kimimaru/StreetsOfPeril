﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Scarecrow (soon-to-be Purple Bush) in Level 4 that functions as scenery
    //It can be hit, but nothing seems to happen...UNLESS it gets hit 25 times, in which it'll drop a Bonus Token 
    //If it is hit 50 times, the Super Sword, the most powerful weapon in the game, will fly to the right of the bush and land near the player!
    //After the weapon drops, it'll become usable!
    public class Scarecrow : Hazard
    {
        //Checks if the Super Sword was created after getting hit 50 times
        private bool Spawned;

        //Makes the scarecrow shake a little bit when hit
        private int[] Shakes;
        private int CurShake;
        private int NumShakes;

        //How many times the Scarecrow has been hit
        private int TimesHit;

        //Used if the Purple Bush goes offscreen
        private bool Dead;

        public Scarecrow(Vector3 location)
        {
            Graphic = LoadGraphics.SpikeShooter;
            SetLocation(location);

            ObjectLength = Graphic.Width;
            ObjectHeight = Graphic.Height;

            SetUpHurtbox();
            TimesHit = 0;

            Shakes = new int[] { -1, 0, 1 };
            CurShake = 1;
            NumShakes = 5;

            Spawned = false;

            Dead = false;
        }

        public bool GSSpawned
        {
            get { return Spawned; }
            set { Spawned = value; }
        }

        public bool HitLimit()
        {
            return (TimesHit >= 100);
        }

        //Says where to spawn the new weapon
        /*public Vector2 WeaponSpawnLoc()
        {
            return new Vector2(GCollision.X + Graphic.Width + 20, GCollision.Y - Graphic.Width);
        */

        public override bool IsDead
        {
            get { return Dead; }
        }

        //Objects cannot jump over or on the Purple Bush
        public override bool TouchedX(BeatEmUpObj otherobj, int objvelocityx)
        {
            Rectangle objfeetloc = otherobj.FeetLoc;
            objfeetloc.X += objvelocityx;

            return (BlocksMove == true && IsDead == false && objfeetloc.Intersects(CollisionBox));
        }

        public override bool TouchedY(BeatEmUpObj otherobj, int objvelocityy)
        {
            Rectangle objfeetloc = otherobj.FeetLoc;
            objfeetloc.Y += objvelocityy;

            return (BlocksMove == true && IsDead == false && objfeetloc.Intersects(CollisionBox));
        }

        //The scarecrow won't ever be in the air, so don't bother checking if something hit it from under
        public override bool JumpedUnder(BeatEmUpObj otherobj)
        {
            return false;
        }

        //The scarecrow can't be jumped on, so don't bother checking if something landed on it
        //public bool JumpedOnTop(Vector4 playerloc, float playervelocityZ, Rectangle playerrec)
        //{
        //    return false;
        //}

        public override bool IsOn(BeatEmUpObj otherobj)
        {
            return false;
        }

        //public override void InteractPlayer(float activeTime, Player player, TileEngine TileEngine, List<Collideable> Solids, List<Hazard> HazardList)
        //{
        //    //Make the players unable to hit the scarecrow too quickly
        //    if ((activeTime - Hurtbox.PrevHit) >= 500f)
        //    {
        //        for (int h = 0; h < player.GHitbox.Count; h++)
        //        {
        //            if (player.GHitbox[h].Throw == false && player.GHitbox[h].Intersect(CollisionBox, Location.Z, Location.Z + ObjectHeight) && Hurtbox.CanGetHit(player.GHitbox[h]) == true)
        //            {
        //                player.SetHitLag(activeTime);
        //                GetHit(activeTime, player.GHitbox[h], true);
        //            }
        //        }
        //
        //        //Check if the Wil's bullets hit the scarecrow
        //        if (player.GBullets != null)
        //        {
        //            for (int j = 0; j < player.GBullets.Count; j++)
        //            {
        //                if (player.GBullets[j].GHitbox.Intersect(CollisionBox, Location.Z, Location.Z + ObjectHeight) && Hurtbox.CanGetHit(player.GBullets[j].GHitbox) == true)
        //                {
        //                    GetHit(activeTime, player.GBullets[j].GHitbox, false);
        //
        //                    //Make Wil lose Health or a free bullet if the bullet hit the Scarecrow
        //                    player.SpecialAttackHit(activeTime, j);
        //
        //                    //Make the projectile do something when it hits (Ex. bullet bouncing off or disappearing)
        //                    player.GBullets[j].Intersect();
        //                }
        //            }
        //        }
        //    }
        //}
        //
        //public override void InteractEnemy(float activeTime, Enemy enemy, TileEngine TileEngine)
        //{
        //    //Make the thrown enemy unable to hit the scarecrow too quickly
        //    if ((activeTime - Hurtbox.PrevHit) >= 500f)
        //    {
        //        if (enemy.gIsKnockedDown == true && enemy.gHitbox.Intersect(CollisionBox, Location.Z, Location.Z + ObjectHeight) && Hurtbox.CanGetHit(enemy.gHitbox) == true)
        //        {
        //            enemy.SetHitLag(activeTime);
        //            GetHit(activeTime, enemy.gHitbox, false);
        //        }
        //    }
        //}
        //
        //public override void InteractWeapon(float activeTime, Weapon weapon, TileEngine TileEngine)
        //{
        //    //Make the weapon unable to hit the scarecrow too quickly
        //    if ((activeTime - Hurtbox.PrevHit) >= 500f)
        //    {
        //        if (weapon.GHitbox.Intersect(CollisionBox, Location.Z, Location.Z + ObjectHeight) && Hurtbox.CanGetHit(weapon.GHitbox) == true)
        //        {
        //            if (weapon.IsPickedUp == true)
        //            {
        //                if (weapon.GetPlayer() != null) weapon.GetPlayer().SetHitLag(activeTime);
        //                else if (weapon.GetEnemy() != null) weapon.GetEnemy().SetHitLag(activeTime);
        //            }
        //
        //            GetHit(activeTime, weapon.GHitbox, false);
        //        }
        //    }
        //}

        //The Purple Bush can be hit by anything, but only certain attacks from certain objects will 
        protected override void TakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            //Increment the number of times the Purple Bush got hit if it didn't spawn the Super Sword and got hit by a Player's non-throw move
            if (Spawned == false && objecthitby.ObjType == ObjectType.Player && hitbox.HasThrowDelegate == false)
            {
                //Increment the number of times the Purple Bush got hit
                TimesHit++;
            }

            NumShakes = 0;

            LoadSounds.Play(LoadSounds.Punch1);
        }

        public override void Die()
        {
            base.Die();

            Dead = true;
            LoseSolidity();
        }

        protected override void ObjectUpdate()
        {
            //Make the scarecrow shake a bit when getting hit
            if (NumShakes < 5)
            {
                CurShake = (CurShake + 1) % Shakes.Length;
                if (CurShake == 1) NumShakes++;
            }

            //Clear the hurtbox if the scarecrow didn't get hit for 5 seconds
            //if ((Main.GetActiveTime - Hurtbox.PrevHit) >= 5000f)
            //    Hurtbox.Clear();

            //If the Purple Bush is offscreen, kill it and stop it from drawing
            if (IsOffScreenRange(SubLvl.GCameraOffSet, 100, 100) == true)
            {
                Die();
                DrawEnabled = false;
            }
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            spriteBatch.Draw(Graphic, GetDrawLoc(cameraloc) + new Vector2(Shakes[CurShake], 0), null, Color.White, 0f, NewAnimation.DefaultOrigin(new Vector2(Graphic.Width, Graphic.Height), FacingRight), 1f, SpriteEffects.None, GetDrawDepth(cameraloc));
        }
    }
}
