﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //Spikes used in Level 8-3; they pop up after a certain amount of time for a certain amount of time, then retract
    public sealed class Spikes : BeatEmUpObj//Hazard
    {
        //The strength of the Spikes is constant for now
        private const int SpikeStrength = 5;

        //The amount of time the Spikes are out
        private float TimeOut;
        private float PrevTimeOut;

        //The amount of time the Spikes are retracted
        private float TimeRetracted;
        private float PrevTimeRectrated;

        //Tells if the Spikes are retracted or not
        private bool Retracted;

        public Spikes(Vector3 location, float timeout, float timeretracted, bool startretracted, Status status)
        {
            SolidBlocked = false;

            Location = location;

            TimeOut = timeout;
            TimeRetracted = timeretracted;

            PrevTimeOut = 0f;
            PrevTimeRectrated = 0f;

            //Set if the Spikes start out retracted or not
            SetRetracted(startretracted);

            //Spikes do not have health
            SetUpHealth(0);

            //Set the Spike's Status
            InflictStatus(status);
        }

        private void SetRetracted(bool retracted)
        {
            Retracted = retracted;

            if (Retracted == true)
            {
                PrevTimeRectrated = Main.GetActiveTime + TimeRetracted;
                ClearHitboxes();
            }
            else
            {
                PrevTimeOut = Main.GetActiveTime + TimeOut;
                CreateHitboxSurrounding(SpikeStrength, 40, 0f, TimeOut, Hitbox.HitboxTypes.KnockDown, null, null, true);
            }
        }

        protected override void ObjectUpdate()
        {
            //If the Spikes are out, check to see if they should retract and vice versa
            if (Retracted == false)
            {
                if (Main.GetActiveTime >= PrevTimeOut)
                    SetRetracted(true);
            }
            else
            {
                if (Main.GetActiveTime >= PrevTimeRectrated)
                    SetRetracted(false);
            }
        }

        protected override void ObjectDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            base.ObjectDraw(spriteBatch, cameraloc, TileEngine);
        }
    }
}
