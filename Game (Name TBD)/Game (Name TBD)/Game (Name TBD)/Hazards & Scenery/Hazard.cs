﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Game__Name_TBD_
{
    //Level hazards, such as water, fire, conveyer belts, falling rocks, and more
    public abstract class Hazard : BeatEmUpObj
    {
        //The numerical value of each type of hazard
        public enum Hazards
        {
            FallingRock, BSShooter, PoisonGasShooter, Scarecrow, Platform
        };

        //The graphic of the hazard and the graphic/animation of whatever happens when you interact with the hazard (Ex. water splashing)
        protected Texture2D Graphic;

        protected Animation InteractionGraphic;

        public Hazard()
        {
            ObjType = ObjectType.Hazard;
        }
    }
}
