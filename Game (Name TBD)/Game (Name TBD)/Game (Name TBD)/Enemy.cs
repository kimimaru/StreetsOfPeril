﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    public abstract class Enemy : Fighter
    {
        //The numerical value of each enemy; any value after Diver is a boss
        public enum Enemies
        {
            Mark, Christopher, Paul, Tank, Greg, Grunt, Rogue, Changer, Imposter, WeaponWielder, Samson, Theodore, MinecartRider, Diver, Commander
        };

        //The value denoting no player index
        public const int NoPlayerIndex = -1;

        protected Grabbox Grabbox;

        //The number representing the enemy
        protected int EnemNum;
        
        //An animation that points to the most recent animation that is associated with hitboxes (Ex. AttackAnim); it's used to extend the duration of the animation when you hit something
        //protected Animation CurDamagingAnim;

        protected NewAnimation StandAnim;
        protected NewAnimation WalkingAnim;
        protected NewAnimation AttackingAnim;
        protected NewAnimation GrabbedAnim;
        protected NewAnimation GrabHitAnim;
        protected NewAnimation PickupAnim;

        //Manages the enemy's actions
        protected ActionManager ActionManage;

        //Controls how long the game is slowed down after a boss is defeated
        public static float PrevBossTimer;

        //Determines if an enemy has its AI enabled or not
        protected bool AIEnabled;

        protected bool IsWalking;
        protected bool IsAttacking;
        protected bool IsJumping;
        protected bool IsGrabbedHit;

        //Physics Info - Weight determines how much damage an enemy does when thrown and how far the enemy slides when knocked down
        protected bool InAir;
        protected int Weight;

        //Tells if the enemy is carried by Jeff
        protected bool Carried;

        //The current difficulty and level of the enemy; these are used for increased stats
        protected int DifficultyLevel;
        protected int LevelNumber;

        //The status the enemy spawned with and the status the enemy had right before it was killed
        protected int SpawnStatus;
        protected int KilledStatus;

        //The enemy's target and the last player that attacked the enemy
        protected Fighter Target;
        protected Player LastAttacked;

        //Constructor
        public Enemy()
        {
            MoveModifier = Vector2.Zero;
            OrigJumpVelocity = new Vector3(4f, 4f, 4f);
            AirVelocity = new Vector3(0f, 0f, 0f);
            GravityValue = TileEngine.Gravity;
            Weight = 140;

            ObjType = ObjectType.Enemy;

            //Default icon and spritesheets; default to Mark
            Icon = LoadGraphics.EnemyIcon[(int)Enemies.Mark];
            SpriteSheet = LoadGraphics.EnemySheets[(int)Enemies.Mark][0];

            ObjectLength = 39;
            ObjectHeight = 79;

            Grabbox = new Grabbox(Grabbox.GetLength(ObjectLength), 6, 2000, 1000);

            ActionManage = new ActionManager(this);

            AIEnabled = true;

            FacingRight = true;

            InAir = false;
            IsWalking = false;
            IsAttacking = false;
            IsJumping = false;
            IsGrabbedHit = false;

            GainKnockInv = false;

            Name = "Enemy";

            SpawnStatus = (int)Status.Statuses.None;
            KilledStatus = (int)Status.Statuses.None;

            Carried = false;

            Designated = true;

            Target = null;
            LastAttacked = null;

            hud = new FighterHUD(this);
        }

        static Enemy()
        {
            PrevBossTimer = 0f;
        }

        //Setups up an enemy's spritesheet and icon
        public void SetUpSprites()
        {
            SpriteSheet = LoadGraphics.EnemySheets[EnemNum][Alternate];
            Icon = LoadGraphics.EnemyIcon[EnemNum];
        }

        //Enemy-specific property for checking if a move grants the enemy invincibility
        //NOTE: THIS WILL LIKELY BE REMOVED LATER AND WILL HAVE MOVES DEFINE WHETHER THEY HAVE INVINCIBILITY OR NOT
        protected virtual bool EnemMoveInvincibility
        {
            get { return false; }
        }

        protected override bool HasMoveInvincibility
        {
            get { return (base.HasMoveInvincibility == true || EnemMoveInvincibility == true); }
        }

        public override Rectangle GrabBox
        {
            get 
            {
                if (Grabbox != null)
                {
                    return new Rectangle(CollisionBox.X + ((Hurtbox.Width / 2) - (Grabbox.Width / 2)), CollisionBox.Y + (Hurtbox.Height / 2) - (Grabbox.Height / 2), Grabbox.Width, Grabbox.Height);
                }

                return base.GrabBox;
            }
        }

        public override bool ShouldRemove
        {
            get { return (base.ShouldRemove == true && IsInAir == false); }
        }

        public float GetGravityValue
        {
            get { return GravityValue; }
        }

        public float GetSlideSpeed
        {
            get { return SlideSpeed; }
        }

        public Fighter gTarget
        {
            get { return Target; }
        }

        public Player gLastAttacked
        {
            get { return LastAttacked; }
        }

        public int gWeight
        {
            get { return Weight; }
        }

        public int gScore
        {
            get { return Score; }
        }

        public override bool KnockedDown
        {
            get { return (ActionManage.CurrentAction.GetActionType == Action.ActionType.KnockedDown); }
        }

        public bool gsIsWalking
        {
            get { return IsWalking; }
            set { IsWalking = value; }
        }

        public virtual bool gsIsAttacking
        {
            get { return IsAttacking; }
            set { IsAttacking = value; }
        }

        public bool gsIsJumping
        {
            get { return IsJumping; }
            set { IsJumping = value; }
        }

        public bool gsCarried
        {
            get { return Carried; }
            set { Carried = value; }
        }

        public virtual Grabbox gGrabbox
        {
            get { return Grabbox; }
        }

        public ActionManager gsAction
        {
            get { return ActionManage; }
            set { ActionManage = value; }
        }

        //Check if an enemy has statuses it can spawn with
        public bool HasSpawnableStatuses
        {
            get 
            {
                Status[] statuses = PossibleStatuses;
                return (statuses != null && statuses.Length > 0); 
            }
        }

        //Gets the enemy's possible statuses; each enemy has a static property containing them, and this simply gets them from each static property
        //This essentially acts as static inheritance, allowing us to obtain the proper statuses at runtime and easily obtaining each enemy's statuses at compile time
        public virtual Status[] PossibleStatuses
        {
            get { return null; }
        }

        public int StartingStatus
        {
            get { return SpawnStatus; }
        }

        public int DeathStatus
        {
            get { return KilledStatus; }
        }

        //Gets the enemy's standing or idle animation; used when the enemy is idle
        public NewAnimation GetIdleAnim
        {
            get { return StandAnim; }
        }

        //Gets the enemy's walk animation; used in most movement actions
        public NewAnimation GetWalkAnim
        {
            get { return WalkingAnim; }
        }

        //Gets the enemy's current knock down animation
        public NewAnimation GetCurKnockDownAnim
        {
            get { return CurKnockDownAnim; }
        }

        //Tells if the enemy is a boss or not
        public virtual bool IsBoss()
        {
            return false;
        }

        //Checks if the enemy is offscreen in the X direction
        public bool IsOffScreenX(Vector2 cameraloc)
        {
            Rectangle feetloc = FeetLocCamera(cameraloc);

            //Check the left and right of the screen
            return (feetloc.X >= Main.ScreenSize.X || feetloc.Right <= 0);
        }

        //Checks if the enemy is offscreen in the Y direction
        public bool IsOffScreenY(Vector2 cameraloc)
        {
            Rectangle feetloc = FeetLocCamera(cameraloc);

            //Check the top and bottom of the screen
            return (feetloc.Y >= Main.ScreenSize.Y || feetloc.Bottom <= 0);
        }

        //NOTE: Change these methods to use the centers of the enemy and target so it's even regardless of where the enemy is in relation to the target (Ex. Enemy left of the target should return the same value as when the enemy is to the right of the target)
        //Gets the position of the target in relation to the enemy
        public float TargetProximity()
        {
            return (Vector2.Distance(GetLocationVec2, Target.GetLocationVec2));
        }

        //Gets the X position of the target in relation to the enemy
        public float TargetProximityX()
        {
            return (Math.Abs(GetLocationVec2.X - Target.GetLocationVec2.X));
        }

        //Gets the Y position of the target in relation to the enemy
        public float TargetProximityY()
        {
            return (Math.Abs(GetLocationVec2.Y - Target.GetLocationVec2.Y));
        }

        //Checks if the target is in front of the enemy; this can be dependent on the direction the enemy is facing or absolute (target has a higher X position)
        public bool TargetInFront(bool absolute = true)
        {
            //If absolute is false, then check if the enemy is facing right; if so, the target must have a higher X value than the enemy to be considered in front
            if (absolute == false)
            {
                if (FacingRight == false) return (Target.GetLocationVec2.X < GetLocationVec2.X);
            }
            
            return (Target.GetLocationVec2.X > GetLocationVec2.X);
        }

        //Checks if the target is behind the enemy; this can be dependent on the direction the enemy is facing or absolute
        public bool TargetBehind(bool absolute = true)
        {
            return (TargetInFront(absolute) == false);
        }

        //Checks if the target is above the enemy in the Y position
        public bool TargetAbove()
        {
            return (Target.GetLocationVec2.Y < GetLocationVec2.Y);
        }

        //Checks if the target is below the enemy in the Y position (needed so an enemy doesn't keep walking up and down if it is matching the Y position of the player when its Y is equal to the player's)
        public bool TargetBelow()
        {
            return (Target.GetLocationVec2.Y > GetLocationVec2.Y);
        }

        //Checks if the target is higher than the enemy in height
        public bool TargetHigher()
        {
            return (Target.CurHeight > CurHeight);
        }

        //Set the Enemy's AI state
        public void SetAIEnabled(bool AIenabled)
        {
            AIEnabled = AIenabled;
        }

        //Makes the enemy jump; if a value is null, then don't modify it - this can be used to make an enemy change direction in the air without modifying its vertical jump velocity
        public void Jump(float? jumpvelocityx, float? jumpvelocityv, float? jumpvelocityz, bool actualjump = true)
        {
            if (jumpvelocityx != null) AirVelocity.X = (float)jumpvelocityx;
            if (jumpvelocityv != null) AirVelocity.Y = (float)jumpvelocityv;
            if (jumpvelocityz != null) AirVelocity.Z = (float)jumpvelocityz;

            //Don't make the enemy jump if you're setting the jump velocity for another reason (Ex. Diver so he can ignore gravity)
            if (actualjump == true) IsJumping = true;
        }

        protected override void KnockDownActions()
        {
            base.KnockDownActions();

            //Go to the KnockedDown state
            ActionManage.CurrentAction = new KnockedDown(this, CurKnockDownAnim, ActionManage.CurrentAction.FinishBehavior, ActionManage.CurrentAction.InterruptBehavior, ActionManage.CurrentAction.UnfinishBehavior);
            ActionManage.ChoseAction(Main.GetActiveTime);
        }

        //Kill the enemy
        public override void Die()
        {
            KilledStatus = status.GCond;
            base.Die();

            //If the enemy is a boss and died, slow down the game for a few seconds
            if (IsBoss() == true)
            {
                PrevBossTimer = Main.GetActiveTime;
                Main.ChangeGameSpeed(30f);
            }

            //Give the player that killed the enemy points
            GivePoints(Score);

            StatForever = false;
        }

        //Changes the enemy's alternate outfit
        public void ChangeCostume(int costume)
        {
            if (LoadGraphics.EnemySheets[EnemNum] != null && costume >= 0 && costume < LoadGraphics.EnemySheets[EnemNum].Length && LoadGraphics.EnemySheets[EnemNum][costume] != null)
            {
                Alternate = costume;
                SpriteSheet = LoadGraphics.EnemySheets[EnemNum][Alternate];
            }
        }

        //Creates an oxygen tank if the enemy is in an underwater level
        public void CreateOxygenTank()
        {
            OxygenTank = new OxygenTank(LoadGraphics.FallingRock, LoadGraphics.OxygenTankFillBlue, this);
        }

        protected override void HitLagActions(float diff)
        {
            base.HitLagActions(diff);

            if (ActionManage.CurrentAction.Animation != null) ActionManage.CurrentAction.Animation.DelayAnim(diff);
        }

        //Makes the enemy recover from a grab; usually called when something grabbing the enemy is damaged, a player releases the enemy, or when the enemy breaks free from whatever is grabbing it
        public override void GrabRecover()
        {
            //NOTE: If the Enemy BREAKS out of the grab, make sure to recover the Fighter grabbing the enemy too
            //This would not be called right here, as this is simply a recover method


            base.GrabRecover();

            IsGrabbedHit = false;
            Grabbox.Recover(Main.GetActiveTime);
        }

        protected override void TakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.TakeDamageActions(objecthitby, damage, hitbox);

            //Switch the enemy's behavior upon getting hurt
            //if (ShouldSwitchBehavior() == true) ActionManage.SwitchBehavior(ActionManage.CurrentAction.InterruptBehavior);
        }

        protected override void AltTakeDamageActions(BeatEmUpObj objecthitby, int damage, Hitbox hitbox)
        {
            base.AltTakeDamageActions(objecthitby, damage, hitbox);

            //Set the last player that attacked the Enemy
            if (LastAttacked != objecthitby)
                LastAttacked = objecthitby as Player;
        }

        //Checks if the enemy should give the player that last attacked it points
        public void GivePoints(int points)
        {
            if (LastAttacked != null) LastAttacked.AddScore(points);
        }

        //Clears the Fighter that last attacked the enemy
        public void ClearLastAttacked()
        {
            LastAttacked = null;
        }

        //For resetting the knock down animation 
        //This is used in Sublevel 6-3, the Minecart rush level, after touching the minecart tracks because changing the jump velocity doesn't work if the enemy is already knocked down after dying since it doesn't reset the animation in that case
        public void ResetKnockDownAnim()
        {
            CurKnockDownAnim.Reset();
        }

        //Sets the enemy's starting status in the event that the correct one cannot be passed through the constructor (Ex. Commander not knowing the type of minion when spawning it)
        public void SetSpawnStatus(Status newstatus)
        {
            //Make sure the status is valid
            if (newstatus != null)
            {
                status = newstatus;
                SpawnStatus = status.GCond;
                StatForever = true;
            }
        }

        //Reset the enemy's actions upon getting hurt or falling
        protected override void ResetActions()
        {
            base.ResetActions();

            IsWalking = false;
            IsAttacking = false;
            IsJumping = false;
            //IsHit = false;
            IsGrabbedHit = false;
            //Grabbox.GSIsGrabbed = false;
            Carried = false;
            InAir = false;
            if (AirVelocity.Z < 0 && AirVelocity.Z >= -OrigJumpVelocity.Z)
                AirVelocity.Z = -AirVelocity.Z;
        }

        protected override void EnterHitstun()
        {
            base.EnterHitstun();

            if (ActionManage.CurrentAction.GetActionType != Action.ActionType.Hurt)
            {
                ActionManage.CurrentAction = new Hurt(this, HurtAnim, ActionManage.CurrentAction.FinishBehavior, ActionManage.CurrentAction.InterruptBehavior, ActionManage.CurrentAction.UnfinishBehavior);
                ActionManage.ChoseAction(Main.GetActiveTime);
            }
        }

        public override void GetGrabbed(BeatEmUpObj opponent)
        {
            base.GetGrabbed(opponent);

            //Target = opponent;
            //if (ShouldSwitchBehavior() == true) ActionManage.SwitchBehavior(ActionManage.CurrentAction.InterruptBehavior);
            ActionManage.CurrentAction = new Grabbed(this, GrabbedAnim, ActionManage.CurrentAction.InterruptBehavior, ActionManage.CurrentAction.InterruptBehavior, ActionManage.CurrentAction.InterruptBehavior);
            ActionManage.ChoseAction(Main.GetActiveTime);

            Grabbox.GetGrabbed(Main.GetActiveTime);
        }

        //Boosts an Enemy's stats based on its Difficulty and LevelNumber
        //NOTE: There should just be modifiers that are hardcoded for each Enemy and factored into the stat increases
        //This way, we don't need to override this method for each enemy
        protected virtual void BoostStats()
        {

        }

        //Make the enemy adjust its speed, after calculating its true velocity, depending on its behavior (Ex. moving faster when target is further away)
        protected override Vector2 ObjVelocityFactor()
        {
            Vector2 increase = MoveModifier;

            //Make the enemy move faster based on difficulty
            if (Main.CheckState(Main.GameState.InGame) == true || Main.CheckState(Main.GameState.InChallenge) == true) increase += new Vector2((Main.Difficulty / 2), (Main.Difficulty / 2));

            return increase;
        }

        //Make the enemy choose the appropriate action
        protected virtual void EnemyAction(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Player> Players, List<Enemy> EnemList)
        {
            
        }

        //Overrides whatever action an enemy is performing and resets it to Idle
        //This is ONLY used in RPS to stop Tank from infinitely parrying; DO NOT use it in any other circumstance
        public virtual void OverrideActions()
        {
            
        }

        //Determines if an enemy is able to perform an action or not
        protected virtual bool CanDoAction()
        {
            return (AIEnabled == true && Carried == false);
        }

        //Determines if an enemy is able to actually carry out the action or not (different from CanDoAction(), which determines if an enemy can even choose one to begin with)
        //This is needed because enemies wouldn't go to their interrupt actions when hit if they had a timer BEFORE choosing the next action because ActionChose would be false
        protected virtual bool CanPerformAction()
        {
            return (AIEnabled == true && ActionManage.ActionDone == false && ActionManage.ActionNotDone == false);
        }

        //A method for determining what an enemy does before or after the action timer is over (Ex. Choosing an action, Tank sliding after blocking an attack, etc.)
        protected virtual void SetDoAction()
        {
            //Choose which action to do
            if (ActionManage.ActionChose == false)
            {
                EnemyAction(Main.GetActiveTime, SubLvl.GCameraOffSet, SubLvl.GetTileEngine, SubLvl.GetPlayers, SubLvl.GetEnemies);
                ActionManage.ChoseAction(Main.GetActiveTime);
            }
            
            //Do the action if another condition is met (timer, etc.)
            if (CanPerformAction() == true)
            {
                if (ActionManage.CurrentAction != null) ActionManage.CurrentAction.Update(Main.GetActiveTime, SubLvl);
            }

            ActionFinished(Main.GetActiveTime);
        }

        protected virtual void ActionFinished(float activeTime)
        {
            //If the enemy is done with an action, reset back to idle and commence the wait time
            if (ActionManage.IsActionDone() == true)
            {
                if (ActionManage.ActionDone == true) ActionManage.SwitchBehavior(ActionManage.CurrentAction.FinishBehavior);
                else if (ActionManage.ActionNotDone == true) ActionManage.SwitchBehavior(ActionManage.CurrentAction.UnfinishBehavior);
            }
        }

        //Check if the enemy is facing the target
        public bool IsFacingTarget()
        {
            return (FacingRight == Target.FacingRight);
        }

        //Make the enemy face the target; if the target has a greater X value, face to the right, otherwise face to the left
        public void FaceTarget()
        {
            //If there is no target to face, don't try
            if (Target != null)
            {
                FacingRight = TargetInFront();
            }
        }

        //Make the enemy face away from the target
        public void FaceAwayTarget()
        {
            //If there's no target to look away from, don't try
            if (Target != null)
            {
                FaceTarget();
                FacingRight = !FacingRight;
            }
        }

        //What an enemy does after losing a weapon; right now this is here because the WeaponWielders need to change their behavior state after losing their weapons
        protected virtual void LoseWeapon()
        {

        }

        //The center of the weapon's drawing location (standing position) - the weapon moves and rotates around this point depending on the animation
        public Vector3 WeaponLoc(bool FacingRight, int spritewidth)
        {
            return Vector3.Zero;//StandingAnim.CurWeaponLoc(FacingRight, spritewidth);
        }

        protected override void BeginFall()
        {
            base.BeginFall();

            //Switch the enemy's behavior upon falling
            if (ShouldSwitchBehavior() == true) ActionManage.SwitchBehavior(ActionManage.CurrentAction.UnfinishBehavior);

            //TEMPORARY
            InAir = true;
        }

        protected override void LandKnockedDown()
        {
            ClearRotation();

            if (ObjectTile.Z >= 0f)
                LoadSounds.Play(LoadSounds.Land);
        }

        //Make the Diver fall back to its original height when knocked down like Jet in Streets of Rage 2
        //For use by the Diver when being knocked down; it checks if the Diver is at or below its constant height and stops it from being knocked down if so
        public virtual void DiverHeightCheck()
        {

        }

        protected override void HitWallX()
        {
            ActionManage.CurrentAction.HitWallSolid(true);
        }

        protected override void HitWallY()
        {
            ActionManage.CurrentAction.HitWallSolid(false);
        }

        protected override void HitSolidX()
        {
            ActionManage.CurrentAction.HitWallSolid(true);
        }

        protected override void HitSolidY()
        {
            ActionManage.CurrentAction.HitWallSolid(false);
        }

        //Allows enemies to move freely without worrying about collisions
        public void PureMove(float velocityX, float velocityY, bool changedirection = false)
        {
            if (changedirection == true)
            {
                if (velocityX > 0f) FacingRight = false;
                else if (velocityX < 0f) FacingRight = true;
            }
            Location.X += velocityX;
            Location.Y += velocityY;
        }

        //This is for sliding after getting knocked down or thrown
        public void EnemySlide(float slidedeceleration = .45f)
        {
            ObjectMove(new Vector2(ForwardVal(-SlideSpeed, SlideSpeed), 0));

            if (AirVelocity.Z == 0f)
            {
                SlideSpeed -= (float)(Math.Round(((Weight - OrigJumpVelocity.X) / 333f), 2));//slidedeceleration;
                if (SlideSpeed < 0f) SlideSpeed = 0f;
                Location.X = (int)Location.X;
            }
        }

        //Makes the Enemy switch to a new Action
        protected void SwitchAction(Action action)
        {
            ActionManage.CurrentAction = action;
        }

        //Makes the enemy choose a new player as a target
        //NOTE: Change this to allow some enemies to not set a target until a player comes near
        protected void NewTarget(List<Player> Players)
        {
            //Check if the enemy should switch targets
            if (Players.Count > 1 && ActionManage.ShouldSwitchTarget() == true)
                Target = ActionManage.SwitchTarget(Players);
            //Set a default target if the target was not set initially
            //Alternatively, the Target can stay null to have some enemies be in an "inactive" state when created where they do some special animation until the player gets close enough!
            else Target = Players[0];
        }

        protected virtual void UpdateHitAnim(float activeTime)
        {
            if (IsHit == true)
            {
                HurtAnim.Update(activeTime, GravityValue);
                if (IsInHitstun == false)
                {
                    IsHit = false;
                }
            }
        }

        protected void UpdateGrabbedHitAnim(float activeTime)
        {
            if (IsGrabbedHit == true)
            {
                GrabHitAnim.Update(activeTime, GravityValue);
                if (GrabHitAnim.IsAnimationEnd() == true)
                    IsGrabbedHit = false;
            }
        }

        protected void UpdateKnockDownAnim(float activeTime, float slidedeceleration = .45f)
        {
            //If the enemy is knocked down, update the animation
            if (IsKnockedDown == true)
            {
                //If the enemy is on the second frame of the knock down animation (thus on the ground), start making the enemy recover
                if (CurKnockDownAnim.CurrentFrame != 0)
                {
                    EnemySlide(slidedeceleration);

                    //If the enemy is on the ground, update the knock down animation; otherwise, if the enemy got into the air again, start the animation over
                    if (IsOnGround == true)
                    {
                        CurKnockDownAnim.Update(activeTime, status == (int)Status.Statuses.StunDown ? .4f : .2f);
                    }
                    else
                    {
                        CurKnockDownAnim.Reset();
                        AirVelocity.X = ForwardVal(-SlideSpeed);
                    }

                    //Check if the enemy is dead, on the floor, and not invincible yet
                    if (IsDead == true && status != (int)Status.Statuses.Invincible)
                    {
                        InflictStatus(new Status((int)Status.Statuses.Invincible, Status.InfiniteStatus, 0));
                        LoadSounds.Play(LoadSounds.EDeath);
                    }

                    //If the enemy's jump velocity is 0, the enemy is on the floor; once that's true, check if the animation is done or if the enemy is dead, which ends the animation earlier
                    if (AirVelocity.Z == 0f && (CurKnockDownAnim.IsAnimationEnd() == true || IsDead == true && CurKnockDownAnim.CurrentFrame == 2))
                    {
                        //Recover from a knock down
                        KnockDownRecover();
                    }
                }
                else
                {
                    //if (Rotation != 0) Movement.RotateClockWise(!FacingLeft, ref Rotation, 25); //Rotation = FacingLeft == true ? Rotation + (float)(Math.PI / 25) : Rotation - (float)(Math.PI / 25);
                    ObjectMove(new Vector2(AirVelocity.X, 0));

                    //Check for the Diver falling back to its original height
                    DiverHeightCheck();
                }
            }
        }

        //Tells if the Enemy should switch behaviors or not
        public bool ShouldSwitchBehavior()
        {
            return (ActionManage.CurrentAction != null && ActionManage.CurrentAction.GetActionType != Action.ActionType.KnockedDown);
        }

        public override bool CanPickUp()
        {
            return (base.CanPickUp() == true && ActionManage.CurrentAction.GetActionType != Action.ActionType.Utility);
        }

        //Checks if the enemy can pick up an item or weapon
        public virtual bool CheckCanPickUp()
        {
            return (status != (int)Status.Statuses.NoGrab && InAir == false && IsDead == false && KnockedDown == false && IsInHitstun == false && Grabbox.GSIsGrabbed == false && Carried == false && IsJumping == false && !(ActionManage.CurrentAction is Pickup));
        }

        //Checks if the enemy wants to pick up the item or weapon depending on its preferences or current state (health remaining, etc.)
        public virtual bool CheckShouldPickUp(Item item, Weapon Weapon)
        {
            return false;
        }

        //Checks if the enemy is in a bad state (hit, knocked down, etc.)
        public bool IsWell()
        {
            return (IsDead == false && IsHit == false && IsKnockedDown == false && Grabbox.GSIsGrabbed == false && Carried == false);
        }

        //Makes the enemy pick up the item or weapon
        public override void Pickup(PickupObj pickupobj)
        {
            base.Pickup(pickupobj);

            ActionManage.CurrentAction = new Pickup(this, PickupAnim);
            ActionManage.ChoseAction(Main.GetActiveTime);
        }

        public override bool CanGetGrabbed()
        {
            return (base.CanGetGrabbed() == true && Grabbox.CanGetGrabbed(Main.GetActiveTime) == true && IsOnGround == true);
        }

        //NOTE: One idea is to update the current action's animation here, but then there's less flexibility when dealing with actions that are animation-dependent, such as the Weapon Wielders' Jumpslash move
        protected virtual void UpdateAnimations(float activeTime, Vector2 OffSet, TileEngine TileEngine, List<Collideable> Solids)
        {
            //UpdateStandingAnim(activeTime);
            //UpdateWalkAnim(activeTime);
            //UpdateAttackAnim(activeTime);
        }

        protected virtual void DrawAnimations(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine, float waterheight, Color drawcolor, float depth)
        {
            Vector2 drawloc = new Vector2(Location.X + OffSet.X, Location.Y - (int)Location.Z + OffSet.Y);

            //if (IsKnockedDown == true)
            //    CurKnockDownAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //else if (IsGrabbedHit == true || InAir == true || Carried == true)
            //    GrabHitAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //else if (IsHit == true)
            //    HurtAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //else if (Grabbox.GSIsGrabbed == true)
            //    GrabbedAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //Draw the current action's animation
            /*NOTE: Find a way to keep drawing the action if two animations work with each other in follow-up actions (Ex. Greg's Nunchuck Swing attack hits with the first and follows into the next hit)
              One solution can be to have CanDraw virtual and enable the conditions to change based on the action; then, the previous action can skip to the last frame in its animation if it's finished and needs to in order to display that animation for one more frame*/
            if (ActionManage.CurrentAction.CanDraw() == true)
                ActionManage.CurrentAction.Draw(spriteBatch, OffSet, TileEngine, waterheight, drawcolor, GetRotation, depth);
            //else if (IsAttacking == true)
            //    AttackingAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //else if (IsWalking == true)
            //    WalkingAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
            //else if (IsDead == false)
            //    StandAnim.Draw(spriteBatch, SpriteSheet, drawloc, FacingRight, drawcolor, GetRotation, depth);
        }

        //Checks if the enemy is dead and is a boss; if so, kill all the other enemies that aren't dead yet
        protected void CheckBossDead()
        {
            if (IsBoss() == true && IsDead == true)
            {
                //Kill all the enemies that are still alive
                for (int i = 0; i < SubLvl.GetEnemies.Count; i++)
                {
                    //If the enemy isn't dead, kill the enemy and reset its player index so the player that hit it last doesn't get points for killing it in this case
                    if (SubLvl.GetEnemies[i].IsDead == false)
                    {
                        //EnemList[i].Die();
                        SubLvl.GetEnemies[i].ClearLastAttacked();
                    }
                }
            }
        }

        protected override void FighterUpdate()
        {
            //UpdateKnockDownAnim(Main.GetActiveTime);

            //Check if enemy is in grab hitstun
            //UpdateGrabbedHitAnim(Main.GetActiveTime);

            //Check if enemy is still in hitstun
            //UpdateHitAnim(Main.GetActiveTime);

            //Choose a random target if the current one is null (target died completely)
            if (Target == null || Target.IsDead == true || ActionManage.TargetSet == false)
                NewTarget(SubLvl.GetPlayers);

            //Checks if the enemy is capable of doing an action
            if (CanDoAction() == true)
            {
                if (InAir == false)
                {
                    SetDoAction();
                }
            }

            //Grabbox.Update(Main.GetActiveTime);

            //Check if the enemy is a boss and died
            CheckBossDead();
        }

        public override void LevelEditorUpdate(SublevelProp levelprop)
        {
            base.LevelEditorUpdate(levelprop);

            DrawColor = status.GStatusColor;
        }

        protected override void FighterDraw(SpriteBatch spriteBatch, Vector2 cameraloc, TileEngine TileEngine)
        {
            //Don't draw the enemy if the enemy is invisible when invincible
            if (status.GStatusColor.A != 0)
            {
                DrawAnimations(spriteBatch, cameraloc, TileEngine, ObjectTile.GetObjectDepthInTile(CurHeight), DrawColor, GetDrawDepth(cameraloc));

                //If the enemy has the NoJump, NoAttack, NoGrab, or NoSpecial status, draw the NoStatus symbol above the enemy's head
                status.DrawNoStatusSymbol(spriteBatch, new Vector2(Location.X + cameraloc.X - (Hurtbox.Width / 2), Location.Y - Location.Z + cameraloc.Y - (int)(ObjectHeight * 1.3f)), GetDrawDepth(cameraloc) + .001f);
            }
        }
    }
}