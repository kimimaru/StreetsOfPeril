﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //Bonus stages - Final Fight style!
    //IDEA FOR SEQUEL: Have it so a possible reward for Bonus Stages can be a 30 second positive status at the beginning of the next level (including Invincible)!
    public abstract class BStage : SubLevel
    {
        //The numerical value of each Bonus Stage
        public enum Bonuses
        {
            GrabItem, GBarrel, AvoidRock
        };

        //The amount of time the players have for the bonus stage
        protected float Timer;
        protected float PrevTime;

        //Check if the time should be stopped or not
        protected bool StopTime;

        //For flashing "Bonus Stage" on the screen after the intro
        protected bool Started;
        protected bool On;
        protected int FlashCount;
        protected float PrevDuration;

        public BStage()
        {
            SubLevelFinished = false;

            TileEngine = new TileEngine(new Vector2(1000, 680), -100, -100);
            Solids = new List<BeatEmUpObj>();

            Fade = new Fade(-6, 0, 255);

            Started = false;
            On = true;
            FlashCount = 0;
            PrevDuration = 0;
        }

        //Chooses a random bonus stage to add at the end of the level when a player picks up a Bonus Token
        public static BStage RandomBonusStage(List<Player> Players)
        {
            Random randnum = new Random();
            int randbonus = randnum.Next(Enum.GetValues(typeof(Bonuses)).Length);

            switch (randbonus)
            {
                case (int)Bonuses.GrabItem: return new GrabItem(Players);
                case (int)Bonuses.GBarrel: return new GBarrel(Players);
                default: return new AvoidRock(Players);
            }
        }

        public override bool CanPause()
        {
            return (Started == true && Fade.IsFading() == false && Main.CurrentFPS == Main.FPS);
        }

        //Switches the text at the top on and off
        protected void TextSwitch(float activeTime)
        {
            if (PrevDuration == 0) PrevDuration = activeTime;

            if ((activeTime - PrevDuration) >= 1500f)
            {
                On = !On;
                PrevDuration = 0;
                FlashCount++;
                if (FlashCount == 3)
                {
                    Started = true;
                    PrevTime = activeTime + (Timer * 1000f);
                }
            }

            UpdatePlayerAnimations(activeTime);
        }

        //The fading black intro and outro of bonus stages
        protected override void Transition(float activeTime)
        {
            Fade.Update(activeTime);
            if (Fade.Finished() == true) SubLevelFinished = true;
            else if (Fade.IsFadingIn() == true) UpdatePlayerAnimations(activeTime);
            else if (Fade.IsFading() == false && Fade.IsFadingIn() == false) LoadSounds.PlayBonusMusic();
        }

        protected virtual void EndBonusWon(float activeTime)
        {
            base.EndSubLevel();
        }

        protected virtual void EndBonusLost(float activeTime)
        {
            base.EndSubLevel();
        }

        //Spawns Players in the right spots for bonus stages
        //public override void SpawnBonus(float activeTime);

        //Tells if the bonus stage is completed (before done)
        protected virtual bool BonusWon()
        {
            return Timer <= 0f;
        }

        protected virtual bool BonusLost()
        {
            return Timer <= 0f;
        }

        //Tells if the bonus stage is done
        public override bool SubLevelComplete()
        {
            return SubLevelFinished;
        }

        //Updates the timer
        protected virtual void UpdateTimer(float activeTime)
        {
            //If the time is enabled, update it
            if (StopTime == false)
            {
                //Update the timer if it's greater than 0
                //Get the time down to the nearest 2 digits for more accurate time
                if (Timer > 0f) Timer = (PrevTime - activeTime) / 1000f;

                //If the time reaches less than 0, end the challenge stage
                if (Timer <= 0f)
                {
                    Timer = 0;
                    Fade.ContinueFade();
                }
            }
        }

        public override void Update(float activeTime, Main main)
        {
            //Don't update if you have to draw the bonus stage intro/outro
            if (Fade.IsFading() == false)
            {
                //Check if the bonus stage was lost
                if (BonusLost() == true)
                {
                    //Does whatever needs to be done after losing a bonus stage
                    EndBonusLost(activeTime);
                }
                //Check if the bonus stage was won
                else if (BonusWon() == true)
                {
                    //Does whatever needs to be done after winning a bonus stage
                    EndBonusWon(activeTime);
                }
                //Update like normal
                else
                {
                    //If the bonus stage was started, update everything
                    if (Started == true)
                    {
                        //Pause game
                        if (CanPause() == true)
                        {
                            main.PauseGame(Players);
                            if (Main.IsPaused == true) return;
                        }

                        UpdateCollisions(activeTime);

                        UpdateCamera(activeTime);

                        //Update lists
                        UpdateEnemyList(activeTime, true);
                        UpdateContainerList(activeTime);
                        UpdateItemList(activeTime);
                        UpdateWeaponList(activeTime);
                        UpdateHazards(activeTime);

                        //Update players
                        UpdatePlayers(activeTime);

                        //Update the timer
                        UpdateTimer(activeTime);

                        //Does anything that needs to be done when the bonus stage isn't complete
                        UpdateOtherNotComplete(activeTime);
                    }
                    //Otherwise, if the bonus stage wasn't started yet, wait for the text saying "Bonus Stage!" followed by "(Text describing bonus stage here)" to end
                    else
                    {
                        TextSwitch(activeTime);
                    }
                }
            }
            else
            {
                Transition(activeTime);

                //Does anything that needs to be done when the bonus stage is complete while transitioning
                UpdateOtherComplete(activeTime);
            }

            //Does anything that needs to be done regardless of whether the bonus stage is complete or not
            UpdateOther(activeTime);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            DrawTimer(spriteBatch);
        }

        protected virtual void DrawTimer(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(LoadGraphics.HUDFont, "Time: " + String.Format("{0:0}", Timer), new Vector2(Main.ScreenHalf.X - 40, 50), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, .998f);
        }
    }
}
