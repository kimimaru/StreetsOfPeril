﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //The Avoid Rock bonus stage - Rocks are constantly falling and the players must make it back safely; the score earned at the end is based on how many players survived and how fast they made it there
    //Jeff gets his health reduced down to 1 bar so it's more fair, and all players have the NoSpecial status so they're unable to use their special attacks
    //There are some item containers that have nothing in them; the space they provide upon being broken is completely safe and allows the players to group up there
    //When players die, they are removed from the temp list, which stores the players that are alive
    //If this ends up being too easy, maybe make the rocks fall on their own or something
    //Rules on difficulty: 
    //1. No spot is ever safe aside from the starting area and container spots
    //2. If Graham were to walk through the entire thing in a straight line, it should take roughly half the total time required for the bonus stage
    public class AvoidRock : BStage
    {
        private bool GiveScore;
        private int CurPlayerCheck;
        private float PrevCompletion;

        //Stores the X position of the rocks so when they bounce after hitting players they spawn in the same spots
        private List<float> OrigRockX;

        //Stores the original status of the rocks
        private List<Status> OrigRockStat;

        //A list of temporary players for those that temporarily died so the camera can scroll properly and objects don't react to temporarily dead players
        private List<Player> TempPlayers;

        public AvoidRock(List<Player> players)
        {
            Timer = 30;

            GiveScore = false;
            CurPlayerCheck = 0;
            PrevCompletion = 0f;
            PrevTime = 0f;

            LevelData leveldata = LevelEditor.LoadLevel("AvoidRock").ConvertToLevelData();
            TileEngine = leveldata.TileEngine;

            OrigRockX = new List<float>();
            OrigRockStat = new List<Status>();
            TempPlayers = new List<Player>();
            Players = players;

            TempPlayers.AddRange(Players);

            //LevelCamera = new Camera(LoadGraphics.BG, LoadGraphics.FG, new LevelManager(leveldata));

            //Load all the original X values in
            for (int i = 0; i < leveldata.HazardPoints.Count; i++)
            {
                FallingRock rock = leveldata.HazardPoints[i].NewObject as FallingRock;

                if (rock != null)
                {
                    OrigRockX.Add(rock.GetLocationHeight.X);
                    OrigRockStat.Add(rock.status);
                }
            }
        }

        protected override bool BonusWon()
        {
            //Check if all the players made it to the end
            for (int i = 0; i < TempPlayers.Count; i++)
            {
                if (TempPlayers[i].GetLocationHeight.X < ((TileEngine.GetTileLength(true) - 4) * TileEngine.TileSize) || TempPlayers[i].IsDead == true) return false;
            }

            return true;
        }

        protected override void EndBonusWon(float activeTime)
        {
            //The players successfully got to the end, so start giving bonus points
            if (GiveScore == false)
            {
                GiveScore = true;
                FlashCount++;
                PrevCompletion = activeTime;
                PrevTime = activeTime;
                Timer = (int)Timer;
            }

            //Wait a second and a half, then start giving the players points for completing the bonus
            if ((activeTime - PrevCompletion) >= 1500f)
            {
                //Give players 500 points for each second remaining in the bonus stage
                if (Timer > 0f)
                {
                    if ((activeTime - PrevTime) >= 75f)
                    {
                        Timer--;
                        for (int i = 0; i < TempPlayers.Count; i++)
                        {
                            TempPlayers[i].AddScore(500);
                        }
                        LoadSounds.Play(LoadSounds.ScoreAdd);
                        PrevTime = activeTime;
                    }
                }
                //Give players 5000 points for each player remaining in the bonus stage
                else if ((activeTime - PrevTime) >= 500f)
                {
                    //If a player isn't completely dead or hasn't died in the bonus stage, give 5000 points to each player; skip over players so it doesn't wait 200 milliseconds on someone dead
                    if (CurPlayerCheck < TempPlayers.Count)
                    {
                        for (int i = 0; i < TempPlayers.Count; i++)
                        {
                            TempPlayers[i].AddScore(5000);
                        }
                        LoadSounds.Play(LoadSounds.ScoreAdd);

                        CurPlayerCheck++;
                        PrevTime = activeTime;
                    }
                    else if (CurPlayerCheck >= TempPlayers.Count)
                    {
                        base.EndBonusWon(activeTime);
                    }
                }
            }
        }

        //Spawns Players in the right spots for bonus stages
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, TileEngine.TileSize, (i * TileEngine.TileSize) + 168, 0f, true, false);
                Players[i].SetStats(null, 0, null, null, null, new Status((int)Status.Statuses.NoSpecial, 1f, 0));
            }

            LevelCamera.Update(this);
        }

        //End the bonus if there's no time left, all players are completely dead, or every player who's alive makes it to the end of the stage
        protected override bool BonusLost()
        {
            //Check if all the players are dead; that is, none are left in the temporary player list
            return ((GiveScore == false && base.BonusLost() == true) || TempPlayers.Count <= 0);
        }

        protected override void UpdateHazards(float activeTime)
        {
            for (int i = 0; i < Hazards.Count; i++)
            {
                FallingRock rock = Hazards[i] as FallingRock;

                if (rock != null)
                {
                    rock.ImmediateFall();
                }
                Hazards[i].Update();

                if (Hazards[i].ShouldRemove == true)
                {
                    //When the rock lands, respawn it - all rocks deal the same damage
                    Hazards[i] = new FallingRock(rock, OrigRockX[i], 100, 50, OrigRockStat[i]);
                }
            }
        }

        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            for (int i = 0; i < TempPlayers.Count; i++)
            {
                TempPlayers[i].Update();

                //Check if a player is dead; if so, state that the player is dead and stop updating that player
                if (TempPlayers[i].IsDead == true && TempPlayers[i].KnockedDown == false)
                {
                    TempPlayers.RemoveAt(i);
                    i--;
                }
            }
        }

        protected override void DrawPlayers(float activeTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < TempPlayers.Count; i++)
            {
                TempPlayers[i].Draw(spriteBatch, GCameraOffSet, TileEngine);
            }
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            base.DrawOther(spriteBatch);

            if (On == true && Fade.IsFading() == false)
            {
                if (FlashCount == 0)
                    spriteBatch.DrawString(LoadGraphics.HUDFont, "Bonus Stage!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                else spriteBatch.DrawString(LoadGraphics.HUDFont, "Avoid falling rocks!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
            }
            else if (FlashCount >= 4)
            {
                spriteBatch.DrawString(LoadGraphics.HUDFont, "Complete!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
            }
        }
    }
}
