﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Grab Item bonus stage - point items come flying at the players and they have to grab as many as they can until time is up
    public class GrabItem : BStage
    {
        //How many items can be on screen at once (scales with number of players)
        private int NumItems;

        //Checks if there's already a biscuit on the map
        private Item Biscuit;

        public GrabItem(List<Player> players)
        {
            BG = LoadGraphics.BG;

            Players = players;

            TileEngine = LevelEditor.LoadLevel("GrabItem").TileEngine;

            Timer = 15;

            NumItems = 7;

            //The more players there are, the more items there are onscreen at once
            for (int i = 0; i < players.Count; i++) NumItems += 2;

            Biscuit = null;
        }

        //Spawns a random bonus item with a random velocity at one of the 8 cardinal directions on the screen
        private Item RandItem()
        {
            //Random variables
            Random rand = new Random();

            //Stuff influenced by the random variables
            Item newitem;
            Vector2 velocity;
            Vector2 location;

            RandLocVelocity(rand, out location, out velocity);

            //A special biscuit used to increase your movement speed to help you pick up more items - only one can be on the map at a time and there is a 1/5 chance of it appearing when another item spawns
            if (Biscuit == null && rand.Next(0, 5) == 0)
            {
                Biscuit = new Item(LoadGraphics.ItemSprite, "Biscuit", 0, 0, 1, false, 0, new Status((int)Status.Statuses.SpeedBoost, 3500), Vector2.Zero, new Vector2(rand.Next(20, (int)Main.ScreenSize.X - 20), rand.Next(20, (int)Main.ScreenSize.Y - 20)));
                ItemList.Add(Biscuit);
            }

            //Randomly choose one of the 4 different types of items to spawn; there is an increasingly lower chance of spawning one that gives more points
            int randitem = rand.Next(10);

            //0-3 generates the worst item, 4-6 generates the next worst, 7-8 generates the second best, and 9 generates the best
            if (randitem < 4)
                newitem = new Item(LoadGraphics.ItemSprite, "Bonus Item 1", 0, 0, 1, false, 500, new Status(), velocity, location);
            else if (randitem < 7)
                newitem = new Item(LoadGraphics.ItemSprite, "Bonus Item 2", 0, 0, 1, false, 1000, new Status(), velocity, location);
            else if (randitem < 9)
                newitem = new Item(LoadGraphics.ItemSprite, "Bonus Item 3", 0, 0, 1, false, 2000, new Status(), velocity, location);
            else
                newitem = new Item(LoadGraphics.ItemSprite, "Bonus Item 4", 0, 0, 1, false, 4000, new Status(), velocity, location);

            return newitem;
        }

        //Spawns an item in one of the 8 cardinal directions on screen and gives the item a velocity based on where it spawned
        private void RandLocVelocity(Random rand, out Vector2 location, out Vector2 velocity)
        {
            int randvel = rand.Next(8);

            switch (randvel)
            {
                //Left
                case 0:
                    location = new Vector2(-26, rand.Next(13, 253));
                    velocity = new Vector2(rand.Next(2, 8), 0);
                    break;
                //Bottom
                case 1:
                    location = new Vector2(rand.Next(10, 405), 300);
                    velocity = new Vector2(0, -rand.Next(2, 8));
                    break;
                //Right
                case 2:
                    location = new Vector2(442, rand.Next(13, 253));
                    velocity = new Vector2(-rand.Next(2, 8), 0);
                    break;
                //Top
                case 3:
                    location = new Vector2(rand.Next(10, 405), -33);
                    velocity = new Vector2(0, rand.Next(2, 8));
                    break;
                //Diagonal Top-Left
                case 4:
                    location = new Vector2(rand.Next(0, 104), -33);
                    velocity = new Vector2(rand.Next(2, 8), rand.Next(2, 8));
                    break;
                //Diagonal Bottom-Left
                case 5:
                    location = new Vector2(rand.Next(0, 104), 300);
                    velocity = new Vector2(rand.Next(2, 8), -rand.Next(2, 8));
                    break;
                //Diagonal Bottom-Right
                case 6:
                    location = new Vector2(rand.Next(312, 416), 300);
                    velocity = new Vector2(-rand.Next(2, 8), -rand.Next(2, 8));
                    break;
                //Diagonal Top-Right
                default:
                    location = new Vector2(rand.Next(312, 416), -33);
                    velocity = new Vector2(-rand.Next(2, 8), rand.Next(2, 8));
                    break;
            }
        }

        //Spawns Players in the right spots for bonus stages
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Spawn(this);
                Players[i].SpawnSub(activeTime, TileEngine, (i * 150) + 100, 200, 0f, true);
            }
        }

        //Update all items
        protected override void UpdateItemList(float activeTime)
        {
            for (int i = 0; i < ItemList.Count; i++)
            {
                //if (ItemList[i].GetTimesHeal != 0)
                //    ItemList[i].Move(activeTime, TileEngine);

                ItemList[i].Update();

                if ((ItemList[i].ShouldRemove == true) || (ItemList[i].CollisionBox.X <= -40 || ItemList[i].CollisionBox.X >= (Main.ScreenSize.X + 40) || ItemList[i].CollisionBox.Y <= -40 || ItemList[i].CollisionBox.Y >= (Main.ScreenSize.Y + 40)))
                {
                    //Check if the biscuit was removed from the list
                    if (Biscuit == ItemList[i]) Biscuit = null;
                    ItemList.RemoveAt(i);
                    i--;
                }
            }
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            //If there are fewer items than should be spawned, spawn more items
            if (ItemList.Count < NumItems)
                ItemList.Add(RandItem());
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            base.DrawOther(spriteBatch);

            if (On == true && Fade.IsFading() == false)
            {
                if (FlashCount == 0)
                    spriteBatch.DrawString(LoadGraphics.HUDFont, "Bonus Stage!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                else spriteBatch.DrawString(LoadGraphics.HUDFont, "Pick up items!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
            }
        }
    }
}
