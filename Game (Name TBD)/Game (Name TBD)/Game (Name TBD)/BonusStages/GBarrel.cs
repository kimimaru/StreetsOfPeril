﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Guess the Barrel bonus stage - There are 9 barrels randomized with different items (sometimes duplicates, and there are 3 barrels with nothing in them)
    //The items are shown above the barrels; shortly afterwards, the barrels are moved around 3 times
    //Players take turns breaking barrels (the order is randomized at the start) and get to keep the item inside; only one barrel per player
    public class GBarrel : BStage
    {
        //The max number of barrels in the bonus stage
        private const int MaxBarrels = 9;

        //Makes sure all the items in the container are unique
        //private bool[] Selected;

        //A list of all the item sprites so they can be displayed above their barrels before the barrels move
        private List<Texture2D> ItemDisplayList;
        private readonly float MaxItemDisplayY;
        private float ItemDisplayY;
        private float PrevDisplay;

        //The barrels that switch places with each other
        private List<Vector2> BarrelSwitch;
        private int SwitchTimes;
        private bool Set;
        private bool Switched;

        //Checks if a barrel was broken to prevent other barrels from being broken by the same player
        private bool Broke;
        private bool CompletelyBroken;

        //Counter for how many barrels need to be broken
        private int BCount;

        //The player to update
        private List<int> PlayerOrder;
        private int PlayerC;

        public GBarrel(List<Player> players)
        {
            BG = LoadGraphics.BG;

            Players = players;

            ItemDisplayList = new List<Texture2D>();
            MaxItemDisplayY = 51f;
            ItemDisplayY = 0f;
            PrevDisplay = 0f;
            BarrelSwitch = new List<Vector2>();
            SwitchTimes = 0;
            Set = false;
            Switched = false;

            Timer = 10;
            StopTime = true;

            //Selected = new bool[4];
            Broke = false;
            CompletelyBroken = false;
            BCount = Players.Count;
            PlayerOrder = new List<int>();
            PlayerC = 0;

            LevelData level = LevelEditor.LoadLevel("GBarrel").ConvertToLevelData();

            Random random = new Random();
            int noitem = 0;

            //Create a list of the player indexes to choose from when randomizing the order they go in
            List<int> playerorder = new List<int>();
            for (int i = 0; i < Players.Count; i++) playerorder.Add(i);

            //Randomize the order the players go in
            for (int i = 0; i < Players.Count; i++)
            {
                //Choose from the available players
                int randplayer = random.Next(0, playerorder.Count);

                //Make sure that the same player doesn't get put in the list twice by removing that player's index from the possible indexes to choose from
                PlayerOrder.Add(playerorder[randplayer]);
                playerorder.RemoveAt(randplayer);
            }

            //Add the 9 barrels, making sure that 3 and only 3 have nothing in them
            //A Graham Doll has a 50% chance of appearing in the bonus stage, and only one of them can be in the bonus stage at a time
            for (int i = 0; i < level.ContPoints.Count; i++)
            {
                Item newitem = RandomItem(noitem < 3 ? random.Next(0, 6) : random.Next(0, 5));

                //Keep track of how many containers have nothing in them and make sure that they eventually fill up
                if (newitem == null)
                {
                    noitem++;
                }
                //Automatically set the remaining containers to have nothing if the random numbers didn't produce enough empty containers
                else if (i >= (level.ContPoints.Count - 3) && (i - noitem) >= (MaxBarrels - 3))
                {
                    newitem = null;
                    noitem++;
                }
                
                ItemDisplayList.Add(newitem != null ? newitem.SpriteSheet : null);

                ContainerList.Add(CreateObjects.Barrel(new Vector3(level.ContPoints[i].NewObject.GetLocationHeight.X, level.ContPoints[i].NewObject.GetLocationHeight.Y, 0f), 1, newitem, null));
                BarrelSwitch.Add(new Vector2(level.ContPoints[i].NewObject.GetLocationHeight.X, level.ContPoints[i].NewObject.GetLocationHeight.Y));
            }
            //Solids.AddRange(ContainerList);
        }

        public override bool CanPause()
        {
            return (base.CanPause() == true && Switched == true);
        }

        //Randomizes the items in the containers
        private Item RandomItem(int rand)
        {
            //if (Selected[rand] == false)
            //{
                //Selected[rand] = true;

                switch(rand)
                {
                    case 0:
                        return CreateObjects.BagofJewels();
                    case 1:
                        return CreateObjects.PileOfMoney();
                    case 2:
                        return CreateObjects.PocketWatch();
                    case 3:
                        return CreateObjects.Turkey(new Status());
                    case 4:
                        return CreateObjects.GrahamDoll();
                    default: 
                        return null;
                }
            //}
        }

        //The player whose turn it is
        private int CurrentPlayer
        {
            get { return PlayerOrder[PlayerC]; }
        }

        //If all the barrels that needed to be broken are broken, the bonus stage is done
        protected override bool BonusLost()
        {
            return BCount <= 0;
        }

        protected override bool BonusWon()
        {
            return BCount <= 0;
        }

        //Spawns Players in the right spots for bonus stages
        public override void SpawnPlayers(float activeTime, bool first = false)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Players[PlayerOrder[i]].Spawn(this);
                Players[PlayerOrder[i]].SpawnSub(activeTime, TileEngine, (i * 100) + 52, 250, 0f, true);
            }
        }

        //A special collision method with item containers making sure that the player can't break more than one item container; it's also more efficient because it checks for only the active player
        private void ContCollisions(float activeTime)
        {
            List<Item> itemlist = ItemList;
            List<Weapon> weaponlist = WeaponList;

            //Container collisions
            for (int i = 0; i < ContainerList.Count; i++)
            {
                //Check if container was broken
                for (int h = 0; h < Players[CurrentPlayer].Hitboxes.Count; h++)
                {
                    /*if (ContainerList[i].CanBreak(Players[CurrentPlayer].GetHitboxes[h]) == true)
                    {
                        //If you hit a container, reset the previous HUD so it doesn't show up so you can draw the new one
                        //HUD.ChangeHUD(activeTime, CurrentPlayer, ContainerList[i].GetHUD);

                        //Make the container take damage
                        ContainerList[i].Break(activeTime, Players[CurrentPlayer].TotalDamageDealt(Players[CurrentPlayer].GetHitboxes[h]), Players[CurrentPlayer].GetHitboxes[h], itemlist, weaponlist, TileEngine, Solids);

                        Players[CurrentPlayer].EnterHitLag();

                        if (ContainerList[i].IsDead == true) return;
                    }*/
                }

                //Check if Wil's bullets hit item containers
                /*if (Players[CurrentPlayer].GBullets != null)
                {
                    for (int j = 0; j < Players[CurrentPlayer].GBullets.Count; j++)
                    {
                        if (ContainerList[i].CanBreak(Players[CurrentPlayer].GBullets[j].GHitbox) == true)
                        {
                            //If you hit a container, reset the previous HUD so it doesn't show up so you can draw the new one
                            //HUD.ChangeHUD(activeTime, CurrentPlayer, ContainerList[i].GetHUD);

                            //Make the container take damage
                            ContainerList[i].Break(activeTime, Players[CurrentPlayer].GetDamage + Players[CurrentPlayer].GBullets[j].GHitbox.Strength, Players[CurrentPlayer].GBullets[j].GHitbox, itemlist, weaponlist, TileEngine, Solids);

                            //Make Wil lose Health or a free bullet if the bullet hit the enemy
                            Players[CurrentPlayer].SpecialAttackHit(activeTime, j);

                            //Make the bullet do something when it hits (Ex. bullet bouncing off or disappearing)
                            Players[CurrentPlayer].GBullets[j].Intersect();

                            if (ContainerList[i].IsDead == true) return;
                        }
                    }
                }*/
            }
        }

        //Moves onto the next player
        private void NextPlayer(float activeTime)
        {
            BCount--;
            Broke = false;
            StopTime = true;
            Players[CurrentPlayer].ChangeControl(true);
            if (PlayerC < (Players.Count - 1))
            {
                PlayerC++;
                Timer = 10;
                Switched = false;
            }
        }

        //Shows the items in the barrels for 2 seconds, then shuffles the barrels
        private void ShowItemShuffle(float activeTime)
        {
            //Show the items in the containers for a while
            if (PrevDisplay == 0f || ItemDisplayY != 0f)
            {
                if (PrevDisplay == 0f)
                {
                    ItemDisplayY++;
                    if (ItemDisplayY == MaxItemDisplayY)
                    {
                        PrevDisplay = activeTime;
                    }
                }
                else if ((activeTime - PrevDisplay) >= 2000f)
                    ItemDisplayY--;
            }
            //Shuffle the barrels
            else
            {
                //Check if the new barrel locations are set
                if (Set == false)
                {
                    Random random = new Random();

                    //Create a list of the possible barrels each other barrel can switch to, then remove from it as you choose barrels
                    List<int> Shuffled = new List<int>();
                    for (int i = 0; i < BarrelSwitch.Count; i++) Shuffled.Add(i);

                    bool BreakEarly = false;

                    //Find the barrels that each barrel is switching places with
                    for (int i = 0; i < BarrelSwitch.Count; i++)
                    {
                        //If this index wasn't already chosen, go on; otherwise skip it
                        if (Shuffled.Contains(i) == true)
                        {
                            //Remove the current index from the shuffled list; this ensures that it can't be chosen again
                            Shuffled.Remove(i);

                            //If there are no more indexes to choose from because there are an odd number of barrels, choose any other possible barrel aside from this one
                            if (Shuffled.Count == 0)
                            {
                                //Re-add all the barrel locations
                                for (int j = 0; j < BarrelSwitch.Count; j++)
                                {
                                    if (j != i) Shuffled.Add(j);
                                }

                                //Since this is the last barrel switching, break out of the loop early
                                BreakEarly = true;
                            }

                            //The random barrel location to switch to
                            int randnum = random.Next(0, Shuffled.Count);

                            //Set the barrels to switch places with each other by swapping their locations
                            Helper.SwapInCollection<Vector2>(BarrelSwitch, i, Shuffled[randnum]);

                            //Remove the index chosen from the list so it can't be chosen again
                            Shuffled.RemoveAt(randnum);

                            //Break out of the loop if every barrel has a new destination
                            if (BreakEarly == true) break;
                        }
                    }

                    Set = true;
                }
                else
                {
                    bool inplace = true;

                    //Move each barrel to its new location
                    for (int i = 0; i < BarrelSwitch.Count; i++)
                    {
                        Vector2 origlocation = new Vector2(ContainerList[i].GetLocationHeight.X, ContainerList[i].GetLocationHeight.Y);

                        if (ContainerList[i].GetLocationHeight.X < BarrelSwitch[i].X)
                            ContainerList[i].PureMove(5, 0, 0);
                        else if (ContainerList[i].GetLocationHeight.X > BarrelSwitch[i].X)
                            ContainerList[i].PureMove(-5, 0, 0);
                        if (ContainerList[i].GetLocationHeight.Y < BarrelSwitch[i].Y)
                            ContainerList[i].PureMove(0, 5, 0);
                        else if (ContainerList[i].GetLocationHeight.Y > BarrelSwitch[i].Y)
                            ContainerList[i].PureMove(0, -5, 0);

                        if (ContainerList[i].GetLocationHeight.X != origlocation.X || ContainerList[i].GetLocationHeight.Y != origlocation.Y)
                            inplace = false;
                    }

                    if (inplace == true)
                    {
                        SwitchTimes++;
                        Set = false;

                        if (SwitchTimes > 2)
                        {
                            PrevDisplay = 0f;
                            PrevTime = activeTime + (Timer * 1000f);
                            SwitchTimes = 0;
                            Switched = true;
                            StopTime = false;
                        }
                    }
                }
            }
        }

        //Updates the time remaining - if the player runs out of time, move onto the next player's turn
        protected override void UpdateTimer(float activeTime)
        {
            //Get the time down to the nearest 2 digits for more accurate time
            if (StopTime == false && Timer > 0f) Timer = (PrevTime - activeTime) / 1000f;

            //If the time reaches 0, end the challenge stage
            if (Timer <= 0f)
            {
                Vector2 move = new Vector2(Players[CurrentPlayer].GetLocationHeight.X < (Main.ScreenSize.X + 30) ? 1 : 0, 0);

                //Players[CurrentPlayer].DoAction(activeTime, Vector2.Zero, TileEngine, Solids, 0, move);
                if (Players[CurrentPlayer].GetLocationHeight.X >= (Main.ScreenSize.X + 30))
                {
                    NextPlayer(activeTime);
                }
            }
        }

        //Updates all item containers
        protected override void UpdateContainerList(float activeTime)
        {
            for (int i = 0; i < ContainerList.Count; i++)
            {
                ContainerList[i].Update();

                //If the player destroyed the barrel, mark it as destroyed so more barrels can't be destroyed
                if (ContainerList[i].IsDead == true && Broke == false && CompletelyBroken == false)
                {
                    Broke = true;
                    CompletelyBroken = true;

                    //If the player destroyed one of the empty barrels, move onto the next player - also remove the item sprite for the barrel's item so it doesn't appear if the next player starts and this barrel hasn't been removed yet
                    if (ItemDisplayList[i] == null) Timer = 0;
                    BarrelSwitch.RemoveAt(i);
                }

                if (ContainerList[i].ShouldRemove == true)
                {
                    ContainerList.RemoveAt(i);
                    ItemDisplayList.RemoveAt(i);
                    i--;
                    CompletelyBroken = false;
                }
            }
        }

        //Update all items
        protected override void UpdateItemList(float activeTime)
        {
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].Update();

                //If the player got the item, move onto the next player
                if (ItemList[i].GetTimesHeal == 0 && StopTime == false)
                {
                    StopTime = true;
                    Broke = true;
                    Timer = 0;
                }

                //If the player got the item and the item HUD is complete, 
                if ((ItemList[i].ShouldRemove == true))
                {
                    ItemList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Update all players
        protected override void UpdatePlayers(float activeTime, bool wallcheck = true)
        {
            if (Switched == true) Players[CurrentPlayer].Update();
            
            //Update all players' bullets (and animations if it's not their turn)
            for (int i = 0; i < Players.Count; i++)
            {
                if (i != CurrentPlayer || Switched == false) Players[i].UpdateAnimations();
            }
        }

        protected override void UpdateCollisions(float activeTime)
        {
            if (Broke == false)
                ContCollisions(activeTime);
            //Collision.PlayerItemCollisions(activeTime, this);
        }

        protected override void UpdateOtherNotComplete(float activeTime)
        {
            //Don't let players move until the items were shown and the barrels switched places
            if (Switched == false)
                ShowItemShuffle(activeTime);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            base.DrawOther(spriteBatch);

            if (ItemDisplayY > 0f)
                DrawDisplayItems(spriteBatch);

            if (On == true && Fade.IsFading() == false)
            {
                if (FlashCount == 0)
                    spriteBatch.DrawString(LoadGraphics.HUDFont, "Bonus Stage!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
                else spriteBatch.DrawString(LoadGraphics.HUDFont, "Pick a barrel!", new Vector2(160, 120), Color.Blue, 0f, Vector2.Zero, 1f, SpriteEffects.None, .995f);
            }
        }

        //Draws the items above the barrels
        private void DrawDisplayItems(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < ItemDisplayList.Count; i++)
            {
                if (ItemDisplayList[i] != null && ContainerList[i].IsDead == false)
                    spriteBatch.Draw(ItemDisplayList[i], new Rectangle((int)ContainerList[i].GetLocationHeight.X, (int)ContainerList[i].GetLocationHeight.Y - (int)ItemDisplayY, ItemDisplayList[i].Width, ItemDisplayList[i].Height), null, Color.White * (ItemDisplayY / MaxItemDisplayY), 0f, Animation.DefaultOrigin(ItemDisplayList[i]), SpriteEffects.None, .998f);
            }
        }
    }
}
