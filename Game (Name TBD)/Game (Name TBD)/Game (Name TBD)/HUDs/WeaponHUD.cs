﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The HUD for Weapons; it shows how many more times it can be dropped before disappearing
    public sealed class WeaponHUD : HUD
    {
        //Weapon reference
        private Weapon weapon;

        public WeaponHUD(Weapon Weapon)
        {
            BeatEmUpObject = Weapon;
            weapon = Weapon;
        }

        protected sealed override void HUDDraw(SpriteBatch spriteBatch)
        {
            //Draw the number of times the weapon can be dropped where its health would be; health is not drawn since the max health of Weapons is 0
            spriteBatch.DrawString(LoadGraphics.ImgFont, "x" + weapon.GetLandsRemaining, HealthPos, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
        }
    }
}
