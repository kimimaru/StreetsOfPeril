﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The Player HUD
    /*Here's the HUD priority for the Player's InteractionHUD
     *1. Recent interaction - Item heal, PickUpObj pick up, take damage, deal damage, etc.
     *2. Grabbing Fighter
     *3. Standing over an Item
     *4. Holding a Weapon
     *5. Having an HoT item healing the Player (not the instant it healed)*/
    public class PlayerHUD : FighterHUD
    {
        //How far underneath the Player's HUD the InteractionHUD is set
        public const float PlayerHUDDiff = 48f;

        //How long the heal indicator pops up after a Player goes over an item that restores health
        protected const float HealIndDur = 1f;

        //Bonus Status opacity when inactive
        protected const float BonusStatusOpacity = .4f;

        //The locations for the player HUDs
        protected static readonly Vector2[] PlayerNumHUDLoc;

        //Player reference
        private Player player;

        //Helps with showing how much an item heals when a Player stands over it
        //protected float OrigHealthHeal;
        //protected float PrevHealthHeal;

        public PlayerHUD(Player play) : base(play)
        {
            player = play;

            MainPosition = GetPlayerHUDLoc(player.GPlayerNum);
        }

        static PlayerHUD()
        {
            PlayerNumHUDLoc = new Vector2[4] { new Vector2(4, 15), new Vector2(95, 15), new Vector2(231, 15), new Vector2(322, 15) };
        }

        public static Vector2 GetPlayerHUDLoc(int playernum)
        {
            return PlayerNumHUDLoc[playernum];
        }

        protected override Vector2 NamePos
        {
            get { return (base.NamePos + new Vector2(0, -3)); }
        }

        protected override Vector2 HealthPos
        {
            get { return (base.HealthPos + new Vector2(0, 2)); }
        }

        protected Vector2 ScorePos
        {
            get { return (NamePos + new Vector2(0, 10)); }
        }

        protected Vector2 LivesPos
        {
            get { return (HealthPos + new Vector2((GetHealthBarScale() * GetHealthTextures()[HealthBarIndex].Width) + 2, -1)); }
        }

        protected Vector2 BonusStatusPos
        {
            get { return (StatusPos + new Vector2(0, -13)); }
        }

        //Draws the player's Bonus Status if he/she has one
        protected void DrawBonusStatus(SpriteBatch spriteBatch)
        {
            Color drawcolor = Color.White;

            //If the Bonus Status isn't activated or available, show that to the player by reducing the icon's transparency
            if (player.GetBonusStatus.Activated == false)
                drawcolor *= BonusStatusOpacity;

            //Draw the Bonus Status if the player is about to obtain one
            spriteBatch.Draw(LoadGraphics.StatusBonus, BonusStatusPos, null, GetDrawColor(drawcolor), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
        }

        protected override void HUDDraw(SpriteBatch spriteBatch)
        {
            base.HUDDraw(spriteBatch);

            //Draw the Player's score
            spriteBatch.DrawString(LoadGraphics.ImgFont, fighter.Score.ToString(), ScorePos, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);

            //Draw the Player's lives
            spriteBatch.DrawString(LoadGraphics.ImgFont, "x" + player.GetLives, LivesPos, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);

            //Draw the Bonus Status if it should be drawn
            if (player.GetBonusStatus != null && player.GetBonusStatus.ShouldDraw == true)
                DrawBonusStatus(spriteBatch);
        }
    }
}
