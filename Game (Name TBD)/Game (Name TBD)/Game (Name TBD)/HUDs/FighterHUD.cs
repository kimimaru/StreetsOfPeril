﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A HUD for Fighters
    public class FighterHUD : HUD
    {
        //Fighter reference
        protected Fighter fighter;

        public FighterHUD(Fighter fight)
        {
            BeatEmUpObject = fight;
            fighter = fight;
        }

        protected Vector2 OxygenTankPos
        {
            get { return (HealthPos + new Vector2(0, LoadGraphics.TempBarTex.Height + 1)); }
        }

        protected /*virtual*/ bool ShouldUseTempBarGraphic()
        {
            return true;
        }

        //Return the temp bar graphic if the Fighter has a temp bar
        protected sealed override Texture2D[] GetHealthTextures()
        {
            Texture2D[] healthtextures = base.GetHealthTextures();

            //Check if the Fighter has a temp bar
            if (fighter.HasTempBar == true)
            {
                //The Fighter does; now check one last condition depending on the type of Fighter to see if it can use the temp bar graphic
                if (ShouldUseTempBarGraphic() == true)
                {
                    healthtextures[CurHealthIndex] = LoadGraphics.TempBarTex;
                }
            }

            return healthtextures;
        }

        protected void DrawOxygenTank(SpriteBatch spriteBatch)
        {
            //Get the icon of the OxygenTank so we can use its width and height
            Texture2D tankfill = fighter.OxygenTank.Icon;

            //Draw the outline of the tank icon
            spriteBatch.Draw(LoadGraphics.OxygenTankOutline, OxygenTankPos, null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);

            //The OxygenTank has 5 Health max, so scale the fill of the outline based on how much health it currently has relative to its max health
            int remainingtankfill = (int)(tankfill.Height * (fighter.OxygenTank.GetHealth / (float)fighter.OxygenTank.CurMaxHealth));

            //Draw the fill; the fill has the same dimensions as the outline
            //However, if the fill is shorter because the OxygenTank isn't at max health, draw it lower so it fills up the bottom of the outline
            //NOTE: This currently looks messed up because the filled oxygen tanks icons are not the same size as the outlines! Make them the same size!
            spriteBatch.Draw(tankfill, OxygenTankPos + new Vector2(0, (tankfill.Height - remainingtankfill)), new Rectangle(0, tankfill.Height - remainingtankfill, tankfill.Width, remainingtankfill), GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[0]);
        }

        protected override void HUDDraw(SpriteBatch spriteBatch)
        {
            //If the Fighter has an OxygenTank, show it on the HUD
            if (fighter.HasOxygenTank == true)
                DrawOxygenTank(spriteBatch);
        }
    }
}
