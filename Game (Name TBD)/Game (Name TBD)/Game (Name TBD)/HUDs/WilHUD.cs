﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //The HUD for Wil; we will simply add his free bullets underneath the health bar
    public sealed class WilHUD : PlayerHUD
    {
        //Wil reference
        private Wil wil;

        public WilHUD(Wil wilref) : base(wilref)
        {
            wil = wilref;
        }

        //Draw the free bullets at the end of the health bar
        private Vector2 FreeBulletPos
        {
            get { return (new Vector2((LivesPos.X - 3) - ((LoadGraphics.FreeBulletIcon.Width + 1) * Wil.MaxFreeBullets), OxygenTankPos.Y + 1)); }
        }

        private void DrawFreeBullets(SpriteBatch spriteBatch)
        {
            //Draw the free bullets icon for each free bullet Wil has
            for (int i = 0; i < wil.GetFreeBullets; i++)
            {
                Vector2 nextbulletloc = new Vector2(i * (LoadGraphics.FreeBulletIcon.Width + 1), 0);

                spriteBatch.Draw(LoadGraphics.FreeBulletIcon, FreeBulletPos + nextbulletloc, null, GetDrawColor(Color.White), 0f, Vector2.Zero, 1f, SpriteEffects.None, EndDepth[1]);
            }
        }

        protected sealed override void HUDDraw(SpriteBatch spriteBatch)
        {
            base.HUDDraw(spriteBatch);

            //Draw Wil's free bullets
            DrawFreeBullets(spriteBatch);
        }
    }
}
