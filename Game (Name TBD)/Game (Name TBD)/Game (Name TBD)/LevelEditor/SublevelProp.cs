﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //Sublevel properties, like character starting locations, level entrances/exits, etc.
    //USER EDITED
    [DataContract(Namespace = "")]
    public class SublevelProp
    {
        //The name of the level
        [DataMember]
        public String LevelName;

        //The level background
        protected Texture2D Background;
        protected Vector2 BackgroundStartLoc;

        //Character starting locations
        [DataMember]
        public PlayerSpawn[] StartingLocations;
        [DataMember]
        public Vector2 StartingOffset;

        //Level entrances and exits
        [DataMember]
        public int LevelEntrance;
        [DataMember]
        public int LevelExit;

        //The level offset and tile engine
        public Vector2 OffSet;
        [DataMember]
        public TileEngine TileEngine;

        //The spawns in the level
        [DataMember]
        public List<Spawn> LevelSpawns;

        //The stop points in the level
        [DataMember]
        public List<StopPoint> StopPoints;

        //The type of camera in the level and the range for the Limited camera; false = Normal, true = Limited
        [DataMember]
        public bool CameraType;
        [DataMember]
        public Vector2 CameraRange;

        //Whether the level is underwater or not
        [DataMember]
        public bool Underwater;

        //Whether the level has parallax scrolling or not
        [DataMember]
        public bool Parallax;

        //The background for the level
        [DataMember]
        public int LevelBackground;

        //The music for the level
        [DataMember]
        public int LevelMusic;

        //Tells whether you're editing tiles or not
        public bool TileEdit;

        //What information about the tiles you're seeing
        public bool? TileInfo;

        public SublevelProp()
        {
            LevelName = "Untitled";

            LevelEntrance = (int)Level.LevelEntrances.None;
            LevelExit = (int)Level.LevelExits.None;

            OffSet = Vector2.Zero;
            TileEngine = new TileEngine(new Vector2(416, 320));
            Underwater = false;
            TileEdit = false;
            TileInfo = true;

            CameraType = false;
            CameraRange = Vector2.Zero;

            Parallax = false;

            StartingLocations = new PlayerSpawn[4] { new PlayerSpawn(0, TileEngine), new PlayerSpawn(1, TileEngine), new PlayerSpawn(2, TileEngine), new PlayerSpawn(3, TileEngine) };
            StartingOffset = Vector2.Zero;

            LevelSpawns = new List<Spawn>();
            StopPoints = new List<StopPoint>();

            Background = LoadAssets.BG;
            BackgroundStartLoc = Vector2.Zero;

            LevelBackground = 0;
            LevelMusic = 0;
        }

        public SublevelProp(Texture2D background) : this()
        {
            Background = background;
        }

        //Checks if there is a spawn on the level
        public bool HasSpawn
        {
            get { return (EditorHelper.ArrayEmpty<Spawn>(LevelSpawns) == false); }
        }

        //The rectangle the screen showing the level is in
        public Rectangle LevelRect(Vector2 levelposition)
        {
            return new Rectangle((int)(BackgroundStartLoc.X + levelposition.X), (int)(BackgroundStartLoc.Y + levelposition.Y), 416, 320);
        }

        //Converts the sublevel properties into LevelData readable by the main game
        public LevelData ConvertToLevelData()
        {
            LevelData level = new LevelData(new List<Vector2>(), null, null, null, null);

            IEnumerable<Spawn> sortedSpawns =
                from spawn in LevelSpawns
                orderby spawn.StopPoint ascending
                select spawn;

            LevelSpawns = sortedSpawns.ToList<Spawn>();

            //NOTE: Sort the level spawns by StopNumber before adding them to the list; this way, it won't matter the order you make the spawns, as long as they all spawn at the same stop point!
            //Also, you'll additionally have to sort by SpawnOffset; this is trickier since negative offsets complicate things; make sure to get the above done first since that'll be the biggest improvement!
            for (int i = 0; i < LevelSpawns.Count; i++)
            {
                int? stoppoint = LevelSpawns[i].StopPoint;
                if (stoppoint < 0) stoppoint = null;

                for (int j = 0; j < LevelSpawns[i].SpawnEnemies.Count; j++)
                {
                    level.EnemPoints.Add(new ObjectSpawnPoint<Enemy>(LevelSpawns[i].OffSet, LevelSpawns[i].SpawnEnemies[j].EnemySpawned, stoppoint, LevelSpawns[i].DifficultyLevel, LevelSpawns[i].MinPlayers, LevelSpawns[i].SpawnEnemies[j].Designated));
                }

                for (int j = 0; j < LevelSpawns[i].SpawnContainers.Count; j++)
                {
                    level.ContPoints.Add(new ObjectSpawnPoint<ItemContainer>(LevelSpawns[i].OffSet, LevelSpawns[i].SpawnContainers[j].ContainerSpawned, stoppoint, LevelSpawns[i].DifficultyLevel, LevelSpawns[i].MinPlayers, LevelSpawns[i].SpawnContainers[j].Designated));
                }

                for (int j = 0; j < LevelSpawns[i].SpawnHazards.Count; j++)
                {
                    level.HazardPoints.Add(new ObjectSpawnPoint<Hazard>(LevelSpawns[i].OffSet, LevelSpawns[i].SpawnHazards[j].HazardSpawned, stoppoint, LevelSpawns[i].DifficultyLevel, LevelSpawns[i].MinPlayers, LevelSpawns[i].SpawnHazards[j].Designated));
                }
            }

            for (int i = 0; i < StopPoints.Count; i++)
                level.StopPoints.Add(StopPoints[i].StopOffSet);

            level.TileEngine = TileEngine;

            return level;
        }

        //Checks if the stoppoints are valid and will load in properly
        public bool CheckStopPointsInBounds()
        {
            //Make sure all the stop points are in bounds and can be reached
            for (int i = 0; i < StopPoints.Count; i++)
            {
                Vector2 stoploc = StopPoints[i].TrueLocation;

                if (TileEngine.XYExists(stoploc) == false) return false;
            }

            return true;
        }

        //Checks if the spawns are valid and will load in properly
        public bool CheckSpawnsLoadable()
        {
            //Make sure all the spawns are loadable
            for (int i = 0; i < LevelSpawns.Count; i++)
            {
                if (LevelSpawns[i].Loadable(TileEngine) == false) return false;
            }

            return true;
        }

        //Checks if the player spawns are in bounds
        public bool CheckPlayersInBounds()
        {
            for (int i = 0; i < StartingLocations.Length; i++)
            {
                Vector2 playerloc = new Vector2(StartingLocations[i].Location.X, StartingLocations[i].Location.Y);

                if (TileEngine.XYExists(playerloc) == false) return false;
            }

            return true;
        }

        //Changes the level entrance
        public void ChangeLevelEntrance(int newentrance)
        {
            if (Helper.WithinEnumRange(typeof(Level.LevelEntrances), newentrance) == true)
                LevelEntrance = newentrance;
        }

        //Changes the level exit
        public void ChangeLevelExit(int newexit)
        {
            if (Helper.WithinEnumRange(typeof(Level.LevelExits), newexit) == true)
                LevelExit = newexit;
        }

        //Adds a spawn
        public void AddSpawn(Spawn spawn)
        {
            LevelSpawns.Add(spawn);
        }

        //Removes a Spawn and reorders the remaining spawns
        public void RemoveSpawn(Spawn spawnremoved)
        {
            if (spawnremoved != null)
            {
                int spawnremovednum = spawnremoved.SpawnNumber;

                LevelSpawns.Remove(spawnremoved);
                ReorderSpawns(spawnremovednum);
            }
        }

        //Removes a Spawn by index and reorders the remaining spawns
        public void RemoveSpawn(int spawnindex)
        {
            if (EditorHelper.IndexWithinArray<Spawn>(LevelSpawns, spawnindex) == true && LevelSpawns[spawnindex] != null)
            {
                int spawnremovednum = LevelSpawns[spawnindex].SpawnNumber;

                LevelSpawns.RemoveAt(spawnindex);
                ReorderSpawns(spawnremovednum);
            }
        }

        //Reorders all spawns above this spawn number; this occurs when a spawn is removed
        private void ReorderSpawns(int spawnnumber)
        {
            for (int i = 0; i < LevelSpawns.Count; i++)
            {
                //Subtract 1 from the spawn numbers of all spawns above this one
                if (LevelSpawns[i].SpawnNumber > spawnnumber) LevelSpawns[i].SpawnNumber--;
            }
        }

        //Adds a stop point
        public void AddStopPoint(StopPoint stoppoint)
        {
            StopPoints.Add(stoppoint);
        }

        //Removes a stoppoint by index and reorders the remaining stoppoints
        public void RemoveStopPoint(int stopindex)
        {
            if (EditorHelper.IndexWithinArray<StopPoint>(StopPoints, stopindex) == true && StopPoints[stopindex] != null)
            {
                int stopremovednum = StopPoints[stopindex].StopNumber;

                StopPoints.RemoveAt(stopindex);
                ReorderStopPoints(stopremovednum);
            }
        }

        //Reorders all stoppoints above this stop number; this occurs when a stop point is removed
        private void ReorderStopPoints(int stopnumber)
        {
            for (int i = 0; i < StopPoints.Count; i++)
            {
                //Subtract 1 from the stop numbers of all stoppoints above this one
                if (StopPoints[i].StopNumber > stopnumber) StopPoints[i].StopNumber--;
            }
        }

        //Check for editing a tile
        private void CheckEditTile(MainProgram main)
        {
            Vector2 mouseloc = EditorHelper.GetMousePositionLevel(this);

            if (TileEngine.TileXYExists(new Rectangle((int)mouseloc.X, (int)mouseloc.Y, 1, 1), Vector2.Zero) == true)
            {
                main.SetSelectedObject(new TileMenu(TileEngine, TileEngine.TileAtMouseLoc(mouseloc)));
            }
        }

        private void CheckEditObject(MainProgram main)
        {
            //Check spawns
            for (int i = 0; i < LevelSpawns.Count; i++)
            {
                if (EditorHelper.ClickedRect(LevelSpawns[i].SelectionRectangle(-OffSet)) == true)
                {
                    main.SetSelectedObject(new SpawnMenu(LevelSpawns[i], true));
                    return;
                }
                //Check if you clicked on any of this spawn's objects
                else
                {
                    //Check enemies
                    for (int j = 0; j < LevelSpawns[i].SpawnEnemies.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnEnemies[j].SelectionRectangle(-OffSet)) == true)
                        {
                            main.SetSelectedObject(LevelSpawns[i].SpawnEnemies[j].BringUpMenu());
                            return;
                        }
                    }

                    //Check containers
                    for (int j = 0; j < LevelSpawns[i].SpawnContainers.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnContainers[j].SelectionRectangle(-OffSet)) == true)
                        {
                            main.SetSelectedObject(LevelSpawns[i].SpawnContainers[j].BringUpMenu());
                            return;
                        }
                    }

                    //Check hazards
                    for (int j = 0; j < LevelSpawns[i].SpawnHazards.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnHazards[j].SelectionRectangle(-OffSet)) == true)
                        {
                            main.SetSelectedObject(LevelSpawns[i].SpawnHazards[j].BringUpMenu());
                            return;
                        }
                    }

                    //Check items
                    for (int j = 0; j < LevelSpawns[i].SpawnItems.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnItems[j].SelectionRectangle(-OffSet)) == true)
                        {
                            main.SetSelectedObject(LevelSpawns[i].SpawnItems[j].BringUpMenu());
                            return;
                        }
                    }

                    //Check weapons
                    for (int j = 0; j < LevelSpawns[i].SpawnWeapons.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnWeapons[j].SelectionRectangle(-OffSet)) == true)
                        {
                            main.SetSelectedObject(LevelSpawns[i].SpawnWeapons[j].BringUpMenu());
                            return;
                        }
                    }
                }
            }

            //Check stoppoints
            for (int i = 0; i < StopPoints.Count; i++)
            {
                if (EditorHelper.ClickedRect(StopPoints[i].SelectionRectangle(-OffSet)) == true)
                {
                    main.SetSelectedObject(new StopPointMenu(StopPoints[i], true));
                    return;
                }
            }

            //Check player spawns (starting locations)
            for (int i = 0; i < StartingLocations.Length; i++)
            {
                if (EditorHelper.ClickedRect(StartingLocations[i].SelectionRectangle(-OffSet)) == true)
                {
                    main.SetSelectedObject(new PlayerSpawnMenu(StartingLocations[i]));
                    return;
                }
            }
        }

        private void CheckRemoveObject(MainProgram main)
        {
            //Check removing a stoppoint
            for (int i = 0; i < StopPoints.Count; i++)
            {
                if (EditorHelper.ClickedRect(StopPoints[i].SelectionRectangle(-OffSet)) == true)
                {
                    RemoveStopPoint(i);
                    return;
                }
            }

            //Check spawns
            for (int i = 0; i < LevelSpawns.Count; i++)
            {
                if (EditorHelper.ClickedRect(LevelSpawns[i].SelectionRectangle(-OffSet)) == true)
                {
                    RemoveSpawn(i);
                    return;
                }
                //Check if you clicked on any of this spawn's objects
                else
                {
                    //Check enemies
                    for (int j = 0; j < LevelSpawns[i].SpawnEnemies.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnEnemies[j].SelectionRectangle(-OffSet)) == true)
                        {
                            LevelSpawns[i].RemoveEnemy(j);
                            return;
                        }
                    }

                    //Check containers
                    for (int j = 0; j < LevelSpawns[i].SpawnContainers.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnContainers[j].SelectionRectangle(-OffSet)) == true)
                        {
                            LevelSpawns[i].RemoveContainer(j);
                            return;
                        }
                    }

                    //Check hazards
                    for (int j = 0; j < LevelSpawns[i].SpawnHazards.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnHazards[j].SelectionRectangle(-OffSet)) == true)
                        {
                            LevelSpawns[i].RemoveHazard(j);
                            return;
                        }
                    }

                    //Check items
                    for (int j = 0; j < LevelSpawns[i].SpawnItems.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnItems[j].SelectionRectangle(-OffSet)) == true)
                        {
                            LevelSpawns[i].RemoveItem(j);
                            return;
                        }
                    }

                    //Check weapons
                    for (int j = 0; j < LevelSpawns[i].SpawnWeapons.Count; j++)
                    {
                        if (EditorHelper.ClickedRect(LevelSpawns[i].SpawnWeapons[j].SelectionRectangle(-OffSet)) == true)
                        {
                            LevelSpawns[i].RemoveWeapon(j);
                            return;
                        }
                    }
                }
            }
        }

        //Check for clicking on an object for selecting, or removing, an object
        public void CheckClickObject(MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            //If an object isn't selected
            //NOTE: This is rather inefficient; find a better way to organize it and improve performance because the more objects there are, the more it will lag
            //The first thing I can think of is checking only all the on-screen objects
            if (main.SelectingObject == false)
            {
                //Check if you left-clicked and it was inside the level rectangle
                if (EditorHelper.LeftClicked(mouse) == true && EditorHelper.ClickedRect(LevelRect(Vector2.Zero)) == true)
                {
                    //Check for editing a tile
                    if (TileEdit == true)
                        CheckEditTile(main);
                    //Check for editing an object
                    else CheckEditObject(main);
                }
                //Check if you right-clicked and it was inside the level rectangle
                else if (EditorHelper.RightClicked(mouse) == true && EditorHelper.ClickedRect(LevelRect(Vector2.Zero)) == true)
                {
                    //Check removing an object
                    CheckRemoveObject(main);
                }
            }
        }

        public void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            //Update objects
            for (int i = 0; i < LevelSpawns.Count; i++)
                LevelSpawns[i].Update(this);
        }

        //Draws all the stop points in the level
        protected void DrawStopPoints(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < StopPoints.Count; i++)
                StopPoints[i].Draw(spriteBatch, -OffSet, TileEngine);
        }

        //Draws all the objects in each spawn
        protected void DrawSpawns(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < LevelSpawns.Count; i++)
                LevelSpawns[i].Draw(spriteBatch, -OffSet, TileEngine);
        }

        //Draws the player start locations
        protected void DrawPlayerStartLoc(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < StartingLocations.Length; i++)
            {
                StartingLocations[i].Draw(spriteBatch, this);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 levelposition)
        {
            //Show the name of the level you're editing
            spriteBatch.DrawString(LoadAssets.EditorFont, LevelName, new Vector2(Helper.DrawTextCenter(LoadAssets.EditorFont, LevelName).X + levelposition.X, levelposition.Y), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            //Draw the background if it's not null
            if (Background != null)
            {
                spriteBatch.Draw(Background, -OffSet, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .0000001f);
            }

            //Draw lines enclosing the box for the level
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(416, 0), null, Color.Black, 0f, Vector2.Zero, new Vector2(1, 320), SpriteEffects.None, .9986f);
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(0, 320), null, Color.Black, 0f, Vector2.Zero, new Vector2(417, 1), SpriteEffects.None, .9986f);

            //Check for editing tiles
            if (TileEdit == true) TileEngine.DrawHeight(spriteBatch, OffSet, -OffSet, TileInfo);

            //Draw all the spawns
            DrawSpawns(spriteBatch);

            //Draw all the stop points
            DrawStopPoints(spriteBatch);

            //Draw the player start locations
            DrawPlayerStartLoc(spriteBatch);

            //Draw giant, prioritized boxes around the space that is not the level so we can have level objects properly look like they're offscreen
            spriteBatch.Draw(LoadAssets.BGGraphic, new Vector2(417, 0), null, Color.White, 0f, Vector2.Zero, new Vector2(MainProgram.ScreenSize.X - 417, MainProgram.ScreenSize.Y), SpriteEffects.None, .998f);
            spriteBatch.Draw(LoadAssets.BGGraphic, new Vector2(0, 321), null, Color.White, 0f, Vector2.Zero, new Vector2(418, MainProgram.ScreenSize.Y - 321), SpriteEffects.None, .998f);
        }
    }
}
