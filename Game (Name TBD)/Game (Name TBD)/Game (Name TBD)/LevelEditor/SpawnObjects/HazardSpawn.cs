﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A hazard spawn object
    [DataContract(Namespace = "")]
    [KnownType("GetKnownTypes")]
    public abstract class HazardSpawn : SpawnObject
    {
        //The hazard spawned
        [IgnoreDataMember]
        public Hazard HazardSpawned;

        protected HazardSpawn()
        {
            HazardSpawned = null;
        }

        protected HazardSpawn(Hazard hazardspawned, Spawn spawn) : base(hazardspawned, spawn)
        {
            HazardSpawned = hazardspawned;
        }

        public override void SetObject()
        {
            base.SetObject();

            HazardSpawned = (Hazard)ObjectSpawned;
        }

        public override BeatEmUpObj ParseObject()
        {
            return null;
        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] { typeof(BSShooterSpawn), typeof(FallingRockSpawn), typeof(PoisonGasShooterSpawn), typeof(PlatformSpawn), typeof(ScarecrowSpawn) };
        }
    }
}
