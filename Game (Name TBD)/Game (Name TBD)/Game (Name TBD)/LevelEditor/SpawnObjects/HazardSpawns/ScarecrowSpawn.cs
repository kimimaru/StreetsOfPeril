﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public sealed class ScarecrowSpawn : HazardSpawn
    {
        public ScarecrowSpawn(Scarecrow hazardspawned, Spawn spawn) : base(hazardspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new ScarecrowMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Scarecrow(EditorHelper.ParseVector3(Params[0]));
        }
    }
}
