﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public sealed class PlatformSpawn : HazardSpawn
    {
        public PlatformSpawn(Platform hazardspawned, Spawn spawn)
        {
            HazardSpawned = hazardspawned;

            SpawnAssociated = spawn;
        }

        public override ObjectMenu BringUpMenu()
        {
            return new PlatformMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Platform(EditorHelper.ParseVector3(Params[0]), Convert.ToBoolean(EditorHelper.TextToNumber(Params[1])));
        }
    }
}
