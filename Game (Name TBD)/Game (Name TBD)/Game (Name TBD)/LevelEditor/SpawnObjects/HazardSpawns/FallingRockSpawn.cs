﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public sealed class FallingRockSpawn : HazardSpawn
    {
        public FallingRockSpawn(FallingRock hazardspawned, Spawn spawn)
        {
            HazardSpawned = hazardspawned;

            SpawnAssociated = spawn;
        }

        public override ObjectMenu BringUpMenu()
        {
            return new FallingRockMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.FallingRock(LoadGraphics.FallingRock, LoadGraphics.FallingRockDust, EditorHelper.ParseVector3(Params[0]), Status.StatFull(Params[2]), EditorHelper.TextToNumber(Params[1]));
        }
    }
}
