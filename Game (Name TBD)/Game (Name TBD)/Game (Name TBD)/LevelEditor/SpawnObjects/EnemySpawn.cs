﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //An enemy spawn object
    [DataContract(Namespace = "")]
    [KnownType("GetKnownTypes")]
    public abstract class EnemySpawn : SpawnObject
    {
        //The enemy spawned
        [IgnoreDataMember]
        public Enemy EnemySpawned;

        //The level number the enemy is at; at higher levels, enemies are generally stronger, and this will change stats accordingly
        [DataMember]
        public int LevelNum;

        //The difficulty level the enemy; at higher difficulties, enemies are generally stronger, and this will change stats accordingly
        [DataMember]
        public int DifficultyNum;

        //The alternative, or recolors, of the enemy (Ex. Hector for Mark)
        [DataMember]
        public int Alternative;

        protected EnemySpawn()
        {
            EnemySpawned = null;
            LevelNum = 0;
            DifficultyNum = 0;
            Alternative = 0;
            Designated = true;
        }

        protected EnemySpawn(Enemy enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            EnemySpawned = enemyspawned;
            LevelNum = 0;
            DifficultyNum = 0;
            Alternative = 0;
            Designated = true;
        }

        //Returns the stored location of the enemy object
        protected Vector3 GetStoredLocation
        {
            get 
            {
                if (EditorHelper.IndexWithinArray<String>(Params, 0) == true)
                    return EditorHelper.ParseVector3(Params[0]);
                else return Vector3.Zero;
            }
        }

        //Returns the stored status of the enemy object
        protected Status GetStoredStatus
        {
            get 
            {
                if (EditorHelper.IndexWithinArray<String>(Params, 1) == true)
                    return Status.StatFull(Params[1]);
                else return new Status();
            }
        }

        protected int GetStoredDiffNum
        {
            get
            {
                if ((Params.Length - 2) < 0) return 0;
                return EditorHelper.TextToNumber(Params[Params.Length - 2]);
            }
        }

        protected int GetStoredAlt
        {
            get 
            {
                if ((Params.Length - 1) < 0) return 0;
                return EditorHelper.TextToNumber(Params[Params.Length - 1]); 
            }
        }

        //Returns the stored level number of the enemy object (NOTE: This may be removed later if storing the level number ends up being redundant!)
        protected int GetStoredLevelNum
        {
            get
            {
                if ((Params.Length - 3) < 0) return 0;
                return EditorHelper.TextToNumber(Params[Params.Length - 3]);
            }
        }

        public override void SetObject()
        {
            base.SetObject();

            EnemySpawned = (Enemy)ObjectSpawned;
        }

        public override BeatEmUpObj ParseObject()
        {
            return null;
        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] { typeof(MarkSpawn), typeof(ChristopherSpawn), typeof(PaulSpawn), typeof(TankSpawn), typeof(GregSpawn), typeof(GruntSpawn), typeof(RogueSpawn), 
                                typeof(ChangerSpawn), typeof(ImposterSpawn), typeof(WeaponWielderSpawn), typeof(SamsonSpawn), typeof(TheodoreSpawn), typeof(MinecartRiderSpawn), 
                                typeof(DiverSpawn), typeof(CommanderSpawn) };
        }
    }
}
