﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A PickUpObj spawn object
    [DataContract(Namespace = "")]
    [KnownType("GetKnownTypes")]
    public abstract class PickUpObjSpawn : SpawnObject
    {
        protected PickUpObjSpawn()
        {

        }

        protected PickUpObjSpawn(PickupObj pickupobjspawned, Spawn spawn) : base(pickupobjspawned, spawn)
        {

        }

        private static Type[] GetKnownTypes()
        {
            return new Type[] { typeof(ItemSpawn), typeof(WeaponSpawn) };
        }
    }
}
