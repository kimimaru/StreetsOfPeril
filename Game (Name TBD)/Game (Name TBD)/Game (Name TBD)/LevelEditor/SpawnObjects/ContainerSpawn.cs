﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A container spawn object
    [DataContract(Namespace = "")]
    public class ContainerSpawn : SpawnObject
    {
        //The container spawned
        [IgnoreDataMember]
        public ItemContainer ContainerSpawned;

        protected ContainerSpawn()
        {
            ContainerSpawned = null;
        }

        public ContainerSpawn(ItemContainer containerspawned, Spawn spawn) : base(containerspawned, spawn)
        {
            ContainerSpawned = containerspawned;
        }

        //Adds a PickUpObj to be placed in the container and removes that PickUpObj from the spawn's list, depending on the type of object it is (it'll get added to the level list when the container is broken in-game)
        //To add, you have to place a PickUpObj in the level in the same spawn as the container and drag and drop it on the item container
        public void AddPickUpObj(SpawnObject pickupobj)
        {
            if (pickupobj.GetObject.ObjType == BeatEmUpObj.ObjectType.Item)
            {
                ContainerSpawned.SetHeldObj((PickupObj)pickupobj.GetObject);
                SpawnAssociated.RemoveItem((ItemSpawn)pickupobj);
            }
            else if (pickupobj.GetObject.ObjType == BeatEmUpObj.ObjectType.Weapon)
            {
                ContainerSpawned.SetHeldObj((PickupObj)pickupobj.GetObject);
                SpawnAssociated.RemoveWeapon((WeaponSpawn)pickupobj);
            }
        }

        //Adds an item to be placed in the container and removes that item from the spawn's item list (it'll get added to the level list when the container is broken in-game)
        //To add, you have to place an item in the level in the same spawn as the container and drag and drop it on the item container
        public void AddItem(ItemSpawn item)
        {
            ContainerSpawned = CreateObjects.Barrel(new Vector3(ContainerSpawned.GetLocationHeight.X, ContainerSpawned.GetLocationHeight.Y, ContainerSpawned.CurHeight), ContainerSpawned.GetHealth, item.ItemSpawned, ContainerSpawned.GetHeldWeapon);

            SpawnAssociated.RemoveItem(item);
        }

        //Adds a weapon to be placed in the container and removes that weapon from the spawn's weapon list (it'll get added to the level list when the container is broken in-game)
        //To add, you have to place a weapon in the level in the same spawn as the container and drag and drop it on the item container
        public void AddWeapon(WeaponSpawn weapon)
        {
            ContainerSpawned = CreateObjects.Barrel(new Vector3(ContainerSpawned.GetLocationHeight.X, ContainerSpawned.GetLocationHeight.Y, ContainerSpawned.CurHeight), ContainerSpawned.GetHealth, ContainerSpawned.GetHeldItem, weapon.WeaponSpawned);

            SpawnAssociated.RemoveWeapon(weapon);
        }

        private int GetAltNum
        {
            get
            {
                if ((Params.Length - 1) < 0) return 0;
                return EditorHelper.TextToNumber(Params[Params.Length - 1]);
            }
        }

        public override ObjectMenu BringUpMenu()
        {
            return new ContainerMenu(this, true);
        }

        public override void SetObject()
        {
            base.SetObject();

            ContainerSpawned = (ItemContainer)ObjectSpawned;
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Barrel(EditorHelper.ParseVector3(Params[0]), EditorHelper.TextToNumber(Params[1]), ItemSpawn.ParseHeldItem(Params[2]), WeaponSpawn.ParseHeldWeapon(Params[3]), GetAltNum);
        }

        //private static Type[] GetKnownTypes()
        //{
        //    
        //}
    }
}
