﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class TheodoreSpawn : EnemySpawn
    {
        public TheodoreSpawn(Theodore enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new TheodoreMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Theodore(GetStoredLocation, new Status(), EditorHelper.TextToNumber(Params[1]), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
