﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class PaulSpawn : EnemySpawn
    {
        public PaulSpawn(Paul enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new PaulMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Paul(GetStoredLocation, new Status(), EditorHelper.TextToNumber(Params[1]), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
