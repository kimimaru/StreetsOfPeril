﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class DiverSpawn : EnemySpawn
    {
        public DiverSpawn(Diver enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new DiverMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Diver("Diver", GetStoredLocation, GetStoredStatus, LoadGraphics.enemsprite, EditorHelper.TextToNumber(Params[2]), EditorHelper.TextToNumber(Params[3]), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
