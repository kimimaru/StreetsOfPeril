﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public sealed class CommanderSpawn : EnemySpawn
    {
        public CommanderSpawn(Commander enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new CommanderMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Commander(GetStoredLocation, new Status(), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
