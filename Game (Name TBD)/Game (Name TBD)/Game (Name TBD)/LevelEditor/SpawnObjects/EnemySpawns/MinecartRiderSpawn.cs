﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class MinecartRiderSpawn : EnemySpawn
    {
        public MinecartRiderSpawn(MinecartRider enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {

        }

        public override ObjectMenu BringUpMenu()
        {
            return new MinecartRiderMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            MinecartRider minecartrider = CreateObjects.MinecartRider("Minecart Rider", GetStoredLocation, GetStoredStatus, LoadGraphics.enemsprite, EditorHelper.ParseVector2(Params[3]), GetStoredDiffNum, GetStoredAlt);
            minecartrider.ChangeDirection(Convert.ToBoolean(EditorHelper.TextToNumber(Params[2])));

            return minecartrider;
        }
    }
}
