﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class GruntSpawn : EnemySpawn
    {
        public GruntSpawn(Grunt enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        public override ObjectMenu BringUpMenu()
        {
            return new GruntMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Grunt(GetStoredLocation, GetStoredStatus, EditorHelper.TextToNumber(Params[2]), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
