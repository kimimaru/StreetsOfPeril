﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class ChangerSpawn : EnemySpawn
    {
        public ChangerSpawn(Changer enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {

        }

        public override ObjectMenu BringUpMenu()
        {
            return new ChangerMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            return CreateObjects.Changer(GetStoredLocation, new Status(), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
