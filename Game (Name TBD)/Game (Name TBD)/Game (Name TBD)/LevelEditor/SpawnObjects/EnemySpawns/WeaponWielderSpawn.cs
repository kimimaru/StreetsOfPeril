﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    [DataContract(Namespace = "")]
    public class WeaponWielderSpawn : EnemySpawn
    {
        //Checks if the Weapon Wielder is Gerald or Kate
        public bool Male;

        public WeaponWielderSpawn(WeaponWielder enemyspawned, Spawn spawn) : base(enemyspawned, spawn)
        {
            
        }

        //Gives a weapon to the Weapon Wielder
        public void GiveWeapon(WeaponSpawn weaponspawn)
        {
            //We don't need the weapon to recognize the enemy picking it up because this will happen when the Weapon Wielder is created in-game
            //We just need to know which weapon the Weapon Wielder will be holding so we can pass it into its constructor when it's created in-game
            EnemySpawned.SetWeapon(weaponspawn.WeaponSpawned);
            //EnemySpawned.gsWeapon.PickUp(MainProgram.activeTime, EnemySpawned);

            SpawnAssociated.RemoveWeapon(weaponspawn);
        }

        public override ObjectMenu BringUpMenu()
        {
            return new WeaponWielderMenu(this, true);
        }

        public override BeatEmUpObj ParseObject()
        {
            Male = Convert.ToBoolean(EditorHelper.TextToNumber(Params[2]));

            return CreateObjects.WeaponWielder(GetStoredLocation, GetStoredStatus, Male, WeaponSpawn.ParseHeldWeapon(Params[3]), GetStoredLevelNum, GetStoredDiffNum, GetStoredAlt);
        }
    }
}
