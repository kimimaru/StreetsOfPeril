﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //An item spawn object
    [DataContract(Namespace = "")]
    public class ItemSpawn : PickUpObjSpawn
    {
        //The item spawned
        [IgnoreDataMember]
        public Item ItemSpawned;

        protected ItemSpawn()
        {
            ItemSpawned = null;
        }

        public ItemSpawn(Item itemspawned, Spawn spawn) : base(itemspawned, spawn)
        {
            ItemSpawned = itemspawned;
        }

        //We don't need different spawns for each item since they're not different enough, so bring up menus according to the item's name
        public override ObjectMenu BringUpMenu()
        {
            if (ItemSpawned.GetTimesHeal > 1)
            {
                switch (ItemSpawned.GetTimesHeal)
                {
                    case 3: return new RiceBowlMenu(this, true);
                    case 4: return new EnergyDrinkMenu(this, true);
                    case 50: return new HealthCandyMenu(this, true);
                    default: return null;
                }
            }
            else if (ItemSpawned.Score > 0)
            {
                switch (ItemSpawned.Score)
                {
                    case 1000: return new BiscuitMenu(this, true);
                    case 3000: return new PileOfMoneyMenu(this, true);
                    case 8000: return new PocketWatchMenu(this, true);
                    case 15000: return new BagOfJewelsMenu(this, true);
                    default: return null;
                }
            }
            else
            {
                switch (ItemSpawned.Name)
                {
                    case "Turkey": return new TurkeyMenu(this, true);
                    case "Bonus Token": return new BTokenMenu(this, true);
                    case "Replacement Tank": return new OTReplaceMenu(this, true);
                    case "Health Juice": return new HealthJuiceMenu(this, true);
                    case "Bear Claw": return new BearClawMenu(this, true);
                    case "Clam Shell": return new ClamShellMenu(this, true);
                    case "Graham Trophy": return new GrahamTrophyMenu(this, true);
                    case "Graham Doll": return new GrahamDollMenu(this, true);
                    case "Mystery Pudding": return new MysteryPuddingMenu(this, true);
                    default: return null;
                }
            }
        }

        public override void SetObject()
        {
            base.SetObject();

            ItemSpawned = (Item)ObjectSpawned;
        }

        //Returns an item that an object (ItemContainer) must hold based on a string
        public static Item ParseHeldItem(String itemstring)
        {
            Item item = null;

            String[] Params = itemstring.Split(',');

            //Make sure the item is in the right format
            if (Params.Length == 4)
            {
                Status status = Status.StatFull(Params[1] + "," + Params[2] + "," + Params[3]);

                switch (Params[0])
                {
                    case "Rice Bowl": item = CreateObjects.RiceBowl();
                        break;
                    case "Biscuit": item = CreateObjects.Biscuit(status);
                        break;
                    case "Energy Drink": item = CreateObjects.EnergyDrink();
                        break;
                    case "Turkey": item = CreateObjects.Turkey(status);
                        break;
                    case "Bonus Token": item = CreateObjects.BonusToken();
                        break;
                    case "Replacement Tank": item = CreateObjects.OTReplace();
                        break;
                    case "Health Candy": item = CreateObjects.HealthCandy();
                        break;
                    case "Health Juice": item = CreateObjects.HealthJuice();
                        break;
                    case "Bear Claw": item = CreateObjects.BearClaw();
                        break;
                    case "Clam Shell": item = CreateObjects.ClamShell();
                        break;
                    case "Pile of Money": item = CreateObjects.PileOfMoney();
                        break;
                    case "Pocket Watch": item = CreateObjects.PocketWatch();
                        break;
                    case "Bag of Jewels": item = CreateObjects.BagofJewels();
                        break;
                    case "Graham Trophy": item = CreateObjects.GrahamTrophy();
                        break;
                    case "Graham Doll": item = CreateObjects.GrahamDoll();
                        break;
                    case "Mystery Pudding": item = new Item(LoadGraphics.ItemSprite, "Mystery Pudding", 0, 0, 1, false, 0, status, Vector2.Zero, Vector2.Zero);
                        break;
                }
            }

            return item;
        }

        public override BeatEmUpObj ParseObject()
        {
            Item item = ParseHeldItem(Params[1] + "," + Params[2]);

            if (item != null) item.SetLocation(EditorHelper.ParseVector3(Params[0]));
            return item;
        }

        //private static Type[] GetKnownTypes()
        //{
        //    return new Type[] {  };
        //}
    }
}
