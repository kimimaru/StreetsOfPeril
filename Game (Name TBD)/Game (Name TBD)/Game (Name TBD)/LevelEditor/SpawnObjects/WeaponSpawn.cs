﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A weapon spawn object
    [DataContract(Namespace = "")]
    public class WeaponSpawn : PickUpObjSpawn
    {
        //The weapon spawned
        [IgnoreDataMember]
        public Weapon WeaponSpawned;

        public WeaponSpawn()
        {
            WeaponSpawned = null;
        }

        public WeaponSpawn(Weapon weaponspawned, Spawn spawn) : base(weaponspawned, spawn)
        {
            WeaponSpawned = weaponspawned;
        }

        //There is no need for different weapon spawns since they're all the same type, so we'll bring up the menus by name
        public override ObjectMenu BringUpMenu()
        {
            if (WeaponSpawned.GetWeaponType == (int)Weapon.WeaponTypes.None)
            {
                return null;
            }
            else if (WeaponSpawned.GetWeaponType == (int)Weapon.WeaponTypes.Stab)
            {
                return new KnifeMenu(this, true);
            }
            else if (WeaponSpawned.GetWeaponType == (int)Weapon.WeaponTypes.Swing)
            {
                if (WeaponSpawned.Name == "Katana")
                {
                    return new KatanaMenu(this, true);
                }
                else return new SwiftBladeMenu(this, true);
            }
            else return null;
        }

        public override void SetObject()
        {
            base.SetObject();

            WeaponSpawned = (Weapon)ObjectSpawned;
        }

        //Returns a weapon that an object (WeaponWielder or ItemContainer) must hold based on a string
        public static Weapon ParseHeldWeapon(String weaponstring)
        {
            Weapon weapon = null;

            String[] Params = weaponstring.Split(',');

            //Make sure the weapon is in the right format
            if (Params.Length == 4)
            {
                Status status = Status.StatFull(Params[1] + "," + Params[2] + "," + Params[3]);

                switch (Params[0])
                {
                    case "Knife": weapon = CreateObjects.Knife(status);
                        break;
                    case "Katana": weapon = CreateObjects.Katana(status);
                        break;
                    case "Swift Blade": weapon = CreateObjects.SwiftBlade(status);
                        break;
                }
            }

            return weapon;
        }

        public override BeatEmUpObj ParseObject()
        {
            Weapon weapon = ParseHeldWeapon(Params[1] + "," + Params[2]);

            if (weapon != null) weapon.SetLocation(EditorHelper.ParseVector3(Params[0]));
            return weapon;
        }
    }
}
