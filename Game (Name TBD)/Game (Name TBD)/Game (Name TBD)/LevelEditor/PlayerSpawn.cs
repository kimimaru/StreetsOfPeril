﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A spawn point for the player; it determines where the player will spawn
    //These are already placed on the level and are simply moved; to the left shows the type of level entrance and to the right shows the type of level exit
    //There are currently no plans for being able to assign characters or individual level entrances/exits to each player
    //USER EDITED
    [DataContract(Namespace="")]
    public sealed class PlayerSpawn
    {
        //The location of the player spawn
        [DataMember]
        public Vector3 Location;

        //The graphic representing the player spawn
        [IgnoreDataMember]
        public Texture2D Graphic;

        //The tile the player spawn is on; used for representing height
        [IgnoreDataMember]
        public TileEngine.Tile PlayerSpawnTile;

        //Constructor; initialize values based off player number
        public PlayerSpawn(int playernum, TileEngine TileEngine)
        {
            Location = GetInitLoc(playernum);
            Graphic = LoadGraphics.PlayerNumSelection[playernum];

            PlayerSpawnTile = TileEngine.CurTile(SelectionRectangle(Vector2.Zero));
        }

        private Vector3 GetInitLoc(int playernum)
        {
            return new Vector3(20, 100 + (playernum * 40), 0);
        }

        //The selection rectangle for clicking on a player spawn that has already been placed
        public Rectangle SelectionRectangle(Vector2 LevelOffSet)
        {
            return (new Rectangle((int)(Location.X + LevelOffSet.X) - (Graphic.Width / 2), (int)(Location.Y + LevelOffSet.Y) - (Graphic.Height / 2), Graphic.Width, Graphic.Height));
        }

        //Draws a shadow underneath the player spawn if it's above the ground
        private void DrawShadow(SpriteBatch spriteBatch, Vector2 LevelOffSet)
        {
            if (Location.Z > PlayerSpawnTile.Z)
            {
                //Reduce the size of the shadow the higher you are in the air
                float ShadowScale = (float)(1f - ((Location.Z - PlayerSpawnTile.Z) / 100f));
                if (ShadowScale < .4f || (Location.Z - PlayerSpawnTile.Z) > 99) ShadowScale = .4f;

                spriteBatch.Draw(LoadGraphics.PlayerShadow, new Vector2(Location.X + LevelOffSet.X, Location.Y + (Graphic.Height / 2) + LevelOffSet.Y - PlayerSpawnTile.Z), null, Color.White, 0f, new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2), ShadowScale, SpriteEffects.None, 0.0019f);
            }
        }

        private SpriteEffects GetFlip(int entranceexit)
        {
            switch (entranceexit)
            {
                case 2: return SpriteEffects.FlipHorizontally;
                case 4: return SpriteEffects.FlipVertically;
                default: return SpriteEffects.None;
            }
        }

        //Get the graphic representing the level entrance or exit; a null graphic indicates that there is no entrance or exit
        private Texture2D GetGraphic(int entranceexit)
        {
            if (entranceexit == 0) return null;
            else return (entranceexit > 2) ? LoadAssets.IntroExitUp : LoadAssets.IntroExitRight;
        }

        //Draws the current level entrance and exit to the left and right of the PlayerSpawn, respectively
        private void DrawEntranceExitIndicator(SpriteBatch spriteBatch, SublevelProp level)
        {
            Texture2D entrancegraphic = GetGraphic(level.LevelEntrance);
            Texture2D exitgraphic = GetGraphic(level.LevelExit);

            //Entrance, then exit
            if (entrancegraphic != null)
                spriteBatch.Draw(entrancegraphic, new Vector2(Location.X - (entrancegraphic.Width + 4) - level.OffSet.X, Location.Y - level.OffSet.Y), null, Color.White, 0f, new Vector2(entrancegraphic.Width / 2, entrancegraphic.Height / 2), 1f, GetFlip(level.LevelEntrance), .9944f);

            if (exitgraphic != null)
                spriteBatch.Draw(exitgraphic, new Vector2(Location.X + (exitgraphic.Width + 4) - level.OffSet.X, Location.Y - level.OffSet.Y), null, Color.White, 0f, new Vector2(exitgraphic.Width / 2, exitgraphic.Height / 2), 1f, GetFlip(level.LevelExit), .9944f);
        }

        public void Draw(SpriteBatch spriteBatch, SublevelProp level)
        {
            if (Graphic != null)
            {
                spriteBatch.Draw(Graphic, new Vector2(Location.X - level.OffSet.X, Location.Y - level.OffSet.Y - Location.Z), null, Color.White, 0f, new Vector2(Graphic.Width / 2, Graphic.Height / 2), 1f, SpriteEffects.None, .9945f);
                DrawEntranceExitIndicator(spriteBatch, level);

                DrawShadow(spriteBatch, -level.OffSet);
            }
        }
    }
}
