﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A collection of objects that get spawned at a particular offset, stoppoint, difficulty, and minimum number of players
    //The objects must know what spawn they're in; keep in mind that deleting a spawn will delete all objects contained in the spawn
    //USER EDITED
    [DataContract(IsReference=true, Namespace="")]
    public sealed class Spawn
    {
        //The color of the Spawn when disabled
        public static readonly Color DisabledColor;

        //The objects in a spawn
        [DataMember]
        public List<EnemySpawn> SpawnEnemies;
        [DataMember]
        public List<ContainerSpawn> SpawnContainers;
        [DataMember]
        public List<HazardSpawn> SpawnHazards;
        [DataMember]
        public List<ItemSpawn> SpawnItems;
        [DataMember]
        public List<WeaponSpawn> SpawnWeapons;

        //Spawn properties
        [DataMember]
        public Vector3 SpawnLocation;
        [DataMember]
        public int SpawnNumber;
        [DataMember]
        public bool Disabled;

        [IgnoreDataMember]
        public TileEngine.Tile SpawnTile;

        //Tells if the objects in the spawn move with the spawn or not when it's moved; the alternative is just updating the spawn offsets of the objects
        //This is for convenience; having both options allows you to be very flexible when placing objects
        [DataMember]
        public bool ObjectsMove;

        //The offset, stoppoint, difficulty, and minimum number of players the Spawn spawns (all enemies, containers, etc. appear)
        [DataMember]
        public Vector2 OffSet;
        [DataMember]
        public int StopPoint;
        [DataMember]
        public int DifficultyLevel;
        [DataMember]
        public int MinPlayers;

        //The total number of each object the spawn contains; it's just a useful bit of information to have and isn't necessary
        /*NOTE: These may be changed to an array of 5 elements, with each element representing a different type of object; this will keep the information more organized
        and make it easier to gather the sum*/
        [DataMember]
        public int TotalEnemies;
        [DataMember]
        public int TotalContainers;
        [DataMember]
        public int TotalHazards;
        [DataMember]
        public int TotalItems;
        [DataMember]
        public int TotalWeapons;

        public Spawn()
        {
            SpawnEnemies = new List<EnemySpawn>();
            SpawnContainers = new List<ContainerSpawn>();
            SpawnHazards = new List<HazardSpawn>();
            SpawnItems = new List<ItemSpawn>();
            SpawnWeapons = new List<WeaponSpawn>();

            SpawnLocation = Vector3.Zero;
            SpawnNumber = 0;
            Disabled = false;
            
            ObjectsMove = true;

            OffSet = Vector2.Zero;
            StopPoint = -1;
            DifficultyLevel = 0;
            MinPlayers = 1;

            TotalEnemies = 0;
            TotalContainers = 0;
            TotalHazards = 0;
            TotalItems = 0;
            TotalWeapons = 0;
        }

        public Spawn(Vector3 spawnlocation, Vector2 offset, int spawnnumber, int stoppoint, int difficultylevel, int minplayers) : this()
        {
            SpawnLocation = spawnlocation;
            OffSet = offset;

            SpawnNumber = spawnnumber;
            StopPoint = stoppoint;
            DifficultyLevel = difficultylevel;
            MinPlayers = minplayers;
        }

        static Spawn()
        {
            DisabledColor = Color.White * .6f;
        }

        //Returns all the enemy objects in the Spawn
        public List<Enemy> GetAllEnemies
        {
            get
            {
                List<Enemy> Enemies = new List<Enemy>();

                for (int i = 0; i < SpawnEnemies.Count; i++)
                    Enemies.Add((Enemy)SpawnEnemies[i].GetObject);

                return Enemies;
            }
        }

        //Returns all the item container objects in the Spawn
        public List<ItemContainer> GetAllContainers
        {
            get
            {
                List<ItemContainer> Containers = new List<ItemContainer>();

                for (int i = 0; i < SpawnContainers.Count; i++)
                    Containers.Add((ItemContainer)SpawnContainers[i].GetObject);

                return Containers;
            }
        }

        //Returns all the hazard objects in the Spawn
        public List<Hazard> GetAllHazards
        {
            get
            {
                List<Hazard> Hazards = new List<Hazard>();

                for (int i = 0; i < SpawnHazards.Count; i++)
                    Hazards.Add((Hazard)SpawnHazards[i].GetObject);

                return Hazards;
            }
        }

        //Returns all the item objects in the Spawn
        public List<Item> GetAllItems
        {
            get
            {
                List<Item> Items = new List<Item>();

                for (int i = 0; i < SpawnItems.Count; i++)
                    Items.Add((Item)SpawnItems[i].GetObject);

                return Items;
            }
        }

        //Returns all the weapon objects in the Spawn
        public List<Weapon> GetAllWeapons
        {
            get
            {
                List<Weapon> Weapons = new List<Weapon>();

                for (int i = 0; i < SpawnWeapons.Count; i++)
                    Weapons.Add((Weapon)SpawnWeapons[i].GetObject);

                return Weapons;
            }
        }

        //The total number of objects the spawn has
        public int TotalObjects
        {
            get { return (TotalEnemies + TotalContainers + TotalHazards + TotalItems + TotalWeapons); }
        }

        //The actual comparison location for spawning the Spawn
        public Vector2 TrueLocation
        {
            get { return (Game__Name_TBD_.StopPoint.GetScreenCenter + OffSet); }
        }

        //The selection rectangle for clicking on a spawn that has already been placed
        public Rectangle SelectionRectangle(Vector2 LevelOffSet)
        {
            return (new Rectangle((int)(SpawnLocation.X + LevelOffSet.X) - (LoadAssets.SpawnIcon.Width / 2), (int)(SpawnLocation.Y + LevelOffSet.Y) - (LoadAssets.SpawnIcon.Height / 2), LoadAssets.SpawnIcon.Width, LoadAssets.SpawnIcon.Height));
        }

        //Checks if a spawn object is loadable (valid)
        private bool SpawnObjectLoadable<T>(List<T> spawnlist, TileEngine TileEngine) where T : SpawnObject
        {
            for (int i = 0; i < spawnlist.Count; i++)
            {
                if (spawnlist[i].Loadable(TileEngine) == false) return false;
            }

            return true;
        }

        //Checks if the spawn is in the level and can be loaded
        private bool InBounds(Rectangle objectbounds, TileEngine TileEngine)
        {
            return (objectbounds.Left > TileEngine[0, 0].Left && objectbounds.Right < TileEngine[TileEngine.NumTilesX - 1, 0].Right && objectbounds.Top > TileEngine[0, 0].Top && objectbounds.Bottom < TileEngine[0, TileEngine.NumTilesY - 1].Bottom);
        }

        //Checks if the spawn is loadable (valid) by checking if all of its objects are loadable
        public bool Loadable(TileEngine TileEngine)
        {
            return (InBounds(SelectionRectangle(Vector2.Zero), TileEngine) == true && SpawnObjectLoadable(SpawnEnemies, TileEngine) == true && SpawnObjectLoadable(SpawnContainers, TileEngine) == true && SpawnObjectLoadable(SpawnHazards, TileEngine) == true && SpawnObjectLoadable(SpawnItems, TileEngine) == true && SpawnObjectLoadable(SpawnWeapons, TileEngine) == true);
        }

        private void LoadSpawnObjList<T>(List<T> SpawnObjects) where T : SpawnObject
        {
            for (int i = 0; i < SpawnObjects.Count; i++)
            {
                //Look for errors
                try
                {
                    SpawnObjects[i].SetObject();
                }
                //If we encounter any error at all with loading the object, remove it from the list and continue
                catch (Exception e)
                {
                    //Debug message
                    Debug.OutputValue("Error when loading object: " + typeof(T).Name + " at index: " + i + " with error: " + e.Message);

                    SpawnObjects.RemoveAt(i);
                    i--;
                }
            }
        }

        //Load all objects in the spawn by creating them based on their stored values
        public void LoadObjects()
        {
            LoadSpawnObjList<EnemySpawn>(SpawnEnemies);
            LoadSpawnObjList<ContainerSpawn>(SpawnContainers);
            LoadSpawnObjList<HazardSpawn>(SpawnHazards);
            LoadSpawnObjList<ItemSpawn>(SpawnItems);
            LoadSpawnObjList<WeaponSpawn>(SpawnWeapons);
        }

        private void UpdateObjectOffSet<T>(List<T> SpawnObjects, Vector3 difference) where T : SpawnObject
        {
            for (int i = 0; i < SpawnObjects.Count; i++)
                SpawnObjects[i].SpawnOffSet += difference;
        }

        //Changes all objects' spawn offsets with the Spawn it moves (this is an option; the other is that they literally move with the Spawn)
        public void UpdateObjectOffSets(Vector3 difference)
        {
            UpdateObjectOffSet(SpawnEnemies, difference);
            UpdateObjectOffSet(SpawnContainers, difference);
            UpdateObjectOffSet(SpawnHazards, difference);
            UpdateObjectOffSet(SpawnItems, difference);
            UpdateObjectOffSet(SpawnWeapons, difference);
        }

        //Moves all enemies with the spawn
        private void MoveEnemies(Vector3 difference)
        {
            for (int i = 0; i < SpawnEnemies.Count; i++)
            {
                SpawnEnemies[i].EnemySpawned.SetLocation(new Vector3(SpawnEnemies[i].EnemySpawned.GetLocation.X + difference.X, SpawnEnemies[i].EnemySpawned.GetLocation.Y + difference.Y, SpawnEnemies[i].EnemySpawned.CurHeight + difference.Z));

                Vector4 enemyloc = SpawnEnemies[i].EnemySpawned.GetLocationHeight;

                SpawnEnemies[i].Params[0] = EditorHelper.ConvertVector3String(new Vector3(enemyloc.X, enemyloc.Y, enemyloc.Z));
            }
        }

        //Moves all containers with the spawn
        private void MoveContainers(Vector3 difference)
        {
            for (int i = 0; i < SpawnContainers.Count; i++)
            {
                SpawnContainers[i].ContainerSpawned.PureMove(difference.X, difference.Y, difference.Z);

                Vector4 containerloc = SpawnContainers[i].ContainerSpawned.GetLocationHeight;

                SpawnContainers[i].Params[0] = EditorHelper.ConvertVector3String(new Vector3(containerloc.X, containerloc.Y, containerloc.Z));
            }
        }

        //Moves all hazards with the spawn
        private void MoveHazards(Vector3 difference)
        {
            for (int i = 0; i < SpawnHazards.Count; i++)
            {
                SpawnHazards[i].HazardSpawned.SetLocation(new Vector3(SpawnHazards[i].HazardSpawned.GetLocationHeight.X + difference.X, SpawnHazards[i].HazardSpawned.GetLocationHeight.Y + difference.Y, SpawnHazards[i].HazardSpawned.GetLocationHeight.Z + difference.Z));

                Vector4 hazardloc = SpawnHazards[i].HazardSpawned.GetLocationHeight;

                SpawnHazards[i].Params[0] = EditorHelper.ConvertVector3String(new Vector3(hazardloc.X, hazardloc.Y, hazardloc.Z));
            }
        }

        //Moves all items with the spawn
        private void MoveItems(Vector3 difference)
        {
            for (int i = 0; i < SpawnItems.Count; i++)
            {
                SpawnItems[i].ItemSpawned.SetLocation(new Vector3(SpawnItems[i].ItemSpawned.GetLocationHeight.X + difference.X, SpawnItems[i].ItemSpawned.GetLocationHeight.Y + difference.Y, SpawnItems[i].ItemSpawned.CurHeight + difference.Z));

                Vector4 itemloc = SpawnItems[i].ItemSpawned.GetLocationHeight;

                SpawnItems[i].Params[0] = EditorHelper.ConvertVector3String(new Vector3(itemloc.X, itemloc.Y, itemloc.Z));
            }
        }

        //Moves all weapons with the spawn
        private void MoveWeapons(Vector3 difference)
        {
            for (int i = 0; i < SpawnWeapons.Count; i++)
            {
                SpawnWeapons[i].WeaponSpawned.SetLocation(new Vector3(SpawnWeapons[i].WeaponSpawned.GetLocationHeight.X + difference.X, SpawnWeapons[i].WeaponSpawned.GetLocationHeight.Y + difference.Y, SpawnWeapons[i].WeaponSpawned.CurHeight + difference.Z));

                Vector4 weaponloc = SpawnWeapons[i].WeaponSpawned.GetLocationHeight;

                SpawnWeapons[i].Params[0] = EditorHelper.ConvertVector3String(new Vector3(weaponloc.X, weaponloc.Y, weaponloc.Z));
            }
        }

        //Generic method for moving an object list with the spawn
        private void MoveObjList<T>(List<T> ObjList, Vector3 difference) where T : SpawnObject
        {
            for (int i = 0; i < ObjList.Count; i++)
            {
                ObjList[i].ObjectSpawned.MoveLocation(difference);
                ObjList[i].Params[0] = EditorHelper.ConvertVector3String(ObjList[i].ObjectSpawned.GetLocation);
            }
        }

        //Moves all objects with the spawn (only occurs if all objects are set to move)
        public void MoveObjects(Vector3 difference)
        {
            MoveEnemies(difference);
            MoveContainers(difference);
            MoveHazards(difference);
            MoveItems(difference);
            MoveWeapons(difference);
        }

        //Sorts the indices for a specific SpawnObject type in the spawn's list
        public void SortIndexList<T>(List<T> spawnobjectlist) where T : SpawnObject
        {
            for (int i = 0; i < spawnobjectlist.Count; i++)
                spawnobjectlist[i].Index = i;
        }

        //Adds a spawn object to the spawn
        public void AddEnemy(EnemySpawn SpawnEnemy)
        {
            SpawnEnemies.Insert(SpawnEnemy.Index, SpawnEnemy);
            SortIndexList(SpawnEnemies);

            TotalEnemies++;
        }

        public void RemoveEnemy(int index)
        {
            if (EditorHelper.IndexWithinArray<EnemySpawn>(SpawnEnemies, index) == true)
            {
                SpawnEnemies.RemoveAt(index);
                SortIndexList(SpawnEnemies);

                TotalEnemies--;
            }
        }

        public void RemoveEnemy(EnemySpawn SpawnEnemy)
        {
            if (SpawnEnemies.Remove(SpawnEnemy) == true)
            {
                SortIndexList(SpawnEnemies);
                TotalEnemies--;
            }
        }

        public void AddContainer(ContainerSpawn SpawnContainer)
        {
            SpawnContainers.Insert(SpawnContainer.Index, SpawnContainer);
            SortIndexList(SpawnContainers);

            TotalContainers++;
        }

        public void RemoveContainer(int index)
        {
            if (EditorHelper.IndexWithinArray<ContainerSpawn>(SpawnContainers, index) == true)
            {
                SpawnContainers.RemoveAt(index);
                SortIndexList(SpawnContainers);

                TotalContainers--;
            }
        }

        public void RemoveContainer(ContainerSpawn SpawnContainer)
        {
            if (SpawnContainers.Remove(SpawnContainer) == true)
            {
                SortIndexList(SpawnContainers);
                TotalContainers--;
            }
        }

        public void AddHazard(HazardSpawn SpawnHazard)
        {
            SpawnHazards.Insert(SpawnHazard.Index, SpawnHazard);
            SortIndexList(SpawnHazards);

            TotalHazards++;
        }

        public void RemoveHazard(int index)
        {
            if (EditorHelper.IndexWithinArray<HazardSpawn>(SpawnHazards, index) == true)
            {
                SpawnHazards.RemoveAt(index);
                SortIndexList(SpawnHazards);

                TotalHazards--;
            }
        }

        public void RemoveHazard(HazardSpawn SpawnHazard)
        {
            if (SpawnHazards.Remove(SpawnHazard) == true)
            {
                SortIndexList(SpawnHazards);
                TotalHazards--;
            }
        }

        public void AddItem(ItemSpawn SpawnItem)
        {
            SpawnItems.Insert(SpawnItem.Index, SpawnItem);
            SortIndexList(SpawnItems);

            TotalItems++;
        }

        public void RemoveItem(int index)
        {
            if (EditorHelper.IndexWithinArray<ItemSpawn>(SpawnItems, index) == true)
            {
                SpawnItems.RemoveAt(index);
                SortIndexList(SpawnItems);

                TotalItems--;
            }
        }

        public void RemoveItem(ItemSpawn SpawnItem)
        {
            if (SpawnItems.Remove(SpawnItem) == true)
            {
                SortIndexList(SpawnItems);
                TotalItems--;
            }
        }

        public void AddWeapon(WeaponSpawn SpawnWeapon)
        {
            SpawnWeapons.Insert(SpawnWeapon.Index, SpawnWeapon);
            SortIndexList(SpawnWeapons);

            TotalWeapons++;
        }

        public void RemoveWeapon(int index)
        {
            if (index >= 0 && index < SpawnWeapons.Count)
            {
                SpawnWeapons.RemoveAt(index);
                SortIndexList(SpawnWeapons);

                TotalWeapons--;
            }
        }

        public void RemoveWeapon(WeaponSpawn SpawnWeapon)
        {
            if (SpawnWeapons.Remove(SpawnWeapon) == true)
            {
                SortIndexList(SpawnWeapons);
                TotalWeapons--;
            }
        }

        //Generic method for updating an object list
        private void UpdateObjList<T>(SublevelProp levelprop, List<T> ObjList) where T : SpawnObject
        {
            for (int i = 0; i < ObjList.Count; i++)
                ObjList[i].ObjectSpawned.LevelEditorUpdate(levelprop);
        }

        //Updates all the objects in the spawn
        private void UpdateObjects(SublevelProp levelprop)
        {
            UpdateObjList<EnemySpawn>(levelprop, SpawnEnemies);
            UpdateObjList<ContainerSpawn>(levelprop, SpawnContainers);
            UpdateObjList<HazardSpawn>(levelprop, SpawnHazards);
            UpdateObjList<ItemSpawn>(levelprop, SpawnItems);
            UpdateObjList<WeaponSpawn>(levelprop, SpawnWeapons);
        }

        public void Update(SublevelProp levelprop)
        {
            //Update all objects
            UpdateObjects(levelprop);
        }

        //Generic method for drawing an object list
        private void DrawObjList<T>(SpriteBatch spriteBatch, List<T> ObjList, Vector2 LevelOffSet, TileEngine TileEngine) where T : SpawnObject
        {
            for (int i = 0; i < ObjList.Count; i++)
                ObjList[i].Draw(spriteBatch, LevelOffSet, TileEngine);
        }

        //Draws all the objects in the spawn
        private void DrawObjects(SpriteBatch spriteBatch, Vector2 LevelOffSet, TileEngine TileEngine)
        {
            DrawObjList<EnemySpawn>(spriteBatch, SpawnEnemies, LevelOffSet, TileEngine);
            DrawObjList<ContainerSpawn>(spriteBatch, SpawnContainers, LevelOffSet, TileEngine);
            DrawObjList<HazardSpawn>(spriteBatch, SpawnHazards, LevelOffSet, TileEngine);
            DrawObjList<ItemSpawn>(spriteBatch, SpawnItems, LevelOffSet, TileEngine);
            DrawObjList<WeaponSpawn>(spriteBatch, SpawnWeapons, LevelOffSet, TileEngine);
        }

        //Draws a shadow underneath the player spawn if it's above the ground
        private void DrawShadow(SpriteBatch spriteBatch, Vector2 LevelOffSet)
        {
            if (SpawnLocation.Z > SpawnTile.Z)
            {
                //Reduce the size of the shadow the higher you are in the air
                float ShadowScale = (float)(1f - ((SpawnLocation.Z - SpawnTile.Z) / 100f));
                if (ShadowScale < .4f || (SpawnLocation.Z - SpawnTile.Z) > 99) ShadowScale = .4f;

                spriteBatch.Draw(LoadGraphics.PlayerShadow, new Vector2(SpawnLocation.X + LevelOffSet.X, SpawnLocation.Y + (LoadAssets.SpawnIcon.Height / 2) + LevelOffSet.Y - SpawnTile.Z), null, Disabled == false ? Color.White : DisabledColor, 0f, new Vector2(LoadGraphics.PlayerShadow.Width / 2, LoadGraphics.PlayerShadow.Height / 2), ShadowScale, SpriteEffects.None, 0.0019f);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 LevelOffSet, TileEngine TileEngine)
        {
            //Draw the location of the spawn
            spriteBatch.Draw(LoadAssets.SpawnIcon, new Vector2(SpawnLocation.X, SpawnLocation.Y - SpawnLocation.Z) + LevelOffSet, null, Disabled == false ? Color.White : DisabledColor, 0f, new Vector2(LoadAssets.SpawnIcon.Width / 2, LoadAssets.SpawnIcon.Height / 2), 1f, SpriteEffects.None, .995f);

            //Don't draw a Spawn's objects when disabled; this is for now since we'll try and draw the individual objects slightly transparent like the Spawn itself
            if (Disabled == false)
                DrawObjects(spriteBatch, LevelOffSet, TileEngine);

            DrawShadow(spriteBatch, LevelOffSet);
        }
    }
}
