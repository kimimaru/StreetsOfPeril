﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Game__Name_TBD_
{
    //A class for loading in all the assets required for the Level Editor
    public static class LoadAssets
    {
        //Graphics
        public static Texture2D BG;
        public static Texture2D BGGraphic;
        public static Texture2D ButtonDefault;
        public static Texture2D StopPointIcon;
        public static Texture2D SpawnIcon;
        public static Texture2D TileIcon;

        public static Texture2D IntroExitUp;
        public static Texture2D IntroExitRight;

        //Object settings
        public static Texture2D RemoveItemIcon;
        public static Texture2D RemoveWeaponIcon;

        public static Texture2D TempMinecartRiderIcon;

        public static Texture2D MinusIcon;
        public static Texture2D PlusIcon;

        //Fonts
        public static SpriteFont EditorFont;

        //Load all the assets
        public static void LoadContent(ContentManager Content)
        {
            LoadGraphics(Content);
            LoadFonts(Content);
        }

        //Load all the graphics
        public static void LoadGraphics(ContentManager Content)
        {
            BG = Content.Load<Texture2D>("BG2");
            BGGraphic = Content.Load<Texture2D>("BG Graphic");
            ButtonDefault = Content.Load<Texture2D>("ButtonDefault");
            StopPointIcon = Content.Load<Texture2D>("StopPoint Icon");
            SpawnIcon = Content.Load<Texture2D>("Spawn Icon");
            TileIcon = Content.Load<Texture2D>("Tile Icon");

            IntroExitUp = Content.Load<Texture2D>("IntroExit - Up");
            IntroExitRight = Content.Load<Texture2D>("IntroExit - Right");

            //Object settings
            RemoveItemIcon = Content.Load<Texture2D>("Remove Item Icon");
            RemoveWeaponIcon = Content.Load<Texture2D>("Remove Weapon Icon");

            TempMinecartRiderIcon = Content.Load<Texture2D>("TempMinecartRiderIcon");

            MinusIcon = Content.Load<Texture2D>("Minus Icon");
            PlusIcon = Content.Load<Texture2D>("Plus Icon");
        }

        //Load all the fonts
        public static void LoadFonts(ContentManager Content)
        {
            EditorFont = Content.Load<SpriteFont>("EditorFont");
        }
    }
}
