﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //Helper functions for the Level Editor
    public static class EditorHelper
    {
        //Draws an outline around a rectangle
        public static void DrawOutlineRect(SpriteBatch spriteBatch, Rectangle rectangle, Vector2 boundoffset)
        {
            //Draw an outline by drawing lines around the rectangle
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(rectangle.X + (int)boundoffset.X - 1, rectangle.Y + (int)boundoffset.Y - 1), null, Color.Black, 0f, Vector2.Zero, new Vector2(rectangle.Width + 1f, 1f), SpriteEffects.None, Button.ButtonDepth - .0001f);
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(rectangle.Right + (int)boundoffset.X, rectangle.Y + (int)boundoffset.Y - 1), null, Color.Black, 0f, Vector2.Zero, new Vector2(1f, rectangle.Height + 2f), SpriteEffects.None, Button.ButtonDepth - .0001f);
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(rectangle.X + (int)boundoffset.X - 1, rectangle.Bottom + (int)boundoffset.Y), null, Color.Black, 0f, Vector2.Zero, new Vector2(rectangle.Width + 1f, 1f), SpriteEffects.None, Button.ButtonDepth - .0001f);
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(rectangle.X + (int)boundoffset.X - 1, rectangle.Y + (int)boundoffset.Y - 1), null, Color.Black, 0f, Vector2.Zero, new Vector2(1f, rectangle.Height + 1f), SpriteEffects.None, Button.ButtonDepth - .0001f);
        }

        //Adds a range to a list with the convenience of the params parameter
        public static void AddToList<T>(List<T> list, params T[] items)
        {
            if (list != null)
                list.AddRange(items);
        }

        //Checks if a list-type object with direct indexing is null or empty
        public static bool ArrayEmpty<T>(IList<T> array)
        {
            return (array == null || array.Count == 0);
        }

        //Checks if an index doesn't go over the count of a list-type object with direct indexing
        public static bool IndexWithinArray<T>(IList<T> array, int index)
        {
            return (ArrayEmpty<T>(array) == false && index < array.Count);
        }

        //Tells if a key pressed is a letter
        public static bool IsLetter(Keys key)
        {
            return ((key >= Keys.A && key <= Keys.Z) || key == Keys.OemComma);
        }

        //Tells if a key pressed is a number
        public static bool IsNumber(Keys key)
        {
            return (key >= Keys.D0 && key <= Keys.D9 || key == Keys.OemMinus);
        }

        //Tells if a string is numeric
        public static bool IsNumeric(String text)
        {
            int newint;

            return (int.TryParse(text, out newint));
        }

        //Converts a string to a number
        public static int TextToNumber(String text)
        {
            int newint;

            int.TryParse(text, out newint);

            return newint;
        }

        //Checks for keys pressed that do not directly convert properly into ASCII characters and returns their character equivalents
        public static char ConvertInput(Keys key)
        {
            switch (key)
            {
                case Keys.OemComma: return ',';
                case Keys.OemTilde: return '~';
                case Keys.OemPeriod: return '.';
                case Keys.OemMinus: return '-';
                case Keys.OemPlus: return '+';
                case Keys.OemOpenBrackets: return '[';
                case Keys.OemCloseBrackets: return ']';
                case Keys.OemPipe: return '|';
                case Keys.OemQuestion: return '?';
                default: return (char)key;
            }
        }

        //Input methods
        public static Vector2 GetMousePosition()
        {
            return (new Vector2(Mouse.GetState().X, Mouse.GetState().Y));
        }

        public static Rectangle GetMouseRect()
        {
            return (new Rectangle((int)Mouse.GetState().X, (int)Mouse.GetState().Y, 1, 1));
        }

        public static bool MouseLeftHeld()
        {
            return (Mouse.GetState().LeftButton == ButtonState.Pressed);
        }

        public static bool ClickedRect(Rectangle rect)
        {
            return (GetMouseRect().Intersects(rect));
        }

        public static bool LeftClicked(MouseState mouse)
        {
            return (Mouse.GetState().LeftButton == ButtonState.Pressed && mouse.LeftButton == ButtonState.Released);
        }

        public static bool RightClicked(MouseState mouse)
        {
            return (Mouse.GetState().RightButton == ButtonState.Pressed && mouse.RightButton == ButtonState.Released);
        }

        public static bool DoubleClicked(MouseState mouse, float doubleclicktimer, float totaldoubleclicktime)
        {
            float doubleclicktime = Main.GetActiveTime - doubleclicktimer;

            return (Mouse.GetState().LeftButton == ButtonState.Pressed && mouse.LeftButton == ButtonState.Released && doubleclicktimer >= totaldoubleclicktime);
        }

        //Gets the location of the mouse position in the level
        public static Vector2 GetMousePositionLevel(SublevelProp level)
        {
            return (GetMousePosition() + level.OffSet);
        }

        //Convert methods
        //Converts a Vector2 into a string
        public static String ConvertVector2String(Vector2 vector2)
        {
            return ((int)vector2.X + "," + (int)vector2.Y);
        }

        //Converts a Vector3 into a string
        public static String ConvertVector3String(Vector3 vector3)
        {
            return ((int)vector3.X + "," + (int)vector3.Y + "," + (int)vector3.Z);
        }

        //Parse methods
        //Parses a string into a Vector2
        public static Vector2 ParseVector2(String vector2)
        {
            String[] vector = vector2.Split(',');

            Vector2 returnvector = Vector2.Zero;

            //Parse the X value
            if (vector.Length > 0)
            {
                float.TryParse(vector[0], out returnvector.X);

                //Parse the Y value
                if (vector.Length > 1)
                {
                    float.TryParse(vector[1], out returnvector.Y);
                }
            }

            return returnvector;
        }

        //Parses a string into a Vector3
        public static Vector3 ParseVector3(String vector3)
        {
            String[] vector = vector3.Split(',');

            Vector3 returnvector = Vector3.Zero;

            //Parse the X value
            if (vector.Length > 0)
            {
                float.TryParse(vector[0], out returnvector.X);

                //Parse the Y value
                if (vector.Length > 1)
                {
                    float.TryParse(vector[1], out returnvector.Y);

                    //Parse the Z value
                    if (vector.Length > 2)
                    {
                        float.TryParse(vector[2], out returnvector.Z);
                    }
                }
            }

            return returnvector;
        }
    }
}
