﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //An object that spawns in a spawn; it shares most of the spawn's traits, including offset, stop point, etc.
    //When created, it is spawned in the spawn's location; you can then offset it by modifying the offset text box in the accompanying GeneralMenu
    //USER EDITED
    [DataContract(Namespace = "")]
    public abstract class SpawnObject
    {
        //The object spawned
        [IgnoreDataMember]
        public BeatEmUpObj ObjectSpawned;

        //The spawn the object is associated with
        [DataMember]
        public Spawn SpawnAssociated;

        //The index of the object in its list
        [DataMember]
        public int Index;

        //Whether the object needs to be destroyed for the camera to move on or not
        [DataMember]
        public bool Designated;

        //The amount the object's location is offset from the spawn's location
        [DataMember]
        public Vector3 SpawnOffSet;

        //The status the object selected; this is needed because the status is usually parsed directly from the string, so we need to store the value to go back to
        //Alternatively, we could call ToString() on the Status and check if the string matches any of the selections and sets it to that index; we'll see how this
        //goes first, though that method will be universal and less cumbersome to check for in all honesty (at the cost of some speed, but that's negligible)
        [DataMember]
        public int StatusChosen;

        //The name of the type of the object
        [DataMember]
        public String ObjectType;

        //The object's constructor parameters
        [DataMember]
        public String[] Params;

        protected SpawnObject()
        {
            ObjectSpawned = null;
            SpawnAssociated = null;
            Index = -1;
            Designated = false;
            SpawnOffSet = Vector3.Zero;
            StatusChosen = 0;

            ObjectType = this.GetType().Name;
        }

        protected SpawnObject(BeatEmUpObj objectspawned, Spawn spawnassociated) : this()
        {
            ObjectSpawned = objectspawned;
            SpawnAssociated = spawnassociated;
        }

        //Gets the objects location in the level
        public Vector2 GetLevelLocation
        {
            get { return new Vector2(SpawnAssociated.SpawnLocation.X + SpawnOffSet.X, SpawnAssociated.SpawnLocation.Y + SpawnOffSet.Y); }
        }

        //The actual location of the object, relative to its spawn
        public Vector3 GetActualLocation(int height)
        {
            Vector3 actualloc = SpawnAssociated.SpawnLocation + SpawnOffSet;
            actualloc.Z = height;

            return actualloc;
        }

        //Gets the object that the spawn object is holding
        public virtual BeatEmUpObj GetObject
        {
            get { return ObjectSpawned; }
        }

        //The selection rectangle for clicking on a SpawnObject that has already been placed
        public virtual Rectangle SelectionRectangle(Vector2 LevelOffSet)
        {
            Rectangle selectionrect = ObjectSpawned.CollisionBox;

            selectionrect.X += (int)LevelOffSet.X;
            selectionrect.Y += (int)LevelOffSet.Y;

            return selectionrect;
        }

        //Checks if the object is okay and can be loaded
        //The spawn associated with this object must not be null, and the parameters of the object must work out
        public virtual bool Loadable(TileEngine TileEngine)
        {
            return (SpawnAssociated != null && Params != null && Params.Length > 0 && ObjectSpawned != null && InBounds(ObjectSpawned.FeetLoc, TileEngine) == true);
        }

        //Checks if the object is in the level and can be loaded
        protected bool InBounds(Rectangle objectbounds, TileEngine TileEngine)
        {
            return (objectbounds.Left > TileEngine[0, 0].Left && objectbounds.Right < TileEngine[TileEngine.NumTilesX - 1, 0].Right && objectbounds.Top > TileEngine[0, 0].Top && objectbounds.Bottom < TileEngine[0, TileEngine.NumTilesY - 1].Bottom);
        }

        //Sets the SpawnObject's object parameters
        public void SetParams(params String[] parameters)
        {
            Params = parameters;
        }

        //Brings up the proper object menu for the spawn object
        //This is called when you click on an object, so EditingObject is always true for the respective menu
        public abstract ObjectMenu BringUpMenu();

        //Sets the current object to the object created by its ParseObject() method
        public virtual void SetObject()
        {
            ObjectSpawned = ParseObject();
            ObjectSpawned.SetDesignated(Designated);
        }

        //Creates the proper object based on the parameters it contains; this will return an object
        public abstract BeatEmUpObj ParseObject();

        //Draws the object
        //This is abstract so the object can actually be drawn without having to know its type; this makes sense since a SpawnObject reference is held, not any of the derived types
        public virtual void Draw(SpriteBatch spriteBatch, Vector2 OffSet, TileEngine TileEngine)
        {
            if (ObjectSpawned != null)
                ObjectSpawned.Draw(spriteBatch, OffSet, TileEngine);
        }
    }
}
