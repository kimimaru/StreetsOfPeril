﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The editing menu for stoppoints; it's in the same location as the object editing menu
    public sealed class StopPointMenu : ObjectMenu
    {
        //The stoppoint
        public StopPoint StopPointSelected;

        //Constructor
        public StopPointMenu(StopPoint stoppoint, bool isediting)
        {
            MenuLocation = ObjectMenuLocation;
            EditingObject = isediting;

            DragGraphic = LoadAssets.StopPointIcon;
            GraphicOrigin = new Vector2(LoadAssets.StopPointIcon.Width / 2, LoadAssets.StopPointIcon.Height / 2);

            StopPointSelected = stoppoint;

            //At the moment you can't change the stop number for the stop point since it's just for reference; it may be possible to modify in the future
            Controls.Add(new Label(SetLocation(0, -10), "Number: " + StopPointSelected.StopNumber, Color.Black));

            //Offset X
            Controls.Add(new TextBox(SetSize(0, 0, 50, 20), null, new Vector2(62, 2), "Offset X: " + StopPointSelected.StopOffSet.X, 10, 5));

            //Offset Y
            Controls.Add(new TextBox(SetSize(0, 24, 50, 20), null, new Vector2(62, 2), "Offset Y: " + StopPointSelected.StopOffSet.Y, 10, 5));

            //Enabled
            Controls.Add(new DropDown(SetSize(0, 48, 37, 15), "Disabled:", Convert.ToInt32(StopPointSelected.Disabled), new Vector2(75, 2), "False", "True"));

            /*We can add minimum number of players and etc. for stop points to allow for more flexibility and diversity at different difficulties, but I don't feel it's 
              necessary and the levels should stay the same across difficulties; save it for the sequel if it ever happens*/
        }

        public override void Parse(SublevelProp level)
        {
            //If the stoppoint is a new one, set its stop number
            if (EditingObject == false) StopPointSelected.StopNumber = level.StopPoints.Count;

            Vector2 stopoffset = new Vector2(EditorHelper.TextToNumber((String)Controls[1].GetControlValue), EditorHelper.TextToNumber((String)Controls[2].GetControlValue));
            
            //The extra character of input is just for negative numbers; if the X or Y exceeds 9999, set the value back to 9999
            if (stopoffset.X > 9999) stopoffset.X = 9999;
            if (stopoffset.Y > 9999) stopoffset.Y = 9999;

            StopPointSelected.StopOffSet = stopoffset;
            StopPointSelected.Disabled = Convert.ToBoolean(Controls[Controls.Count - 1].GetControlValue);
        }

        //Adds the stoppoint to the level
        public override void AddToLevel(SublevelProp level)
        {
            level.AddStopPoint(StopPointSelected);
        }
    }
}
