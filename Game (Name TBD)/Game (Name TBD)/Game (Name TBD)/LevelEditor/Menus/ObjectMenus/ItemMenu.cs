﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The base class for item menus
    public abstract class ItemMenu : ObjectMenu
    {
        public ItemSpawn ItemSelected;

        public ItemMenu()
        {
            MenuLocation = ObjectMenuLocation;
        }

        //Creates base controls for the items; the initialstatuschoice parameter is there for items that don't have None as a possible Status
        protected void CreateBaseControls(Status[] possiblestatuses, String initialstatuschoice = "None,0,0")
        {
            int height = EditingObject == false ? (int)ItemSelected.SpawnAssociated.SpawnLocation.Z : (int)ItemSelected.ItemSpawned.CurHeight;

            //Item Name
            Controls.Add(new Label(SetLocation(0, 10), ItemSelected.ItemSpawned.Name, Color.Black));

            //Height
            Controls.Add(new TextBox(SetSize(0, 22, 35, 15), true, new Vector2(55, -1), "Height: " + height, 8, 3));

            //Statuses
            CreateStatusDropDown(ItemSelected.StatusChosen, possiblestatuses, 38, initialstatuschoice);

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);

            GeneralSpawnMenu = new GeneralMenu(ItemSelected);

            MenuLocation.Y += 54;
        }

        public override void ChangeObjectIndex()
        {
            ItemSelected.SpawnAssociated.RemoveItem(ItemSelected);
            ItemSelected.SpawnAssociated.AddItem(ItemSelected);
        }

        protected Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[1].GetControlValue)));
        }

        protected Vector3 GetLocationHeightText()
        {
            return (new Vector3(GeneralSpawnMenu.GetSpawnOffSet(), EditorHelper.TextToNumber((String)Controls[1].GetControlValue) - ItemSelected.SpawnAssociated.SpawnLocation.Z));
        }

        //The actual location of the item, relative to its spawn
        protected Vector3 GetActualLocation()
        {
            return (ItemSelected.GetActualLocation(EditorHelper.TextToNumber((String)Controls[1].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            GeneralSpawnMenu.Parse(level);

            Vector3 spawnoffset = GetLocationHeight(level);

            ItemSelected.StatusChosen = (int)Controls[2].GetControlValue;
            if (GeneralSpawnMenu.IsSpawnOffSetText() == false) ItemSelected.SpawnOffSet = spawnoffset - ItemSelected.SpawnAssociated.SpawnLocation;
            else ItemSelected.SpawnOffSet = GetLocationHeightText();

            ValidateIndex(ItemSelected.SpawnAssociated.SpawnItems);

            ItemSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                   Controls[0].GetControlText,
                                   Controls[2].GetControlText);

            ItemSelected.SetObject();

            if (IsEditingObject == true)
                CheckGiveContainer(level);
        }

        //Checks if the item should be given to an Item Container and gives it to it if so
        private bool CheckGiveContainer(SublevelProp level)
        {
            for (int i = 0; i < ItemSelected.SpawnAssociated.SpawnContainers.Count; i++)
            {
                //Check if the container doesn't have an item and check if the item's click box intersects the container's click box
                if (ItemSelected.SpawnAssociated.SpawnContainers[i].ContainerSpawned.GetHeldItem == null && EditorHelper.ClickedRect(ItemSelected.SpawnAssociated.SpawnContainers[i].SelectionRectangle(-level.OffSet)) == true)
                {
                    ItemSelected.SpawnAssociated.SpawnContainers[i].AddItem(ItemSelected);
                    return true;
                }
            }

            return false;
        }

        public override void AddToLevel(SublevelProp level)
        {
            //Check if the item is going to be placed on an Item Container so it can be given to it
            //NOTE: Move this; it should work if the item is already in the level and was simply moved
            if (CheckGiveContainer(level) == false)
            {
                //The item was not given to an Item Container, so it'll spawn on its own in the level
                ItemSelected.SpawnAssociated.AddItem(ItemSelected);
            }
        }
    }
}
