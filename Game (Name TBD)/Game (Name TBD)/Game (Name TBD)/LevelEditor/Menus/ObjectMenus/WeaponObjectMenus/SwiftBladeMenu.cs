﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class SwiftBladeMenu : WeaponMenu
    {
        public SwiftBladeMenu(WeaponSpawn weaponselected, bool isediting)
        {
            EditingObject = isediting;
            DragGraphic = LoadGraphics.WeaponSprite[weaponselected.WeaponSpawned.GetWeaponType];

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);

            WeaponSelected = weaponselected;

            GeneralSpawnMenu = new GeneralMenu(WeaponSelected);

            CreateBaseControls(Weapon.SwiftBladeStatuses);
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            WeaponSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     "Swift Blade",
                                     Controls[2].GetControlText);

            WeaponSelected.SetObject();
        }
    }
}
