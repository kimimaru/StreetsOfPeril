﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class KnifeMenu : WeaponMenu
    {
        public KnifeMenu(WeaponSpawn weaponselected, bool isediting)
        {
            EditingObject = isediting;
            DragGraphic = LoadGraphics.WeaponSprite[weaponselected.WeaponSpawned.GetWeaponType];

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);

            WeaponSelected = weaponselected;

            GeneralSpawnMenu = new GeneralMenu(WeaponSelected);

            CreateBaseControls(Weapon.KnifeStatuses);
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            WeaponSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     "Knife",
                                     Controls[2].GetControlText);

            WeaponSelected.SetObject();
        }
    }
}
