﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //Base class for enemy menus
    //To have the proper menu show up, call the BringUpMenu function in SpawnObject
    public abstract class EnemyMenu : ObjectMenu
    {
        public EnemySpawn EnemySelected;

        public EnemyMenu()
        {
            MenuLocation = ObjectMenuLocation;
        }

        //Creates the base controls that each enemy has
        protected void CreateBaseControls(Status[] possiblestatuses)
        {
            int height = EditingObject == false ? (int)EnemySelected.SpawnAssociated.SpawnLocation.Z : (int)EnemySelected.EnemySpawned.CurHeight;

            Controls.Add(new TextBox(SetSize(0, 0, 35, 15), true, new Vector2(55, -1), "Height: " + height, 8, 3));

            //Level number and difficulty level of the enemy; these only influence the enemy's stats
            Controls.Add(new DropDown(SetSize(0, 20, 13, 20), "Level:", EnemySelected.LevelNum, new Vector2(50, 0), "1", "2", "3", "4", "5", "6", "7", "8", "9"));
            Controls.Add(new DropDown(SetSize(0, 41, 60, 20), "Diff:", EnemySelected.DifficultyNum, new Vector2(50, 0), "V Easy", "Easy", "Normal", "Hard", "V Hard"));

            //Alternate
            Controls.Add(new DropDown(SetSize(0, 62, 60, 20), "Alt:", EnemySelected.Alternative, new Vector2(50, 0), "1", "2", "3", "4"));

            CreateStatusDropDown(EnemySelected.StatusChosen, possiblestatuses);

            //Set the new menu location here so the derived menus can add enemy-specific controls easily
            MenuLocation.Y += 104;
            GeneralSpawnMenu = new GeneralMenu(EnemySelected);

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);
        }

        public override void ChangeObjectIndex()
        {
            EnemySelected.SpawnAssociated.RemoveEnemy(EnemySelected);
            EnemySelected.SpawnAssociated.AddEnemy(EnemySelected);
        }

        protected Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        protected Vector3 GetLocationHeightText()
        {
            return (new Vector3(GeneralSpawnMenu.GetSpawnOffSet(), EditorHelper.TextToNumber((String)Controls[0].GetControlValue) - EnemySelected.SpawnAssociated.SpawnLocation.Z));
        }

        //The actual location of the enemy, relative to its spawn
        protected Vector3 GetActualLocation()
        {
            return (EnemySelected.GetActualLocation(EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);
            GeneralSpawnMenu.Parse(level);

            Vector3 spawnoffset = GetLocationHeight(level);

            EnemySelected.StatusChosen = (int)Controls[4].GetControlValue;
            if (GeneralSpawnMenu.IsSpawnOffSetText() == false) EnemySelected.SpawnOffSet = spawnoffset - EnemySelected.SpawnAssociated.SpawnLocation;
            else EnemySelected.SpawnOffSet = GetLocationHeightText();
            
            EnemySelected.LevelNum = (int)Controls[1].GetControlValue;
            EnemySelected.DifficultyNum = (int)Controls[2].GetControlValue;
            EnemySelected.Alternative = (int)Controls[3].GetControlValue;

            ValidateIndex(EnemySelected.SpawnAssociated.SpawnEnemies);
        }

        public override void AddToLevel(SublevelProp level)
        {
            level.LevelSpawns[EnemySelected.SpawnAssociated.SpawnNumber].AddEnemy(EnemySelected);
        }
    }
}
