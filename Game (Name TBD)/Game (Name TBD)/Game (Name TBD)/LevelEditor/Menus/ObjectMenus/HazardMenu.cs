﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The base class for item menus
    public abstract class HazardMenu : ObjectMenu
    {
        public HazardSpawn HazardSelected;

        public HazardMenu()
        {
            MenuLocation = ObjectMenuLocation;
        }

        //Creates base controls for the hazards
        protected void CreateBaseControls()
        {
            int height = EditingObject == false ? (int)HazardSelected.SpawnAssociated.SpawnLocation.Z : (int)HazardSelected.HazardSpawned.GetLocationHeight.Z;

            //Height
            Controls.Add(new TextBox(SetSize(0, 0, 35, 15), true, new Vector2(55, -1), "Height: " + height, 8, 3));

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);

            GeneralSpawnMenu = new GeneralMenu(HazardSelected);

            MenuLocation.Y += 16;
        }

        public override void ChangeObjectIndex()
        {
            HazardSelected.SpawnAssociated.RemoveHazard(HazardSelected);
            HazardSelected.SpawnAssociated.AddHazard(HazardSelected);
        }

        protected Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        protected Vector3 GetLocationHeightText()
        {
            return (new Vector3(GeneralSpawnMenu.GetSpawnOffSet(), EditorHelper.TextToNumber((String)Controls[0].GetControlValue) - HazardSelected.SpawnAssociated.SpawnLocation.Z));
        }

        //The actual location of the hazard, relative to its spawn
        protected Vector3 GetActualLocation()
        {
            return (HazardSelected.GetActualLocation(EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            GeneralSpawnMenu.Parse(level);

            Vector3 spawnoffset = GetLocationHeight(level);

            HazardSelected.StatusChosen = 0;
            if (GeneralSpawnMenu.IsSpawnOffSetText() == false) HazardSelected.SpawnOffSet = spawnoffset - HazardSelected.SpawnAssociated.SpawnLocation;
            else HazardSelected.SpawnOffSet = GetLocationHeightText();

            ValidateIndex(HazardSelected.SpawnAssociated.SpawnHazards);
        }

        public override void AddToLevel(SublevelProp level)
        {
            HazardSelected.SpawnAssociated.AddHazard(HazardSelected);
        }
    }
}
