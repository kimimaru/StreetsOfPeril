﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class PlatformMenu : HazardMenu
    {
        public PlatformMenu(PlatformSpawn hazardselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.SpikeShooter;

            HazardSelected = hazardselected;

            CreateBaseControls();

            //Decide whether you jump through the platform or not
            Controls.Add(new DropDown(SetSize(0, 0, 50, 20), "Jmp Thr:", Convert.ToInt32((HazardSelected.HazardSpawned as Platform).CanJumpThrough), new Vector2(65, 0), "No", "Yes"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            HazardSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     Convert.ToString(Controls[1].GetControlValue));

            HazardSelected.SetObject();
        }
    }
}
