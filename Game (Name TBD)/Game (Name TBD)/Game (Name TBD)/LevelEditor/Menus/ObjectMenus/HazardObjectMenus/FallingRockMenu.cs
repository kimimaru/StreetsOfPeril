﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class FallingRockMenu : HazardMenu
    {
        public FallingRockMenu(FallingRockSpawn hazardselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.FallingRock;

            HazardSelected = hazardselected;

            CreateBaseControls();

            Controls.Add(new TextBox(SetSize(0, 0, 30, 20), true, new Vector2(68, 0), "Strength: " + (HazardSelected.Params != null ? EditorHelper.TextToNumber(HazardSelected.Params[1]) : 0), 10, 3));

            CreateStatusDropDown(HazardSelected.StatusChosen, new Status[] { new Status((int)Status.Statuses.DamageDown, 8000, 0), new Status((int)Status.Statuses.SpeedDown, 5000f, 0) }, 21);
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);
            
            HazardSelected.StatusChosen = (int)Controls[2].GetControlValue;

            HazardSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     (String)Controls[1].GetControlValue,
                                     Controls[2].GetControlText);
                                     
            HazardSelected.SetObject();
        }
    }
}
