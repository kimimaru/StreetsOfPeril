﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class ScarecrowMenu : HazardMenu
    {
        public ScarecrowMenu(ScarecrowSpawn hazardselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.SpikeShooter;

            HazardSelected = hazardselected;

            CreateBaseControls();
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            HazardSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()));

            HazardSelected.SetObject();
        }
    }
}
