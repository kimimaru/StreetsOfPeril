﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class BSShooterMenu : HazardMenu
    {
        public BSShooterMenu(BSShooterSpawn hazardselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.SpikeShooter;

            HazardSelected = hazardselected;

            CreateBaseControls();

            BSShooter shooter = HazardSelected.HazardSpawned as BSShooter;

            //Bullet or spike
            Controls.Add(new DropDown(SetSize(0, 0, 43, 20), "Projectile:", Convert.ToInt32(shooter.IsBulletShooter), new Vector2(76, 0), "Spike", "Bullet"));

            //Direction facing
            Controls.Add(new DropDown(SetSize(0, 21, 40, 20), "Direction:", Convert.ToInt32(shooter.GFacingRight), new Vector2(76, 0), "Left", "Right"));

            //Shoot rate
            Controls.Add(new TextBox(SetSize(0, 42, 60, 20), true, new Vector2(40, 0), "Rate: " + (int)shooter.GetShootRate, 6, 5));

            //Projectile damage
            Controls.Add(new TextBox(SetSize(0, 63, 30, 20), true, new Vector2(62, 0), "Damage: " + shooter.GetProjectileDamage, 8, 2));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            HazardSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     Controls[1].GetControlValue.ToString(),
                                     Controls[2].GetControlValue.ToString(),
                                     (String)Controls[3].GetControlValue,
                                     (String)Controls[4].GetControlValue);

            HazardSelected.SetObject();
        }
    }
}
