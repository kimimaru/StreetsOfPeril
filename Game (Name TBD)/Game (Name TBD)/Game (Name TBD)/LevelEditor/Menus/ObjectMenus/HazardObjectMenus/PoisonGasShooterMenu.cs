﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class PoisonGasShooterMenu : HazardMenu
    {
        public PoisonGasShooterMenu(PoisonGasShooterSpawn hazardselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.PoisonShooter;

            HazardSelected = hazardselected;

            CreateBaseControls();

            PoisonGasShooter gasshooter = HazardSelected.HazardSpawned as PoisonGasShooter;

            //If the shooter starts out shooting
            Controls.Add(new DropDown(SetSize(0, 0, 40, 20), "Active:", Convert.ToInt32(gasshooter.CurrentlyShooting), new Vector2(53, 0), "False", "True"));

            //Shoot length
            Controls.Add(new TextBox(SetSize(0, 21, 50, 20), true, new Vector2(55, 0), "Length: " + gasshooter.GetShootLength, 8, 3));

            //Shoot duration
            Controls.Add(new TextBox(SetSize(0, 42, 60, 20), true, new Vector2(70, 0), "Shot Dur: " + (int)gasshooter.GetShootTime, 10, 5));

            //Stop duration
            Controls.Add(new TextBox(SetSize(0, 63, 60, 20), true, new Vector2(70, 0), "Stop Dur: " + (int)gasshooter.GetStopTime, 10, 5));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            HazardSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                     Controls[1].GetControlValue.ToString(),
                                     (String)Controls[2].GetControlValue,
                                     (String)Controls[3].GetControlValue,
                                     (String)Controls[4].GetControlValue);

            //PoisonGasShooter poisongasshooter = CreateObjects.PoisonGasShooter(GetLocationHeight(level), Convert.ToBoolean(Controls[1].GetControlValue), EditorHelper.TextToNumber((String)Controls[2].GetControlValue), EditorHelper.TextToNumber((String)Controls[3].GetControlValue), EditorHelper.TextToNumber((String)Controls[4].GetControlValue));
            //HazardSelected.HazardSpawned = poisongasshooter;
            HazardSelected.SetObject();
        }
    }
}
