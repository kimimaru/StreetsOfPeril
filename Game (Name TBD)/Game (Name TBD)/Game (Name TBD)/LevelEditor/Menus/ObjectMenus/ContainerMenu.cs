﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The class for Item Container menus
    public sealed class ContainerMenu : ObjectMenu
    {
        //Container spawn reference
        public ContainerSpawn ContainerSelected;

        public ContainerMenu(ContainerSpawn containerspawn, bool isediting)
        {
            MenuLocation = ObjectMenuLocation;
            EditingObject = isediting;

            ContainerSelected = containerspawn;

            DragGraphic = LoadGraphics.ContainerSprite;

            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height);

            GeneralSpawnMenu = new GeneralMenu(ContainerSelected);

            int height = EditingObject == false ? (int)ContainerSelected.SpawnAssociated.SpawnLocation.Z : (int)ContainerSelected.ContainerSpawned.CurHeight;

            //Container Height
            Controls.Add(new TextBox(SetSize(0, 0, 50, 20), true, new Vector2(55, 0), "Height: " + height, 8, 4));

            //Container Health
            Controls.Add(new TextBox(SetSize(0, 21, 50, 20), true, new Vector2(55, 0), "Health: " + ContainerSelected.ContainerSpawned.GetHealth, 8, 3));

            //Labels for HeldItem and HeldWeapon as well as the accompanying buttons to set them empty
            //The Item Container's item; to set it, place the item on the level in the same spawn as the Item Container and drag it onto the Item Container
            Controls.Add(new Label(SetLocation(0, 55), ContainerSelected.ContainerSpawned.GetHeldItem == null ? "None" : ContainerSelected.ContainerSpawned.GetHeldItem.ToString(), Color.Black));

            //A button for clearing the item the Item Container holds
            Controls.Add(new Button(delegate() 
                { 
                    ContainerSelected.ContainerSpawned = CreateObjects.Barrel(new Vector3(ContainerSelected.ContainerSpawned.GetLocationHeight.X, ContainerSelected.ContainerSpawned.GetLocationHeight.Y, ContainerSelected.ContainerSpawned.GetLocationHeight.Z), ContainerSelected.ContainerSpawned.GetHealth, null, ContainerSelected.ContainerSpawned.GetHeldWeapon);
                    Controls[2].StringUpdate("None");
                }, SetSize(-18, 47, 16, 15), LoadAssets.RemoveItemIcon, Color.White, null, false));

            //The Item Container's weapon; to set it, place the weapon on the level in the same spawn as the Item Container and drag it onto the Item Container
            Controls.Add(new Label(SetLocation(0, 76), ContainerSelected.ContainerSpawned.GetHeldWeapon == null ? "None" : ContainerSelected.ContainerSpawned.GetHeldWeapon.ToString(), Color.Black));

            //A button for clearing the weapon the Item Container holds
            Controls.Add(new Button(delegate() 
                { 
                    ContainerSelected.ContainerSpawned = CreateObjects.Barrel(new Vector3(ContainerSelected.ContainerSpawned.GetLocationHeight.X, ContainerSelected.ContainerSpawned.GetLocationHeight.Y, ContainerSelected.ContainerSpawned.GetLocationHeight.Z), ContainerSelected.ContainerSpawned.GetHealth, ContainerSelected.ContainerSpawned.GetHeldItem, null);
                    Controls[4].StringUpdate("None");
                }, SetSize(-18, 68, 16, 15), LoadAssets.RemoveWeaponIcon, Color.White, null, false));

            //Alternate
            Controls.Add(new DropDown(SetSize(0, 89, 60, 20), "Alt:", ContainerSelected.ContainerSpawned.Alternate, new Vector2(50, 0), "1", "2", "3", "4"));
        }

        public override void ChangeObjectIndex()
        {
            ContainerSelected.SpawnAssociated.RemoveContainer(ContainerSelected);
            ContainerSelected.SpawnAssociated.AddContainer(ContainerSelected);
        }

        private Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        private Vector3 GetLocationHeightText()
        {
            return (new Vector3(GeneralSpawnMenu.GetSpawnOffSet(), EditorHelper.TextToNumber((String)Controls[0].GetControlValue) - ContainerSelected.SpawnAssociated.SpawnLocation.Z));
        }

        //The actual location of the container, relative to its spawn
        private Vector3 GetActualLocation()
        {
            return (ContainerSelected.GetActualLocation(EditorHelper.TextToNumber((String)Controls[0].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            GeneralSpawnMenu.Parse(level);

            Vector3 spawnoffset = GetLocationHeight(level);

            if (GeneralSpawnMenu.IsSpawnOffSetText() == false) ContainerSelected.SpawnOffSet = spawnoffset - ContainerSelected.SpawnAssociated.SpawnLocation;
            else ContainerSelected.SpawnOffSet = GetLocationHeightText();

            ValidateIndex(ContainerSelected.SpawnAssociated.SpawnContainers);

            ContainerSelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                        (String)Controls[1].GetControlValue,
                                        Controls[2].GetControlText,
                                        Controls[4].GetControlText,
                                        ((int)Controls[Controls.Count - 1].GetControlValue).ToString());
                                        
            ContainerSelected.SetObject();
        }

        public override void AddToLevel(SublevelProp level)
        {
            ContainerSelected.SpawnAssociated.AddContainer(ContainerSelected);
        }
    }
}
