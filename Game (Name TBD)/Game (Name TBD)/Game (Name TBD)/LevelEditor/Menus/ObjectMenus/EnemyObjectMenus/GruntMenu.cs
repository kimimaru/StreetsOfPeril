﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class GruntMenu : EnemyMenu
    {
        public GruntMenu(GruntSpawn gruntspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = gruntspawn;

            CreateBaseControls(Grunt.GetPossibleStatuses);

            int behavior = EnemySelected.EnemySpawned.gsAction.CurState;
            if (behavior > (int)ActionManager.ActionStates.Movement) behavior = 1;

            //Grunt's initial behavior options
            Controls.Add(new DropDown(SetSize(0, 0, 64, 20), "Action:", behavior, new Vector2(55, 0), "Default", "Minecart"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    Controls[4].GetControlText,
                                    ((int)Controls[Controls.Count - 1].GetControlValue).ToString(),
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
