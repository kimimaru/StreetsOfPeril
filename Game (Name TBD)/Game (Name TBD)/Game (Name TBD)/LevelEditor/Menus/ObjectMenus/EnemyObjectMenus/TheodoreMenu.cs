﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class TheodoreMenu : EnemyMenu
    {
        public TheodoreMenu(TheodoreSpawn theodorespawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = theodorespawn;

            CreateBaseControls(null);

            //Choose if Theodore starts out enraged or not (you shouldn't really be able to choose but I'm going to allow it anyway)
            int behavior = 0;
            if (EnemySelected.EnemySpawned.gsAction.CurState == (int)ActionManager.ActionStates.Preset1) behavior = 1;

            //Theodore enraged options
            Controls.Add(new DropDown(SetSize(0, 0, 60, 20), "State:", behavior, new Vector2(45, 0), "Normal", "Enraged"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    ((int)Controls[Controls.Count - 1].GetControlValue).ToString(),
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
