﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class CommanderMenu : EnemyMenu
    {
        public CommanderMenu(CommanderSpawn commanderspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = commanderspawn;

            CreateBaseControls(null);
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);
            
            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
