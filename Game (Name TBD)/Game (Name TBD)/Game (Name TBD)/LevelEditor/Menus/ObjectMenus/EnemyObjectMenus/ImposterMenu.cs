﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class ImposterMenu : EnemyMenu
    {
        public ImposterMenu(ImposterSpawn imposterspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = imposterspawn;

            CreateBaseControls(Imposter.GetPossibleStatuses);

            //Imposter's initial mask
            Controls.Add(new DropDown(SetSize(0, 0, 72, 20), "Mask:", (EnemySelected.EnemySpawned as Imposter).GetCurMask(), new Vector2(45, 0), "Offensive", "Defensive", "Speed"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()), 
                                    Controls[4].GetControlText,
                                    ((int)Controls[Controls.Count - 1].GetControlValue).ToString(),
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
