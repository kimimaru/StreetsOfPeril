﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class DiverMenu : EnemyMenu
    {
        public DiverMenu(DiverSpawn diverspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = diverspawn;

            CreateBaseControls(Diver.GetPossibleStatuses);

            //Diver's dive rate; it really shouldn't be longer than 9 seconds
            Controls.Add(new TextBox(SetSize(0, 0, 42, 20), null, new Vector2(75, 0), "Dive Rate: " + (int)(EnemySelected.EnemySpawned as Diver).GetDiveRate, 11, 4));

            int weapon = 0;
            if (EnemySelected.Params != null) weapon = EditorHelper.TextToNumber(EnemySelected.Params[3]);

            //Diver's behavior; this determines whether he uses his harpoon or throws bombs
            Controls.Add(new DropDown(SetSize(0, 21, 64, 20), "Attack:", weapon, new Vector2(55, 0), "Harpoon", "Bombs"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    Controls[4].GetControlText,
                                    (String)Controls[5].GetControlValue,
                                    ((int)Controls[Controls.Count - 1].GetControlValue).ToString(),
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
