﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class WeaponWielderMenu : EnemyMenu
    {
        public WeaponWielderMenu(WeaponWielderSpawn weaponwielderspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = weaponwielderspawn;

            CreateBaseControls(WeaponWielder.GetPossibleStatuses);

            //The Weapon Wielder's weapon; to set it, place the weapon on the level in the same spawn as the Weapon Wielder and drag it onto the Weapon Wielder like you would for an Item Container
            Controls.Add(new Label(SetLocation(0, 10), EnemySelected.EnemySpawned.weapon == null ? "None" : EnemySelected.EnemySpawned.weapon.ToString(), Color.Black));

            //A button for clearing the weapon the Weapon Wielder has
            Controls.Add(new Button(delegate() 
                { 
                    EnemySelected.EnemySpawned.ClearWeapon();
                    Controls[4].StringUpdate("None");
                }, SetSize(-18, 3, 16, 15), LoadAssets.RemoveWeaponIcon, Color.White, null, false));

            //Whether the Weapon Wielder is male (Gerald) or female (Kate)
            Controls.Add(new DropDown(SetSize(0, 21, 53, 20), "Gender:", Convert.ToInt32((EnemySelected.EnemySpawned as WeaponWielder).IsMale), new Vector2(60, 0), "Female", "Male"));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    Controls[4].GetControlText,
                                    ((int)Controls[Controls.Count - 1].GetControlValue).ToString(),
                                    Controls[5].GetControlText,
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
