﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class MinecartRiderMenu : EnemyMenu
    {
        public MinecartRiderMenu(MinecartRiderSpawn minecartriderspawn, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.enemsprite;

            EnemySelected = minecartriderspawn;

            CreateBaseControls(MinecartRider.GetPossibleStatuses);

            //The direction the MinecartRider is facing; this also dictates which direction he moves in
            Controls.Add(new DropDown(SetSize(0, 0, 45, 20), "Move:", Convert.ToInt32(EnemySelected.EnemySpawned.FacingRight), new Vector2(50, 0), "Right", "Left"));

            //The MinecartRider's throw velocity
            Controls.Add(new TextBox(SetSize(0, 21, 60, 20), null, new Vector2(55, 0), "Throw: " + EditorHelper.ConvertVector2String((EnemySelected.EnemySpawned as MinecartRider).GetBottleVelocity), 7, 5));
        }

        public override void Parse(SublevelProp level)
        {
            base.Parse(level);

            EnemySelected.SetParams(EditorHelper.ConvertVector3String(GetActualLocation()),
                                    Controls[4].GetControlText,
                                    ((int)Controls[5].GetControlValue).ToString(),
                                    (String)Controls[6].GetControlValue,
                                    EnemySelected.LevelNum.ToString(),
                                    EnemySelected.DifficultyNum.ToString(),
                                    EnemySelected.Alternative.ToString());

            EnemySelected.SetObject();
        }
    }
}
