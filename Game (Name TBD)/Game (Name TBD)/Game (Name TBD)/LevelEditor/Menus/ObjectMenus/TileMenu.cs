﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A menu for changing a tile's properties, like height, typeheight, and tile type
    //It acts like an object menu but technically isn't one since it's strictly for editing tiles and doesn't involve placing them
    public class TileMenu : ObjectMenu
    {
        //Tile Engine reference
        private TileEngine TileEngine;

        //Tile value
        private TileEngine.Tile Tile;

        //NOTE: This may be harder because tiles are value types instead of reference types; see what you can do to easily edit them
        //Try passing in the tile engine itself and the index to modify
        public TileMenu(TileEngine tileengine, TileEngine.Tile tile)
        {
            MenuLocation = ObjectMenuLocation;
            EditingObject = true;

            TileEngine = tileengine;
            Tile = tile;

            //Tile Height
            Controls.Add(new TextBox(SetSize(-20, 0, 40, 20), true, new Vector2(55, 0), "Height: " + (int)Tile.Z, 8, 4));

            //Tile Typeheight
            Controls.Add(new TextBox(SetSize(-20, 21, 40, 20), true, new Vector2(87, 0), "Typeheight: " + (int)Tile.TypeHeight, 12, 4));

            //Tile type
            Controls.Add(new DropDown(SetSize(-20, 42, 63, 20), "Tile Type:", Tile.TileType, new Vector2(75, 0), "None", "Water", "Grass", "Museum"));

            //Label for applying below changes
            Controls.Add(new Label(SetLocation(-40, 73), "Apply to Row/Col", Color.Black));

            //Determine whether to apply this change to all of the tiles on the same row
            Controls.Add(new DropDown(SetSize(0, 84, 40, 20), "All Row:", 0, new Vector2(70, 0), "False", "True"));

            //Determine whether to apply this change to all of the tiles on the same column
            Controls.Add(new DropDown(SetSize(0, 105, 40, 20), "All Cols:", 0, new Vector2(70, 0), "False", "True"));
        }

        public override void Parse(SublevelProp level)
        {
            Vector4 tileloc = new Vector4(Tile.Location.X, Tile.Location.Y, Tile.Location.Z, 0f);

            float tileheight = (float)EditorHelper.TextToNumber((String)Controls[0].GetControlValue);
            float typeheight = (float)EditorHelper.TextToNumber((String)Controls[1].GetControlValue);

            //Since you can type in 4 numbers to allow 3 digit negative numbers, restrain the positive numbers to 3 digits
            if (tileheight > 999) tileheight = 999;
            if (typeheight > 999) typeheight = 999;

            int tiletype = (int)Controls[2].GetControlValue;

            bool row = Convert.ToBoolean(Controls[4].GetControlValue);
            bool column = Convert.ToBoolean(Controls[5].GetControlValue);

            //Edit the tile values
            if (row == false && column == false) TileEngine.EditTileValues(tileloc, tileheight, typeheight, tiletype);
            else
            {
                //Check whether to apply this change to the entire row
                if (row == true) TileEngine.EditTileValuesRow(tileloc, tileheight, typeheight, tiletype);

                //Check whether to apply this change to the entire column
                if (column == true) TileEngine.EditTileValuesColumn(tileloc, tileheight, typeheight, tiletype);
            }
        }
    }
}
