﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class PileOfMoneyMenu : ItemMenu
    {
        public PileOfMoneyMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.PileOfMoney;

            ItemSelected = itemselected;

            CreateBaseControls(null);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item pileofmoney = CreateObjects.PileOfMoney();
        //    ItemSelected.ItemSpawned = pileofmoney;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
