﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class PocketWatchMenu : ItemMenu
    {
        public PocketWatchMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.PocketWatch;

            ItemSelected = itemselected;

            CreateBaseControls(null);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item pocketwatch = CreateObjects.PocketWatch();
        //    ItemSelected.ItemSpawned = pocketwatch;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
