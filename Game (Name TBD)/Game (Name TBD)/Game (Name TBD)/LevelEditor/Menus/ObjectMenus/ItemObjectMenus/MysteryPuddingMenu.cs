﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class MysteryPuddingMenu : ItemMenu
    {
        public MysteryPuddingMenu(ItemSpawn itemspawned, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemspawned;

            CreateBaseControls(Item.MysteryPuddingStatuses);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    //Mystery Pudding isn't officially in the game yet since there's no easy CreateObjects method for it (and no actual item type that would choose a random status to have), but we'll do the temporary way for now
        //    Item mysterypudding = new Item(LoadGraphics.ItemSprite, "Mystery Pudding", 0, 0f, 1, false, 0, Status.StatFull(Controls[2].GetControlText), Vector2.Zero, Vector2.Zero);
        //    ItemSelected.ItemSpawned = mysterypudding;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
