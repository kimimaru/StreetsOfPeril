﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class BiscuitMenu : ItemMenu
    {
        public BiscuitMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemselected;

            CreateBaseControls(Item.BiscuitStatuses);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item biscuit = CreateObjects.Biscuit(Status.StatFull(Controls[2].GetControlText));
        //    ItemSelected.ItemSpawned = biscuit;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
