﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class BearClawMenu : ItemMenu
    {
        public BearClawMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemselected;

            CreateBaseControls(null, "DamageBoost,10000,0");
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item bearclaw = CreateObjects.BearClaw();
        //    ItemSelected.ItemSpawned = bearclaw;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
