﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class TurkeyMenu : ItemMenu
    {
        public TurkeyMenu(ItemSpawn itemspawned, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.Turkey;

            ItemSelected = itemspawned;

            CreateBaseControls(Item.TurkeyStatuses);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item turkey = CreateObjects.Turkey(Status.StatFull(Controls[2].GetControlText));
        //    ItemSelected.ItemSpawned = turkey;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
