﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class RiceBowlMenu : ItemMenu
    {
        public RiceBowlMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemselected;

            CreateBaseControls(null);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item ricebowl = CreateObjects.RiceBowl();
        //    ItemSelected.ItemSpawned = ricebowl;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
