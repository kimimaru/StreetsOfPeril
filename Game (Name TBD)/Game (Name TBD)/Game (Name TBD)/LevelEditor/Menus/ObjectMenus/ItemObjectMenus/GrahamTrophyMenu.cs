﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class GrahamTrophyMenu : ItemMenu
    {
        public GrahamTrophyMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemselected;

            CreateBaseControls(null, "Invincible,15000,0");
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    Item grahamtrophy = CreateObjects.GrahamTrophy();
        //    ItemSelected.ItemSpawned = grahamtrophy;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
