﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class HealthJuiceMenu : ItemMenu
    {
        public HealthJuiceMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            ItemSelected = itemselected;

            DragGraphic = LoadGraphics.ItemSprite;

            CreateBaseControls(null);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    HealthJuice healthjuice = CreateObjects.HealthJuice();
        //    ItemSelected.ItemSpawned = healthjuice;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
