﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    public sealed class HealthCandyMenu : ItemMenu
    {
        public HealthCandyMenu(ItemSpawn itemselected, bool isediting)
        {
            EditingObject = isediting;

            DragGraphic = LoadGraphics.ItemSprite;

            ItemSelected = itemselected;

            CreateBaseControls(null);
        }

        //public override void Parse(SublevelProp level)
        //{
        //    base.Parse(level);
        //
        //    HealthCandy healthcandy = CreateObjects.HealthCandy();
        //    ItemSelected.ItemSpawned = healthcandy;
        //    ItemSelected.ItemSpawned.SetLocation(GetLocationHeight(level));
        //}
    }
}
