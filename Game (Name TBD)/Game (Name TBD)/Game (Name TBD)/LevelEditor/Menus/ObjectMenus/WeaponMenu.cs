﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The base class for weapon menus
    public abstract class WeaponMenu : ObjectMenu
    {
        public WeaponSpawn WeaponSelected;
        
        public WeaponMenu()
        {
            MenuLocation = ObjectMenuLocation;
        }

        //Creates base controls for the weapons
        protected void CreateBaseControls(Status[] possiblestatuses)
        {
            int height = EditingObject == false ? (int)WeaponSelected.SpawnAssociated.SpawnLocation.Z : (int)WeaponSelected.WeaponSpawned.CurHeight;

            //Weapon Name
            Controls.Add(new Label(SetLocation(0, 10), "Name: " + WeaponSelected.WeaponSpawned.Name, Color.Black));

            //Height
            Controls.Add(new TextBox(SetSize(0, 22, 35, 15), true, new Vector2(55, -1), "Height: " + height, 8, 3));

            //Statuses
            CreateStatusDropDown(WeaponSelected.StatusChosen, possiblestatuses, 38);

            MenuLocation.Y += 54;
        }

        public override void ChangeObjectIndex()
        {
            WeaponSelected.SpawnAssociated.RemoveWeapon(WeaponSelected);
            WeaponSelected.SpawnAssociated.AddWeapon(WeaponSelected);
        }

        protected Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[1].GetControlValue)));
        }

        protected Vector3 GetLocationHeightText()
        {
            return (new Vector3(GeneralSpawnMenu.GetSpawnOffSet(), EditorHelper.TextToNumber((String)Controls[1].GetControlValue) - WeaponSelected.SpawnAssociated.SpawnLocation.Z));
        }

        //The actual location of the item, relative to its spawn
        protected Vector3 GetActualLocation()
        {
            return (WeaponSelected.GetActualLocation(EditorHelper.TextToNumber((String)Controls[1].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            GeneralSpawnMenu.Parse(level);

            Vector3 spawnoffset = GetLocationHeight(level);

            WeaponSelected.StatusChosen = (int)Controls[2].GetControlValue;

            if (GeneralSpawnMenu.IsSpawnOffSetText() == false) WeaponSelected.SpawnOffSet = spawnoffset - WeaponSelected.SpawnAssociated.SpawnLocation;
            else WeaponSelected.SpawnOffSet = GetLocationHeightText();

            ValidateIndex(WeaponSelected.SpawnAssociated.SpawnWeapons);

            if (IsEditingObject == true)
                CheckGiveWeaponWielderContainer(level);
        }

        //Checks if the weapon should be given to a Weapon Wielder or Item Container and gives it to it if so
        private bool CheckGiveWeaponWielderContainer(SublevelProp level)
        {
            for (int i = 0; i < WeaponSelected.SpawnAssociated.SpawnEnemies.Count; i++)
            {
                //Check if the enemy doesn't have a weapon and the weapon's click box intersects the enemy's click box
                if (WeaponSelected.SpawnAssociated.SpawnEnemies[i].EnemySpawned.weapon == null && EditorHelper.ClickedRect(WeaponSelected.SpawnAssociated.SpawnEnemies[i].SelectionRectangle(-level.OffSet)) == true)
                {
                    //Now check if the enemy is a Weapon Wielder
                    WeaponWielderSpawn weaponwielderspawn = WeaponSelected.SpawnAssociated.SpawnEnemies[i] as WeaponWielderSpawn;
                    if (weaponwielderspawn != null)
                    {
                        //It is, so give the enemy this weapon
                        weaponwielderspawn.GiveWeapon(WeaponSelected);
                        return true;
                    }
                }
            }

            for (int i = 0; i < WeaponSelected.SpawnAssociated.SpawnContainers.Count; i++)
            {
                //Check if the container doesn't have a weapon and check if the weapon's click box intersects the container's click box
                if (WeaponSelected.SpawnAssociated.SpawnContainers[i].ContainerSpawned.GetHeldWeapon == null && EditorHelper.ClickedRect(WeaponSelected.SpawnAssociated.SpawnContainers[i].SelectionRectangle(-level.OffSet)) == true)
                {
                    WeaponSelected.SpawnAssociated.SpawnContainers[i].AddWeapon(WeaponSelected);
                    return true;
                }
            }

            return false;
        }

        public override void AddToLevel(SublevelProp level)
        {
            //Check if the weapon is going to be placed on a Weapon Wielder or Item Container so it can be given to it
            //NOTE: Move this; it should work if the weapon is already in the level and was simply moved
            if (CheckGiveWeaponWielderContainer(level) == false)
            {
                //The weapon was not given to a Weapon Wielder or Item Container, so it'll spawn on its own in the level
                level.LevelSpawns[WeaponSelected.SpawnAssociated.SpawnNumber].AddWeapon(WeaponSelected);
            }
        }
    }
}
