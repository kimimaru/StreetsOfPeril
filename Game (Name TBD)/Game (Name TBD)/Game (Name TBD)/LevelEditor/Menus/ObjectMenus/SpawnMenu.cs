﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The editing menu for the spawn; it's in the same location as the object editing menu
    public sealed class SpawnMenu : ObjectMenu
    {
        //The spawn
        public Spawn SpawnSelected;

        //Constructor - pass in a Spawn if you're editing it, otherwise if you're adding a new Spawn pass in one instantiated with the default Spawn constructor
        public SpawnMenu(Spawn spawnselected, bool isediting)
        {
            MenuLocation = ObjectMenuLocation;
            EditingObject = isediting;

            DragGraphic = LoadAssets.SpawnIcon;
            GraphicOrigin = new Vector2(LoadAssets.SpawnIcon.Width / 2, LoadAssets.SpawnIcon.Height / 2);

            SpawnSelected = spawnselected;

            //At the moment you can't change the spawn number for the spawn since I find no need to, but in the future it may be necessary
            Controls.Add(new Label(SetLocation(0, -30), "Number: " + SpawnSelected.SpawnNumber, Color.Black));

            Controls.Add(new Label(SetLocation(0, -12), "Loc: " + EditorHelper.ConvertVector3String(SpawnSelected.SpawnLocation), Color.Black));
            Controls.Add(new TextBox(SetSize(0, 2, 50, 15), true, new Vector2(55, 0), "Height: " + spawnselected.SpawnLocation.Z, 8, 4));
            Controls.Add(new TextBox(SetSize(0, 22, 50, 15), null, new Vector2(48, 0), "Offset: " + EditorHelper.ConvertVector2String(SpawnSelected.OffSet), 8, 7));
            Controls.Add(new TextBox(SetSize(0, 40, 50, 15), null, new Vector2(38, 0), "Stop: " + SpawnSelected.StopPoint, 6, 3));
            Controls.Add(new DropDown(SetSize(0, 60, 75, 20), "Diff:", SpawnSelected.DifficultyLevel, new Vector2(38, 0), "Very Easy", "Easy", "Normal", "Hard", "Very Hard"));//TextBox(SetSize(0, 80, 50, 20), null, "Difficulty: " + SpawnSelected.DifficultyLevel, 12, 1));
            Controls.Add(new DropDown(SetSize(0, 80, 12, 18), "Min Players:", SpawnSelected.MinPlayers - 1, new Vector2(95, 2), "1", "2", "3", "4"));
            Controls.Add(new DropDown(SetSize(0, 100, 37, 15), "Move All:", Convert.ToInt32(SpawnSelected.ObjectsMove), new Vector2(75, 2), "False", "True"));
            Controls.Add(new DropDown(SetSize(0, 120, 37, 15), "Disabled:", Convert.ToInt32(SpawnSelected.Disabled), new Vector2(75, 2), "False", "True"));

            //Total number of objects the spawn has
            Controls.Add(new Label(SetLocation(0, 150), "Enemies: " + SpawnSelected.TotalEnemies, Color.Black));
            Controls.Add(new Label(SetLocation(0, 170), "Containers: " + SpawnSelected.TotalContainers, Color.Black));
            Controls.Add(new Label(SetLocation(0, 190), "Hazards: " + SpawnSelected.TotalHazards, Color.Black));
            Controls.Add(new Label(SetLocation(0, 210), "Items: " + SpawnSelected.TotalItems, Color.Black));
            Controls.Add(new Label(SetLocation(0, 230), "Weapons: " + SpawnSelected.TotalWeapons, Color.Black));
            Controls.Add(new Label(SetLocation(0, 250), "Total: " + SpawnSelected.TotalObjects, Color.Black));
        }

        public override void Parse(SublevelProp level)
        {
            //If the spawn is a new one, set its spawn number
            if (EditingObject == false) SpawnSelected.SpawnNumber = level.LevelSpawns.Count;

            //Store the old spawn location so you can move all of its objects by the amount it moved so they move with it
            Vector3 prevspawnlocation = SpawnSelected.SpawnLocation;

            //NOTE: I'm not sure about editing the spawn location via the location parameter yet because it can be a pain and isn't as intuitive as clicking
            SpawnSelected.SpawnLocation = new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[2].GetControlValue));//EditorHelper.ParseVector2((String)Controls[0].GetControlValue);
            SpawnSelected.OffSet = EditorHelper.ParseVector2((String)Controls[3].GetControlValue);

            SpawnSelected.StopPoint = EditorHelper.TextToNumber((String)Controls[4].GetControlValue);
            if (SpawnSelected.StopPoint < 0 || SpawnSelected.StopPoint >= level.StopPoints.Count) SpawnSelected.StopPoint = level.StopPoints.Count - 1;

            SpawnSelected.DifficultyLevel = (int)Controls[5].GetControlValue;
            SpawnSelected.MinPlayers = (int)Controls[6].GetControlValue + 1;
            SpawnSelected.ObjectsMove = Convert.ToBoolean(Controls[7].GetControlValue);
            SpawnSelected.Disabled = Convert.ToBoolean(Controls[8].GetControlValue);

            SpawnSelected.SpawnTile = level.TileEngine.CurTile(SpawnSelected.SelectionRectangle(Vector2.Zero));

            //If we're editing the Spawn and it's in a new location, move all of the objects the Spawn contains with it
            if (EditingObject == true)
            {
                //Check if the spawn is in a new location
                if (prevspawnlocation != SpawnSelected.SpawnLocation)
                {
                    //If objects aren't set to move with the spawn, simply update all their offsets
                    if (SpawnSelected.ObjectsMove == false)
                    {
                        //Update all objects' offsets in the spawn
                        SpawnSelected.UpdateObjectOffSets(prevspawnlocation - SpawnSelected.SpawnLocation);
                    }
                    //Otherwise, move all the objects with the spawn
                    else
                    {
                        //Move all objects in the spawn
                        SpawnSelected.MoveObjects(SpawnSelected.SpawnLocation - prevspawnlocation);
                    }
                }
            }
        }

        //Adds the spawn to the level
        public override void AddToLevel(SublevelProp level)
        {
            level.AddSpawn(SpawnSelected);
        }
    }
}
