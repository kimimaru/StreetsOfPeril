﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for editing PlayerSpawns
    public sealed class PlayerSpawnMenu : ObjectMenu
    {
        //Player spawn reference
        public PlayerSpawn PlayerSelected;

        public PlayerSpawnMenu(PlayerSpawn playerselected)
        {
            MenuLocation = ObjectMenuLocation;
            EditingObject = true;

            PlayerSelected = playerselected;

            DragGraphic = PlayerSelected.Graphic;
            GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height / 2);

            //Location
            Controls.Add(new Label(SetLocation(0, 0), "Location: \n" + EditorHelper.ConvertVector3String(PlayerSelected.Location), Color.Black));

            //Height
            Controls.Add(new TextBox(SetSize(0, 20, 50, 20), true, new Vector2(55, 0), "Height: " + (int)PlayerSelected.Location.Z, 8, 3));
        }

        private Vector3 GetLocationHeight(SublevelProp level)
        {
            return (new Vector3(EditorHelper.GetMousePositionLevel(level), EditorHelper.TextToNumber((String)Controls[1].GetControlValue)));
        }

        public override void Parse(SublevelProp level)
        {
            PlayerSelected.Location = GetLocationHeight(level);

            PlayerSelected.PlayerSpawnTile = level.TileEngine.CurTile(PlayerSelected.SelectionRectangle(Vector2.Zero));
        }
    }
}
