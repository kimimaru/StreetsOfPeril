﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for creating items to place on the level
    public sealed class ItemCreateMenu : EditingMenu
    {
        private SublevelProp Level;

        public ItemCreateMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = CreateMenuLocation;
            Level = level;

            //Create buttons that show the item icons on them so it's easy to know which button corresponds to which item
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new RiceBowlMenu(new ItemSpawn(CreateObjects.RiceBowl(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new BiscuitMenu(new ItemSpawn(CreateObjects.Biscuit(new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(24, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new EnergyDrinkMenu(new ItemSpawn(CreateObjects.EnergyDrink(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(48, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new TurkeyMenu(new ItemSpawn(CreateObjects.Turkey(new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(72, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new BTokenMenu(new ItemSpawn(CreateObjects.BonusToken(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(96, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new OTReplaceMenu(new ItemSpawn(CreateObjects.OTReplace(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(120, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new HealthJuiceMenu(new ItemSpawn(CreateObjects.HealthJuice(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(144, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new HealthCandyMenu(new ItemSpawn(CreateObjects.HealthCandy(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(168, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new BearClawMenu(new ItemSpawn(CreateObjects.BearClaw(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(192, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ClamShellMenu(new ItemSpawn(CreateObjects.ClamShell(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(216, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new PileOfMoneyMenu(new ItemSpawn(CreateObjects.PileOfMoney(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(240, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new PocketWatchMenu(new ItemSpawn(CreateObjects.PocketWatch(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(264, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new BagOfJewelsMenu(new ItemSpawn(CreateObjects.BagofJewels(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(288, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new GrahamTrophyMenu(new ItemSpawn(CreateObjects.GrahamTrophy(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(312, 0, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new GrahamDollMenu(new ItemSpawn(CreateObjects.GrahamDoll(), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 24, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new MysteryPuddingMenu(new ItemSpawn(new Item(LoadGraphics.ItemSprite, "Mystery Pudding", 0, 0f, 1, false, 0, new Status(), Vector2.Zero, Vector2.Zero), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(24, 24, 16, 15), LoadGraphics.ItemIcon, Color.White, null, false));
        }
    }
}
