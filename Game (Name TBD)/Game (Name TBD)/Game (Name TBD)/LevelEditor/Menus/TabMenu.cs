﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A tab menu that contains a set of menus, each accessed through their respective tabs
    public class TabMenu : EditingMenu
    {
        //The current tab that's selected
        private int CurrentTab;
        
        //An array of menus that correspond to each tab
        private EditingMenu[] Menus;

        //Level reference for editing
        private SublevelProp Level;

        public TabMenu(SublevelProp level, params EditingMenu[] menus)
        {
            MenuLocation = new Vector2(0, 330);
            Menus = menus;
            CurrentTab = 0;
            Level = level;

            //The button tabs
            Controls.Add(new Button(delegate() { CurrentTab = 0; }, SetSize(1, 0, 70, 30), null, Color.White, "Level", false));
            Controls.Add(new Button(delegate() { CurrentTab = 1; }, SetSize(75, 0, 70, 30), null, Color.White, "Enemies", false));
            Controls.Add(new Button(delegate() { CurrentTab = 2; }, SetSize(150, 0, 70, 30), null, Color.White, "Containers", false));
            Controls.Add(new Button(delegate() { CurrentTab = 3; }, SetSize(225, 0, 70, 30), null, Color.White, "Hazards", false));
            Controls.Add(new Button(delegate() { CurrentTab = 4; }, SetSize(300, 0, 70, 30), null, Color.White, "Items", false));
            Controls.Add(new Button(delegate() { CurrentTab = 5; }, SetSize(375, 0, 70, 30), null, Color.White, "Weapons", false));
        }

        //Updates the displayed tile offset after a level has been loaded
        public void UpdateTileOffSet()
        {
            LevelMenu levelmenu = Menus[0] as LevelMenu;

            if (levelmenu != null)
                levelmenu.UpdateTileOffSet();
        }

        public override void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            base.Update(activeTime, mouse, keyboard, main);

            //A level needs a spawn before you can place objects
            if (CurrentTab == 0 || (/*EditorHelper.IndexWithinArray<EditingMenu>(Menus, CurrentTab) == true && Menus[CurrentTab] != null && */Level.HasSpawn == true))
                Menus[CurrentTab].Update(activeTime, mouse, keyboard, main);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            if (EditorHelper.IndexWithinArray<EditingMenu>(Menus, CurrentTab) == true && Menus[CurrentTab] != null)
                Menus[CurrentTab].Draw(spriteBatch);
        }
    }
}
