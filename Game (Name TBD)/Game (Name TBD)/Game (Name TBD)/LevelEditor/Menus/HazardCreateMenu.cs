﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for creating hazards to place on the level
    public sealed class HazardCreateMenu : EditingMenu
    {
        private SublevelProp Level;

        public HazardCreateMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = CreateMenuLocation;
            Level = level;

            //Create buttons that show the hazard icons on them so it's easy to know which button corresponds to which hazard
            //NOTE: Most of these Hazards are scrapped from the game and will no longer be used
            //We're keeping their Menus and Spawns in the game to maintain compatibility (and it's easier), so they will simply be inaccessible
            //Controls.Add(new Button(delegate() { main.SetSelectedObject(new FallingRockMenu(new FallingRockSpawn(CreateObjects.FallingRock(LoadGraphics.FallingRock, LoadGraphics.FallingRockDust, Vector3.Zero, new Status(), 0), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.CrystalIcon, Color.White, null, false));
            //Controls.Add(new Button(delegate() { main.SetSelectedObject(new BSShooterMenu(new BSShooterSpawn(CreateObjects.BSShooter(true, Vector3.Zero, true), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(24, 0, 16, 15), LoadGraphics.GrahamIcon, Color.White, null, false));
            //Controls.Add(new Button(delegate() { main.SetSelectedObject(new PoisonGasShooterMenu(new PoisonGasShooterSpawn(CreateObjects.PoisonGasShooter(Vector3.Zero, false, 50, 500f, 500f), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(48, 0, 16, 15), LoadGraphics.WilIcon, Color.White, null, false));
            //Controls.Add(new Button(delegate() { main.SetSelectedObject(new PlatformMenu(new PlatformSpawn(CreateObjects.Platform(Vector3.Zero, false), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(72, 0, 16, 15), LoadGraphics.JeffIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ScarecrowMenu(new ScarecrowSpawn(CreateObjects.Scarecrow(Vector3.Zero), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.JeffIcon, Color.White, null, false));
        }
    }
}
