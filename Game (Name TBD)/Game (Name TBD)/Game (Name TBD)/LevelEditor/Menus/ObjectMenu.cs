﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //An object menu for specific objects
    //USER EDITED
    public abstract class ObjectMenu : EditingMenu
    {
        //A reference to a general menu for spawns; many level objects will have it for changing spawn properties
        public GeneralMenu GeneralSpawnMenu;

        //The graphic shown when dragging the object (to position it)
        public Texture2D DragGraphic;

        //The origin for drawing the graphic
        protected Vector2 GraphicOrigin;

        //Tells if you're editing an object
        protected bool EditingObject;

        public ObjectMenu()
        {
            GeneralSpawnMenu = null;
            EditingObject = false;
            GraphicOrigin = Vector2.Zero;
            
            if (DragGraphic != null) GraphicOrigin = new Vector2(DragGraphic.Width / 2, DragGraphic.Height / 2);
        }

        public bool IsEditingObject
        {
            get { return EditingObject; }
        }

        public override bool IsFocused()
        {
            return (base.IsFocused() == true || (GeneralSpawnMenu != null && GeneralSpawnMenu.IsFocused() == true));
        }

        //Adds the object to the appropriate list in the level
        public virtual void AddToLevel(SublevelProp level)
        {

        }

        //Creates a drop down of selectable Status Effects when given an array of statuses
        protected void CreateStatusDropDown(int statuschosen, Status[] possiblestatuses, int y = 83, String initialstatuschoice = "None,0,0")
        {
            String[] statuschoices = new String[possiblestatuses != null ? (possiblestatuses.Length + 1) : 1];
            statuschoices[0] = initialstatuschoice ?? "None,0,0";

            if (possiblestatuses != null)
            {
                for (int i = 1; i < statuschoices.Length; i++)
                    statuschoices[i] = possiblestatuses[i - 1].ToString();
            }
            Controls.Add(new DropDown(SetSize(0, y, 80, 20), statuschosen, statuschoices));
        }

        //Validates the index of the object in case the index wasn't within range of the list it belongs to
        protected void ValidateIndex<T>(List<T> objectlist) where T : SpawnObject
        {
            if (GeneralSpawnMenu != null && (GeneralSpawnMenu.SelectedObject.Index < 0 || GeneralSpawnMenu.SelectedObject.Index >= objectlist.Count))
            {
                GeneralSpawnMenu.SelectedObject.Index = EditingObject == false ? objectlist.Count : objectlist.Count - 1;
            }
        }

        //Called when a SpawnObject's index is changed; it removes the SpawnObject from its respective list and adds it back
        //This enables it to be in the list at the right index
        public virtual void ChangeObjectIndex()
        {
            
        }

        public override void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            base.Update(activeTime, mouse, keyboard, main);

            if (GeneralSpawnMenu != null) GeneralSpawnMenu.Update(activeTime, mouse, keyboard, main);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            if (GeneralSpawnMenu != null) GeneralSpawnMenu.Draw(spriteBatch);

            if (DragGraphic != null)
                spriteBatch.Draw(DragGraphic, EditorHelper.GetMousePosition(), null, Color.White, 0f, GraphicOrigin, 1f, SpriteEffects.None, 1f);
        }
    }
}
