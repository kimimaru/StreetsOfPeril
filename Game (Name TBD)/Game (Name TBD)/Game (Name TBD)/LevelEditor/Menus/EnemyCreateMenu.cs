﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for creating enemies to place on the level
    public sealed class EnemyCreateMenu : EditingMenu
    {
        private SublevelProp Level;

        public EnemyCreateMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = CreateMenuLocation;
            Level = level;

            //Create buttons that show the enemy icons on them so it's easy to know which button corresponds to which enemy
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new MarkMenu(new MarkSpawn(CreateObjects.Mark(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Mark], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ChristopherMenu(new ChristopherSpawn(CreateObjects.Christopher(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(24, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Christopher], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new PaulMenu(new PaulSpawn(CreateObjects.Paul(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(48, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Paul], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new TankMenu(new TankSpawn(CreateObjects.Tank(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(72, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Tank], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new GregMenu(new GregSpawn(CreateObjects.Greg(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(96, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Greg], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new GruntMenu(new GruntSpawn(CreateObjects.Grunt(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(120, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Grunt], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new RogueMenu(new RogueSpawn(CreateObjects.Rogue(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(144, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Rogue], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ChangerMenu(new ChangerSpawn(CreateObjects.Changer(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(168, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Changer], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ImposterMenu(new ImposterSpawn(CreateObjects.Imposter(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(192, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Imposter], Color.White, null, false));            
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new WeaponWielderMenu(new WeaponWielderSpawn(CreateObjects.WeaponWielder(Vector3.Zero, new Status(), true, null), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(216, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.WeaponWielder], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new SamsonMenu(new SamsonSpawn(CreateObjects.Samson(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(240, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Samson], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new TheodoreMenu(new TheodoreSpawn(CreateObjects.Theodore(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(264, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Theodore], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new MinecartRiderMenu(new MinecartRiderSpawn(CreateObjects.MinecartRider("Minecart Rider", Vector3.Zero, new Status(), LoadGraphics.enemsprite, Vector2.Zero), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(288, 0, 16, 15), /*LoadGraphics.EnemyIcon[(int)Enemy.Enemies.MinecartRider]*/LoadAssets.TempMinecartRiderIcon, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new DiverMenu(new DiverSpawn(CreateObjects.Diver("Diver", Vector3.Zero, new Status(), LoadGraphics.enemsprite, 0, 1), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(312, 0, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Diver], Color.White, null, false));

            //Bosses are underneath the enemies
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(0, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(24, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(48, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(72, 46, 16, 15), LoadGraphics.EnemyIcon[(int)Enemy.Enemies.Commander], Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(96, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(120, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(144, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(168, 46, 16, 15), null, Color.White, null, false));
            Controls.Add(new Button(delegate() { /*main.SetSelectedObject(new CommanderMenu(new CommanderSpawn(CreateObjects.Commander(Vector3.Zero, new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false));*/ }, SetSize(192, 46, 16, 15), null, Color.White, null, false));
        }
    }
}
