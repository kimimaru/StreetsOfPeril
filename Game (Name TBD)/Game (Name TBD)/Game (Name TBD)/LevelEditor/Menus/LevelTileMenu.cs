﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for editing tiles
    public class LevelTileMenu : EditingMenu
    {
        //The level reference for accessing and changing its information
        private SublevelProp Level;

        public LevelTileMenu(SublevelProp level)
        {
            MenuLocation = new Vector2(200, 400);

            Level = level;

            Controls.Add(new Label(SetLocation(0, -10), "Tiles:", Color.Black));

            //Show the tile engine
            Controls.Add(new Button(delegate()
            {
                Level.TileEdit = !Level.TileEdit;
                if (Level.TileEdit == false)
                {
                    Controls[1].ChangeColor(new Color(150, 150, 150));
                    HideButtons();
                }
                if (Level.TileEdit == true)
                {
                    Controls[1].ChangeColor(Color.White);
                    ShowButtons();
                }
            }, SetSize(0, 0, 16, 15), LoadAssets.TileIcon, new Color(150, 150, 150), null, false));

            //Increase number of tile columns
            Controls.Add(new Button(delegate() { Level.TileEngine.AddRemoveTiles(1, 0); }, SetSize(21, 40, 16, 15), LoadAssets.PlusIcon, Color.White, null, false, true));

            //Decrease number of tile columns
            Controls.Add(new Button(delegate() { if (Level.TileEngine.NumTilesX > (Main.ScreenSize.X / TileEngine.TileSize)) Level.TileEngine.AddRemoveTiles(-1, 0); }, SetSize(3, 40, 16, 15), LoadAssets.MinusIcon, Color.White, null, false, true));

            //Increase number of tile rows
            Controls.Add(new Button(delegate() { Level.TileEngine.AddRemoveTiles(0, 1); }, SetSize(12, 56, 16, 15), LoadAssets.PlusIcon, Color.White, null, false, true));

            //Decrease number of tile rows
            Controls.Add(new Button(delegate() { if (Level.TileEngine.NumTilesY > (Main.ScreenSize.Y / TileEngine.TileSize)) Level.TileEngine.AddRemoveTiles(0, -1); }, SetSize(12, 24, 16, 15), LoadAssets.MinusIcon, Color.White, null, false, true));

            //A button for the type of information to show on the tile engine
            Controls.Add(new Button(delegate()
            {
                if (Level.TileInfo == true) Level.TileInfo = false;
                else if (Level.TileInfo == false) Level.TileInfo = null;
                else Level.TileInfo = true;
            }, SetSize(24, 0, 16, 15), null, Color.White, null, false, true));

            //Tile offset X
            Controls.Add(new TextBox(delegate(TextBox textbox)
            {
                Vector2 oldtileoffset = Level.TileEngine.GetTileOffSet();
                textbox.CorrectEmptyTextValue("0");
                int newtileoffset = EditorHelper.TextToNumber((String)textbox.GetControlValue);
                if (newtileoffset > 9999)
                {
                    newtileoffset = 9999;
                    textbox.StringUpdate(textbox.GetControlText.Substring(0, textbox.GetControlText.Length - 1));
                }

                Level.TileEngine.UpdateTileOffSet(newtileoffset - (int)oldtileoffset.X, 0);
            }, SetSize(60, -5, 80, 20), null, new Vector2(0, 12), true, "Tile Offset X: \n" + (int)Level.TileEngine.GetTileOffSet().X, 16, 5));

            //Tile offset Y
            Controls.Add(new TextBox(delegate(TextBox textbox)
            {
                Vector2 oldtileoffset = Level.TileEngine.GetTileOffSet();
                textbox.CorrectEmptyTextValue("0");
                int newtileoffset = EditorHelper.TextToNumber((String)textbox.GetControlValue);
                if (newtileoffset > 9999)
                {
                    newtileoffset = 9999;
                    textbox.StringUpdate(textbox.GetControlText.Substring(0, textbox.GetControlText.Length - 1));
                }

                Level.TileEngine.UpdateTileOffSet(0, newtileoffset - (int)oldtileoffset.Y);
            }, SetSize(60, 40, 80, 20), null, new Vector2(0, 12), true, "Tile Offset Y: \n" + (int)Level.TileEngine.GetTileOffSet().Y, 16, 5));
        }

        //Hides buttons when tile editing is disabled
        private void HideButtons()
        {
            for (int i = 2; i < Controls.Count; i++)
            {
                Controls[i].Hide();
            }
        }

        //Reveals buttons when tile editing is enabled
        private void ShowButtons()
        {
            for (int i = 2; i < Controls.Count; i++)
            {
                Controls[i].Show();
            }

            Controls[7].StringUpdate("Tile Offset X: \n" + (int)Level.TileEngine.GetTileOffSet().X);
            Controls[8].StringUpdate("Tile Offset Y: \n" + (int)Level.TileEngine.GetTileOffSet().Y);
        }

        //Updates the displayed tile offset after a level has been loaded
        public void UpdateTileOffSet()
        {
            Controls[7].StringUpdate("Tile Offset X: \n" + (int)Level.TileEngine.GetTileOffSet().X);
            Controls[8].StringUpdate("Tile Offset Y: \n" + (int)Level.TileEngine.GetTileOffSet().Y);
        }
    }
}
