﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for creating containers to place on the level
    public class ContainerCreateMenu : EditingMenu
    {
        private SublevelProp Level;

        public ContainerCreateMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = CreateMenuLocation;
            Level = level;

            //Create buttons that show the container icons on them so it's easy to know which button corresponds to which container
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new ContainerMenu(new ContainerSpawn(CreateObjects.Barrel(Vector3.Zero, 1, null, null), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.BarrelIcon, Color.White, null, false));
        }
    }
}
