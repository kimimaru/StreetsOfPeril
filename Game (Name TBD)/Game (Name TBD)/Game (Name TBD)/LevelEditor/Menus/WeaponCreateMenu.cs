﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The menu for creating weapons to place on the level
    public sealed class WeaponCreateMenu : EditingMenu
    {
        private SublevelProp Level;

        public WeaponCreateMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = CreateMenuLocation;
            Level = level;

            //Create buttons that show the weapon icons on them so it's easy to know which button corresponds to which weapon
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new KnifeMenu(new WeaponSpawn(CreateObjects.Knife(new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(0, 0, 16, 15), LoadGraphics.WeaponIcons[(int)Weapon.WeaponTypes.Stab], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new KatanaMenu(new WeaponSpawn(CreateObjects.Katana(new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(24, 0, 16, 15), LoadGraphics.WeaponIcons[(int)Weapon.WeaponTypes.Swing], Color.White, null, false));
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new SwiftBladeMenu(new WeaponSpawn(CreateObjects.SwiftBlade(new Status()), Level.LevelSpawns[Level.LevelSpawns.Count - 1]), false)); }, SetSize(48, 0, 16, 15), LoadGraphics.WeaponIcons[(int)Weapon.WeaponTypes.Swing], Color.White, null, false));
        }
    }
}
