﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //An editing menu for editing the level (placing spawns, stoppoints, changing offset, etc.)
    public class LevelMenu : EditingMenu
    {
        //The level reference for accessing and changing its information
        private SublevelProp Level;

        //The tile menu; it's separated to avoid clutter in this class
        private LevelTileMenu TileMenu;

        public LevelMenu(MainProgram main, SublevelProp level)
        {
            MenuLocation = new Vector2(20, 370);

            Level = level;

            //Create stop point
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new StopPointMenu(new StopPoint(), false)); }, SetSize(0, 0, 16, 20), LoadAssets.StopPointIcon, Color.White, null, false));

            //Create a spawn
            Controls.Add(new Button(delegate() { main.SetSelectedObject(new SpawnMenu(new Spawn(Vector3.Zero, Vector2.Zero, 0, level.StopPoints.Count - 1, 0, 1), false)); }, SetSize(72, 0, 16, 20), LoadAssets.SpawnIcon, Color.White, null, false));

            //OffSet buttons
            Controls.Add(new Button(delegate() { Level.OffSet.X -= 1; UpdateOffSetString(); }, SetSize(0, 48, 16, 20), null, Color.White, null, true));
            Controls.Add(new Button(delegate() { Level.OffSet.X += 1; UpdateOffSetString(); }, SetSize(48, 48, 16, 20), null, Color.White, null, true));
            Controls.Add(new Button(delegate() { Level.OffSet.Y -= 1; UpdateOffSetString(); }, SetSize(24, 24, 16, 20), null, Color.White, null, true));
            Controls.Add(new Button(delegate() { Level.OffSet.Y += 1; UpdateOffSetString(); }, SetSize(24, 72, 16, 20), null, Color.White, null, true));

            //Current offset - X
            Controls.Add(new TextBox(delegate(TextBox textbox)
            {
                textbox.CorrectEmptyTextValue(((int)Level.OffSet.X).ToString());
                Level.OffSet.X = EditorHelper.TextToNumber((String)Controls[Controls.Count - 2].GetControlValue);
                UpdateOffSetString();
            }, SetSize(72, 48, 55, 20), null, new Vector2(0, 10), "Cur Offset:\n" + ((int)Level.OffSet.X).ToString(), 12, 6));

            //Current offset - Y
            Controls.Add(new TextBox(delegate(TextBox textbox)
            {
                textbox.CorrectEmptyTextValue(((int)Level.OffSet.Y).ToString());
                Level.OffSet.Y = EditorHelper.TextToNumber((String)Controls[Controls.Count - 1].GetControlValue);
                UpdateOffSetString();
            }, SetSize(72, 84, 55, 20), null, new Vector2(0, 0), ((int)Level.OffSet.Y).ToString(), 0, 6));

            //Level tile menu (helps for separation)
            TileMenu = new LevelTileMenu(Level);
        }

        private void UpdateOffSetString()
        {
            Controls[Controls.Count - 2].StringUpdate("Cur Offset:\n" + ((int)Level.OffSet.X).ToString());
            Controls[Controls.Count - 1].StringUpdate(((int)Level.OffSet.Y).ToString());
        }

        private void KeyboardInput(KeyboardState keyboard)
        {
            if (Input.KeyHeld(Keys.Up) == true)
            {
                Level.OffSet.Y -= 1;
                UpdateOffSetString();
            }
            if (Input.KeyHeld(Keys.Down) == true)
            {
                Level.OffSet.Y += 1;
                UpdateOffSetString();
            }
            if (Input.KeyHeld(Keys.Left) == true)
            {
                Level.OffSet.X -= 1;
                UpdateOffSetString();
            }
            if (Input.KeyHeld(Keys.Right) == true)
            {
                Level.OffSet.X += 1;
                UpdateOffSetString();
            }
        }
        
        //Updates the displayed tile offset after a level has been loaded
        public void UpdateTileOffSet()
        {
            TileMenu.UpdateTileOffSet();
        }

        public override void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            base.Update(activeTime, mouse, keyboard, main);

            //Check for changing the offset with the arrow keys
            KeyboardInput(keyboard);

            TileMenu.Update(activeTime, mouse, keyboard, main);
        }

        protected override void DrawOther(SpriteBatch spriteBatch)
        {
            TileMenu.Draw(spriteBatch);
        }
    }
}
