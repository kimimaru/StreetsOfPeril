﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A menu that shows general object information; this only appears if an object is selected
    public class GeneralMenu : EditingMenu
    {
        //The object reference for changing its values
        public SpawnObject SelectedObject;

        public GeneralMenu(SpawnObject selectedobject)
        {
            MenuLocation = new Vector2(680, 20);
            SelectedObject = selectedobject;

            //Top to bottom: Object type, spawn number associated with it, spawn offset, index, designated, and SpawnOffSet enterable or not
            Controls.Add(new Label(SetLocation(-20, -12), "General Properties", Color.Black));
            Controls.Add(new Label(SetLocation(0, 12), "Object: " + SelectedObject.GetObject.GetType().Name, Color.Black));
            Controls.Add(new TextBox(SetSize(0, 20, 30, 20), true, new Vector2(50, 0), "Spawn: " + SelectedObject.SpawnAssociated.SpawnNumber, 7, 2));
            Controls.Add(SpawnOffSetLabel);
            Controls.Add(new TextBox(SetSize(0, 80, 45, 20), null, new Vector2(48, 1), "Index: " + SelectedObject.Index, 7, 3));
            Controls.Add(new DropDown(SetSize(0, 100, 40, 20), "Required:", Convert.ToInt32(SelectedObject.Designated), new Vector2(75, 0), "False", "True"));
            Controls.Add(new DropDown(delegate()
                {
                    if ((int)Controls[6].GetControlValue == 0) Controls[3] = SpawnOffSetLabel;
                    else Controls[3] = SpawnOffSetTextBox;
                }, SetSize(0, 120, 50, 20), "S. Offset:", 0, new Vector2(65, 0), "Mouse", "Text"));
        }

        //The Label version of the object's SpawnOffSet
        private Label SpawnOffSetLabel
        {
            get { return new Label(SetLocation(0, 63), "Spawn Offset: \n" + EditorHelper.ConvertVector3String(SelectedObject.SpawnOffSet), Color.Black); }
        }

        //The Textbox version of the object's SpawnOffSet
        private TextBox SpawnOffSetTextBox
        {
            get 
            {
                Vector2 spawnoffset = new Vector2(SelectedObject.SpawnOffSet.X, SelectedObject.SpawnOffSet.Y);
                return new TextBox(SetSize(0, 56, 60, 15), null, new Vector2(0, 10), "Spawn Offset: \n" + EditorHelper.ConvertVector2String(spawnoffset), 15, 9);
            }
        }

        //Gets the value of the SpawnOffSet when it is entered via textbox
        public Vector2 GetSpawnOffSet()
        {
            return EditorHelper.ParseVector2((String)Controls[3].GetControlValue);
        }

        public bool IsSpawnOffSetText()
        {
            return ((int)Controls[6].GetControlValue == 1);
        }

        private void ChangeSpawns(SublevelProp level, int prevspawn, int newspawn)
        {
            //Set the new spawn
            SelectedObject.SpawnAssociated = level.LevelSpawns[newspawn];

            //Find the type of the selected object
            EnemySpawn enemy = SelectedObject as EnemySpawn;
            bool enemcontains = level.LevelSpawns[prevspawn].SpawnEnemies.Contains(enemy);

            if (enemy != null)
            {
                if (enemcontains == true)
                {
                    level.LevelSpawns[prevspawn].RemoveEnemy(enemy);
                    SelectedObject.SpawnAssociated.AddEnemy(enemy);
                }
                return;
            }

            ContainerSpawn container = SelectedObject as ContainerSpawn;
            bool contcontains = level.LevelSpawns[prevspawn].SpawnContainers.Contains(container);

            if (container != null) 
            {
                if (contcontains == true)
                {
                    level.LevelSpawns[prevspawn].RemoveContainer(container);
                    SelectedObject.SpawnAssociated.AddContainer(container);
                }
                return;
            }

            HazardSpawn hazard = SelectedObject as HazardSpawn;
            bool hazardcontains = level.LevelSpawns[prevspawn].SpawnHazards.Contains(hazard);

            if (hazard != null)
            {
                if (hazardcontains == true)
                {
                    level.LevelSpawns[prevspawn].RemoveHazard(hazard);
                    SelectedObject.SpawnAssociated.AddHazard(hazard);
                }
                return;
            }

            ItemSpawn item = SelectedObject as ItemSpawn;
            bool itemcontains = level.LevelSpawns[prevspawn].SpawnItems.Contains(item);

            if (item != null)
            {
                if (itemcontains == true)
                {
                    level.LevelSpawns[prevspawn].RemoveItem(item);
                    SelectedObject.SpawnAssociated.AddItem(item);
                }
                return;
            }

            WeaponSpawn weapon = SelectedObject as WeaponSpawn;
            bool weaponcontains = level.LevelSpawns[prevspawn].SpawnWeapons.Contains(weapon);

            if (weapon != null)
            {
                if (weaponcontains == true)
                {
                    level.LevelSpawns[prevspawn].RemoveWeapon(weapon);
                    SelectedObject.SpawnAssociated.AddWeapon(weapon);
                }
            }
        }

        public override void Parse(SublevelProp level)
        {
            //The previous spawn
            int prevspawn = SelectedObject.SpawnAssociated.SpawnNumber;

            int spawnnum = EditorHelper.TextToNumber((String)Controls[2].GetControlValue);
            if (spawnnum < 0 || spawnnum >= level.LevelSpawns.Count) spawnnum = level.LevelSpawns.Count - 1;

            //Change spawns if it's now on a new spawn
            if (prevspawn != spawnnum)
            {
                ChangeSpawns(level, prevspawn, spawnnum);
            }

            //SelectedObject.SpawnAssociated = level.LevelSpawns[spawnnum];
            SelectedObject.Index = EditorHelper.TextToNumber((String)Controls[4].GetControlValue);
            SelectedObject.Designated = Convert.ToBoolean(Controls[5].GetControlValue);
        }
    }
}
