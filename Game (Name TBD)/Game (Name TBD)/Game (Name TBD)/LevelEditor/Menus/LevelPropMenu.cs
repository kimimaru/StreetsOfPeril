﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game__Name_TBD_
{
    //A menu that shows properties of the level
    //USER EDITED
    public class LevelPropMenu : EditingMenu
    {
        //Level reference
        private SublevelProp Level;

        public LevelPropMenu(SublevelProp level, MainProgram main)
        {
            MenuLocation = new Vector2(440, 20);

            Level = level;
            Controls.Add(new Label(SetLocation(-10, -10), "Level Properties", Color.Black));
            Controls.Add(new TextBox(delegate(TextBox textbox) 
            {
                textbox.CorrectEmptyTextValue("Untitled");
                Level.LevelName = (String)textbox.GetControlValue;
            }, SetSize(0, 0, 70, 20), null, new Vector2(95, 0), "Level Name: " + level.LevelName, 12, 30));
            Controls.Add(new DropDown(delegate() { Level.LevelEntrance = (int)Controls[2].GetControlValue; }, SetSize(0, 20, 50, 20), "Level Entrance:", Level.LevelEntrance, new Vector2(115, 0), "None", "Right", "Left", "Up", "Down"));
            Controls.Add(new DropDown(delegate() { Level.LevelExit = (int)Controls[3].GetControlValue; }, SetSize(0, 40, 50, 20), "Level Exit:", Level.LevelExit, new Vector2(80, 0), "None", "Right", "Left", "Up", "Down"));
            Controls.Add(new TextBox(delegate(TextBox textbox) { Level.StartingOffset = EditorHelper.ParseVector2((String)Controls[4].GetControlValue); }, SetSize(0, 60, 80, 20), null, new Vector2(65, 0), "S. Offset: " + EditorHelper.ConvertVector2String(Level.StartingOffset), 11, 9));
            Controls.Add(new DropDown(delegate() { Level.CameraType = Convert.ToBoolean((int)Controls[5].GetControlValue); }, SetSize(0, 80, 60, 20), "Camera Type:", Convert.ToInt32(Level.CameraType), new Vector2(105, 0), "Normal", "Limited"));
            Controls.Add(new TextBox(delegate(TextBox textbox) { Level.CameraRange = EditorHelper.ParseVector2((String)Controls[6].GetControlValue); }, SetSize(0, 102, 50, 15), null, new Vector2(50, 0), "Range: " + EditorHelper.ConvertVector2String(Level.CameraRange), 7, 7));

            //Set whether a level is underwater or not; if set to true, this will set each tile's TypeHeight to 999 and TileType to Water in-game (the values in the editor will be unaffected)
            Controls.Add(new DropDown(delegate() { Level.Underwater = Convert.ToBoolean(Controls[7].GetControlValue); }, SetSize(0, 120, 50, 20), "Underwater:", Convert.ToInt32(Level.Underwater), new Vector2(95, 0), "No", "Yes"));

            //Whether the level has parallax scrolling or not for the background
            Controls.Add(new DropDown(delegate() { Level.Parallax = Convert.ToBoolean(Controls[8].GetControlValue); }, SetSize(0, 140, 50, 20), "Parallax:", Convert.ToInt32(Level.Parallax), new Vector2(67, 0), "No", "Yes"));

            //The music for the level

            //The level background; each one may come with accompanying foreground

            //Save and Load Buttons
            Controls.Add(new Button(main.SaveLevel, new Rectangle(350, 420, 40, 40), null, Color.Red, "Save", false));
            Controls.Add(new Button(main.LoadLevel, new Rectangle(450, 420, 40, 40), null, Color.Blue, "Load", false));

            //"Compile" button; checks for errors with the level
            Controls.Add(new Button(main.CheckLevelLoad, new Rectangle(395, 420, 50, 40), null, Color.GreenYellow, "Check\nErrors", false));

            //The error
            Controls.Add(new Label(SetLocation(0, 160), "Error: No errors!", Color.Red, true));
        }

        public void DisplayMessage(String message)
        {
            if (message != null)
            {
                Controls[Controls.Count - 1].StringUpdate("Error: " + message);
            }
        }

        public override void UpdateControls()
        {
            Controls[1].StringUpdate("Level Name: " + Level.LevelName);
            (Controls[2] as DropDown).ChangeSelectedOption(Level.LevelEntrance);
            (Controls[3] as DropDown).ChangeSelectedOption(Level.LevelExit);
            Controls[4].StringUpdate("S. Offset: " + EditorHelper.ConvertVector2String(Level.StartingOffset));
            (Controls[5] as DropDown).ChangeSelectedOption(Convert.ToInt32(Level.CameraType));
            Controls[6].StringUpdate("Range: " + EditorHelper.ConvertVector2String(Level.CameraRange));
            (Controls[7] as DropDown).ChangeSelectedOption(Convert.ToInt32(Level.Underwater));
            (Controls[8] as DropDown).ChangeSelectedOption(Convert.ToInt32(Level.Parallax));
        }
    }
}
