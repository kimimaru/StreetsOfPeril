﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //An editing menu - there may be separate menus for editing enemies, containers, etc.
    public abstract class EditingMenu
    {
        public const float MenuDepth = .9981f;

        //The location of the menu
        protected Vector2 MenuLocation;

        //Collapsed icon
        protected Texture2D CollapsedIcon;

        //Tells if the menu is collapsed or not
        protected bool Collapsed;

        //The controls on the menu
        protected List<Control> Controls;

        public EditingMenu()
        {
            MenuLocation = Vector2.Zero;
            Collapsed = false;

            Controls = new List<Control>();
        }

        //The location for the various object menus
        public static Vector2 ObjectMenuLocation
        {
            get { return new Vector2(680, 225); }
        }

        //The location for the various menus where you select certain types of objects to place on the level
        public static Vector2 CreateMenuLocation
        {
            get { return new Vector2(20, 370); }
        }

        //Checks if any of the controls on the menu are focused
        public virtual bool IsFocused()
        {
            for (int i = 0; i < Controls.Count; i++)
            {
                if (Controls[i].IsFocused == true) return true;
            }

            return false;
        }

        //Called inside Parse() - it ensures that the input it's parsing is correct and corrects it if not
        protected virtual void HandleInputErrors(List<String> values)
        {

        }

        //Makes changes such as creating or changing an object and more
        //When adding, pass in a new object then parse and add to list
        //When editing, pass in the object you edit then parse to change it
        //Object menus should decide whether to have a general menu or not
        //Object menus have their own virtual AddToList method; each menu knows what type its object is, so it knows which list to add it to
        public virtual void Parse(SublevelProp level)
        {

        }

        protected Vector2 SetLocation(int xoffset, int yoffset)
        {
            return (new Vector2((int)MenuLocation.X + xoffset, (int)MenuLocation.Y + yoffset));
        }

        protected Rectangle SetSize(int xoffset, int yoffset, int width, int height)
        {
            return (new Rectangle((int)MenuLocation.X + xoffset, (int)MenuLocation.Y + yoffset, width, height));
        }

        public virtual void UpdateControls()
        {

        }

        public virtual void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            //Ensure that you can click on only one control at a time
            bool clicked = false;

            //Go through all the controls and see if any were clicked
            //NOTE: You may want to make this more efficient later the more controls you add
            for (int i = 0; i < Controls.Count; i++)
            {
                Controls[i].Update(activeTime, mouse, keyboard, main);

                //Check if the mouse clicked
                if (Controls[i].IsHidden == false && Controls[i].CheckMouseClick(mouse) == true)
                {
                    //Check if the mouse was on the control when it clicked; if so, do something
                    if (clicked == false && Controls[i].CheckClicked(mouse) == true)
                    {
                        Controls[i].Click();
                        clicked = true;
                    }
                    //Otherwise, unselect the control or do another action if need be
                    else if (Controls[i].IsFocused == true) Controls[i].UnClick();
                }
            }
        }

        //Draws anything else on the editing menu that may need to be drawn
        protected virtual void DrawOther(SpriteBatch spriteBatch)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            //Draw the menu if its not collapsed
            if (Collapsed == false)
            {
                //Draw all the controls
                for (int i = 0; i < Controls.Count; i++)
                {
                    if (Controls[i].IsHidden == false)
                        Controls[i].Draw(spriteBatch);
                }
            }
            //Draw the collapsed icon
            else if (CollapsedIcon != null)
            {
                spriteBatch.Draw(CollapsedIcon, MenuLocation, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, MenuDepth);
            }

            //Draws anything else that needs to be drawn
            DrawOther(spriteBatch);
        }
    }
}
