using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //The main program of the Level Editor; since the Level Editor depends on the game's libraries, it will likely need to be put in the same folder as the game to work
    //All classes that can be edited by the user through the level editor will be signified with a "USER EDITED" comment above the class definition
    //NOTE: Perhaps show what a specific control does when hovering over it by displaying hover text in a specific part of the screen (top-right corner?)
    public sealed class MainProgram : Microsoft.Xna.Framework.Game
    {
        //Tells if the Level Editor is being run or not
        private static bool IsInLevelEditor;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        //The mouse and keyboard states
        private MouseState mouse;
        private KeyboardState keyboard;

        //The menus
        private LevelPropMenu LevelPropMenu;
        private ObjectMenu ObjectMenu;
        private TabMenu TabMenu;

        //The level to edit; it's in its own window
        private SublevelProp Level;
        private Vector2 LevelPosition;

        public MainProgram()
        {
            //If this is instantiated, we're in the level editor
            IsInLevelEditor = true;

            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
            IsMouseVisible = true;

            ObjectMenu = null;

            Content.RootDirectory = "SoPLibraryContent";
        }

        static MainProgram()
        {
            IsInLevelEditor = false;
        }

        public static bool InLevelEditor
        {
            get { return IsInLevelEditor; }
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            //Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Load all the content
            LoadGraphics.LoadContent(Content);
            LoadAssets.LoadContent(Content);

            //Create a new level
            Level = new SublevelProp();
            LevelPosition = Vector2.Zero;

            mouse = new MouseState();
            keyboard = new KeyboardState();

            LevelPropMenu = new LevelPropMenu(Level, this);
            TabMenu = new TabMenu(Level, new LevelMenu(this, Level), new EnemyCreateMenu(this, Level), new ContainerCreateMenu(this, Level), new HazardCreateMenu(this, Level), new ItemCreateMenu(this, Level), new WeaponCreateMenu(this, Level));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public static Vector2 ScreenSize
        {
            get { return new Vector2(800, 480); }
        }

        public static Vector2 LevelScreenSize
        {
            get { return new Vector2(416, 320); }
        }

        //Checks if you're selecting an object in the editor
        public bool SelectingObject
        {
            get { return ObjectMenu != null; }
        }

        //Sets the selected object and creates menus relating to the type of object selected
        public void SetSelectedObject(ObjectMenu objectmenu)
        {
            ObjectMenu = objectmenu;
        }

        //Deselects the selected object and deletes the menu associated with it
        public void DeselectObject()
        {
            ObjectMenu = null;
        }

        //Saves the level
        public void SaveLevel()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(SublevelProp));

            SaveFileDialog file = new SaveFileDialog();

            file.Filter = "SOP File | *.SOP";

            if (file.ShowDialog() == DialogResult.OK)
            {
                using (FileStream stream = new FileStream(file.FileName, FileMode.Create))
                {
                    serializer.WriteObject(stream, Level);
                }

                CheckLevelLoad();
            }
        }

        //Loads a level
        public void LoadLevel()
        {
            SublevelProp level = LoadLevel(null);

            if (level != null)
            {
                Level.LevelName = level.LevelName;
                Level.LevelEntrance = level.LevelEntrance;
                Level.LevelExit = level.LevelExit;
                Level.StopPoints = level.StopPoints;
                Level.StartingLocations = level.StartingLocations;
                Level.StartingOffset = level.StartingOffset;
                Level.LevelSpawns = level.LevelSpawns;
                Level.TileEngine = level.TileEngine;
                Level.CameraType = level.CameraType;
                Level.CameraRange = level.CameraRange;
                Level.Underwater = level.Underwater;
                Level.LevelBackground = level.LevelBackground;
                Level.LevelMusic = level.LevelMusic;

                LevelPropMenu.UpdateControls();
                TabMenu.UpdateTileOffSet();
            }
        }

        public static SublevelProp LoadLevel(String filename)
        {
            SublevelProp level = new SublevelProp(LoadGraphics.BG);

            DataContractSerializer serializer = new DataContractSerializer(typeof(SublevelProp));

            OpenFileDialog file = null;

            if (filename == null)
            {
                file = new OpenFileDialog();
                file.RestoreDirectory = true;
                //file.InitialDirectory = "bin";
                file.Filter = "SOP File | *.SOP";
            }

            if (file == null || file.ShowDialog() == DialogResult.OK)
            {
                if (file != null) filename = file.FileName;

                if (File.Exists(filename) == true)
                {
                    using (FileStream stream = new FileStream(filename, FileMode.Open))
                    {
                        /*Handle errors serializing; if a level cannot be loaded, don't load it and send the player either to the next level (if it exists) or back
                          to the previous screen - loading should occur before the player is sent to the level*/
                        SublevelProp templevel = null;
                        try
                        {
                            templevel = (SublevelProp)serializer.ReadObject(stream);
                        }
                        catch (Exception e)
                        {
                            Debug.OutputValue("Could not load level! Error: " + e.Message);
                            return null;
                        }

                        level.LevelName = templevel.LevelName;
                        level.LevelEntrance = templevel.LevelEntrance;
                        level.LevelExit = templevel.LevelExit;
                        level.TileEngine = templevel.TileEngine;
                        level.LevelSpawns = templevel.LevelSpawns;
                        level.StopPoints = templevel.StopPoints;
                        level.CameraType = templevel.CameraType;
                        level.CameraRange = templevel.CameraRange;
                        level.Underwater = templevel.Underwater;
                        level.LevelBackground = templevel.LevelBackground;
                        level.LevelMusic = templevel.LevelMusic;

                        //Initialize starting locations
                        for (int i = 0; i < templevel.StartingLocations.Length; i++)
                        {
                            level.StartingLocations[i] = new PlayerSpawn(i, level.TileEngine);
                            level.StartingLocations[i].Location = templevel.StartingLocations[i].Location;
                        }

                        level.StartingOffset = templevel.StartingOffset;

                        //Load all spawn objects
                        for (int i = 0; i < level.LevelSpawns.Count; i++)
                        {
                            level.LevelSpawns[i].LoadObjects();
                        }
                    }
                }
                else
                {
                    Debug.OutputValue("File for level: " + filename + " cannot be found!");
                }
            }

            return level;
        }

        //Checks whether the level will load or not and displays an error if there is one
        public void CheckLevelLoad()
        {
            if (Level.StopPoints.Count == 0)
            {
                LevelPropMenu.DisplayMessage("No StopPoints! You need\nat least one in each level!");
            }
            else if (Level.LevelSpawns.Count == 0)
            {
                LevelPropMenu.DisplayMessage("No Spawns! The\nlevel will not spawn any objects!");
            }
            else if (Level.CameraType == true && Level.CameraRange == Vector2.Zero)
            {
                LevelPropMenu.DisplayMessage("Camera type is\nLimited, but it has no Range!");
            }
            else if (Level.CheckStopPointsInBounds() == false)
            {
                LevelPropMenu.DisplayMessage("A StopPoint is impossible to reach!\nIt may be out of bounds!");
            }
            else if (Level.CheckSpawnsLoadable() == false)
            {
                LevelPropMenu.DisplayMessage("A spawn object is invalid;\nsomething may be out of bounds!\nThe level may error when\nthat object spawns!");
            }
            else if (Level.CheckPlayersInBounds() == false)
            {
                LevelPropMenu.DisplayMessage("A player is out of bounds!");
            }
            else
            {
                LevelPropMenu.DisplayMessage("No errors!");
            }
        }

        //Displays an error message
        public void DisplayErrorMessage(String message)
        {
            LevelPropMenu.DisplayMessage(message);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //Update game time; we'll use Main's activeTime
            Main.LevelEditorUpdate(gameTime);

            //Only allow the level to be changed when the editor is active
            if (this.IsActive == true)
            {
                //Check if you're selecting an object
                if (SelectingObject == true)
                {
                    ObjectMenu.Update(Main.GetActiveTime, mouse, keyboard, this);
                    if (EditorHelper.RightClicked(mouse) == true && ObjectMenu.IsFocused() == false)
                    {
                        DeselectObject();
                    }
                    else if (EditorHelper.LeftClicked(mouse) == true && EditorHelper.ClickedRect(Level.LevelRect(LevelPosition)) == true)
                    {
                        //Save the object
                        ObjectMenu.Parse(Level);

                        //Add a new object to the level if the object in the menu is a new one
                        if (ObjectMenu.IsEditingObject == false)
                        {
                            ObjectMenu.AddToLevel(Level);
                        }
                        //Reorder the object list when you place an object
                        else ObjectMenu.ChangeObjectIndex();

                        DeselectObject();
                    }
                }
                //If you're not selecting an object, check for clicking on other menus or selecting one
                else
                {
                    //LevelPropMenu.Update(activeTime, mouse, keyboard, this);
                    TabMenu.Update(Main.GetActiveTime, mouse, keyboard, this);

                    //Check for clicking on a level object
                    Level.CheckClickObject(mouse, keyboard, this);
                }

                LevelPropMenu.Update(Main.GetActiveTime, mouse, keyboard, this);
            }

            //Update level
            Level.Update(Main.GetActiveTime, mouse, keyboard, this);

            //Update debug commands
            Debug.Update();

            //Update inputs
            mouse = Mouse.GetState();
            keyboard = Keyboard.GetState();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Scale the graphics to fit with different resolutions
            //Matrix ScalingMatrix = Matrix.CreateScale(new Vector3(/*(int)*/((float)graphics.GraphicsDevice.PresentationParameters.BackBufferWidth / (float)416), /*(int)*/((float)graphics.GraphicsDevice.PresentationParameters.BackBufferHeight / (float)320), 1));

            //Begin drawing
            spriteBatch.Begin(SpriteSortMode.FrontToBack, null, SamplerState.PointClamp, null, null, null);// ScalingMatrix);

            Level.Draw(spriteBatch, LevelPosition);

            LevelPropMenu.Draw(spriteBatch);

            if (SelectingObject == true)
            {
                ObjectMenu.Draw(spriteBatch);
            }

            TabMenu.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
