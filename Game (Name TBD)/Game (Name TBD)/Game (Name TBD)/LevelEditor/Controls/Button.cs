﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A rectangular button that does some action in the level editor
    public class Button : Control
    {
        public const float ButtonDepth = .9983f;

        //Method pointer; what the button does when clicked
        public delegate void ButtonPress();

        //The method to be invoked when the button is pressed
        private ButtonPress Pressed;

        //The graphic for the button
        private Texture2D ButtonGraphic;

        //Constructor
        public Button(Rectangle size, Texture2D graphic, Color color, String text, bool canhold)
        {
            Bounds = size;
            ButtonGraphic = graphic;
            ControlColor = color;
            ControlText = text;

            CanHold = canhold;
        }

        public Button(ButtonPress pressed, Rectangle size, Texture2D graphic, Color color, String text, bool canhold) : this(size, graphic, color, text, canhold)
        {
            Pressed = pressed;
        }

        public Button(ButtonPress pressed, Rectangle size, Texture2D graphic, Color color, String text, bool canhold, bool hidden) : this(pressed, size, graphic, color, text, canhold)
        {
            Hidden = hidden;
        }

        //Press the button
        public override void Click()
        {
            if (Pressed != null) Pressed();
        }

        //Changes the graphic of the button; make sure the new graphic is the same size as the previous graphic because the size of the button won't change
        public void ChangeGraphic(Texture2D newgraphic)
        {
            ButtonGraphic = newgraphic;
        }

        //Draws the button
        public override void Draw(SpriteBatch spriteBatch)
        {
            //Draw the button graphic
            if (ButtonGraphic != null)
                spriteBatch.Draw(ButtonGraphic, new Vector2(Bounds.X, Bounds.Y), null, ControlColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, ButtonDepth);
            //If there's no graphic, draw the default button graphic
            else spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(Bounds.X, Bounds.Y), null, ControlColor, 0f, Vector2.Zero, new Vector2(Bounds.Width, Bounds.Height), SpriteEffects.None, ButtonDepth);
            
            //Draw an outline around the button
            spriteBatch.Draw(LoadAssets.ButtonDefault, new Vector2(Bounds.X - 1, Bounds.Y - 1), null, Color.Black, 0f, Vector2.Zero, new Vector2(Bounds.Width + 2, Bounds.Height + 2), SpriteEffects.None, ButtonDepth - .0001f);

            //Only draw the button's text if there is text to draw (it should so the user knows what it does)
            if (String.IsNullOrEmpty(ControlText) == false)
                spriteBatch.DrawString(LoadAssets.EditorFont, ControlText, ControlCenter, Color.Black, 0f, TextOrigin, 1f, SpriteEffects.None, ButtonDepth + .0001f);
        }
    }
}
