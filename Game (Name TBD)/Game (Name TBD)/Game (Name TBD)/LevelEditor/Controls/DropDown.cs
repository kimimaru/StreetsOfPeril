﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A drop down menu with several options
    public class DropDown : Control
    {
        //Method pointer; what the drop down does when it loses focus
        public delegate void LoseFocus();

        //The method to be invoked when the drop down loses focus
        private LoseFocus LostFocus;

        //The text describing what the drop down is for
        private String DescriptionText;

        //The options
        protected String[] Options;

        //The selected option
        protected int SelectedOption;

        public DropDown()
        {
            SelectedOption = 0;
            LostFocus = null;

            DescriptionText = String.Empty;
        }

        public DropDown(Rectangle size, params String[] options) : this()
        {
            Bounds = size;

            Options = options;
        }

        public DropDown(LoseFocus losefocus, Rectangle size, params String[] options) : this(size, options)
        {
            LostFocus = losefocus;
        }

        //Starts the drop down at a specific option
        public DropDown(Rectangle size, int selectedoption, params String[] options) : this(size, options)
        {
            SelectedOption = selectedoption;
        }

        public DropDown(Rectangle size, String descriptiontext, int selectedoption, Vector2 boundoffset, params String[] options) : this(size, selectedoption, options)
        {
            DescriptionText = descriptiontext;
            BoundOffSet = boundoffset;
        }

        public DropDown(LoseFocus losefocus, Rectangle size, int selectedoption, params String[] options) : this(size, selectedoption, options)
        {
            LostFocus = losefocus;
        }

        public DropDown(LoseFocus losefocus, Rectangle size, String descriptiontext, int selectedoption, params String[] options) : this (losefocus, size, selectedoption, options)
        {
            DescriptionText = descriptiontext;
            BoundOffSet = new Vector2(DescriptionText.Length * 8, 0f);
        }

        public DropDown(Rectangle size, Vector2 boundoffset, params String[] options) : this(size, options)
        {
            BoundOffSet = boundoffset;
        }

        public DropDown(Rectangle size, int selectedoption, Vector2 boundoffset, params String[] options) : this(size, selectedoption, options)
        {
            BoundOffSet = boundoffset;
        }

        public DropDown(LoseFocus losefocus, Rectangle size, int selectedoption, Vector2 boundoffset, params String[] options) : this(size, selectedoption, boundoffset, options)
        {
            LostFocus = losefocus;
        }

        public DropDown(LoseFocus losefocus, Rectangle size, String descriptiontext, int selectedoption, Vector2 boundoffset, params String[] options) : this(losefocus, size, selectedoption, boundoffset, options)
        {
            DescriptionText = descriptiontext;
        }

        public override String GetControlText
        {
            get { return Options[SelectedOption] ?? String.Empty; }
        }

        public override object GetControlValue
        {
            get { return SelectedOption; }
        }

        public void ChangeSelectedOption(int option)
        {
            if (EditorHelper.IndexWithinArray<String>(Options, option) == true)
            {
                SelectedOption = option;
            }
        }

        //Checks if the drop down is empty
        protected bool NotEmpty
        {
            get { return (Options != null && Options.Length > 0); }
        }

        //Check if any of the options were clicked
        protected int CheckOptionsClicked()
        {
            //Make sure the drop down isn't empty
            if (NotEmpty == true)
            {
                Vector2 optionsize = LoadAssets.EditorFont.MeasureString(Options[0]);
                optionsize = new Vector2((int)optionsize.X, (int)optionsize.Y);

                //Go through all the options and check if one was clicked
                for (int i = 0; i < Options.Length; i++)
                {
                    Rectangle mouserect = new Rectangle((int)Mouse.GetState().X, (int)Mouse.GetState().Y, 1, 1);

                    Rectangle BoundRect = new Rectangle(Bounds.X + (int)BoundOffSet.X, (Bounds.Y + (int)BoundOffSet.Y) + ((i+1) * (int)optionsize.Y), Bounds.Width, Bounds.Height);

                    if (mouserect.Intersects(BoundRect) == true) return i;
                }
            }

            return -1;
        }

        //Check if the drop down or its options were clicked
        public override bool CheckClicked(MouseState mouse)
        {
            if (Focused == false) return base.CheckClicked(mouse);
            else
            {
                return (CheckOptionsClicked() >= 0);
            }
        }

        //Bring up the drop downs
        public override void Click()
        {
            if (Focused == false) Focused = true;
            else
            {
                int clickedoption = CheckOptionsClicked();

                if (clickedoption >= 0) SelectedOption = clickedoption;

                UnClick();
            }
        }

        //Close the drop downs
        public override void UnClick()
        {
            if (LostFocus != null) LostFocus();
            Focused = false;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //Draw the text describing the DropDown
            spriteBatch.DrawString(LoadAssets.EditorFont, DescriptionText, new Vector2(Bounds.X, Bounds.Y), Color.Black, 0f, new Vector2(0f, TextOrigin.Y), 1f, SpriteEffects.None, Button.ButtonDepth);

            //If the drop down isn't empty
            if (NotEmpty == true)
            {
                //If the Drop Down is focused, draw all the choices
                if (Focused == true)
                {
                    Vector2 optionsize = LoadAssets.EditorFont.MeasureString(Options[0]);
                    optionsize = new Vector2((int)optionsize.X, (int)optionsize.Y);

                    //Draw all the choices
                    for (int i = 0; i < Options.Length; i++)
                    {
                        spriteBatch.DrawString(LoadAssets.EditorFont, Options[i], new Vector2(Bounds.X + BoundOffSet.X, (Bounds.Y + BoundOffSet.Y) + ((i+1) * (int)optionsize.Y)), ControlColor, 0f, new Vector2(0f, TextOrigin.Y), 1f, SpriteEffects.None, Button.ButtonDepth);
                    }
                }
                //Otherwise draw only the selected choice
                else
                {
                    spriteBatch.DrawString(LoadAssets.EditorFont, Options[SelectedOption], new Vector2(Bounds.X + BoundOffSet.X, Bounds.Y + BoundOffSet.Y), ControlColor, 0f, new Vector2(0f, TextOrigin.Y), 1f, SpriteEffects.None, Button.ButtonDepth);
                }
            }

            EditorHelper.DrawOutlineRect(spriteBatch, Bounds, BoundOffSet);
        }
    }
}
