﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A simple label that displays text
    public class Label : Control
    {
        public const float LabelDepth = .9982f;

        //Choose to draw the label without the standard text origin
        private bool NoTextOrigin;

        public Label(Vector2 location, String text, Color textcolor)
        {
            Bounds = new Rectangle((int)location.X, (int)location.Y, 1, 1);

            ControlText = text;
            ControlColor = textcolor;
            NoTextOrigin = false;
        }

        public Label(Vector2 location, String text, Color textcolor, bool notextorigin) : this(location, text, textcolor)
        {
            NoTextOrigin = notextorigin;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //Don't draw the text if there's nothing to draw
            if (String.IsNullOrEmpty(ControlText) == false)
            {
                Vector2 textorigin = new Vector2(0f, TextOrigin.Y);
                if (NoTextOrigin == true) textorigin.Y = 0f;

                spriteBatch.DrawString(LoadAssets.EditorFont, ControlText, new Vector2(Bounds.X, ControlCenter.Y), ControlColor, 0f, textorigin, 1f, SpriteEffects.None, LabelDepth);
            }
        }
    }
}
