﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A box for entering information
    //USER EDITED
    public class TextBox : Control
    {
        //Method pointer; what the textbox does when it loses focus
        public delegate void LoseFocus(TextBox textbox);

        //The method to be invoked when the textbox loses focus
        private LoseFocus LostFocus;

        //The default text for the textbox if the text value is empty
        //private String DefaultText;

        //Tells if the textbox takes only numbers, letters, or both; true indicates numbers, false indicates letters, null means both
        protected bool? NumCharsOnly;

        //Temporary text so the user can cancel their options
        protected String TempText;

        //The minimum length allowed in the string in the textbox
        protected int MinLength;

        //The maximum length allowed in the string in the textbox
        protected int MaxLength;

        public TextBox(Rectangle size, bool? numcharsonly, String startingtext = "", int minlength = 0, int maxlength = 100)
        {
            Bounds = size;

            ControlText = startingtext;
            Focused = false;
            NumCharsOnly = numcharsonly;
            MinLength = minlength;
            MaxLength = MinLength + maxlength;

            LostFocus = null;
        }

        public TextBox(LoseFocus lostfocus, Rectangle size, bool? numcharsonly, String startingtext = "", int minlength = 0, int maxlength = 100) : this(size, numcharsonly, startingtext, minlength, maxlength)
        {
            LostFocus = lostfocus;
        }

        public TextBox(Rectangle size, bool? numcharsonly, Vector2 boundoffset, String startingtext = "", int minlength = 0, int maxlength = 100) : this(size, numcharsonly, startingtext, minlength, maxlength)
        {
            BoundOffSet = boundoffset;
        }

        public TextBox(LoseFocus lostfocus, Rectangle size, bool? numcharsonly, Vector2 boundoffset, String startingtext = "", int minlength = 0, int maxlength = 100) : this(size, numcharsonly, boundoffset, startingtext, minlength, maxlength)
        {
            LostFocus = lostfocus;
        }

        public TextBox(LoseFocus lostfocus, Rectangle size, bool? numcharsonly, Vector2 boundoffset, bool hidden, String startingtext = "", int minlength = 0, int maxlength = 100) : this(lostfocus, size, numcharsonly, boundoffset, startingtext, minlength, maxlength)
        {
            Hidden = hidden;
        }

        //Returns the value part of the text (it excludes part that acts as a label)
        public override object GetControlValue
        {
            get 
            {
                if (ControlText.Length < MinLength) return String.Empty;
                else return ControlText.Substring(MinLength); 
            }
        }

        //Applies a change to something when Enter is pressed
        protected virtual void ApplyChanges()
        {
            //ControlText = TempText;
            UnClick();
            //TempText = String.Empty;
        }

        public override void Click()
        {
            if (Focused == false)
            {
                Focused = true;
                TempText = ControlText;
            }
        }

        public override void UnClick()
        {
            ControlText = TempText;
            if (LostFocus != null) LostFocus(this);
            Focused = false;
            TempText = String.Empty;
        }

        //Ensures that a textbox's text value isn't empty by correcting it if necessary
        public void CorrectEmptyTextValue(String newtext)
        {
            if (ControlText.Length <= MinLength)
            {
                if (newtext != null)
                    ControlText += newtext;
                else ControlText += "Empty";
            }
        }

        //Make sure the key is a valid one
        private bool IsKeyValid(Keys key)
        {
            return (key == Keys.Back || key == Keys.Enter || EditorHelper.IsLetter(key) == true || EditorHelper.IsNumber(key) == true);
        }

        //Reads input into the textbox
        private void ReadTextInput(KeyboardState keyboard)
        {
            Keys[] pressedkeys = Keyboard.GetState().GetPressedKeys();

            for (int i = 0; i < pressedkeys.Length; i++)
            {
                //Make sure the key wasnt pressed before
                if (IsKeyValid(pressedkeys[i]) == true && Input.CheckKeyPress(keyboard, pressedkeys[i]) == true)
                {
                    //If backspace is pressed, remove a character
                    if (pressedkeys[i] == Keys.Back)
                    {
                        if (TempText.Length > 0 && TempText.Length > MinLength)
                            TempText = TempText.Remove(TempText.Length - 1, 1);
                    }
                    //If Enter is pressed, apply changes and make the textbox lose focus
                    else if (pressedkeys[i] == Keys.Enter)
                        ApplyChanges();
                    //Make sure this textbox takes valid input
                    else if (TempText.Length < MaxLength && (NumCharsOnly == null || (NumCharsOnly == true && EditorHelper.IsNumber(pressedkeys[i]) == true) || (NumCharsOnly == false && EditorHelper.IsLetter(pressedkeys[i]) == true)))
                    {
                        char character = EditorHelper.ConvertInput(pressedkeys[i]);

                        //Check for caps
                        if (Input.KeyHeld(Keys.LeftShift) == true || Input.KeyHeld(Keys.RightShift) == true)
                        {
                            TempText += char.ToUpper(character);
                        }
                        else TempText += char.ToLower(character);
                    }
                }
            }
        }

        public override void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {
            if (Focused == true)
                ReadTextInput(keyboard);
        }

        //Draws an outline around the textbox
        protected void DrawOutline(SpriteBatch spriteBatch)
        {
            //Draw an outline around the textbox
            EditorHelper.DrawOutlineRect(spriteBatch, Bounds, BoundOffSet);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(LoadAssets.EditorFont, Focused == true ? TempText : ControlText, new Vector2(Bounds.X, ControlCenter.Y), ControlColor, 0f, new Vector2(0f, TextOrigin.Y), 1f, SpriteEffects.None, Button.ButtonDepth);

            //Draw the outline
            DrawOutline(spriteBatch);
        }
    }
}
