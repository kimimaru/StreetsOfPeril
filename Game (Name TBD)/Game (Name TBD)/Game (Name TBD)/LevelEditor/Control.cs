﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game__Name_TBD_
{
    //A basic level editor control, like a label, text box, button, etc.
    public abstract class Control
    {
        //The color of the control
        protected Color ControlColor;

        //The text for the control
        protected String ControlText;

        //Tells if the control is focused or not
        protected bool Focused;

        //Tells if the control is hidden or not
        protected bool Hidden;

        //Tells if the control can be held down or not (Ex. a button increasing a value)
        protected bool CanHold;

        //The bounding box for the control
        protected Rectangle Bounds;

        //An Offset for the bounding box so Controls like TextBoxes can have parts that act like labels
        protected Vector2 BoundOffSet;

        public Control()
        {
            ControlColor = Color.Black;
            ControlText = String.Empty;

            Bounds = Rectangle.Empty;
            BoundOffSet = Vector2.Zero;
            Focused = false;
            Hidden = false;
            CanHold = false;
        }

        //Returns the text of the control
        public virtual String GetControlText
        {
            get { return ControlText ?? String.Empty; }
        }

        //Returns a value for the control
        public virtual object GetControlValue
        {
            get { return 0; }
        }

        //Tells if the Control is focused or not
        public bool IsFocused
        {
            get { return Focused; }
        }

        //Tells if the control is hidden or not
        public bool IsHidden
        {
            get { return Hidden; }
        }

        //The center of the control
        protected Vector2 ControlCenter
        {
            get { return new Vector2(Bounds.Center.X, Bounds.Center.Y); }
        }

        //Centers the text
        protected Vector2 TextOrigin
        {
            get
            {
                Vector2 textsize = LoadAssets.EditorFont.MeasureString(ControlText);

                return new Vector2((int)textsize.X / 2, (int)textsize.Y / 2);
            }
        }

        //Shows the control
        public void Show()
        {
            Hidden = false;
        }

        //Hides the Control
        public void Hide()
        {
            Hidden = true;
        }

        //Changes the color of the control
        public void ChangeColor(Color newcolor)
        {
            ControlColor = newcolor;
        }

        //Check mouse click
        public bool CheckMouseClick(MouseState mouse)
        {
            return ((CanHold == false && EditorHelper.LeftClicked(mouse) == true) || (CanHold == true && EditorHelper.MouseLeftHeld() == true));
        }

        //Check if the control was clicked
        public virtual bool CheckClicked(MouseState mouse)
        {
            Rectangle BoundRect = new Rectangle(Bounds.X + (int)BoundOffSet.X, Bounds.Y + (int)BoundOffSet.Y, Bounds.Width, Bounds.Height);

            return (EditorHelper.ClickedRect(BoundRect) == true);
        }

        public virtual bool CheckUnClicked(MouseState mouse)
        {
            return (Focused == true && CheckClicked(mouse) == false);
        }

        //Does something when the control is clicked
        public virtual void Click()
        {

        }

        //Does something when the control was unclicked
        public virtual void UnClick()
        {

        }

        //Does something when something about a control confirms a value (Ex. pressing Enter in a textbox)
        public virtual void Apply()
        {

        }

        public virtual void StringUpdate(String newcontroltext)
        {
            ControlText = newcontroltext;
        }

        public virtual void Update(float activeTime, MouseState mouse, KeyboardState keyboard, MainProgram main)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
