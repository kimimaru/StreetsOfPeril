﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game__Name_TBD_;

namespace Game__Name_TBD_
{
    //A stop point; it has a number and offset associated with - the offset dictates the point in the level at which the camera stops moving to spawn enemies
    [DataContract(Namespace = "")]
    public sealed class StopPoint
    {
        //The number of the stop point
        [DataMember]
        public int StopNumber;

        //The camera offset from the start of the level where the stop point stops the camera
        [DataMember]
        public Vector2 StopOffSet;

        //Whether the StopPoint is disabled or not
        [DataMember]
        public bool Disabled;

        public StopPoint()
        {
            StopNumber = 0;

            StopOffSet = Vector2.Zero;

            Disabled = false;
        }

        public StopPoint(int stopnumber, Vector2 stopoffset) : this()
        {
            StopNumber = stopnumber;

            StopOffSet = stopoffset;
        }

        //Since the camera is centered on the screen, we need to display the stoppoint starting there
        public static Vector2 GetScreenCenter
        {
            get { return new Vector2(MainProgram.LevelScreenSize.X / 2, MainProgram.LevelScreenSize.Y / 2); }
        }

        public Vector2 TrueLocation
        {
            get { return (GetScreenCenter + StopOffSet); }
        }

        //The selection rectangle for clicking on a stoppoint that has already been placed
        public Rectangle SelectionRectangle(Vector2 LevelOffSet)
        {
            Vector2 center = GetScreenCenter;
            return (new Rectangle((int)(center.X + StopOffSet.X + LevelOffSet.X) - (LoadAssets.StopPointIcon.Width / 2), (int)(center.Y + StopOffSet.Y + LevelOffSet.Y) - (LoadAssets.StopPointIcon.Height / 2), LoadAssets.StopPointIcon.Width, LoadAssets.StopPointIcon.Height));
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 LevelOffSet, TileEngine TileEngine)
        {
            //Draw the location of the stoppoint, offset from the center of the screen
            spriteBatch.Draw(LoadAssets.StopPointIcon, GetScreenCenter + StopOffSet + LevelOffSet, null, Disabled == false ? Color.White : Spawn.DisabledColor, 0f, new Vector2(LoadAssets.StopPointIcon.Width / 2, LoadAssets.StopPointIcon.Height / 2), 1f, SpriteEffects.None, .9951f);
            //DebugDraw(spriteBatch, LevelOffSet, TileEngine);
        }

        private void DebugDraw(SpriteBatch spriteBatch, Vector2 LevelOffSet, TileEngine TileEngine)
        {
            spriteBatch.Draw(LoadAssets.ButtonDefault, SelectionRectangle(LevelOffSet), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, .999f);
        }
    }
}
