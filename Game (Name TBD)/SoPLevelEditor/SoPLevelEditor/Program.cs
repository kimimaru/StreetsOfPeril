using System;
using Game__Name_TBD_;

namespace SoPLevelEditor
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            using (MainProgram game = new MainProgram())
            {
                game.Run();
            }
        }
    }
#endif
}

